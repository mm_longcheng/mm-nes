/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuBarcode_h__
#define __mmEmuBarcode_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuNes;

struct mmEmuBarcode
{
    struct mmEmuNes* nes;

    // For Barcode 1
    mmBool_t    m_bBarcode;
    mmByte_t    m_BarcodeOut;
    mmByte_t    m_BarcodePtr;
    mmInt_t     m_BarcodeCycles;
    mmByte_t    m_BarcodeData[256];

    // For Barcode 2
    mmBool_t    m_bBarcode2;
    mmInt_t     m_Barcode2seq;
    mmInt_t     m_Barcode2ptr;
    mmInt_t     m_Barcode2cnt;
    mmByte_t    m_Barcode2bit;
    mmByte_t    m_Barcode2data[32];
};

MM_EXPORT_EMU void mmEmuBarcode_Init(struct mmEmuBarcode* p);
MM_EXPORT_EMU void mmEmuBarcode_Destroy(struct mmEmuBarcode* p);

MM_EXPORT_EMU void mmEmuBarcode_SetParent(struct mmEmuBarcode* p, struct mmEmuNes* parent);

// Barcode battler(Bandai)
MM_EXPORT_EMU void mmEmuBarcode_SetBarcodeData(struct mmEmuBarcode* p, mmByte_t* code, mmInt_t len);
MM_EXPORT_EMU void mmEmuBarcode_Barcode(struct mmEmuBarcode* p, mmInt_t cycles);
static mmInline mmBool_t mmEmuBarcode_IsBarcodeEnable(const struct mmEmuBarcode* p) { return p->m_bBarcode; };
static mmInline mmByte_t mmEmuBarcode_GetBarcodeStatus(const struct mmEmuBarcode* p) { return p->m_BarcodeOut; };

// Barcode world(Sunsoft/EPOCH)
MM_EXPORT_EMU void mmEmuBarcode_SetBarcode2Data(struct mmEmuBarcode* p, mmByte_t* code, mmInt_t len);
MM_EXPORT_EMU mmByte_t mmEmuBarcode_Barcode2(struct mmEmuBarcode* p);
static mmInline mmBool_t mmEmuBarcode_IsBarcode2Enable(const struct mmEmuBarcode* p) { return p->m_bBarcode2; };

#include "core/mmSuffix.h"

#endif//__mmEmuBarcode_h__
