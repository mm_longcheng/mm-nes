/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadToprider.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuPadToprider_Init(struct mmEmuPadToprider* p)
{
    mmEmuExpad_Init(&p->super);

    p->rider_bita = 0;
    p->rider_bitb = 0;

    p->rider_pos = 0;
    p->rider_accel = 0;
    p->rider_brake = 0;
    p->rider_button = 0;

    //
    p->super.Reset = &mmEmuPadToprider_Reset;
    //p->super.Strobe = &mmEmuPadToprider_Strobe;
    //p->super.Read4016 = &mmEmuPadToprider_Read4016;
    p->super.Read4017 = &mmEmuPadToprider_Read4017;
    p->super.Write4016 = &mmEmuPadToprider_Write4016;
    //p->super.Write4017 = &mmEmuPadToprider_Write4017;
    p->super.Sync = &mmEmuPadToprider_Sync;
    p->super.SetSyncData = &mmEmuPadToprider_SetSyncData;
    p->super.GetSyncData = &mmEmuPadToprider_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadToprider_Destroy(struct mmEmuPadToprider* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->rider_bita = 0;
    p->rider_bitb = 0;

    p->rider_pos = 0;
    p->rider_accel = 0;
    p->rider_brake = 0;
    p->rider_button = 0;
}

MM_EXPORT_EMU void mmEmuPadToprider_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadToprider* p = (struct mmEmuPadToprider*)(super);

    p->rider_bita = 0;
    p->rider_bitb = 0;

    p->rider_pos = 0;
    p->rider_accel = 0;
    p->rider_brake = 0;
    p->rider_button = 0;
}

MM_EXPORT_EMU void mmEmuPadToprider_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadToprider* p = (struct mmEmuPadToprider*)(super);

    if (data & 0x01)
    {
        p->rider_bita = p->rider_bitb = 0;
        if (p->rider_pos > 0)
        {
            // RIGHT
            if (p->rider_pos > 0x10)
            {
                p->rider_bita |= (1 << 1) | (1 << 3);
            }
            else if (p->rider_pos > 0x0A)
            {
                p->rider_bita |= (1 << 1) | (0 << 3);
            }
            else if (p->rider_pos > 0x04)
            {
                p->rider_bita |= (0 << 1) | (1 << 3);
            }
        }
        else
        {
            // LEFT
            if (p->rider_pos < -0x10)
            {
                p->rider_bita |= (1 << 2) | (1 << 4);
            }
            else if (p->rider_pos < -0x0A)
            {
                p->rider_bita |= (1 << 2) | (0 << 4);
            }
            else if (p->rider_pos < -0x04)
            {
                p->rider_bita |= (0 << 2) | (1 << 4);
            }
        }
        if (p->rider_accel > 0x08 || p->rider_brake < 0x08)
        {
            if (p->rider_accel > 0x10)
            {
                p->rider_bitb |= (1 << 0);
            }
            else if (p->rider_accel > 0x0A)
            {
                p->rider_bitb |= (1 << 4);
            }
            else if (p->rider_accel > 0x04)
            {
                p->rider_bitb |= (1 << 5);
            }
        }
        else
        {
            p->rider_bita |= (1 << 5);
            if (p->rider_brake > 0x10)
            {
                p->rider_bitb |= (1 << 1);
            }
            else if (p->rider_brake > 0x0A)
            {
                p->rider_bitb |= (1 << 2);
            }
            else if (p->rider_brake > 0x04)
            {
                p->rider_bitb |= (1 << 3);
            }
        }
        // Wheelie.
        if (p->rider_button & 0x01)
        {
            p->rider_bita |= 0x80;
        }
        // SHIFT(Toggle)
        if (p->rider_button & 0x80)
        {
            p->rider_bita |= 0x40;
        }
        // START,SELECT
        if (p->rider_button & 0x10)
        {
            p->rider_bitb |= 0x40;
        }
        if (p->rider_button & 0x20)
        {
            p->rider_bitb |= 0x80;
        }
    }
}
MM_EXPORT_EMU mmByte_t mmEmuPadToprider_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadToprider* p = (struct mmEmuPadToprider*)(super);

    mmByte_t data = 0;
    data |= (p->rider_bita & 0x01) << 4;
    data |= (p->rider_bitb & 0x01) << 3;
    p->rider_bita >>= 1;
    p->rider_bitb >>= 1;
    return data;
}

MM_EXPORT_EMU void mmEmuPadToprider_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadToprider* p = (struct mmEmuPadToprider*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmWord_t* nButton = p->super.nes->config.controller.nButton[0];

    mmByte_t bit = 0;

    // Up
    if ((nButton[0] && mmEmuController_KeyboardStatus(controller, nButton[0])) ||
        (nButton[16] && mmEmuController_KeyboardStatus(controller, nButton[16])))
    {
        bit |= 1 << 4;
    }
    // Down
    if ((nButton[1] && mmEmuController_KeyboardStatus(controller, nButton[1])) ||
        (nButton[17] && mmEmuController_KeyboardStatus(controller, nButton[17])))
    {
        bit |= 1 << 5;
    }
    // Left
    if ((nButton[2] && mmEmuController_KeyboardStatus(controller, nButton[2])) ||
        (nButton[18] && mmEmuController_KeyboardStatus(controller, nButton[18])))
    {
        bit |= 1 << 6;
    }
    // Right
    if ((nButton[3] && mmEmuController_KeyboardStatus(controller, nButton[3])) ||
        (nButton[19] && mmEmuController_KeyboardStatus(controller, nButton[19])))
    {
        bit |= 1 << 7;
    }

    // Prohibit simultaneous left and right input.
    if ((bit&((1 << 6) | (1 << 7))) == ((1 << 6) | (1 << 7)))
    {
        bit &= ~((1 << 6) | (1 << 7));
    }

    // A,B
    if ((nButton[4] && mmEmuController_KeyboardStatus(controller, nButton[4])) ||
        (nButton[20] && mmEmuController_KeyboardStatus(controller, nButton[20])))
    {
        bit |= 1 << 0;
    }
    if ((nButton[5] && mmEmuController_KeyboardStatus(controller, nButton[5])) ||
        (nButton[21] && mmEmuController_KeyboardStatus(controller, nButton[21])))
    {
        bit |= 1 << 1;
    }

    // Start, Select
    if ((nButton[8] && mmEmuController_KeyboardStatus(controller, nButton[8])) ||
        (nButton[24] && mmEmuController_KeyboardStatus(controller, nButton[24])))
    {
        bit |= 1 << 2;
    }
    if ((nButton[9] && mmEmuController_KeyboardStatus(controller, nButton[9])) ||
        (nButton[25] && mmEmuController_KeyboardStatus(controller, nButton[25])))
    {
        bit |= 1 << 3;
    }

    if (!(bit & ((1 << 6) | (1 << 7))))
    {
        // To center...
        if (p->rider_pos)
        {
            p->rider_pos += (p->rider_pos > 0) ? -1 : 1;
        }
    }
    else if (bit & (1 << 6))
    {
        // LEFT
        p->rider_pos -= (p->rider_pos > -0x18) ? 1 : 0;
    }
    else if (bit & (1 << 7))
    {
        // RIGHT
        p->rider_pos += (p->rider_pos < 0x18) ? 1 : 0;
    }

    // Brake(A)
    if (bit & (1 << 0))
    {
        p->rider_brake += (p->rider_brake < 0x18) ? 1 : 0;
    }
    else
    {
        p->rider_brake -= (p->rider_brake > 0) ? 1 : 0;
    }
    // Accel(B)
    if (bit & (1 << 1))
    {
        p->rider_accel += (p->rider_accel < 0x18) ? 1 : 0;
    }
    else
    {
        p->rider_accel -= (p->rider_accel > 0) ? 1 : 0;
    }

    p->rider_button &= 0xC0;

    // Shift(UP)(Toggle)
    if (bit & (1 << 4))
    {
        if (!(p->rider_button & 0x40))
        {
            if (p->rider_button & 0x80)
            {
                p->rider_button &= ~0x80;
            }
            else
            {
                p->rider_button |= 0x80;
            }
        }
    }
    // old
    if (bit & (1 << 4))
    {
        p->rider_button |= 0x40;
    }
    else
    {
        p->rider_button &= ~0x40;
    }

    // Willey(DOWN)
    if (bit & (1 << 5))
    {
        p->rider_button |= 0x01;
    }
    // Start, Select
    if (bit & (1 << 2))
    {
        p->rider_button |= 0x20;
    }
    if (bit & (1 << 3))
    {
        p->rider_button |= 0x10;
    }
#if 0
    DEBUGOUT("RIDER POS=%d\n", rider_pos);
    DEBUGOUT("RIDER ACC=%d\n", rider_accel);
    DEBUGOUT("RIDER BRK=%d\n", rider_brake);
    DEBUGOUT("RIDER BTN=%02X\n", rider_button);
#endif
}
MM_EXPORT_EMU void mmEmuPadToprider_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadToprider* p = (struct mmEmuPadToprider*)(super);

    p->rider_pos = (mmInt_t)((mmSChar_t)(data & 0x000000FF));
    p->rider_accel = (mmInt_t)((mmSChar_t)((data >> 8) & 0x000000FF));
    p->rider_brake = (mmInt_t)((mmSChar_t)((data >> 16) & 0x000000FF));
    p->rider_button = (mmByte_t)(data >> 24);

#if 0
    DEBUGOUT("RIDER POS=%d\n", rider_pos);
    DEBUGOUT("RIDER ACC=%d\n", rider_accel);
    DEBUGOUT("RIDER BRK=%d\n", rider_brake);
    DEBUGOUT("RIDER BTN=%02X\n", rider_button);
#endif
}
MM_EXPORT_EMU mmLong_t mmEmuPadToprider_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadToprider* p = (struct mmEmuPadToprider*)(super);

    mmLong_t data = 0;

    data =
        (p->rider_pos & 0xFF) |
        ((p->rider_accel & 0xFF) << 8) |
        ((p->rider_brake & 0xFF) << 16) |
        (p->rider_button << 24);

    return data;
}

static struct mmEmuExpad* __static_mmEmuPadToprider_Produce(void)
{
    struct mmEmuPadToprider* p = (struct mmEmuPadToprider*)mmMalloc(sizeof(struct mmEmuPadToprider));
    mmEmuPadToprider_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadToprider_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadToprider* p = (struct mmEmuPadToprider*)(m);
    mmEmuPadToprider_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_TOPRIDER =
{
    &__static_mmEmuPadToprider_Produce,
    &__static_mmEmuPadToprider_Recycle,
};
