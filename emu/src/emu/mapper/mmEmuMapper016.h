/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper016_h__
#define __mmEmuMapper016_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

enum
{
    MM_EMU_X24C01_IDLE = 0, // Idle
    MM_EMU_X24C01_ADDRESS,  // Address set
    MM_EMU_X24C01_READ,     // Read
    MM_EMU_X24C01_WRITE,    // Write
    MM_EMU_X24C01_ACK,      // Acknowledge
    MM_EMU_X24C01_ACK_WAIT, // Acknowledge wait
};
struct mmEmuX24C01
{
    mmInt_t     now_state, next_state;
    mmInt_t     bitcnt;
    mmByte_t    addr, data;
    mmByte_t    sda;
    mmByte_t    scl_old, sda_old;

    mmByte_t*   pEEPDATA;
};
MM_EXPORT_EMU void mmEmuX24C01_Init(struct mmEmuX24C01* p);
MM_EXPORT_EMU void mmEmuX24C01_Destroy(struct mmEmuX24C01* p);
MM_EXPORT_EMU void mmEmuX24C01_Reset(struct mmEmuX24C01* p, mmByte_t* ptr);
MM_EXPORT_EMU void mmEmuX24C01_Write(struct mmEmuX24C01* p, mmByte_t scl_in, mmByte_t sda_in);
MM_EXPORT_EMU mmByte_t mmEmuX24C01_Read(struct mmEmuX24C01* p);
// For State save
MM_EXPORT_EMU void mmEmuX24C01_Load(struct mmEmuX24C01* p, const mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuX24C01_Save(const struct mmEmuX24C01* p, mmByte_t* buffer);

enum
{
    MM_EMU_X24C02_IDLE = 0, // Idle
    MM_EMU_X24C02_DEVADDR,  // Device address set
    MM_EMU_X24C02_ADDRESS,  // Address set
    MM_EMU_X24C02_READ,     // Read
    MM_EMU_X24C02_WRITE,    // Write
    MM_EMU_X24C02_ACK,      // Acknowledge
    MM_EMU_X24C02_NAK,      // Not Acknowledge
    MM_EMU_X24C02_ACK_WAIT, // Acknowledge wait
};

struct mmEmuX24C02
{
    mmInt_t     now_state, next_state;
    mmInt_t     bitcnt;
    mmByte_t    addr, data, rw;
    mmByte_t    sda;
    mmByte_t    scl_old, sda_old;

    mmByte_t*   pEEPDATA;
};
MM_EXPORT_EMU void mmEmuX24C02_Init(struct mmEmuX24C02* p);
MM_EXPORT_EMU void mmEmuX24C02_Destroy(struct mmEmuX24C02* p);
MM_EXPORT_EMU void mmEmuX24C02_Reset(struct mmEmuX24C02* p, mmByte_t* ptr);
MM_EXPORT_EMU void mmEmuX24C02_Write(struct mmEmuX24C02* p, mmByte_t scl_in, mmByte_t sda_in);
MM_EXPORT_EMU mmByte_t mmEmuX24C02_Read(struct mmEmuX24C02* p);
// For State save
MM_EXPORT_EMU void mmEmuX24C02_Load(struct mmEmuX24C02* p, const mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuX24C02_Save(const struct mmEmuX24C02* p, mmByte_t* buffer);

//----------------------------------------------------------------------//
// Mapper016  Bandai Standard                                           //
//----------------------------------------------------------------------//
struct mmEmuMapper016
{
    struct mmEmuMapper super;

    mmByte_t    patch;        // For Famicom Jump 2
    mmByte_t    eeprom_type;  // EEPROM type

    mmByte_t    reg[3];

    mmByte_t    irq_enable;
    mmInt_t     irq_counter;
    mmInt_t     irq_latch;
    mmByte_t    irq_type;

    struct mmEmuX24C01 x24c01;
    struct mmEmuX24C02 x24c02;
};

MM_EXPORT_EMU void mmEmuMapper016_Init(struct mmEmuMapper016* p);
MM_EXPORT_EMU void mmEmuMapper016_Destroy(struct mmEmuMapper016* p);

// For Datach Barcode Battler
MM_EXPORT_EMU void mmEmuMapper016_SetBarcodeData(struct mmEmuMapper016* p, mmByte_t* code, mmInt_t len);
// Normal mapper #16
MM_EXPORT_EMU void mmEmuMapper016_WriteSubA(struct mmEmuMapper016* p, mmWord_t addr, mmByte_t data);
// Famicom Jump 2
MM_EXPORT_EMU void mmEmuMapper016_WriteSubB(struct mmEmuMapper016* p, mmWord_t addr, mmByte_t data);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper016_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU mmByte_t mmEmuMapper016_ReadLow(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper016_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper016_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper016_HSync(struct mmEmuMapper* super, mmInt_t scanline);
MM_EXPORT_EMU void mmEmuMapper016_Clock(struct mmEmuMapper* super, mmInt_t cycles);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper016_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper016_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper016_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER016;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper016_h__
