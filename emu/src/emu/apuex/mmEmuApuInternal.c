/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApuInternal.h"

#include "emu/mmEmuState.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuCpu.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

// Dummy
#define APU_CLOCK   1789772.5f

// Volume shift
#define RECTANGLE_VOL_SHIFT 8
#define TRIANGLE_VOL_SHIFT  9
#define NOISE_VOL_SHIFT     8
#define DPCM_VOL_SHIFT      8

// Tables
static const mmInt_t vbl_length[32] =
{
     5, 127,   10,   1,   19,   2,   40,   3,
    80,   4,   30,   5,    7,   6,   13,   7,
     6,   8,   12,   9,   24,  10,   48,  11,
    96,  12,   36,  13,    8,  14,   16,  15,
};

static const mmInt_t freq_limit[8] =
{
    0x03FF, 0x0555, 0x0666, 0x071C, 0x0787, 0x07C1, 0x07E0, 0x07F0,
};
static const mmInt_t duty_lut[4] =
{
    2,  4,  8, 12,
};

static const mmInt_t noise_freq[16] =
{
      4,    8,   16,   32,   64,   96,  128,  160,
    202,  254,  380,  508,  762, 1016, 2034, 4068,
};

// DMC
static const mmInt_t dpcm_cycles_ntsc[16] =
{
    428, 380, 340, 320, 286, 254, 226, 214,
    190, 160, 142, 128, 106,  85,  72,  54,
};

static const mmInt_t dpcm_cycles_pal[16] =
{
    397, 353, 315, 297, 265, 235, 209, 198,
    176, 148, 131, 118,  98,  78,  66,  50,
};
static const mmInt_t dpcm_cycles_dendy[16] =
{
    428, 380, 340, 320, 286, 254, 226, 214,
    190, 160, 142, 128, 106,  85,  72,  54,
};

static const mmInt_t* dpcm_cycles_impl[3] =
{
    dpcm_cycles_ntsc,
    dpcm_cycles_pal,
    dpcm_cycles_dendy,
};
//mmInt_t   vol_effect[16] = 
//{
//  100,  94,  88,  83,  78,  74,  71,  67,
//   64,  61,  59,  56,  54,  52,  50,  48,
//};

static mmBool_t __static_mmEmuApuInternal_BufferGets(char buf[512], mmUInt8_t* buffer, size_t offset, size_t length, size_t* o);
static const mmInt_t* __static_mmEmuApuInternal_DpcmCycles(mmInt_t nMode)
{
    if (MM_EMU_NES_MODEL_NTSC <= nMode && nMode <= MM_EMU_NES_MODEL_DENDY)
    {
        return dpcm_cycles_impl[nMode];
    }
    else
    {
        return dpcm_cycles_impl[MM_EMU_NES_MODEL_NTSC];
    }
}

MM_EXPORT_EMU void mmEmuApuInternal_Init(struct mmEmuApuInternal* p)
{
    mmEmuApuInterface_Init(&p->super);
    //
    p->FrameCycle = 0;
    p->FrameCount = 0;
    p->FrameType = 0;
    p->FrameIRQ = 0xC0;
    p->FrameIRQoccur = 0;

    // Channels
    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuInternalRectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuInternalRectangle));
    mmMemset(&p->ch2, 0, sizeof(struct mmEmuApuInternalTriangle));
    mmMemset(&p->ch3, 0, sizeof(struct mmEmuApuInternalNoise));
    mmMemset(&p->ch4, 0, sizeof(struct mmEmuApuInternalDpcm));

    // $4015 Reg
    p->reg4015 = 0;
    p->sync_reg4015 = 0;

    // Sound
    p->cpu_clock = APU_CLOCK;
    p->sampling_rate = 22050;
    p->cycle_rate = (mmInt_t)(p->cpu_clock*65536.0f / 22050.0f);

    mmMemset(&p->bToneTableEnable, 0, sizeof(p->bToneTableEnable));
    mmMemset(&p->ToneTable, 0, sizeof(p->ToneTable));
    mmMemset(&p->ChannelTone, 0, sizeof(p->ChannelTone));

    p->bChangeTone = MM_FALSE;
    p->bDisableVolumeEffect = MM_FALSE;
    //
    p->super.Reset = &mmEmuApuInternal_Reset;
    p->super.Setup = &mmEmuApuInternal_Setup;
    p->super.Process = &mmEmuApuInternal_Process;
    p->super.Write = &mmEmuApuInternal_Write;
    p->super.Read = &mmEmuApuInternal_Read;
    p->super.SyncWrite = &mmEmuApuInternal_SyncWrite;
    p->super.SyncRead = &mmEmuApuInternal_SyncRead;
    p->super.Sync = &mmEmuApuInternal_Sync;
    p->super.GetFreq = &mmEmuApuInternal_GetFreq;
    p->super.GetStateSize = &mmEmuApuInternal_GetStateSize;
    p->super.SaveState = &mmEmuApuInternal_SaveState;
    p->super.LoadState = &mmEmuApuInternal_LoadState;
}
MM_EXPORT_EMU void mmEmuApuInternal_Destroy(struct mmEmuApuInternal* p)
{
    mmEmuApuInterface_Destroy(&p->super);
    //
    p->FrameCycle = 0;
    p->FrameCount = 0;
    p->FrameType = 0;
    p->FrameIRQ = 0xC0;
    p->FrameIRQoccur = 0;

    // Channels
    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuInternalRectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuInternalRectangle));
    mmMemset(&p->ch2, 0, sizeof(struct mmEmuApuInternalTriangle));
    mmMemset(&p->ch3, 0, sizeof(struct mmEmuApuInternalNoise));
    mmMemset(&p->ch4, 0, sizeof(struct mmEmuApuInternalDpcm));

    // $4015 Reg
    p->reg4015 = 0;
    p->sync_reg4015 = 0;

    // Sound
    p->cpu_clock = APU_CLOCK;
    p->sampling_rate = 22050;
    p->cycle_rate = (mmInt_t)(p->cpu_clock*65536.0f / 22050.0f);;

    mmMemset(&p->bToneTableEnable, 0, sizeof(p->bToneTableEnable));
    mmMemset(&p->ToneTable, 0, sizeof(p->ToneTable));
    mmMemset(&p->ChannelTone, 0, sizeof(p->ChannelTone));

    p->bChangeTone = MM_FALSE;
    p->bDisableVolumeEffect = MM_FALSE;
}

//
// Wavetable loader
//
MM_EXPORT_EMU void mmEmuApuInternal_ToneTableLoad(struct mmEmuApuInternal* p)
{
    struct mmEmuAssets* assets = &p->super.nes->assets;

    struct mmByteBuffer tone_byte_buffer;
    mmByteBuffer_Init(&tone_byte_buffer);
    mmEmuAssets_AcquireFileByteBuffer(assets, mmString_CStr(&assets->szToneFile), &tone_byte_buffer);
    mmEmuApuInternal_ToneTableAnalysis(p, tone_byte_buffer.buffer, tone_byte_buffer.offset, tone_byte_buffer.length);
    mmEmuAssets_ReleaseFileByteBuffer(assets, &tone_byte_buffer);
    mmByteBuffer_Destroy(&tone_byte_buffer);
}

MM_EXPORT_EMU void mmEmuApuInternal_ToneTableAnalysis(struct mmEmuApuInternal* p, mmUInt8_t* buffer, size_t offset, size_t length)
{
    if (0 != length)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        size_t o = 0;

        char buf[512] = { 0 };

        char c = 0;

        mmLogger_LogI(gLogger, "%s %d Loading ROM ToneTable file....", __FUNCTION__, __LINE__);

        // Analysis
        while (__static_mmEmuApuInternal_BufferGets(buf, buffer, offset, length, &o) == MM_TRUE)
        {
            if (buf[0] == ';' || strlen(buf) <= 0)
            {
                continue;
            }

            c = toupper(buf[0]);

            if (c == '@')
            {
                // Tone define
                char* pbuf = &buf[1];
                char* pptr;
                mmInt_t no, val;
                int i = 0;

                // Tone
                no = (mmInt_t)strtol(pbuf, &pptr, 10);
                if (pbuf == pptr)
                {
                    continue;
                }
                if (no < 0 || no > MM_EMU_APU_INTERNAL_TONEDATA_MAX - 1)
                {
                    continue;
                }

                // '='
                pptr = strchr(pbuf, '=');
                if (pptr == NULL)
                {
                    continue;
                }
                //
                pbuf = pptr + 1;
                // Tone
                for (i = 0; i < MM_EMU_APU_INTERNAL_TONEDATA_LEN; i++)
                {
                    val = (mmInt_t)strtol(pbuf, &pptr, 10);
                    if (pbuf == pptr)   // failure
                    {
                        break;
                    }
                    if (*pptr == ',')   // ,
                    {
                        pbuf = pptr + 1;
                    }
                    else
                    {
                        pbuf = pptr;
                    }

                    p->ToneTable[no][i] = val;
                }
                if (i >= MM_EMU_APU_INTERNAL_TONEDATA_MAX)
                {
                    p->bToneTableEnable[no] = MM_TRUE;
                }
            }
            else
            {
                if (c == 'A' || c == 'B')
                {
                    // Tone define
                    char* pbuf = &buf[1];
                    char* pptr;
                    int no, val;

                    // internal Tone
                    no = (mmInt_t)strtol(pbuf, &pptr, 10);
                    if (pbuf == pptr)
                    {
                        continue;
                    }
                    pbuf = pptr;
                    if (no < 0 || no > MM_EMU_APU_INTERNAL_TONE_MAX - 1)
                    {
                        continue;
                    }

                    // '='
                    pptr = strchr(pbuf, '=');
                    if (pptr == NULL)
                    {
                        continue;
                    }
                    //
                    pbuf = pptr + 1;

                    // Tone
                    val = (mmInt_t)strtol(pbuf, &pptr, 10);
                    if (pbuf == pptr)
                    {
                        continue;
                    }
                    pbuf = pptr;

                    if (val > MM_EMU_APU_INTERNAL_TONEDATA_MAX - 1)
                    {
                        continue;
                    }

                    if (val >= 0 && p->bToneTableEnable[val])
                    {
                        if (c == 'A')
                        {
                            p->ChannelTone[0][no] = val + 1;
                        }
                        else
                        {
                            p->ChannelTone[1][no] = val + 1;
                        }
                    }
                    else
                    {
                        if (c == 'A')
                        {
                            p->ChannelTone[0][no] = 0;
                        }
                        else
                        {
                            p->ChannelTone[1][no] = 0;
                        }
                    }
                }
                else
                {
                    if (c == 'C')
                    {
                        // Tone define
                        char* pbuf = &buf[1];
                        char* pptr;
                        int val;

                        // '='
                        pptr = strchr(pbuf, '=');
                        if (pptr == NULL)
                        {
                            continue;
                        }
                        //
                        pbuf = pptr + 1;

                        // Tone
                        val = (mmInt_t)strtol(pbuf, &pptr, 10);
                        if (pbuf == pptr)
                        {
                            continue;
                        }
                        pbuf = pptr;

                        if (val > MM_EMU_APU_INTERNAL_TONEDATA_MAX - 1)
                        {
                            continue;
                        }

                        if (val >= 0 && p->bToneTableEnable[val])
                        {
                            p->ChannelTone[2][0] = val + 1;
                        }
                        else
                        {
                            p->ChannelTone[2][0] = 0;
                        }
                    }
                }
            }
        }

        mmLogger_LogI(gLogger, "%s %d Loading ROM ToneTable complete.", __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_EMU void mmEmuApuInternal_SetChangeTone(struct mmEmuApuInternal* p, mmBool_t bChangeTone)
{
    p->bChangeTone = bChangeTone;
}
MM_EXPORT_EMU void mmEmuApuInternal_SetDisableVolumeEffect(struct mmEmuApuInternal* p, mmBool_t bDisableVolumeEffect)
{
    p->bDisableVolumeEffect = bDisableVolumeEffect;
}

MM_EXPORT_EMU void mmEmuApuInternal_GetFrameIRQ(struct mmEmuApuInternal* p, mmInt_t* cycle, mmByte_t* count, mmByte_t* type, mmByte_t* irq, mmByte_t* occur)
{
    *cycle = (mmInt_t)p->FrameCycle;
    *count = (mmByte_t)p->FrameCount;
    *type = (mmByte_t)p->FrameType;
    *irq = p->FrameIRQ;
    *occur = p->FrameIRQoccur;
}

MM_EXPORT_EMU void mmEmuApuInternal_SetFrameIRQ(struct mmEmuApuInternal* p, mmInt_t cycle, mmByte_t count, mmByte_t type, mmByte_t irq, mmByte_t occur)
{
    p->FrameCycle = cycle;
    p->FrameCount = (mmInt_t)count;
    p->FrameType = (mmInt_t)type;
    p->FrameIRQ = irq;
    p->FrameIRQoccur = occur;
}

MM_EXPORT_EMU void mmEmuApuInternal_SyncWrite4017(struct mmEmuApuInternal* p, mmByte_t data)
{
    p->FrameCycle = 0;
    p->FrameIRQ = data;
    p->FrameIRQoccur = 0;

    mmEmuCpu_ClrIRQ(&p->super.nes->cpu, MM_EMU_CPU_IRQ_FRAMEIRQ);

    p->FrameType = (data & 0x80) ? 1 : 0;
    p->FrameCount = 0;
    if (data & 0x80)
    {
        mmEmuApuInternal_UpdateFrame(p);
    }
    p->FrameCount = 1;
    p->FrameCycle = 14915;
}
MM_EXPORT_EMU void mmEmuApuInternal_UpdateFrame(struct mmEmuApuInternal* p)
{
    if (!p->FrameCount)
    {
        if (!(p->FrameIRQ & 0xC0) && mmEmuNes_GetFrameIRQmode(p->super.nes))
        {
            p->FrameIRQoccur = 0xFF;
            mmEmuCpu_SetIRQ(&p->super.nes->cpu, MM_EMU_CPU_IRQ_FRAMEIRQ);
        }
    }

    if (p->FrameCount == 3)
    {
        if (p->FrameIRQ & 0x80)
        {
            p->FrameCycle += 14915;
        }
    }

    // Counters Update
    mmEmuNes_Write(p->super.nes, 0x4018, (mmByte_t)p->FrameCount);

    p->FrameCount = (p->FrameCount + 1) & 3;
}

// rectangle
MM_EXPORT_EMU void mmEmuApuInternal_WriteRectangle(struct mmEmuApuInternal* p, mmInt_t no, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuInternalRectangle* ch = (no == 0) ? &p->ch0 : &p->ch1;

    ch->reg[addr & 3] = data;

    switch (addr & 3)
    {
    case 0:
        ch->holdnote = data & 0x20;
        ch->volume = data & 0x0F;
        ch->env_fixed = data & 0x10;
        ch->env_decay = (data & 0x0F) + 1;
        ch->duty = duty_lut[data >> 6];
        break;
    case 1:
        ch->swp_on = data & 0x80;
        ch->swp_inc = data & 0x08;
        ch->swp_shift = data & 0x07;
        ch->swp_decay = ((data >> 4) & 0x07) + 1;
        ch->freqlimit = freq_limit[data & 0x07];
        break;
    case 2:
        ch->freq = (ch->freq&(~0xFF)) + data;
        break;
    case 3: // Master
        ch->freq = ((data & 0x07) << 8) + (ch->freq & 0xFF);
        ch->len_count = vbl_length[data >> 3] * 2;
        ch->env_vol = 0x0F;
        ch->env_count = ch->env_decay + 1;
        ch->adder = 0;

        if (p->reg4015&(1 << no))
        {
            ch->enable = 0xFF;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuApuInternal_UpdateRectangle(struct mmEmuApuInternal* p, struct mmEmuApuInternalRectangle* ch, mmInt_t type)
{
    if (!ch->enable || ch->len_count <= 0)
    {
        return;
    }

    // Update Length/Sweep
    if (!(type & 1))
    {
        // Update Length
        if (ch->len_count && !ch->holdnote)
        {
            // Holdnote
            if (ch->len_count)
            {
                ch->len_count--;
            }
        }

        // Update Sweep
        if (ch->swp_on && ch->swp_shift)
        {
            if (ch->swp_count)
            {
                ch->swp_count--;
            }
            if (ch->swp_count == 0)
            {
                ch->swp_count = ch->swp_decay;
                if (ch->swp_inc)
                {
                    // Sweep increment(to higher frequency)
                    if (!ch->complement)
                    {
                        ch->freq += ~(ch->freq >> ch->swp_shift); // CH 0
                    }
                    else
                    {
                        ch->freq -= (ch->freq >> ch->swp_shift); // CH 1
                    }
                }
                else
                {
                    // Sweep decrement(to lower frequency)
                    ch->freq += (ch->freq >> ch->swp_shift);
                }
            }
        }
    }

    // Update Envelope
    if (ch->env_count)
    {
        ch->env_count--;
    }
    if (ch->env_count == 0)
    {
        ch->env_count = ch->env_decay;

        // Holdnote
        if (ch->holdnote)
        {
            ch->env_vol = (ch->env_vol - 1) & 0x0F;
        }
        else if (ch->env_vol)
        {
            ch->env_vol--;
        }
    }

    if (!ch->env_fixed)
    {
        ch->nowvolume = ch->env_vol << RECTANGLE_VOL_SHIFT;
    }
}
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_RenderRectangle(struct mmEmuApuInternal* p, struct mmEmuApuInternalRectangle* ch)
{
    mmInt_t volume = 0;

    if (!ch->enable || ch->len_count <= 0)
    {
        return 0;
    }

    // Channel disable?
    if ((ch->freq < 8) || (!ch->swp_inc && ch->freq > ch->freqlimit))
    {
        return 0;
    }

    if (ch->env_fixed)
    {
        ch->nowvolume = ch->volume << RECTANGLE_VOL_SHIFT;
    }
    volume = ch->nowvolume;

    if (!(p->bChangeTone && p->ChannelTone[(!ch->complement) ? 0 : 1][ch->reg[0] >> 6]))
    {
        // 
        mmInt_t freq = 0;
        double total;
        double sample_weight = ch->phaseacc;
        if (sample_weight > p->cycle_rate)
        {
            sample_weight = p->cycle_rate;
        }
        total = (ch->adder < ch->duty) ? sample_weight : -sample_weight;

        freq = MM_EMU_INT2FIX(ch->freq + 1);
        ch->phaseacc -= p->cycle_rate;
        while (ch->phaseacc < 0)
        {
            ch->phaseacc += freq;
            ch->adder = (ch->adder + 1) & 0x0F;

            sample_weight = freq;
            if (ch->phaseacc > 0)
            {
                sample_weight -= ch->phaseacc;
            }
            total += (ch->adder < ch->duty) ? sample_weight : -sample_weight;
        }
        return (mmInt_t)floor(volume*total / p->cycle_rate + 0.5);
    }
    else
    {
        mmInt_t freq = 0;
        mmInt_t num_times, total;

        mmInt_t* pTone = p->ToneTable[p->ChannelTone[(!ch->complement) ? 0 : 1][ch->reg[0] >> 6] - 1];

        // 
        ch->phaseacc -= p->cycle_rate * 2;
        if (ch->phaseacc >= 0)
        {
            return pTone[ch->adder & 0x1F] * volume / ((1 << RECTANGLE_VOL_SHIFT) / 2);
        }

        // 
        freq = MM_EMU_INT2FIX(ch->freq + 1);
        if (freq > p->cycle_rate * 2)
        {
            ch->phaseacc += freq;
            ch->adder = (ch->adder + 1) & 0x1F;
            return pTone[ch->adder & 0x1F] * volume / ((1 << RECTANGLE_VOL_SHIFT) / 2);
        }
        // 
        num_times = total = 0;
        while (ch->phaseacc < 0)
        {
            ch->phaseacc += freq;
            ch->adder = (ch->adder + 1) & 0x1F;
            total += pTone[ch->adder & 0x1F] * volume / ((1 << RECTANGLE_VOL_SHIFT) / 2);
            num_times++;
        }
        return total / num_times;
    }
}
// for sync
MM_EXPORT_EMU void mmEmuApuInternal_SyncWriteRectangle(struct mmEmuApuInternal* p, mmInt_t no, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuInternalRectangle* ch = (no == 0) ? &p->ch0 : &p->ch1;

    ch->sync_reg[addr & 3] = data;
    switch (addr & 3)
    {
    case 0:
        ch->sync_holdnote = data & 0x20;
        break;
    case 1:
    case 2:
        break;
    case 3: // Master
        ch->sync_len_count = vbl_length[data >> 3] * 2;
        if (p->sync_reg4015&(1 << no))
        {
            ch->sync_enable = 0xFF;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuApuInternal_SyncUpdateRectangle(struct mmEmuApuInternal* p, struct mmEmuApuInternalRectangle* ch, mmInt_t type)
{
    if (!ch->sync_enable || ch->sync_len_count <= 0)
    {
        return;
    }

    // Update Length
    if (ch->sync_len_count && !ch->sync_holdnote)
    {
        if (!(type & 1) && ch->sync_len_count)
        {
            ch->sync_len_count--;
        }
    }
}

// triangle
MM_EXPORT_EMU void mmEmuApuInternal_WriteTriangle(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data)
{
    p->ch2.reg[addr & 3] = data;
    switch (addr & 3)
    {
    case 0:
        p->ch2.holdnote = data & 0x80;
        break;
    case 1: // Unused
        break;
    case 2:
        p->ch2.freq = MM_EMU_INT2FIX(((((mmInt_t)p->ch2.reg[3] & 0x07) << 8) + (mmInt_t)data + 1));
        break;
    case 3: // Master
        p->ch2.freq = MM_EMU_INT2FIX(((((mmInt_t)data & 0x07) << 8) + (mmInt_t)p->ch2.reg[2] + 1));
        p->ch2.len_count = vbl_length[data >> 3] * 2;
        p->ch2.counter_start = 0x80;

        if (p->reg4015&(1 << 2))
        {
            p->ch2.enable = 0xFF;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuApuInternal_UpdateTriangle(struct mmEmuApuInternal* p, mmInt_t type)
{
    if (!p->ch2.enable)
    {
        return;
    }

    if (!(type & 1) && !p->ch2.holdnote)
    {
        if (p->ch2.len_count)
        {
            p->ch2.len_count--;
        }
    }

    //if( !p->ch2.len_count ) 
    //{
    //  p->ch2.lin_count = 0;
    //}

    // Update Length/Linear
    if (p->ch2.counter_start)
    {
        p->ch2.lin_count = p->ch2.reg[0] & 0x7F;
    }
    else if (p->ch2.lin_count)
    {
        p->ch2.lin_count--;
    }
    if (!p->ch2.holdnote && p->ch2.lin_count)
    {
        p->ch2.counter_start = 0;
    }
}
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_RenderTriangle(struct mmEmuApuInternal* p)
{
    mmInt_t vol;
    if (p->bDisableVolumeEffect)
    {
        vol = 256;
    }
    else
    {
        vol = 256 - (mmInt_t)((p->ch4.reg[1] & 0x01) + p->ch4.dpcm_value * 2);
    }

    if (!p->ch2.enable || (p->ch2.len_count <= 0) || (p->ch2.lin_count <= 0))
    {
        return p->ch2.nowvolume*vol / 256;
    }

    if (p->ch2.freq < MM_EMU_INT2FIX(8))
    {
        return p->ch2.nowvolume*vol / 256;
    }

    if (!(p->bChangeTone && p->ChannelTone[2][0]))
    {
        mmInt_t num_times, total;
        //
        p->ch2.phaseacc -= p->cycle_rate;
        if (p->ch2.phaseacc >= 0)
        {
            return p->ch2.nowvolume*vol / 256;
        }

        if (p->ch2.freq > p->cycle_rate)
        {
            p->ch2.phaseacc += p->ch2.freq;
            p->ch2.adder = (p->ch2.adder + 1) & 0x1F;

            if (p->ch2.adder < 0x10)
            {
                p->ch2.nowvolume = (p->ch2.adder & 0x0F) << TRIANGLE_VOL_SHIFT;
            }
            else
            {
                p->ch2.nowvolume = (0x0F - (p->ch2.adder & 0x0F)) << TRIANGLE_VOL_SHIFT;
            }

            return p->ch2.nowvolume*vol / 256;
        }

        // 
        num_times = total = 0;
        while (p->ch2.phaseacc < 0)
        {
            p->ch2.phaseacc += p->ch2.freq;
            p->ch2.adder = (p->ch2.adder + 1) & 0x1F;

            if (p->ch2.adder < 0x10)
            {
                p->ch2.nowvolume = (p->ch2.adder & 0x0F) << TRIANGLE_VOL_SHIFT;
            }
            else
            {
                p->ch2.nowvolume = (0x0F - (p->ch2.adder & 0x0F)) << TRIANGLE_VOL_SHIFT;
            }

            total += p->ch2.nowvolume;
            num_times++;
        }

        return (total / num_times)*vol / 256;
    }
    else
    {
        mmInt_t* pTone = p->ToneTable[p->ChannelTone[2][0] - 1];
        mmInt_t num_times, total;

        p->ch2.phaseacc -= p->cycle_rate;
        if (p->ch2.phaseacc >= 0)
        {
            return p->ch2.nowvolume*vol / 256;
        }

        if (p->ch2.freq > p->cycle_rate)
        {
            p->ch2.phaseacc += p->ch2.freq;
            p->ch2.adder = (p->ch2.adder + 1) & 0x1F;
            p->ch2.nowvolume = pTone[p->ch2.adder & 0x1F] * 0x0F;
            return p->ch2.nowvolume*vol / 256;
        }

        // 
        num_times = total = 0;
        while (p->ch2.phaseacc < 0)
        {
            p->ch2.phaseacc += p->ch2.freq;
            p->ch2.adder = (p->ch2.adder + 1) & 0x1F;
            total += pTone[p->ch2.adder & 0x1F] * 0x0F;
            num_times++;
        }

        return (total / num_times)*vol / 256;
    }
}
// for sync
MM_EXPORT_EMU void mmEmuApuInternal_SyncWriteTriangle(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data)
{
    p->ch2.sync_reg[addr & 3] = data;

    switch (addr & 3)
    {
    case 0:
        p->ch2.sync_holdnote = data & 0x80;
        break;
    case 1:
        break;
    case 2:
        break;
    case 3: // Master
        p->ch2.sync_len_count = vbl_length[p->ch2.sync_reg[3] >> 3] * 2;
        p->ch2.sync_counter_start = 0x80;

        if (p->sync_reg4015&(1 << 2))
        {
            p->ch2.sync_enable = 0xFF;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuApuInternal_SyncUpdateTriangle(struct mmEmuApuInternal* p, mmInt_t type)
{
    if (!p->ch2.sync_enable)
    {
        return;
    }

    if (!(type & 1) && !p->ch2.sync_holdnote)
    {
        if (p->ch2.sync_len_count)
        {
            p->ch2.sync_len_count--;
        }
    }

    //if( !p->ch2.sync_len_count ) 
    //{
    //  p->ch2.sync_lin_count = 0;
    //}

    // Update Length/Linear
    if (p->ch2.sync_counter_start)
    {
        p->ch2.sync_lin_count = p->ch2.sync_reg[0] & 0x7F;
    }
    else if (p->ch2.sync_lin_count)
    {
        p->ch2.sync_lin_count--;
    }
    if (!p->ch2.sync_holdnote && p->ch2.sync_lin_count)
    {
        p->ch2.sync_counter_start = 0;
    }
}

// noise
MM_EXPORT_EMU void mmEmuApuInternal_WriteNoise(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data)
{
    p->ch3.reg[addr & 3] = data;

    switch (addr & 3)
    {
    case 0:
        p->ch3.holdnote = data & 0x20;
        p->ch3.volume = data & 0x0F;
        p->ch3.env_fixed = data & 0x10;
        p->ch3.env_decay = (data & 0x0F) + 1;
        break;
    case 1: // Unused
        break;
    case 2:
        p->ch3.freq = MM_EMU_INT2FIX(noise_freq[data & 0x0F]);
        p->ch3.xor_tap = (data & 0x80) ? 0x40 : 0x02;
        break;
    case 3: // Master
        p->ch3.len_count = vbl_length[data >> 3] * 2;
        p->ch3.env_vol = 0x0F;
        p->ch3.env_count = p->ch3.env_decay + 1;

        if (p->reg4015&(1 << 3))
        {
            p->ch3.enable = 0xFF;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuApuInternal_UpdateNoise(struct mmEmuApuInternal* p, mmInt_t type)
{
    if (!p->ch3.enable || p->ch3.len_count <= 0)
    {
        return;
    }

    // Update Length
    if (!p->ch3.holdnote)
    {
        // Holdnote
        if (!(type & 1) && p->ch3.len_count)
        {
            p->ch3.len_count--;
        }
    }

    // Update Envelope
    if (p->ch3.env_count)
    {
        p->ch3.env_count--;
    }
    if (p->ch3.env_count == 0)
    {
        p->ch3.env_count = p->ch3.env_decay;

        // Holdnote
        if (p->ch3.holdnote)
        {
            p->ch3.env_vol = (p->ch3.env_vol - 1) & 0x0F;
        }
        else if (p->ch3.env_vol)
        {
            p->ch3.env_vol--;
        }
    }

    if (!p->ch3.env_fixed)
    {
        p->ch3.nowvolume = p->ch3.env_vol << RECTANGLE_VOL_SHIFT;
    }
}
MM_EXPORT_EMU mmByte_t mmEmuApuInternal_NoiseShiftreg(struct mmEmuApuInternal* p, mmByte_t xor_tap)
{
    int bit0, bit14;

    bit0 = p->ch3.shift_reg & 1;
    if (p->ch3.shift_reg & xor_tap)
    {
        bit14 = bit0 ^ 1;
    }
    else
    {
        bit14 = bit0 ^ 0;
    }
    p->ch3.shift_reg >>= 1;
    p->ch3.shift_reg |= (bit14 << 14);
    return (bit0 ^ 1);
}
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_RenderNoise(struct mmEmuApuInternal* p)
{
    mmInt_t vol = 0;
    mmInt_t num_times, total;
    //
    if (!p->ch3.enable || p->ch3.len_count <= 0)
    {
        return 0;
    }

    if (p->ch3.env_fixed)
    {
        p->ch3.nowvolume = p->ch3.volume << RECTANGLE_VOL_SHIFT;
    }

    vol = 256 - (mmInt_t)((p->ch4.reg[1] & 0x01) + p->ch4.dpcm_value * 2);

    p->ch3.phaseacc -= p->cycle_rate;
    if (p->ch3.phaseacc >= 0)
    {
        return p->ch3.output*vol / 256;
    }

    if (p->ch3.freq > p->cycle_rate)
    {
        p->ch3.phaseacc += p->ch3.freq;
        if (mmEmuApuInternal_NoiseShiftreg(p, p->ch3.xor_tap))
        {
            p->ch3.output = p->ch3.nowvolume;
        }
        else
        {
            p->ch3.output = -p->ch3.nowvolume;
        }

        return p->ch3.output*vol / 256;
    }

    num_times = total = 0;
    while (p->ch3.phaseacc < 0)
    {
        p->ch3.phaseacc += p->ch3.freq;
        if (mmEmuApuInternal_NoiseShiftreg(p, p->ch3.xor_tap))
        {
            p->ch3.output = p->ch3.nowvolume;
        }
        else
        {
            p->ch3.output = -p->ch3.nowvolume;
        }

        total += p->ch3.output;
        num_times++;
    }

    return (total / num_times)*vol / 256;
}
// for sync
MM_EXPORT_EMU void mmEmuApuInternal_SyncWriteNoise(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data)
{
    p->ch3.sync_reg[addr & 3] = data;

    switch (addr & 3)
    {
    case 0:
        p->ch3.sync_holdnote = data & 0x20;
        break;
    case 1:
        break;
    case 2:
        break;
    case 3: // Master
        p->ch3.sync_len_count = vbl_length[data >> 3] * 2;
        if (p->sync_reg4015&(1 << 3))
        {
            p->ch3.sync_enable = 0xFF;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuApuInternal_SyncUpdateNoise(struct mmEmuApuInternal* p, mmInt_t type)
{
    if (!p->ch3.sync_enable || p->ch3.sync_len_count <= 0)
    {
        return;
    }

    // Update Length
    if (p->ch3.sync_len_count && !p->ch3.sync_holdnote)
    {
        if (!(type & 1) && p->ch3.sync_len_count)
        {
            p->ch3.sync_len_count--;
        }
    }
}

// dpcm
MM_EXPORT_EMU void mmEmuApuInternal_WriteDPCM(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data)
{
    mmInt_t video_mode = 0;
    const mmInt_t* dpcm_cycles = NULL;

    p->ch4.reg[addr & 3] = data;

    video_mode = mmEmuNes_GetVideoMode(p->super.nes);
    dpcm_cycles = __static_mmEmuApuInternal_DpcmCycles(video_mode);

    switch (addr & 3)
    {
    case 0:
        p->ch4.freq = MM_EMU_INT2FIX(dpcm_cycles[data & 0x0F]);
        // p->ch4.freq    = INT2FIX( dpcm_cycles[data&0x0F] );
        // p->ch4.freq    = INT2FIX( (dpcm_cycles[data&0x0F]-((data&0x0F)^0x0F)*2-2) );
        p->ch4.looping = data & 0x40;
        break;
    case 1:
        p->ch4.dpcm_value = (data & 0x7F) >> 1;
        break;
    case 2:
        p->ch4.cache_addr = 0xC000 + (mmWord_t)(data << 6);
        break;
    case 3:
        p->ch4.cache_dmalength = ((data << 4) + 1) << 3;
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_RenderDPCM(struct mmEmuApuInternal* p)
{
    if (p->ch4.dmalength)
    {
        p->ch4.phaseacc -= p->cycle_rate;

        while (p->ch4.phaseacc < 0)
        {
            p->ch4.phaseacc += p->ch4.freq;
            if (!(p->ch4.dmalength & 7))
            {
                p->ch4.cur_byte = mmEmuNes_Read(p->super.nes, p->ch4.address);
                if (0xFFFF == p->ch4.address)
                {
                    p->ch4.address = 0x8000;
                }
                else
                {
                    p->ch4.address++;
                }
            }

            if (!(--p->ch4.dmalength))
            {
                if (p->ch4.looping)
                {
                    p->ch4.address = p->ch4.cache_addr;
                    p->ch4.dmalength = p->ch4.cache_dmalength;
                }
                else
                {
                    p->ch4.enable = 0;
                    break;
                }
            }
            // positive delta
            if (p->ch4.cur_byte&(1 << ((p->ch4.dmalength & 7) ^ 7)))
            {
                if (p->ch4.dpcm_value < 0x3F)
                {
                    p->ch4.dpcm_value += 1;
                }
            }
            else
            {
                // negative delta
                if (p->ch4.dpcm_value > 1)
                {
                    p->ch4.dpcm_value -= 1;
                }
            }
        }
    }

#if 1
    // (TEST)
    p->ch4.dpcm_output_real = (mmInt_t)((p->ch4.reg[1] & 0x01) + p->ch4.dpcm_value * 2) - 0x40;
    if (abs(p->ch4.dpcm_output_real - p->ch4.dpcm_output_fake) <= 8)
    {
        p->ch4.dpcm_output_fake = p->ch4.dpcm_output_real;
        p->ch4.output = (mmInt_t)p->ch4.dpcm_output_real << DPCM_VOL_SHIFT;
    }
    else
    {
        if (p->ch4.dpcm_output_real > p->ch4.dpcm_output_fake)
        {
            p->ch4.dpcm_output_fake += 8;
        }
        else
        {
            p->ch4.dpcm_output_fake -= 8;
        }
        p->ch4.output = (mmInt_t)p->ch4.dpcm_output_fake << DPCM_VOL_SHIFT;
    }
#else
    p->ch4.output = (((mmInt_t)p->ch4.reg[1] & 0x01) + (mmInt_t)p->ch4.dpcm_value * 2) << DPCM_VOL_SHIFT;
    // p->ch4.output = ((((mmInt_t)p->ch4.reg[1]&0x01)+(mmInt_t)p->ch4.dpcm_value*2)-0x40)<<DPCM_VOL_SHIFT;
#endif
    return p->ch4.output;
}
// for sync
MM_EXPORT_EMU void mmEmuApuInternal_SyncWriteDPCM(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data)
{
    mmInt_t video_mode = 0;
    const mmInt_t* dpcm_cycles = NULL;

    p->ch4.reg[addr & 3] = data;

    video_mode = mmEmuNes_GetVideoMode(p->super.nes);
    dpcm_cycles = __static_mmEmuApuInternal_DpcmCycles(video_mode);

    switch (addr & 3)
    {
    case 0:
        // p->ch4.sync_cache_cycles = dpcm_cycles[data&0x0f] * 8 - ((data&0x0f)^0x0f)*16 - 0x10;
        // p->ch4.sync_cycles       = 0;
        // p->ch4.sync_cache_cycles = dpcm_cycles[data&0x0f] * 8;
        p->ch4.sync_cache_cycles = dpcm_cycles[data & 0x0F] * 8;
        p->ch4.sync_looping = data & 0x40;
        p->ch4.sync_irq_gen = data & 0x80;
        if (!p->ch4.sync_irq_gen)
        {
            p->ch4.sync_irq_enable = 0;
            mmEmuCpu_ClrIRQ(&p->super.nes->cpu, MM_EMU_CPU_IRQ_DPCM);
        }
        break;
    case 1:
        break;
    case 2:
        break;
    case 3:
        p->ch4.sync_cache_dmalength = (data << 4) + 1;
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU mmBool_t mmEmuApuInternal_SyncUpdateDPCM(struct mmEmuApuInternal* p, mmInt_t cycles)
{
    mmBool_t bIRQ = MM_FALSE;

    if (p->ch4.sync_enable)
    {
        p->ch4.sync_cycles -= cycles;
        while (p->ch4.sync_cycles < 0)
        {
            p->ch4.sync_cycles += p->ch4.sync_cache_cycles;
            if (p->ch4.sync_dmalength)
            {
                // if( !(--p->ch4.sync_dmalength) ) {
                if (--p->ch4.sync_dmalength < 2)
                {
                    if (p->ch4.sync_looping)
                    {
                        p->ch4.sync_dmalength = p->ch4.sync_cache_dmalength;
                    }
                    else
                    {
                        p->ch4.sync_dmalength = 0;

                        if (p->ch4.sync_irq_gen)
                        {
                            p->ch4.sync_irq_enable = 0xFF;
                            mmEmuCpu_SetIRQ(&p->super.nes->cpu, MM_EMU_CPU_IRQ_DPCM);
                        }
                    }
                }
            }
        }
    }
    if (p->ch4.sync_irq_enable)
    {
        bIRQ = MM_TRUE;
    }

    return bIRQ;
}

MM_EXPORT_EMU void mmEmuApuInternal_Reset(struct mmEmuApuInterface* super, double fclock, mmInt_t nrate)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    mmWord_t addr;
    //
    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuInternalRectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuInternalRectangle));
    mmMemset(&p->ch2, 0, sizeof(struct mmEmuApuInternalTriangle));
    mmMemset(&p->ch3, 0, sizeof(struct mmEmuApuInternalNoise));
    //mmMemset( &p->ch4, 0, sizeof(struct mmEmuApuInternalDpcm) );

    mmMemset(&p->bToneTableEnable, 0, sizeof(mmStatus_t) * MM_EMU_APU_INTERNAL_TONEDATA_MAX);
    mmMemset(&p->ToneTable, 0, sizeof(mmInt_t) * MM_EMU_APU_INTERNAL_TONEDATA_MAX * MM_EMU_APU_INTERNAL_TONEDATA_LEN);
    mmMemset(&p->ChannelTone, 0, sizeof(mmInt_t) * MM_EMU_APU_INTERNAL_CHANNEL_MAX * MM_EMU_APU_INTERNAL_TONE_MAX);

    p->reg4015 = p->sync_reg4015 = 0;

    // Sweep complement
    p->ch0.complement = 0x00;
    p->ch1.complement = 0xFF;

    // Noise shift register
    p->ch3.shift_reg = 0x4000;

    (*(p->super.Setup))(super, fclock, nrate);

    // $4011 is not initialized
    for (addr = 0x4000; addr <= 0x4010; addr++)
    {
        (*(p->super.Write))(super, addr, 0x00);
        (*(p->super.SyncWrite))(super, addr, 0x00);
    }
    //(*(p->super.write))( super, 0x4001, 0x08 );
    //(*(p->super.write))( super, 0x4005, 0x08 );
    (*(p->super.Write))(super, 0x4012, 0x00);
    (*(p->super.Write))(super, 0x4013, 0x00);
    (*(p->super.Write))(super, 0x4015, 0x00);
    (*(p->super.SyncWrite))(super, 0x4012, 0x00);
    (*(p->super.SyncWrite))(super, 0x4013, 0x00);
    (*(p->super.SyncWrite))(super, 0x4015, 0x00);

    // $4017
    p->FrameIRQ = 0xC0;
    p->FrameCycle = 0;
    p->FrameIRQoccur = 0;
    p->FrameCount = 0;
    p->FrameType = 0;

    // ToneLoad
    mmEmuApuInternal_ToneTableLoad(p);
}
MM_EXPORT_EMU void mmEmuApuInternal_Setup(struct mmEmuApuInterface* super, double fclock, mmInt_t nrate)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    p->cpu_clock = fclock;
    p->sampling_rate = nrate;

    p->cycle_rate = (mmInt_t)(fclock*65536.0f / (float)nrate);
}

MM_EXPORT_EMU mmInt_t   mmEmuApuInternal_Process(struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    switch (channel)
    {
    case 0:
        return mmEmuApuInternal_RenderRectangle(p, &p->ch0);
    case 1:
        return mmEmuApuInternal_RenderRectangle(p, &p->ch1);
    case 2:
        return mmEmuApuInternal_RenderTriangle(p);
    case 3:
        return mmEmuApuInternal_RenderNoise(p);
    case 4:
        return mmEmuApuInternal_RenderDPCM(p);
    default:
        return 0;
    }

    return 0;
}

MM_EXPORT_EMU void mmEmuApuInternal_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    switch (addr)
    {
        // CH0,1 rectangle
    case 0x4000:    case 0x4001:
    case 0x4002:    case 0x4003:
    case 0x4004:    case 0x4005:
    case 0x4006:    case 0x4007:
        mmEmuApuInternal_WriteRectangle(p, (addr < 0x4004) ? 0 : 1, addr, data);
        break;

        // CH2 triangle
    case 0x4008:    case 0x4009:
    case 0x400A:    case 0x400B:
        mmEmuApuInternal_WriteTriangle(p, addr, data);
        break;

        // CH3 noise
    case 0x400C:    case 0x400D:
    case 0x400E:    case 0x400F:
        mmEmuApuInternal_WriteNoise(p, addr, data);
        break;

        // CH4 DPCM
    case 0x4010:    case 0x4011:
    case 0x4012:    case 0x4013:
        mmEmuApuInternal_WriteDPCM(p, addr, data);
        break;

    case 0x4015:
        p->reg4015 = data;

        if (!(data&(1 << 0)))
        {
            p->ch0.enable = 0;
            p->ch0.len_count = 0;
        }
        if (!(data&(1 << 1)))
        {
            p->ch1.enable = 0;
            p->ch1.len_count = 0;
        }
        if (!(data&(1 << 2)))
        {
            p->ch2.enable = 0;
            p->ch2.len_count = 0;
            p->ch2.lin_count = 0;
            p->ch2.counter_start = 0;
        }
        if (!(data&(1 << 3)))
        {
            p->ch3.enable = 0;
            p->ch3.len_count = 0;
        }
        if (!(data&(1 << 4)))
        {
            p->ch4.enable = 0;
            p->ch4.dmalength = 0;
        }
        else
        {
            p->ch4.enable = 0xFF;
            if (!p->ch4.dmalength)
            {
                p->ch4.address = p->ch4.cache_addr;
                p->ch4.dmalength = p->ch4.cache_dmalength;
                p->ch4.phaseacc = 0;
            }
        }
        break;

    case 0x4017:
        break;

        //  
    case 0x4018:
        mmEmuApuInternal_UpdateRectangle(p, &p->ch0, (mmInt_t)data);
        mmEmuApuInternal_UpdateRectangle(p, &p->ch1, (mmInt_t)data);
        mmEmuApuInternal_UpdateTriangle(p, (mmInt_t)data);
        mmEmuApuInternal_UpdateNoise(p, (mmInt_t)data);
        break;

    default:
        break;
    }
}

MM_EXPORT_EMU mmByte_t mmEmuApuInternal_Read(struct mmEmuApuInterface* super, mmWord_t addr)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    mmByte_t data = addr >> 8;

    if (addr == 0x4015)
    {
        data = 0;
        if (p->ch0.enable && p->ch0.len_count > 0) data |= (1 << 0);
        if (p->ch1.enable && p->ch1.len_count > 0) data |= (1 << 1);
        if (p->ch2.enable && p->ch2.len_count > 0) data |= (1 << 2);
        if (p->ch3.enable && p->ch3.len_count > 0) data |= (1 << 3);
    }
    return data;
}

MM_EXPORT_EMU void mmEmuApuInternal_SyncWrite(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    //struct mmLogger* gLogger = mmLogger_Instance();
    //mmLogger_LogV( gLogger, "%s %d $%04X=$%02X", __FUNCTION__, __LINE__, addr, data );

    switch (addr)
    {
        // CH0,1 rectangle
    case 0x4000:    case 0x4001:
    case 0x4002:    case 0x4003:
    case 0x4004:    case 0x4005:
    case 0x4006:    case 0x4007:
        mmEmuApuInternal_SyncWriteRectangle(p, (addr < 0x4004) ? 0 : 1, addr, data);
        break;

        // CH2 triangle
    case 0x4008:    case 0x4009:
    case 0x400A:    case 0x400B:
        mmEmuApuInternal_SyncWriteTriangle(p, addr, data);
        break;

        // CH3 noise
    case 0x400C:    case 0x400D:
    case 0x400E:    case 0x400F:
        mmEmuApuInternal_SyncWriteNoise(p, addr, data);
        break;

        // CH4 DPCM
    case 0x4010:    case 0x4011:
    case 0x4012:    case 0x4013:
        mmEmuApuInternal_SyncWriteDPCM(p, addr, data);
        break;

    case 0x4015:
        p->sync_reg4015 = data;

        if (!(data&(1 << 0)))
        {
            p->ch0.sync_enable = 0;
            p->ch0.sync_len_count = 0;
        }
        if (!(data&(1 << 1)))
        {
            p->ch1.sync_enable = 0;
            p->ch1.sync_len_count = 0;
        }
        if (!(data&(1 << 2)))
        {
            p->ch2.sync_enable = 0;
            p->ch2.sync_len_count = 0;
            p->ch2.sync_lin_count = 0;
            p->ch2.sync_counter_start = 0;
        }
        if (!(data&(1 << 3)))
        {
            p->ch3.sync_enable = 0;
            p->ch3.sync_len_count = 0;
        }
        if (!(data&(1 << 4)))
        {
            p->ch4.sync_enable = 0;
            p->ch4.sync_dmalength = 0;
            p->ch4.sync_irq_enable = 0;

            mmEmuCpu_ClrIRQ(&p->super.nes->cpu, MM_EMU_CPU_IRQ_DPCM);
        }
        else
        {
            p->ch4.sync_enable = 0xFF;
            if (!p->ch4.sync_dmalength)
            {
                // p->ch4.sync_cycles    = p->ch4.sync_cache_cycles;
                p->ch4.sync_dmalength = p->ch4.sync_cache_dmalength;
                p->ch4.sync_cycles = 0;
            }
        }
        break;

    case 0x4017:
        mmEmuApuInternal_SyncWrite4017(p, data);
        break;

        // 
    case 0x4018:
        mmEmuApuInternal_SyncUpdateRectangle(p, &p->ch0, (mmInt_t)data);
        mmEmuApuInternal_SyncUpdateRectangle(p, &p->ch1, (mmInt_t)data);
        mmEmuApuInternal_SyncUpdateTriangle(p, (mmInt_t)data);
        mmEmuApuInternal_SyncUpdateNoise(p, (mmInt_t)data);
        break;

    default:
        break;
    }
}

MM_EXPORT_EMU mmByte_t mmEmuApuInternal_SyncRead(struct mmEmuApuInterface* super, mmWord_t addr)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmByte_t data = addr >> 8;

    if (addr == 0x4015)
    {
        data = 0;
        if (p->ch0.sync_enable && p->ch0.sync_len_count > 0)    data |= (1 << 0);
        if (p->ch1.sync_enable && p->ch1.sync_len_count > 0)    data |= (1 << 1);
        if (p->ch2.sync_enable && p->ch2.sync_len_count > 0)    data |= (1 << 2);
        if (p->ch3.sync_enable && p->ch3.sync_len_count > 0)    data |= (1 << 3);
        if (p->ch4.sync_enable && p->ch4.sync_dmalength)        data |= (1 << 4);
        if (p->FrameIRQoccur)                                   data |= (1 << 6);
        if (p->ch4.sync_irq_enable)                             data |= (1 << 7);
        p->FrameIRQoccur = 0;

        mmEmuCpu_ClrIRQ(&p->super.nes->cpu, MM_EMU_CPU_IRQ_FRAMEIRQ);
        //mmLogger_LogV( gLogger, "%s %d R 4015 %02X", __FUNCTION__, __LINE__, data );
    }
    if (addr == 0x4017)
    {
        if (p->FrameIRQoccur)
        {
            data = 0;
        }
        else
        {
            data |= (1 << 6);
        }
        //mmLogger_LogV( gLogger, "%s %d R 4017 %02X", __FUNCTION__, __LINE__, data );
    }
    return data;
}

MM_EXPORT_EMU mmBool_t mmEmuApuInternal_Sync(struct mmEmuApuInterface* super, mmInt_t cycles)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    p->FrameCycle -= cycles * 2;
    if (p->FrameCycle <= 0)
    {
        p->FrameCycle += 14915;

        mmEmuApuInternal_UpdateFrame(p);
    }

    return p->FrameIRQoccur | mmEmuApuInternal_SyncUpdateDPCM(p, cycles);
}

MM_EXPORT_EMU mmInt_t mmEmuApuInternal_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    mmInt_t freq = 0;

    // Rectangle
    if (channel == 0 || channel == 1)
    {
        struct mmEmuApuInternalRectangle* ch;
        if (channel == 0)
        {
            ch = &p->ch0;
        }
        else
        {
            ch = &p->ch1;
        }
        if (!ch->enable || ch->len_count <= 0)
        {
            return 0;
        }
        if ((ch->freq < 8) || (!ch->swp_inc && ch->freq > ch->freqlimit))
        {
            return 0;
        }

        if (!ch->volume)
        {
            return 0;
        }

        // freq = (((mmInt_t)ch->reg[3]&0x07)<<8)+(INT)ch->reg[2]+1;
        freq = (mmInt_t)(16.0f*p->cpu_clock / (double)(ch->freq + 1));
        return freq;
    }

    // Triangle
    if (channel == 2)
    {
        if (!p->ch2.enable || p->ch2.len_count <= 0)
        {
            return 0;
        }
        if (p->ch2.lin_count <= 0 || p->ch2.freq < MM_EMU_INT2FIX(8))
        {
            return 0;
        }
        freq = (((mmInt_t)p->ch2.reg[3] & 0x07) << 8) + (mmInt_t)p->ch2.reg[2] + 1;
        freq = (mmInt_t)(8.0f*p->cpu_clock / (double)freq);
        return freq;
    }

    // Noise
    if (channel == 3)
    {
        if (!p->ch3.enable || p->ch3.len_count <= 0)
        {
            return 0;
        }
        if (p->ch3.env_fixed)
        {
            if (!p->ch3.volume)
            {
                return 0;
            }
        }
        else
        {
            if (!p->ch3.env_vol)
            {
                return 0;
            }
        }
        return 1;
    }

    // DPCM
    if (channel == 4)
    {
        if (p->ch4.enable && p->ch4.dmalength)
        {
            return 1;
        }
    }

    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuInternal_GetStateSize(const struct mmEmuApuInterface* super)
{
    // struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    return 4 * sizeof(mmByte_t) + 3 * sizeof(mmInt_t) +
        sizeof(struct mmEmuApuInternalRectangle) +
        sizeof(struct mmEmuApuInternalRectangle) +
        sizeof(struct mmEmuApuInternalTriangle) +
        sizeof(struct mmEmuApuInternalNoise) +
        sizeof(struct mmEmuApuInternalDpcm);
}

MM_EXPORT_EMU void mmEmuApuInternal_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    MM_EMU_SETBYTE(buffer, p->reg4015);
    MM_EMU_SETBYTE(buffer, p->sync_reg4015);

    MM_EMU_SETINT(buffer, (mmInt_t)p->FrameCycle);
    MM_EMU_SETINT(buffer, p->FrameCount);
    MM_EMU_SETINT(buffer, p->FrameType);
    MM_EMU_SETBYTE(buffer, p->FrameIRQ);
    MM_EMU_SETBYTE(buffer, p->FrameIRQoccur);

    MM_EMU_SETBLOCK(buffer, &p->ch0, sizeof(struct mmEmuApuInternalRectangle));
    MM_EMU_SETBLOCK(buffer, &p->ch1, sizeof(struct mmEmuApuInternalRectangle));
    MM_EMU_SETBLOCK(buffer, &p->ch2, sizeof(struct mmEmuApuInternalTriangle));
    MM_EMU_SETBLOCK(buffer, &p->ch3, sizeof(struct mmEmuApuInternalNoise));
    MM_EMU_SETBLOCK(buffer, &p->ch4, sizeof(struct mmEmuApuInternalDpcm));
}

MM_EXPORT_EMU void mmEmuApuInternal_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuInternal* p = (struct mmEmuApuInternal*)(super);
    //
    MM_EMU_GETBYTE(buffer, p->reg4015);
    MM_EMU_GETBYTE(buffer, p->sync_reg4015);

    MM_EMU_GETINT(buffer, p->FrameCycle);
    MM_EMU_GETINT(buffer, p->FrameCount);
    MM_EMU_GETINT(buffer, p->FrameType);
    MM_EMU_GETBYTE(buffer, p->FrameIRQ);
    MM_EMU_GETBYTE(buffer, p->FrameIRQoccur);

    MM_EMU_GETBLOCK(buffer, &p->ch0, sizeof(struct mmEmuApuInternalRectangle));
    MM_EMU_GETBLOCK(buffer, &p->ch1, sizeof(struct mmEmuApuInternalRectangle));
    MM_EMU_GETBLOCK(buffer, &p->ch2, sizeof(struct mmEmuApuInternalTriangle));
    MM_EMU_GETBLOCK(buffer, &p->ch3, sizeof(struct mmEmuApuInternalNoise));
    //  buffer += sizeof(p->ch3);
    MM_EMU_GETBLOCK(buffer, &p->ch4, sizeof(struct mmEmuApuInternalDpcm));
}

static mmBool_t __static_mmEmuApuInternal_BufferGets(char buf[512], mmUInt8_t* buffer, size_t offset, size_t length, size_t* o)
{
    char c = 0;
    size_t b = (*o);
    size_t l = 0;
    size_t i = 0;
    const char* d = (const char*)(buffer + offset);
    while ((*o) + l < length)
    {
        c = d[b + l];
        i++;
        if (c == '\n' || c == '\r')
        {
            break;
        }
        l++;
    }
    (*o) += i;
    mmMemcpy(buf, d + b, l);
    buf[l] = 0;
    return (*o) < length;
}
