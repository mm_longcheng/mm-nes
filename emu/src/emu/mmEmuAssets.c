/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuAssets.h"
#include "mmEmuNes.h"

#include "core/mmTime.h"
#include "core/mmTimeCache.h"
#include "core/mmFilePath.h"

MM_EXPORT_EMU void mmEmuAssets_Init(struct mmEmuAssets* p)
{
    p->nes = NULL;

    mmFileContext_Init(&p->hFileContext);

    mmString_Init(&p->szRomPath);
    mmString_Init(&p->szRomName);
    mmString_Init(&p->szWritablePath);

    mmString_Init(&p->szRomDirectory);
    mmString_Init(&p->szRom);
    mmString_Init(&p->szRomSuffix);

    mmString_Init(&p->szDISKSYSFile);
    mmString_Init(&p->szRomFile);
    mmString_Init(&p->szIpsFile);
    mmString_Init(&p->szToneFile);

    mmString_Init(&p->szGenieFile);

    mmString_Assigns(&p->szWritablePath, ".");

    mmString_Assigns(&p->szDISKSYSFile, "DISKSYS.ROM");
}
MM_EXPORT_EMU void mmEmuAssets_Destroy(struct mmEmuAssets* p)
{
    p->nes = NULL;

    mmFileContext_Destroy(&p->hFileContext);

    mmString_Destroy(&p->szRomPath);
    mmString_Destroy(&p->szRomName);
    mmString_Destroy(&p->szWritablePath);

    mmString_Destroy(&p->szRomDirectory);
    mmString_Destroy(&p->szRom);
    mmString_Destroy(&p->szRomSuffix);

    mmString_Destroy(&p->szDISKSYSFile);
    mmString_Destroy(&p->szRomFile);
    mmString_Destroy(&p->szIpsFile);
    mmString_Destroy(&p->szToneFile);

    mmString_Destroy(&p->szGenieFile);
}

MM_EXPORT_EMU void mmEmuAssets_SetParent(struct mmEmuAssets* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU void mmEmuAssets_SetRomPath(struct mmEmuAssets* p, const char* szRomPath)
{
    struct mmString basename;
    struct mmString pathname;
    struct mmString romname;

    mmString_Init(&basename);
    mmString_Init(&pathname);
    mmString_Init(&romname);

    mmString_Assigns(&p->szRomPath, szRomPath);

    mmPathSplitFileName(&p->szRomPath, &basename, &pathname);
    mmDirectoryNoneSuffix(&pathname, mmString_CStr(&pathname));

    mmPathSplitSuffixName(&basename, &romname, &p->szRomSuffix);

    mmString_Assign(&p->szRomDirectory, &pathname);
    mmString_Assign(&p->szRom, &romname);

    // most time if the szRomName is empty, we use szRom for it.
    mmString_Assign(&p->szRomName, &romname);

    mmString_Destroy(&basename);
    mmString_Destroy(&pathname);
    mmString_Destroy(&romname);
}
MM_EXPORT_EMU void mmEmuAssets_SetRomName(struct mmEmuAssets* p, const char* szRomName)
{
    mmString_Assigns(&p->szRomName, szRomName);
}
MM_EXPORT_EMU void mmEmuAssets_SetWritablePath(struct mmEmuAssets* p, const char* szWritablePath)
{
    mmString_Assigns(&p->szWritablePath, szWritablePath);
    mmDirectoryNoneSuffix(&p->szWritablePath, mmString_CStr(&p->szWritablePath));
}
MM_EXPORT_EMU void mmEmuAssets_SetResourceRoot(struct mmEmuAssets* p, mmUInt32_t type_assets, const char* root_path, const char* root_base)
{
    switch (type_assets)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
        mmFileContext_SetAssetsRootFolder(&p->hFileContext, root_path, root_base);
        break;
    case MM_FILE_ASSETS_TYPE_SOURCE:
        mmFileContext_SetAssetsRootSource(&p->hFileContext, root_path, root_base);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuAssets_GetGenieFile(struct mmEmuAssets* p, struct mmString* szFileName)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    mmString_Init(&path);

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szGeniePath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s.gen", mmString_CStr(&p->szRomName));
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Assign(&p->szGenieFile, szFileName);

    mmString_Destroy(&path);
}

MM_EXPORT_EMU void mmEmuAssets_GetSnapshotFile(struct mmEmuAssets* p, struct mmString* szFileName, mmBool_t png, struct timeval* tv)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct tm tm;
    struct mmTimeInfo tp;
    char time_path[64] = { 0 };
    const char* suffix = png ? "png" : "bmp";

    struct mmString path;

    mmString_Init(&path);

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szSnapshotPath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    tp.sec = tv->tv_sec;
    tp.msec = tv->tv_usec / 1000;
    mmTime_Tm(&tp, &tm);

    mmSprintf(time_path, "%4d%02d%02d_%02d%02d%02d_%03d",
        tm.mmTm_year, tm.mmTm_mon ,
        tm.mmTm_mday, tm.mmTm_hour,
        tm.mmTm_min , tm.mmTm_sec , tp.msec);

    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s/%s/%s_%s.%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szSnapshotPath), 
        mmString_CStr(&p->szRomName), 
        time_path, 
        suffix);
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Destroy(&path);
}

MM_EXPORT_EMU void mmEmuAssets_GetCheatCodeFile(struct mmEmuAssets* p, struct mmString* szFileName, const char* vct)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    const char* szRealFile = (0 == mmStrlen(vct)) ? mmString_CStr(&p->szRomName) : vct;

    mmString_Init(&path);

    // not append .vmv, just use the input file name.
    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s", szRealFile);
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szCheatCodePath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Destroy(&path);
}
MM_EXPORT_EMU void mmEmuAssets_GetTurboFile(struct mmEmuAssets* p, struct mmString* szFileName)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    mmString_Init(&path);

    // append .vtf
    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s/%s/%s.vtf", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szTurboPath), 
        mmString_CStr(&p->szRomName));
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szTurboPath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Destroy(&path);
}
MM_EXPORT_EMU void mmEmuAssets_GetMovieFile(struct mmEmuAssets* p, struct mmString* szFileName, const char* vmv)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    const char* szRealFile = (0 == mmStrlen(vmv)) ? mmString_CStr(&p->szRomName) : vmv;

    mmString_Init(&path);

    // not append .vmv, just use the input file name.
    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s/%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szMoviePath), 
        szRealFile);
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szMoviePath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Destroy(&path);
}
MM_EXPORT_EMU void mmEmuAssets_GetStateFile(struct mmEmuAssets* p, struct mmString* szFileName, const char* stn)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    const char* szRealFile = (0 == mmStrlen(stn)) ? mmString_CStr(&p->szRomName) : stn;

    mmString_Init(&path);

    // not append .sta, just use the input file name.
    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s/%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szStatePath), 
        szRealFile);
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szStatePath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Destroy(&path);
}
MM_EXPORT_EMU void mmEmuAssets_GetTapeFile(struct mmEmuAssets* p, struct mmString* szFileName)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    mmString_Init(&path);

    // append .vtp
    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s/%s/%s.vtp", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szTapePath), 
        mmString_CStr(&p->szRomName));
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szTapePath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Destroy(&path);
}
MM_EXPORT_EMU void mmEmuAssets_GetSaveFile(struct mmEmuAssets* p, struct mmString* szFileName)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    mmString_Init(&path);

    // append .sav
    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s/%s/%s.sav", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szSavePath), 
        mmString_CStr(&p->szRomName));
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szSavePath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Destroy(&path);
}
MM_EXPORT_EMU void mmEmuAssets_GetDiskFile(struct mmEmuAssets* p, struct mmString* szFileName)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    mmString_Init(&path);

    // append .dsv
    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s/%s/%s.dsv", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szDiskPath), 
        mmString_CStr(&p->szRomName));
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szDiskPath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Destroy(&path);
}
MM_EXPORT_EMU void mmEmuAssets_GetNetworkFile(struct mmEmuAssets* p, struct mmString* szFileName)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    mmString_Init(&path);

    // append .stn
    mmString_Clear(szFileName);
    mmString_Sprintf(szFileName, "%s/%s/%s.stn", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szNetworkPath), 
        mmString_CStr(&p->szRomName));
    mmCleanPath(szFileName, mmString_CStr(szFileName));

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szNetworkPath));
    mmMkdirIfNotExist(mmString_CStr(&path));

    mmString_Destroy(&path);
}

MM_EXPORT_EMU void mmEmuAssets_AddResourceFolder(struct mmEmuAssets* p, const char* name, const char* path, const char* base)
{
    mmFileContext_AddAssetsFolder(&p->hFileContext, name, path, base);
}
MM_EXPORT_EMU void mmEmuAssets_RmvResourceFolder(struct mmEmuAssets* p, const char* name)
{
    mmFileContext_RmvAssetsFolder(&p->hFileContext, name);
}
//
MM_EXPORT_EMU void mmEmuAssets_AddResourceSource(struct mmEmuAssets* p, const char* name, const char* path, const char* base)
{
    mmFileContext_AddAssetsSource(&p->hFileContext, name, path, base);
}
MM_EXPORT_EMU void mmEmuAssets_RmvResourceSource(struct mmEmuAssets* p, const char* name)
{
    mmFileContext_RmvAssetsSource(&p->hFileContext, name);
}

MM_EXPORT_EMU void mmEmuAssets_AcquireFileByteBuffer(struct mmEmuAssets* p, const char* path_name, struct mmByteBuffer* byte_buffer)
{
    if (1 == mmFileContext_IsFileExists(&p->hFileContext, path_name))
    {
        mmFileContext_AcquireFileByteBuffer(&p->hFileContext, path_name, byte_buffer);
    }
}
MM_EXPORT_EMU void mmEmuAssets_ReleaseFileByteBuffer(struct mmEmuAssets* p, struct mmByteBuffer* byte_buffer)
{
    if (0 != byte_buffer->length)
    {
        mmFileContext_ReleaseFileByteBuffer(&p->hFileContext, byte_buffer);
    }
}

MM_EXPORT_EMU void mmEmuAssets_MakeDirectory(struct mmEmuAssets* p)
{
    struct mmEmuPathConfig* path_config = &p->nes->config.path;

    struct mmString path;

    mmString_Init(&path);

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szIpsPath));
    mmMkdirIfNotExist(mmString_CStr(&path));
    mmFileContext_AddAssetsFolder(&p->hFileContext, mmString_CStr(&path), mmString_CStr(&path), "");

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szTonePath));
    mmMkdirIfNotExist(mmString_CStr(&path));
    mmFileContext_AddAssetsFolder(&p->hFileContext, mmString_CStr(&path), mmString_CStr(&path), "");

    mmString_Clear(&path);
    mmString_Sprintf(&path, "%s/%s", 
        mmString_CStr(&p->szWritablePath), 
        mmString_CStr(&path_config->szGeniePath));
    mmMkdirIfNotExist(mmString_CStr(&path));
    mmFileContext_AddAssetsFolder(&p->hFileContext, mmString_CStr(&path), mmString_CStr(&path), "");

    // file.
    mmString_Clear(&p->szRomFile);
    mmString_Assign(&p->szRomFile, &p->szRomPath);
    mmCleanPath(&p->szRomFile, mmString_CStr(&p->szRomFile));

    mmString_Clear(&p->szIpsFile);
    mmString_Sprintf(&p->szIpsFile, "%s.ips", mmString_CStr(&p->szRomName));

    mmString_Clear(&p->szToneFile);
    mmString_Sprintf(&p->szToneFile, "%s.vtd", mmString_CStr(&p->szRomName));

    mmString_Clear(&p->szGenieFile);
    mmString_Sprintf(&p->szGenieFile, "%s.gen", mmString_CStr(&p->szRomName));

    mmString_Destroy(&path);
}

