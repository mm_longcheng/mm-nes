/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper023.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper023_Init(struct mmEmuMapper023* p)
{
    mmEmuMapper_Init(&p->super);

    p->addrmask = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    //
    p->super.Reset = &mmEmuMapper023_Reset;
    p->super.Write = &mmEmuMapper023_Write;
    //p->super.Read = &mmEmuMapper023_Read;
    //p->super.ReadLow = &mmEmuMapper023_ReadLow;
    //p->super.WriteLow = &mmEmuMapper023_WriteLow;
    //p->super.ExRead = &mmEmuMapper023_ExRead;
    //p->super.ExWrite = &mmEmuMapper023_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper023_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper023_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper023_HSync;
    //p->super.VSync = &mmEmuMapper023_VSync;
    p->super.Clock = &mmEmuMapper023_Clock;
    //p->super.PPULatch = &mmEmuMapper023_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper023_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper023_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper023_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper023_IsStateSave;
    p->super.SaveState = &mmEmuMapper023_SaveState;
    p->super.LoadState = &mmEmuMapper023_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper023_Destroy(struct mmEmuMapper023* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->addrmask = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;
}

MM_EXPORT_EMU void mmEmuMapper023_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper023* p = (struct mmEmuMapper023*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t  crc = 0;

    mmInt_t i = 0;

    p->addrmask = 0xFFFF;

    for (i = 0; i < 8; i++)
    {
        p->reg[i] = i;
    }
    p->reg[8] = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    // p->reg[9] = 1;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    //mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x93794634 ||        // Akumajou Special Boku Dracula Kun(J)
        crc == 0xc7829dae ||        // Akumajou Special Boku Dracula Kun(T-Eng)
        crc == 0xf82dc02f)          // Akumajou Special Boku Dracula Kun(T-Eng v1.02)
    {
        p->addrmask = 0xF00C;
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
    if (crc == 0xdd53c4ae)          // Tiny Toon Adventures(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
    }
}
MM_EXPORT_EMU void mmEmuMapper023_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper023* p = (struct mmEmuMapper023*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );
    switch (addr & p->addrmask)
    {
    case 0x8000:
    case 0x8004:
    case 0x8008:
    case 0x800C:
        if (p->reg[8])
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        }
        break;

    case 0x9000:
        if (data != 0xFF)
        {
            data &= 0x03;
            if (data == 0)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            else if (data == 1)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else if (data == 2)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
            }
        }
        break;

    case 0x9008:
        p->reg[8] = data & 0x02;
        break;

    case 0xA000:
    case 0xA004:
    case 0xA008:
    case 0xA00C:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;

    case 0xB000:
        p->reg[0] = (p->reg[0] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;
    case 0xB001:
    case 0xB004:
        p->reg[0] = (p->reg[0] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;

    case 0xB002:
    case 0xB008:
        p->reg[1] = (p->reg[1] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;

    case 0xB003:
    case 0xB00C:
        p->reg[1] = (p->reg[1] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;

    case 0xC000:
        p->reg[2] = (p->reg[2] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;

    case 0xC001:
    case 0xC004:
        p->reg[2] = (p->reg[2] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;

    case 0xC002:
    case 0xC008:
        p->reg[3] = (p->reg[3] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;

    case 0xC003:
    case 0xC00C:
        p->reg[3] = (p->reg[3] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;

    case 0xD000:
        p->reg[4] = (p->reg[4] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;

    case 0xD001:
    case 0xD004:
        p->reg[4] = (p->reg[4] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;

    case 0xD002:
    case 0xD008:
        p->reg[5] = (p->reg[5] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;

    case 0xD003:
    case 0xD00C:
        p->reg[5] = (p->reg[5] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;

    case 0xE000:
        p->reg[6] = (p->reg[6] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;

    case 0xE001:
    case 0xE004:
        p->reg[6] = (p->reg[6] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;

    case 0xE002:
    case 0xE008:
        p->reg[7] = (p->reg[7] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;

    case 0xE003:
    case 0xE00C:
        p->reg[7] = (p->reg[7] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;

    case 0xF000:
        p->irq_latch = (p->irq_latch & 0xF0) | (data & 0x0F);
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xF004:
        p->irq_latch = (p->irq_latch & 0x0F) | ((data & 0x0F) << 4);
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF008:
        p->irq_enable = data & 0x03;
        p->irq_counter = p->irq_latch;
        p->irq_clock = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF00C:
        p->irq_enable = (p->irq_enable & 0x01) * 3;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper023_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper023* p = (struct mmEmuMapper023*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (p->irq_enable & 0x02)
    {
        p->irq_clock += cycles * 3;
        while (p->irq_clock >= 341)
        {
            p->irq_clock -= 341;
            p->irq_counter++;
            if (p->irq_counter == 0)
            {
                p->irq_counter = p->irq_latch;
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper023_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper023_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper023* p = (struct mmEmuMapper023*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 9);
    buffer[9] = p->irq_enable;
    buffer[10] = p->irq_counter;
    buffer[11] = p->irq_latch;
    *(mmInt_t*)&buffer[12] = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper023_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper023* p = (struct mmEmuMapper023*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 9);
    p->irq_enable = buffer[9];
    p->irq_counter = buffer[10];
    p->irq_latch = buffer[11];
    p->irq_clock = *(mmInt_t*)&buffer[12];
}

static struct mmEmuMapper* __static_mmEmuMapper023_Produce(void)
{
    struct mmEmuMapper023* p = (struct mmEmuMapper023*)mmMalloc(sizeof(struct mmEmuMapper023));
    mmEmuMapper023_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper023_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper023* p = (struct mmEmuMapper023*)(m);
    mmEmuMapper023_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER023 =
{
    &__static_mmEmuMapper023_Produce,
    &__static_mmEmuMapper023_Recycle,
};
