/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper040.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper040_Init(struct mmEmuMapper040* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_line = 0;

    //
    p->super.Reset = &mmEmuMapper040_Reset;
    p->super.Write = &mmEmuMapper040_Write;
    //p->super.Read = &mmEmuMapper040_Read;
    //p->super.ReadLow = &mmEmuMapper040_ReadLow;
    //p->super.WriteLow = &mmEmuMapper040_WriteLow;
    //p->super.ExRead = &mmEmuMapper040_ExRead;
    //p->super.ExWrite = &mmEmuMapper040_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper040_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper040_ExCmdWrite;
    p->super.HSync = &mmEmuMapper040_HSync;
    //p->super.VSync = &mmEmuMapper040_VSync;
    //p->super.Clock = &mmEmuMapper040_Clock;
    //p->super.PPULatch = &mmEmuMapper040_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper040_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper040_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper040_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper040_IsStateSave;
    p->super.SaveState = &mmEmuMapper040_SaveState;
    p->super.LoadState = &mmEmuMapper040_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper040_Destroy(struct mmEmuMapper040* p)
{
    mmEmuMapper_Destroy(&p->super);
    p->irq_enable = 0;
    p->irq_line = 0;
}

MM_EXPORT_EMU void mmEmuMapper040_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper040* p = (struct mmEmuMapper040*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->irq_enable = 0;
    p->irq_line = 0;

    mmEmuMmu_SetPROM8KBank(mmu, 3, 6);
    mmEmuMmu_SetPROM32KBankArray(mmu, 4, 5, 0, 7);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper040_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper040* p = (struct mmEmuMapper040*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE000)
    {
    case 0x8000:
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xA000:
        p->irq_enable = 0xFF;
        p->irq_line = 37;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xC000:
        break;
    case 0xE000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data & 0x07);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper040_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper040* p = (struct mmEmuMapper040*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if (--p->irq_line <= 0)
        {
            //mm_emu_cpu_IRQ(&nes->cpu);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper040_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper040_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper040* p = (struct mmEmuMapper040*)(super);

    buffer[0] = p->irq_enable;
    *(mmInt_t*)&buffer[1] = p->irq_line;
}
MM_EXPORT_EMU void mmEmuMapper040_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper040* p = (struct mmEmuMapper040*)(super);

    p->irq_enable = buffer[0];
    p->irq_line = *(mmInt_t*)&buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper040_Produce(void)
{
    struct mmEmuMapper040* p = (struct mmEmuMapper040*)mmMalloc(sizeof(struct mmEmuMapper040));
    mmEmuMapper040_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper040_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper040* p = (struct mmEmuMapper040*)(m);
    mmEmuMapper040_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER040 =
{
    &__static_mmEmuMapper040_Produce,
    &__static_mmEmuMapper040_Recycle,
};
