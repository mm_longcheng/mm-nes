/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuVSSetting.h"
#include "mmEmuNes.h"

MM_EXPORT_EMU void mmEmuNes_VSSettingApply(struct mmEmuNes* p)
{
    struct mmEmuPad* pad = &p->pad;
    struct mmEmuPpu* ppu = &p->ppu;

    mmUInt32_t crc = mmEmuRom_GetPROMCRC(&p->rom);

    mmBool_t vsmode = MM_FALSE;

    switch (crc)
    {
    case 0xeb2dba63:    // VS TKO Boxing
    case 0x9818f656:
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0xed588f00:    // VS Duck Hunt
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPEZ);
        vsmode = MM_TRUE;
        break;
    case 0x8c0c2df5:    // VS Top Gun
        mmEmuPpu_SetVSSecurity(ppu, 0x1B);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0x16d3f469:    // VS Ninja Jajamaru Kun (J)
        mmEmuPpu_SetVSSecurity(ppu, 0x1B);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE3);
        vsmode = MM_TRUE;
        break;
    case 0x8850924b:    // VS Tetris
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0xcf36261e:    // VS Sky Kid
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE3);
        vsmode = MM_TRUE;
        break;
    case 0xe1aa8214:    // VS Star Luster
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0xec461db9:    // VS Pinball
        mmEmuPpu_SetVSColorMap(ppu, 0);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0xe528f651:    // VS Pinball (alt)
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0x17ae56be:    // VS Freedom Force
        // ppu->SetVSColorMap( 0 );
        mmEmuPpu_SetVSColorMap(ppu, 4);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPEZ);
        vsmode = MM_TRUE;
        break;
    case 0xe2c0a2be:    // VS Platoon
        mmEmuPpu_SetVSColorMap(ppu, 0);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0xff5135a3:    // VS Hogan's Alley
        mmEmuPpu_SetVSColorMap(ppu, 0);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPEZ);
        vsmode = MM_TRUE;
        break;
    case 0x70901b25:    // VS Slalom
        mmEmuPpu_SetVSColorMap(ppu, 1);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0x0b65a917:    // VS Mach Rider(Endurance Course)
    case 0x8a6a9848:
        mmEmuPpu_SetVSColorMap(ppu, 1);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0xae8063ef:    // VS Mach Rider(Japan, Fighting Course)
        mmEmuPpu_SetVSColorMap(ppu, 0);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0xcc2c4b5d:    // VS Golf
        mmEmuPpu_SetVSColorMap(ppu, 1);
        // pad->SetVSType( PAD::VS_TYPE0 );
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE6);
        vsmode = MM_TRUE;
        break;
    case 0xa93a5aee:    // VS Stroke and Match Golf
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0x86167220:    // VS Lady Golf
        mmEmuPpu_SetVSColorMap(ppu, 1);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0xffbef374:    // VS Castlevania
        mmEmuPpu_SetVSColorMap(ppu, 1);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0x135adf7c:    // VS Atari RBI Baseball
        mmEmuPpu_SetVSColorMap(ppu, 2);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0xd5d7eac4:    // VS Dr. Mario
        mmEmuPpu_SetVSColorMap(ppu, 2);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0x46914e3e:    // VS Soccer
        mmEmuPpu_SetVSColorMap(ppu, 2);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0x70433f2c:    // VS Battle City
    case 0x8d15a6e6:    // VS bad .nes
        mmEmuPpu_SetVSColorMap(ppu, 2);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0x1e438d52:    // VS Goonies
        mmEmuPpu_SetVSColorMap(ppu, 2);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0xcbe85490:    // VS Excitebike
        mmEmuPpu_SetVSColorMap(ppu, 2);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0x29155e0c:    // VS Excitebike (alt)
        mmEmuPpu_SetVSColorMap(ppu, 3);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0x07138c06:    // VS Clu Clu Land
        mmEmuPpu_SetVSColorMap(ppu, 3);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0x43a357ef:    // VS Ice Climber
        mmEmuPpu_SetVSColorMap(ppu, 3);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0x737dd1bf:    // VS Super Mario Bros
    case 0x4bf3972d:    // VS Super Mario Bros
    case 0x8b60cc58:    // VS Super Mario Bros
    case 0x8192c804:    // VS Super Mario Bros
        mmEmuPpu_SetVSColorMap(ppu, 3);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    case 0xd99a2087:    // VS Gradius
        mmEmuPpu_SetVSColorMap(ppu, 4);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE1);
        vsmode = MM_TRUE;
        break;
    case 0xf9d3b0a3:    // VS Super Xevious
    case 0x9924980a:    // VS Super Xevious
    case 0x66bb838f:    // VS Super Xevious
        mmEmuPpu_SetVSColorMap(ppu, 4);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;

    case 0xc99ec059:    // VS Raid on Bungeling Bay(J)
        mmEmuPpu_SetVSColorMap(ppu, 1);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE5);
        vsmode = MM_TRUE;
        break;
    case 0xca85e56d:    // VS Mighty Bomb Jack(J)
        mmEmuPpu_SetVSSecurity(ppu, 0x3D);
        mmEmuPad_SetVSType(pad, MM_EMU_PAD_VS_TYPE0);
        vsmode = MM_TRUE;
        break;
    default:
        break;
    }
    //
    mmEmuPpu_SetVSMode(ppu, vsmode);
}

