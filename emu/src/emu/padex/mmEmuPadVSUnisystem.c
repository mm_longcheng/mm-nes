/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadVSUnisystem.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuPadVSUnisystem_Init(struct mmEmuPadVSUnisystem* p)
{
    mmEmuExpad_Init(&p->super);

    //
    p->super.Reset = &mmEmuPadVSUnisystem_Reset;
    //p->super.Strobe = &mmEmuPadVSUnisystem_Strobe;
    p->super.Read4016 = &mmEmuPadVSUnisystem_Read4016;
    p->super.Read4017 = &mmEmuPadVSUnisystem_Read4017;
    //p->super.Write4016 = &mmEmuPadVSUnisystem_Write4016;
    //p->super.Write4017 = &mmEmuPadVSUnisystem_Write4017;
    //p->super.Sync = &mmEmuPadVSUnisystem_Sync;
    //p->super.SetSyncData = &mmEmuPadVSUnisystem_SetSyncData;
    //p->super.GetSyncData = &mmEmuPadVSUnisystem_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadVSUnisystem_Destroy(struct mmEmuPadVSUnisystem* p)
{
    mmEmuExpad_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuPadVSUnisystem_Reset(struct mmEmuExpad* super)
{
    // struct mmEmuPadVSUnisystem* p = (struct mmEmuPadVSUnisystem*)(super);
}

MM_EXPORT_EMU mmByte_t mmEmuPadVSUnisystem_Read4016(struct mmEmuExpad* super)
{
    struct mmEmuPadVSUnisystem* p = (struct mmEmuPadVSUnisystem*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmWord_t* nVSUnisystem = p->super.nes->config.controller.nVSUnisystem;

    mmByte_t data = 0x00;

    // Coin 1
    if (mmEmuController_ButtonCheckKeys(controller, 0, nVSUnisystem))
    {
        data |= 0x20;
    }
    // Coin 2
    if (mmEmuController_ButtonCheckKeys(controller, 1, nVSUnisystem))
    {
        data |= 0x40;
    }
    // Service
    if (mmEmuController_ButtonCheckKeys(controller, 2, nVSUnisystem))
    {
        data |= 0x04;
    }

    // Dip-Switch
    data |= ((mmEmuNes_GetVSDipSwitch(p->super.nes) << 3) & 0x18);

    if (mmEmuRom_GetPROMCRC(&p->super.nes->rom) == 0xC99EC059) // VS Raid on Bungeling Bay(J)
    {
        data |= 0x80;
    }

    return data;
}
MM_EXPORT_EMU mmByte_t mmEmuPadVSUnisystem_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadVSUnisystem* p = (struct mmEmuPadVSUnisystem*)(super);

    mmByte_t data = 0x00;

    // Dip-Switch   
    data = mmEmuNes_GetVSDipSwitch(p->super.nes) & 0xFC;

    return data;
}

static struct mmEmuExpad* __static_mmEmuPadVSUnisystem_Produce(void)
{
    struct mmEmuPadVSUnisystem* p = (struct mmEmuPadVSUnisystem*)mmMalloc(sizeof(struct mmEmuPadVSUnisystem));
    mmEmuPadVSUnisystem_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadVSUnisystem_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadVSUnisystem* p = (struct mmEmuPadVSUnisystem*)(m);
    mmEmuPadVSUnisystem_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_VSUNISYSTEM =
{
    &__static_mmEmuPadVSUnisystem_Produce,
    &__static_mmEmuPadVSUnisystem_Recycle,
};
