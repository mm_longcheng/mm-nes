/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuNes.h"
#include "mmEmuMapper.h"
#include "mmEmuSnapshot.h"
#include "mmEmuVersion.h"
#include "mmEmuErrorCode.h"
#include "mmEmuVSSetting.h"
#include "mmEmuMapperRegister.h"
#include "mmEmuStateFile.h"
#include "mmEmuSrom.h"
#include "mmEmuDisk.h"
#include "mmEmuTurboFile.h"
#include "mmEmuNesDraw.h"

#include "core/mmLogger.h"
#include "core/mmString.h"
#include "core/mmTime.h"
#include "core/mmTimeCache.h"
#include "core/mmFileSystem.h"

static void __static_mmEmuNes_MovieFinish(struct mmEmuMovie* obj);
//
static void __static_mmEmuNes_ScreenMessage(struct mmEmuScreen* obj, const char* message);

MM_EXPORT_EMU void mmEmuNes_Init(struct mmEmuNes* p)
{
    struct mmEmuMovieCallback movie_callback;
    struct mmEmuScreenCallback screen_callback;

    mmEmuRom_Init(&p->rom);
    mmEmuMmu_Init(&p->mmu);
    mmEmuCpu_Init(&p->cpu);
    mmEmuPpu_Init(&p->ppu);
    mmEmuApu_Init(&p->apu);
    mmEmuPad_Init(&p->pad);

    mmEmuNesConfig_Init(&p->config);

    mmEmuAssets_Init(&p->assets);
    mmEmuController_Init(&p->controller);
    mmEmuScreen_Init(&p->screen);
    mmEmuWaveRecord_Init(&p->wave_record);

    mmEmuMapperFactory_Init(&p->mapper_factory);
    p->mapper = NULL;
    p->mapper_id = 0;

    p->nIRQtype = MM_EMU_NES_IRQ_HSYNC;
    p->nVideoMode = MM_EMU_NES_MODEL_NTSC;
    p->bFrameIRQ = MM_FALSE;

    p->NES_scanline = 0;
    p->bZapper = MM_FALSE;
    p->ZapperX = 0;
    p->ZapperY = 0;

    mmEmuMovie_Init(&p->movie);

    p->m_CommandRequest = 0;

    p->m_bDiskThrottle = MM_FALSE;

    p->RenderMethod = MM_EMU_NES_PRE_RENDER;

    p->base_cycles = 0;
    p->emul_cycles = 0;

    p->SAVERAM_SIZE = 8 * 1024; // 8K byte

    p->m_VSDipValue = 0;
    p->m_VSDipTable = MM_EMU_VSDIP_DEFAULT;

    mmEmuCheat_Init(&p->cheat);

    mmEmuNsf_Init(&p->nsf);

    mmEmuTape_Init(&p->tape);

    mmEmuBarcode_Init(&p->barcode);

    p->m_TurboFileBank = 0;

    p->m_saveRenderMethod = 0;
    p->m_saveIrqType = 0;
    p->m_saveFrameIRQ = 0;
    p->m_saveVideoMode = 0;

    mmEmuNesProfiler_Init(&p->m_Profiler);

    mmEmuNesCallback_Init(&p->callback);
    // 
    p->config.nescfg = MM_EMU_VIDEO_FORMAT_NTSC;

    mmEmuRom_SetParent(&p->rom, p);
    mmEmuCpu_SetParent(&p->cpu, p);
    mmEmuPpu_SetParent(&p->ppu, p);
    mmEmuApu_SetParent(&p->apu, p);
    mmEmuPad_SetParent(&p->pad, p);

    mmEmuAssets_SetParent(&p->assets, p);
    mmEmuController_SetParent(&p->controller, p);
    mmEmuScreen_SetParent(&p->screen, p);
    mmEmuMovie_SetParent(&p->movie, p);
    mmEmuCheat_SetParent(&p->cheat, p);
    mmEmuNsf_SetParent(&p->nsf, p);
    mmEmuTape_SetParent(&p->tape, p);
    mmEmuBarcode_SetParent(&p->barcode, p);

    mmEmuPpu_SetScreenPtr(&p->ppu, mmEmuScreen_GetRenderScreen(&p->screen));
    mmEmuPpu_SetColormodePtr(&p->ppu, mmEmuScreen_GetLineColormode(&p->screen));

    mmEmuScreen_CalcPaletteTable(&p->screen);

    // Cheat
    mmEmuNes_CheatInitial(p);

    // callback
    movie_callback.MovieFinish = &__static_mmEmuNes_MovieFinish;
    movie_callback.obj = p;
    mmEmuMovie_SetCallback(&p->movie, &movie_callback);

    screen_callback.ScreenMessage = &__static_mmEmuNes_ScreenMessage;
    screen_callback.obj = p;
    mmEmuScreen_SetCallback(&p->screen, &screen_callback);
}
MM_EXPORT_EMU void mmEmuNes_Destroy(struct mmEmuNes* p)
{
    mmEmuNes_MovieStop(p);

    mmEmuNes_SaveSRAM(p);
    mmEmuNes_SaveDISK(p);
    mmEmuNes_SaveTurboFile(p);

    mmEmuMapperFactory_Recycle(&p->mapper_factory, p->mapper_id, p->mapper);
    p->mapper = NULL;

    mmEmuRom_Destroy(&p->rom);
    mmEmuMmu_Destroy(&p->mmu);
    mmEmuCpu_Destroy(&p->cpu);
    mmEmuPpu_Destroy(&p->ppu);
    mmEmuApu_Destroy(&p->apu);
    mmEmuPad_Destroy(&p->pad);

    mmEmuNesConfig_Destroy(&p->config);

    mmEmuAssets_Destroy(&p->assets);
    mmEmuController_Destroy(&p->controller);
    mmEmuScreen_Destroy(&p->screen);
    mmEmuWaveRecord_Destroy(&p->wave_record);

    mmEmuMapperFactory_Destroy(&p->mapper_factory);
    p->mapper = NULL;
    p->mapper_id = 0;

    p->nIRQtype = MM_EMU_NES_IRQ_HSYNC;
    p->nVideoMode = MM_EMU_NES_MODEL_NTSC;
    p->bFrameIRQ = MM_FALSE;

    p->NES_scanline = 0;
    p->bZapper = MM_FALSE;
    p->ZapperX = 0;
    p->ZapperY = 0;

    mmEmuMovie_Destroy(&p->movie);

    p->m_CommandRequest = 0;

    p->m_bDiskThrottle = MM_FALSE;

    p->RenderMethod = MM_EMU_NES_PRE_RENDER;

    p->base_cycles = 0;
    p->emul_cycles = 0;

    p->SAVERAM_SIZE = 8 * 1024; // 8K byte

    p->m_VSDipValue = 0;
    p->m_VSDipTable = MM_EMU_VSDIP_DEFAULT;

    mmEmuCheat_Destroy(&p->cheat);

    mmEmuNsf_Destroy(&p->nsf);

    mmEmuTape_Destroy(&p->tape);

    mmEmuBarcode_Destroy(&p->barcode);

    p->m_TurboFileBank = 0;

    p->m_saveRenderMethod = 0;
    p->m_saveIrqType = 0;
    p->m_saveFrameIRQ = 0;
    p->m_saveVideoMode = 0;

    mmEmuNesProfiler_Destroy(&p->m_Profiler);
    mmEmuNesCallback_Destroy(&p->callback);
}

MM_EXPORT_EMU void mmEmuNes_SetRomPath(struct mmEmuNes* p, const char* szRomPath)
{
    mmEmuAssets_SetRomPath(&p->assets, szRomPath);
}
MM_EXPORT_EMU void mmEmuNes_SetRomName(struct mmEmuNes* p, const char* szRomName)
{
    mmEmuAssets_SetRomName(&p->assets, szRomName);
}
MM_EXPORT_EMU void mmEmuNes_SetWritablePath(struct mmEmuNes* p, const char* szWritablePath)
{
    mmEmuAssets_SetWritablePath(&p->assets, szWritablePath);
}
MM_EXPORT_EMU void mmEmuNes_SetResourceRoot(struct mmEmuNes* p, mmUInt32_t type_assets, const char* root_path, const char* root_base)
{
    mmEmuAssets_SetResourceRoot(&p->assets, type_assets, root_path, root_base);
}
MM_EXPORT_EMU void mmEmuNes_SetPaletteTableByte(struct mmEmuNes* p, const mmUInt32_t pal[64])
{
    mmEmuScreen_SetPaletteTableByte(&p->screen, pal);
}
MM_EXPORT_EMU void mmEmuNes_SetCallback(struct mmEmuNes* p, struct mmEmuNesCallback* callback)
{
    assert(NULL != callback && "NULL != callback is invalid.");
    p->callback = *callback;
}

MM_EXPORT_EMU int mmEmuNes_LoadRom(struct mmEmuNes* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;
    int err = MM_UNKNOWN;

    do
    {
        mmEmuAssets_MakeDirectory(&p->assets);
        mmLogger_LogI(gLogger, "%s %d emu asset MakeDirectory complete.", __FUNCTION__, __LINE__);

        mmLogger_LogI(gLogger, "%s %d Loading ROM Image....", __FUNCTION__, __LINE__);

        err = mmEmuRom_LoadBuffer(&p->rom);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d Loading ROM Image failure.", __FUNCTION__, __LINE__);
            code = err;
            break;
        }
        mmLogger_LogD(gLogger, "%s %d Loading ROM Image success.", __FUNCTION__, __LINE__);

        mmEmuMapperFactory_Recycle(&p->mapper_factory, p->mapper_id, p->mapper);
        p->mapper = NULL;

        p->mapper_id = mmEmuRom_GetMapperNo(&p->rom);

        p->mapper = mmEmuMapperFactory_Produce(&p->mapper_factory, p->mapper_id);
        if (NULL == p->mapper)
        {
            mmLogger_LogE(gLogger, "%s %d not supported mapper: %u .", __FUNCTION__, __LINE__, p->mapper_id);
            code = MM_ERR_EMU_NOT_SUPPORTED_MAPPER;
            break;
        }
        mmEmuMapper_SetParent(p->mapper, p);
        mmLogger_LogD(gLogger, "%s %d create ROM mapper: %u success.", __FUNCTION__, __LINE__, p->mapper_id);

        // try load SRAM, if SRAM file exist, not need check error code.
        err = mmEmuNes_LoadSRAM(p);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d Loading ROM SRAM failure.", __FUNCTION__, __LINE__);
            code = err;
            break;
        }
        mmLogger_LogD(gLogger, "%s %d Loading ROM SRAM success.", __FUNCTION__, __LINE__);

        // try load DISK, if DISK file exist, not need check error code.
        err = mmEmuNes_LoadDISK(p);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d Loading ROM DISK failure.", __FUNCTION__, __LINE__);
            code = err;
            break;
        }
        mmLogger_LogD(gLogger, "%s %d Loading ROM DISK success.", __FUNCTION__, __LINE__);

        {
            // Since initialization timing is slow in the Pad class, here.
            mmUInt32_t crc = mmEmuRom_GetPROMCRC(&p->rom);
            if (crc == 0xe792de94 ||        // Best Play - Pro Yakyuu (New) (J)
                crc == 0xf79d684a ||        // Best Play - Pro Yakyuu (Old) (J)
                crc == 0xc2ef3422 ||        // Best Play - Pro Yakyuu 2 (J)
                crc == 0x974e8840 ||        // Best Play - Pro Yakyuu '90 (J)
                crc == 0xb8747abf ||        // Best Play - Pro Yakyuu Special (J)
                crc == 0x9fa1c11f ||        // Castle Excellent (J)
                crc == 0x0b0d4d1b ||        // Derby Stallion - Zenkoku Ban (J)
                crc == 0x728c3d98 ||        // Downtown - Nekketsu Monogatari (J)
                crc == 0xd68a6f33 ||        // Dungeon Kid (J)
                crc == 0x3a51eb04 ||        // Fleet Commander (J)
                crc == 0x7c46998b ||        // Haja no Fuuin (J)
                crc == 0x7e5d2f1a ||        // Itadaki Street - Watashi no Mise ni Yottette (J)
                crc == 0xcee5857b ||        // Ninjara Hoi! (J)
                crc == 0x50ec5e8b ||        // Wizardry - Legacy of Llylgamyn (J)
                crc == 0x343e9146 ||        // Wizardry - Proving Grounds of the Mad Overlord (J)
                crc == 0x33d07e45)          // Wizardry - The Knight of Diamonds (J)
            {
                mmEmuPad_SetExController(&p->pad, MM_EMU_PAD_EXCONTROLLER_TURBOFILE);
            }
        }

        err = mmEmuNes_LoadTurboFile(p);
        if (MM_SUCCESS != err)
        {
            mmLogger_LogE(gLogger, "%s %d Loading ROM TurboFile failure.", __FUNCTION__, __LINE__);
            code = err;
            break;
        }
        mmLogger_LogD(gLogger, "%s %d Loading ROM TurboFile success.", __FUNCTION__, __LINE__);

        // VS-Unisystem default setting.
        if (mmEmuRom_IsVSUNISYSTEM(&p->rom))
        {
            mmUInt32_t crc = mmEmuRom_GetPROMCRC(&p->rom);

            p->m_VSDipValue = mmEmuGetVSDefaultDipSwitchValue(crc);
            p->m_VSDipTable = mmEmuFindVSDipSwitchTable(crc);

            mmEmuNes_VSSettingApply(p);
        }
        else
        {
            p->m_VSDipValue = 0;
            p->m_VSDipTable = MM_EMU_VSDIP_DEFAULT;
        }

        mmEmuNes_HardwareReset(p);

        // logger information.
        mmEmuNes_LoggerInformation(p, gLogger);

        mmLogger_LogI(gLogger, "%s %d emulation complete.", __FUNCTION__, __LINE__);

        code = MM_SUCCESS;
    } while (0);
    return code;
}
MM_EXPORT_EMU void mmEmuNes_LoggerInformation(struct mmEmuNes* p, struct mmLogger* logger)
{
    mmLogger_LogI(logger, "NES ROM Status:");
    mmLogger_LogI(logger, "NES Status -----------------------------------");
    mmLogger_LogI(logger, "NES Status         Rom File: %s", mmString_CStr(&p->assets.szRomFile));
    mmLogger_LogI(logger, "NES Status           PRGCRC: 0x%08" PRIx32, mmEmuRom_GetPROMCRC(&p->rom));
    mmLogger_LogI(logger, "NES Status           ROMCRC: 0x%08" PRIx32, mmEmuRom_GetAROMCRC(&p->rom));
    mmLogger_LogI(logger, "NES Status           CHRCRC: 0x%08" PRIx32, mmEmuRom_GetVROMCRC(&p->rom));
    mmLogger_LogI(logger, "NES Status           Mapper: %03d", mmEmuRom_GetMapperNo(&p->rom));
    mmLogger_LogI(logger, "NES Status         PRG SIZE: (%4dK)", 0x10 * (mmInt_t)mmEmuRom_GetPROMSIZE(&p->rom));
    mmLogger_LogI(logger, "NES Status         CHR SIZE: (%4dK)", 0x08 * (mmInt_t)mmEmuRom_GetVROMSIZE(&p->rom));
    mmLogger_LogI(logger, "NES Status         V MIRROR: %s", mmEmuRom_IsVMIRROR(&p->rom) ? "Yes" : "No");
    mmLogger_LogI(logger, "NES Status         4 SCREEN: %s", mmEmuRom_Is4SCREEN(&p->rom) ? "Yes" : "No");
    mmLogger_LogI(logger, "NES Status          Battery: %s", mmEmuRom_IsBATTERY(&p->rom) ? "Yes" : "No");
    mmLogger_LogI(logger, "NES Status          Trainer: %s", mmEmuRom_IsTRAINER(&p->rom) ? "Yes" : "No");
    mmLogger_LogI(logger, "NES Status     VS-Unisystem: %s", mmEmuRom_IsVSUNISYSTEM(&p->rom) ? "Yes" : "No");
    mmLogger_LogI(logger, "NES Status        VideoMode: %s", mmEmuVideoMode_String(p->nVideoMode));
    mmLogger_LogI(logger, "NES Status     RenderMethod: %d", p->RenderMethod);
    mmLogger_LogI(logger, "NES Status -----------------------------------");
}

MM_EXPORT_EMU void mmEmuNes_HardwareReset(struct mmEmuNes* p)
{
    mmEmuNes_SaveSRAM(p);
    mmEmuNes_SaveDISK(p);
    mmEmuNes_SaveTurboFile(p);

    // Reset the mmu first.
    // Must ensure that the data is completely reset.
    // Some data inside some applications may not be initialized.
    // Bug fix for load different rom, some applications failed to launch.
    mmEmuMmu_Reset(&p->mmu);

    // RAM Clear
    mmMemset(p->mmu.RAM, 0, sizeof(mmByte_t) * 8 * 1024);

    if (mmEmuRom_GetPROMCRC(&p->rom) == 0x29401686) // Minna no Taabou no Nakayoshi Dai Sakusen(J)
    {
        mmMemset(p->mmu.RAM, 0xFF, sizeof(mmByte_t) * 8 * 1024);
    }

    // RAM set
    if (!mmEmuRom_IsBATTERY(&p->rom) && mmEmuRom_GetMapperNo(&p->rom) != 20)
    {
        mmMemset(p->mmu.WRAM, 0xFF, sizeof(mmByte_t) * 128 * 1024);
    }

    mmMemset(p->mmu.CRAM, 0, sizeof(mmByte_t) * 32 * 1024);
    mmMemset(p->mmu.VRAM, 0, sizeof(mmByte_t) * 4 * 1024);

    mmMemset(p->mmu.SPRAM, 0, sizeof(mmByte_t) * 0x100);
    mmMemset(p->mmu.BGPAL, 0, sizeof(mmByte_t) * 0x10);
    mmMemset(p->mmu.SPPAL, 0, sizeof(mmByte_t) * 0x10);

    mmMemset(p->mmu.CPUREG, 0, sizeof(mmByte_t) * 0x18);
    mmMemset(p->mmu.PPUREG, 0, sizeof(mmByte_t) * 0x04);

    p->m_bDiskThrottle = MM_FALSE;

    mmEmuNes_SetRenderMethod(p, MM_EMU_NES_PRE_RENDER);

    mmEmuNes_SetVideoMode(p, mmEmuRom_GetVideoMode(&p->rom));

    p->mmu.PROM = mmEmuRom_GetPROM(&p->rom);
    p->mmu.VROM = mmEmuRom_GetVROM(&p->rom);

    p->mmu.PROM_08K_SIZE = mmEmuRom_GetPROMSIZE(&p->rom) * 2;
    p->mmu.PROM_16K_SIZE = mmEmuRom_GetPROMSIZE(&p->rom);
    p->mmu.PROM_32K_SIZE = mmEmuRom_GetPROMSIZE(&p->rom) / 2;

    p->mmu.VROM_1K_SIZE = mmEmuRom_GetVROMSIZE(&p->rom) * 8;
    p->mmu.VROM_2K_SIZE = mmEmuRom_GetVROMSIZE(&p->rom) * 4;
    p->mmu.VROM_4K_SIZE = mmEmuRom_GetVROMSIZE(&p->rom) * 2;
    p->mmu.VROM_8K_SIZE = mmEmuRom_GetVROMSIZE(&p->rom);

    // Default bank.
    if (p->mmu.VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(&p->mmu, 0);
    }
    else
    {
        mmEmuMmu_SetCRAM8KBank(&p->mmu, 0);
    }

    // mirror.  
    if (mmEmuRom_Is4SCREEN(&p->rom))
    {
        mmEmuMmu_SetVRAMMirror(&p->mmu, MM_EMU_MMU_VRAM_MIRROR4);
    }
    else if (mmEmuRom_IsVMIRROR(&p->rom))
    {
        mmEmuMmu_SetVRAMMirror(&p->mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(&p->mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }

    mmEmuApu_SelectExSound(&p->apu, 0);

    mmEmuPpu_Reset(&p->ppu);
    (*(p->mapper->Reset))(p->mapper);

    // Trainer. 
    if (mmEmuRom_IsTRAINER(&p->rom))
    {
        mmMemcpy(p->mmu.WRAM + 0x1000, mmEmuRom_GetTRAINER(&p->rom), 512);
    }

    mmEmuPad_Reset(&p->pad);
    mmEmuCpu_HardwareReset(&p->cpu);
    mmEmuApu_Reset(&p->apu);

    mmEmuController_Reset(&p->controller);

    if (mmEmuRom_IsNSF(&p->rom))
    {
        (*(p->mapper->Reset))(p->mapper);
    }

    p->base_cycles = p->emul_cycles = 0;
}
MM_EXPORT_EMU void mmEmuNes_SoftwareReset(struct mmEmuNes* p)
{
    mmEmuPad_Reset(&p->pad);
    mmEmuCpu_SoftwareReset(&p->cpu);
    mmEmuApu_Reset(&p->apu);

    mmEmuController_Reset(&p->controller);

    if (mmEmuRom_IsNSF(&p->rom))
    {
        (*(p->mapper->Reset))(p->mapper);
    }

    p->m_bDiskThrottle = MM_FALSE;

    p->base_cycles = p->emul_cycles = 0;
}

MM_EXPORT_EMU void mmEmuNes_Clock(struct mmEmuNes* p, mmInt_t cycles)
{
    mmEmuNes_Tape(p, cycles);
    mmEmuNes_Barcode(p, cycles);
}
MM_EXPORT_EMU mmByte_t mmEmuNes_Read(struct mmEmuNes* p, mmWord_t addr)
{
    switch (addr >> 13)
    {
    case 0x00:  // $0000-$1FFF
        return p->mmu.RAM[addr & 0x07FF];
    case 0x01:  // $2000-$3FFF
        return mmEmuPpu_Read(&p->ppu, addr & 0xE007);
    case 0x02:  // $4000-$5FFF
        if (addr < 0x4100)
        {
            return mmEmuNes_ReadReg(p, addr);
        }
        else
        {
            return (*(p->mapper->ReadLow))(p->mapper, addr);
        }
        break;
    case 0x03:  // $6000-$7FFF
        return (*(p->mapper->ReadLow))(p->mapper, addr);
    case 0x04:  // $8000-$9FFF
    case 0x05:  // $A000-$BFFF
    case 0x06:  // $C000-$DFFF
    case 0x07:  // $E000-$FFFF
        return p->mmu.CPU_MEM_BANK[addr >> 13][addr & 0x1FFF];
    default:
        break;
    }

    return 0x00;    // Warning Prevention.
}
MM_EXPORT_EMU void mmEmuNes_Write(struct mmEmuNes* p, mmWord_t addr, mmByte_t data)
{
    switch (addr >> 13)
    {
    case 0x00:  // $0000-$1FFF
        p->mmu.RAM[addr & 0x07FF] = data;
        break;
    case 0x01:  // $2000-$3FFF
        if (!mmEmuRom_IsNSF(&p->rom))
        {
            mmEmuPpu_Write(&p->ppu, addr & 0xE007, data);
        }
        break;
    case 0x02:  // $4000-$5FFF
        if (addr < 0x4100)
        {
            mmEmuNes_WriteReg(p, addr, data);
        }
        else
        {
            (*(p->mapper->WriteLow))(p->mapper, addr, data);
        }
        break;
    case 0x03:  // $6000-$7FFF
        (*(p->mapper->WriteLow))(p->mapper, addr, data);
        break;
    case 0x04:  // $8000-$9FFF
    case 0x05:  // $A000-$BFFF
    case 0x06:  // $C000-$DFFF
    case 0x07:  // $E000-$FFFF
        (*(p->mapper->Write))(p->mapper, addr, data);

        mmEmuNes_GenieCodeProcess(p);
    default:
        break;
        break;
    }
}

MM_EXPORT_EMU void mmEmuNes_EmulationCPU(struct mmEmuNes* p, mmInt_t basecycles)
{
    mmInt_t cycles;

    p->base_cycles += basecycles;

    // we not ASM version here, we need compatibility.
    cycles = (mmInt_t)((p->base_cycles / 12) - p->emul_cycles);

    if (cycles > 0)
    {
        p->emul_cycles += mmEmuCpu_EXEC(&p->cpu, cycles);
    }
}
MM_EXPORT_EMU void mmEmuNes_EmulationCPUBeforeNMI(struct mmEmuNes* p, mmInt_t cycles)
{
    p->base_cycles += cycles;
    p->emul_cycles += mmEmuCpu_EXEC(&p->cpu, cycles / 12);
}

MM_EXPORT_EMU void mmEmuNes_EmulateFrame(struct mmEmuNes* p, mmBool_t bDraw)
{
    mmBool_t bAllSprite = p->config.graphics.bAllSprite;
    mmBool_t bLeftClip = p->config.graphics.bLeftClip;

    struct mmEmuVideoFormatConfig* nescfg = &p->config.nescfg;

    mmInt_t scanline = 0;

    // Time of NSF player.  
    if (mmEmuRom_IsNSF(&p->rom))
    {
        mmEmuNes_EmulateNSF(p);
        return;
    }

    // Cheat
    mmEmuNes_CheatCodeProcess(p);
    //
    p->NES_scanline = scanline;

    if (p->RenderMethod != MM_EMU_NES_TILE_RENDER)
    {
        p->bZapper = MM_FALSE;
        while (MM_TRUE)
        {
            mmEmuPpu_SetRenderScanline(&p->ppu, scanline);

            if (scanline == 0)
            {
                // Dummy scan line.
                if (p->RenderMethod < MM_EMU_NES_POST_RENDER)
                {
                    mmEmuNes_EmulationCPU(p, nescfg->ScanlineCycles);
                    mmEmuPpu_FrameStart(&p->ppu);
                    mmEmuPpu_ScanlineNext(&p->ppu);
                    (*(p->mapper->HSync))(p->mapper, scanline);
                    mmEmuPpu_ScanlineStart(&p->ppu);
                }
                else
                {
                    mmEmuNes_EmulationCPU(p, nescfg->HDrawCycles);
                    mmEmuPpu_FrameStart(&p->ppu);
                    mmEmuPpu_ScanlineNext(&p->ppu);
                    (*(p->mapper->HSync))(p->mapper, scanline);
                    mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 32);
                    mmEmuPpu_ScanlineStart(&p->ppu);
                    mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10 + nescfg->ScanlineEndCycles);
                }
            }
            else if (scanline < 240)
            {
                // Screen drawing (Scanline 1 to 239).
                if (p->RenderMethod < MM_EMU_NES_POST_RENDER)
                {
                    if (p->RenderMethod == MM_EMU_NES_POST_ALL_RENDER)
                    {
                        mmEmuNes_EmulationCPU(p, nescfg->ScanlineCycles);
                    }
                    if (bDraw)
                    {
                        mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                    }
                    else
                    {
                        if (mmEmuPad_IsZapperMode(&p->pad) && scanline == p->ZapperY)
                        {
                            mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                        }
                        else
                        {
                            if (!mmEmuPpu_IsSprite0(&p->ppu, scanline))
                            {
                                mmEmuPpu_DummyScanline(&p->ppu, scanline);
                            }
                            else
                            {
                                mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                            }
                        }
                    }
                    // At this position the screen is different on the raster system.
                    mmEmuPpu_ScanlineNext(&p->ppu);
                    if (p->RenderMethod == MM_EMU_NES_PRE_ALL_RENDER)
                    {
                        mmEmuNes_EmulationCPU(p, nescfg->ScanlineCycles);
                    }
                    // At this position the screen is different on the raster system.
                    // ppu->ScanlineNext();
                    (*(p->mapper->HSync))(p->mapper, scanline);
                    mmEmuPpu_ScanlineStart(&p->ppu);
                }
                else
                {
                    if (p->RenderMethod == MM_EMU_NES_POST_RENDER)
                    {
                        mmEmuNes_EmulationCPU(p, nescfg->HDrawCycles);
                    }
                    if (bDraw)
                    {
                        mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                    }
                    else
                    {
                        if (mmEmuPad_IsZapperMode(&p->pad) && scanline == p->ZapperY)
                        {
                            mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                        }
                        else
                        {
                            if (!mmEmuPpu_IsSprite0(&p->ppu, scanline))
                            {
                                mmEmuPpu_DummyScanline(&p->ppu, scanline);
                            }
                            else
                            {
                                mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                            }
                        }
                    }
                    if (p->RenderMethod == MM_EMU_NES_PRE_RENDER)
                    {
                        mmEmuNes_EmulationCPU(p, nescfg->HDrawCycles);
                    }
                    mmEmuPpu_ScanlineNext(&p->ppu);
                    (*(p->mapper->HSync))(p->mapper, scanline);
                    mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 32);
                    mmEmuPpu_ScanlineStart(&p->ppu);
                    mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10 + nescfg->ScanlineEndCycles);
                }
            }
            else if (scanline <= nescfg->NormalScanlines)
            {
                // Dummy scan line (Scanline 240 to nescfg->NormalScanlines).
                (*(p->mapper->VSync))(p->mapper);
                if (p->RenderMethod < MM_EMU_NES_POST_RENDER)
                {
                    mmEmuNes_EmulationCPU(p, nescfg->ScanlineCycles);
                    (*(p->mapper->HSync))(p->mapper, scanline);
                }
                else
                {
                    mmEmuNes_EmulationCPU(p, nescfg->HDrawCycles);
                    (*(p->mapper->HSync))(p->mapper, scanline);
                    mmEmuNes_EmulationCPU(p, nescfg->HBlankCycles);
                }
            }
            else if (scanline <= nescfg->TotalScanlines - 1)
            {
                mmEmuPad_VSync(&p->pad);

                // VBLANK period.
                if (scanline == nescfg->TotalScanlines - 1)
                {
                    mmEmuPpu_VBlankEnd(&p->ppu);
                }
                if (p->RenderMethod < MM_EMU_NES_POST_RENDER)
                {
                    if (scanline == nescfg->NormalScanlines + 1)
                    {
                        mmEmuPpu_VBlankStart(&p->ppu);
                        if (p->mmu.PPUREG[0] & MM_EMU_PPU_VBLANK_BIT)
                        {
                            mmEmuCpu_NMI(&p->cpu);
                        }
                    }
                    mmEmuNes_EmulationCPU(p, nescfg->ScanlineCycles);
                    (*(p->mapper->HSync))(p->mapper, scanline);
                }
                else
                {
                    if (scanline == nescfg->NormalScanlines + 1)
                    {
                        mmEmuPpu_VBlankStart(&p->ppu);
                        if (p->mmu.PPUREG[0] & MM_EMU_PPU_VBLANK_BIT)
                        {
                            mmEmuCpu_NMI(&p->cpu);
                        }
                    }
                    mmEmuNes_EmulationCPU(p, nescfg->HDrawCycles);
                    (*(p->mapper->HSync))(p->mapper, scanline);
                    mmEmuNes_EmulationCPU(p, nescfg->HBlankCycles);
                }

                if (scanline == nescfg->TotalScanlines - 1)
                {
                    break;
                }
            }
            if (mmEmuPad_IsZapperMode(&p->pad))
            {
                if (scanline == p->ZapperY)
                {
                    p->bZapper = MM_TRUE;
                }
                else
                {
                    p->bZapper = MM_FALSE;
                }
            }

            scanline++;
            p->NES_scanline = scanline;
        }
    }
    else
    {
        p->bZapper = MM_FALSE;
        while (MM_TRUE)
        {
            mmEmuPpu_SetRenderScanline(&p->ppu, scanline);

            if (scanline == 0)
            {
                // Dummy scan line.
                // H-Draw (4fetches*32)
                mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 128);
                mmEmuPpu_FrameStart(&p->ppu);
                mmEmuPpu_ScanlineNext(&p->ppu);
                mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10);
                (*(p->mapper->HSync))(p->mapper, scanline);
                mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 22);
                mmEmuPpu_ScanlineStart(&p->ppu);
                mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10 + nescfg->ScanlineEndCycles);
            }
            else if (scanline < 240)
            {
                // Screen drawing (Scanline 1 to 239).
                if (bDraw)
                {
                    mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                    mmEmuPpu_ScanlineNext(&p->ppu);
                    mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10);
                    (*(p->mapper->HSync))(p->mapper, scanline);
                    mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 22);
                    mmEmuPpu_ScanlineStart(&p->ppu);
                    mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10 + nescfg->ScanlineEndCycles);
                }
                else
                {
                    if (mmEmuPad_IsZapperMode(&p->pad) && scanline == p->ZapperY)
                    {
                        mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                        mmEmuPpu_ScanlineNext(&p->ppu);
                        mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10);
                        (*(p->mapper->HSync))(p->mapper, scanline);
                        mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 22);
                        mmEmuPpu_ScanlineStart(&p->ppu);
                        mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10 + nescfg->ScanlineEndCycles);
                    }
                    else
                    {
                        if (!mmEmuPpu_IsSprite0(&p->ppu, scanline))
                        {
                            // H-Draw (4fetches*32)
                            mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 128);
                            mmEmuPpu_DummyScanline(&p->ppu, scanline);
                            mmEmuPpu_ScanlineNext(&p->ppu);
                            mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10);
                            (*(p->mapper->HSync))(p->mapper, scanline);
                            mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 22);
                            mmEmuPpu_ScanlineStart(&p->ppu);
                            mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10 + nescfg->ScanlineEndCycles);
                        }
                        else
                        {
                            mmEmuPpu_Scanline(&p->ppu, scanline, bAllSprite, bLeftClip);
                            mmEmuPpu_ScanlineNext(&p->ppu);
                            mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10);
                            (*(p->mapper->HSync))(p->mapper, scanline);
                            mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 22);
                            mmEmuPpu_ScanlineStart(&p->ppu);
                            mmEmuNes_EmulationCPU(p, MM_EMU_NES_FETCH_CYCLES * 10 + nescfg->ScanlineEndCycles);
                        }
                    }
                }
            }
            else if (scanline <= nescfg->NormalScanlines)
            {
                // Dummy scan line (Scanline 240 to nescfg->NormalScanlines).
                (*(p->mapper->VSync))(p->mapper);

                mmEmuNes_EmulationCPU(p, nescfg->HDrawCycles);
                // H-Sync
                (*(p->mapper->HSync))(p->mapper, scanline);

                mmEmuNes_EmulationCPU(p, nescfg->HBlankCycles);
            }
            else if (scanline <= nescfg->TotalScanlines - 1)
            {
                mmEmuPad_VSync(&p->pad);

                // VBLANK period.
                if (scanline == nescfg->TotalScanlines - 1)
                {
                    mmEmuPpu_VBlankEnd(&p->ppu);
                }
                if (scanline == nescfg->NormalScanlines + 1)
                {
                    mmEmuPpu_VBlankStart(&p->ppu);
                    if (p->mmu.PPUREG[0] & MM_EMU_PPU_VBLANK_BIT)
                    {
                        mmEmuCpu_NMI(&p->cpu);
                    }
                }
                mmEmuNes_EmulationCPU(p, nescfg->HDrawCycles);

                // H-Sync
                (*(p->mapper->HSync))(p->mapper, scanline);

                mmEmuNes_EmulationCPU(p, nescfg->HBlankCycles);

                if (scanline == nescfg->TotalScanlines - 1)
                {
                    mmEmuPpu_FrameEnd(&p->ppu);
                    break;
                }
            }
            if (mmEmuPad_IsZapperMode(&p->pad))
            {
                if (scanline == p->ZapperY)
                {
                    p->bZapper = MM_TRUE;
                }
                else
                {
                    p->bZapper = MM_FALSE;
                }
            }

            scanline++;
            p->NES_scanline = scanline;
        }
    }

    // Movie pad
    //if( Config.movie.bPadDisplay && bDraw ) 
    //{
    //  mmEmuNes_DrawPad(p);
    //}
    // Movie pad
    if (bDraw)
    {
        mmEmuNes_DrawPad(p);
    }
#if MM_EMU_NES_PROFILER
    if (p->m_Profiler.m_ProfileEnable)
    {
        mmChar_t szString[256] = { 0 };
        mmSprintf(szString, "Cyc:%10" PRIu64 "", p->m_Profiler.m_dwProfileCycle);
        mmEmuNes_DrawString(p, 9, 240 - 32, (mmByte_t*)szString, 0x1F);
        mmEmuNes_DrawString(p, 8, 240 - 33, (mmByte_t*)szString, 0x30);
        mmSprintf(szString, "Ave:%10" PRIu64 "", p->m_Profiler.m_dwProfileAveCycle);
        mmEmuNes_DrawString(p, 9, 240 - 23, (mmByte_t*)szString, 0x1F);
        mmEmuNes_DrawString(p, 8, 240 - 24, (mmByte_t*)szString, 0x30);
        mmSprintf(szString, "Max:%10" PRIu64 "", p->m_Profiler.m_dwProfileMaxCycle);
        mmEmuNes_DrawString(p, 9, 240 - 14, (mmByte_t*)szString, 0x1F);
        mmEmuNes_DrawString(p, 8, 240 - 15, (mmByte_t*)szString, 0x30);
    }
#endif
}

// For NSF
MM_EXPORT_EMU void mmEmuNes_EmulateNSF(struct mmEmuNes* p)
{
    mmEmuNsf_EmulateNSF(&p->nsf);
}
MM_EXPORT_EMU void mmEmuNes_SetNsfPlay(struct mmEmuNes* p, mmInt_t songno, mmInt_t songmode)
{
    mmEmuNsf_SetNsfPlay(&p->nsf, songno, songmode);
}
MM_EXPORT_EMU void mmEmuNes_SetNsfStop(struct mmEmuNes* p)
{
    mmEmuNsf_SetNsfStop(&p->nsf);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_IsNsfPlaying(struct mmEmuNes* p)
{
    return mmEmuNsf_IsNsfPlaying(&p->nsf);
}

MM_EXPORT_EMU void mmEmuNes_PadSync(struct mmEmuNes* p)
{
    mmEmuPad_Sync(&p->pad);
}
MM_EXPORT_EMU void mmEmuNes_SetPadSyncData(struct mmEmuNes* p, mmUInt32_t data)
{
    mmEmuPad_SetSyncData(&p->pad, data);
}
MM_EXPORT_EMU mmUInt32_t mmEmuNes_GetPadSyncData(struct mmEmuNes* p)
{
    return mmEmuPad_GetSyncData(&p->pad);
}

MM_EXPORT_EMU void mmEmuNes_ApuProcess(struct mmEmuNes* p, mmByte_t* lpBuffer, mmUInt32_t dwSize, mmInt_t nRate)
{
    mmEmuApu_Process(&p->apu, lpBuffer, dwSize, nRate);
}

MM_EXPORT_EMU mmByte_t* mmEmuNes_GetFramebuffer(struct mmEmuNes* p)
{
    return mmEmuScreen_GetFramebuffer(&p->screen);
}
MM_EXPORT_EMU mmByte_t* mmEmuNes_GetScreenPtr(struct mmEmuNes* p)
{
    return mmEmuScreen_GetRenderScreen(&p->screen);
}
MM_EXPORT_EMU void mmEmuNes_GetPaletteData(struct mmEmuNes* p, struct mmEmuRGBQuad* rgb)
{
    mmEmuScreen_GetPaletteData(&p->screen, rgb);
}
MM_EXPORT_EMU void mmEmuNes_GetPaletteR8G8B8A8(struct mmEmuNes* p, mmUInt32_t* palette)
{
    mmEmuScreen_GetPaletteR8G8B8A8(&p->screen, palette);
}
MM_EXPORT_EMU void mmEmuNes_ScreenClear(struct mmEmuNes* p)
{
    mmEmuScreen_ScreenClear(&p->screen);
}
MM_EXPORT_EMU void mmEmuNes_ScreenBitBlockTransfer(struct mmEmuNes* p)
{
    mmEmuScreen_BitBlockTransfer(&p->screen);
}

MM_EXPORT_EMU void mmEmuNes_KeyboardPressed(struct mmEmuNes* p, int hId)
{
    mmEmuController_KeyboardPressed(&p->controller, hId);
}
MM_EXPORT_EMU void mmEmuNes_KeyboardRelease(struct mmEmuNes* p, int hId)
{
    mmEmuController_KeyboardRelease(&p->controller, hId);
}
MM_EXPORT_EMU void mmEmuNes_MouseBegan(struct mmEmuNes* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuController_MouseBegan(&p->controller, x, y, button_mask);
    //
    mmEmuScreen_SetZapperPos(&p->screen, x, y);
    mmEmuNes_SetZapperPos(p, x, y);
}
MM_EXPORT_EMU void mmEmuNes_MouseMoved(struct mmEmuNes* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuController_MouseMoved(&p->controller, x, y, button_mask);
    //
    mmEmuScreen_SetZapperPos(&p->screen, x, y);
    mmEmuNes_SetZapperPos(p, x, y);
}
MM_EXPORT_EMU void mmEmuNes_MouseEnded(struct mmEmuNes* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuController_MouseEnded(&p->controller, x, y, button_mask);
    //
    mmEmuScreen_SetZapperPos(&p->screen, x, y);
    mmEmuNes_SetZapperPos(p, x, y);
}

// For Manual bit controller Joypad.
MM_EXPORT_EMU void mmEmuNes_JoypadBitSetData(struct mmEmuNes* p, int n, mmWord_t data)
{
    mmEmuPad_JoypadBitSetData(&p->pad, n, data);
}
MM_EXPORT_EMU mmWord_t mmEmuNes_JoypadBitGetData(struct mmEmuNes* p, int n)
{
    return mmEmuPad_JoypadBitGetData(&p->pad, n);
}
MM_EXPORT_EMU void mmEmuNes_JoypadBitPressed(struct mmEmuNes* p, int n, int hId)
{
    mmEmuPad_JoypadBitPressed(&p->pad, n, hId);
}
MM_EXPORT_EMU void mmEmuNes_JoypadBitRelease(struct mmEmuNes* p, int n, int hId)
{
    mmEmuPad_JoypadBitRelease(&p->pad, n, hId);
}
// For Manual bit controller Nsf.
MM_EXPORT_EMU void mmEmuNes_NsfBitSetData(struct mmEmuNes* p, mmByte_t data)
{
    mmEmuPad_NsfBitSetData(&p->pad, data);
}
MM_EXPORT_EMU mmByte_t mmEmuNes_NsfBitGetData(struct mmEmuNes* p)
{
    return mmEmuPad_NsfBitGetData(&p->pad);
}
MM_EXPORT_EMU void mmEmuNes_NsfBitPressed(struct mmEmuNes* p, int hId)
{
    mmEmuPad_NsfBitPressed(&p->pad, hId);
}
MM_EXPORT_EMU void mmEmuNes_NsfBitRelease(struct mmEmuNes* p, int hId)
{
    mmEmuPad_NsfBitRelease(&p->pad, hId);
}

MM_EXPORT_EMU void mmEmuNes_SetMessageString(struct mmEmuNes* p, const char* message)
{
    mmEmuScreen_SetMessageString(&p->screen, message);
}

MM_EXPORT_EMU void mmEmuNes_WaveRecordStart(struct mmEmuNes* p, const char* szFile)
{
    struct mmEmuAudioConfig* audio_config = &p->config.audio;

    mmEmuWaveRecord_Start(&p->wave_record, szFile, audio_config->nRate, audio_config->nBits, MM_FALSE);
}
MM_EXPORT_EMU void mmEmuNes_WaveRecordStop(struct mmEmuNes* p)
{
    mmEmuWaveRecord_Stop(&p->wave_record);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_WaveRecordIsWaveRecord(struct mmEmuNes* p)
{
    return mmEmuWaveRecord_IsWaveRecord(&p->wave_record);
}
MM_EXPORT_EMU void mmEmuNes_WaveRecordOutput(struct mmEmuNes* p, void* lpBuf, mmUInt32_t dwSize)
{
    mmEmuWaveRecord_Output(&p->wave_record, lpBuf, dwSize);
}

MM_EXPORT_EMU mmBool_t mmEmuNes_LoadEsf(struct mmEmuNes* p, const char* szFileName, mmInt_t no)
{
    return MM_FALSE;
}
MM_EXPORT_EMU mmBool_t mmEmuNes_EsfPlay(struct mmEmuNes* p, mmInt_t no)
{
    return MM_FALSE;
}
MM_EXPORT_EMU mmBool_t mmEmuNes_EsfPlayLoop(struct mmEmuNes* p, mmInt_t no)
{
    return MM_FALSE;
}
MM_EXPORT_EMU mmBool_t mmEmuNes_EsfStop(struct mmEmuNes* p, mmInt_t no)
{
    return MM_FALSE;
}
MM_EXPORT_EMU void mmEmuNes_EsfAllStop(struct mmEmuNes* p)
{

}

MM_EXPORT_EMU void mmEmuNes_SetVideoMode(struct mmEmuNes* p, mmInt_t nMode)
{
    if (MM_EMU_NES_MODEL_NTSC <= nMode && nMode <= MM_EMU_NES_MODEL_DENDY)
    {
        p->nVideoMode = nMode;
        mmEmuNesConfig_SetVideoMode(&p->config, p->nVideoMode);
        //
        mmEmuApu_SoundSetup(&p->apu);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogW(gLogger, "%s %d Not supported VideoMode: %d.", __FUNCTION__, __LINE__, nMode);
    }
}
MM_EXPORT_EMU mmInt_t mmEmuNes_GetDiskNo(struct mmEmuNes* p)
{
    return mmEmuRom_GetDiskNo(&p->rom);
}
MM_EXPORT_EMU void mmEmuNes_SoundSetup(struct mmEmuNes* p)
{
    mmEmuApu_SoundSetup(&p->apu);
}

MM_EXPORT_EMU mmInt_t mmEmuNes_IsStateFile(struct mmEmuNes* p, const char* fname)
{
    return mmEmuStateFile_IsStateFile(p, fname);
}
MM_EXPORT_EMU mmUInt32_t mmEmuNes_LoadState(struct mmEmuNes* p, const char* fname)
{
    return mmEmuStateFile_LoadState(p, fname);
}
MM_EXPORT_EMU mmUInt32_t mmEmuNes_SaveState(struct mmEmuNes* p, const char* fname)
{
    return mmEmuStateFile_SaveState(p, fname);
}
MM_EXPORT_EMU mmUInt32_t mmEmuNes_Snapshot(struct mmEmuNes* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEmuAssets* assets = &p->assets;
    // struct mmEmuPathConfig* path_config = &p->config.path;

    mmUInt32_t code = MM_UNKNOWN;

    struct mmString szSnapshotFile;

    struct timeval tv;

    mmString_Init(&szSnapshotFile);

    mmGettimeofday(&tv, NULL);

    mmEmuAssets_GetSnapshotFile(assets, &szSnapshotFile, p->config.emulator.bPNGsnapshot, &tv);

    mmLogger_LogD(gLogger, "%s %d Snapshot: %s", __FUNCTION__, __LINE__, mmString_CStr(&szSnapshotFile));

    {
        mmByte_t* lpScn = NULL;
        struct mmEmuSnapshot emu_snapshot;
        struct mmEmuRGBQuad rgb[256];

        mmEmuSnapshot_Init(&emu_snapshot);

        lpScn = mmEmuPpu_GetScreenPtr(&p->ppu);
        mmEmuScreen_GetPaletteData(&p->screen, rgb);

        mmEmuSnapshot_SetFilename(&emu_snapshot, mmString_CStr(&szSnapshotFile));
        mmEmuSnapshot_SetWidth(&emu_snapshot, 256);
        mmEmuSnapshot_SetHeight(&emu_snapshot, 240);
        mmEmuSnapshot_SetRGB(&emu_snapshot, rgb);
        mmEmuSnapshot_SetBitmap(&emu_snapshot, lpScn + 8);
        mmEmuSnapshot_SetScan(&emu_snapshot, 272);

        if (!p->config.emulator.bPNGsnapshot)
        {
            code = mmEmuSnapshot_WriteBmp(&emu_snapshot);
        }
        else
        {
            code = mmEmuSnapshot_WritePng(&emu_snapshot);
        }

        mmEmuSnapshot_destroy(&emu_snapshot);
    }

    mmString_Destroy(&szSnapshotFile);

    return code;
}
MM_EXPORT_EMU mmInt_t mmEmuNes_IsMovieFile(const struct mmEmuNes* p, const char* fname)
{
    return mmEmuMovie_IsMovieFile(&p->movie, fname);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_IsMoviePlay(const struct mmEmuNes* p)
{
    return mmEmuMovie_IsMoviePlay(&p->movie);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_IsMovieRec(const struct mmEmuNes* p)
{
    return mmEmuMovie_IsMovieRec(&p->movie);
}
MM_EXPORT_EMU int mmEmuNes_MoviePlay(struct mmEmuNes* p, const char* fname)
{
    return mmEmuMovie_MoviePlay(&p->movie, fname);
}
MM_EXPORT_EMU int mmEmuNes_MovieRec(struct mmEmuNes* p, const char* fname)
{
    return mmEmuMovie_MovieRec(&p->movie, fname);
}
MM_EXPORT_EMU int mmEmuNes_MovieRecAppend(struct mmEmuNes* p, const char* fname)
{
    return mmEmuMovie_MovieRecAppend(&p->movie, fname);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_MovieStop(struct mmEmuNes* p)
{
    return mmEmuMovie_MovieStop(&p->movie);
}
// Call every frame.
MM_EXPORT_EMU void mmEmuNes_Movie(struct mmEmuNes* p)
{
    mmEmuMovie_Movie(&p->movie);
}
MM_EXPORT_EMU void mmEmuNes_GetMovieInfo(const struct mmEmuNes* p, struct mmEmuMovieInfo* info)
{
    mmEmuMovie_GetMovieInfo(&p->movie, info);
}
MM_EXPORT_EMU void mmEmuNes_Command(struct mmEmuNes* p, mmInt_t cmd)
{
    mmEmuNes_CommandParam(p, cmd, 0);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_CommandParam(struct mmEmuNes* p, mmInt_t cmd, mmInt_t param)
{
    switch (cmd)
    {
    case MM_EMU_NES_NESCMD_NONE:
        break;
    case MM_EMU_NES_NESCMD_DISK_THROTTLE_ON:
        if (p->config.emulator.bDiskThrottle)
        {
            p->m_bDiskThrottle = MM_TRUE;
        }
        break;
    case MM_EMU_NES_NESCMD_DISK_THROTTLE_OFF:
        p->m_bDiskThrottle = MM_FALSE;
        break;
    case MM_EMU_NES_NESCMD_DISK_EJECT:
        (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKEJECT, 0);
        p->m_CommandRequest = (mmInt_t)cmd;
        break;
    case MM_EMU_NES_NESCMD_DISK_0A:
        if (mmEmuRom_GetDiskNo(&p->rom) > 0)
        {
            (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 0);
            p->m_CommandRequest = (mmInt_t)cmd;
        }
        break;
    case MM_EMU_NES_NESCMD_DISK_0B:
        if (mmEmuRom_GetDiskNo(&p->rom) > 1)
        {
            (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 1);
            p->m_CommandRequest = (mmInt_t)cmd;
        }
        break;
    case MM_EMU_NES_NESCMD_DISK_1A:
        if (mmEmuRom_GetDiskNo(&p->rom) > 2)
        {
            (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 2);
            p->m_CommandRequest = (mmInt_t)cmd;
        }
        break;
    case MM_EMU_NES_NESCMD_DISK_1B:
        if (mmEmuRom_GetDiskNo(&p->rom) > 3)
        {
            (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 3);
            p->m_CommandRequest = (mmInt_t)cmd;
        }
        break;
    case MM_EMU_NES_NESCMD_DISK_2A:
        if (mmEmuRom_GetDiskNo(&p->rom) > 4)
        {
            (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 4);
            p->m_CommandRequest = (mmInt_t)cmd;
        }
        break;
    case MM_EMU_NES_NESCMD_DISK_2B:
        if (mmEmuRom_GetDiskNo(&p->rom) > 5)
        {
            (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 5);
            p->m_CommandRequest = (mmInt_t)cmd;
        }
        break;
    case MM_EMU_NES_NESCMD_DISK_3A:
        if (mmEmuRom_GetDiskNo(&p->rom) > 6)
        {
            (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 6);
            p->m_CommandRequest = (mmInt_t)cmd;
        }
        break;
    case MM_EMU_NES_NESCMD_DISK_3B:
        if (mmEmuRom_GetDiskNo(&p->rom) > 7)
        {
            (*(p->mapper->ExCmdWrite))(p->mapper, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 7);
            p->m_CommandRequest = (mmInt_t)cmd;
        }
        break;

    case MM_EMU_NES_NESCMD_HWRESET:
        mmEmuNes_HardwareReset(p);
        p->m_CommandRequest = (mmInt_t)cmd;
        break;
    case MM_EMU_NES_NESCMD_SWRESET:
        mmEmuNes_SoftwareReset(p);
        p->m_CommandRequest = (mmInt_t)cmd;
        break;

    case MM_EMU_NES_NESCMD_EXCONTROLLER:
        mmEmuPad_SetExController(&p->pad, param & 0xFF);
        p->m_CommandRequest = 0x0100 | (param & 0xFF);
        break;

    case MM_EMU_NES_NESCMD_SOUND_MUTE:
        // The return value is the muted state after change.
        return mmEmuApu_SetChannelMute(&p->apu, (mmBool_t)param);
    default:
        break;
    }

    return MM_TRUE;
}

MM_EXPORT_EMU void mmEmuNes_CheatInitial(struct mmEmuNes* p)
{
    mmEmuCheat_CheatInitial(&p->cheat);
}

MM_EXPORT_EMU mmBool_t mmEmuNes_IsCheatCodeAdd(struct mmEmuNes* p)
{
    return mmEmuCheat_IsCheatCodeAdd(&p->cheat);
}

MM_EXPORT_EMU mmInt_t mmEmuNes_GetCheatCodeNum(const struct mmEmuNes* p)
{
    return mmEmuCheat_GetCheatCodeNum(&p->cheat);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_GetCheatCode(const struct mmEmuNes* p, mmInt_t no, struct mmEmuCheatcode* code)
{
    return mmEmuCheat_GetCheatCode(&p->cheat, no, code);
}
MM_EXPORT_EMU void mmEmuNes_SetCheatCodeFlag(struct mmEmuNes* p, mmInt_t no, mmBool_t bEnable)
{
    mmEmuCheat_SetCheatCodeFlag(&p->cheat, no, bEnable);
}
MM_EXPORT_EMU void mmEmuNes_SetCheatCodeAllFlag(struct mmEmuNes* p, mmBool_t bEnable, mmBool_t bKey)
{
    mmEmuCheat_SetCheatCodeAllFlag(&p->cheat, bEnable, bKey);
}

MM_EXPORT_EMU void mmEmuNes_ReplaceCheatCode(struct mmEmuNes* p, mmInt_t no, struct mmEmuCheatcode* code)
{
    mmEmuCheat_ReplaceCheatCode(&p->cheat, no, code);
}
MM_EXPORT_EMU void mmEmuNes_AddCheatCode(struct mmEmuNes* p, struct mmEmuCheatcode* code)
{
    mmEmuCheat_AddCheatCode(&p->cheat, code);
}
MM_EXPORT_EMU void mmEmuNes_DelCheatCode(struct mmEmuNes* p, mmInt_t no)
{
    mmEmuCheat_DelCheatCode(&p->cheat, no);
}

MM_EXPORT_EMU mmUInt32_t mmEmuNes_CheatRead(struct mmEmuNes* p, mmInt_t length, mmWord_t addr)
{
    return mmEmuCheat_CheatRead(&p->cheat, length, addr);
}
MM_EXPORT_EMU void mmEmuNes_CheatWrite(struct mmEmuNes* p, mmInt_t length, mmWord_t addr, mmUInt32_t data)
{
    mmEmuCheat_CheatWrite(&p->cheat, length, addr, data);
}
MM_EXPORT_EMU void mmEmuNes_CheatCodeProcess(struct mmEmuNes* p)
{
    mmEmuCheat_CheatCodeProcess(&p->cheat);
}

MM_EXPORT_EMU void mmEmuNes_GenieInitial(struct mmEmuNes* p)
{
    mmEmuCheat_GenieInitial(&p->cheat);
}
MM_EXPORT_EMU void mmEmuNes_GenieLoad(struct mmEmuNes* p, const char* fname)
{
    mmEmuCheat_GenieLoad(&p->cheat, fname);
}
MM_EXPORT_EMU void mmEmuNes_GenieCodeProcess(struct mmEmuNes* p)
{
    mmEmuCheat_GenieCodeProcess(&p->cheat);
}

MM_EXPORT_EMU mmBool_t mmEmuNes_IsTapePlay(const struct mmEmuNes* p)
{
    return mmEmuTape_IsTapePlay(&p->tape);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_IsTapeRec(const struct mmEmuNes* p)
{
    return mmEmuTape_IsTapeRec(&p->tape);
}
MM_EXPORT_EMU int mmEmuNes_TapePlay(struct mmEmuNes* p, const char* fname)
{
    return mmEmuTape_TapePlay(&p->tape, fname);
}
MM_EXPORT_EMU int mmEmuNes_TapeRec(struct mmEmuNes* p, const char* fname)
{
    return mmEmuTape_TapeRec(&p->tape, fname);
}
MM_EXPORT_EMU void mmEmuNes_TapeStop(struct mmEmuNes* p)
{
    mmEmuTape_TapeStop(&p->tape);
}
MM_EXPORT_EMU void mmEmuNes_Tape(struct mmEmuNes* p, mmInt_t cycles)
{
    mmEmuTape_Tape(&p->tape, cycles);
}

MM_EXPORT_EMU void mmEmuNes_SetBarcodeData(struct mmEmuNes* p, mmByte_t* code, mmInt_t len)
{
    mmEmuBarcode_SetBarcodeData(&p->barcode, code, len);
}
MM_EXPORT_EMU void mmEmuNes_Barcode(struct mmEmuNes* p, mmInt_t cycles)
{
    mmEmuBarcode_Barcode(&p->barcode, cycles);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_IsBarcodeEnable(const struct mmEmuNes* p)
{
    return mmEmuBarcode_IsBarcodeEnable(&p->barcode);
}
MM_EXPORT_EMU mmByte_t mmEmuNes_GetBarcodeStatus(const struct mmEmuNes* p)
{
    return mmEmuBarcode_GetBarcodeStatus(&p->barcode);
}

MM_EXPORT_EMU void mmEmuNes_SetBarcode2Data(struct mmEmuNes* p, mmByte_t* code, mmInt_t len)
{
    mmEmuBarcode_SetBarcode2Data(&p->barcode, code, len);
}
MM_EXPORT_EMU mmByte_t mmEmuNes_Barcode2(struct mmEmuNes* p)
{
    return mmEmuBarcode_Barcode2(&p->barcode);
}
MM_EXPORT_EMU mmBool_t mmEmuNes_IsBarcode2Enable(const struct mmEmuNes* p)
{
    return mmEmuBarcode_IsBarcode2Enable(&p->barcode);
}

MM_EXPORT_EMU mmByte_t mmEmuNes_ReadReg(struct mmEmuNes* p, mmWord_t addr)
{
    switch (addr & 0xFF)
    {
    case 0x00: case 0x01: case 0x02: case 0x03:
    case 0x04: case 0x05: case 0x06: case 0x07:
    case 0x08: case 0x09: case 0x0A: case 0x0B:
    case 0x0C: case 0x0D: case 0x0E: case 0x0F:
    case 0x10: case 0x11: case 0x12: case 0x13:
        return mmEmuApu_Read(&p->apu, addr);
        break;
    case 0x15:
        return mmEmuApu_Read(&p->apu, addr);
        break;

    case 0x14:
        return addr & 0xFF;
        break;

    case 0x16:
        if (mmEmuRom_IsVSUNISYSTEM(&p->rom))
        {
            return mmEmuPad_Read(&p->pad, addr);
        }
        else
        {
            return mmEmuPad_Read(&p->pad, addr) | 0x40 | p->tape.m_TapeOut;
        }
        break;
    case 0x17:
        if (mmEmuRom_IsVSUNISYSTEM(&p->rom))
        {
            return mmEmuPad_Read(&p->pad, addr);
        }
        else
        {
            return mmEmuPad_Read(&p->pad, addr) | mmEmuApu_Read(&p->apu, addr);
        }
        break;
    default:
        return (*(p->mapper->ExRead))(p->mapper, addr);
        break;
    }
}
MM_EXPORT_EMU void mmEmuNes_WriteReg(struct mmEmuNes* p, mmWord_t addr, mmByte_t data)
{
    switch (addr & 0xFF)
    {
    case 0x00: case 0x01: case 0x02: case 0x03:
    case 0x04: case 0x05: case 0x06: case 0x07:
    case 0x08: case 0x09: case 0x0A: case 0x0B:
    case 0x0C: case 0x0D: case 0x0E: case 0x0F:
    case 0x10: case 0x11: case 0x12: case 0x13:
    case 0x15:
        mmEmuApu_Write(&p->apu, addr, data);
        p->mmu.CPUREG[addr & 0xFF] = data;
        break;

    case 0x14:
        mmEmuPpu_DMA(&p->ppu, data);
        mmEmuCpu_DMA(&p->cpu, 514);// DMA Pending cycle
        p->mmu.CPUREG[addr & 0xFF] = data;
        break;

    case 0x16:
        (*(p->mapper->ExWrite))(p->mapper, addr, data);// For VS-Unisystem
        mmEmuPad_Write(&p->pad, addr, data);
        p->mmu.CPUREG[addr & 0xFF] = data;
        p->tape.m_TapeIn = data;
        break;
    case 0x17:
        p->mmu.CPUREG[addr & 0xFF] = data;
        mmEmuPad_Write(&p->pad, addr, data);
        mmEmuApu_Write(&p->apu, addr, data);
        break;
        // Virtua NES specific port.
    case 0x18:
        mmEmuApu_Write(&p->apu, addr, data);
        break;

#if MM_EMU_NES_PROFILER
    case 0x1D:
        p->m_Profiler.m_dwProfileAveCycle = 0;
        p->m_Profiler.m_dwProfileMaxCycle = 0;
        break;
    case 0x1E:
        p->m_Profiler.m_dwProfileTempCycle = mmEmuCpu_GetTotalCycles(&p->cpu);
        break;
    case 0x1F:
        p->m_Profiler.m_dwProfileCycle = mmEmuCpu_GetTotalCycles(&p->cpu) - p->m_Profiler.m_dwProfileTempCycle - 4;
        if (!p->m_Profiler.m_dwProfileAveCycle)
        {
            p->m_Profiler.m_dwProfileAveCycle += p->m_Profiler.m_dwProfileCycle;
        }
        else
        {
            p->m_Profiler.m_dwProfileAveCycle += p->m_Profiler.m_dwProfileCycle;
            p->m_Profiler.m_dwProfileAveCycle /= 2;
        }
        if (p->m_Profiler.m_dwProfileMaxCycle < p->m_Profiler.m_dwProfileCycle)
        {
            p->m_Profiler.m_dwProfileMaxCycle = p->m_Profiler.m_dwProfileCycle;
        }
        break;
#endif
#if 0
    case 0x1C:
        p->m_Profiler.m_dwProfileTempCycle = mmEmuCpu_GetTotalCycles(&p->cpu);
        break;
    case 0x1D:
        p->m_Profiler.m_dwProfileCycle = mmEmuCpu_GetTotalCycles(&p->cpu) - p->m_Profiler.m_dwProfileTempCycle - 4;
        p->m_Profiler.m_dwProfileTotalCycle += p->m_Profiler.m_dwProfileCycle;
        p->m_Profiler.m_dwProfileTotalCount++;
        break;
    case 0x1E:
        p->m_Profiler.m_dwProfileTotalCount = 0;
        p->m_Profiler.m_dwProfileTotalCycle = 0;
        p->m_Profiler.m_dwTotalTempCycle = mmEmuCpu_GetTotalCycles(&p->cpu);
        break;
    case 0x1F:
        p->m_Profiler.m_dwTotalCycle = mmEmuCpu_GetTotalCycles(&p->cpu) - p->m_Profiler.m_dwTotalTempCycle - 4;
        break;
#endif
    default:
        (*(p->mapper->ExWrite))(p->mapper, addr, data);
        break;
    }
}

MM_EXPORT_EMU int mmEmuNes_ReadState(struct mmEmuNes* p, FILE* fp)
{
    return mmEmuStateFile_ReadState(p, fp);
}
MM_EXPORT_EMU int mmEmuNes_WriteState(struct mmEmuNes* p, FILE* fp)
{
    return mmEmuStateFile_WriteState(p, fp);
}

MM_EXPORT_EMU int mmEmuNes_LoadSRAM(struct mmEmuNes* p)
{
    return mmEmuSrom_LoadSRAM(p);
}
MM_EXPORT_EMU int mmEmuNes_SaveSRAM(struct mmEmuNes* p)
{
    return mmEmuSrom_SaveSRAM(p);
}

MM_EXPORT_EMU int mmEmuNes_LoadDISK(struct mmEmuNes* p)
{
    return mmEmuDisk_LoadDISK(p);
}
MM_EXPORT_EMU int mmEmuNes_SaveDISK(struct mmEmuNes* p)
{
    return mmEmuDisk_SaveDISK(p);
}

MM_EXPORT_EMU int mmEmuNes_LoadTurboFile(struct mmEmuNes* p)
{
    return mmEmuTurboFile_LoadTurboFile(p);
}
MM_EXPORT_EMU int mmEmuNes_SaveTurboFile(struct mmEmuNes* p)
{
    return mmEmuTurboFile_SaveTurboFile(p);
}

MM_EXPORT_EMU void mmEmuNes_DrawPad(struct mmEmuNes* p)
{
    mmEmuNesDraw_DrawPad(p);
}
MM_EXPORT_EMU void mmEmuNes_DrawBitmap(struct mmEmuNes* p, mmInt_t x, mmInt_t y, const mmByte_t* lpBitmap)
{
    mmEmuNesDraw_DrawBitmap(p, x, y, lpBitmap);
}

MM_EXPORT_EMU void mmEmuNes_DrawFont(struct mmEmuNes* p, mmInt_t x, mmInt_t y, mmByte_t chr, mmByte_t col)
{
    mmEmuNesDraw_DrawFont(p, x, y, chr, col);
}
MM_EXPORT_EMU void mmEmuNes_DrawString(struct mmEmuNes* p, mmInt_t x, mmInt_t y, mmByte_t* str, mmByte_t col)
{
    mmEmuNesDraw_DrawString(p, x, y, str, col);
}

static void __static_mmEmuNes_MovieFinish(struct mmEmuMovie* obj)
{
    struct mmEmuNes* nes = (struct mmEmuNes*)(obj->callback.obj);
    (*(nes->callback.MovieFinish))(nes);
}
static void __static_mmEmuNes_ScreenMessage(struct mmEmuScreen* obj, const char* message)
{
    struct mmEmuNes* nes = (struct mmEmuNes*)(obj->callback.obj);
    (*(nes->callback.ScreenMessage))(nes, message);
}


