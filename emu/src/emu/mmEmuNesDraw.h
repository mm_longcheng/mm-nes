/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuNesDraw_h__
#define __mmEmuNesDraw_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_EMU extern const mmByte_t MM_EMU_NES_FONT6X8[];

MM_EXPORT_EMU extern const mmByte_t MM_EMU_NES_PADIMG[];
MM_EXPORT_EMU extern const mmByte_t MM_EMU_NES_KEYIMG0[];
MM_EXPORT_EMU extern const mmByte_t MM_EMU_NES_KEYIMG1[];
MM_EXPORT_EMU extern const mmByte_t MM_EMU_NES_KEYIMG2[];

struct mmEmuNes;

MM_EXPORT_EMU void mmEmuNesDraw_DrawPad(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNesDraw_DrawBitmap(struct mmEmuNes* p, mmInt_t x, mmInt_t y, const mmByte_t* lpBitmap);

MM_EXPORT_EMU void mmEmuNesDraw_DrawFont(struct mmEmuNes* p, mmInt_t x, mmInt_t y, mmByte_t chr, mmByte_t col);
MM_EXPORT_EMU void mmEmuNesDraw_DrawString(struct mmEmuNes* p, mmInt_t x, mmInt_t y, mmByte_t* str, mmByte_t col);

#include "core/mmSuffix.h"

#endif//__mmEmuNesDraw_h__
