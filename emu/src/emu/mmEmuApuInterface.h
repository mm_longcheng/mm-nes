/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApuInterface_h__
#define __mmEmuApuInterface_h__

#include "core/mmCore.h"
#include "core/mmTypes.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

#define MM_EMU_APU_CLOCK    1789772.5f

// Fixed point decimal macro
#define MM_EMU_INT2FIX(x)   ((x)<<16)
#define MM_EMU_FIX2INT(x)   ((x)>>16)

struct mmEmuNes;

struct mmEmuApuInterface
{
    struct mmEmuNes* nes;

    void(*Reset)(struct mmEmuApuInterface* p, double fclock, mmInt_t nrate);
    void(*Setup)(struct mmEmuApuInterface* p, double fclock, mmInt_t nrate);
    void(*Write)(struct mmEmuApuInterface* p, mmWord_t addr, mmByte_t data);
    mmInt_t(*Process)(struct mmEmuApuInterface* p, mmInt_t channel);

    mmByte_t(*Read)(struct mmEmuApuInterface* p, mmWord_t addr);

    void(*SyncWrite)(struct mmEmuApuInterface* p, mmWord_t addr, mmByte_t data);
    mmByte_t(*SyncRead)(struct mmEmuApuInterface* p, mmWord_t addr);
    void(*VSync)(struct mmEmuApuInterface* p);
    mmBool_t(*Sync)(struct mmEmuApuInterface* p, mmInt_t cycles);

    mmInt_t(*GetFreq)(const struct mmEmuApuInterface* p, mmInt_t channel);

    // for state save
    mmInt_t(*GetStateSize)(const struct mmEmuApuInterface* p);
    void(*SaveState)(struct mmEmuApuInterface* p, mmByte_t* buffer);
    void(*LoadState)(struct mmEmuApuInterface* p, mmByte_t* buffer);
};

MM_EXPORT_EMU void mmEmuApuInterface_Init(struct mmEmuApuInterface* p);
MM_EXPORT_EMU void mmEmuApuInterface_Destroy(struct mmEmuApuInterface* p);

MM_EXPORT_EMU void mmEmuApuInterface_SetParent(struct mmEmuApuInterface* p, struct mmEmuNes* parent);

// virtual function for super.
MM_EXPORT_EMU void mmEmuApuInterface_Reset(struct mmEmuApuInterface* p, double fclock, mmInt_t nrate);
MM_EXPORT_EMU void mmEmuApuInterface_Setup(struct mmEmuApuInterface* p, double fclock, mmInt_t nrate);
MM_EXPORT_EMU void mmEmuApuInterface_Write(struct mmEmuApuInterface* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmInt_t mmEmuApuInterface_Process(struct mmEmuApuInterface* p, mmInt_t channel);

MM_EXPORT_EMU mmByte_t mmEmuApuInterface_Read(struct mmEmuApuInterface* p, mmWord_t addr);

MM_EXPORT_EMU void mmEmuApuInterface_SyncWrite(struct mmEmuApuInterface* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuApuInterface_SyncRead(struct mmEmuApuInterface* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuApuInterface_VSync(struct mmEmuApuInterface* p);
MM_EXPORT_EMU mmBool_t mmEmuApuInterface_Sync(struct mmEmuApuInterface* p, mmInt_t cycles);

MM_EXPORT_EMU mmInt_t mmEmuApuInterface_GetFreq(const struct mmEmuApuInterface* p, mmInt_t channel);
// for state save
MM_EXPORT_EMU mmInt_t mmEmuApuInterface_GetStateSize(const struct mmEmuApuInterface* p);
MM_EXPORT_EMU void mmEmuApuInterface_SaveState(struct mmEmuApuInterface* p, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuApuInterface_LoadState(struct mmEmuApuInterface* p, mmByte_t* buffer);

#include "core/mmSuffix.h"

#endif//__mmEmuApuInterface_h__
