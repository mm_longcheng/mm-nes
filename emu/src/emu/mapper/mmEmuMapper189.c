/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper189.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper189_Init(struct mmEmuMapper189* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 2);

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    mmMemset(p->protect_dat, 0, sizeof(mmByte_t) * 4);
    p->lwd = 0;

    //
    p->super.Reset = &mmEmuMapper189_Reset;
    p->super.Write = &mmEmuMapper189_Write;
    //p->super.Read = &mmEmuMapper189_Read;
    //p->super.ReadLow = &mmEmuMapper189_ReadLow;
    p->super.WriteLow = &mmEmuMapper189_WriteLow;
    //p->super.ExRead = &mmEmuMapper189_ExRead;
    //p->super.ExWrite = &mmEmuMapper189_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper189_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper189_ExCmdWrite;
    p->super.HSync = &mmEmuMapper189_HSync;
    //p->super.VSync = &mmEmuMapper189_VSync;
    //p->super.Clock = &mmEmuMapper189_Clock;
    //p->super.PPULatch = &mmEmuMapper189_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper189_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper189_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper189_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper189_IsStateSave;
    p->super.SaveState = &mmEmuMapper189_SaveState;
    p->super.LoadState = &mmEmuMapper189_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper189_Destroy(struct mmEmuMapper189* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 2);

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    mmMemset(p->protect_dat, 0, sizeof(mmByte_t) * 4);
    p->lwd = 0;
}

MM_EXPORT_EMU void mmEmuMapper189_SetBankCPU(struct mmEmuMapper189* p)
{
    //struct mmEmuNes* nes = p->super.nes;
    //struct mmEmuMmu* mmu = &nes->mmu;
}
MM_EXPORT_EMU void mmEmuMapper189_SetBankPPU(struct mmEmuMapper189* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->patch)
    {
        mmEmuMmu_SetVROM8KBankArray(
            mmu,
            p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1,
            p->chr4, p->chr5, p->chr6, p->chr7);
    }
    else
    {
        if (mmu->VROM_1K_SIZE)
        {
            if (p->reg[0] & 0x80)
            {
                mmEmuMmu_SetVROM8KBankArray(
                    mmu,
                    p->chr4, p->chr5, p->chr6, p->chr7,
                    p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1);
            }
            else
            {
                mmEmuMmu_SetVROM8KBankArray(
                    mmu,
                    p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1,
                    p->chr4, p->chr5, p->chr6, p->chr7);
            }
        }
        else
        {
            if (p->reg[0] & 0x80)
            {
                mmEmuMmu_SetCRAM1KBank(mmu, 4, (p->chr01 + 0) & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 5, (p->chr01 + 1) & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 6, (p->chr23 + 0) & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 7, (p->chr23 + 1) & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 0, p->chr4 & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 1, p->chr5 & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 2, p->chr6 & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 3, p->chr7 & 0x07);
            }
            else
            {
                mmEmuMmu_SetCRAM1KBank(mmu, 0, (p->chr01 + 0) & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 1, (p->chr01 + 1) & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 2, (p->chr23 + 0) & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 3, (p->chr23 + 1) & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 4, p->chr4 & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 5, p->chr5 & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 6, p->chr6 & 0x07);
                mmEmuMmu_SetCRAM1KBank(mmu, 7, p->chr7 & 0x07);
            }
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper189_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper189* p = (struct mmEmuMapper189*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmEmuMmu_SetPROM32KBankArray(
        mmu,
        mmu->PROM_08K_SIZE - 4, mmu->PROM_08K_SIZE - 3,
        mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->reg[0] = p->reg[1] = 0;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    mmEmuMapper189_SetBankPPU(p);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    mmMemset(p->protect_dat, 0, sizeof(mmByte_t) * 4);
    p->lwd = 0xFF;

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x20ca2ad3)          // Street Fighter IV (GOUDER)
    {
        p->patch = 1;
        mmEmuMmu_SetPROM32KBank(mmu, 0);

        // $4000-$5FFF
        mmEmuMmu_SetPROMBank(mmu, 2, mmu->XRAM, MM_EMU_MMU_BANKTYPE_ROM);
    }
}

MM_EXPORT_EMU void mmEmuMapper189_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper189* p = (struct mmEmuMapper189*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xFF00) == 0x4100)
    {
        // Street Fighter 2 YOKO
        mmEmuMmu_SetPROM32KBank(mmu, (data & 0x30) >> 4);
    }
    else if ((addr & 0xFF00) == 0x6100)
    {
        // Master Fighter 2
        mmEmuMmu_SetPROM32KBank(mmu, data & 0x03);
    }

    if (p->patch)
    {
        // Street Fighter IV (GOUDER)
        static const mmByte_t a5000xordat[256] =
        {
            0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x49, 0x19, 0x09, 0x59, 0x49, 0x19, 0x09,
            0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x51, 0x41, 0x11, 0x01, 0x51, 0x41, 0x11, 0x01,
            0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x49, 0x19, 0x09, 0x59, 0x49, 0x19, 0x09,
            0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x51, 0x41, 0x11, 0x01, 0x51, 0x41, 0x11, 0x01,
            0x00, 0x10, 0x40, 0x50, 0x00, 0x10, 0x40, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x08, 0x18, 0x48, 0x58, 0x08, 0x18, 0x48, 0x58, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x10, 0x40, 0x50, 0x00, 0x10, 0x40, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x08, 0x18, 0x48, 0x58, 0x08, 0x18, 0x48, 0x58, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x58, 0x48, 0x18, 0x08, 0x58, 0x48, 0x18, 0x08,
            0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x50, 0x40, 0x10, 0x00, 0x50, 0x40, 0x10, 0x00,
            0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x58, 0x48, 0x18, 0x08, 0x58, 0x48, 0x18, 0x08,
            0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x59, 0x50, 0x40, 0x10, 0x00, 0x50, 0x40, 0x10, 0x00,
            0x01, 0x11, 0x41, 0x51, 0x01, 0x11, 0x41, 0x51, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x09, 0x19, 0x49, 0x59, 0x09, 0x19, 0x49, 0x59, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x01, 0x11, 0x41, 0x51, 0x01, 0x11, 0x41, 0x51, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x09, 0x19, 0x49, 0x59, 0x09, 0x19, 0x49, 0x59, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        };

        if ((addr >= 0x4800) && (addr <= 0x4FFF))
        {
            mmEmuMmu_SetPROM32KBank(mmu, ((data & 0x10) >> 3) + (data & 0x1));

            if (!mmEmuRom_Is4SCREEN(&nes->rom))
            {
                if (data & 0x20)
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
                }
                else
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
                }
            }
        }
        if ((addr >= 0x5000) && (addr <= 0x57FF))
        {
            p->lwd = data;
        }
        if ((addr >= 0x5800) && (addr <= 0x5FFF))
        {
            //mmu->XRAM[0x1000+(addr & 3)] = 
            // $5800 "JMP $xxxx" write
            mmu->XRAM[0x1800 + (addr & 3)] = p->protect_dat[addr & 3] = data ^ a5000xordat[p->lwd];
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper189_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper189* p = (struct mmEmuMapper189*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper189_SetBankPPU(p);
        break;

    case 0x8001:
        p->reg[1] = data;
        mmEmuMapper189_SetBankPPU(p);
        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr01 = data & 0xFE;
            mmEmuMapper189_SetBankPPU(p);
            break;
        case 0x01:
            p->chr23 = data & 0xFE;
            mmEmuMapper189_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper189_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper189_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper189_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper189_SetBankPPU(p);
            break;
        default:
            break;
        }
        break;

    case 0xA000:
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;

    case 0xC000:
        p->irq_counter = data;
        break;
    case 0xC001:
        p->irq_latch = data;
        break;
    case 0xE000:
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->irq_enable = 0xFF;
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper189_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper189* p = (struct mmEmuMapper189*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(--p->irq_counter))
                {
                    //if( !(irq_counter--) )
                    p->irq_counter = p->irq_latch;
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper189_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper189_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper189* p = (struct mmEmuMapper189*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->chr01;
    buffer[3] = p->chr23;
    buffer[4] = p->chr4;
    buffer[5] = p->chr5;
    buffer[6] = p->chr6;
    buffer[7] = p->chr7;

    buffer[8] = p->irq_enable;
    buffer[9] = p->irq_counter;
    buffer[10] = p->irq_latch;

    buffer[16] = p->protect_dat[0];
    buffer[17] = p->protect_dat[1];
    buffer[18] = p->protect_dat[2];
    buffer[19] = p->protect_dat[3];
    buffer[20] = p->lwd;
}
MM_EXPORT_EMU void mmEmuMapper189_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper189* p = (struct mmEmuMapper189*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->chr01 = buffer[2];
    p->chr23 = buffer[3];
    p->chr4 = buffer[4];
    p->chr5 = buffer[5];
    p->chr6 = buffer[6];
    p->chr7 = buffer[7];

    p->irq_enable = buffer[8];
    p->irq_counter = buffer[9];
    p->irq_latch = buffer[10];

    p->protect_dat[0] = buffer[16];
    p->protect_dat[1] = buffer[17];
    p->protect_dat[2] = buffer[18];
    p->protect_dat[3] = buffer[19];
    p->lwd = buffer[20];
}

static struct mmEmuMapper* __static_mmEmuMapper189_Produce(void)
{
    struct mmEmuMapper189* p = (struct mmEmuMapper189*)mmMalloc(sizeof(struct mmEmuMapper189));
    mmEmuMapper189_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper189_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper189* p = (struct mmEmuMapper189*)(m);
    mmEmuMapper189_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER189 =
{
    &__static_mmEmuMapper189_Produce,
    &__static_mmEmuMapper189_Recycle,
};
