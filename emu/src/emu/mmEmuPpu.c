/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPpu.h"
#include "mmEmuNes.h"
#include "mmEmuMapper.h"

static void __static_mmEmuPpu_HorizontalInvertedMaskTable(struct mmEmuPpu* p);

MM_EXPORT_EMU const mmByte_t MM_EMU_PPU_VSCOLORMAP[5][64] =
{
    {
        0x35, 0xFF, 0x16, 0x22, 0x1C, 0xFF, 0xFF, 0x15,
        0xFF, 0x00, 0x27, 0x05, 0x04, 0x27, 0x08, 0x30,
        0x21, 0xFF, 0xFF, 0x29, 0x3C, 0xFF, 0x36, 0x12,
        0xFF, 0x2B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01,
        0xFF, 0x31, 0xFF, 0x2A, 0x2C, 0x0C, 0xFF, 0xFF,
        0xFF, 0x07, 0x34, 0x06, 0x13, 0xFF, 0x26, 0x0F,
        0xFF, 0x19, 0x10, 0x0A, 0xFF, 0xFF, 0xFF, 0x17,
        0xFF, 0x11, 0x09, 0xFF, 0xFF, 0x25, 0x18, 0xFF,
    },
    {
        0xFF, 0x27, 0x18, 0xFF, 0x3A, 0x25, 0xFF, 0x31,
        // 0x16, 0x13, 0x38, 0x34, 0x20, 0x23, 0xFF, 0x0B,
        // 0x16, 0x13, 0x38, 0x34, 0x20, 0x23, 0xFF, 0x1A,
        0x16, 0x13, 0x38, 0x34, 0x20, 0x23, 0x31, 0x1A,
        0xFF, 0x21, 0x06, 0xFF, 0x1B, 0x29, 0xFF, 0x22,
        0xFF, 0x24, 0xFF, 0xFF, 0xFF, 0x08, 0xFF, 0x03,
        0xFF, 0x36, 0x26, 0x33, 0x11, 0xFF, 0x10, 0x02,
        0x14, 0xFF, 0x00, 0x09, 0x12, 0x0F, 0xFF, 0x30,
        0xFF, 0xFF, 0x2A, 0x17, 0x0C, 0x01, 0x15, 0x19,
        0xFF, 0x2C, 0x07, 0x37, 0xFF, 0x05, 0xFF, 0xFF,
    },
#if 1
    {
        0xFF, 0xFF, 0xFF, 0x10, 0x1A, 0x30, 0x31, 0x09,
        0x01, 0x0F, 0x36, 0x08, 0x15, 0xFF, 0xFF, 0xF0,
        0x22, 0x1C, 0xFF, 0x12, 0x19, 0x18, 0x17, 0xFF,
        0x00, 0xFF, 0xFF, 0x02, 0x16, 0x06, 0xFF, 0x35,
        0x23, 0xFF, 0x8B, 0xF7, 0xFF, 0x27, 0x26, 0x20,
        0x29, 0xFF, 0x21, 0x24, 0x11, 0xFF, 0xEF, 0xFF,
        0x2C, 0xFF, 0xFF, 0xFF, 0x07, 0xF9, 0x28, 0xFF,
        0x0A, 0xFF, 0x32, 0x37, 0x13, 0xFF, 0xFF, 0x0C,
    },
#else
    {
        0xFF, 0xFF, 0xFF, 0x10, 0x0B, 0x30, 0x31, 0x09, // 00-07
        0x01, 0x0F, 0x36, 0x08, 0x15, 0xFF, 0xFF, 0x3C, // 08-0F
        0x22, 0x1C, 0xFF, 0x12, 0x19, 0x18, 0x17, 0x1B, // 10-17
        0x00, 0xFF, 0xFF, 0x02, 0x16, 0x06, 0xFF, 0x35, // 18-1F
        0x23, 0xFF, 0x8B, 0x3C, 0xFF, 0x27, 0x26, 0x20, // 20-27
        0x29, 0x04, 0x21, 0x24, 0x11, 0xFF, 0xEF, 0xFF, // 28-2F
        0x2C, 0xFF, 0xFF, 0xFF, 0x07, 0x39, 0x28, 0xFF, // 30-37
        0x0A, 0xFF, 0x32, 0x38, 0x13, 0x3B, 0xFF, 0x0C, // 38-3F
    },
#endif
#if 0
    {
        0x18, 0xFF, 0x1C, 0x89, 0x0F, 0xFF, 0x01, 0x17,
        0x10, 0x0F, 0x2A, 0xFF, 0x36, 0x37, 0x1A, 0xFF,
        0x25, 0xFF, 0x12, 0xFF, 0x0F, 0xFF, 0xFF, 0x26,
        0xFF, 0xFF, 0x22, 0xFF, 0xFF, 0x0F, 0x3A, 0x21,
        0x05, 0x0A, 0x07, 0xC2, 0x13, 0xFF, 0x00, 0x15,
        0x0C, 0xFF, 0x11, 0xFF, 0xFF, 0x38, 0xFF, 0xFF,
        0xFF, 0xFF, 0x08, 0x45, 0xFF, 0xFF, 0x30, 0x3C,
        0x0F, 0x27, 0xFF, 0x60, 0x29, 0xFF, 0x30, 0x09,
    },
#else
    {
        0x18, 0xFF, 0x1C, 0x89, 0x0F, 0xFF, 0x01, 0x17, // 00-07
        0x10, 0x0F, 0x2A, 0xFF, 0x36, 0x37, 0x1A, 0xFF, // 08-0F
        0x25, 0xFF, 0x12, 0xFF, 0x0F, 0xFF, 0xFF, 0x26, // 10-17
        0xFF, 0xFF, 0x22, 0xFF, 0xFF, 0x0F, 0x3A, 0x21, // 18-1F
        0x05, 0x0A, 0x07, 0xC2, 0x13, 0xFF, 0x00, 0x15, // 20-27
        0x0C, 0xFF, 0x11, 0xFF, 0xFF, 0x38, 0xFF, 0xFF, // 28-2F
        0xFF, 0xFF, 0x08, 0x16, 0xFF, 0xFF, 0x30, 0x3C, // 30-37
        0x0F, 0x27, 0xFF, 0x60, 0x29, 0xFF, 0x30, 0x09, // 38-3F
    },
#endif
    // Super Xevious/Gradius
    {
        0x35, 0xFF, 0x16, 0x22, 0x1C, 0x09, 0xFF, 0x15, // 00-07
        0x20, 0x00, 0x27, 0x05, 0x04, 0x28, 0x08, 0x30, // 08-0F
        0x21, 0xFF, 0xFF, 0x29, 0x3C, 0xFF, 0x36, 0x12, // 10-17
        0xFF, 0x2B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01, // 18-1F
        0xFF, 0x31, 0xFF, 0x2A, 0x2C, 0x0C, 0x1B, 0xFF, // 20-27
        0xFF, 0x07, 0x34, 0x06, 0xFF, 0x25, 0x26, 0x0F, // 28-2F
        0xFF, 0x19, 0x10, 0x0A, 0xFF, 0xFF, 0xFF, 0x17, // 30-37
        0xFF, 0x11, 0x1A, 0xFF, 0x38, 0xFF, 0x18, 0x3A, // 38-3F
    },
};

MM_EXPORT_EMU void mmEmuPpu_Init(struct mmEmuPpu* p)
{
    p->nes = NULL;

    p->bExtLatch = MM_FALSE;
    p->bChrLatch = MM_FALSE;
    p->bExtNameTable = MM_FALSE;
    p->bExtMono = MM_FALSE;

    p->loopy_y = 0;
    p->loopy_shift = 0;

    p->lpScreen = NULL;
    p->lpScanline = NULL;
    p->ScanlineNo = 0;
    p->lpColormode = NULL;

    mmMemset(p->Bit2Rev, 0, sizeof(mmByte_t) * 256);

    p->bVSMode = MM_FALSE;
    p->VSSecurityData = 0;
    p->nVSColorMap = -1;

    __static_mmEmuPpu_HorizontalInvertedMaskTable(p);
}
MM_EXPORT_EMU void mmEmuPpu_Destroy(struct mmEmuPpu* p)
{
    p->nes = NULL;

    p->bExtLatch = MM_FALSE;
    p->bChrLatch = MM_FALSE;
    p->bExtNameTable = MM_FALSE;
    p->bExtMono = MM_FALSE;

    p->loopy_y = 0;
    p->loopy_shift = 0;

    p->lpScreen = NULL;
    p->lpScanline = NULL;
    p->ScanlineNo = 0;
    p->lpColormode = NULL;

    mmMemset(p->Bit2Rev, 0, sizeof(mmByte_t) * 256);

    p->bVSMode = MM_FALSE;
    p->VSSecurityData = 0;
    p->nVSColorMap = -1;
}
MM_EXPORT_EMU void mmEmuPpu_SetParent(struct mmEmuPpu* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU void mmEmuPpu_Reset(struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->bExtLatch = MM_FALSE;
    p->bChrLatch = MM_FALSE;
    p->bExtNameTable = MM_FALSE;
    p->bExtMono = MM_FALSE;

    mmu->PPUREG[0] = mmu->PPUREG[1] = 0;

    mmu->PPU56Toggle = 0;

    // I'm going crazy with VS Excitebike (there is a bug that goes to read $ 2006).
    mmu->PPU7_Temp = 0xFF;
    // mmu->PPU7_Temp = 0;

    mmu->loopy_v = mmu->loopy_t = 0;
    mmu->loopy_x = p->loopy_y = 0;
    p->loopy_shift = 0;

    if (p->lpScreen)
    {
        mmMemset(p->lpScreen, 0x3F, MM_EMU_PPU_SCREEN_W * MM_EMU_PPU_SCREEN_H * sizeof(mmByte_t));
    }

    if (p->lpColormode)
    {
        mmMemset(p->lpColormode, 0, MM_EMU_PPU_SCREEN_H * sizeof(mmByte_t));
    }
}

MM_EXPORT_EMU mmByte_t mmEmuPpu_Read(struct mmEmuPpu* p, mmWord_t addr)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t data = 0x00;

    switch (addr)
    {
        // Write only Register
    case 0x2000: // PPU Control Register #1(W)
    case 0x2001: // PPU Control Register #2(W)
    case 0x2003: // SPR-RAM Address Register(W)
    case 0x2005: // PPU Scroll Register(W2)
    case 0x2006: // VRAM Address Register(W2)
        data = mmu->PPU7_Temp;  // Perhaps
        break;
        // Read/Write Register
    case 0x2002: // PPU Status Register(R)
        // DEBUGOUT( "2002 RD L:%3d C:%8d\n", ScanlineNo, nes->cpu->GetTotalCycles() );
        data = mmu->PPUREG[2] | p->VSSecurityData;
        mmu->PPU56Toggle = 0;
        mmu->PPUREG[2] &= ~MM_EMU_PPU_VBLANK_FLAG;
        break;
    case 0x2004: // SPR_RAM I/O Register(RW)
        data = mmu->SPRAM[mmu->PPUREG[3]++];
        break;
    case 0x2007: // VRAM I/O Register(RW)
    {
        mmWord_t addr = 0;

        addr = mmu->loopy_v & 0x3FFF;
        data = mmu->PPU7_Temp;
        if (mmu->PPUREG[0] & MM_EMU_PPU_INC32_BIT)
        {
            mmu->loopy_v += 32;
        }
        else
        {
            mmu->loopy_v++;
        }
        if (addr >= 0x3000)
        {
            if (addr >= 0x3F00)
            {
                // data &= 0x3F;
                if (!(addr & 0x0010))
                {
                    return mmu->BGPAL[addr & 0x000F];
                }
                else
                {
                    return mmu->SPPAL[addr & 0x000F];
                }
            }
            addr &= 0xEFFF;
        }
        mmu->PPU7_Temp = mmu->PPU_MEM_BANK[addr >> 10][addr & 0x03FF];
    }
    break;
    default:
        break;
    }

    return  data;
}
MM_EXPORT_EMU void mmEmuPpu_Write(struct mmEmuPpu* p, mmWord_t addr, mmByte_t data)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmWord_t vaddr = 0;

    if (p->bVSMode && p->VSSecurityData)
    {
        if (addr == 0x2000)
        {
            addr = 0x2001;
        }
        else if (addr == 0x2001)
        {
            addr = 0x2000;
        }
    }

    switch (addr)
    {
        // Read only Register
    case 0x2002: // PPU Status register(R)
        break;
        // Write Register
    case 0x2000: // PPU Control Register #1(W)
        // NameTable select
        // t:0000110000000000=d:00000011
        mmu->loopy_t = (mmu->loopy_t & 0xF3FF) | (((mmWord_t)data & 0x03) << 10);

        if ((data & 0x80) && !(mmu->PPUREG[0] & 0x80) && (mmu->PPUREG[2] & 0x80))
        {
            // hmm..
            mmEmuCpu_NMI(&nes->cpu);
        }
        //DEBUGOUT( "W2000 %02X O:%02X S:%02X L:%3d C:%8d\n", data, PPUREG[0], PPUREG[2], ScanlineNo, nes->cpu->GetTotalCycles() );
        mmu->PPUREG[0] = data;
        break;
    case 0x2001: // PPU Control Register #2(W)
        //DEBUGOUT( "W2001 %02X L:%3d C:%8d\n", data, ScanlineNo, nes->cpu->GetTotalCycles() );
        mmu->PPUREG[1] = data;
        break;
    case 0x2003: // SPR-RAM Address Register(W)
        mmu->PPUREG[3] = data;
        break;
    case 0x2004: // SPR_RAM I/O Register(RW)
        mmu->SPRAM[mmu->PPUREG[3]++] = data;
        break;

    case 0x2005: // PPU Scroll Register(W2)
        //DEBUGOUT( "SCR WRT L:%3d C:%8d\n", ScanlineNo, nes->cpu->GetTotalCycles() );
        if (!mmu->PPU56Toggle)
        {
            // First write
            // tile X t:0000000000011111=d:11111000
            mmu->loopy_t = (mmu->loopy_t & 0xFFE0) | (((mmWord_t)data) >> 3);
            // scroll offset X x=d:00000111
            mmu->loopy_x = data & 0x07;
        }
        else
        {
            // Second write
            // tile Y t:0000001111100000=d:11111000
            mmu->loopy_t = (mmu->loopy_t & 0xFC1F) | ((((mmWord_t)data) & 0xF8) << 2);
            // scroll offset Y t:0111000000000000=d:00000111
            mmu->loopy_t = (mmu->loopy_t & 0x8FFF) | ((((mmWord_t)data) & 0x07) << 12);
        }
        mmu->PPU56Toggle = !mmu->PPU56Toggle;
        break;
    case 0x2006: // VRAM Address Register(W2)
        if (!mmu->PPU56Toggle)
        {
            // First write
            // t:0011111100000000=d:00111111
            // t:1100000000000000=0
            mmu->loopy_t = (mmu->loopy_t & 0x00FF) | ((((mmWord_t)data) & 0x3F) << 8);
        }
        else
        {
            // Second write
            // t:0000000011111111=d:11111111
            mmu->loopy_t = (mmu->loopy_t & 0xFF00) | (mmWord_t)data;
            // v=t
            mmu->loopy_v = mmu->loopy_t;

            (*(nes->mapper->PPULatch))(nes->mapper, mmu->loopy_v);
        }
        mmu->PPU56Toggle = !mmu->PPU56Toggle;
        break;

    case 0x2007: // VRAM I/O Register(RW)
        vaddr = mmu->loopy_v & 0x3FFF;
        if (mmu->PPUREG[0] & MM_EMU_PPU_INC32_BIT)
        {
            mmu->loopy_v += 32;
        }
        else
        {
            mmu->loopy_v++;
        }

        if (vaddr >= 0x3000)
        {
            if (vaddr >= 0x3F00)
            {
                data &= 0x3F;
                if (p->bVSMode && p->nVSColorMap != -1)
                {
                    mmByte_t color_t = MM_EMU_PPU_VSCOLORMAP[p->nVSColorMap][data];
                    if (color_t != 0xFF)
                    {
                        data = color_t & 0x3F;
                    }
                }

                if (!(vaddr & 0x000F))
                {
                    mmu->BGPAL[0] = mmu->SPPAL[0] = data;
                }
                else if (!(vaddr & 0x0010))
                {
                    mmu->BGPAL[vaddr & 0x000F] = data;
                }
                else
                {
                    mmu->SPPAL[vaddr & 0x000F] = data;
                }
                mmu->BGPAL[0x04] = mmu->BGPAL[0x08] = mmu->BGPAL[0x0C] = mmu->BGPAL[0x00];
                mmu->SPPAL[0x00] = mmu->SPPAL[0x04] = mmu->SPPAL[0x08] = mmu->SPPAL[0x0C] = mmu->BGPAL[0x00];
                return;
            }
            vaddr &= 0xEFFF;
        }
        if (mmu->PPU_MEM_TYPE[vaddr >> 10] != MM_EMU_MMU_BANKTYPE_VROM)
        {
            mmu->PPU_MEM_BANK[vaddr >> 10][vaddr & 0x03FF] = data;
        }
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuPpu_DMA(struct mmEmuPpu* p, mmByte_t data)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i = 0;

    mmWord_t addr = data << 8;

    for (i = 0; i < 256; i++)
    {
        mmu->SPRAM[i] = mmEmuNes_Read(nes, addr + i);
    }
}

MM_EXPORT_EMU void mmEmuPpu_VBlankStart(struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmu->PPUREG[2] |= MM_EMU_PPU_VBLANK_FLAG;
    // mmu->PPUREG[2] |= MM_EMU_PPU_SPHIT_FLAG; // Always ON when entering VBlank?
}
MM_EXPORT_EMU void mmEmuPpu_VBlankEnd(struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmu->PPUREG[2] &= ~MM_EMU_PPU_VBLANK_FLAG;
    // Cleared at the time of escape from VBlank.
    // Important in Excite Bikes.
    mmu->PPUREG[2] &= ~MM_EMU_PPU_SPHIT_FLAG;
}

MM_EXPORT_EMU void mmEmuPpu_FrameStart(struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->PPUREG[1] & (MM_EMU_PPU_SPDISP_BIT | MM_EMU_PPU_BGDISP_BIT))
    {
        mmu->loopy_v = mmu->loopy_t;
        p->loopy_shift = mmu->loopy_x;
        p->loopy_y = (mmu->loopy_v & 0x7000) >> 12;
    }

    if (p->lpScreen)
    {
        mmMemset(p->lpScreen, 0x3F, MM_EMU_PPU_SCREEN_W * sizeof(mmByte_t));
    }
    if (p->lpColormode)
    {
        p->lpColormode[0] = 0;
    }
}
MM_EXPORT_EMU void mmEmuPpu_FrameEnd(struct mmEmuPpu* p)
{
    // do nothing.
}

MM_EXPORT_EMU void mmEmuPpu_SetRenderScanline(struct mmEmuPpu* p, mmInt_t scanline)
{
    p->ScanlineNo = scanline;
    if (scanline < 240)
    {
        p->lpScanline = p->lpScreen + MM_EMU_PPU_SCREEN_W * scanline;
    }
}

MM_EXPORT_EMU void mmEmuPpu_ScanlineStart(struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->PPUREG[1] & (MM_EMU_PPU_BGDISP_BIT | MM_EMU_PPU_SPDISP_BIT))
    {
        mmu->loopy_v = (mmu->loopy_v & 0xFBE0) | (mmu->loopy_t & 0x041F);
        p->loopy_shift = mmu->loopy_x;
        p->loopy_y = (mmu->loopy_v & 0x7000) >> 12;
        (*(nes->mapper->PPULatch))(nes->mapper, 0x2000 + (mmu->loopy_v & 0x0FFF));
    }
}
MM_EXPORT_EMU void mmEmuPpu_ScanlineNext(struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->PPUREG[1] & (MM_EMU_PPU_BGDISP_BIT | MM_EMU_PPU_SPDISP_BIT))
    {
        if ((mmu->loopy_v & 0x7000) == 0x7000)
        {
            mmu->loopy_v &= 0x8FFF;
            if ((mmu->loopy_v & 0x03E0) == 0x03A0)
            {
                mmu->loopy_v ^= 0x0800;
                mmu->loopy_v &= 0xFC1F;
            }
            else
            {
                if ((mmu->loopy_v & 0x03E0) == 0x03E0)
                {
                    mmu->loopy_v &= 0xFC1F;
                }
                else
                {
                    mmu->loopy_v += 0x0020;
                }
            }
        }
        else
        {
            mmu->loopy_v += 0x1000;
        }
        p->loopy_y = (mmu->loopy_v & 0x7000) >> 12;
    }
}

MM_EXPORT_EMU mmWord_t mmEmuPpu_GetPPUADDR(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->loopy_v;
}

MM_EXPORT_EMU void mmEmuPpu_Scanline(struct mmEmuPpu* p, mmInt_t scanline, mmBool_t bMax, mmBool_t bLeftClip)
{
    // https://wiki.nesdev.org/w/index.php?title=Overscan
    //
    // The NES PPU always generates a 256x240 pixel picture.

    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;
    mmByte_t** PPU_MEM_BANK = mmu->PPU_MEM_BANK;

    mmByte_t BGwrite[33 + 1];
    mmByte_t BGmono[33 + 1];

    mmMemset(BGwrite, 0, sizeof(mmByte_t) * (33 + 1));
    mmMemset(BGmono, 0, sizeof(mmByte_t) * (33 + 1));

    // Linecolor mode
    p->lpColormode[scanline] = ((mmu->PPUREG[1] & MM_EMU_PPU_BGCOLOR_BIT) >> 5) | ((mmu->PPUREG[1] & MM_EMU_PPU_COLORMODE_BIT) << 7);

    // Render BG
    if (!(mmu->PPUREG[1] & MM_EMU_PPU_BGDISP_BIT))
    {
        mmMemset(p->lpScanline, mmu->BGPAL[0], MM_EMU_PPU_SCREEN_W);
        if (mmEmuNes_GetRenderMethod(nes) == MM_EMU_NES_TILE_RENDER)
        {
            mmEmuNes_EmulationCPU(nes, MM_EMU_NES_FETCH_CYCLES * 4 * 32);
        }
    }
    else
    {
        if (mmEmuNes_GetRenderMethod(nes) != MM_EMU_NES_TILE_RENDER)
        {
            if (!p->bExtLatch)
            {
                mmInt_t i = 0;
                // Without Extension Latch
                mmByte_t* pScn = p->lpScanline + (8 - p->loopy_shift);
                mmByte_t* pBGw = BGwrite;

                mmInt_t tileofs = (mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) << 8;
                mmInt_t ntbladr = 0x2000 + (mmu->loopy_v & 0x0FFF);
                mmInt_t attradr = 0x23C0 + (mmu->loopy_v & 0x0C00) + ((mmu->loopy_v & 0x0380) >> 4);
                mmInt_t ntbl_x = ntbladr & 0x001F;
                mmInt_t attrsft = (ntbladr & 0x0040) >> 4;
                mmByte_t* pNTBL = PPU_MEM_BANK[ntbladr >> 10];

                mmInt_t tileadr;
                mmInt_t cache_tile = 0xFFFF0000;
                mmByte_t cache_attr = 0xFF;

                mmByte_t chr_h, chr_l, attr;

                attradr &= 0x3FF;

                for (i = 0; i < 33; i++)
                {
                    tileadr = tileofs + pNTBL[ntbladr & 0x03FF] * 0x10 + p->loopy_y;
                    attr = ((pNTBL[attradr + (ntbl_x >> 2)] >> ((ntbl_x & 2) + attrsft)) & 3) << 2;

                    if (cache_tile == tileadr && cache_attr == attr)
                    {
                        //*(mmUInt32_t*)(pScn + 0) = *(mmUInt32_t*)(pScn - 8);
                        //*(mmUInt32_t*)(pScn + 4) = *(mmUInt32_t*)(pScn - 4);
                        // pScn sometime is odd number when p->loopy_shift is odd number, 
                        // and some system odd number transform to uint32 pointer will crash.
                        mmMemcpy(pScn + 0, pScn - 8, 8);
                        *(pBGw + 0) = *(pBGw - 1);
                    }
                    else
                    {
                        mmByte_t* pBGPAL = NULL;

                        cache_tile = tileadr;
                        cache_attr = attr;

                        chr_l = PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
                        chr_h = PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
                        *pBGw = chr_h | chr_l;

                        pBGPAL = &mmu->BGPAL[attr];
                        {
                            register mmInt_t c1 = ((chr_l >> 1) & 0x55) | (chr_h & 0xAA);
                            register mmInt_t c2 = (chr_l & 0x55) | ((chr_h << 1) & 0xAA);
                            pScn[0] = pBGPAL[(c1 >> 6)    ];
                            pScn[1] = pBGPAL[(c2 >> 6)    ];
                            pScn[2] = pBGPAL[(c1 >> 4) & 3];
                            pScn[3] = pBGPAL[(c2 >> 4) & 3];
                            pScn[4] = pBGPAL[(c1 >> 2) & 3];
                            pScn[5] = pBGPAL[(c2 >> 2) & 3];
                            pScn[6] = pBGPAL[(c1     ) & 3];
                            pScn[7] = pBGPAL[(c2     ) & 3];
                        }
                    }
                    pScn += 8;
                    pBGw++;

                    // Character latch(For MMC2/MMC4)
                    if (p->bChrLatch)
                    {
                        (*(nes->mapper->PPUChrLatch))(nes->mapper, tileadr);
                    }

                    if (++ntbl_x == 32)
                    {
                        ntbl_x = 0;
                        ntbladr ^= 0x41F;
                        attradr = 0x03C0 + ((ntbladr & 0x0380) >> 4);
                        pNTBL = PPU_MEM_BANK[ntbladr >> 10];
                    }
                    else
                    {
                        ntbladr++;
                    }
                }
            }
            else
            {
                mmInt_t i = 0;
                // With Extension Latch(For MMC5)
                mmByte_t* pScn = p->lpScanline + (8 - p->loopy_shift);
                mmByte_t* pBGw = BGwrite;

                mmInt_t ntbladr = 0x2000 + (mmu->loopy_v & 0x0FFF);
                mmInt_t ntbl_x = ntbladr & 0x1F;

                mmInt_t cache_tile = 0xFFFF0000;
                mmByte_t cache_attr = 0xFF;

                mmByte_t chr_h, chr_l, attr, exattr;

                for (i = 0; i < 33; i++)
                {
                    (*(nes->mapper->PPUExtLatchX))(nes->mapper, i);
                    (*(nes->mapper->PPUExtLatch))(nes->mapper, ntbladr, &chr_l, &chr_h, &exattr);
                    attr = exattr & 0x0C;

                    if (cache_tile != (((mmInt_t)chr_h << 8) + (mmInt_t)chr_l) || cache_attr != attr)
                    {
                        mmByte_t* pBGPAL = NULL;

                        cache_tile = (((mmInt_t)chr_h << 8) + (mmInt_t)chr_l);
                        cache_attr = attr;
                        *pBGw = chr_h | chr_l;

                        pBGPAL = &mmu->BGPAL[attr];
                        {
                            register mmInt_t c1 = ((chr_l >> 1) & 0x55) | (chr_h & 0xAA);
                            register mmInt_t c2 = (chr_l & 0x55) | ((chr_h << 1) & 0xAA);
                            pScn[0] = pBGPAL[(c1 >> 6)    ];
                            pScn[1] = pBGPAL[(c2 >> 6)    ];
                            pScn[2] = pBGPAL[(c1 >> 4) & 3];
                            pScn[3] = pBGPAL[(c2 >> 4) & 3];
                            pScn[4] = pBGPAL[(c1 >> 2) & 3];
                            pScn[5] = pBGPAL[(c2 >> 2) & 3];
                            pScn[6] = pBGPAL[(c1     ) & 3];
                            pScn[7] = pBGPAL[(c2     ) & 3];
                        }
                    }
                    else
                    {
                        //*(mmUInt32_t*)(pScn + 0) = *(mmUInt32_t*)(pScn - 8);
                        //*(mmUInt32_t*)(pScn + 4) = *(mmUInt32_t*)(pScn - 4);
                        // pScn sometime is odd number when p->loopy_shift is odd number, 
                        // and some system odd number transform to uint32 pointer will crash.
                        mmMemcpy(pScn + 0, pScn - 8, 8);
                        *(pBGw + 0) = *(pBGw - 1);
                    }
                    pScn += 8;
                    pBGw++;

                    if (++ntbl_x == 32)
                    {
                        ntbl_x = 0;
                        ntbladr ^= 0x41F;
                    }
                    else
                    {
                        ntbladr++;
                    }
                }
            }
        }
        else
        {
            if (!p->bExtLatch)
            {
                // Without Extension Latch
                if (!p->bExtNameTable)
                {
                    mmInt_t i = 0;

                    mmByte_t* pScn = p->lpScanline + (8 - p->loopy_shift);
                    mmByte_t* pBGw = BGwrite;

                    mmInt_t ntbladr = 0x2000 + (mmu->loopy_v & 0x0FFF);
                    mmInt_t attradr = 0x03C0 + ((mmu->loopy_v & 0x0380) >> 4);
                    mmInt_t ntbl_x = ntbladr & 0x001F;
                    mmInt_t attrsft = (ntbladr & 0x0040) >> 4;
                    mmByte_t* pNTBL = PPU_MEM_BANK[ntbladr >> 10];

                    mmInt_t tileadr;
                    mmInt_t cache_tile = 0xFFFF0000;
                    mmByte_t cache_attr = 0xFF;
                    // mmByte_t cache_mono = 0x00;

                    mmByte_t chr_h, chr_l, attr;

                    for (i = 0; i < 33; i++)
                    {
                        tileadr = ((mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) << 8) + pNTBL[ntbladr & 0x03FF] * 0x10 + p->loopy_y;

                        if (i != 0)
                        {
                            mmEmuNes_EmulationCPU(nes, MM_EMU_NES_FETCH_CYCLES * 4);
                        }

                        attr = ((pNTBL[attradr + (ntbl_x >> 2)] >> ((ntbl_x & 2) + attrsft)) & 3) << 2;

                        if (cache_tile != tileadr || cache_attr != attr)
                        {
                            mmByte_t* pBGPAL = NULL;

                            cache_tile = tileadr;
                            cache_attr = attr;

                            chr_l = PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
                            chr_h = PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
                            *pBGw = chr_l | chr_h;

                            pBGPAL = &mmu->BGPAL[attr];
                            {
                                register mmInt_t c1 = ((chr_l >> 1) & 0x55) | (chr_h & 0xAA);
                                register mmInt_t c2 = (chr_l & 0x55) | ((chr_h << 1) & 0xAA);
                                pScn[0] = pBGPAL[(c1 >> 6)    ];
                                pScn[1] = pBGPAL[(c2 >> 6)    ];
                                pScn[2] = pBGPAL[(c1 >> 4) & 3];
                                pScn[3] = pBGPAL[(c2 >> 4) & 3];
                                pScn[4] = pBGPAL[(c1 >> 2) & 3];
                                pScn[5] = pBGPAL[(c2 >> 2) & 3];
                                pScn[6] = pBGPAL[(c1     ) & 3];
                                pScn[7] = pBGPAL[(c2     ) & 3];
                            }
                        }
                        else
                        {
                            //*(mmUInt32_t*)(pScn + 0) = *(mmUInt32_t*)(pScn - 8);
                            //*(mmUInt32_t*)(pScn + 4) = *(mmUInt32_t*)(pScn - 4);
                            // pScn sometime is odd number when p->loopy_shift is odd number, 
                            // and some system odd number transform to uint32 pointer will crash.
                            mmMemcpy(pScn + 0, pScn - 8, 8);
                            *(pBGw + 0) = *(pBGw - 1);
                        }
                        pScn += 8;
                        pBGw++;

                        // Character latch(For MMC2/MMC4)
                        if (p->bChrLatch)
                        {
                            (*(nes->mapper->PPUChrLatch))(nes->mapper, tileadr);
                        }

                        if (++ntbl_x == 32)
                        {
                            ntbl_x = 0;
                            ntbladr ^= 0x41F;
                            attradr = 0x03C0 + ((ntbladr & 0x0380) >> 4);
                            pNTBL = PPU_MEM_BANK[ntbladr >> 10];
                        }
                        else
                        {
                            ntbladr++;
                        }
                    }
                }
                else
                {
                    mmInt_t i = 0;

                    mmByte_t* pScn = p->lpScanline + (8 - p->loopy_shift);
                    mmByte_t* pBGw = BGwrite;

                    mmInt_t ntbladr;
                    mmInt_t tileadr;
                    mmInt_t cache_tile = 0xFFFF0000;
                    mmByte_t cache_attr = 0xFF;
                    // mmByte_t cache_mono = 0x00;

                    mmByte_t chr_h, chr_l, attr;

                    mmWord_t loopy_v_tmp = mmu->loopy_v;

                    for (i = 0; i < 33; i++)
                    {
                        if (i != 0)
                        {
                            mmEmuNes_EmulationCPU(nes, MM_EMU_NES_FETCH_CYCLES * 4);
                        }

                        ntbladr = 0x2000 + (mmu->loopy_v & 0x0FFF);

                        tileadr = 
                            ((mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) << 8) + 
                            PPU_MEM_BANK[ntbladr >> 10][ntbladr & 0x03FF] * 0x10 + 
                            ((mmu->loopy_v & 0x7000) >> 12);

                        attr = 
                            ((PPU_MEM_BANK[ntbladr >> 10][0x03C0 + ((ntbladr & 0x0380) >> 4) + 
                            ((ntbladr & 0x001C) >> 2)] >> (((ntbladr & 0x40) >> 4) + (ntbladr & 0x02))) & 3) << 2;

                        if (cache_tile != tileadr || cache_attr != attr)
                        {
                            mmByte_t* pBGPAL = NULL;

                            cache_tile = tileadr;
                            cache_attr = attr;

                            chr_l = PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
                            chr_h = PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
                            *pBGw = chr_l | chr_h;

                            pBGPAL = &mmu->BGPAL[attr];
                            {
                                register mmInt_t c1 = ((chr_l >> 1) & 0x55) | (chr_h & 0xAA);
                                register mmInt_t c2 = (chr_l & 0x55) | ((chr_h << 1) & 0xAA);
                                pScn[0] = pBGPAL[(c1 >> 6)    ];
                                pScn[1] = pBGPAL[(c2 >> 6)    ];
                                pScn[2] = pBGPAL[(c1 >> 4) & 3];
                                pScn[3] = pBGPAL[(c2 >> 4) & 3];
                                pScn[4] = pBGPAL[(c1 >> 2) & 3];
                                pScn[5] = pBGPAL[(c2 >> 2) & 3];
                                pScn[6] = pBGPAL[(c1     ) & 3];
                                pScn[7] = pBGPAL[(c2     ) & 3];
                            }
                        }
                        else
                        {
                            //*(mmUInt32_t*)(pScn + 0) = *(mmUInt32_t*)(pScn - 8);
                            //*(mmUInt32_t*)(pScn + 4) = *(mmUInt32_t*)(pScn - 4);
                            // pScn sometime is odd number when p->loopy_shift is odd number, 
                            // and some system odd number transform to uint32 pointer will crash.
                            mmMemcpy(pScn + 0, pScn - 8, 8);
                            *(pBGw + 0) = *(pBGw - 1);
                        }
                        pScn += 8;
                        pBGw++;

                        // Character latch(For MMC2/MMC4)
                        if (p->bChrLatch)
                        {
                            (*(nes->mapper->PPUChrLatch))(nes->mapper, tileadr);
                        }

                        if ((mmu->loopy_v & 0x1F) == 0x1F)
                        {
                            mmu->loopy_v ^= 0x041F;
                        }
                        else
                        {
                            mmu->loopy_v++;
                        }
                    }
                    mmu->loopy_v = loopy_v_tmp;
                }
            }
            else
            {
                mmInt_t i = 0;
                // With Extension Latch(For MMC5)
                mmByte_t* pScn = p->lpScanline + (8 - p->loopy_shift);
                mmByte_t* pBGw = BGwrite;

                mmInt_t ntbladr = 0x2000 + (mmu->loopy_v & 0x0FFF);
                mmInt_t ntbl_x = ntbladr & 0x1F;

                mmInt_t cache_tile = 0xFFFF0000;
                mmByte_t cache_attr = 0xFF;

                mmByte_t chr_h, chr_l, attr, exattr;

                for (i = 0; i < 33; i++)
                {
                    if (i != 0)
                    {
                        mmEmuNes_EmulationCPU(nes, MM_EMU_NES_FETCH_CYCLES * 4);
                    }
                    (*(nes->mapper->PPUExtLatchX))(nes->mapper, i);
                    (*(nes->mapper->PPUExtLatch))(nes->mapper, ntbladr, &chr_l, &chr_h, &exattr);
                    attr = exattr & 0x0C;

                    if (cache_tile != (((mmInt_t)chr_h << 8) + (mmInt_t)chr_l) || cache_attr != attr)
                    {
                        mmByte_t* pBGPAL = NULL;

                        cache_tile = (((mmInt_t)chr_h << 8) + (mmInt_t)chr_l);
                        cache_attr = attr;
                        *pBGw = chr_l | chr_h;

                        pBGPAL = &mmu->BGPAL[attr];
                        {
                            register mmInt_t c1 = ((chr_l >> 1) & 0x55) | (chr_h & 0xAA);
                            register mmInt_t c2 = (chr_l & 0x55) | ((chr_h << 1) & 0xAA);
                            pScn[0] = pBGPAL[(c1 >> 6)    ];
                            pScn[1] = pBGPAL[(c2 >> 6)    ];
                            pScn[2] = pBGPAL[(c1 >> 4) & 3];
                            pScn[3] = pBGPAL[(c2 >> 4) & 3];
                            pScn[4] = pBGPAL[(c1 >> 2) & 3];
                            pScn[5] = pBGPAL[(c2 >> 2) & 3];
                            pScn[6] = pBGPAL[(c1     ) & 3];
                            pScn[7] = pBGPAL[(c2     ) & 3];
                        }
                    }
                    else
                    {
                        //*(mmUInt32_t*)(pScn + 0) = *(mmUInt32_t*)(pScn - 8);
                        //*(mmUInt32_t*)(pScn + 4) = *(mmUInt32_t*)(pScn - 4);
                        // pScn sometime is odd number when p->loopy_shift is odd number, 
                        // and some system odd number transform to uint32 pointer will crash.
                        mmMemcpy(pScn + 0, pScn - 8, 8);
                        *(pBGw + 0) = *(pBGw - 1);
                    }
                    pScn += 8;
                    pBGw++;

                    if (++ntbl_x == 32)
                    {
                        ntbl_x = 0;
                        ntbladr ^= 0x41F;
                    }
                    else
                    {
                        ntbladr++;
                    }
                }
            }
        }
        if (!(mmu->PPUREG[1] & MM_EMU_PPU_BGCLIP_BIT) && bLeftClip)
        {
            mmByte_t* pScn = p->lpScanline + 8;
            mmMemset(pScn, mmu->BGPAL[0], 8);
        }
    }

    // Render sprites
    mmu->PPUREG[2] &= ~MM_EMU_PPU_SPMAX_FLAG;

    // Cancel if it is outside the display period.
    if (scanline > 239)
    {
        return;
    }

    if (!(mmu->PPUREG[1] & MM_EMU_PPU_SPDISP_BIT))
    {
        return;
    }

    {
        mmInt_t i = 0;

        mmByte_t SPwrite[33 + 1];
        mmInt_t spmax;
        mmInt_t spraddr, sp_y, sp_h;
        mmByte_t chr_h, chr_l;
        struct mmEmuPpuSprite* sp = NULL;

        mmByte_t* pBGw = BGwrite;
        mmByte_t* pSPw = SPwrite;
        mmByte_t* pBit2Rev = p->Bit2Rev;

        mmByte_t SPpat = 0;
        mmInt_t SPpos = 0;
        mmInt_t SPsft = 0;
        mmByte_t SPmsk = 0;
        mmWord_t SPwrt = 0;

        mmByte_t* pSPPAL = NULL;
        mmByte_t* pScn = NULL;

        mmMemset(SPwrite, 0, sizeof(mmByte_t) * (33 + 1));

        spmax = 0;
        sp = (struct mmEmuPpuSprite*)mmu->SPRAM;
        sp_h = (mmu->PPUREG[0] & MM_EMU_PPU_SP16_BIT) ? 15 : 7;

        // Left clip
        if (!(mmu->PPUREG[1] & MM_EMU_PPU_SPCLIP_BIT) && bLeftClip)
        {
            SPwrite[0] = 0xFF;
        }

        for (i = 0; i < 64; i++, sp++)
        {
            sp_y = scanline - (sp->y + 1);
            // Check if SPRITE exists in scan line.
            if (sp_y != (sp_y & sp_h))
            {
                continue;
            }

            if (!(mmu->PPUREG[0] & MM_EMU_PPU_SP16_BIT))
            {
                // 8x8 Sprite
                spraddr = (((mmInt_t)mmu->PPUREG[0] & MM_EMU_PPU_SPTBL_BIT) << 9) + ((mmInt_t)sp->tile << 4);
                if (!(sp->attr&MM_EMU_PPU_SP_VMIRROR_BIT))
                {
                    spraddr += sp_y;
                }
                else
                {
                    spraddr += 7 - sp_y;
                }
            }
            else
            {
                // 8x16 Sprite
                spraddr = (((mmInt_t)sp->tile & 1) << 12) + (((mmInt_t)sp->tile & 0xFE) << 4);
                if (!(sp->attr&MM_EMU_PPU_SP_VMIRROR_BIT))
                {
                    spraddr += ((sp_y & 8) << 1) + (sp_y & 7);
                }
                else
                {
                    spraddr += ((~sp_y & 8) << 1) + (7 - (sp_y & 7));
                }
            }
            // Character pattern
            chr_l = PPU_MEM_BANK[spraddr >> 10][spraddr & 0x3FF];
            chr_h = PPU_MEM_BANK[spraddr >> 10][(spraddr & 0x3FF) + 8];

            // Character latch(For MMC2/MMC4)
            if (p->bChrLatch)
            {
                (*(nes->mapper->PPUChrLatch))(nes->mapper, spraddr);
            }

            // pattern mask
            if (sp->attr & MM_EMU_PPU_SP_HMIRROR_BIT)
            {
                chr_l = pBit2Rev[chr_l];
                chr_h = pBit2Rev[chr_h];
            }

            SPpat = chr_l | chr_h;

            // Sprite hitcheck
            if (i == 0 && !(mmu->PPUREG[2] & MM_EMU_PPU_SPHIT_FLAG))
            {
                mmInt_t BGpos = ((sp->x & 0xF8) + ((p->loopy_shift + (sp->x & 7)) & 8)) >> 3;
                mmInt_t BGsft = 8 - ((p->loopy_shift + sp->x) & 7);
                mmByte_t BGmsk = (((mmWord_t)pBGw[BGpos + 0] << 8) | (mmWord_t)pBGw[BGpos + 1]) >> BGsft;

                if (SPpat & BGmsk)
                {
                    mmu->PPUREG[2] |= MM_EMU_PPU_SPHIT_FLAG;
                }
            }

            // Sprite mask
            SPpos = sp->x / 8;
            SPsft = 8 - (sp->x & 7);
            SPmsk = (((mmWord_t)pSPw[SPpos + 0] << 8) | (mmWord_t)pSPw[SPpos + 1]) >> SPsft;
            SPwrt = (mmWord_t)SPpat << SPsft;

            pSPw[SPpos + 0] |= SPwrt >> 8;
            pSPw[SPpos + 1] |= SPwrt & 0xFF;
            SPpat &= ~SPmsk;

            if (sp->attr & MM_EMU_PPU_SP_PRIORITY_BIT)
            {
                // BG > SP priority
                mmInt_t BGpos = ((sp->x & 0xF8) + ((p->loopy_shift + (sp->x & 7)) & 8)) >> 3;
                mmInt_t BGsft = 8 - ((p->loopy_shift + sp->x) & 7);
                mmWord_t BGmsk = (((mmWord_t)pBGw[BGpos + 0] << 8) | (mmWord_t)pBGw[BGpos + 1]) >> BGsft;

                SPpat &= ~BGmsk;
            }
            // Attribute
            pSPPAL = &mmu->SPPAL[(sp->attr&MM_EMU_PPU_SP_COLOR_BIT) << 2];
            // Ptr
            pScn = p->lpScanline + sp->x + 8;

            if (!p->bExtMono)
            {
                register mmInt_t c1 = ((chr_l >> 1) & 0x55) | (chr_h & 0xAA);
                register mmInt_t c2 = (chr_l & 0x55) | ((chr_h << 1) & 0xAA);
                if (SPpat & 0x80) pScn[0] = pSPPAL[(c1 >> 6)    ];
                if (SPpat & 0x40) pScn[1] = pSPPAL[(c2 >> 6)    ];
                if (SPpat & 0x20) pScn[2] = pSPPAL[(c1 >> 4) & 3];
                if (SPpat & 0x10) pScn[3] = pSPPAL[(c2 >> 4) & 3];
                if (SPpat & 0x08) pScn[4] = pSPPAL[(c1 >> 2) & 3];
                if (SPpat & 0x04) pScn[5] = pSPPAL[(c2 >> 2) & 3];
                if (SPpat & 0x02) pScn[6] = pSPPAL[(c1     ) & 3];
                if (SPpat & 0x01) pScn[7] = pSPPAL[(c2     ) & 3];
            }
            else
            {
                // Monocrome effect (for Final Fantasy)
                mmByte_t mono = BGmono[((sp->x & 0xF8) + ((p->loopy_shift + (sp->x & 7)) & 8)) >> 3];

                register mmInt_t c1 = ((chr_l >> 1) & 0x55) | (chr_h & 0xAA);
                register mmInt_t c2 = (chr_l & 0x55) | ((chr_h << 1) & 0xAA);
                if (SPpat & 0x80) pScn[0] = pSPPAL[(c1 >> 6)    ] | mono;
                if (SPpat & 0x40) pScn[1] = pSPPAL[(c2 >> 6)    ] | mono;
                if (SPpat & 0x20) pScn[2] = pSPPAL[(c1 >> 4) & 3] | mono;
                if (SPpat & 0x10) pScn[3] = pSPPAL[(c2 >> 4) & 3] | mono;
                if (SPpat & 0x08) pScn[4] = pSPPAL[(c1 >> 2) & 3] | mono;
                if (SPpat & 0x04) pScn[5] = pSPPAL[(c2 >> 2) & 3] | mono;
                if (SPpat & 0x02) pScn[6] = pSPPAL[(c1     ) & 3] | mono;
                if (SPpat & 0x01) pScn[7] = pSPPAL[(c2     ) & 3] | mono;
            }

            if (++spmax > 8 - 1)
            {
                if (!bMax)
                {
                    break;
                }
            }
        }
        if (spmax > 8 - 1)
        {
            mmu->PPUREG[2] |= MM_EMU_PPU_SPMAX_FLAG;
        }
    }
}
MM_EXPORT_EMU void mmEmuPpu_DummyScanline(struct mmEmuPpu* p, mmInt_t scanline)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i;
    mmInt_t spmax;
    mmInt_t sp_h;
    struct mmEmuPpuSprite* sp = NULL;

    mmu->PPUREG[2] &= ~MM_EMU_PPU_SPMAX_FLAG;

    // Cancel sprite hidden.
    if (!(mmu->PPUREG[1] & MM_EMU_PPU_SPDISP_BIT))
    {
        return;
    }

    // Cancel if it is outside the display period.
    if (scanline < 0 || scanline > 239)
    {
        return;
    }

    sp = (struct mmEmuPpuSprite*)mmu->SPRAM;
    sp_h = (mmu->PPUREG[0] & MM_EMU_PPU_SP16_BIT) ? 15 : 7;

    spmax = 0;
    // Sprite Max check
    for (i = 0; i < 64; i++, sp++)
    {
        // Check if SPRITE exists in scan line.
        if ((scanline < (mmInt_t)sp->y + 1) || (scanline > ((mmInt_t)sp->y + sp_h + 1)))
        {
            continue;
        }

        if (++spmax > 8 - 1)
        {
            mmu->PPUREG[2] |= MM_EMU_PPU_SPMAX_FLAG;
            break;
        }
    }
}

MM_EXPORT_EMU mmBool_t mmEmuPpu_IsDispON(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[1] & (MM_EMU_PPU_BGDISP_BIT | MM_EMU_PPU_SPDISP_BIT);
}
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsBGON(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[1] & MM_EMU_PPU_BGDISP_BIT;
}
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSPON(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[1] & MM_EMU_PPU_SPDISP_BIT;
}

MM_EXPORT_EMU mmByte_t mmEmuPpu_GetBGCOLOR(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return (mmu->PPUREG[1] & MM_EMU_PPU_BGCOLOR_BIT) >> 5;
}

MM_EXPORT_EMU mmBool_t mmEmuPpu_IsBGCLIP(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[1] & MM_EMU_PPU_BGCLIP_BIT;
}
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSPCLIP(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[1] & MM_EMU_PPU_SPCLIP_BIT;
}

MM_EXPORT_EMU mmBool_t mmEmuPpu_IsMONOCROME(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[1] & MM_EMU_PPU_COLORMODE_BIT;
}

MM_EXPORT_EMU mmBool_t mmEmuPpu_IsVBLANK(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[2] & MM_EMU_PPU_VBLANK_FLAG;
}
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSPHIT(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[2] & MM_EMU_PPU_SPHIT_FLAG;
}
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSPMAX(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[2] & MM_EMU_PPU_SPMAX_FLAG;
}
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsPPUWE(const struct mmEmuPpu* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    return mmu->PPUREG[2] & MM_EMU_PPU_WENABLE_FLAG;
}

// Line that Sprite 0 might hit?
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSprite0(const struct mmEmuPpu* p, mmInt_t scanline)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // Cancel (do not hit) sprite or BG non-display.
    if ((mmu->PPUREG[1] & (MM_EMU_PPU_SPDISP_BIT | MM_EMU_PPU_BGDISP_BIT)) != (MM_EMU_PPU_SPDISP_BIT | MM_EMU_PPU_BGDISP_BIT))
    {
        return MM_FALSE;
    }

    // Cancel if already hit.
    if (mmu->PPUREG[2] & MM_EMU_PPU_SPHIT_FLAG)
    {
        return MM_FALSE;
    }

    if (!(mmu->PPUREG[0] & MM_EMU_PPU_SP16_BIT))
    {
        // 8x8
        if ((scanline < (mmInt_t)mmu->SPRAM[0] + 1) || (scanline > ((mmInt_t)mmu->SPRAM[0] + 7 + 1)))
        {
            return MM_FALSE;
        }
    }
    else
    {
        // 8x16
        if ((scanline < (mmInt_t)mmu->SPRAM[0] + 1) || (scanline > ((mmInt_t)mmu->SPRAM[0] + 15 + 1)))
        {
            return MM_FALSE;
        }
    }

    return MM_TRUE;
}

static void __static_mmEmuPpu_HorizontalInvertedMaskTable(struct mmEmuPpu* p)
{
    mmInt_t i = 0;
    mmInt_t j = 0;
    // Horizontal inverted mask table.
    for (i = 0; i < 256; i++)
    {
        mmByte_t m = 0x80;
        mmByte_t c = 0;
        for (j = 0; j < 8; j++)
        {
            if (i&(1 << j))
            {
                c |= m;
            }
            m >>= 1;
        }
        p->Bit2Rev[i] = c;
    }
}

