/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper034.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper034_Init(struct mmEmuMapper034* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper034_Reset;
    p->super.Write = &mmEmuMapper034_Write;
    //p->super.Read = &mmEmuMapper034_Read;
    //p->super.ReadLow = &mmEmuMapper034_ReadLow;
    p->super.WriteLow = &mmEmuMapper034_WriteLow;
    //p->super.ExRead = &mmEmuMapper034_ExRead;
    //p->super.ExWrite = &mmEmuMapper034_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper034_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper034_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper034_HSync;
    //p->super.VSync = &mmEmuMapper034_VSync;
    //p->super.Clock = &mmEmuMapper034_Clock;
    //p->super.PPULatch = &mmEmuMapper034_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper034_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper034_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper034_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper034_IsStateSave;
    //p->super.SaveState = &mmEmuMapper034_SaveState;
    //p->super.LoadState = &mmEmuMapper034_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper034_Destroy(struct mmEmuMapper034* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper034_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper034* p = (struct mmEmuMapper034*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper034_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper034* p = (struct mmEmuMapper034*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x7FFD:
        mmEmuMmu_SetPROM32KBank(mmu, data);
        break;
    case 0x7FFE:
        mmEmuMmu_SetVROM4KBank(mmu, 0, data);
        break;
    case 0x7FFF:
        mmEmuMmu_SetVROM4KBank(mmu, 4, data);
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper034_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper034* p = (struct mmEmuMapper034*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, data);
}

static struct mmEmuMapper* __static_mmEmuMapper034_Produce(void)
{
    struct mmEmuMapper034* p = (struct mmEmuMapper034*)mmMalloc(sizeof(struct mmEmuMapper034));
    mmEmuMapper034_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper034_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper034* p = (struct mmEmuMapper034*)(m);
    mmEmuMapper034_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER034 =
{
    &__static_mmEmuMapper034_Produce,
    &__static_mmEmuMapper034_Recycle,
};
