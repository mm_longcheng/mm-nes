/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuWaveRecord_h__
#define __mmEmuWaveRecord_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( push, 1 )
#endif

struct mmEmuWaveFile
{
    mmChar_t        tagRIFF[4];     // "RIFF"
    mmUInt32_t      dwFileSize;
    mmChar_t        tagTYPE[8];     // "WAVEfmt "
    mmUInt32_t      dwChunkOffset;
    mmWord_t        wCodingType;
    mmWord_t        wChannel;
    mmUInt32_t      dwSample;
    mmUInt32_t      dwBytesPerSec;
    mmWord_t        wBytesPerSample;
    mmWord_t        wBits;
    mmChar_t        tagDATA[4];     // "data"
    mmUInt32_t      dwDataSize;
};

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( pop )
#endif

struct mmEmuWaveRecord
{
    struct mmEmuWaveFile wavefile;
    FILE*                fp;
};

MM_EXPORT_EMU void mmEmuWaveRecord_Init(struct mmEmuWaveRecord* p);
MM_EXPORT_EMU void mmEmuWaveRecord_Destroy(struct mmEmuWaveRecord* p);

MM_EXPORT_EMU void mmEmuWaveRecord_Start(struct mmEmuWaveRecord* p, const char* szFile, mmInt_t nSample, mmInt_t nBits, mmBool_t bStereo);
MM_EXPORT_EMU void mmEmuWaveRecord_Stop(struct mmEmuWaveRecord* p);

MM_EXPORT_EMU mmBool_t mmEmuWaveRecord_IsWaveRecord(const struct mmEmuWaveRecord* p);

MM_EXPORT_EMU void mmEmuWaveRecord_Output(struct mmEmuWaveRecord* p, void* lpBuf, mmUInt32_t dwSize);

#include "core/mmSuffix.h"

#endif//__mmEmuWaveRecord_h__
