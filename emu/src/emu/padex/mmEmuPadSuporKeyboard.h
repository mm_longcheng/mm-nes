/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuPadSuporKeyboard_h__
#define __mmEmuPadSuporKeyboard_h__

#include "core/mmCore.h"

#include "emu/mmEmuExpad.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Supor Keyboard                                                       //
//----------------------------------------------------------------------//
struct mmEmuPadSuporKeyboard
{
    struct mmEmuExpad super;

    mmBool_t    bGraph;
    mmBool_t    bOut;
    mmByte_t    ScanNo;
};

MM_EXPORT_EMU void mmEmuPadSuporKeyboard_Init(struct mmEmuPadSuporKeyboard* p);
MM_EXPORT_EMU void mmEmuPadSuporKeyboard_Destroy(struct mmEmuPadSuporKeyboard* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuPadSuporKeyboard_Reset(struct mmEmuExpad* super);

MM_EXPORT_EMU mmByte_t mmEmuPadSuporKeyboard_Read4016(struct mmEmuExpad* super);
MM_EXPORT_EMU mmByte_t mmEmuPadSuporKeyboard_Read4017(struct mmEmuExpad* super);
MM_EXPORT_EMU void mmEmuPadSuporKeyboard_Write4016(struct mmEmuExpad* super, mmByte_t data);

MM_EXPORT_EMU extern const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_SUPORKEYBOARD;

#include "core/mmSuffix.h"

#endif//__mmEmuPadSuporKeyboard_h__
