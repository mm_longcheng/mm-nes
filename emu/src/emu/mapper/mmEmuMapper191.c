/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper191.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper191_Init(struct mmEmuMapper191* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;

    p->highbank = 0;

    //
    p->super.Reset = &mmEmuMapper191_Reset;
    //p->super.Write = &mmEmuMapper191_Write;
    //p->super.Read = &mmEmuMapper191_Read;
    //p->super.ReadLow = &mmEmuMapper191_ReadLow;
    p->super.WriteLow = &mmEmuMapper191_WriteLow;
    //p->super.ExRead = &mmEmuMapper191_ExRead;
    //p->super.ExWrite = &mmEmuMapper191_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper191_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper191_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper191_HSync;
    //p->super.VSync = &mmEmuMapper191_VSync;
    //p->super.Clock = &mmEmuMapper191_Clock;
    //p->super.PPULatch = &mmEmuMapper191_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper191_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper191_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper191_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper191_IsStateSave;
    p->super.SaveState = &mmEmuMapper191_SaveState;
    p->super.LoadState = &mmEmuMapper191_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper191_Destroy(struct mmEmuMapper191* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;

    p->highbank = 0;
}

MM_EXPORT_EMU void mmEmuMapper191_SetBankCPU(struct mmEmuMapper191* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, p->prg0);
}
MM_EXPORT_EMU void mmEmuMapper191_SetBankPPU(struct mmEmuMapper191* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM1KBank(mmu, 0, (((p->highbank << 3) + p->chr0) << 2) + 0);
        mmEmuMmu_SetVROM1KBank(mmu, 1, (((p->highbank << 3) + p->chr0) << 2) + 1);
        mmEmuMmu_SetVROM1KBank(mmu, 2, (((p->highbank << 3) + p->chr1) << 2) + 2);
        mmEmuMmu_SetVROM1KBank(mmu, 3, (((p->highbank << 3) + p->chr1) << 2) + 3);
        mmEmuMmu_SetVROM1KBank(mmu, 4, (((p->highbank << 3) + p->chr2) << 2) + 0);
        mmEmuMmu_SetVROM1KBank(mmu, 5, (((p->highbank << 3) + p->chr2) << 2) + 1);
        mmEmuMmu_SetVROM1KBank(mmu, 6, (((p->highbank << 3) + p->chr3) << 2) + 2);
        mmEmuMmu_SetVROM1KBank(mmu, 7, (((p->highbank << 3) + p->chr3) << 2) + 3);
    }
}

MM_EXPORT_EMU void mmEmuMapper191_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper191* p = (struct mmEmuMapper191*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    //p->prg1 = 1;
    mmEmuMapper191_SetBankCPU(p);

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;
    p->highbank = 0;
    mmEmuMapper191_SetBankPPU(p);
}
MM_EXPORT_EMU void mmEmuMapper191_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper191* p = (struct mmEmuMapper191*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x4100:
        p->reg[0] = data;
        break;
    case 0x4101:
        p->reg[1] = data;
        switch (p->reg[0])
        {
        case 0:
            p->chr0 = data & 7;
            mmEmuMapper191_SetBankPPU(p);
            break;
        case 1:
            p->chr1 = data & 7;
            mmEmuMapper191_SetBankPPU(p);
            break;
        case 2:
            p->chr2 = data & 7;
            mmEmuMapper191_SetBankPPU(p);
            break;
        case 3:
            p->chr3 = data & 7;
            mmEmuMapper191_SetBankPPU(p);
            break;
        case 4:
            p->highbank = data & 7;
            mmEmuMapper191_SetBankPPU(p);
            break;
        case 5:
            p->prg0 = data & 7;
            mmEmuMapper191_SetBankCPU(p);
            break;
        case 7:
            if (data & 0x02)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper191_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper191_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper191* p = (struct mmEmuMapper191*)(super);

    buffer[0] = p->prg0;
    buffer[1] = p->chr0;
    buffer[2] = p->chr1;
    buffer[3] = p->chr2;
    buffer[4] = p->chr3;
    buffer[5] = p->highbank;
}
MM_EXPORT_EMU void mmEmuMapper191_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper191* p = (struct mmEmuMapper191*)(super);

    p->prg0 = buffer[0];
    p->chr0 = buffer[1];
    p->chr1 = buffer[2];
    p->chr2 = buffer[3];
    p->chr3 = buffer[4];
    p->highbank = buffer[5];
}

static struct mmEmuMapper* __static_mmEmuMapper191_Produce(void)
{
    struct mmEmuMapper191* p = (struct mmEmuMapper191*)mmMalloc(sizeof(struct mmEmuMapper191));
    mmEmuMapper191_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper191_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper191* p = (struct mmEmuMapper191*)(m);
    mmEmuMapper191_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER191 =
{
    &__static_mmEmuMapper191_Produce,
    &__static_mmEmuMapper191_Recycle,
};
