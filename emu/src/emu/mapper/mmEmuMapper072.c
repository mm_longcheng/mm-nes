/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper072.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuExtSoundFile.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuMapper072_Init(struct mmEmuMapper072* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper072_Reset;
    p->super.Write = &mmEmuMapper072_Write;
    //p->super.Read = &mmEmuMapper072_Read;
    //p->super.ReadLow = &mmEmuMapper072_ReadLow;
    //p->super.WriteLow = &mmEmuMapper072_WriteLow;
    //p->super.ExRead = &mmEmuMapper072_ExRead;
    //p->super.ExWrite = &mmEmuMapper072_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper072_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper072_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper072_HSync;
    //p->super.VSync = &mmEmuMapper072_VSync;
    //p->super.Clock = &mmEmuMapper072_Clock;
    //p->super.PPULatch = &mmEmuMapper072_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper072_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper072_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper072_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper072_IsStateSave;
    //p->super.SaveState = &mmEmuMapper072_SaveState;
    //p->super.LoadState = &mmEmuMapper072_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper072_Destroy(struct mmEmuMapper072* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper072_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper072* p = (struct mmEmuMapper072*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper072_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper072* p = (struct mmEmuMapper072*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    struct mmEmuAudioConfig* audio_config = &nes->config.audio;

    if (data & 0x80)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, data & 0x0F);
    }
    else if (data & 0x40)
    {
        mmEmuMmu_SetVROM8KBank(mmu, data & 0x0F);
    }
    else
    {
        if (addr >= 0xC100 && addr <= 0xC11F && data == 0x20)
        {
            //struct mmLogger* gLogger = mmLogger_Instance();
            //
            //DEBUGOUT( "ADDR:%04X DATA:%02X\n", addr, data );
            //mmLogger_LogD(gLogger, "%s %d SOUND CODE:%02X", __FUNCTION__, __LINE__, addr & 0x1F);

            // Should it be an OSD ...
            if (audio_config->bExtraSoundEnable)
            {
                mmEmuNes_EsfAllStop(nes);
                mmEmuNes_EsfPlay(nes, MM_EMU_ESF_MOETENNIS_00 + (addr & 0x1F));
            }
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper072_Produce(void)
{
    struct mmEmuMapper072* p = (struct mmEmuMapper072*)mmMalloc(sizeof(struct mmEmuMapper072));
    mmEmuMapper072_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper072_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper072* p = (struct mmEmuMapper072*)(m);
    mmEmuMapper072_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER072 =
{
    &__static_mmEmuMapper072_Produce,
    &__static_mmEmuMapper072_Recycle,
};
