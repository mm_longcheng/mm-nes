/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApuFds_h__
#define __mmEmuApuFds_h__

#include "core/mmCore.h"

#include "emu/mmEmuApuInterface.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuApuFdsSound
{
    mmByte_t    reg[0x80];

    mmByte_t    volenv_mode;        // Volume Envelope
    mmByte_t    volenv_gain;
    mmByte_t    volenv_decay;
    double      volenv_phaseacc;

    mmByte_t    swpenv_mode;        // Sweep Envelope
    mmByte_t    swpenv_gain;
    mmByte_t    swpenv_decay;
    double      swpenv_phaseacc;

    // For envelope unit
    mmByte_t    envelope_enable;    // $4083 bit6
    mmByte_t    envelope_speed;     // $408A

    // For $4089
    mmByte_t    wave_setup;         // bit7
    mmInt_t     master_volume;      // bit1-0

    // For Main unit
    mmInt_t     main_wavetable[64];
    mmByte_t    main_enable;
    mmInt_t     main_frequency;
    mmInt_t     main_addr;

    // For Effector(LFO) unit
    mmByte_t    lfo_wavetable[64];
    mmByte_t    lfo_enable;         // 0:Enable 1:Wavetable setup
    mmInt_t     lfo_frequency;
    mmInt_t     lfo_addr;
    double      lfo_phaseacc;

    // For Sweep unit
    mmInt_t     sweep_bias;

    // Misc
    mmInt_t     now_volume;
    mmInt_t     now_freq;
    mmInt_t     output;
};

struct mmEmuApuFds
{
    struct mmEmuApuInterface super;

    struct mmEmuApuFdsSound fds;
    struct mmEmuApuFdsSound fds_sync;

    mmInt_t sampling_rate;
    mmInt_t output_buf[8];
};

MM_EXPORT_EMU void mmEmuApuFds_Init(struct mmEmuApuFds* p);
MM_EXPORT_EMU void mmEmuApuFds_Destroy(struct mmEmuApuFds* p);

// Write Sub
MM_EXPORT_EMU void mmEmuApuFds_WriteSub(struct mmEmuApuFds* p, mmWord_t addr, mmByte_t data, struct mmEmuApuFdsSound* ch, double rate);

// virtual function for super.
MM_EXPORT_EMU void mmEmuApuFds_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuFds_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuFds_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuApuFds_Read(struct mmEmuApuInterface* super, mmWord_t addr);
MM_EXPORT_EMU mmInt_t mmEmuApuFds_Process(struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU void mmEmuApuFds_SyncWrite(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuApuFds_SyncRead(struct mmEmuApuInterface* super, mmWord_t addr);
MM_EXPORT_EMU mmBool_t mmEmuApuFds_Sync(struct mmEmuApuInterface* super, mmInt_t cycles);

MM_EXPORT_EMU mmInt_t mmEmuApuFds_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuFds_GetStateSize(const struct mmEmuApuInterface* super);
MM_EXPORT_EMU void mmEmuApuFds_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuApuFds_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer);

#include "core/mmSuffix.h"

#endif//__mmEmuApuFds_h__
