/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper201.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper201_Init(struct mmEmuMapper201* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper201_Reset;
    p->super.Write = &mmEmuMapper201_Write;
    //p->super.Read = &mmEmuMapper201_Read;
    //p->super.ReadLow = &mmEmuMapper201_ReadLow;
    //p->super.WriteLow = &mmEmuMapper201_WriteLow;
    //p->super.ExRead = &mmEmuMapper201_ExRead;
    //p->super.ExWrite = &mmEmuMapper201_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper201_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper201_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper201_HSync;
    //p->super.VSync = &mmEmuMapper201_VSync;
    //p->super.Clock = &mmEmuMapper201_Clock;
    //p->super.PPULatch = &mmEmuMapper201_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper201_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper201_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper201_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper201_IsStateSave;
    //p->super.SaveState = &mmEmuMapper201_SaveState;
    //p->super.LoadState = &mmEmuMapper201_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper201_Destroy(struct mmEmuMapper201* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper201_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper201* p = (struct mmEmuMapper201*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmEmuMmu_SetPROM32KBank(mmu, 0, 1, mmu->PROM_08K_SIZE-2, mmu->PROM_08K_SIZE-1);
    mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
    mmEmuMmu_SetPROM16KBank(mmu, 6, 0);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper201_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper201* p = (struct mmEmuMapper201*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t bank = (mmByte_t)addr & 0x03;
    if (!(addr & 0x08))
    {
        bank = 0;
    }
    mmEmuMmu_SetPROM32KBank(mmu, bank);
    mmEmuMmu_SetVROM8KBank(mmu, bank);
}

static struct mmEmuMapper* __static_mmEmuMapper201_Produce(void)
{
    struct mmEmuMapper201* p = (struct mmEmuMapper201*)mmMalloc(sizeof(struct mmEmuMapper201));
    mmEmuMapper201_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper201_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper201* p = (struct mmEmuMapper201*)(m);
    mmEmuMapper201_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER201 =
{
    &__static_mmEmuMapper201_Produce,
    &__static_mmEmuMapper201_Recycle,
};
