/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper075.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper075_Init(struct mmEmuMapper075* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 1;

    //
    p->super.Reset = &mmEmuMapper075_Reset;
    p->super.Write = &mmEmuMapper075_Write;
    //p->super.Read = &mmEmuMapper075_Read;
    //p->super.ReadLow = &mmEmuMapper075_ReadLow;
    //p->super.WriteLow = &mmEmuMapper075_WriteLow;
    //p->super.ExRead = &mmEmuMapper075_ExRead;
    //p->super.ExWrite = &mmEmuMapper075_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper075_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper075_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper075_HSync;
    //p->super.VSync = &mmEmuMapper075_VSync;
    //p->super.Clock = &mmEmuMapper075_Clock;
    //p->super.PPULatch = &mmEmuMapper075_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper075_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper075_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper075_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper075_IsStateSave;
    p->super.SaveState = &mmEmuMapper075_SaveState;
    p->super.LoadState = &mmEmuMapper075_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper075_Destroy(struct mmEmuMapper075* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 1;
}

MM_EXPORT_EMU void mmEmuMapper075_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper075* p = (struct mmEmuMapper075*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->reg[0] = 0;
    p->reg[1] = 1;
}
MM_EXPORT_EMU void mmEmuMapper075_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper075* p = (struct mmEmuMapper075*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF000)
    {
    case 0x8000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;

    case 0x9000:
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }

        p->reg[0] = (p->reg[0] & 0x0F) | ((data & 0x02) << 3);
        p->reg[1] = (p->reg[1] & 0x0F) | ((data & 0x04) << 2);
        mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[0]);
        mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[1]);
        break;

    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0xC000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;

    case 0xE000:
        p->reg[0] = (p->reg[0] & 0x10) | (data & 0x0F);
        mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[0]);
        break;

    case 0xF000:
        p->reg[1] = (p->reg[1] & 0x10) | (data & 0x0F);
        mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[1]);
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper075_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper075_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper075* p = (struct mmEmuMapper075*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
}
MM_EXPORT_EMU void mmEmuMapper075_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper075* p = (struct mmEmuMapper075*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper075_Produce(void)
{
    struct mmEmuMapper075* p = (struct mmEmuMapper075*)mmMalloc(sizeof(struct mmEmuMapper075));
    mmEmuMapper075_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper075_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper075* p = (struct mmEmuMapper075*)(m);
    mmEmuMapper075_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER075 =
{
    &__static_mmEmuMapper075_Produce,
    &__static_mmEmuMapper075_Recycle,
};
