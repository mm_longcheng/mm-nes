/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper079.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper079_Init(struct mmEmuMapper079* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper079_Reset;
    //p->super.Write = &mmEmuMapper079_Write;
    //p->super.Read = &mmEmuMapper079_Read;
    //p->super.ReadLow = &mmEmuMapper079_ReadLow;
    p->super.WriteLow = &mmEmuMapper079_WriteLow;
    //p->super.ExRead = &mmEmuMapper079_ExRead;
    //p->super.ExWrite = &mmEmuMapper079_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper079_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper079_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper079_HSync;
    //p->super.VSync = &mmEmuMapper079_VSync;
    //p->super.Clock = &mmEmuMapper079_Clock;
    //p->super.PPULatch = &mmEmuMapper079_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper079_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper079_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper079_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper079_IsStateSave;
    //p->super.SaveState = &mmEmuMapper079_SaveState;
    //p->super.LoadState = &mmEmuMapper079_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper079_Destroy(struct mmEmuMapper079* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper079_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper079* p = (struct mmEmuMapper079*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper079_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper079* p = (struct mmEmuMapper079*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr & 0x0100)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (data >> 3) & 0x01);
        mmEmuMmu_SetVROM8KBank(mmu, data & 0x07);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper079_Produce(void)
{
    struct mmEmuMapper079* p = (struct mmEmuMapper079*)mmMalloc(sizeof(struct mmEmuMapper079));
    mmEmuMapper079_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper079_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper079* p = (struct mmEmuMapper079*)(m);
    mmEmuMapper079_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER079 =
{
    &__static_mmEmuMapper079_Produce,
    &__static_mmEmuMapper079_Recycle,
};
