/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadKeyboard.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuPadKeyboard_Init(struct mmEmuPadKeyboard* p)
{
    mmEmuExpad_Init(&p->super);

    p->bGraph = MM_FALSE;
    p->bOut = MM_FALSE;
    p->ScanNo = 0;

    //
    p->super.Reset = &mmEmuPadKeyboard_Reset;
    //p->super.Strobe = &mmEmuPadKeyboard_Strobe;
    p->super.Read4016 = &mmEmuPadKeyboard_Read4016;
    p->super.Read4017 = &mmEmuPadKeyboard_Read4017;
    p->super.Write4016 = &mmEmuPadKeyboard_Write4016;
    //p->super.Write4017 = &mmEmuPadKeyboard_Write4017;
    //p->super.Sync = &mmEmuPadKeyboard_Sync;
    //p->super.SetSyncData = &mmEmuPadKeyboard_SetSyncData;
    //p->super.GetSyncData = &mmEmuPadKeyboard_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadKeyboard_Destroy(struct mmEmuPadKeyboard* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->bGraph = MM_FALSE;
    p->bOut = MM_FALSE;
    p->ScanNo = 0;
}

MM_EXPORT_EMU void mmEmuPadKeyboard_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadKeyboard* p = (struct mmEmuPadKeyboard*)(super);

    p->bGraph = MM_FALSE;
    p->bOut = MM_FALSE;
    p->ScanNo = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadKeyboard_Read4016(struct mmEmuExpad* super)
{
    // struct mmEmuPadKeyboard* p = (struct mmEmuPadKeyboard*)(super);

    mmByte_t data = 0;

    return  data;
}
MM_EXPORT_EMU mmByte_t mmEmuPadKeyboard_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadKeyboard* p = (struct mmEmuPadKeyboard*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmByte_t data = 0;

    if (p->ScanNo)
    {
        data = 0x1E;
    }

    if (mmEmuController_KeyboardStatus(controller, MM_KC_NEXT)) p->bGraph = MM_TRUE;
    if (mmEmuController_KeyboardStatus(controller, MM_KC_PRIOR)) p->bGraph = MM_FALSE;

    switch (p->ScanNo)
    {
    case 1:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F8)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_RETURN)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LBRACKET)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_RBRACKET)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F12)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_RSHIFT)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_BACKSLASH)
                || mmEmuController_KeyboardStatus(controller, MM_KC_YEN)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_END)) data &= ~0x10;
        }
        break;
    case 2:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F7)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_AT)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_COLON)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_SEMICOLON)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_UNDERLINE)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_SLASH)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_MINUS)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_CIRCUMFLEX)) data &= ~0x10;
        }
        break;
    case 3:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F6)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_O)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_L)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_K)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_PERIOD)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_COMMA)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_P)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_0)) data &= ~0x10;
        }
        break;
    case 4:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F5)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_I)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_U)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_J)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_M)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_N)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_9)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_8)) data &= ~0x10;
        }
        break;
    case 5:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F4)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_Y)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_G)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_H)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_B)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_V)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_7)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_6)) data &= ~0x10;
        }
        break;
    case 6:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F3)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_T)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_R)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_D)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_C)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_5)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_4)) data &= ~0x10;
        }
        break;
    case 7:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F2)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_W)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_S)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_A)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_X)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_Z)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_E)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_3)) data &= ~0x10;
        }
        break;
    case 8:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F1)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_ESCAPE)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_Q)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LCONTROL)
                || mmEmuController_KeyboardStatus(controller, MM_KC_RCONTROL)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LSHIFT)) data &= ~0x02;
            if (p->bGraph) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_1)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_2)) data &= ~0x10;
        }
        break;
    case 9:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_HOME)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_UP)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_RIGHT)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LEFT)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_DOWN)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_SPACE)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_DELETE)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_INSERT)) data &= ~0x10;
        }
        break;
    case 10:
        break;
    default:
        break;
    }

    return data & 0x1E;
}
MM_EXPORT_EMU void mmEmuPadKeyboard_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadKeyboard* p = (struct mmEmuPadKeyboard*)(super);

    if (data == 0x05)
    {
        p->bOut = MM_FALSE;
        p->ScanNo = 0;
    }
    else if (data == 0x04)
    {
        // if(++p->ScanNo > 9)
        if (++p->ScanNo > 10)
        {
            p->ScanNo = 0;
        }
        p->bOut = !p->bOut;
    }
    else if (data == 0x06)
    {
        p->bOut = !p->bOut;
    }
    else
    {
        p->ScanNo = 0;
    }
}

static struct mmEmuExpad* __static_mmEmuPadKeyboard_Produce(void)
{
    struct mmEmuPadKeyboard* p = (struct mmEmuPadKeyboard*)mmMalloc(sizeof(struct mmEmuPadKeyboard));
    mmEmuPadKeyboard_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadKeyboard_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadKeyboard* p = (struct mmEmuPadKeyboard*)(m);
    mmEmuPadKeyboard_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_KEYBOARD =
{
    &__static_mmEmuPadKeyboard_Produce,
    &__static_mmEmuPadKeyboard_Recycle,
};
