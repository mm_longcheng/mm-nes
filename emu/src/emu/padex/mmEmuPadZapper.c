/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadZapper.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuPadZapper_Init(struct mmEmuPadZapper* p)
{
    mmEmuExpad_Init(&p->super);

    p->zapper_x = 0;
    p->zapper_y = 0;
    p->zapper_button = 0;
    p->zapper_offscreen = 0;

    //
    p->super.Reset = &mmEmuPadZapper_Reset;
    //p->super.Strobe = &mmEmuPadZapper_Strobe;
    //p->super.Read4016 = &mmEmuPadZapper_Read4016;
    p->super.Read4017 = &mmEmuPadZapper_Read4017;
    //p->super.Write4016 = &mmEmuPadZapper_Write4016;
    //p->super.Write4017 = &mmEmuPadZapper_Write4017;
    p->super.Sync = &mmEmuPadZapper_Sync;
    p->super.SetSyncData = &mmEmuPadZapper_SetSyncData;
    p->super.GetSyncData = &mmEmuPadZapper_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadZapper_Destroy(struct mmEmuPadZapper* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->zapper_x = 0;
    p->zapper_y = 0;
    p->zapper_button = 0;
    p->zapper_offscreen = 0;
}

MM_EXPORT_EMU void mmEmuPadZapper_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadZapper* p = (struct mmEmuPadZapper*)(super);

    p->zapper_button = 0;
    p->zapper_offscreen = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadZapper_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadZapper* p = (struct mmEmuPadZapper*)(super);

    mmByte_t data = 0x08;

    if (p->zapper_button)
    {
        data |= 0x10;
        p->zapper_offscreen = p->zapper_button & 0x02;
    }

    if (mmEmuNes_GetZapperHit(p->super.nes))
    {
        if (mmEmuScreen_GetZapperHit(&p->super.nes->screen) >= 0x40)
        {
            data &= ~0x08;
        }
    }

    // Fire offscreen with right mouse button
    if (p->zapper_offscreen)
    {
        data |= 0x08;
    }

    // Fire at the screen with left mouse button
    if (p->zapper_button & 0x01)
    {
        p->zapper_offscreen = 0;
    }

    return data;
}

MM_EXPORT_EMU void mmEmuPadZapper_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadZapper* p = (struct mmEmuPadZapper*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmEmuNes_GetZapperPos(p->super.nes, &p->zapper_x, &p->zapper_y);

    p->zapper_button = 0;
    if (mmEmuController_MouseButtonStatus(controller, MM_MB_LBUTTON))
    {
        p->zapper_button |= 0x01;
    }
    if (mmEmuController_MouseButtonStatus(controller, MM_MB_RBUTTON))
    {
        p->zapper_button |= 0x02;
    }
}
MM_EXPORT_EMU void mmEmuPadZapper_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadZapper* p = (struct mmEmuPadZapper*)(super);

    if (type == 0)
    {
        p->zapper_x = data;
    }
    else if (type == 1)
    {
        p->zapper_y = data;
    }
    else if (type == 2)
    {
        p->zapper_button = (mmByte_t)(data & 0x03);
    }
}
MM_EXPORT_EMU mmLong_t mmEmuPadZapper_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadZapper* p = (struct mmEmuPadZapper*)(super);

    mmLong_t data = 0;

    if (type == 0)
    {
        data = p->zapper_x;
    }
    else if (type == 1)
    {
        data = p->zapper_y;
    }
    else if (type == 2)
    {
        data = (p->zapper_button & 0x03);
    }
    return data;
}

static struct mmEmuExpad* __static_mmEmuPadZapper_Produce(void)
{
    struct mmEmuPadZapper* p = (struct mmEmuPadZapper*)mmMalloc(sizeof(struct mmEmuPadZapper));
    mmEmuPadZapper_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadZapper_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadZapper* p = (struct mmEmuPadZapper*)(m);
    mmEmuPadZapper_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_ZAPPER =
{
    &__static_mmEmuPadZapper_Produce,
    &__static_mmEmuPadZapper_Recycle,
};
