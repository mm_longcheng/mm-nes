/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuExpadRegister.h"
#include "mmEmuPad.h"

#include "emu/padex/mmEmuPadCrazyClimber.h"
#include "emu/padex/mmEmuPadExcitingBoxing.h"
#include "emu/padex/mmEmuPadFamilyTrainerA.h"
#include "emu/padex/mmEmuPadFamilyTrainerB.h"
#include "emu/padex/mmEmuPadGyromite.h"
#include "emu/padex/mmEmuPadHyperShot.h"
#include "emu/padex/mmEmuPadKeyboard.h"
#include "emu/padex/mmEmuPadMahjang.h"
#include "emu/padex/mmEmuPadOekakidsTablet.h"
#include "emu/padex/mmEmuPadPaddle.h"
#include "emu/padex/mmEmuPadSpaceShadowGun.h"
#include "emu/padex/mmEmuPadSuporKeyboard.h"
#include "emu/padex/mmEmuPadToprider.h"
#include "emu/padex/mmEmuPadTurboFile.h"
#include "emu/padex/mmEmuPadVSUnisystem.h"
#include "emu/padex/mmEmuPadVSZapper.h"
#include "emu/padex/mmEmuPadZapper.h"

MM_EXPORT_EMU void mmEmuExpadRegister_Internal(struct mmEmuExpadFactory* p)
{
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_CRAZYCLIMBER, &MM_EMU_PAD_CREATOR_CRAZYCLIMBER);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_EXCITINGBOXING, &MM_EMU_PAD_CREATOR_EXCITINGBOXING);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERA, &MM_EMU_PAD_CREATOR_FAMLYTRAINERA);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERB, &MM_EMU_PAD_CREATOR_FAMLYTRAINERB);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_GYROMITE, &MM_EMU_PAD_CREATOR_GYROMITE);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_HYPERSHOT, &MM_EMU_PAD_CREATOR_HYPERSHOT);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_KEYBOARD, &MM_EMU_PAD_CREATOR_KEYBOARD);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_MAHJANG, &MM_EMU_PAD_CREATOR_MAHJANG);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_OEKAKIDSTABLET, &MM_EMU_PAD_CREATOR_OEKAKIDSTABLET);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_PADDLE, &MM_EMU_PAD_CREATOR_PADDLE);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN, &MM_EMU_PAD_CREATOR_SPACESHADOWGUN);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_SUPORKEYBOARD, &MM_EMU_PAD_CREATOR_SUPORKEYBOARD);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_TOPRIDER, &MM_EMU_PAD_CREATOR_TOPRIDER);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_TURBOFILE, &MM_EMU_PAD_CREATOR_TURBOFILE);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_VSUNISYSTEM, &MM_EMU_PAD_CREATOR_VSUNISYSTEM);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_VSZAPPER, &MM_EMU_PAD_CREATOR_VSZAPPER);
    mmEmuExpadFactory_Register(p, MM_EMU_PAD_EXCONTROLLER_ZAPPER, &MM_EMU_PAD_CREATOR_ZAPPER);
}


