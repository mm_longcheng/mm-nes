#version 120

// 16x16 palette
uniform sampler2D texture0;
uniform sampler2D texture1;

varying vec2 outUV0;
varying vec4 outColor;

void main(void)
{
	vec2 s_uv;

	// x_scale = 128 / 256
	// y_scale = 256 / 240
	s_uv.x = outUV0.x / 0.5;
	s_uv.y = outUV0.y / 1.0666667;

	// single channel --> rgba index.
	vec2 i_idx;
	s_uv.x = s_uv.x * 128.0;
	i_idx.x = s_uv.x / 4.0;
	i_idx.y = s_uv.x - 4.0 * floor(i_idx.x);
	
	int c_idx = int(i_idx.y);
	s_uv.x = i_idx.x / 128.0;

	float s_idx = texture2D(texture1, s_uv)[c_idx];
	// index [0.0, 255.0]
	float index = floor(s_idx * 256.0);

	vec2 v_idx;
	v_idx.y = index / 16.0;
	v_idx.x = index - 16.0 * floor(v_idx.y);
	v_idx.y /= 16.0;
	v_idx.x /= 16.0;
	vec4 tc = texture2D(texture0, v_idx);
	
	gl_FragColor = tc * outColor;
}