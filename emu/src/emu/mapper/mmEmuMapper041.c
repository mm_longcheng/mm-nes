/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper041.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper041_Init(struct mmEmuMapper041* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[0] = p->reg[1] = 0;

    //
    p->super.Reset = &mmEmuMapper041_Reset;
    p->super.Write = &mmEmuMapper041_Write;
    //p->super.Read = &mmEmuMapper041_Read;
    //p->super.ReadLow = &mmEmuMapper041_ReadLow;
    p->super.WriteLow = &mmEmuMapper041_WriteLow;
    //p->super.ExRead = &mmEmuMapper041_ExRead;
    //p->super.ExWrite = &mmEmuMapper041_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper041_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper041_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper041_HSync;
    //p->super.VSync = &mmEmuMapper041_VSync;
    //p->super.Clock = &mmEmuMapper041_Clock;
    //p->super.PPULatch = &mmEmuMapper041_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper041_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper041_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper041_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper041_IsStateSave;
    p->super.SaveState = &mmEmuMapper041_SaveState;
    p->super.LoadState = &mmEmuMapper041_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper041_Destroy(struct mmEmuMapper041* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg[0] = p->reg[1] = 0;
}

MM_EXPORT_EMU void mmEmuMapper041_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper041* p = (struct mmEmuMapper041*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->reg[0] = p->reg[1] = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper041_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper041* p = (struct mmEmuMapper041*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x6000 && addr < 0x6800)
    {
        mmEmuMmu_SetPROM32KBank(mmu, addr & 0x07);
        p->reg[0] = addr & 0x04;
        p->reg[1] &= 0x03;
        p->reg[1] |= (addr >> 1) & 0x0C;
        mmEmuMmu_SetVROM8KBank(mmu, p->reg[1]);
        if (addr & 0x20)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper041_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper041* p = (struct mmEmuMapper041*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0])
    {
        p->reg[1] &= 0x0C;
        p->reg[1] |= addr & 0x03;
        mmEmuMmu_SetVROM8KBank(mmu, p->reg[1]);
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper041_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper041_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper041* p = (struct mmEmuMapper041*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
}
MM_EXPORT_EMU void mmEmuMapper041_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper041* p = (struct mmEmuMapper041*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper041_Produce(void)
{
    struct mmEmuMapper041* p = (struct mmEmuMapper041*)mmMalloc(sizeof(struct mmEmuMapper041));
    mmEmuMapper041_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper041_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper041* p = (struct mmEmuMapper041*)(m);
    mmEmuMapper041_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER041 =
{
    &__static_mmEmuMapper041_Produce,
    &__static_mmEmuMapper041_Recycle,
};
