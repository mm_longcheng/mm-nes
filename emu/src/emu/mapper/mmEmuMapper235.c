/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper235.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper235_Init(struct mmEmuMapper235* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper235_Reset;
    p->super.Write = &mmEmuMapper235_Write;
    //p->super.Read = &mmEmuMapper235_Read;
    //p->super.ReadLow = &mmEmuMapper235_ReadLow;
    //p->super.WriteLow = &mmEmuMapper235_WriteLow;
    //p->super.ExRead = &mmEmuMapper235_ExRead;
    //p->super.ExWrite = &mmEmuMapper235_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper235_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper235_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper235_HSync;
    //p->super.VSync = &mmEmuMapper235_VSync;
    //p->super.Clock = &mmEmuMapper235_Clock;
    //p->super.PPULatch = &mmEmuMapper235_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper235_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper235_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper235_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper235_IsStateSave;
    //p->super.SaveState = &mmEmuMapper235_SaveState;
    //p->super.LoadState = &mmEmuMapper235_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper235_Destroy(struct mmEmuMapper235* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper235_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper235* p = (struct mmEmuMapper235*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(mmu->DRAM, 0xFF, 0x2000);

    mmEmuMmu_SetPROM32KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper235_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper235* p = (struct mmEmuMapper235*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t prg = ((addr & 0x0300) >> 3) | (addr & 0x001F);
    mmByte_t bus = 0;

    if (mmu->PROM_08K_SIZE == 64 * 2)
    {
        // 100-in-1
        switch (addr & 0x0300)
        {
        case 0x0000:
            break;
        case 0x0100:
            bus = 1;
            break;
        case 0x0200:
            bus = 1;
            break;
        case 0x0300:
            bus = 1;
            break;
        default:
            break;
        }
    }
    else if (mmu->PROM_08K_SIZE == 128 * 2)
    {
        // 150-in-1
        switch (addr & 0x0300)
        {
        case 0x0000:
            break;
        case 0x0100:
            bus = 1;
            break;
        case 0x0200:
            prg = (prg & 0x1F) | 0x20;
            break;
        case 0x0300:
            bus = 1;
            break;
        default:
            break;
        }
    }
    else if (mmu->PROM_08K_SIZE == 192 * 2)
    {
        // 150-in-1
        switch (addr & 0x0300)
        {
        case 0x0000:
            break;
        case 0x0100:
            bus = 1;
            break;
        case 0x0200:
            prg = (prg & 0x1F) | 0x20;
            break;
        case 0x0300:
            prg = (prg & 0x1F) | 0x40;
            break;
        default:
            break;
        }
    }
    else if (mmu->PROM_08K_SIZE == 256 * 2)
    {

    }

    if (addr & 0x0800)
    {
        if (addr & 0x1000)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, prg * 4 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 5, prg * 4 + 3);
            mmEmuMmu_SetPROM8KBank(mmu, 6, prg * 4 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 7, prg * 4 + 3);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, prg * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 5, prg * 4 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 6, prg * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, prg * 4 + 1);
        }
    }
    else
    {
        mmEmuMmu_SetPROM32KBank(mmu, prg);
    }

    if (bus)
    {
        mmEmuMmu_SetPROMBank(mmu, 4, mmu->DRAM, MM_EMU_MMU_BANKTYPE_ROM);
        mmEmuMmu_SetPROMBank(mmu, 5, mmu->DRAM, MM_EMU_MMU_BANKTYPE_ROM);
        mmEmuMmu_SetPROMBank(mmu, 6, mmu->DRAM, MM_EMU_MMU_BANKTYPE_ROM);
        mmEmuMmu_SetPROMBank(mmu, 7, mmu->DRAM, MM_EMU_MMU_BANKTYPE_ROM);
    }

    if (addr & 0x0400)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
    }
    else if (addr & 0x2000)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper235_Produce(void)
{
    struct mmEmuMapper235* p = (struct mmEmuMapper235*)mmMalloc(sizeof(struct mmEmuMapper235));
    mmEmuMapper235_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper235_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper235* p = (struct mmEmuMapper235*)(m);
    mmEmuMapper235_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER235 =
{
    &__static_mmEmuMapper235_Produce,
    &__static_mmEmuMapper235_Recycle,
};
