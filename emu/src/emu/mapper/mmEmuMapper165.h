/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper165_h__
#define __mmEmuMapper165_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper165   Fire Emblem Chinese version                              //
//----------------------------------------------------------------------//
struct mmEmuMapper165
{
    struct mmEmuMapper super;

    mmByte_t    reg[8];
    mmByte_t    prg0, prg1;
    mmByte_t    chr0, chr1, chr2, chr3;
    mmByte_t    we_sram;
    mmByte_t    latch;
};

MM_EXPORT_EMU void mmEmuMapper165_Init(struct mmEmuMapper165* p);
MM_EXPORT_EMU void mmEmuMapper165_Destroy(struct mmEmuMapper165* p);

MM_EXPORT_EMU void mmEmuMapper165_SetBankCPU(struct mmEmuMapper165* p);
MM_EXPORT_EMU void mmEmuMapper165_SetBankPPU(struct mmEmuMapper165* p);
MM_EXPORT_EMU void mmEmuMapper165_SetBankPPUSUB(struct mmEmuMapper165* p, int bank, int page);
MM_EXPORT_EMU void mmEmuMapper165_Update_Latch(struct mmEmuMapper165* p, mmWord_t addr);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper165_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper165_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper165_PPULatch(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper165_PPUChrLatch(struct mmEmuMapper* super, mmWord_t addr);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper165_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper165_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper165_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER165;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper165_h__
