/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper229.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper229_Init(struct mmEmuMapper229* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper229_Reset;
    p->super.Write = &mmEmuMapper229_Write;
    //p->super.Read = &mmEmuMapper229_Read;
    //p->super.ReadLow = &mmEmuMapper229_ReadLow;
    //p->super.WriteLow = &mmEmuMapper229_WriteLow;
    //p->super.ExRead = &mmEmuMapper229_ExRead;
    //p->super.ExWrite = &mmEmuMapper229_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper229_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper229_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper229_HSync;
    //p->super.VSync = &mmEmuMapper229_VSync;
    //p->super.Clock = &mmEmuMapper229_Clock;
    //p->super.PPULatch = &mmEmuMapper229_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper229_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper229_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper229_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper229_IsStateSave;
    //p->super.SaveState = &mmEmuMapper229_SaveState;
    //p->super.LoadState = &mmEmuMapper229_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper229_Destroy(struct mmEmuMapper229* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper229_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper229* p = (struct mmEmuMapper229*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper229_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper229* p = (struct mmEmuMapper229*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr & 0x001E)
    {
        mmByte_t prg = addr & 0x001F;

        mmEmuMmu_SetPROM8KBank(mmu, 4, prg * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, prg * 2 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, prg * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 7, prg * 2 + 1);

        mmEmuMmu_SetVROM8KBank(mmu, addr & 0x0FFF);
    }
    else
    {
        mmEmuMmu_SetPROM32KBank(mmu, 0);
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    if (addr & 0x0020)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper229_Produce(void)
{
    struct mmEmuMapper229* p = (struct mmEmuMapper229*)mmMalloc(sizeof(struct mmEmuMapper229));
    mmEmuMapper229_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper229_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper229* p = (struct mmEmuMapper229*)(m);
    mmEmuMapper229_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER229 =
{
    &__static_mmEmuMapper229_Produce,
    &__static_mmEmuMapper229_Recycle,
};
