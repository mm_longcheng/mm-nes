/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmulatorAssets.h"

#include "dish/mmFilePath.h"

#include "emu/mmEmuEmulator.h"

MM_EXPORT_EMULATOR void mmEmulatorAssets_Init(struct mmEmulatorAssets* p)
{
    mmString_Init(&p->fullname);
    mmString_Init(&p->pathname);
    mmString_Init(&p->basename);
    mmString_Init(&p->suffixname);

    mmString_Init(&p->rootpath);

    mmString_Init(&p->assets_name);
    mmString_Init(&p->source_name);

    mmString_Init(&p->writable_directory);
    mmString_Init(&p->writable_path);
    mmString_Init(&p->writable_base);

    mmString_Init(&p->emulator_directory);

    p->assets_type = MM_EMU_ASSETS_FOLDER;

    p->file_context = NULL;

    mmString_Assigns(&p->pathname, "emulator");
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_Destroy(struct mmEmulatorAssets* p)
{
    mmString_Destroy(&p->fullname);
    mmString_Destroy(&p->pathname);
    mmString_Destroy(&p->basename);
    mmString_Destroy(&p->suffixname);

    mmString_Destroy(&p->rootpath);

    mmString_Destroy(&p->assets_name);
    mmString_Destroy(&p->source_name);

    mmString_Destroy(&p->writable_directory);
    mmString_Destroy(&p->writable_path);
    mmString_Destroy(&p->writable_base);

    mmString_Destroy(&p->emulator_directory);

    p->assets_type = MM_EMU_ASSETS_FOLDER;

    p->file_context = NULL;
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_Reset(struct mmEmulatorAssets* p, struct mmEmulatorAssets* q)
{
    mmString_Clear(&p->fullname);
    mmString_Clear(&p->pathname);
    mmString_Clear(&p->basename);
    mmString_Clear(&p->suffixname);

    mmString_Clear(&p->rootpath);

    mmString_Clear(&p->assets_name);
    mmString_Clear(&p->source_name);

    mmString_Clear(&p->writable_directory);
    mmString_Clear(&p->writable_path);
    mmString_Clear(&p->writable_base);

    mmString_Clear(&p->emulator_directory);

    p->assets_type = MM_EMU_ASSETS_FOLDER;

    p->file_context = NULL;
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_Copy(struct mmEmulatorAssets* p, struct mmEmulatorAssets* q)
{
    mmString_Assign(&p->fullname, &q->fullname);
    mmString_Assign(&p->pathname, &q->pathname);
    mmString_Assign(&p->basename, &q->basename);
    mmString_Assign(&p->suffixname, &q->suffixname);

    mmString_Assign(&p->rootpath, &q->rootpath);

    mmString_Assign(&p->assets_name, &q->assets_name);
    mmString_Assign(&p->source_name, &q->source_name);

    mmString_Assign(&p->writable_directory, &q->writable_directory);
    mmString_Assign(&p->writable_path, &q->writable_path);
    mmString_Assign(&p->writable_base, &q->writable_base);

    mmString_Assign(&p->emulator_directory, &q->emulator_directory);

    p->assets_type = q->assets_type;

    p->file_context = q->file_context;
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetPath(struct mmEmulatorAssets* p, const char* path, const char* base)
{
    struct mmString bname;

    mmString_Init(&bname);

    mmString_Assigns(&p->pathname, path);
    mmString_Assigns(&p->basename, base);
    mmString_Assigns(&p->fullname, path);
    mmString_Appends(&p->fullname, "/");
    mmString_Appends(&p->fullname, base);

    mmPathSplitSuffixName(&p->basename, &bname, &p->suffixname);

    mmString_Destroy(&bname);
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetAssetsType(struct mmEmulatorAssets* p, mmUInt32_t assets_type)
{
    p->assets_type = assets_type;
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetRootPath(struct mmEmulatorAssets* p, const char* rootpath)
{
    mmString_Assigns(&p->rootpath, rootpath);
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetAssetsName(struct mmEmulatorAssets* p, const char* assets_name)
{
    mmString_Assigns(&p->assets_name, assets_name);
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetSourceName(struct mmEmulatorAssets* p, const char* source_name)
{
    mmString_Assigns(&p->source_name, source_name);
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetWritable(struct mmEmulatorAssets* p, const char* writable_path, const char* writable_base)
{
    mmString_Assigns(&p->writable_path, writable_path);
    mmString_Assigns(&p->writable_base, writable_base);

    mmString_Assign(&p->writable_directory, &p->writable_path);
    mmString_Appends(&p->writable_directory, mmString_Empty(&p->writable_path) ? "" : "/");
    mmString_Append(&p->writable_directory, &p->writable_base);

    // remove the suffix if t is empty.
    mmDirectoryNoneSuffix(&p->writable_directory, mmString_CStr(&p->writable_directory));

    mmString_Assign(&p->emulator_directory, &p->writable_base);
    mmString_Appends(&p->emulator_directory, "/");
    mmString_Append(&p->emulator_directory, &p->source_name);

    // remove the suffix if source_name is empty.
    mmDirectoryNoneSuffix(&p->emulator_directory, mmString_CStr(&p->emulator_directory));
}
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetFileContext(struct mmEmulatorAssets* p, struct mmFileContext* file_context)
{
    p->file_context = file_context;
}
