/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper227.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper227_Init(struct mmEmuMapper227* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper227_Reset;
    p->super.Write = &mmEmuMapper227_Write;
    //p->super.Read = &mmEmuMapper227_Read;
    //p->super.ReadLow = &mmEmuMapper227_ReadLow;
    //p->super.WriteLow = &mmEmuMapper227_WriteLow;
    //p->super.ExRead = &mmEmuMapper227_ExRead;
    //p->super.ExWrite = &mmEmuMapper227_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper227_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper227_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper227_HSync;
    //p->super.VSync = &mmEmuMapper227_VSync;
    //p->super.Clock = &mmEmuMapper227_Clock;
    //p->super.PPULatch = &mmEmuMapper227_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper227_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper227_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper227_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper227_IsStateSave;
    //p->super.SaveState = &mmEmuMapper227_SaveState;
    //p->super.LoadState = &mmEmuMapper227_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper227_Destroy(struct mmEmuMapper227* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper227_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper227* p = (struct mmEmuMapper227*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 0, 1);
}
MM_EXPORT_EMU void mmEmuMapper227_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper227* p = (struct mmEmuMapper227*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t bank = ((addr & 0x0100) >> 4) | ((addr & 0x0078) >> 3);

    if (addr & 0x0001)
    {
        mmEmuMmu_SetPROM32KBank(mmu, bank);
    }
    else
    {
        if (addr & 0x0004)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, bank * 4 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 5, bank * 4 + 3);
            mmEmuMmu_SetPROM8KBank(mmu, 6, bank * 4 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 7, bank * 4 + 3);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, bank * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 5, bank * 4 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 6, bank * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, bank * 4 + 1);
        }
    }

    if (!(addr & 0x0080))
    {
        if (addr & 0x0200)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, (bank & 0x1C) * 4 + 14);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (bank & 0x1C) * 4 + 15);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, (bank & 0x1C) * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (bank & 0x1C) * 4 + 1);
        }
    }
    if (addr & 0x0002)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper227_Produce(void)
{
    struct mmEmuMapper227* p = (struct mmEmuMapper227*)mmMalloc(sizeof(struct mmEmuMapper227));
    mmEmuMapper227_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper227_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper227* p = (struct mmEmuMapper227*)(m);
    mmEmuMapper227_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER227 =
{
    &__static_mmEmuMapper227_Produce,
    &__static_mmEmuMapper227_Recycle,
};
