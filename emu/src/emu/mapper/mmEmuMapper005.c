/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper005.h"
#include "emu/mmEmuNes.h"

#define MMC5_IRQ_METAL      (1<<0)

MM_EXPORT_EMU void mmEmuMapper005_Init(struct mmEmuMapper005* p)
{
    mmEmuMapper_Init(&p->super);

    p->sram_size = 0;

    p->prg_size = 3;
    p->chr_size = 3;
    p->sram_we_a = 0x00;
    p->sram_we_b = 0x00;
    p->graphic_mode = 0;
    p->nametable_mode = 0;
    mmMemset(p->nametable_type, 0, sizeof(mmByte_t) * 4);

    p->sram_page = 0;

    p->fill_chr = 0;
    p->fill_pal = 0;
    p->split_control = 0;
    p->split_scroll = 0;
    p->split_page = 0;

    p->split_x = 0;
    p->split_addr = 0;
    p->split_yofs = 0;

    p->chr_type = 0;
    p->chr_mode = 0;
    mmMemset(p->chr_page, 0, sizeof(mmByte_t) * 2 * 8);
    mmMemset(p->BG_MEM_BANK, 0, sizeof(mmByte_t*) * 8);
    mmMemset(p->BG_MEM_PAGE, 0, sizeof(mmByte_t) * 8);

    p->irq_status = 0;
    p->irq_enable = 0;
    p->irq_line = 0;
    p->irq_scanline = 0;
    p->irq_clear = 0;
    p->irq_type = 0;

    p->mult_a = 0;
    p->mult_b = 0;

    //
    p->super.Reset = &mmEmuMapper005_Reset;
    p->super.Write = &mmEmuMapper005_Write;
    //p->super.Read = &mmEmuMapper005_Read;
    p->super.ReadLow = &mmEmuMapper005_ReadLow;
    p->super.WriteLow = &mmEmuMapper005_WriteLow;
    //p->super.ExRead = &mmEmuMapper005_ExRead;
    //p->super.ExWrite = &mmEmuMapper005_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper005_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper005_ExCmdWrite;
    p->super.HSync = &mmEmuMapper005_HSync;
    //p->super.VSync = &mmEmuMapper005_VSync;
    //p->super.Clock = &mmEmuMapper005_Clock;
    //p->super.PPULatch = &mmEmuMapper005_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper005_PPUChrLatch;
    p->super.PPUExtLatchX = &mmEmuMapper005_PPUExtLatchX;
    p->super.PPUExtLatch = &mmEmuMapper005_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper005_IsStateSave;
    p->super.SaveState = &mmEmuMapper005_SaveState;
    p->super.LoadState = &mmEmuMapper005_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper005_Destroy(struct mmEmuMapper005* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->sram_size = 0;

    p->prg_size = 3;
    p->chr_size = 3;
    p->sram_we_a = 0x00;
    p->sram_we_b = 0x00;
    p->graphic_mode = 0;
    p->nametable_mode = 0;
    mmMemset(p->nametable_type, 0, sizeof(mmByte_t) * 4);

    p->sram_page = 0;

    p->fill_chr = 0;
    p->fill_pal = 0;
    p->split_control = 0;
    p->split_scroll = 0;
    p->split_page = 0;

    p->split_x = 0;
    p->split_addr = 0;
    p->split_yofs = 0;

    p->chr_type = 0;
    p->chr_mode = 0;
    mmMemset(p->chr_page, 0, sizeof(mmByte_t) * 2 * 8);
    mmMemset(p->BG_MEM_BANK, 0, sizeof(mmByte_t*) * 8);
    mmMemset(p->BG_MEM_PAGE, 0, sizeof(mmByte_t) * 8);

    p->irq_status = 0;
    p->irq_enable = 0;
    p->irq_line = 0;
    p->irq_scanline = 0;
    p->irq_clear = 0;
    p->irq_type = 0;

    p->mult_a = 0;
    p->mult_b = 0;
}

MM_EXPORT_EMU void mmEmuMapper005_SetBankCPU(struct mmEmuMapper005* p, mmWord_t addr, mmByte_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (data & 0x80)
    {
        // PROM Bank
        switch (addr & 7)
        {
        case 4:
            if (p->prg_size == 3)
            {
                mmEmuMmu_SetPROM8KBank(mmu, 4, data & 0x7F);
            }
            break;
        case 5:
            if (p->prg_size == 1 || p->prg_size == 2)
            {
                mmEmuMmu_SetPROM16KBank(mmu, 4, (data & 0x7F) >> 1);
            }
            else if (p->prg_size == 3)
            {
                mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x7F));
            }
            break;
        case 6:
            if (p->prg_size == 2 || p->prg_size == 3)
            {
                mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x7F));
            }
            break;
        case 7:
            if (p->prg_size == 0)
            {
                mmEmuMmu_SetPROM32KBank(mmu, (data & 0x7F) >> 2);
            }
            else if (p->prg_size == 1)
            {
                mmEmuMmu_SetPROM16KBank(mmu, 6, (data & 0x7F) >> 1);
            }
            else if (p->prg_size == 2 || p->prg_size == 3)
            {
                mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x7F));
            }
            break;
        default:
            break;
        }
    }
    else
    {
        // WRAM Bank
        switch (addr & 7)
        {
        case 4:
            if (p->prg_size == 3)
            {
                mmEmuMapper005_SetBankSRAM(p, 4, data & 0x07);
            }
            break;
        case 5:
            if (p->prg_size == 1 || p->prg_size == 2)
            {
                mmEmuMapper005_SetBankSRAM(p, 4, (data & 0x06) + 0);
                mmEmuMapper005_SetBankSRAM(p, 5, (data & 0x06) + 1);
            }
            else if (p->prg_size == 3)
            {
                mmEmuMapper005_SetBankSRAM(p, 5, data & 0x07);
            }
            break;
        case 6:
            if (p->prg_size == 2 || p->prg_size == 3)
            {
                mmEmuMapper005_SetBankSRAM(p, 6, data & 0x07);
            }
            break;
        default:
            break;
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper005_SetBankSRAM(struct mmEmuMapper005* p, mmByte_t page, mmByte_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->sram_size == 0) data = (data > 3) ? 8 : 0;
    if (p->sram_size == 1) data = (data > 3) ? 1 : 0;
    if (p->sram_size == 2) data = (data > 3) ? 8 : data;
    if (p->sram_size == 3) data = (data > 3) ? 4 : data;

    if (data != 8)
    {
        mmEmuMmu_SetPROMBank(mmu, page, &mmu->WRAM[0x2000 * data], MM_EMU_MMU_BANKTYPE_RAM);
        mmu->CPU_MEM_PAGE[page] = data;
    }
    else
    {
        mmu->CPU_MEM_TYPE[page] = MM_EMU_MMU_BANKTYPE_ROM;
    }
}
MM_EXPORT_EMU void mmEmuMapper005_SetBankPPU(struct mmEmuMapper005* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i;

    if (p->chr_mode == 0)
    {
        // PPU SP Bank
        switch (p->chr_size)
        {
        case 0:
            mmEmuMmu_SetVROM8KBank(mmu, p->chr_page[0][7]);
            break;
        case 1:
            mmEmuMmu_SetVROM4KBank(mmu, 0, p->chr_page[0][3]);
            mmEmuMmu_SetVROM4KBank(mmu, 4, p->chr_page[0][7]);
            break;
        case 2:
            mmEmuMmu_SetVROM2KBank(mmu, 0, p->chr_page[0][1]);
            mmEmuMmu_SetVROM2KBank(mmu, 2, p->chr_page[0][3]);
            mmEmuMmu_SetVROM2KBank(mmu, 4, p->chr_page[0][5]);
            mmEmuMmu_SetVROM2KBank(mmu, 6, p->chr_page[0][7]);
            break;
        case 3:
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr_page[0][0],
                p->chr_page[0][1],
                p->chr_page[0][2],
                p->chr_page[0][3],
                p->chr_page[0][4],
                p->chr_page[0][5],
                p->chr_page[0][6],
                p->chr_page[0][7]);
            break;
        default:
            break;
        }
    }
    else if (p->chr_mode == 1)
    {
        // PPU BG Bank
        switch (p->chr_size)
        {
        case 0:
            for (i = 0; i < 8; i++)
            {
                p->BG_MEM_BANK[i] = mmu->VROM + 0x2000 * (p->chr_page[1][7] % mmu->VROM_8K_SIZE) + 0x0400 * i;
                p->BG_MEM_PAGE[i] = (p->chr_page[1][7] % mmu->VROM_8K_SIZE) * 8 + i;
            }
            break;
        case 1:
            for (i = 0; i < 4; i++)
            {
                p->BG_MEM_BANK[i + 0] = mmu->VROM + 0x1000 * (p->chr_page[1][3] % mmu->VROM_4K_SIZE) + 0x0400 * i;
                p->BG_MEM_BANK[i + 4] = mmu->VROM + 0x1000 * (p->chr_page[1][7] % mmu->VROM_4K_SIZE) + 0x0400 * i;
                p->BG_MEM_PAGE[i + 0] = (p->chr_page[1][3] % mmu->VROM_4K_SIZE) * 4 + i;
                p->BG_MEM_PAGE[i + 4] = (p->chr_page[1][7] % mmu->VROM_4K_SIZE) * 4 + i;
            }
            break;
        case 2:
            for (i = 0; i < 2; i++)
            {
                p->BG_MEM_BANK[i + 0] = mmu->VROM + 0x0800 * (p->chr_page[1][1] % mmu->VROM_2K_SIZE) + 0x0400 * i;
                p->BG_MEM_BANK[i + 2] = mmu->VROM + 0x0800 * (p->chr_page[1][3] % mmu->VROM_2K_SIZE) + 0x0400 * i;
                p->BG_MEM_BANK[i + 4] = mmu->VROM + 0x0800 * (p->chr_page[1][5] % mmu->VROM_2K_SIZE) + 0x0400 * i;
                p->BG_MEM_BANK[i + 6] = mmu->VROM + 0x0800 * (p->chr_page[1][7] % mmu->VROM_2K_SIZE) + 0x0400 * i;
                p->BG_MEM_PAGE[i + 0] = (p->chr_page[1][1] % mmu->VROM_2K_SIZE) * 2 + i;
                p->BG_MEM_PAGE[i + 2] = (p->chr_page[1][3] % mmu->VROM_2K_SIZE) * 2 + i;
                p->BG_MEM_PAGE[i + 4] = (p->chr_page[1][5] % mmu->VROM_2K_SIZE) * 2 + i;
                p->BG_MEM_PAGE[i + 6] = (p->chr_page[1][7] % mmu->VROM_2K_SIZE) * 2 + i;
            }
            break;
        case 3:
            for (i = 0; i < 8; i++)
            {
                p->BG_MEM_BANK[i] = mmu->VROM + 0x0400 * (p->chr_page[1][i] % mmu->VROM_1K_SIZE);
                p->BG_MEM_PAGE[i] = (p->chr_page[1][i] % mmu->VROM_1K_SIZE) + i;
            }
            break;
        default:
            break;
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper005_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmInt_t i;

    p->prg_size = 3;
    p->chr_size = 3;

    p->sram_we_a = 0x00;
    p->sram_we_b = 0x00;

    p->graphic_mode = 0;
    p->nametable_mode = 0;

    mmMemset(p->nametable_type, 0, sizeof(mmByte_t) * 4);

    p->fill_chr = p->fill_pal = 0;
    p->split_control = p->split_scroll = p->split_page = 0;

    p->irq_enable = 0;
    p->irq_status = 0;
    p->irq_scanline = 0;
    p->irq_line = 0;
    p->irq_clear = 0;

    p->irq_type = 0;

    p->mult_a = p->mult_b = 0;

    p->chr_type = 0;
    p->chr_mode = 0;
    for (i = 0; i < 8; i++)
    {
        p->chr_page[0][i] = i;
        p->chr_page[1][i] = 4 + (i & 0x03);
    }

    mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 1, mmu->PROM_08K_SIZE - 1, mmu->PROM_08K_SIZE - 1, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    for (i = 0; i < 8; i++)
    {
        p->BG_MEM_BANK[i] = mmu->VROM + 0x0400 * i;
        p->BG_MEM_PAGE[i] = i;
    }

    // SRAM setting.
    mmEmuMapper005_SetBankSRAM(p, 3, 0);

    p->sram_size = 0;
    mmEmuNes_SetSAVERAMSIZE(nes, 16 * 1024);

    crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);

    if (crc == 0x2b548d75 ||    // Bandit Kings of Ancient China(U)
        crc == 0xf4cd4998 ||    // Dai Koukai Jidai(J)
        crc == 0x8fa95456 ||    // Ishin no Arashi(J)
        crc == 0x98c8e090 ||    // Nobunaga no Yabou - Sengoku Gunyuu Den(J)
        crc == 0x8e9a5e2f ||    // L'Empereur(Alt)(U)
        crc == 0x57e3218b ||    // L'Empereur(U)
        crc == 0x2f50bd38 ||    // L'Empereur(J)
        crc == 0xb56958d1 ||    // Nobunaga's Ambition 2(U)
        crc == 0xe6c28c5f ||    // Suikoden - Tenmei no Chikai(J)
        crc == 0xcd35e2e9)      // Uncharted Waters(U)
    {
        p->sram_size = 1;
        mmEmuNes_SetSAVERAMSIZE(nes, 32 * 1024);
    }
    else
    {
        if (crc == 0xf4120e58 ||    // Aoki Ookami to Shiroki Mejika - Genchou Hishi(J)
            crc == 0x286613d8 ||    // Nobunaga no Yabou - Bushou Fuuun Roku(J)
            crc == 0x11eaad26 ||    // Romance of the Three Kingdoms 2(U)
            crc == 0x95ba5733)      // Sangokushi 2(J)
        {
            p->sram_size = 2;
            mmEmuNes_SetSAVERAMSIZE(nes, 64 * 1024);
        }
    }

    if (crc == 0x95ca9ec7)          // Castlevania 3 - Dracula's Curse(U)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }

    if (crc == 0xcd9acf43)          // Metal Slader Glory(J)
    {
        p->irq_type = MMC5_IRQ_METAL;
    }

    if (crc == 0xe91548d8)          // Shin 4 Nin Uchi Mahjong - Yakuman Tengoku(J)
    {
        p->chr_type = 1;
    }

    mmEmuPpu_SetExtLatchMode(&nes->ppu, MM_TRUE);
    mmEmuApu_SelectExSound(&nes->apu, 8);
}

MM_EXPORT_EMU mmByte_t mmEmuMapper005_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t data = (mmByte_t)(addr >> 8);

    switch (addr)
    {
    case 0x5015:
        data = mmEmuApu_ExRead(&nes->apu, addr);
        break;

    case 0x5204:
        data = p->irq_status;
        // p->irq_status = 0;
        p->irq_status &= ~0x80;

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x5205:
        data = p->mult_a * p->mult_b;
        break;
    case 0x5206:
        data = (mmByte_t)(((mmWord_t)p->mult_a * (mmWord_t)p->mult_b) >> 8);
        break;
    default:
        break;
    }

    if (addr >= 0x5C00 && addr <= 0x5FFF)
    {
        // ExRAM mode
        if (p->graphic_mode >= 2)
        {
            data = mmu->VRAM[0x0800 + (addr & 0x3FF)];
        }
    }
    else if (addr >= 0x6000 && addr <= 0x7FFF)
    {
        data = mmEmuMapper_ReadLow(super, addr);
    }

    return data;
}
MM_EXPORT_EMU void mmEmuMapper005_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i;

#if 0
    if (addr >= 0x5000 && addr <= 0x50FF)
    {
        DEBUGOUT("$%04X=%02X C:%10d\n", addr, data, nes->cpu->GetTotalCycles());
    }
#endif

    switch (addr)
    {
    case 0x5100:
        p->prg_size = data & 0x03;
        break;
    case 0x5101:
        p->chr_size = data & 0x03;
        break;

    case 0x5102:
        p->sram_we_a = data & 0x03;
        break;
    case 0x5103:
        p->sram_we_b = data & 0x03;
        break;

    case 0x5104:
        p->graphic_mode = data & 0x03;
        break;
    case 0x5105:
        p->nametable_mode = data;
        for (i = 0; i < 4; i++)
        {
            p->nametable_type[i] = data & 0x03;
            mmEmuMmu_SetVRAM1KBank(mmu, 8 + i, p->nametable_type[i]);
            data >>= 2;
        }
        break;

    case 0x5106:
        p->fill_chr = data;
        break;
    case 0x5107:
        p->fill_pal = data & 0x03;
        break;

    case 0x5113:
        mmEmuMapper005_SetBankSRAM(p, 3, data & 0x07);
        break;

    case 0x5114:
    case 0x5115:
    case 0x5116:
    case 0x5117:
        mmEmuMapper005_SetBankCPU(p, addr, data);
        break;

    case 0x5120:
    case 0x5121:
    case 0x5122:
    case 0x5123:
    case 0x5124:
    case 0x5125:
    case 0x5126:
    case 0x5127:
        p->chr_mode = 0;
        p->chr_page[0][addr & 0x07] = data;
        mmEmuMapper005_SetBankPPU(p);
        break;

    case 0x5128:
    case 0x5129:
    case 0x512A:
    case 0x512B:
        p->chr_mode = 1;
        p->chr_page[1][(addr & 0x03) + 0] = data;
        p->chr_page[1][(addr & 0x03) + 4] = data;
        mmEmuMapper005_SetBankPPU(p);
        break;

    case 0x5200:
        p->split_control = data;
        break;
    case 0x5201:
        p->split_scroll = data;
        break;
    case 0x5202:
        p->split_page = data & 0x3F;
        break;

    case 0x5203:
        p->irq_line = data;

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x5204:
        p->irq_enable = data;

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0x5205:
        p->mult_a = data;
        break;
    case 0x5206:
        p->mult_b = data;
        break;

    default:
        if (addr >= 0x5000 && addr <= 0x5015)
        {
            mmEmuApu_ExWrite(&nes->apu, addr, data);
        }
        else if (addr >= 0x5C00 && addr <= 0x5FFF)
        {
            if (p->graphic_mode == 2) // ExRAM
            {
                mmu->VRAM[0x0800 + (addr & 0x3FF)] = data;
            }
            else if (p->graphic_mode != 3) // Split,ExGraphic
            {
                if (p->irq_status & 0x40)
                {
                    mmu->VRAM[0x0800 + (addr & 0x3FF)] = data;
                }
                else
                {
                    mmu->VRAM[0x0800 + (addr & 0x3FF)] = 0;
                }
            }
        }
        else if (addr >= 0x6000 && addr <= 0x7FFF)
        {
            if ((p->sram_we_a == 0x02) && (p->sram_we_b == 0x01))
            {
                if (mmu->CPU_MEM_TYPE[3] == MM_EMU_MMU_BANKTYPE_RAM)
                {
                    mmu->CPU_MEM_BANK[3][addr & 0x1FFF] = data;
                }
            }
        }
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper005_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->sram_we_a == 0x02 && p->sram_we_b == 0x01)
    {
        if (addr >= 0x8000 && addr < 0xE000)
        {
            if (mmu->CPU_MEM_TYPE[addr >> 13] == MM_EMU_MMU_BANKTYPE_RAM)
            {
                mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
            }
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper005_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (p->irq_type & MMC5_IRQ_METAL)
    {
        if (p->irq_scanline == p->irq_line)
        {
            p->irq_status |= 0x80;
        }
    }

    //  if(mmEmuPpu_IsDispON(&nes->ppu) && scanline < 239 )
    if (mmEmuPpu_IsDispON(&nes->ppu) && scanline < 240)
    {
        p->irq_scanline++;
        p->irq_status |= 0x40;
        p->irq_clear = 0;
    }
    else if (p->irq_type & MMC5_IRQ_METAL)
    {
        p->irq_scanline = 0;
        p->irq_status &= ~0x80;
        p->irq_status &= ~0x40;
    }

    if (!(p->irq_type & MMC5_IRQ_METAL))
    {
        if (p->irq_scanline == p->irq_line)
        {
            p->irq_status |= 0x80;
        }

        if (++p->irq_clear > 2)
        {
            p->irq_scanline = 0;
            p->irq_status &= ~0x80;
            p->irq_status &= ~0x40;

            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }

    if ((p->irq_enable & 0x80) && (p->irq_status & 0x80) && (p->irq_status & 0x40))
    {
        mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        // mm_emu_cpu_IRQ_NotPending(&nes->cpu);
#if 0
        {
            mmInt_t i = 0;
            mmByte_t lpScn = mmEmuPpu_GetScreenPtr(&nes->ppu);

            lpScn = lpScn + (256 + 16)*scanline;

            for (i = 0; i < 256 + 16; i++)
            {
                lpScn[i] = 22;
            }
        }
#endif
    }

    // For Split mode!
    if (scanline == 0)
    {
        p->split_yofs = p->split_scroll & 0x07;
        p->split_addr = ((p->split_scroll & 0xF8) << 2);
    }
    else if (mmEmuPpu_IsDispON(&nes->ppu))
    {
        if (p->split_yofs == 7)
        {
            p->split_yofs = 0;
            if ((p->split_addr & 0x03E0) == 0x03A0)
            {
                p->split_addr &= 0x001F;
            }
            else
            {
                if ((p->split_addr & 0x03E0) == 0x03E0)
                {
                    p->split_addr &= 0x001F;
                }
                else
                {
                    p->split_addr += 0x0020;
                }
            }
        }
        else
        {
            p->split_yofs++;
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper005_PPUExtLatchX(struct mmEmuMapper* super, mmInt_t x)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    p->split_x = x;
}
MM_EXPORT_EMU void mmEmuMapper005_PPUExtLatch(struct mmEmuMapper* super, mmWord_t addr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmWord_t    ntbladr, attradr, tileadr, tileofs;
    mmWord_t    tile_yofs;
    mmUInt32_t  tilebank;
    mmBool_t    bSplit;

    tile_yofs = mmEmuPpu_GetTILEY(&nes->ppu);

    bSplit = MM_FALSE;
    if (p->split_control & 0x80)
    {
        if (!(p->split_control & 0x40))
        {
            // Left side
            if ((p->split_control & 0x1F) > p->split_x)
            {
                bSplit = MM_TRUE;
            }
        }
        else
        {
            // Right side
            if ((p->split_control & 0x1F) <= p->split_x)
            {
                bSplit = MM_TRUE;
            }
        }
    }

    if (!bSplit)
    {
        if (p->nametable_type[(addr & 0x0C00) >> 10] == 3)
        {
            // Fill mode
            if (p->graphic_mode == 1)
            {
                // ExGraphic mode
                ntbladr = 0x2000 + (addr & 0x0FFF);
                // Get Nametable
                tileadr = p->fill_chr * 0x10 + tile_yofs;
                // Get TileBank
                tilebank = 0x1000 * ((mmu->VRAM[0x0800 + (ntbladr & 0x03FF)] & 0x3F) % mmu->VROM_4K_SIZE);
                // Attribute
                (*attr) = (p->fill_pal << 2) & 0x0C;
                // Get Pattern
                (*chr_l) = mmu->VROM[tilebank + tileadr];
                (*chr_h) = mmu->VROM[tilebank + tileadr + 8];
            }
            else
            {
                // Normal
                tileofs = (mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) ? 0x1000 : 0x0000;
                tileadr = tileofs + p->fill_chr * 0x10 + tile_yofs;
                (*attr) = (p->fill_pal << 2) & 0x0C;
                // Get Pattern
                if (p->chr_type)
                {
                    (*chr_l) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
                    (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
                }
                else
                {
                    (*chr_l) = p->BG_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
                    (*chr_h) = p->BG_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
                }
            }
        }
        else if (p->graphic_mode == 1)
        {
            // ExGraphic mode
            ntbladr = 0x2000 + (addr & 0x0FFF);
            // Get Nametable
            tileadr = (mmWord_t)mmu->PPU_MEM_BANK[ntbladr >> 10][ntbladr & 0x03FF] * 0x10 + tile_yofs;
            // Get TileBank
            tilebank = 0x1000 * ((mmu->VRAM[0x0800 + (ntbladr & 0x03FF)] & 0x3F) % mmu->VROM_4K_SIZE);
            // Get Attribute
            (*attr) = (mmu->VRAM[0x0800 + (ntbladr & 0x03FF)] & 0xC0) >> 4;
            // Get Pattern
            (*chr_l) = mmu->VROM[tilebank + tileadr];
            (*chr_h) = mmu->VROM[tilebank + tileadr + 8];
        }
        else
        {
            // Normal or ExVRAM
            tileofs = (mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) ? 0x1000 : 0x0000;
            ntbladr = 0x2000 + (addr & 0x0FFF);
            attradr = 0x23C0 + (addr & 0x0C00) + ((addr & 0x0380) >> 4) + ((addr & 0x001C) >> 2);
            // Get Nametable
            tileadr = tileofs + mmu->PPU_MEM_BANK[ntbladr >> 10][ntbladr & 0x03FF] * 0x10 + tile_yofs;
            // Get Attribute
            (*attr) = mmu->PPU_MEM_BANK[attradr >> 10][attradr & 0x03FF];
            if (ntbladr & 0x0002) (*attr) >>= 2;
            if (ntbladr & 0x0040) (*attr) >>= 4;
            (*attr) = ((*attr) & 0x03) << 2;
            // Get Pattern
            if (p->chr_type)
            {
                (*chr_l) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
                (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
            }
            else
            {
                (*chr_l) = p->BG_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
                (*chr_h) = p->BG_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
            }
        }
    }
    else
    {
        ntbladr = ((p->split_addr & 0x03E0) | (p->split_x & 0x1F)) & 0x03FF;
        // Get Split TileBank
        tilebank = 0x1000 * ((mmInt_t)p->split_page%mmu->VROM_4K_SIZE);
        tileadr = (mmWord_t)mmu->VRAM[0x0800 + ntbladr] * 0x10 + p->split_yofs;
        // Get Attribute
        attradr = 0x03C0 + ((ntbladr & 0x0380) >> 4) + ((ntbladr & 0x001C) >> 2);
        (*attr) = mmu->VRAM[0x0800 + attradr];
        if (ntbladr & 0x0002) (*attr) >>= 2;
        if (ntbladr & 0x0040) (*attr) >>= 4;
        (*attr) = ((*attr) & 0x03) << 2;
        // Get Pattern
        (*chr_l) = mmu->VROM[tilebank + tileadr];
        (*chr_h) = mmu->VROM[tilebank + tileadr + 8];
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper005_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper005_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    mmInt_t i, j;

    buffer[0] = p->prg_size;
    buffer[1] = p->chr_size;
    buffer[2] = p->sram_we_a;
    buffer[3] = p->sram_we_b;
    buffer[4] = p->graphic_mode;
    buffer[5] = p->nametable_mode;
    buffer[6] = p->nametable_type[0];
    buffer[7] = p->nametable_type[1];
    buffer[8] = p->nametable_type[2];
    buffer[9] = p->nametable_type[3];
    buffer[10] = p->sram_page;
    buffer[11] = p->fill_chr;
    buffer[12] = p->fill_pal;
    buffer[13] = p->split_control;
    buffer[14] = p->split_scroll;
    buffer[15] = p->split_page;
    buffer[16] = p->chr_mode;
    buffer[17] = p->irq_status;
    buffer[18] = p->irq_enable;
    buffer[19] = p->irq_line;
    buffer[20] = p->irq_scanline;
    buffer[21] = p->irq_clear;
    buffer[22] = p->mult_a;
    buffer[23] = p->mult_b;

    for (j = 0; j < 2; j++)
    {
        for (i = 0; i < 8; i++)
        {
            buffer[24 + j * 8 + i] = p->chr_page[j][i];
        }
    }
    //for (i = 0; i < 8; i++)
    //{
    //  buffer[40 + i] = BG_MEM_PAGE[i];
    //}
}
MM_EXPORT_EMU void mmEmuMapper005_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(super);

    mmInt_t i, j;

    p->prg_size = buffer[0];
    p->chr_size = buffer[1];
    p->sram_we_a = buffer[2];
    p->sram_we_b = buffer[3];
    p->graphic_mode = buffer[4];
    p->nametable_mode = buffer[5];
    p->nametable_type[0] = buffer[6];
    p->nametable_type[1] = buffer[7];
    p->nametable_type[2] = buffer[8];
    p->nametable_type[3] = buffer[9];
    p->sram_page = buffer[10];
    p->fill_chr = buffer[11];
    p->fill_pal = buffer[12];
    p->split_control = buffer[13];
    p->split_scroll = buffer[14];
    p->split_page = buffer[15];
    p->chr_mode = buffer[16];
    p->irq_status = buffer[17];
    p->irq_enable = buffer[18];
    p->irq_line = buffer[19];
    p->irq_scanline = buffer[20];
    p->irq_clear = buffer[21];
    p->mult_a = buffer[22];
    p->mult_b = buffer[23];

    for (j = 0; j < 2; j++)
    {
        for (i = 0; i < 8; i++)
        {
            p->chr_page[j][i] = buffer[24 + j * 8 + i];
        }
    }
    //// Reconfiguration process of BG bank.
    //for( i = 0; i < 8; i++ ) 
    //{
    //  BG_MEM_PAGE[i] = buffer[40+i]%VROM_1K_SIZE;
    //}
    //for( i = 0; i < 8; i++ ) 
    //{
    //  BG_MEM_BANK[i] = VROM+0x0400*BG_MEM_PAGE[i];
    //}

    mmEmuMapper005_SetBankPPU(p);
}

static struct mmEmuMapper* __static_mmEmuMapper005_Produce(void)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)mmMalloc(sizeof(struct mmEmuMapper005));
    mmEmuMapper005_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper005_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper005* p = (struct mmEmuMapper005*)(m);
    mmEmuMapper005_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER005 =
{
    &__static_mmEmuMapper005_Produce,
    &__static_mmEmuMapper005_Recycle,
};
