/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper254.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper254_Init(struct mmEmuMapper254* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    p->irq_type = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    p->protectflag = 0;

    //
    p->super.Reset = &mmEmuMapper254_Reset;
    p->super.Write = &mmEmuMapper254_Write;
    //p->super.Read = &mmEmuMapper254_Read;
    p->super.ReadLow = &mmEmuMapper254_ReadLow;
    p->super.WriteLow = &mmEmuMapper254_WriteLow;
    //p->super.ExRead = &mmEmuMapper254_ExRead;
    //p->super.ExWrite = &mmEmuMapper254_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper254_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper254_ExCmdWrite;
    p->super.HSync = &mmEmuMapper254_HSync;
    //p->super.VSync = &mmEmuMapper254_VSync;
    p->super.Clock = &mmEmuMapper254_Clock;
    //p->super.PPULatch = &mmEmuMapper254_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper254_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper254_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper254_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper254_IsStateSave;
    p->super.SaveState = &mmEmuMapper254_SaveState;
    p->super.LoadState = &mmEmuMapper254_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper254_Destroy(struct mmEmuMapper254* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    p->irq_type = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    p->protectflag = 0;
}

MM_EXPORT_EMU void mmEmuMapper254_SetBankCPU(struct mmEmuMapper254* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, p->prg1, p->prg0, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper254_SetBankPPU(struct mmEmuMapper254* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr4, p->chr5, p->chr6, p->chr7,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1);
        }
        else
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1,
                p->chr4, p->chr5, p->chr6, p->chr7);
        }
    }
    else
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 4, (p->chr01 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 5, (p->chr01 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 6, (p->chr23 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 7, (p->chr23 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 0, p->chr4 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 1, p->chr5 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 2, p->chr6 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 3, p->chr7 & 0x07);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 0, (p->chr01 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 1, (p->chr01 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 2, (p->chr23 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 3, (p->chr23 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 4, p->chr4 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 5, p->chr5 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 6, p->chr6 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 7, p->chr7 & 0x07);
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper254_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->protectflag = 0;

    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper254_SetBankCPU(p);

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    mmEmuMapper254_SetBankPPU(p);

    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;
}
MM_EXPORT_EMU void mmEmuMapper254_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->protectflag = 0xFF;
        p->reg[0] = data;
        mmEmuMapper254_SetBankCPU(p);
        mmEmuMapper254_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;

        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr01 = data & 0xFE;
            mmEmuMapper254_SetBankPPU(p);
            break;
        case 0x01:
            p->chr23 = data & 0xFE;
            mmEmuMapper254_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper254_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper254_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper254_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper254_SetBankPPU(p);
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper254_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper254_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        break;
    case 0xC000:
        p->reg[4] = data;
        p->irq_counter = data;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xC001:
        p->reg[5] = data;
        p->irq_latch = data;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_enable = 0;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 1;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU mmByte_t mmEmuMapper254_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x6000)
    {
        if (p->protectflag)
        {
            return (mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF]);
        }
        else
        {
            return ((mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF]) ^ 0x1);
        }
    }
    return mmEmuMapper_ReadLow(super, addr);
}
MM_EXPORT_EMU void mmEmuMapper254_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF000)
    {
    case 0x6000:
    case 0x7000:
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper254_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    //struct mmEmuMapper254* p = (struct mmEmuMapper254*)(super);

    //struct mmEmuNes* nes = p->super.nes;

    //if (p->irq_request && (mmEmuNes_GetIrqType(nes) == MM_EMU_NES_IRQ_CLOCK))
    //{
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    //}
}
MM_EXPORT_EMU void mmEmuMapper254_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable && !p->irq_request)
            {
                if (scanline == 0)
                {
                    if (p->irq_counter)
                    {
                        p->irq_counter--;
                    }
                }
                if (!(p->irq_counter--))
                {
                    p->irq_request = 0xFF;
                    p->irq_counter = p->irq_latch;
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
    //if (p->irq_request && (mmEmuNes_GetIrqType(nes) == MM_EMU_NES_IRQ_HSYNC))
    //{
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    //}
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper254_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper254_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
    buffer[16] = p->irq_enable;
    buffer[17] = p->irq_counter;
    buffer[18] = p->irq_latch;
    buffer[19] = p->irq_request;
    buffer[20] = p->protectflag;
}
MM_EXPORT_EMU void mmEmuMapper254_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
    p->irq_enable = buffer[16];
    p->irq_counter = buffer[17];
    p->irq_latch = buffer[18];
    p->irq_request = buffer[19];
    p->protectflag = buffer[20];
}

static struct mmEmuMapper* __static_mmEmuMapper254_Produce(void)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)mmMalloc(sizeof(struct mmEmuMapper254));
    mmEmuMapper254_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper254_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper254* p = (struct mmEmuMapper254*)(m);
    mmEmuMapper254_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER254 =
{
    &__static_mmEmuMapper254_Produce,
    &__static_mmEmuMapper254_Recycle,
};
