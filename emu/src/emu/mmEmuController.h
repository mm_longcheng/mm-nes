/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuController_h__
#define __mmEmuController_h__

#include "core/mmCore.h"
#include "core/mmKeyCode.h"

#include "container/mmBitset.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

#define MM_EMU_CONTROLLER_KEYBOARD_NUMBER 256

struct mmEmuControllerCursor
{
    mmLong_t x;
    mmLong_t y;

    int button_mask;
};
MM_EXPORT_EMU void mmEmuControllerCursor_Init(struct mmEmuControllerCursor* p);
MM_EXPORT_EMU void mmEmuControllerCursor_Destroy(struct mmEmuControllerCursor* p);
MM_EXPORT_EMU void mmEmuControllerCursor_Reset(struct mmEmuControllerCursor* p);

struct mmEmuNes;

struct mmEmuController
{
    struct mmEmuNes* nes;

    struct mmBitset keyboard;

    struct mmEmuControllerCursor cursor;
};

MM_EXPORT_EMU void mmEmuController_Init(struct mmEmuController* p);
MM_EXPORT_EMU void mmEmuController_Destroy(struct mmEmuController* p);

// parent nes will not reset.
MM_EXPORT_EMU void mmEmuController_Reset(struct mmEmuController* p);

MM_EXPORT_EMU void mmEmuController_SetParent(struct mmEmuController* p, struct mmEmuNes* parent);

MM_EXPORT_EMU void mmEmuController_KeyboardPressed(struct mmEmuController* p, int hId);
MM_EXPORT_EMU void mmEmuController_KeyboardRelease(struct mmEmuController* p, int hId);

// button_mask MM_MB_*******
//    MM_MB_LBUTTON
//    MM_MB_RBUTTON
MM_EXPORT_EMU void mmEmuController_MouseBegan(struct mmEmuController* p, mmLong_t x, mmLong_t y, int button_mask);
MM_EXPORT_EMU void mmEmuController_MouseMoved(struct mmEmuController* p, mmLong_t x, mmLong_t y, int button_mask);
MM_EXPORT_EMU void mmEmuController_MouseEnded(struct mmEmuController* p, mmLong_t x, mmLong_t y, int button_mask);

MM_EXPORT_EMU void mmEmuController_KeyboardReset(struct mmEmuController* p);

MM_EXPORT_EMU mmBool_t mmEmuController_KeyboardStatus(struct mmEmuController* p, int hId);

MM_EXPORT_EMU mmBool_t mmEmuController_ButtonCheck(struct mmEmuController* p, mmInt_t nNo, mmInt_t nID);
MM_EXPORT_EMU mmBool_t mmEmuController_ExButtonCheck(struct mmEmuController* p, mmInt_t nNo, mmInt_t nID);
MM_EXPORT_EMU mmBool_t mmEmuController_NsfButtonCheck(struct mmEmuController* p, mmInt_t nID);
MM_EXPORT_EMU mmBool_t mmEmuController_ButtonCheckKeys(struct mmEmuController* p, mmInt_t nID, mmWord_t* pKey);

// button_id MM_MB_*******
//    MM_MB_LBUTTON
//    MM_MB_RBUTTON
MM_EXPORT_EMU mmBool_t mmEmuController_MouseButtonStatus(struct mmEmuController* p, int button_id);

#include "core/mmSuffix.h"

#endif//__mmEmuController_h__
