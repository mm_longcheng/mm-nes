//
//  ViewController.m
//  mm_emuplayer
//
//  Created by longer on 2021/11/27.
//  Copyright © 2021 longer. All rights reserved.
//

#import "ViewController.h"

#import "mmApplication.h"

#import <Foundation/NSBundle.h>

@interface ViewController()
{
    struct mmApplication hApp;
}

@end

@implementation ViewController

static void __static_ViewController_AttachNotification(ViewController* p);
static void __static_ViewController_DetachNotification(ViewController* p);

- (void)viewDidLoad
{
    [super viewDidLoad];

    int _argc = gArgument.argc;
    const char** _argv = gArgument.argv;
    
    NSBundle* pBundle = [NSBundle mainBundle];
    NSString* pResourcePath = [pBundle resourcePath];
    const char* res = [pResourcePath UTF8String];
    
    char path[128] = { 0 };
    sprintf(path, "%s/asset.zip", res);

    mmApplication_Init(&self->hApp);
    mmEmuEmulator_SetResourceRoot(&self->hApp.impl.emulator, MM_EMU_ASSETS_SOURCE, path, "asset");
    mmApplication_Initialize(&self->hApp,_argc,_argv);
    mmApplication_Start(&self->hApp);
    
    __static_ViewController_AttachNotification(self);
}


- (void)setRepresentedObject:(id)representedObject
{
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)OnApplicationWillUnhide:(NSNotification *)notification
{
//    mmNativeApplication_OnEnterForeground(&self->hApplication);
}
- (void)OnApplicationDidHide:(NSNotification *)notification
{
//    mmNativeApplication_OnEnterBackground(&self->hApplication);
}
- (void)OnApplicationWillBecomeActive:(NSNotification *)notification
{
//    mmNativeApplication_OnEnterForeground(&self->hApplication);
}
- (void)OnApplicationDidResignActive:(NSNotification *)notification
{
//    mmNativeApplication_OnEnterBackground(&self->hApplication);
}
- (void)OnApplicationWillTerminate:(NSNotification *)notification
{
    __static_ViewController_DetachNotification(self);

    mmApplication_Shutdown(&self->hApp);
    mmApplication_Join(&self->hApp);
    mmApplication_Destroy(&self->hApp);
}

static void __static_ViewController_AttachNotification(ViewController* p)
{
    NSNotificationCenter* pNotify = [NSNotificationCenter defaultCenter];
    
    [pNotify addObserver:p selector:@selector(OnApplicationWillUnhide:)
                    name:NSApplicationWillUnhideNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnApplicationDidHide:)
                    name:NSApplicationDidHideNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnApplicationWillBecomeActive:)
                    name:NSApplicationWillBecomeActiveNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnApplicationDidResignActive:)
                    name:NSApplicationDidResignActiveNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnApplicationWillTerminate:)
                    name:NSApplicationWillTerminateNotification
                  object:nil];
}
static void __static_ViewController_DetachNotification(ViewController* p)
{
    NSNotificationCenter* pNotify = [NSNotificationCenter defaultCenter];
    
    [pNotify removeObserver:p name:NSApplicationWillUnhideNotification object:nil];
    [pNotify removeObserver:p name:NSApplicationDidHideNotification object:nil];
    [pNotify removeObserver:p name:NSApplicationWillBecomeActiveNotification object:nil];
    [pNotify removeObserver:p name:NSApplicationDidResignActiveNotification object:nil];
    [pNotify removeObserver:p name:NSApplicationWillTerminateNotification object:nil];
}

@end
