/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper163.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper163_Init(struct mmEmuMapper163* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[1] = 0xFF;
    p->strobe = 1;
    p->security = p->trigger = p->reg[0] = 0x00;

    //
    p->super.Reset = &mmEmuMapper163_Reset;
    //p->super.Write = &mmEmuMapper163_Write;
    //p->super.Read = &mmEmuMapper163_Read;
    p->super.ReadLow = &mmEmuMapper163_ReadLow;
    p->super.WriteLow = &mmEmuMapper163_WriteLow;
    //p->super.ExRead = &mmEmuMapper163_ExRead;
    //p->super.ExWrite = &mmEmuMapper163_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper163_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper163_ExCmdWrite;
    p->super.HSync = &mmEmuMapper163_HSync;
    //p->super.VSync = &mmEmuMapper163_VSync;
    //p->super.Clock = &mmEmuMapper163_Clock;
    //p->super.PPULatch = &mmEmuMapper163_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper163_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper163_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper163_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper163_IsStateSave;
    p->super.SaveState = &mmEmuMapper163_SaveState;
    p->super.LoadState = &mmEmuMapper163_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper163_Destroy(struct mmEmuMapper163* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper163_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper163* p = (struct mmEmuMapper163*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->reg[1] = 0xFF;
    p->strobe = 1;
    p->security = p->trigger = p->reg[0] = 0x00;

    mmEmuMmu_SetPROM32KBank(mmu, 15);
    mmEmuMmu_SetCRAM8KBank(mmu, 0);
}
MM_EXPORT_EMU mmByte_t mmEmuMapper163_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper163* p = (struct mmEmuMapper163*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr >= 0x5000 && addr < 0x6000))
    {
        switch (addr & 0x7700)
        {
        case 0x5100:
            return p->security;
            break;
        case 0x5500:
            if (p->trigger)
            {
                return p->security;
            }
            else
            {
                return 0;
            }
            break;
        default:
            break;
        }
        return 4;
    }
    else if (addr >= 0x6000)
    {
        return mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF];
    }
    return 0;
}
MM_EXPORT_EMU void mmEmuMapper163_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper163* p = (struct mmEmuMapper163*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr >= 0x4020 && addr < 0x6000))
    {
        if (addr == 0x5101)
        {
            if (p->strobe && !data)
            {
                p->trigger ^= 1;
                //p->trigger ^= 0xFF;
            }
            p->strobe = data;
        }
        else if (addr == 0x5100 && data == 6)
        {
            mmEmuMmu_SetPROM32KBank(mmu, 3);
        }
        else
        {
            switch (addr & 0x7300)
            {
            case 0x5000:
                p->reg[1] = data;
                mmEmuMmu_SetPROM32KBank(mmu, (p->reg[1] & 0xF) | (p->reg[0] << 4));
                if (!(p->reg[1] & 0x80) && (mmEmuPpu_GetScanlineNo(&nes->ppu) < 128))
                {
                    mmEmuMmu_SetCRAM8KBank(mmu, 0);
                }
                break;
            case 0x5200:
                p->reg[0] = data;
                mmEmuMmu_SetPROM32KBank(mmu, (p->reg[1] & 0xF) | (p->reg[0] << 4));
                break;
            case 0x5300:
                p->security = data;
                break;
            default:
                break;
            }
        }
    }
    else if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }
}
MM_EXPORT_EMU void mmEmuMapper163_HSync(struct mmEmuMapper* super, mmInt_t scanline)
//void  Mapper163::HSync(int scanline)
{
    struct mmEmuMapper163* p = (struct mmEmuMapper163*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((p->reg[1] & 0x80) && mmEmuPpu_IsDispON(&nes->ppu))
    {
        if (scanline == 127)
        {
            mmEmuMmu_SetCRAM4KBank(mmu, 0, 1);
            mmEmuMmu_SetCRAM4KBank(mmu, 4, 1);
        }
        if (scanline == 239)
        {
            mmEmuMmu_SetCRAM4KBank(mmu, 0, 0);
            mmEmuMmu_SetCRAM4KBank(mmu, 4, 0);
        }
    }
}
MM_EXPORT_EMU mmBool_t mmEmuMapper163_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper163_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper163* p = (struct mmEmuMapper163*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
}

MM_EXPORT_EMU void mmEmuMapper163_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper163* p = (struct mmEmuMapper163*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper163_Produce(void)
{
    struct mmEmuMapper163* p = (struct mmEmuMapper163*)mmMalloc(sizeof(struct mmEmuMapper163));
    mmEmuMapper163_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper163_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper163* p = (struct mmEmuMapper163*)(m);
    mmEmuMapper163_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER163 =
{
    &__static_mmEmuMapper163_Produce,
    &__static_mmEmuMapper163_Recycle,
};
