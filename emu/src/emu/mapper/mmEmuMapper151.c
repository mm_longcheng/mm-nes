/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper151.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper151_Init(struct mmEmuMapper151* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper151_Reset;
    p->super.Write = &mmEmuMapper151_Write;
    //p->super.Read = &mmEmuMapper151_Read;
    //p->super.ReadLow = &mmEmuMapper151_ReadLow;
    //p->super.WriteLow = &mmEmuMapper151_WriteLow;
    //p->super.ExRead = &mmEmuMapper151_ExRead;
    //p->super.ExWrite = &mmEmuMapper151_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper151_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper151_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper151_HSync;
    //p->super.VSync = &mmEmuMapper151_VSync;
    //p->super.Clock = &mmEmuMapper151_Clock;
    //p->super.PPULatch = &mmEmuMapper151_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper151_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper151_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper151_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper151_IsStateSave;
    //p->super.SaveState = &mmEmuMapper151_SaveState;
    //p->super.LoadState = &mmEmuMapper151_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper151_Destroy(struct mmEmuMapper151* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper151_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper151* p = (struct mmEmuMapper151*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmUInt32_t crc = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

#if 0
    crc = mmEmuRom_GetPROMCRC(&nes->rom);
    if (crc == 0x1E438D52)
    {
        mm_emu_screen_SetVsPalette(&nes->screen, 7);//VS_Goonies
    }
    if (crc == 0xD99A2087)
    {
        mm_emu_screen_SetVsPalette(&nes->screen, 6);//VS_Gradius
    }
#endif
}
MM_EXPORT_EMU void mmEmuMapper151_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper151* p = (struct mmEmuMapper151*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0xC000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;
    case 0xE000:
        mmEmuMmu_SetVROM4KBank(mmu, 0, data);
        break;
    case 0xF000:
        mmEmuMmu_SetVROM4KBank(mmu, 4, data);
        break;
    default:
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper151_Produce(void)
{
    struct mmEmuMapper151* p = (struct mmEmuMapper151*)mmMalloc(sizeof(struct mmEmuMapper151));
    mmEmuMapper151_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper151_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper151* p = (struct mmEmuMapper151*)(m);
    mmEmuMapper151_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER151 =
{
    &__static_mmEmuMapper151_Produce,
    &__static_mmEmuMapper151_Recycle,
};
