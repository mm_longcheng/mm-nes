/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper165.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper165_Init(struct mmEmuMapper165* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 4;
    p->chr3 = 4;

    p->we_sram = 0;
    p->latch = 0xFD;

    //
    p->super.Reset = &mmEmuMapper165_Reset;
    p->super.Write = &mmEmuMapper165_Write;
    //p->super.Read = &mmEmuMapper165_Read;
    //p->super.ReadLow = &mmEmuMapper165_ReadLow;
    //p->super.WriteLow = &mmEmuMapper165_WriteLow;
    //p->super.ExRead = &mmEmuMapper165_ExRead;
    //p->super.ExWrite = &mmEmuMapper165_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper165_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper165_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper165_HSync;
    //p->super.VSync = &mmEmuMapper165_VSync;
    //p->super.Clock = &mmEmuMapper165_Clock;
    p->super.PPULatch = &mmEmuMapper165_PPULatch;
    p->super.PPUChrLatch = &mmEmuMapper165_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper165_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper165_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper165_IsStateSave;
    p->super.SaveState = &mmEmuMapper165_SaveState;
    p->super.LoadState = &mmEmuMapper165_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper165_Destroy(struct mmEmuMapper165* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 4;
    p->chr3 = 4;

    p->we_sram = 0;
    p->latch = 0xFD;
}

MM_EXPORT_EMU void mmEmuMapper165_SetBankCPU(struct mmEmuMapper165* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper165_SetBankPPU(struct mmEmuMapper165* p)
{
    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (p->latch == 0xFD)
    {
        mmEmuMapper165_SetBankPPUSUB(p, 0, p->chr0);
        mmEmuMapper165_SetBankPPUSUB(p, 4, p->chr2);
    }
    else
    {
        mmEmuMapper165_SetBankPPUSUB(p, 0, p->chr1);
        mmEmuMapper165_SetBankPPUSUB(p, 4, p->chr3);
    }
}
MM_EXPORT_EMU void mmEmuMapper165_SetBankPPUSUB(struct mmEmuMapper165* p, int bank, int page)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (page == 0)
    {
        mmEmuMmu_SetCRAM4KBank(mmu, bank, page >> 2);
    }
    else
    {
        mmEmuMmu_SetVROM4KBank(mmu, bank, page >> 2);
    }
}
MM_EXPORT_EMU void mmEmuMapper165_Update_Latch(struct mmEmuMapper165* p, mmWord_t addr)
{
    mmWord_t mask = addr & 0x1FF0;

    if (mask == 0x1FD0)
    {
        p->latch = 0xFD;
        mmEmuMapper165_SetBankPPU(p);
    }
    else if (mask == 0x1FE0)
    {
        p->latch = 0xFE;
        mmEmuMapper165_SetBankPPU(p);
    }
}

MM_EXPORT_EMU void mmEmuMapper165_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper165* p = (struct mmEmuMapper165*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper165_SetBankCPU(p);

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 4;
    p->chr3 = 4;
    p->latch = 0xFD;
    mmEmuMapper165_SetBankPPU(p);

    p->we_sram = 0; // Disable

    mmEmuPpu_SetChrLatchMode(&nes->ppu, MM_TRUE);
}
MM_EXPORT_EMU void mmEmuMapper165_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper165* p = (struct mmEmuMapper165*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper165_SetBankCPU(p);
        mmEmuMapper165_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;

        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr0 = data & 0xFC;
            if (p->latch == 0xFD)
            {
                mmEmuMapper165_SetBankPPU(p);
            }
            break;
        case 0x01:
            p->chr1 = data & 0xFC;
            if (p->latch == 0xFE)
            {
                mmEmuMapper165_SetBankPPU(p);
            }
            break;

        case 0x02:
            p->chr2 = data & 0xFC;
            if (p->latch == 0xFD)
            {
                mmEmuMapper165_SetBankPPU(p);
            }
            break;
        case 0x04:
            p->chr3 = data & 0xFC;
            if (p->latch == 0xFE)
            {
                mmEmuMapper165_SetBankPPU(p);
            }
            break;

        case 0x06:
            p->prg0 = data;
            mmEmuMapper165_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper165_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper165_PPULatch(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper165* p = (struct mmEmuMapper165*)(super);

    mmEmuMapper165_Update_Latch(p, addr);
}
MM_EXPORT_EMU void mmEmuMapper165_PPUChrLatch(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper165* p = (struct mmEmuMapper165*)(super);

    mmEmuMapper165_Update_Latch(p, addr);
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper165_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper165_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper165* p = (struct mmEmuMapper165*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr0;
    buffer[11] = p->chr1;
    buffer[12] = p->chr2;
    buffer[13] = p->chr3;
    buffer[14] = p->latch;
}
MM_EXPORT_EMU void mmEmuMapper165_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper165* p = (struct mmEmuMapper165*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr0 = buffer[10];
    p->chr1 = buffer[11];
    p->chr2 = buffer[12];
    p->chr3 = buffer[13];
    p->latch = buffer[14];
}

static struct mmEmuMapper* __static_mmEmuMapper165_Produce(void)
{
    struct mmEmuMapper165* p = (struct mmEmuMapper165*)mmMalloc(sizeof(struct mmEmuMapper165));
    mmEmuMapper165_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper165_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper165* p = (struct mmEmuMapper165*)(m);
    mmEmuMapper165_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER165 =
{
    &__static_mmEmuMapper165_Produce,
    &__static_mmEmuMapper165_Recycle,
};
