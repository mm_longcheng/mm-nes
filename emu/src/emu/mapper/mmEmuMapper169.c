/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper169.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper169_Init(struct mmEmuMapper169* p)
{
    mmEmuMapper_Init(&p->super);

    p->YWRAM = (mmByte_t*)mmMalloc(sizeof(mmByte_t) * 1024 * 1024);
    p->YSRAM = (mmByte_t*)mmMalloc(sizeof(mmByte_t) * 32 * 1024);
    p->YCRAM = (mmByte_t*)mmMalloc(sizeof(mmByte_t) * 128 * 1024);

    p->YX_type = 0;
    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->a3 = 0;
    p->key_map_row = 0;

    p->cmd_4800_6 = 0;
    p->cmd_4800_7 = 0;
    p->cmd_4800_8 = 0;

    p->cmd_5500_3 = 0;
    p->cmd_5500_8 = 0;

    p->cmd_5501_8 = 0;

    p->MMC3_mode = 0;
    p->MMC3_reg = 0;
    p->MMC3_prg0 = 0;
    p->MMC3_prg1 = 0;

    p->MMC3_chr4 = 0;
    p->MMC3_chr5 = 0;
    p->MMC3_chr6 = 0;
    p->MMC3_chr7 = 0;

    mmMemset(p->YWRAM, 0, sizeof(mmByte_t) * 1024 * 1024);
    mmMemset(p->YSRAM, 0, sizeof(mmByte_t) * 32 * 1024);
    mmMemset(p->YCRAM, 0, sizeof(mmByte_t) * 128 * 1024);

    //
    p->super.Reset = &mmEmuMapper169_Reset;
    p->super.Write = &mmEmuMapper169_Write;
    //p->super.Read = &mmEmuMapper169_Read;
    p->super.ReadLow = &mmEmuMapper169_ReadLow;
    p->super.WriteLow = &mmEmuMapper169_WriteLow;
    //p->super.ExRead = &mmEmuMapper169_ExRead;
    //p->super.ExWrite = &mmEmuMapper169_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper169_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper169_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper169_HSync;
    //p->super.VSync = &mmEmuMapper169_VSync;
    //p->super.Clock = &mmEmuMapper169_Clock;
    p->super.PPULatch = &mmEmuMapper169_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper169_PPUChrLatch;
    p->super.PPUExtLatchX = &mmEmuMapper169_PPUExtLatchX;
    p->super.PPUExtLatch = &mmEmuMapper169_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper169_IsStateSave;
    p->super.SaveState = &mmEmuMapper169_SaveState;
    p->super.LoadState = &mmEmuMapper169_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper169_Destroy(struct mmEmuMapper169* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->YX_type = 0;
    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->a3 = 0;
    p->key_map_row = 0;

    p->cmd_4800_6 = 0;
    p->cmd_4800_7 = 0;
    p->cmd_4800_8 = 0;

    p->cmd_5500_3 = 0;
    p->cmd_5500_8 = 0;

    p->cmd_5501_8 = 0;

    p->MMC3_mode = 0;
    p->MMC3_reg = 0;
    p->MMC3_prg0 = 0;
    p->MMC3_prg1 = 0;

    p->MMC3_chr4 = 0;
    p->MMC3_chr5 = 0;
    p->MMC3_chr6 = 0;
    p->MMC3_chr7 = 0;

    mmMemset(p->YWRAM, 0, sizeof(mmByte_t) * 1024 * 1024);
    mmMemset(p->YSRAM, 0, sizeof(mmByte_t) * 32 * 1024);
    mmMemset(p->YCRAM, 0, sizeof(mmByte_t) * 128 * 1024);

    mmFree(p->YWRAM);
    mmFree(p->YSRAM);
    mmFree(p->YCRAM);
}

MM_EXPORT_EMU void mmEmuMapper169_SetBankCPU(struct mmEmuMapper169* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->cmd_5500_3 && p->MMC3_mode)
    {
        return;
    }

    if (p->cmd_5500_3)
    {
        mmEmuMapper169_SetYWRAM16KBank(p, 4, p->reg[5]);
        mmEmuMapper169_SetYWRAM16KBank(p, 6, 0x3F);
    }
    else
    {
        mmEmuMmu_SetPROM32KBank(mmu, p->reg[1] & 0x1F);
    }

    mmEmuMmu_SetPROMBank(mmu, 3, &p->YSRAM[0x2000 * (p->reg[2] & 0x3)], MM_EMU_MMU_BANKTYPE_RAM);
    mmu->CPU_MEM_PAGE[3] = p->reg[2] & 0x3;
}
MM_EXPORT_EMU void mmEmuMapper169_WriteCPUPRAM(struct mmEmuMapper169* p, mmWord_t addr, mmByte_t data)
{
    if (addr < 0xC000)
    {
        p->YWRAM[(addr - 0x8000) + (p->reg[5] * 0x4000)] = data;
    }
    else
    {
        p->YWRAM[addr + 0xF0000] = data;    //HOME BANK
    }
}

MM_EXPORT_EMU void mmEmuMapper169_MMC3WriteH(struct mmEmuMapper169* p, mmWord_t addr, mmByte_t data)
{
    switch (addr)
    {
    case 0x8000:
        p->MMC3_reg = data;
        mmEmuMapper169_MMC3SetBankCPU(p);
        mmEmuMapper169_MMC3SetBankPPU(p);
        break;
    case 0x8001:
        switch (p->MMC3_reg & 0x07)
        {
        case 0x02:
            p->MMC3_chr4 = data;
            mmEmuMapper169_MMC3SetBankPPU(p);
            break;
        case 0x03:
            p->MMC3_chr5 = data;
            mmEmuMapper169_MMC3SetBankPPU(p);
            break;
        case 0x04:
            p->MMC3_chr6 = data;
            mmEmuMapper169_MMC3SetBankPPU(p);
            break;
        case 0x05:
            p->MMC3_chr7 = data;
            mmEmuMapper169_MMC3SetBankPPU(p);
            break;
        case 0x06:
            p->MMC3_prg0 = data;
            mmEmuMapper169_MMC3SetBankCPU(p);
            break;
        case 0x07:
            p->MMC3_prg1 = data;
            mmEmuMapper169_MMC3SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper169_MMC3SetBankCPU(struct mmEmuMapper169* p)
{
    mmEmuMapper169_SetYWRAM32KBankArray(p, p->MMC3_prg0, p->MMC3_prg1, 0x3E, 0x3F);
}
MM_EXPORT_EMU void mmEmuMapper169_MMC3SetBankPPU(struct mmEmuMapper169* p)
{
    mmEmuMapper169_SetYCRAM1KBank(p, 4, p->MMC3_chr4 & 0x0F);
    mmEmuMapper169_SetYCRAM1KBank(p, 5, p->MMC3_chr5 & 0x0F);
    mmEmuMapper169_SetYCRAM1KBank(p, 6, p->MMC3_chr6 & 0x0F);
    mmEmuMapper169_SetYCRAM1KBank(p, 7, p->MMC3_chr7 & 0x0F);
}
//
// for YuXing 98/F 1024K PRam
MM_EXPORT_EMU void mmEmuMapper169_SetYWRAM8KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    bank %= 0x80;
    mmu->CPU_MEM_BANK[page] = p->YWRAM + 0x2000 * bank;
    mmu->CPU_MEM_TYPE[page] = MM_EMU_MMU_BANKTYPE_RAM;
    mmu->CPU_MEM_PAGE[page] = bank;
}
MM_EXPORT_EMU void mmEmuMapper169_SetYWRAM16KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank)
{
    mmEmuMapper169_SetYWRAM8KBank(p, page + 0, bank * 2 + 0);
    mmEmuMapper169_SetYWRAM8KBank(p, page + 1, bank * 2 + 1);
}
MM_EXPORT_EMU void mmEmuMapper169_SetYWRAM32KBank(struct mmEmuMapper169* p, mmInt_t bank)
{
    mmEmuMapper169_SetYWRAM8KBank(p, 4, bank * 4 + 0);
    mmEmuMapper169_SetYWRAM8KBank(p, 5, bank * 4 + 1);
    mmEmuMapper169_SetYWRAM8KBank(p, 6, bank * 4 + 2);
    mmEmuMapper169_SetYWRAM8KBank(p, 7, bank * 4 + 3);
}
MM_EXPORT_EMU void mmEmuMapper169_SetYWRAM32KBankArray(struct mmEmuMapper169* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3)
{
    mmEmuMapper169_SetYWRAM8KBank(p, 4, bank0);
    mmEmuMapper169_SetYWRAM8KBank(p, 5, bank1);
    mmEmuMapper169_SetYWRAM8KBank(p, 6, bank2);
    mmEmuMapper169_SetYWRAM8KBank(p, 7, bank3);
}
// for YuXing 98/F 128K CRam
MM_EXPORT_EMU void mmEmuMapper169_SetYCRAM1KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    bank &= 0x7F;
    mmu->PPU_MEM_BANK[page] = p->YCRAM + 0x0400 * bank;
    mmu->PPU_MEM_TYPE[page] = MM_EMU_MAPPER169_BANKTYPE_YCRAM;
    mmu->PPU_MEM_PAGE[page] = bank;
}
MM_EXPORT_EMU void mmEmuMapper169_SetYCRAM2KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank)
{
    mmEmuMapper169_SetYCRAM1KBank(p, page + 0, bank * 2 + 0);
    mmEmuMapper169_SetYCRAM1KBank(p, page + 1, bank * 2 + 1);
}
MM_EXPORT_EMU void mmEmuMapper169_SetYCRAM4KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank)
{
    mmEmuMapper169_SetYCRAM1KBank(p, page + 0, bank * 4 + 0);
    mmEmuMapper169_SetYCRAM1KBank(p, page + 1, bank * 4 + 1);
    mmEmuMapper169_SetYCRAM1KBank(p, page + 2, bank * 4 + 2);
    mmEmuMapper169_SetYCRAM1KBank(p, page + 3, bank * 4 + 3);
}
MM_EXPORT_EMU void mmEmuMapper169_SetYCRAM8KBank(struct mmEmuMapper169* p, mmInt_t bank)
{
    mmInt_t i = 0;
    for (i = 0; i < 8; i++)
    {
        mmEmuMapper169_SetYCRAM1KBank(p, i, bank * 8 + i);
    }
}

//
// Partially corrected sprite color problem, not completely.
MM_EXPORT_EMU mmByte_t mmEmuMapper169_PPUExtLatchSP(struct mmEmuMapper169* p)
{
    if (p->cmd_4800_8 && !p->cmd_5500_8)
    {
        // For the time being, the specific reason is unknown, 
        // and it is very likely that it is a problem of VirtuaNES itself.
        return 2;
    }
    else
    {
        return 0;
    }
}

MM_EXPORT_EMU void mmEmuMapper169_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x2B1B969E ||    //YX_V8.2-D
        crc == 0xA70FD0F3)      //YX_V8.3-D
    {
        p->YX_type = 0; //Type D
    }
    if (crc == 0x6085FEE8 ||    //YX_V9.0-98
        crc == 0x2A1E4D89 ||    //YX_V9.2-98
        crc == 0x5C6CE13E)      //YX_V9.2-F
    {
        p->YX_type = 1; //Type 98 & Type F
    }

    mmEmuPpu_SetExtLatchMode(&nes->ppu, MM_TRUE);

    p->reg[0] = 0xFF;   //$5002
    p->reg[1] = 0x00;   //$4800
    p->reg[2] = 0x00;   //$5500
    p->reg[3] = 0x00;   //$5501
    p->reg[4] = 0x00;   //PPULatch
    p->reg[5] = 0x00;   //$8000
    p->reg[6] = 0x00;
    p->reg[7] = 0x01;
    p->cmd_4800_6 = 0x00;
    p->cmd_4800_7 = 0x00;
    p->cmd_4800_8 = 0x00;
    p->cmd_5500_3 = 0x00;
    p->cmd_5500_8 = 0x00;
    p->cmd_5501_8 = 0x00;
    p->key_map_row = 0x00;
    p->MMC3_mode = 0x00;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMapper169_SetYCRAM8KBank(p, 0);
}
MM_EXPORT_EMU mmByte_t mmEmuMapper169_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    struct mmEmuController* controller = &nes->controller;

    //  DEBUGOUT( "ReadL  A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, Mapper::ReadLow( addr ), nes->GetScanline(), nes->cpu->GetTotalCycles() );

    //YuXing_Keyboard data by tpu (fix by temryu)
    static const int key_map[14][8] =
    {
        { MM_KC_ESCAPE, MM_KC_F9,           MM_KC_7,        MM_KC_R,       MM_KC_A,      0x00,              0x00,           MM_KC_LSHIFT, },
        { 0x00,         MM_KC_NUMPADENTER,  0x00,           MM_KC_MULTIPLY,MM_KC_DIVIDE, MM_KC_UP,          MM_KC_BACKSPACE,MM_KC_F12, },
        { 0x00,         0x00,               0x00,           MM_KC_ADD,     MM_KC_NUMLOCK,MM_KC_LEFT,        MM_KC_DOWN,     MM_KC_RIGHT, },
        { 0x00,         MM_KC_NUMPAD7,      MM_KC_NUMPAD8,  MM_KC_SUBTRACT,MM_KC_NUMPAD9,MM_KC_F11,         MM_KC_END,      MM_KC_PGDN, },
        { MM_KC_F8,     MM_KC_6,            MM_KC_E,        MM_KC_RBRACKET,MM_KC_L,      MM_KC_Z,           MM_KC_N,        MM_KC_SPACE, },
        { MM_KC_F7,     MM_KC_5,            MM_KC_W,        MM_KC_LBRACKET,MM_KC_K,      MM_KC_DELETE,      MM_KC_B,        MM_KC_SLASH, },
        { MM_KC_F6,     MM_KC_4,            MM_KC_Q,        MM_KC_P,       MM_KC_J,      MM_KC_BACKSLASH,   MM_KC_V,        MM_KC_PERIOD, },
        { MM_KC_F5,     MM_KC_3,            MM_KC_EQUALS,   MM_KC_O,       MM_KC_H,      MM_KC_APOSTROPHE,  MM_KC_C,        MM_KC_COMMA, },
        { MM_KC_F4,     MM_KC_2,            MM_KC_MINUS,    MM_KC_I,       MM_KC_G,      MM_KC_SEMICOLON,   MM_KC_X,        MM_KC_M, },
        { MM_KC_F3,     MM_KC_1,            MM_KC_0,        MM_KC_U,       MM_KC_F,      MM_KC_CAPITAL,     MM_KC_LWIN,     0x00, },
        { MM_KC_F2,     MM_KC_GRAVE,        MM_KC_9,        MM_KC_Y,       MM_KC_D,      MM_KC_LCONTROL,    MM_KC_LMENU,    MM_KC_SCROLL, },
        { MM_KC_F1,     MM_KC_RETURN,       MM_KC_8,        MM_KC_T,       MM_KC_S,      0x00,              MM_KC_RWIN,     MM_KC_APPS, },
        { MM_KC_DECIMAL,MM_KC_F10,          MM_KC_NUMPAD4,  MM_KC_NUMPAD6, MM_KC_NUMPAD5,MM_KC_INSERT,      MM_KC_HOME,     MM_KC_PGUP, },
        { 0x00,         MM_KC_NUMPAD0,      MM_KC_NUMPAD1,  MM_KC_NUMPAD3, MM_KC_NUMPAD2,MM_KC_SYSRQ,       MM_KC_TAB,      MM_KC_PAUSE, },
    };

    int r, i, data;
    int kc = 0;

    switch (addr)
    {
    case 0x4207:    //YuXing_Keyboard code by tpu
        data = 0;
        for (r = 0; r < 14; r++)
        {
            if (p->key_map_row&(1 << r))
            {
                for (i = 0; i < 8; i++)
                {
                    if (key_map[r][i])
                    {
                        kc = key_map[r][i];

                        if (p->YX_type == 0)    //for D (without new keys)
                        {
                            //key_map[9][6] = 0x00;
                            //key_map[11][6] = 0x00;
                            //key_map[11][7] = 0x00;
                            if ((r == 9 && i == 6) ||
                                (r == 11 && i == 6) ||
                                (r == 11 && i == 7))
                            {
                                kc = 0;
                            }
                        }

                        if (mmEmuController_KeyboardStatus(controller, kc)) data |= 1 << i;
                    }
                    //special cases
                    if ((r == 0) && (i == 7))
                    {
                        if (mmEmuController_KeyboardStatus(controller, MM_KC_RSHIFT)) data |= 1 << i;
                    }
                    if ((r == 10) && (i == 5))
                    {
                        if (mmEmuController_KeyboardStatus(controller, MM_KC_RCONTROL)) data |= 1 << i;
                    }
                    if ((r == 10) && (i == 6))
                    {
                        if (mmEmuController_KeyboardStatus(controller, MM_KC_RMENU)) data |= 1 << i;
                    }
                }
            }
        }
        return data;
        break;

    case 0x4304:
        return p->reg[7];
        break;
    case 0x5002:
        return p->reg[0];
        break;
    default:
        break;
    }

    if (addr >= 0x6000)
    {
        return mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF];
    }
    return mmEmuMapper_ReadLow(super, addr);
}
MM_EXPORT_EMU void mmEmuMapper169_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }

    //  if(addr>0x4207) DEBUGOUT( "WriteL A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );
    switch (addr)
    {
    case 0x4201:
        //
        break;

    case 0x4202:    //YuXing_Keyboard
        p->key_map_row &= 0xff00;
        p->key_map_row |= data;
        break;
    case 0x4203:    //YuXing_Keyboard
    case 0x4303:
        p->key_map_row &= 0x00ff;
        p->key_map_row |= (data & 0x3f) << 8;
        break;

    case 0x4800:
        p->reg[1] = data;
        p->cmd_4800_6 = p->reg[1] & 0x20;   //Get the 6th bit, RAM switch, 1 open, 0 closed
        p->cmd_4800_7 = p->reg[1] & 0x40;   //Get the 7th bit
        p->cmd_4800_8 = p->reg[1] & 0x80;   //Get the 8th bit to control PPUExtLatch

        mmEmuMapper169_SetBankCPU(p);
        break;
    case 0x5500:
        p->reg[2] = data;
        p->cmd_5500_3 = p->reg[2] & 0x04;   //Get the third bit, related to control RAM
        p->cmd_5500_8 = p->reg[2] & 0x80;   //Get the 8th bit to control PPULatch

        mmEmuMapper169_SetBankCPU(p);
        break;
    case 0x5501:
        p->reg[3] = data;
        p->cmd_5501_8 = p->reg[3] & 0x80;   //Get the 8th bit, turn on the MMC3 mode, and call the "Magic Sketchpad" option (???)

        mmEmuMapper169_SetYCRAM8KBank(p, p->reg[3] & 0x7F);
        if (p->cmd_5501_8)
        {
            p->MMC3_mode = 1;
            mmEmuMapper169_SetYWRAM16KBank(p, 6, 0x1F);
        }
        else
        {
            p->MMC3_mode = 0;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper169_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    //  DEBUGOUT( "WriteH A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );

    if (p->cmd_5500_3 && p->MMC3_mode)
    {
        mmEmuMapper169_MMC3WriteH(p, addr, data);
        return;
    }

    if (p->cmd_5500_3)
    {
        if (p->cmd_4800_6)
        {
            p->reg[5] = data & 0x3F;
        }
        else
        {
            mmEmuMapper169_WriteCPUPRAM(p, addr, data);
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper169_PPULatch(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xF000) == 0x2000)
    {
        p->reg[4] = (addr >> 8) & 0x03;
        if (p->cmd_4800_8 && p->cmd_5500_8)
        {
            mmEmuMapper169_SetYCRAM8KBank(p, (p->reg[4] << 2) + (p->reg[3] & 0x03));
            mmEmuMapper169_SetYCRAM4KBank(p, 0, p->reg[3] << 1);
        }
    }

    //  if(DirectInput.m_Sw[DIK_PAUSE]) nes->Dump_YWRAM();
}
MM_EXPORT_EMU void mmEmuMapper169_PPUExtLatchX(struct mmEmuMapper* super, mmInt_t x)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    p->a3 = (x & 1) << 3;
}
MM_EXPORT_EMU void mmEmuMapper169_PPUExtLatch(struct mmEmuMapper* super, mmWord_t ntbladr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t loopy_v = mmEmuPpu_GetPPUADDR(&nes->ppu);
    mmInt_t loopy_y = mmEmuPpu_GetTILEY(&nes->ppu);
    mmInt_t tileofs = (mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) << 8;
    mmInt_t attradr = 0x23C0 + (loopy_v & 0x0C00) + ((loopy_v & 0x0380) >> 4);
    mmInt_t attrsft = (ntbladr & 0x0040) >> 4;
    mmByte_t* pNTBL = mmu->PPU_MEM_BANK[ntbladr >> 10];
    mmInt_t ntbl_x = ntbladr & 0x001F;
    mmInt_t tileadr;
    attradr &= 0x3FF;
    (*attr) = ((pNTBL[attradr + (ntbl_x >> 2)] >> ((ntbl_x & 2) + attrsft)) & 3) << 2;
    tileadr = tileofs + pNTBL[ntbladr & 0x03FF] * 0x10 + loopy_y;
    if (p->cmd_4800_8 && !p->cmd_5500_8)
    {
        if (p->reg[4] & 0x02)
        {
            tileadr |= 0x1000;
        }
        else
        {
            tileadr &= ~0x1000;
        }
        tileadr = (tileadr & 0xfff7) | p->a3;
        (*chr_l) = (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
    }
    else
    {
        (*chr_l) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
        (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper169_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper169_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->YX_type;
    buffer[9] = p->a3;
    buffer[10] = p->key_map_row;
    buffer[11] = p->cmd_4800_6;
    buffer[12] = p->cmd_4800_7;
    buffer[13] = p->cmd_4800_8;

    buffer[14] = p->cmd_5500_3;
    buffer[15] = p->cmd_5500_8;

    buffer[16] = p->cmd_5501_8;

    buffer[17] = p->MMC3_mode;
    buffer[18] = p->MMC3_reg;
    buffer[19] = p->MMC3_prg0;
    buffer[20] = p->MMC3_prg1;

    buffer[21] = p->MMC3_chr4;
    buffer[22] = p->MMC3_chr5;
    buffer[23] = p->MMC3_chr6;
    buffer[24] = p->MMC3_chr7;
}
MM_EXPORT_EMU void mmEmuMapper169_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->YX_type = buffer[8];
    p->a3 = buffer[9];
    p->key_map_row = buffer[10];
    p->cmd_4800_6 = buffer[11];
    p->cmd_4800_7 = buffer[12];
    p->cmd_4800_8 = buffer[13];

    p->cmd_5500_3 = buffer[14];
    p->cmd_5500_8 = buffer[15];

    p->cmd_5501_8 = buffer[16];

    p->MMC3_mode = buffer[17];
    p->MMC3_reg = buffer[18];
    p->MMC3_prg0 = buffer[19];
    p->MMC3_prg1 = buffer[20];

    p->MMC3_chr4 = buffer[21];
    p->MMC3_chr5 = buffer[22];
    p->MMC3_chr6 = buffer[23];
    p->MMC3_chr7 = buffer[24];
}

static struct mmEmuMapper* __static_mmEmuMapper169_Produce(void)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)mmMalloc(sizeof(struct mmEmuMapper169));
    mmEmuMapper169_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper169_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper169* p = (struct mmEmuMapper169*)(m);
    mmEmuMapper169_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER169 =
{
    &__static_mmEmuMapper169_Produce,
    &__static_mmEmuMapper169_Recycle,
};
