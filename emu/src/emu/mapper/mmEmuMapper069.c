/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper069.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper069_Init(struct mmEmuMapper069* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper069_Reset;
    p->super.Write = &mmEmuMapper069_Write;
    //p->super.Read = &mmEmuMapper069_Read;
    //p->super.ReadLow = &mmEmuMapper069_ReadLow;
    //p->super.WriteLow = &mmEmuMapper069_WriteLow;
    //p->super.ExRead = &mmEmuMapper069_ExRead;
    //p->super.ExWrite = &mmEmuMapper069_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper069_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper069_ExCmdWrite;
    p->super.HSync = &mmEmuMapper069_HSync;
    //p->super.VSync = &mmEmuMapper069_VSync;
    p->super.Clock = &mmEmuMapper069_Clock;
    //p->super.PPULatch = &mmEmuMapper069_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper069_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper069_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper069_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper069_IsStateSave;
    p->super.SaveState = &mmEmuMapper069_SaveState;
    p->super.LoadState = &mmEmuMapper069_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper069_Destroy(struct mmEmuMapper069* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper069_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper069* p = (struct mmEmuMapper069*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    mmEmuApu_SelectExSound(&nes->apu, 32);
    mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_CLOCK);

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xfeac6916)          // Honoo no Toukyuuji - Dodge Danpei 2(J)
    {
        //mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }

    if (crc == 0xad28aef6)          // Dynamite Batman(J) / Dynamite Batman - Return of the Joker(U)
    {
        p->patch = 1;
    }
}
MM_EXPORT_EMU void mmEmuMapper069_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper069* p = (struct mmEmuMapper069*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE000)
    {
    case 0x8000:
        p->reg = data;
        break;

    case 0xA000:
        switch (p->reg & 0x0F)
        {
        case 0x00:  case 0x01:
        case 0x02:  case 0x03:
        case 0x04:  case 0x05:
        case 0x06:  case 0x07:
            mmEmuMmu_SetVROM1KBank(mmu, p->reg & 0x07, data);
            break;
        case 0x08:
            if (!p->patch && !(data & 0x40))
            {
                mmEmuMmu_SetPROM8KBank(mmu, 3, data);
            }
            break;
        case 0x09:
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
            break;
        case 0x0A:
            mmEmuMmu_SetPROM8KBank(mmu, 5, data);
            break;
        case 0x0B:
            mmEmuMmu_SetPROM8KBank(mmu, 6, data);
            break;

        case 0x0C:
            data &= 0x03;
            if (data == 0)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            else if (data == 1)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else if (data == 2)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
            }
            break;

        case 0x0D:
            p->irq_enable = data;
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            break;

        case 0x0E:
            p->irq_counter = (p->irq_counter & 0xFF00) | data;
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            break;

        case 0x0F:
            p->irq_counter = (p->irq_counter & 0x00FF) | (data << 8);
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            break;
        default:
            break;
        }
        break;

    case 0xC000:
    case 0xE000:
        mmEmuApu_ExWrite(&nes->apu, addr, data);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper069_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper069* p = (struct mmEmuMapper069*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable && (mmEmuNes_GetIrqType(nes) == MM_EMU_NES_IRQ_CLOCK))
    {
        p->irq_counter -= cycles;
        if (p->irq_counter <= 0)
        {
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            p->irq_enable = 0;
            p->irq_counter = 0xFFFF;
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper069_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper069* p = (struct mmEmuMapper069*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable && (mmEmuNes_GetIrqType(nes) == MM_EMU_NES_IRQ_HSYNC))
    {
        p->irq_counter -= 114;
        if (p->irq_counter <= 0)
        {
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            p->irq_enable = 0;
            p->irq_counter = 0xFFFF;
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper069_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper069_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper069* p = (struct mmEmuMapper069*)(super);

    buffer[0] = p->reg;
    buffer[1] = p->irq_enable;
    *(mmInt_t*)&buffer[2] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper069_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper069* p = (struct mmEmuMapper069*)(super);

    p->reg = buffer[0];
    p->irq_enable = buffer[1];
    p->irq_counter = *(mmInt_t*)&buffer[2];
}

static struct mmEmuMapper* __static_mmEmuMapper069_Produce(void)
{
    struct mmEmuMapper069* p = (struct mmEmuMapper069*)mmMalloc(sizeof(struct mmEmuMapper069));
    mmEmuMapper069_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper069_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper069* p = (struct mmEmuMapper069*)(m);
    mmEmuMapper069_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER069 =
{
    &__static_mmEmuMapper069_Produce,
    &__static_mmEmuMapper069_Recycle,
};
