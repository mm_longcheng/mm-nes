/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuPpu_h__
#define __mmEmuPpu_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

// PPU Control Register #1  PPU #0
#define MM_EMU_PPU_VBLANK_BIT       0x80
#define MM_EMU_PPU_SPHIT_BIT        0x40
#define MM_EMU_PPU_SP16_BIT         0x20
#define MM_EMU_PPU_BGTBL_BIT        0x10
#define MM_EMU_PPU_SPTBL_BIT        0x08
#define MM_EMU_PPU_INC32_BIT        0x04
#define MM_EMU_PPU_NAMETBL_BIT      0x03

// PPU Control Register #2  PPU #1
#define MM_EMU_PPU_BGCOLOR_BIT      0xE0
#define MM_EMU_PPU_SPDISP_BIT       0x10
#define MM_EMU_PPU_BGDISP_BIT       0x08
#define MM_EMU_PPU_SPCLIP_BIT       0x04
#define MM_EMU_PPU_BGCLIP_BIT       0x02
#define MM_EMU_PPU_COLORMODE_BIT    0x01

// PPU Status Register      PPU #2
#define MM_EMU_PPU_VBLANK_FLAG      0x80
#define MM_EMU_PPU_SPHIT_FLAG       0x40
#define MM_EMU_PPU_SPMAX_FLAG       0x20
#define MM_EMU_PPU_WENABLE_FLAG     0x10

// SPRITE Attribute
#define MM_EMU_PPU_SP_VMIRROR_BIT   0x80
#define MM_EMU_PPU_SP_HMIRROR_BIT   0x40
#define MM_EMU_PPU_SP_PRIORITY_BIT  0x20
#define MM_EMU_PPU_SP_COLOR_BIT     0x03

enum
{
    MM_EMU_PPU_SCREEN_W = 256 + 16, // width
    MM_EMU_PPU_SCREEN_H = 240,      // height
};

struct mmEmuPpuSprite
{
    mmByte_t    y;
    mmByte_t    tile;
    mmByte_t    attr;
    mmByte_t    x;
};

MM_EXPORT_EMU extern const mmByte_t MM_EMU_PPU_VSCOLORMAP[5][64];

struct mmEmuNes;

struct mmEmuPpu
{
    struct mmEmuNes* nes;

    mmBool_t    bExtLatch;      // For MMC5
    mmBool_t    bChrLatch;      // For MMC2/MMC4
    mmBool_t    bExtNameTable;  // For Super Monkey no Dai Bouken
    mmBool_t    bExtMono;       // For Final Fantasy

    mmWord_t    loopy_y;
    mmWord_t    loopy_shift;

    mmByte_t*   lpScreen;
    mmByte_t*   lpScanline;
    mmInt_t     ScanlineNo;
    mmByte_t*   lpColormode;

    // Reversed bits
    mmByte_t    Bit2Rev[256];

    // For VS-Unisystem
    mmBool_t    bVSMode;
    mmByte_t    VSSecurityData;
    mmInt_t     nVSColorMap;
};

MM_EXPORT_EMU void mmEmuPpu_Init(struct mmEmuPpu* p);
MM_EXPORT_EMU void mmEmuPpu_Destroy(struct mmEmuPpu* p);

MM_EXPORT_EMU void mmEmuPpu_SetParent(struct mmEmuPpu* p, struct mmEmuNes* parent);

MM_EXPORT_EMU void mmEmuPpu_Reset(struct mmEmuPpu* p);

MM_EXPORT_EMU mmByte_t mmEmuPpu_Read(struct mmEmuPpu* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuPpu_Write(struct mmEmuPpu* p, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuPpu_DMA(struct mmEmuPpu* p, mmByte_t data);

MM_EXPORT_EMU void mmEmuPpu_VBlankStart(struct mmEmuPpu* p);
MM_EXPORT_EMU void mmEmuPpu_VBlankEnd(struct mmEmuPpu* p);

MM_EXPORT_EMU void mmEmuPpu_FrameStart(struct mmEmuPpu* p);
MM_EXPORT_EMU void mmEmuPpu_FrameEnd(struct mmEmuPpu* p);

MM_EXPORT_EMU void mmEmuPpu_SetRenderScanline(struct mmEmuPpu* p, mmInt_t scanline);

MM_EXPORT_EMU void mmEmuPpu_ScanlineStart(struct mmEmuPpu* p);
MM_EXPORT_EMU void mmEmuPpu_ScanlineNext(struct mmEmuPpu* p);

MM_EXPORT_EMU mmWord_t mmEmuPpu_GetPPUADDR(const struct mmEmuPpu* p);
static mmInline mmWord_t mmEmuPpu_GetTILEY(const struct mmEmuPpu* p) { return p->loopy_y; }

// Scanline base render
MM_EXPORT_EMU void mmEmuPpu_Scanline(struct mmEmuPpu* p, mmInt_t scanline, mmBool_t bMax, mmBool_t bLeftClip);
MM_EXPORT_EMU void mmEmuPpu_DummyScanline(struct mmEmuPpu* p, mmInt_t scanline);

// For mapper
static mmInline void mmEmuPpu_SetExtLatchMode(struct mmEmuPpu* p, mmBool_t bMode) { p->bExtLatch = bMode; }
static mmInline void mmEmuPpu_SetChrLatchMode(struct mmEmuPpu* p, mmBool_t bMode) { p->bChrLatch = bMode; }
static mmInline void mmEmuPpu_SetExtNameTableMode(struct mmEmuPpu* p, mmBool_t bMode) { p->bExtNameTable = bMode; }
static mmInline void mmEmuPpu_SetExtMonoMode(struct mmEmuPpu* p, mmBool_t bMode) { p->bExtMono = bMode; }
static mmInline mmBool_t mmEmuPpu_GetExtMonoMode(const struct mmEmuPpu* p) { return p->bExtMono; }

MM_EXPORT_EMU mmBool_t mmEmuPpu_IsDispON(const struct mmEmuPpu* p);
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsBGON(const struct mmEmuPpu* p);
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSPON(const struct mmEmuPpu* p);

MM_EXPORT_EMU mmByte_t mmEmuPpu_GetBGCOLOR(const struct mmEmuPpu* p);

MM_EXPORT_EMU mmBool_t mmEmuPpu_IsBGCLIP(const struct mmEmuPpu* p);
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSPCLIP(const struct mmEmuPpu* p);

MM_EXPORT_EMU mmBool_t mmEmuPpu_IsMONOCROME(const struct mmEmuPpu* p);

MM_EXPORT_EMU mmBool_t mmEmuPpu_IsVBLANK(const struct mmEmuPpu* p);
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSPHIT(const struct mmEmuPpu* p);
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSPMAX(const struct mmEmuPpu* p);
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsPPUWE(const struct mmEmuPpu* p);

// Line that Sprite 0 might hit?
MM_EXPORT_EMU mmBool_t mmEmuPpu_IsSprite0(const struct mmEmuPpu* p, mmInt_t scanline);

static mmInline void mmEmuPpu_SetScreenPtr(struct mmEmuPpu* p, mmByte_t* lpScn) { p->lpScreen = lpScn; }
static mmInline mmByte_t* mmEmuPpu_GetScreenPtr(const struct mmEmuPpu* p) { return p->lpScreen; }

static mmInline void mmEmuPpu_SetColormodePtr(struct mmEmuPpu* p, mmByte_t* lpMode) { p->lpColormode = lpMode; }
static mmInline mmByte_t* mmEmuPpu_GetColormodePtr(const struct mmEmuPpu* p, mmByte_t* lpMode) { return p->lpColormode; }

static mmInline mmInt_t mmEmuPpu_GetScanlineNo(const struct mmEmuPpu* p) { return p->ScanlineNo; }

// For VS-Unisystem
static mmInline void mmEmuPpu_SetVSMode(struct mmEmuPpu* p, mmBool_t bMode) { p->bVSMode = bMode; }
static mmInline void mmEmuPpu_SetVSSecurity(struct mmEmuPpu* p, mmByte_t data) { p->VSSecurityData = data; }
static mmInline void mmEmuPpu_SetVSColorMap(struct mmEmuPpu* p, mmInt_t nColorMap) { p->nVSColorMap = nColorMap; }

#include "core/mmSuffix.h"

#endif//__mmEmuPpu_h__
