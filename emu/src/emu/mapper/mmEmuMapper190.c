/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper190.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper190_Init(struct mmEmuMapper190* p)
{
    mmEmuMapper_Init(&p->super);

    p->cbase = 0;
    p->mp190_lcchk = 0;
    p->mp190_lcmd = 0;
    p->mp190_cmd = 1;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->lowoutdata = 0;

    //
    p->super.Reset = &mmEmuMapper190_Reset;
    p->super.Write = &mmEmuMapper190_Write;
    //p->super.Read = &mmEmuMapper190_Read;
    p->super.ReadLow = &mmEmuMapper190_ReadLow;
    p->super.WriteLow = &mmEmuMapper190_WriteLow;
    //p->super.ExRead = &mmEmuMapper190_ExRead;
    //p->super.ExWrite = &mmEmuMapper190_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper190_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper190_ExCmdWrite;
    p->super.HSync = &mmEmuMapper190_HSync;
    //p->super.VSync = &mmEmuMapper190_VSync;
    //p->super.Clock = &mmEmuMapper190_Clock;
    //p->super.PPULatch = &mmEmuMapper190_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper190_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper190_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper190_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper190_IsStateSave;
    p->super.SaveState = &mmEmuMapper190_SaveState;
    p->super.LoadState = &mmEmuMapper190_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper190_Destroy(struct mmEmuMapper190* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->cbase = 0;
    p->mp190_lcchk = 0;
    p->mp190_lcmd = 0;
    p->mp190_cmd = 1;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->lowoutdata = 0;
}

MM_EXPORT_EMU void mmEmuMapper190_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmUInt32_t crc = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    //crc = mmEmuRom_GetPROMCRC(&nes->rom);
    //if (crc == 0x6F3D187A)
    //{
    //  Temp_Buf = 0;   //Kof96
    //}
    //else
    //{
    //  Temp_Buf = 1;   //ST97
    //}

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->cbase = 0;       /* PowerOn OR RESET : cbase=0 */
    p->mp190_lcchk = 0; /* PowerOn OR RESET */
    p->mp190_lcmd = 1;
}
MM_EXPORT_EMU void mmEmuMapper190_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE003)
    {
    case 0x8000:
        p->mp190_cmd = data;
        if (p->mp190_cmd & 0x80)
        {
            p->cbase = 1;
        }
        else
        {
            p->cbase = 0;
        }
        break;
    case 0x8003:    /* for Street Fighter Zero 2 '97 */
        p->mp190_lcchk = data;
        switch (data)
        {
        case 0x28:
            mmEmuMmu_SetPROM8KBank(mmu, 4, 0x1F);
            mmEmuMmu_SetPROM8KBank(mmu, 5, 0x1F);
            mmEmuMmu_SetPROM8KBank(mmu, 6, 0x17);
            mmEmuMmu_SetPROM8KBank(mmu, 7, 0x1F);
            break;
        case 0x2A:
            mmEmuMmu_SetPROM8KBank(mmu, 4, 0x1F);
            mmEmuMmu_SetPROM8KBank(mmu, 5, 0x0F);
            mmEmuMmu_SetPROM8KBank(mmu, 6, 0x17);
            mmEmuMmu_SetPROM8KBank(mmu, 7, 0x1F);
            break;
        case 0x06:
            mmEmuMmu_SetPROM8KBank(mmu, 4, 0x1E);
            mmEmuMmu_SetPROM8KBank(mmu, 5, 0x1F);
            mmEmuMmu_SetPROM8KBank(mmu, 6, 0x1F);
            mmEmuMmu_SetPROM8KBank(mmu, 7, 0x1F);
            break;
        default:
            break;
        }
        break;
    case 0x8001:
        if ((p->mp190_lcchk == 0x6) || (p->mp190_lcmd == 0x0))
        {
            switch (p->mp190_cmd & 0x07)
            {
            case 0:
                if (p->cbase == 0)
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 0, data + 0x100);
                    mmEmuMmu_SetVROM1KBank(mmu, 1, data + 0x101);
                }
                else
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 4, data + 0x100);
                    mmEmuMmu_SetVROM1KBank(mmu, 5, data + 0x101);
                }
                break;
            case 1:
                if (p->cbase == 0)
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 2, data + 0x100);
                    mmEmuMmu_SetVROM1KBank(mmu, 3, data + 0x101);
                }
                else
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 6, data + 0x100);
                    mmEmuMmu_SetVROM1KBank(mmu, 7, data + 0x101);
                }
                break;
            case 2:
                if (p->cbase == 0)
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 4, data);
                }
                else
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 0, data);
                }
                break;
            case 3:
                if (p->cbase == 0)
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 5, data);
                }
                else
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 1, data);
                }
                break;
            case 4:
                if (p->cbase == 0)
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 6, data);
                }
                else
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 2, data);
                }
                break;
            case 5:
                if (p->cbase == 0)
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 7, data);
                }
                else
                {
                    mmEmuMmu_SetVROM1KBank(mmu, 3, data);
                }
                break;
            case 6:
                data = data & ((mmu->PROM_08K_SIZE * 2) - 1);
                if (p->mp190_lcmd & 0x40)
                {
                    mmEmuMmu_SetPROM8KBank(mmu, 6, data);
                    mmEmuMmu_SetPROM8KBank(mmu, 4, (mmu->PROM_08K_SIZE - 1) * 2);
                }
                else
                {
                    mmEmuMmu_SetPROM8KBank(mmu, 4, data);
                    mmEmuMmu_SetPROM8KBank(mmu, 6, (mmu->PROM_08K_SIZE - 1) * 2);
                }
                break;
            case 7:
                data = data & ((mmu->PROM_08K_SIZE * 2) - 1);
                if (p->mp190_lcmd & 0x40)
                {
                    mmEmuMmu_SetPROM8KBank(mmu, 5, data);
                    mmEmuMmu_SetPROM8KBank(mmu, 4, (mmu->PROM_08K_SIZE - 1) * 2);
                }
                else
                {
                    mmEmuMmu_SetPROM8KBank(mmu, 5, data);
                    mmEmuMmu_SetPROM8KBank(mmu, 6, (mmu->PROM_08K_SIZE - 1) * 2);
                }
                break;
            default:
                break;
            }
        }
        break;
    case 0xA000:
        if ((data & 0x1) == 0x1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    case 0xA001:
        break;
    case 0xC000:
        p->irq_counter = data - 1;
        break;
    case 0xC001:
        p->irq_latch = data - 1;
        break;
    case 0xC002:
        p->irq_counter = data;
        break;
    case 0xC003:
        p->irq_latch = data;
        break;
    case 0xE000:
        p->irq_counter = p->irq_latch;
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->irq_enable = 1;
        break;
    case 0xE002:
        p->irq_counter = p->irq_latch;
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE003:
        p->irq_enable = 1;
        p->irq_counter = p->irq_counter;
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU mmByte_t mmEmuMapper190_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x5000:
        return p->lowoutdata;
    default:
        return mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF];
    }
}
MM_EXPORT_EMU void mmEmuMapper190_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    /* For Initial Copy Protect check (KOF'96) */
    if (addr == 0x5000)
    {
        p->mp190_lcmd = data;
        switch (data)
        {
        case 0xE0:
            mmEmuMmu_SetPROM32KBank(mmu, 0);
            break;
        case 0xEE:
            mmEmuMmu_SetPROM32KBank(mmu, 3);
            break;
        default:
            break;
        }
    }
    if ((addr == 0x5001) && (p->mp190_lcmd == 0x00))
    {
        mmEmuMmu_SetPROM32KBank(mmu, 7);
    }
    if (addr == 0x5080)
    {
        switch (data)
        {
        case 0x1:
            p->lowoutdata = 0x83;
            break;
        case 0x2:
            p->lowoutdata = 0x42;
            break;
        case 0x3:
            p->lowoutdata = 0x00;
            break;
        default:
            break;
        }
    }
    mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
}

MM_EXPORT_EMU void mmEmuMapper190_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(p->irq_counter--))
                {
                    //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper190_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper190_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_counter;
    buffer[2] = p->irq_latch;

    buffer[3] = p->cbase;
    buffer[4] = p->mp190_lcchk;
    buffer[5] = p->mp190_lcmd;
    buffer[6] = p->mp190_cmd;
    buffer[7] = p->lowoutdata;
}
MM_EXPORT_EMU void mmEmuMapper190_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = buffer[1];
    p->irq_latch = buffer[2];

    p->cbase = buffer[3];
    p->mp190_lcchk = buffer[4];
    p->mp190_lcmd = buffer[5];
    p->mp190_cmd = buffer[6];
    p->lowoutdata = buffer[7];
}

static struct mmEmuMapper* __static_mmEmuMapper190_Produce(void)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)mmMalloc(sizeof(struct mmEmuMapper190));
    mmEmuMapper190_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper190_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper190* p = (struct mmEmuMapper190*)(m);
    mmEmuMapper190_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER190 =
{
    &__static_mmEmuMapper190_Produce,
    &__static_mmEmuMapper190_Recycle,
};
