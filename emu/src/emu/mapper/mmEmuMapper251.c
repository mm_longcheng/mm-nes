/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper251.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper251_Init(struct mmEmuMapper251* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 11);
    mmMemset(p->breg, 0, sizeof(mmByte_t) * 4);

    //
    p->super.Reset = &mmEmuMapper251_Reset;
    p->super.Write = &mmEmuMapper251_Write;
    //p->super.Read = &mmEmuMapper251_Read;
    //p->super.ReadLow = &mmEmuMapper251_ReadLow;
    p->super.WriteLow = &mmEmuMapper251_WriteLow;
    //p->super.ExRead = &mmEmuMapper251_ExRead;
    //p->super.ExWrite = &mmEmuMapper251_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper251_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper251_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper251_HSync;
    //p->super.VSync = &mmEmuMapper251_VSync;
    //p->super.Clock = &mmEmuMapper251_Clock;
    //p->super.PPULatch = &mmEmuMapper251_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper251_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper251_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper251_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper251_IsStateSave;
    p->super.SaveState = &mmEmuMapper251_SaveState;
    p->super.LoadState = &mmEmuMapper251_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper251_Destroy(struct mmEmuMapper251* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 11);
    mmMemset(p->breg, 0, sizeof(mmByte_t) * 4);
}

MM_EXPORT_EMU void mmEmuMapper251_SetBank(struct mmEmuMapper251* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t chr[6];
    mmInt_t prg[4];

    mmInt_t i = 0;

    for (i = 0; i < 6; i++)
    {
        chr[i] = (p->reg[i] | (p->breg[1] << 4)) & ((p->breg[2] << 4) | 0x0F);
    }

    if (p->reg[8] & 0x80)
    {
        mmEmuMmu_SetVROM8KBankArray(mmu, chr[2], chr[3], chr[4], chr[5], chr[0], chr[0] + 1, chr[1], chr[1] + 1);
    }
    else
    {
        mmEmuMmu_SetVROM8KBankArray(mmu, chr[0], chr[0] + 1, chr[1], chr[1] + 1, chr[2], chr[3], chr[4], chr[5]);
    }

    prg[0] = (p->reg[6] & ((p->breg[3] & 0x3F) ^ 0x3F)) | (p->breg[1]);
    prg[1] = (p->reg[7] & ((p->breg[3] & 0x3F) ^ 0x3F)) | (p->breg[1]);
    prg[2] = prg[3] = ((p->breg[3] & 0x3F) ^ 0x3F) | (p->breg[1]);
    prg[2] &= mmu->PROM_08K_SIZE - 1;

    if (p->reg[8] & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, prg[2], prg[1], prg[0], prg[3]);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, prg[0], prg[1], prg[2], prg[3]);
    }
}

MM_EXPORT_EMU void mmEmuMapper251_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper251* p = (struct mmEmuMapper251*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 11);
    mmMemset(p->breg, 0, sizeof(mmByte_t) * 4);
}
MM_EXPORT_EMU void mmEmuMapper251_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper251* p = (struct mmEmuMapper251*)(super);

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[8] = data;
        mmEmuMapper251_SetBank(p);
        break;
    case 0x8001:
        p->reg[p->reg[8] & 0x07] = data;
        mmEmuMapper251_SetBank(p);
        break;
    case 0xA001:
        if (data & 0x80)
        {
            p->reg[9] = 1;
            p->reg[10] = 0;
        }
        else
        {
            p->reg[9] = 0;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper251_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper251* p = (struct mmEmuMapper251*)(super);

    if ((addr & 0xE001) == 0x6000)
    {
        if (p->reg[9])
        {
            p->breg[p->reg[10]++] = data;
            if (p->reg[10] == 4)
            {
                p->reg[10] = 0;
                mmEmuMapper251_SetBank(p);
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper251_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper251_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper251* p = (struct mmEmuMapper251*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 11);
    mmMemcpy(&buffer[11], &p->breg[0], sizeof(mmByte_t) * 4);
}
MM_EXPORT_EMU void mmEmuMapper251_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper251* p = (struct mmEmuMapper251*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 11);
    mmMemcpy(&p->breg[0], &buffer[11], sizeof(mmByte_t) * 4);
}

static struct mmEmuMapper* __static_mmEmuMapper251_Produce(void)
{
    struct mmEmuMapper251* p = (struct mmEmuMapper251*)mmMalloc(sizeof(struct mmEmuMapper251));
    mmEmuMapper251_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper251_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper251* p = (struct mmEmuMapper251*)(m);
    mmEmuMapper251_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER251 =
{
    &__static_mmEmuMapper251_Produce,
    &__static_mmEmuMapper251_Recycle,
};
