/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper096.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper096_Init(struct mmEmuMapper096* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 0;

    //
    p->super.Reset = &mmEmuMapper096_Reset;
    p->super.Write = &mmEmuMapper096_Write;
    //p->super.Read = &mmEmuMapper096_Read;
    //p->super.ReadLow = &mmEmuMapper096_ReadLow;
    //p->super.WriteLow = &mmEmuMapper096_WriteLow;
    //p->super.ExRead = &mmEmuMapper096_ExRead;
    //p->super.ExWrite = &mmEmuMapper096_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper096_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper096_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper096_HSync;
    //p->super.VSync = &mmEmuMapper096_VSync;
    //p->super.Clock = &mmEmuMapper096_Clock;
    p->super.PPULatch = &mmEmuMapper096_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper096_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper096_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper096_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper096_IsStateSave;
    p->super.SaveState = &mmEmuMapper096_SaveState;
    p->super.LoadState = &mmEmuMapper096_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper096_Destroy(struct mmEmuMapper096* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 0;
}

MM_EXPORT_EMU void mmEmuMapper096_SetBank(struct mmEmuMapper096* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetCRAM4KBank(mmu, 0, p->reg[0] * 4 + p->reg[1]);
    mmEmuMmu_SetCRAM4KBank(mmu, 4, p->reg[0] * 4 + 0x03);
}

MM_EXPORT_EMU void mmEmuMapper096_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper096* p = (struct mmEmuMapper096*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->reg[0] = p->reg[1] = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);

    mmEmuMapper096_SetBank(p);

    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
}
MM_EXPORT_EMU void mmEmuMapper096_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper096* p = (struct mmEmuMapper096*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, data & 0x03);

    p->reg[0] = (data & 0x04) >> 2;

    mmEmuMapper096_SetBank(p);
}

MM_EXPORT_EMU void mmEmuMapper096_PPULatch(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper096* p = (struct mmEmuMapper096*)(super);

    if ((addr & 0xF000) == 0x2000)
    {
        p->reg[1] = (addr >> 8) & 0x03;
        mmEmuMapper096_SetBank(p);
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper096_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper096_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper096* p = (struct mmEmuMapper096*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
}
MM_EXPORT_EMU void mmEmuMapper096_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper096* p = (struct mmEmuMapper096*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper096_Produce(void)
{
    struct mmEmuMapper096* p = (struct mmEmuMapper096*)mmMalloc(sizeof(struct mmEmuMapper096));
    mmEmuMapper096_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper096_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper096* p = (struct mmEmuMapper096*)(m);
    mmEmuMapper096_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER096 =
{
    &__static_mmEmuMapper096_Produce,
    &__static_mmEmuMapper096_Recycle,
};
