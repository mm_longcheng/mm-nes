/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper066.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper066_Init(struct mmEmuMapper066* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper066_Reset;
    p->super.Write = &mmEmuMapper066_Write;
    //p->super.Read = &mmEmuMapper066_Read;
    //p->super.ReadLow = &mmEmuMapper066_ReadLow;
    p->super.WriteLow = &mmEmuMapper066_WriteLow;
    //p->super.ExRead = &mmEmuMapper066_ExRead;
    //p->super.ExWrite = &mmEmuMapper066_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper066_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper066_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper066_HSync;
    //p->super.VSync = &mmEmuMapper066_VSync;
    //p->super.Clock = &mmEmuMapper066_Clock;
    //p->super.PPULatch = &mmEmuMapper066_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper066_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper066_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper066_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper066_IsStateSave;
    //p->super.SaveState = &mmEmuMapper066_SaveState;
    //p->super.LoadState = &mmEmuMapper066_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper066_Destroy(struct mmEmuMapper066* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper066_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper066* p = (struct mmEmuMapper066*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmUInt32_t crc = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    //crc = mmEmuRom_GetPROMCRC(&nes->rom);

    //if(crc == 0xe30552db)             // Paris-Dakar Rally Special
    //{ 
    //  mmEmuNes_SetFrameIRQmode(nes, MM_FALSE);
    //}
}
MM_EXPORT_EMU void mmEmuMapper066_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper066* p = (struct mmEmuMapper066*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x6000)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (data & 0xF0) >> 4);
        mmEmuMmu_SetVROM8KBank(mmu, data & 0x0F);
    }
}
MM_EXPORT_EMU void mmEmuMapper066_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper066* p = (struct mmEmuMapper066*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, (data & 0xF0) >> 4);
    mmEmuMmu_SetVROM8KBank(mmu, data & 0x0F);
}

static struct mmEmuMapper* __static_mmEmuMapper066_Produce(void)
{
    struct mmEmuMapper066* p = (struct mmEmuMapper066*)mmMalloc(sizeof(struct mmEmuMapper066));
    mmEmuMapper066_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper066_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper066* p = (struct mmEmuMapper066*)(m);
    mmEmuMapper066_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER066 =
{
    &__static_mmEmuMapper066_Produce,
    &__static_mmEmuMapper066_Recycle,
};
