/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper057.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper057_Init(struct mmEmuMapper057* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0;

    //
    p->super.Reset = &mmEmuMapper057_Reset;
    p->super.Write = &mmEmuMapper057_Write;
    //p->super.Read = &mmEmuMapper057_Read;
    //p->super.ReadLow = &mmEmuMapper057_ReadLow;
    //p->super.WriteLow = &mmEmuMapper057_WriteLow;
    //p->super.ExRead = &mmEmuMapper057_ExRead;
    //p->super.ExWrite = &mmEmuMapper057_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper057_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper057_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper057_HSync;
    //p->super.VSync = &mmEmuMapper057_VSync;
    //p->super.Clock = &mmEmuMapper057_Clock;
    //p->super.PPULatch = &mmEmuMapper057_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper057_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper057_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper057_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper057_IsStateSave;
    p->super.SaveState = &mmEmuMapper057_SaveState;
    p->super.LoadState = &mmEmuMapper057_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper057_Destroy(struct mmEmuMapper057* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0;
}

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper057_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper057* p = (struct mmEmuMapper057*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 0, 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    p->reg = 0;
}
MM_EXPORT_EMU void mmEmuMapper057_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper057* p = (struct mmEmuMapper057*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
    case 0x8001:
    case 0x8002:
    case 0x8003:
        if (data & 0x40)
        {
            mmEmuMmu_SetVROM8KBank(mmu, (data & 0x03) + ((p->reg & 0x10) >> 1) + (p->reg & 0x07));
        }
        break;
    case 0x8800:
        p->reg = data;

        if (data & 0x80)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, ((data & 0x40) >> 6) * 4 + 8 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 5, ((data & 0x40) >> 6) * 4 + 8 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 6, ((data & 0x40) >> 6) * 4 + 8 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 7, ((data & 0x40) >> 6) * 4 + 8 + 3);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, ((data & 0x60) >> 5) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 5, ((data & 0x60) >> 5) * 2 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 6, ((data & 0x60) >> 5) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, ((data & 0x60) >> 5) * 2 + 1);
        }

        mmEmuMmu_SetVROM8KBank(mmu, (data & 0x07) + ((data & 0x10) >> 1));

        if (data & 0x08)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper057_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper057_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper057* p = (struct mmEmuMapper057*)(super);

    buffer[0] = p->reg;
}
MM_EXPORT_EMU void mmEmuMapper057_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper057* p = (struct mmEmuMapper057*)(super);

    p->reg = buffer[0];
}

static struct mmEmuMapper* __static_mmEmuMapper057_Produce(void)
{
    struct mmEmuMapper057* p = (struct mmEmuMapper057*)mmMalloc(sizeof(struct mmEmuMapper057));
    mmEmuMapper057_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper057_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper057* p = (struct mmEmuMapper057*)(m);
    mmEmuMapper057_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER057 =
{
    &__static_mmEmuMapper057_Produce,
    &__static_mmEmuMapper057_Recycle,
};
