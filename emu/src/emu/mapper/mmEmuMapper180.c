/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper180.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper180_Init(struct mmEmuMapper180* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper180_Reset;
    p->super.Write = &mmEmuMapper180_Write;
    //p->super.Read = &mmEmuMapper180_Read;
    //p->super.ReadLow = &mmEmuMapper180_ReadLow;
    //p->super.WriteLow = &mmEmuMapper180_WriteLow;
    //p->super.ExRead = &mmEmuMapper180_ExRead;
    //p->super.ExWrite = &mmEmuMapper180_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper180_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper180_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper180_HSync;
    //p->super.VSync = &mmEmuMapper180_VSync;
    //p->super.Clock = &mmEmuMapper180_Clock;
    //p->super.PPULatch = &mmEmuMapper180_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper180_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper180_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper180_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper180_IsStateSave;
    //p->super.SaveState = &mmEmuMapper180_SaveState;
    //p->super.LoadState = &mmEmuMapper180_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper180_Destroy(struct mmEmuMapper180* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper180_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper180* p = (struct mmEmuMapper180*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper180_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper180* p = (struct mmEmuMapper180*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM16KBank(mmu, 6, data & 0x07);
}

static struct mmEmuMapper* __static_mmEmuMapper180_Produce(void)
{
    struct mmEmuMapper180* p = (struct mmEmuMapper180*)mmMalloc(sizeof(struct mmEmuMapper180));
    mmEmuMapper180_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper180_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper180* p = (struct mmEmuMapper180*)(m);
    mmEmuMapper180_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER180 =
{
    &__static_mmEmuMapper180_Produce,
    &__static_mmEmuMapper180_Recycle,
};
