/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapperRegister_h__
#define __mmEmuMapperRegister_h__

#include "core/mmCore.h"

#include "mmEmuMapper.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

// [  0, 255] mapper xxx
// [256, 256] mapper nsf
MM_EXPORT_EMU extern const struct mmEmuMapperCreator* MM_EMU_MAPPER_BASE_CREATOR[256 + 1];

MM_EXPORT_EMU struct mmEmuMapper* mmEmuMapper_BaseProduce(mmUInt32_t id);
MM_EXPORT_EMU void mmEmuMapper_BaseRecycle(mmUInt32_t id, struct mmEmuMapper* m);

MM_EXPORT_EMU void mmEmuMapper_RegisterInternal(struct mmEmuMapperFactory* p);

#include "core/mmSuffix.h"

#endif//__mmEmuMapperRegister_h__
