/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuRomNesHeader_h__
#define __mmEmuRomNesHeader_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

enum mmEmuRomHeaderVersion_t
{
    MM_EMU_ROMHEADERVERSION_ONES = 0, // iNes old
    MM_EMU_ROMHEADERVERSION_INES = 1, // iNes 1.0
    MM_EMU_ROMHEADERVERSION_NES2 = 2, // iNes 2.0
};

MM_EXPORT_EMU extern const char* MM_NESHEADER_VERSION[3];

MM_EXPORT_EMU const char* mmEmuRomNesHeader_VersionToString(int version);

enum mmEmuGameSystem_t
{
    MM_EMU_GAMESYSTEM_NESNTSC,
    MM_EMU_GAMESYSTEM_NESPAL,
    MM_EMU_GAMESYSTEM_FAMICOM,
    MM_EMU_GAMESYSTEM_DENDY,
    MM_EMU_GAMESYSTEM_VSSYSTEM,
    MM_EMU_GAMESYSTEM_PLAYCHOICE,
    MM_EMU_GAMESYSTEM_FDS,
    MM_EMU_GAMESYSTEM_UNKNOWN,
};

MM_EXPORT_EMU extern const char* MM_NESHEADER_GAMESYSTEM[8];

MM_EXPORT_EMU const char* mmEmuRomNesHeader_GameSystemToString(int type);

enum mmEmuMirroring_t
{
    MM_EMU_MIRRORING_HORIZONTAL,
    MM_EMU_MIRRORING_VERTICAL,
    MM_EMU_MIRRORING_SCREENAONLY,
    MM_EMU_MIRRORING_SCREENBONLY,
    MM_EMU_MIRRORING_FOURSCREENS,
};

MM_EXPORT_EMU extern const char* MM_NESHEADER_MIRRORING[5];

MM_EXPORT_EMU const char* mmEmuRomNesHeader_MirroringToString(int type);

enum mmEmuInput_t
{
    MM_EMU_INPUT_UNSPECIFIED = 0,
    MM_EMU_INPUT_STANDARDCONTROLLERS = 1,
    MM_EMU_INPUT_FOURSCORE = 2,
    MM_EMU_INPUT_FOURPLAYERADAPTER = 3,
    MM_EMU_INPUT_VSSYSTEM = 4,
    MM_EMU_INPUT_VSSYSTEMSWAPPED = 5,
    MM_EMU_INPUT_VSSYSTEMSWAPAB = 6,
    MM_EMU_INPUT_VSZAPPER = 7,
    MM_EMU_INPUT_ZAPPER = 8,
    MM_EMU_INPUT_TWOZAPPERS = 9,
    MM_EMU_INPUT_BANDAIHYPERSHOT = 0X0A,
    MM_EMU_INPUT_POWERPADSIDEA = 0X0B,
    MM_EMU_INPUT_POWERPADSIDEB = 0X0C,
    MM_EMU_INPUT_FAMILYTRAINERSIDEA = 0X0D,
    MM_EMU_INPUT_FAMILYTRAINERSIDEB = 0X0E,
    MM_EMU_INPUT_ARKANOIDCONTROLLERNES = 0X0F,
    MM_EMU_INPUT_ARKANOIDCONTROLLERFAMICOM = 0X10,
    MM_EMU_INPUT_DOUBLEARKANOIDCONTROLLER = 0X11,
    MM_EMU_INPUT_KONAMIHYPERSHOT = 0X12,
    MM_EMU_INPUT_PACHINKOCONTROLLER = 0X13,
    MM_EMU_INPUT_EXCITINGBOXING = 0X14,
    MM_EMU_INPUT_JISSENMAHJONG = 0X15,
    MM_EMU_INPUT_PARTYTAP = 0X16,
    MM_EMU_INPUT_OEKAKIDSTABLET = 0X17,
    MM_EMU_INPUT_BARCODEBATTLER = 0X18,
    MM_EMU_INPUT_MIRACLEPIANO = 0x19, //not supported yet
    MM_EMU_INPUT_POKKUNMOGURAA = 0x1A, //not supported yet
    MM_EMU_INPUT_TOPRIDER = 0x1B, //not supported yet
    MM_EMU_INPUT_DOUBLEFISTED = 0x1C, //not supported yet
    MM_EMU_INPUT_FAMICOM3DSYSTEM = 0x1D, //not supported yet
    MM_EMU_INPUT_DOREMIKKOKEYBOARD = 0x1E, //not supported yet
    MM_EMU_INPUT_ROB = 0x1F, //not supported yet
    MM_EMU_INPUT_FAMICOMDATARECORDER = 0x20,
    MM_EMU_INPUT_TURBOFILE = 0X21,
    MM_EMU_INPUT_BATTLEBOX = 0X22,
    MM_EMU_INPUT_FAMILYBASICKEYBOARD = 0X23,
    MM_EMU_INPUT_PEC586KEYBOARD = 0x24, //not supported yet
    MM_EMU_INPUT_BIT79KEYBOARD = 0x25, //not supported yet
    MM_EMU_INPUT_SUBORKEYBOARD = 0X26,
    MM_EMU_INPUT_SUBORKEYBOARDMOUSE1 = 0X27,
    MM_EMU_INPUT_SUBORKEYBOARDMOUSE2 = 0X28,
    MM_EMU_INPUT_SNESMOUSE = 0X29,
    MM_EMU_INPUT_GENERICMULTICART = 0x2A, //not supported yet
    MM_EMU_INPUT_SNESCONTROLLERS = 0x2B,
    MM_EMU_INPUT_RACERMATEBICYCLE = 0x2C, //not supported yet
    MM_EMU_INPUT_UFORCE = 0x2D, //not supported yet
    MM_EMU_INPUT_LASTENTRY,
};

enum mmEmuVSSystem_t
{
    MM_EMU_VSSYSTEM_DEFAULT = 0,
    MM_EMU_VSSYSTEM_RBIBASEBALLPROTECTION = 1,
    MM_EMU_VSSYSTEM_TKOBOXINGPROTECTION = 2,
    MM_EMU_VSSYSTEM_SUPERXEVIOUSPROTECTION = 3,
    MM_EMU_VSSYSTEM_ICECLIMBERPROTECTION = 4,
    MM_EMU_VSSYSTEM_VSDUALSYSTEM = 5,
    MM_EMU_VSSYSTEM_RAIDONBUNGELINGBAYPROTECTION = 6,
};

enum mmEmuPpuModel_t
{
    MM_EMU_PPU2C02  = 0,
    MM_EMU_PPU2C03  = 1,
    MM_EMU_PPU2C04A = 2,
    MM_EMU_PPU2C04B = 3,
    MM_EMU_PPU2C04C = 4,
    MM_EMU_PPU2C04D = 5,
    MM_EMU_PPU2C05A = 6,
    MM_EMU_PPU2C05B = 7,
    MM_EMU_PPU2C05C = 8,
    MM_EMU_PPU2C05D = 9,
    MM_EMU_PPU2C05E = 10,
};

struct mmLogger;

/*
* ----------------------------------------------------------------------------------------------------------
* Thing   | Archaic |   iNES                      |    NES 2.0                                             |
* ----------------------------------------------------------------------------------------------------------
* Byte 06 | Mapper low nibble, Mirroring, Battery/Trainer flags                                            |
* ----------------------------------------------------------------------------------------------------------
* Byte 07 | Unused  | Mapper high nibble, Vs.     | Mapper high nibble, NES 2.0 signature, PlayChoice, Vs. |
* ----------------------------------------------------------------------------------------------------------
* Byte 08 | Unused  | Total PRG RAM size (linear) | Mapper highest nibble, mapper variant                  |
* ----------------------------------------------------------------------------------------------------------
* Byte 09 | Unused  | TV system                   | Upper bits of ROM size                                 |
* ----------------------------------------------------------------------------------------------------------
* Byte 10 | Unused  | Unused                      | PRG RAM size (logarithmic; battery and non-battery)    |
* ----------------------------------------------------------------------------------------------------------
* Byte 11 | Unused  | Unused                      | VRAM size (logarithmic; battery and non-battery)       |
* ----------------------------------------------------------------------------------------------------------
* Byte 12 | Unused  | Unused                      | TV system                                              |
* ----------------------------------------------------------------------------------------------------------
* Byte 13 | Unused  | Unused                      | Vs. PPU variant                                        |
* ----------------------------------------------------------------------------------------------------------
*/

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( push, 1 )
#endif

struct MM_ALIGNED(1) mmEmuRomNesHeader
{
    mmByte_t ID[4];
    mmByte_t PRG_PAGE_SIZE;
    mmByte_t CHR_PAGE_SIZE;
    mmByte_t control_06;
    mmByte_t control_07;
    mmByte_t control_08;
    mmByte_t control_09;
    mmByte_t control_10;
    mmByte_t control_11;
    mmByte_t control_12;
    mmByte_t control_13;
    mmByte_t control_14;
    mmByte_t control_15;
};

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( pop )
#endif

MM_EXPORT_EMU void mmEmuRomNesHeader_Init(struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU void mmEmuRomNesHeader_Destroy(struct mmEmuRomNesHeader* p);

MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetRomHeaderVersion(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetMapperID(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmBool_t mmEmuRomNesHeader_HasBattery(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmBool_t mmEmuRomNesHeader_HasTrainer(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetNesGameSystem(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetGameSystem(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetSizeValue(const struct mmEmuRomNesHeader* p, mmUInt32_t exponent, mmUInt32_t multiplier);
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetPrgSize(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetChrSize(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetWorkRamSize(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetSaveRamSize(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmSInt32_t mmEmuRomNesHeader_GetChrRamSize(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetSaveChrRamSize(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt8_t mmEmuRomNesHeader_GetSubMapper(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetMirroringType(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetInputType(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetVsSystemType(const struct mmEmuRomNesHeader* p);
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetVsSystemPpuModel(const struct mmEmuRomNesHeader* p);

MM_EXPORT_EMU void mmEmuRomNesHeader_LoggerInformation(const struct mmEmuRomNesHeader* p, struct mmLogger* logger);

#include "core/mmSuffix.h"

#endif//__mmEmuRomNesHeader_h__
