/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuCheat.h"
#include "mmEmuNes.h"

MM_EXPORT_EMU void mmEmuCheatcode_Init(struct mmEmuCheatcode* p)
{
    p->enable = MM_FALSE;
    p->type = 0;
    p->length = 0;
    p->address = 0;
    p->data = 0;

    mmString_Init(&p->comment);
}
MM_EXPORT_EMU void mmEmuCheatcode_Destroy(struct mmEmuCheatcode* p)
{
    p->enable = MM_FALSE;
    p->type = 0;
    p->length = 0;
    p->address = 0;
    p->data = 0;

    mmString_Destroy(&p->comment);
}

MM_EXPORT_EMU void mmEmuCheatcode_CopyForm(struct mmEmuCheatcode* p, const struct mmEmuCheatcode* q)
{
    p->enable = q->enable;
    p->type = q->type;
    p->length = q->length;
    p->address = q->address;
    p->data = q->data;

    mmString_Assign(&p->comment, &q->comment);
}

MM_EXPORT_EMU void mmEmuGeniecode_Init(struct mmEmuGeniecode* p)
{
    p->address = 0;
    p->data = 0;
    p->cmp = 0;
}
MM_EXPORT_EMU void mmEmuGeniecode_Destroy(struct mmEmuGeniecode* p)
{
    p->address = 0;
    p->data = 0;
    p->cmp = 0;
}

MM_EXPORT_EMU void mmEmuCheat_Init(struct mmEmuCheat* p)
{
    struct mmVectorValueAllocator hValueAllocatorCheatCode;
    struct mmVectorValueAllocator hValueAllocatorGenieCode;

    p->nes = NULL;

    p->m_bCheatCodeAdd = MM_FALSE;
    mmVectorValue_Init(&p->m_CheatCode);
    mmVectorValue_Init(&p->m_GenieCode);

    hValueAllocatorCheatCode.Produce = &mmEmuCheatcode_Init;
    hValueAllocatorCheatCode.Recycle = &mmEmuCheatcode_Destroy;
    mmVectorValue_SetAllocator(&p->m_CheatCode, &hValueAllocatorCheatCode);
    mmVectorValue_SetElement(&p->m_CheatCode, sizeof(struct mmEmuCheatcode));

    hValueAllocatorGenieCode.Produce = &mmEmuGeniecode_Init;
    hValueAllocatorGenieCode.Recycle = &mmEmuGeniecode_Destroy;
    mmVectorValue_SetAllocator(&p->m_GenieCode, &hValueAllocatorGenieCode);
    mmVectorValue_SetElement(&p->m_GenieCode, sizeof(struct mmEmuGeniecode));
}
MM_EXPORT_EMU void mmEmuCheat_Destroy(struct mmEmuCheat* p)
{
    p->nes = NULL;

    p->m_bCheatCodeAdd = MM_FALSE;
    mmVectorValue_Destroy(&p->m_CheatCode);
    mmVectorValue_Destroy(&p->m_GenieCode);
}

MM_EXPORT_EMU void mmEmuCheat_SetParent(struct mmEmuCheat* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU void mmEmuCheat_CheatInitial(struct mmEmuCheat* p)
{
    mmVectorValue_Clear(&p->m_CheatCode);
}

MM_EXPORT_EMU mmBool_t mmEmuCheat_IsCheatCodeAdd(struct mmEmuCheat* p)
{
    mmBool_t bRet = p->m_bCheatCodeAdd;

    p->m_bCheatCodeAdd = MM_FALSE;

    return bRet;
}

MM_EXPORT_EMU mmInt_t mmEmuCheat_GetCheatCodeNum(const struct mmEmuCheat* p)
{
    return (mmInt_t)mmVectorValue_Size(&p->m_CheatCode);
}
MM_EXPORT_EMU mmBool_t mmEmuCheat_GetCheatCode(const struct mmEmuCheat* p, mmInt_t no, struct mmEmuCheatcode* code)
{
    const struct mmEmuCheatcode* cheatcode = NULL;

    if (p->m_CheatCode.size - 1 < no)
    {
        return MM_FALSE;
    }
    cheatcode = (struct mmEmuCheatcode*)mmVectorValue_GetIndexReference(&p->m_CheatCode, no);
    mmEmuCheatcode_CopyForm(code, cheatcode);
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuCheat_SetCheatCodeFlag(struct mmEmuCheat* p, mmInt_t no, mmBool_t bEnable)
{
    struct mmEmuCheatcode* cheatcode = NULL;

    if (p->m_CheatCode.size - 1 < no)
    {
        return;
    }

    cheatcode = (struct mmEmuCheatcode*)mmVectorValue_GetIndexReference(&p->m_CheatCode, no);

    if (bEnable)
    {
        cheatcode->enable |= MM_EMU_CHEAT_ENABLE;
    }
    else
    {
        cheatcode->enable &= ~MM_EMU_CHEAT_ENABLE;
    }
}
MM_EXPORT_EMU void mmEmuCheat_SetCheatCodeAllFlag(struct mmEmuCheat* p, mmBool_t bEnable, mmBool_t bKey)
{
    struct mmEmuCheatcode* cheatcode = NULL;

    mmInt_t i = 0;

    for (i = 0; i < p->m_CheatCode.size; i++)
    {
        cheatcode = (struct mmEmuCheatcode*)mmVectorValue_GetIndexReference(&p->m_CheatCode, i);

        if (!bKey)
        {
            if (bEnable)
            {
                cheatcode->enable |= MM_EMU_CHEAT_ENABLE;
            }
            else
            {
                cheatcode->enable &= ~MM_EMU_CHEAT_ENABLE;
            }
        }
        else if (!(cheatcode->enable & MM_EMU_CHEAT_KEYDISABLE))
        {
            if (bEnable)
            {
                cheatcode->enable |= MM_EMU_CHEAT_ENABLE;
            }
            else
            {
                cheatcode->enable &= ~MM_EMU_CHEAT_ENABLE;
            }
        }
    }
}

MM_EXPORT_EMU void mmEmuCheat_ReplaceCheatCode(struct mmEmuCheat* p, mmInt_t no, struct mmEmuCheatcode* code)
{
    struct mmEmuCheatcode* cheatcode = NULL;

    if (p->m_CheatCode.size - 1 < no)
    {
        return;
    }
    cheatcode = (struct mmEmuCheatcode*)mmVectorValue_GetIndexReference(&p->m_CheatCode, no);

    *cheatcode = *code;
}
MM_EXPORT_EMU void mmEmuCheat_AddCheatCode(struct mmEmuCheat* p, struct mmEmuCheatcode* code)
{
    mmVectorValue_PushBack(&p->m_CheatCode, code);
    p->m_bCheatCodeAdd = MM_TRUE;
}
MM_EXPORT_EMU void mmEmuCheat_DelCheatCode(struct mmEmuCheat* p, mmInt_t no)
{
    if (p->m_CheatCode.size - 1 < no)
    {
        return;
    }
    mmVectorValue_Remove(&p->m_CheatCode, no);
}

MM_EXPORT_EMU mmUInt32_t mmEmuCheat_CheatRead(struct mmEmuCheat* p, mmInt_t length, mmWord_t addr)
{
    struct mmEmuNes* nes = p->nes;

    mmInt_t i = 0;
    mmUInt32_t data = 0;
    for (i = 0; i <= length; i++)
    {
        data |= (mmUInt32_t)mmEmuNes_Read(nes, addr + i)*(1 << (i * 8));
    }

    return data;
}
MM_EXPORT_EMU void mmEmuCheat_CheatWrite(struct mmEmuCheat* p, mmInt_t length, mmWord_t addr, mmUInt32_t data)
{
    struct mmEmuNes* nes = p->nes;

    mmInt_t i = 0;
    for (i = 0; i <= length; i++)
    {
        mmEmuNes_Write(nes, (mmWord_t)(addr + i), data & 0xFF);
        data >>= 8;
    }
}
MM_EXPORT_EMU void mmEmuCheat_CheatCodeProcess(struct mmEmuCheat* p)
{
    struct mmEmuCheatcode* cheatcode = NULL;

    mmInt_t i = 0;

    for (i = 0; i < p->m_CheatCode.size; i++)
    {
        cheatcode = (struct mmEmuCheatcode*)mmVectorValue_GetIndexReference(&p->m_CheatCode, i);

        if (!(cheatcode->enable & MM_EMU_CHEAT_ENABLE))
        {
            continue;
        }

        switch (cheatcode->type)
        {
        case MM_EMU_CHEAT_TYPE_ALWAYS:
            mmEmuCheat_CheatWrite(p, cheatcode->length, cheatcode->address, cheatcode->data);
            break;
        case MM_EMU_CHEAT_TYPE_ONCE:
            mmEmuCheat_CheatWrite(p, cheatcode->length, cheatcode->address, cheatcode->data);
            cheatcode->enable = 0;
            break;
        case MM_EMU_CHEAT_TYPE_GREATER:
            if (mmEmuCheat_CheatRead(p, cheatcode->length, cheatcode->address) > cheatcode->data)
            {
                mmEmuCheat_CheatWrite(p, cheatcode->length, cheatcode->address, cheatcode->data);
            }
            break;
        case MM_EMU_CHEAT_TYPE_LESS:
            if (mmEmuCheat_CheatRead(p, cheatcode->length, cheatcode->address) < cheatcode->data)
            {
                mmEmuCheat_CheatWrite(p, cheatcode->length, cheatcode->address, cheatcode->data);
            }
            break;
        default:
            break;
        }
    }
}

MM_EXPORT_EMU void mmEmuCheat_GenieInitial(struct mmEmuCheat* p)
{
    p->m_bCheatCodeAdd = MM_FALSE;
    mmVectorValue_Clear(&p->m_GenieCode);
}
MM_EXPORT_EMU void mmEmuCheat_GenieLoad(struct mmEmuCheat* p, const char* fname)
{
    FILE* fp = NULL;

    mmChar_t buf[256];
    struct mmEmuGeniecode code;
    mmByte_t codetmp[9];
    mmInt_t no;

    if ((fp = fopen(fname, "rb")))
    {
        mmVectorValue_Clear(&p->m_GenieCode);

        while (fgets(buf, sizeof(buf), fp))
        {
            if (buf[0] == ';')
            {
                continue;
            }
            if (buf[0] == 0x0D || buf[0] == 0x0A)
            {
                continue;
            }

            if (mmStrlen(buf) < 6)
            {
                continue;
            }

            code.address = 0;
            code.data = 0;
            code.cmp = 0;

            for (no = 0; isalpha(buf[no]) && no < 8; no++)
            {
                switch (buf[no])
                {
                case    'A': codetmp[no] = 0x00; break;
                case    'P': codetmp[no] = 0x01; break;
                case    'Z': codetmp[no] = 0x02; break;
                case    'L': codetmp[no] = 0x03; break;
                case    'G': codetmp[no] = 0x04; break;
                case    'I': codetmp[no] = 0x05; break;
                case    'T': codetmp[no] = 0x06; break;
                case    'Y': codetmp[no] = 0x07; break;
                case    'E': codetmp[no] = 0x08; break;
                case    'O': codetmp[no] = 0x09; break;
                case    'X': codetmp[no] = 0x0A; break;
                case    'U': codetmp[no] = 0x0B; break;
                case    'K': codetmp[no] = 0x0C; break;
                case    'S': codetmp[no] = 0x0D; break;
                case    'V': codetmp[no] = 0x0E; break;
                case    'N': codetmp[no] = 0x0F; break;
                default:
                    break;
                }
            }

            if (no == 6)
            {
                // Address
                code.address |= (mmWord_t)(codetmp[3] & 0x07) << 12;
                code.address |= (mmWord_t)(codetmp[4] & 0x08) << 8;
                code.address |= (mmWord_t)(codetmp[5] & 0x07) << 8;
                code.address |= (mmWord_t)(codetmp[1] & 0x08) << 4;
                code.address |= (mmWord_t)(codetmp[2] & 0x07) << 4;
                code.address |= (mmWord_t)(codetmp[3] & 0x08);
                code.address |= (mmWord_t)(codetmp[4] & 0x07);
                // Data
                code.data |= (codetmp[0] & 0x08) << 4;
                code.data |= (codetmp[1] & 0x07) << 4;
                code.data |= (codetmp[5] & 0x08);
                code.data |= (codetmp[0] & 0x07);

                mmVectorValue_PushBack(&p->m_GenieCode, &code);
            }
            else
            {
                if (no == 8)
                {
                    // Address
                    code.address |= 0x8000;
                    code.address |= (mmWord_t)(codetmp[3] & 0x07) << 12;
                    code.address |= (mmWord_t)(codetmp[4] & 0x08) << 8;
                    code.address |= (mmWord_t)(codetmp[5] & 0x07) << 8;
                    code.address |= (mmWord_t)(codetmp[1] & 0x08) << 4;
                    code.address |= (mmWord_t)(codetmp[2] & 0x07) << 4;
                    code.address |= (mmWord_t)(codetmp[3] & 0x08);
                    code.address |= (mmWord_t)(codetmp[4] & 0x07);
                    // Data
                    code.data |= (codetmp[0] & 0x08) << 4;
                    code.data |= (codetmp[1] & 0x07) << 4;
                    code.data |= (codetmp[7] & 0x08);
                    code.data |= (codetmp[0] & 0x07);
                    // Data
                    code.cmp |= (codetmp[6] & 0x08) << 4;
                    code.cmp |= (codetmp[7] & 0x07) << 4;
                    code.cmp |= (codetmp[5] & 0x08);
                    code.cmp |= (codetmp[6] & 0x07);

                    mmVectorValue_PushBack(&p->m_GenieCode, &code);
                }
            }
        }

        mmEmuCheat_GenieCodeProcess(p);
    }
    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }
}
MM_EXPORT_EMU void mmEmuCheat_GenieCodeProcess(struct mmEmuCheat* p)
{
    struct mmEmuMmu* mmu = &p->nes->mmu;

    mmWord_t addr;
    mmInt_t i = 0;
    struct mmEmuGeniecode* code = NULL;

    for (i = 0; i < p->m_GenieCode.size; i++)
    {
        code = (struct mmEmuGeniecode*)mmVectorValue_GetIndex(&p->m_GenieCode, i);

        addr = code->address;
        if (addr & 0x8000)
        {
            // 8character codes
            if (mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] == code->cmp)
            {
                mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = code->data;
            }
        }
        else
        {
            // 6character codes
            addr |= 0x8000;
            mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = code->data;
        }
    }
}

