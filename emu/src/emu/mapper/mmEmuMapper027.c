/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper027.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper027_Init(struct mmEmuMapper027* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    //
    p->super.Reset = &mmEmuMapper027_Reset;
    p->super.Write = &mmEmuMapper027_Write;
    //p->super.Read = &mmEmuMapper027_Read;
    //p->super.ReadLow = &mmEmuMapper027_ReadLow;
    //p->super.WriteLow = &mmEmuMapper027_WriteLow;
    //p->super.ExRead = &mmEmuMapper027_ExRead;
    //p->super.ExWrite = &mmEmuMapper027_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper027_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper027_ExCmdWrite;
    p->super.HSync = &mmEmuMapper027_HSync;
    //p->super.VSync = &mmEmuMapper027_VSync;
    //p->super.Clock = &mmEmuMapper027_Clock;
    //p->super.PPULatch = &mmEmuMapper027_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper027_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper027_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper027_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper027_IsStateSave;
    p->super.SaveState = &mmEmuMapper027_SaveState;
    p->super.LoadState = &mmEmuMapper027_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper027_Destroy(struct mmEmuMapper027* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;
}

MM_EXPORT_EMU void mmEmuMapper027_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper027* p = (struct mmEmuMapper027*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmInt_t i = 0;

    for (i = 0; i < 8; i++)
    {
        p->reg[i] = i;
    }
    p->reg[8] = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x47DCBCC4)      // Gradius II(sample)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
    if (crc == 0x468F21FC)      // Racer Mini 4 ku(sample)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
}
MM_EXPORT_EMU void mmEmuMapper027_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper027* p = (struct mmEmuMapper027*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF0CF)
    {
    case 0x8000:
        if (p->reg[8] & 0x02)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        }
        break;
    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;

    case 0x9000:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0x9002:
    case 0x9080:
        p->reg[8] = data;
        break;

    case 0xB000:
        p->reg[0] = (p->reg[0] & 0xFF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;
    case 0xB001:
        p->reg[0] = (p->reg[0] & 0x0F) | (data << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;

    case 0xB002:
        p->reg[1] = (p->reg[1] & 0xFF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;
    case 0xB003:
        p->reg[1] = (p->reg[1] & 0x0F) | (data << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;

    case 0xC000:
        p->reg[2] = (p->reg[2] & 0xFF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;
    case 0xC001:
        p->reg[2] = (p->reg[2] & 0x0F) | (data << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;

    case 0xC002:
        p->reg[3] = (p->reg[3] & 0xFF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;
    case 0xC003:
        p->reg[3] = (p->reg[3] & 0x0F) | (data << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;

    case 0xD000:
        p->reg[4] = (p->reg[4] & 0xFF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;
    case 0xD001:
        p->reg[4] = (p->reg[4] & 0x0F) | (data << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;

    case 0xD002:
        p->reg[5] = (p->reg[5] & 0xFF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;
    case 0xD003:
        p->reg[5] = (p->reg[5] & 0x0F) | (data << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;

    case 0xE000:
        p->reg[6] = (p->reg[6] & 0xFF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;
    case 0xE001:
        p->reg[6] = (p->reg[6] & 0x0F) | (data << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;

    case 0xE002:
        p->reg[7] = (p->reg[7] & 0xFF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;
    case 0xE003:
        p->reg[7] = (p->reg[7] & 0x0F) | (data << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;

    case 0xF000:
        p->irq_latch = (p->irq_latch & 0xF0) | (data & 0x0F);
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xF001:
        p->irq_latch = (p->irq_latch & 0x0F) | ((data & 0x0F) << 4);
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF003:
        p->irq_enable = (p->irq_enable & 0x01) * 3;
        p->irq_clock = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF002:
        p->irq_enable = data & 0x03;
        if (p->irq_enable & 0x02)
        {
            p->irq_counter = p->irq_latch;
            p->irq_clock = 0;
        }
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper027_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper027* p = (struct mmEmuMapper027*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable & 0x02)
    {
        if (p->irq_counter == 0xFF)
        {
            p->irq_counter = p->irq_latch;

            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);

            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        else
        {
            p->irq_counter++;
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper027_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper027_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper027* p = (struct mmEmuMapper027*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmWord_t) * 9);
    buffer[9] = p->irq_enable;
    buffer[10] = p->irq_counter;
    buffer[11] = p->irq_latch;
    *(mmInt_t*)&buffer[12] = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper027_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper027* p = (struct mmEmuMapper027*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmWord_t) * 9);
    p->irq_enable = buffer[9];
    p->irq_counter = buffer[10];
    p->irq_latch = buffer[11];
    p->irq_clock = *(mmInt_t*)&buffer[12];
}

static struct mmEmuMapper* __static_mmEmuMapper027_Produce(void)
{
    struct mmEmuMapper027* p = (struct mmEmuMapper027*)mmMalloc(sizeof(struct mmEmuMapper027));
    mmEmuMapper027_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper027_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper027* p = (struct mmEmuMapper027*)(m);
    mmEmuMapper027_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER027 =
{
    &__static_mmEmuMapper027_Produce,
    &__static_mmEmuMapper027_Recycle,
};
