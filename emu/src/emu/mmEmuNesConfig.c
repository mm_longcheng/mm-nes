/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuNesConfig.h"

#include "core/mmAlloc.h"
#include "core/mmKeyCode.h"

MM_EXPORT_EMU void mmEmuPathConfig_Init(struct mmEmuPathConfig* p)
{
    mmString_Init(&p->szIpsPath);
    mmString_Init(&p->szTonePath);
    mmString_Init(&p->szGeniePath);

    mmString_Init(&p->szSnapshotPath);
    mmString_Init(&p->szCheatCodePath);
    mmString_Init(&p->szTurboPath);
    mmString_Init(&p->szMoviePath);
    mmString_Init(&p->szStatePath);
    mmString_Init(&p->szTapePath);
    mmString_Init(&p->szSavePath);
    mmString_Init(&p->szDiskPath);
    mmString_Init(&p->szNetworkPath);

    mmString_Assigns(&p->szIpsPath, "ips");
    mmString_Assigns(&p->szTonePath, "tone");
    mmString_Assigns(&p->szGeniePath, "genie");

    mmString_Assigns(&p->szSnapshotPath, "snapshot");
    mmString_Assigns(&p->szCheatCodePath, "cheatcode");
    mmString_Assigns(&p->szTurboPath, "turbo");
    mmString_Assigns(&p->szMoviePath, "movie");
    mmString_Assigns(&p->szStatePath, "state");
    mmString_Assigns(&p->szTapePath, "tape");
    mmString_Assigns(&p->szSavePath, "save");
    mmString_Assigns(&p->szDiskPath, "disk");
    mmString_Assigns(&p->szNetworkPath, "network");
}
MM_EXPORT_EMU void mmEmuPathConfig_Destroy(struct mmEmuPathConfig* p)
{
    mmString_Destroy(&p->szIpsPath);
    mmString_Destroy(&p->szTonePath);
    mmString_Destroy(&p->szGeniePath);

    mmString_Destroy(&p->szSnapshotPath);
    mmString_Destroy(&p->szCheatCodePath);
    mmString_Destroy(&p->szTurboPath);
    mmString_Destroy(&p->szMoviePath);
    mmString_Destroy(&p->szStatePath);
    mmString_Destroy(&p->szTapePath);
    mmString_Destroy(&p->szSavePath);
    mmString_Destroy(&p->szDiskPath);
    mmString_Destroy(&p->szNetworkPath);
}

MM_EXPORT_EMU void mmEmuPathConfig_Default(struct mmEmuPathConfig* p)
{
    mmString_Assigns(&p->szIpsPath, "ips");
    mmString_Assigns(&p->szTonePath, "tone");
    mmString_Assigns(&p->szGeniePath, "genie");

    mmString_Assigns(&p->szSnapshotPath, "snapshot");
    mmString_Assigns(&p->szCheatCodePath, "cheatcode");
    mmString_Assigns(&p->szTurboPath, "turbo");
    mmString_Assigns(&p->szMoviePath, "movie");
    mmString_Assigns(&p->szStatePath, "state");
    mmString_Assigns(&p->szTapePath, "tape");
    mmString_Assigns(&p->szSavePath, "save");
}

MM_EXPORT_EMU void mmEmuEmulatorConfig_Init(struct mmEmuEmulatorConfig* p)
{
    p->bIllegalOp = MM_FALSE;
    p->bAutoFrameSkip = MM_TRUE;
    p->bThrottle = MM_TRUE;
    p->nThrottleFPS = 120;          // 120FPS
    p->bBackground = MM_FALSE;
    p->nPriority = 3;               // Normal
    p->bFourPlayer = MM_TRUE;       // TRUE:NES FALSE:Famicom
    p->bCrcCheck = MM_TRUE;
    p->bDiskThrottle = MM_TRUE;
    p->bLoadFullscreen = MM_FALSE;
    p->bPNGsnapshot = MM_TRUE;
    p->bAutoIPS = MM_FALSE;
}
MM_EXPORT_EMU void mmEmuEmulatorConfig_Destroy(struct mmEmuEmulatorConfig* p)
{
    p->bIllegalOp = MM_FALSE;
    p->bAutoFrameSkip = MM_TRUE;
    p->bThrottle = MM_TRUE;
    p->nThrottleFPS = 120;          // 120FPS
    p->bBackground = MM_FALSE;
    p->nPriority = 3;               // Normal
    p->bFourPlayer = MM_TRUE;       // TRUE:NES FALSE:Famicom
    p->bCrcCheck = MM_TRUE;
    p->bDiskThrottle = MM_TRUE;
    p->bLoadFullscreen = MM_FALSE;
    p->bPNGsnapshot = MM_TRUE;
    p->bAutoIPS = MM_FALSE;
}

MM_EXPORT_EMU void mmEmuEmulatorConfig_Default(struct mmEmuEmulatorConfig* p)
{
    p->bIllegalOp = MM_FALSE;
    p->bAutoFrameSkip = MM_TRUE;
    p->bThrottle = MM_TRUE;
    p->nThrottleFPS = 120;          // 120FPS
    p->bBackground = MM_FALSE;
    p->nPriority = 3;               // Normal
    p->bFourPlayer = MM_TRUE;       // TRUE:NES FALSE:Famicom
    p->bCrcCheck = MM_TRUE;
    p->bDiskThrottle = MM_TRUE;
    p->bLoadFullscreen = MM_FALSE;
    p->bPNGsnapshot = MM_FALSE;
    p->bAutoIPS = MM_FALSE;
}

MM_EXPORT_EMU void mmEmuGraphicsConfig_Init(struct mmEmuGraphicsConfig* p)
{
    p->bAspect = MM_FALSE;
    p->bAllSprite = MM_TRUE;
    p->bAllLine = MM_FALSE;
    p->bFPSDisp = MM_FALSE;
    p->bTVFrame = MM_FALSE;
    p->bScanline = MM_FALSE;
    p->nScanlineColor = 75;
    p->bSyncDraw = MM_FALSE;
    p->bFitZoom = MM_FALSE;

    p->bLeftClip = MM_TRUE;

    p->bWindowVSync = MM_FALSE;
    p->bSyncNoSleep = MM_FALSE;

    p->bDiskAccessLamp = MM_FALSE;

    p->bDoubleSize = MM_FALSE;
    p->bSystemMemory = MM_FALSE;
    p->bUseHEL = MM_FALSE;

    p->bNoSquareList = MM_FALSE;

    p->nGraphicsFilter = 0;

    p->dwDisplayWidth = 640;
    p->dwDisplayHeight = 480;
    p->dwDisplayDepth = 16;
    p->dwDisplayRate = 0;

    p->bPaletteFile = MM_FALSE;
    mmString_Init(&p->szPaletteFile);

    mmString_Assigns(&p->szPaletteFile, "");
}
MM_EXPORT_EMU void mmEmuGraphicsConfig_Destroy(struct mmEmuGraphicsConfig* p)
{
    p->bAspect = MM_FALSE;
    p->bAllSprite = MM_TRUE;
    p->bAllLine = MM_FALSE;
    p->bFPSDisp = MM_FALSE;
    p->bTVFrame = MM_FALSE;
    p->bScanline = MM_FALSE;
    p->nScanlineColor = 75;
    p->bSyncDraw = MM_FALSE;
    p->bFitZoom = MM_FALSE;

    p->bLeftClip = MM_TRUE;

    p->bWindowVSync = MM_FALSE;
    p->bSyncNoSleep = MM_FALSE;

    p->bDiskAccessLamp = MM_FALSE;

    p->bDoubleSize = MM_FALSE;
    p->bSystemMemory = MM_FALSE;
    p->bUseHEL = MM_FALSE;

    p->bNoSquareList = MM_FALSE;

    p->nGraphicsFilter = 0;

    p->dwDisplayWidth = 640;
    p->dwDisplayHeight = 480;
    p->dwDisplayDepth = 16;
    p->dwDisplayRate = 0;

    p->bPaletteFile = MM_FALSE;
    mmString_Destroy(&p->szPaletteFile);
}

MM_EXPORT_EMU void mmEmuGraphicsConfig_Default(struct mmEmuGraphicsConfig* p)
{
    p->bAspect = MM_FALSE;
    p->bAllSprite = MM_TRUE;
    p->bAllLine = MM_FALSE;
    p->bFPSDisp = MM_FALSE;
    p->bTVFrame = MM_FALSE;
    p->bScanline = MM_FALSE;
    p->nScanlineColor = 75;
    p->bSyncDraw = MM_FALSE;
    p->bFitZoom = MM_FALSE;

    p->bLeftClip = MM_TRUE;

    p->bWindowVSync = MM_FALSE;
    p->bSyncNoSleep = MM_FALSE;

    p->bDiskAccessLamp = MM_FALSE;

    p->bDoubleSize = MM_FALSE;
    p->bSystemMemory = MM_FALSE;
    p->bUseHEL = MM_FALSE;

    p->bNoSquareList = MM_FALSE;

    p->nGraphicsFilter = 0;

    p->dwDisplayWidth = 640;
    p->dwDisplayHeight = 480;
    p->dwDisplayDepth = 16;
    p->dwDisplayRate = 0;

    p->bPaletteFile = MM_FALSE;
    mmString_Assigns(&p->szPaletteFile, "");
}

MM_EXPORT_EMU void mmEmuAudioConfig_Init(struct mmEmuAudioConfig* p)
{
    p->bEnable = MM_TRUE;
    p->nRate = 22050;
    p->nBits = 8;
    p->nBufferSize = 4;
    p->nFilterType = 0;

    p->bChangeTone = MM_FALSE;

    p->bDisableVolumeEffect = MM_FALSE;
    p->bExtraSoundEnable = MM_TRUE;

    mmMemset(p->nVolume, 0, sizeof(mmShort_t) * 16);

    //
    mmEmuAudioConfig_SetVolumeAll(p, 100);
}
MM_EXPORT_EMU void mmEmuAudioConfig_Destroy(struct mmEmuAudioConfig* p)
{
    p->bEnable = MM_TRUE;
    p->nRate = 22050;
    p->nBits = 8;
    p->nBufferSize = 4;
    p->nFilterType = 0;

    p->bChangeTone = MM_FALSE;

    p->bDisableVolumeEffect = MM_FALSE;
    p->bExtraSoundEnable = MM_TRUE;

    mmMemset(p->nVolume, 0, sizeof(mmShort_t) * 16);
}

MM_EXPORT_EMU void mmEmuAudioConfig_Default(struct mmEmuAudioConfig* p)
{
    p->bEnable = MM_TRUE;
    p->nRate = 22050;
    p->nBits = 8;
    p->nBufferSize = 4;
    p->nFilterType = 0;

    p->bChangeTone = MM_FALSE;

    p->bDisableVolumeEffect = MM_FALSE;
    p->bExtraSoundEnable = MM_TRUE;

    mmMemset(p->nVolume, 0, sizeof(mmShort_t) * 16);

    //
    mmEmuAudioConfig_SetVolumeAll(p, 100);
}
MM_EXPORT_EMU void mmEmuAudioConfig_SetVolumeAll(struct mmEmuAudioConfig* p, mmShort_t nVolume)
{
    mmInt_t i = 0;
    for (i = 0; i < 16; i++)
    {
        p->nVolume[i] = nVolume;
    }
}

MM_EXPORT_EMU void mmEmuControllerConfig_Init(struct mmEmuControllerConfig* p)
{
    mmMemset(p->nButton, 0, sizeof(mmWord_t) * 4 * 64);
    mmMemset(p->nRapid, 0, sizeof(mmWord_t) * 4 * 2);

    mmMemset(p->nExButton, 0, sizeof(mmWord_t) * 4 * 64);
    mmMemset(p->nNsfButton, 0, sizeof(mmWord_t) * 64);
    mmMemset(p->nVSUnisystem, 0, sizeof(mmWord_t) * 64);

    mmEmuControllerConfig_Default(p);
}
MM_EXPORT_EMU void mmEmuControllerConfig_Destroy(struct mmEmuControllerConfig* p)
{
    mmMemset(p->nButton, 0, sizeof(mmWord_t) * 4 * 64);
    mmMemset(p->nRapid, 0, sizeof(mmWord_t) * 4 * 2);

    mmMemset(p->nExButton, 0, sizeof(mmWord_t) * 4 * 64);
    mmMemset(p->nNsfButton, 0, sizeof(mmWord_t) * 64);
    mmMemset(p->nVSUnisystem, 0, sizeof(mmWord_t) * 64);
}

MM_EXPORT_EMU void mmEmuControllerConfig_Default(struct mmEmuControllerConfig* p)
{
    mmEmuControllerConfig_DefaultController1(p);
    mmEmuControllerConfig_DefaultController2(p);
    mmEmuControllerConfig_DefaultController3(p);
    mmEmuControllerConfig_DefaultController4(p);
    mmEmuControllerConfig_DefaultExController0(p);
    mmEmuControllerConfig_DefaultExController1(p);
    mmEmuControllerConfig_DefaultExController2(p);
    mmEmuControllerConfig_DefaultExController3(p);
    mmEmuControllerConfig_DefaultNsfController(p);
    mmEmuControllerConfig_DefaultVSUnisystem(p);
}

MM_EXPORT_EMU void mmEmuControllerConfig_DefaultController1(struct mmEmuControllerConfig* p)
{
    mmWord_t* button = p->nButton[0];
    mmWord_t* rapid = p->nRapid[0];

    mmMemset(button, 0, sizeof(mmWord_t) * 64);
    mmMemset(rapid, 0, sizeof(mmWord_t) * 2);

    // 0 Up       W
    // 1 Down     S
    // 2 Lift     A
    // 3 Right    D
    // 4 A        K
    // 5 B        J
    // 6 A Rapid  I
    // 7 B Rapid  U
    // 8 Select   V
    // 9 Start    B
    button[MM_EMU_CONTROLLER_KEY_UP] = MM_KC_W;
    button[MM_EMU_CONTROLLER_KEY_DOWN] = MM_KC_S;
    button[MM_EMU_CONTROLLER_KEY_LIFT] = MM_KC_A;
    button[MM_EMU_CONTROLLER_KEY_RIGHT] = MM_KC_D;
    button[MM_EMU_CONTROLLER_KEY_A] = MM_KC_K;
    button[MM_EMU_CONTROLLER_KEY_B] = MM_KC_J;
    button[MM_EMU_CONTROLLER_KEY_A_RAPID] = MM_KC_I;
    button[MM_EMU_CONTROLLER_KEY_B_RAPID] = MM_KC_U;
    button[MM_EMU_CONTROLLER_KEY_SELECT] = MM_KC_V;
    button[MM_EMU_CONTROLLER_KEY_START] = MM_KC_B;

    rapid[0] = 0;// A Rapid speed
    rapid[1] = 0;// B Rapid speed
}
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultController2(struct mmEmuControllerConfig* p)
{
    mmWord_t* button = p->nButton[1];
    mmWord_t* rapid = p->nRapid[1];

    mmMemset(button, 0, sizeof(mmWord_t) * 64);
    mmMemset(rapid, 0, sizeof(mmWord_t) * 2);

    // 0 Up       NUMPAD8
    // 1 Down     NUMPAD2
    // 2 Lift     NUMPAD4
    // 3 Right    NUMPAD6
    // 4 A        -
    // 5 B        +
    // 6 A Rapid  0
    // 7 B Rapid  0
    // 8 Select   0
    // 9 Start    0
    button[MM_EMU_CONTROLLER_KEY_UP] = MM_KC_NUMPAD8;
    button[MM_EMU_CONTROLLER_KEY_DOWN] = MM_KC_NUMPAD2;
    button[MM_EMU_CONTROLLER_KEY_LIFT] = MM_KC_NUMPAD4;
    button[MM_EMU_CONTROLLER_KEY_RIGHT] = MM_KC_NUMPAD6;
    button[MM_EMU_CONTROLLER_KEY_A] = MM_KC_SUBTRACT;
    button[MM_EMU_CONTROLLER_KEY_B] = MM_KC_ADD;
    button[MM_EMU_CONTROLLER_KEY_A_RAPID] = 0;
    button[MM_EMU_CONTROLLER_KEY_B_RAPID] = 0;
    button[MM_EMU_CONTROLLER_KEY_SELECT] = 0;
    button[MM_EMU_CONTROLLER_KEY_START] = 0;

    button[MM_EMU_CONTROLLER_KEY_MIC] = 0;

    rapid[0] = 0;// A Rapid speed
    rapid[1] = 0;// B Rapid speed
}
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultController3(struct mmEmuControllerConfig* p)
{
    mmWord_t* button = p->nButton[2];
    mmWord_t* rapid = p->nRapid[2];

    mmMemset(button, 0, sizeof(mmWord_t) * 64);
    mmMemset(rapid, 0, sizeof(mmWord_t) * 2);
}
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultController4(struct mmEmuControllerConfig* p)
{
    mmWord_t* button = p->nButton[3];
    mmWord_t* rapid = p->nRapid[3];

    mmMemset(button, 0, sizeof(mmWord_t) * 64);
    mmMemset(rapid, 0, sizeof(mmWord_t) * 2);
}
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultExController0(struct mmEmuControllerConfig* p)
{
    // Crazy Climber Controller (actually does not exist).
    mmWord_t* exbutton = p->nExButton[0];

    mmMemset(exbutton, 0, sizeof(mmWord_t) * 64);
}
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultExController1(struct mmEmuControllerConfig* p)
{
    // Family Trainer Controller.
    mmWord_t* exbutton = p->nExButton[1];

    mmMemset(exbutton, 0, sizeof(mmWord_t) * 64);
}

MM_EXPORT_EMU void mmEmuControllerConfig_DefaultExController2(struct mmEmuControllerConfig* p)
{
    // Exciting Boxing controller.
    mmWord_t* exbutton = p->nExButton[2];

    mmMemset(exbutton, 0, sizeof(mmWord_t) * 64);
}

MM_EXPORT_EMU void mmEmuControllerConfig_DefaultExController3(struct mmEmuControllerConfig* p)
{
    // Mahjang controller.
    mmWord_t* exbutton = p->nExButton[3];

    mmMemset(exbutton, 0, sizeof(mmWord_t) * 64);
}

MM_EXPORT_EMU void mmEmuControllerConfig_DefaultNsfController(struct mmEmuControllerConfig* p)
{
    mmWord_t* nssfbutton = p->nNsfButton;

    mmMemset(nssfbutton, 0, sizeof(mmWord_t) * 64);

    nssfbutton[0] = MM_KC_UP;   // Play
    nssfbutton[1] = MM_KC_DOWN; // Stop
    nssfbutton[2] = MM_KC_LEFT; // Number -1
    nssfbutton[3] = MM_KC_RIGHT;// Number +1
    nssfbutton[4] = MM_KC_PRIOR;// Number +16
    nssfbutton[5] = MM_KC_NEXT; // Number -16
}

MM_EXPORT_EMU void mmEmuControllerConfig_DefaultVSUnisystem(struct mmEmuControllerConfig* p)
{
    mmWord_t* vsunisystem = p->nVSUnisystem;

    mmMemset(vsunisystem, 0, sizeof(mmWord_t) * 64);

    vsunisystem[0] = MM_KC_C;   // Coin
}

MM_EXPORT_EMU void mmEmuMovieConfig_Init(struct mmEmuMovieConfig* p)
{
    p->bUsePlayer[0] = 0xFF;
    p->bUsePlayer[1] = 0x00;
    p->bUsePlayer[2] = 0x00;
    p->bUsePlayer[3] = 0x00;
    p->bRerecord = MM_TRUE;
    p->bLoopPlay = MM_FALSE;
    p->bResetRec = MM_FALSE;
    p->bPadDisplay = MM_FALSE;
    p->bTimeDisplay = MM_FALSE;
}
MM_EXPORT_EMU void mmEmuMovieConfig_Destroy(struct mmEmuMovieConfig* p)
{
    p->bUsePlayer[0] = 0xFF;
    p->bUsePlayer[1] = 0x00;
    p->bUsePlayer[2] = 0x00;
    p->bUsePlayer[3] = 0x00;
    p->bRerecord = MM_TRUE;
    p->bLoopPlay = MM_FALSE;
    p->bResetRec = MM_FALSE;
    p->bPadDisplay = MM_FALSE;
    p->bTimeDisplay = MM_FALSE;
}

MM_EXPORT_EMU void mmEmuMovieConfig_Default(struct mmEmuMovieConfig* p)
{
    p->bUsePlayer[0] = 0xFF;
    p->bUsePlayer[1] = 0x00;
    p->bUsePlayer[2] = 0x00;
    p->bUsePlayer[3] = 0x00;
    p->bRerecord = MM_TRUE;
    p->bLoopPlay = MM_FALSE;
    p->bResetRec = MM_FALSE;
    p->bPadDisplay = MM_FALSE;
    p->bTimeDisplay = MM_FALSE;
}

MM_EXPORT_EMU const char* MM_EMU_VIDEO_MODE[3] =
{
    "NTSC",
    "PAL",
    "Dendy",
};

MM_EXPORT_EMU const char* mmEmuVideoMode_String(int type)
{
    if (MM_EMU_NES_MODEL_NTSC <= type && type <= MM_EMU_NES_MODEL_DENDY)
    {
        return MM_EMU_VIDEO_MODE[type];
    }
    else
    {
        return "Unknown";
    }
}

MM_EXPORT_EMU void mmEmuClockConfig_Init(struct mmEmuClockConfig* p)
{
    // NTSC default.
    p->MasterClock = 21477270.0;
    p->ColorClockFactor = 6.0;
    p->DotClockFactor = 4.0;
    p->CPUClockFactor = 12.0;
    p->FrameRate = 60.09914261;
    //
    p->NormalScanlines = 240;
    p->TotalScanlines = 262;
}
MM_EXPORT_EMU void mmEmuClockConfig_Destroy(struct mmEmuClockConfig* p)
{
    p->MasterClock = 21477270.0;
    p->ColorClockFactor = 6.0;
    p->DotClockFactor = 4.0;
    p->CPUClockFactor = 12.0;
    p->FrameRate = 60.09914261;
    //
    p->NormalScanlines = 240;
    p->TotalScanlines = 262;
}

// Dendy has PAL framerate and resolution, but NTSC timings, and has 50 dummy scanlines to force 50 fps
//                    NTSC    PAL    Dendy
// NormalScanlines    240     240    290

MM_EXPORT_EMU const struct mmEmuClockConfig MM_EMU_CLOCK_NTSC =
{
    21477270.0,
    6.0,
    4.0,
    12.0,
    60.09914261,

    240,
    262,
};
MM_EXPORT_EMU const struct mmEmuClockConfig MM_EMU_CLOCK_PAL =
{
    26601712.5,
    6.0,
    5.0,
    16.0,
    50.00697891,

    240,
    312,
};
MM_EXPORT_EMU const struct mmEmuClockConfig MM_EMU_CLOCK_DENDY =
{
    26601712.5,
    6.0,
    5.0,
    15.0,
    50.00697891,

    290,
    312,
};

static const struct mmEmuClockConfig* mode_clock_config_impl[3] =
{
    &MM_EMU_CLOCK_NTSC,
    &MM_EMU_CLOCK_PAL,
    &MM_EMU_CLOCK_DENDY,
};

MM_EXPORT_EMU const struct mmEmuClockConfig* mmEmuClockConfig_ModeConfig(mmInt_t nMode)
{
    if (MM_EMU_NES_MODEL_NTSC <= nMode && nMode <= MM_EMU_NES_MODEL_DENDY)
    {
        return mode_clock_config_impl[nMode];
    }
    else
    {
        return mode_clock_config_impl[MM_EMU_NES_MODEL_NTSC];
    }
}

MM_EXPORT_EMU void mmEmuVideoFormatConfig_Init(struct mmEmuVideoFormatConfig* p)
{
    // NTSC default.
    p->BaseClock = 21477270.0;
    p->CpuClock = 1789772.5;

    p->FrameRate = (mmInt_t)60.09914261;
    p->FramePeriod = 1000.0 / (mmInt_t)60.09914261;

    p->NormalScanlines = 240;
    p->TotalScanlines = 262;

    p->FrameIrqCycles = (mmInt_t)(1789772.5 / 60.09914261);

    p->ScanlineCycles = 1364;

    p->FrameCycles = p->ScanlineCycles * p->TotalScanlines;

    p->HDrawCycles = 1024;
    p->HBlankCycles = 340;
    p->ScanlineEndCycles = 4;
}
MM_EXPORT_EMU void mmEmuVideoFormatConfig_Destroy(struct mmEmuVideoFormatConfig* p)
{
    // NTSC default.
    p->BaseClock = 21477270.0;
    p->CpuClock = 1789772.5;

    p->FrameRate = (mmInt_t)60.09914261;
    p->FramePeriod = 1000.0 / (mmInt_t)60.09914261;

    p->NormalScanlines = 240;
    p->TotalScanlines = 262;

    p->FrameIrqCycles = (mmInt_t)(1789772.5 / 60.09914261);

    p->ScanlineCycles = 1364;

    p->FrameCycles = p->ScanlineCycles * p->TotalScanlines;

    p->HDrawCycles = 1024;
    p->HBlankCycles = 340;
    p->ScanlineEndCycles = 4;
}
MM_EXPORT_EMU void mmEmuVideoFormatConfig_Default(struct mmEmuVideoFormatConfig* p)
{
    // NTSC default.
    p->BaseClock = 21477270.0;
    p->CpuClock = 1789772.5;

    p->FrameRate = (mmInt_t)60.09914261;
    p->FramePeriod = 1000.0 / (mmInt_t)60.09914261;

    p->NormalScanlines = 240;
    p->TotalScanlines = 262;

    p->FrameIrqCycles = (mmInt_t)(1789772.5 / 60.09914261);

    p->ScanlineCycles = 1364;

    p->FrameCycles = p->ScanlineCycles * p->TotalScanlines;

    p->HDrawCycles = 1024;
    p->HBlankCycles = 340;
    p->ScanlineEndCycles = 4;
}

MM_EXPORT_EMU void mmEmuVideoFormatConfig_FromClockConfig(struct mmEmuVideoFormatConfig* p, const struct mmEmuClockConfig* clock_config)
{
    p->BaseClock = clock_config->MasterClock;
    p->CpuClock = clock_config->MasterClock / clock_config->CPUClockFactor;

    p->NormalScanlines = clock_config->NormalScanlines;
    p->TotalScanlines = clock_config->TotalScanlines;

    p->FrameRate = (mmInt_t)clock_config->FrameRate;
    p->FramePeriod = 1000.0 / p->FrameRate;

    p->FrameIrqCycles = (mmInt_t)(p->CpuClock / p->FrameRate);

    p->ScanlineCycles = (mmInt_t)(p->FrameIrqCycles * 12.0 / p->TotalScanlines);

    p->FrameCycles = p->ScanlineCycles * p->TotalScanlines;

    p->HDrawCycles = (mmInt_t)(p->ScanlineCycles * 0.75);
    p->HBlankCycles = p->ScanlineCycles - p->HDrawCycles;
    p->ScanlineEndCycles = p->ScanlineCycles - 8 * (128 + 2 * 10 + 22);
}

MM_EXPORT_EMU const struct mmEmuVideoFormatConfig MM_EMU_VIDEO_FORMAT_NTSC =
{
    21477270.0,                             // Base clock
    1789772.5,                              // Cpu clock

    (mmInt_t)60.09914261,                   // Frame rate(Hz)
    1000.0 / (mmInt_t)60.09914261,          // Frame period(ms)

    240,                                    // PPU Normal Scanlines
    262,                                    // Total scanlines

    (mmInt_t)(1789772.5 / 60.09914261),     // FrameIRQ cycles

    1364,                                   // Scanline total cycles

    1364 * 262,                             // Frame cycles

    1024,                                   // H-Draw cycles
    340,                                    // H-Blank cycles
    4,                                      // End cycles
};

MM_EXPORT_EMU const struct mmEmuVideoFormatConfig MM_EMU_VIDEO_FORMAT_PAL =
{
    26601712.5,                             // Base clock
    1662607.03125,                          // Cpu clock

    (mmInt_t)50.00697891,                   // Frame rate(Hz)
    1000.0 / (mmInt_t)50.00697891,          // Frame period(ms)

    240,                                    // PPU Normal Scanlines
    312,                                    // Total scanlines

    (mmInt_t)(1662607.03125 / 50.00697891), // FrameIRQ cycles

    1278,                                   // Scanline total cycles

    1278 * 312,                             // Frame cycles

    960,                                    // H-Draw cycles
    318,                                    // H-Blank cycles
    2,                                      // End cycles
};
MM_EXPORT_EMU const struct mmEmuVideoFormatConfig MM_EMU_VIDEO_FORMAT_DENDY =
{
    26601712.5,                             // Base clock
    1773447.5,                              // Cpu clock

    (mmInt_t)50.00697891,                   // Frame rate(Hz)
    1000.0 / (mmInt_t)50.00697891,          // Frame period(ms)

    290,                                    // PPU Normal Scanlines
    312,                                    // Total scanlines

    (mmInt_t)(1773447.5 / 50.00697891),     // FrameIRQ cycles

    1364,                                   // Scanline total cycles

    1364 * 312,                             // Frame cycles

    1024,                                   // H-Draw cycles
    340,                                    // H-Blank cycles
    4,                                      // End cycles
};

static const struct mmEmuVideoFormatConfig* mode_config_impl[3] =
{
    &MM_EMU_VIDEO_FORMAT_NTSC,
    &MM_EMU_VIDEO_FORMAT_PAL,
    &MM_EMU_VIDEO_FORMAT_DENDY,
};

MM_EXPORT_EMU const struct mmEmuVideoFormatConfig* mmEmuVideoFormatConfig_ModeConfig(mmInt_t nMode)
{
    if (MM_EMU_NES_MODEL_NTSC <= nMode && nMode <= MM_EMU_NES_MODEL_DENDY)
    {
        return mode_config_impl[nMode];
    }
    else
    {
        return mode_config_impl[MM_EMU_NES_MODEL_NTSC];
    }
}

MM_EXPORT_EMU void mmEmuNesConfig_Init(struct mmEmuNesConfig* p)
{
    mmEmuPathConfig_Init(&p->path);
    mmEmuEmulatorConfig_Init(&p->emulator);
    mmEmuGraphicsConfig_Init(&p->graphics);
    mmEmuAudioConfig_Init(&p->audio);
    mmEmuControllerConfig_Init(&p->controller);
    mmEmuMovieConfig_Init(&p->movie);
    mmEmuVideoFormatConfig_Init(&p->nescfg);
}
MM_EXPORT_EMU void mmEmuNesConfig_Destroy(struct mmEmuNesConfig* p)
{
    mmEmuPathConfig_Destroy(&p->path);
    mmEmuEmulatorConfig_Destroy(&p->emulator);
    mmEmuGraphicsConfig_Destroy(&p->graphics);
    mmEmuAudioConfig_Destroy(&p->audio);
    mmEmuControllerConfig_Destroy(&p->controller);
    mmEmuMovieConfig_Destroy(&p->movie);
    mmEmuVideoFormatConfig_Destroy(&p->nescfg);
}
MM_EXPORT_EMU void mmEmuNesConfig_Default(struct mmEmuNesConfig* p)
{
    mmEmuPathConfig_Default(&p->path);
    mmEmuEmulatorConfig_Default(&p->emulator);
    mmEmuGraphicsConfig_Default(&p->graphics);
    mmEmuAudioConfig_Default(&p->audio);
    mmEmuControllerConfig_Default(&p->controller);
    mmEmuMovieConfig_Default(&p->movie);
    mmEmuVideoFormatConfig_Default(&p->nescfg);
}
MM_EXPORT_EMU void mmEmuNesConfig_SetVideoMode(struct mmEmuNesConfig* p, mmInt_t nMode)
{
    p->nescfg = *(mmEmuVideoFormatConfig_ModeConfig(nMode));
}

