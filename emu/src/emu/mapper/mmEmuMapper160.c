/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper160.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper160_Init(struct mmEmuMapper160* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->refresh_type = 0;

    //
    p->super.Reset = &mmEmuMapper160_Reset;
    p->super.Write = &mmEmuMapper160_Write;
    //p->super.Read = &mmEmuMapper160_Read;
    //p->super.ReadLow = &mmEmuMapper160_ReadLow;
    //p->super.WriteLow = &mmEmuMapper160_WriteLow;
    //p->super.ExRead = &mmEmuMapper160_ExRead;
    //p->super.ExWrite = &mmEmuMapper160_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper160_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper160_ExCmdWrite;
    p->super.HSync = &mmEmuMapper160_HSync;
    //p->super.VSync = &mmEmuMapper160_VSync;
    //p->super.Clock = &mmEmuMapper160_Clock;
    //p->super.PPULatch = &mmEmuMapper160_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper160_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper160_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper160_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper160_IsStateSave;
    p->super.SaveState = &mmEmuMapper160_SaveState;
    p->super.LoadState = &mmEmuMapper160_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper160_Destroy(struct mmEmuMapper160* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->refresh_type = 0;
}

MM_EXPORT_EMU void mmEmuMapper160_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper160* p = (struct mmEmuMapper160*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->refresh_type = 0;
}
MM_EXPORT_EMU void mmEmuMapper160_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper160* p = (struct mmEmuMapper160*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0x8001:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0x8002:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;

    case 0x9000:
        if (data == 0x2B)
        {
            p->refresh_type = 1;
        }
        else if (data == 0xA8)
        {
            p->refresh_type = 2;
        }
        else if (data == 0x1F)
        {
            p->refresh_type = 3;
        }
        else if (data == 0x7C)
        {
            p->refresh_type = 4;
        }
        else if (data == 0x18)
        {
            p->refresh_type = 5;
        }
        else if (data == 0x60)
        {
            p->refresh_type = 6;
        }
        else
        {
            p->refresh_type = 0;
        }
        mmEmuMmu_SetVROM1KBank(mmu, 0, data);
        break;
    case 0x9001:
        mmEmuMmu_SetVROM1KBank(mmu, 1, data);
        break;

    case 0x9002:
        if (p->refresh_type == 2 && data != 0xE8)
        {
            p->refresh_type = 0;
        }
        mmEmuMmu_SetVROM1KBank(mmu, 2, data);
        break;

    case 0x9003:
        mmEmuMmu_SetVROM1KBank(mmu, 3, data);
        break;
    case 0x9004:
        mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        break;
    case 0x9005:
        mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        break;
    case 0x9006:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        break;
    case 0x9007:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        break;

    case 0xC000:
        p->irq_counter = p->irq_latch;
        p->irq_enable = p->irq_latch;
        break;
    case 0xC001:
        p->irq_latch = data;
        break;
    case 0xC002:
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xC003:
        p->irq_counter = data;
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper160_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper160* p = (struct mmEmuMapper160*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (scanline == 0 || scanline == 239)
    {
        switch (p->refresh_type)
        {
        case 1:
            mmEmuMmu_SetVROM8KBankArray(mmu, 0x58, 0x59, 0x5A, 0x5B, 0x58, 0x59, 0x5A, 0x5B);
            break;
        case 2:
            mmEmuMmu_SetVROM8KBankArray(mmu, 0x78, 0x79, 0x7A, 0x7B, 0x78, 0x79, 0x7A, 0x7B);
            break;
        case 3:
            mmEmuMmu_SetVROM8KBankArray(mmu, 0x7C, 0x7D, 0x7E, 0x7F, 0x7C, 0x7D, 0x7E, 0x7F);
            break;
        case 5:
            mmEmuMmu_SetVROM8KBankArray(mmu, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77);
            break;
        case 6:
            mmEmuMmu_SetVROM8KBankArray(mmu, 0x5C, 0x5D, 0x5E, 0x5F, 0x7C, 0x7D, 0x7E, 0x7F);
            break;
        default:
            break;
        }
    }
    if (scanline == 64)
    {
        if (p->refresh_type == 4)
        {
            if (mmu->PPU_MEM_BANK[8][32 * 10 + 16] == 0x0A)
            {
                mmEmuMmu_SetVROM1KBank(mmu, 0, 0x68);
                mmEmuMmu_SetVROM1KBank(mmu, 1, 0x69);
                mmEmuMmu_SetVROM1KBank(mmu, 2, 0x6A);
                mmEmuMmu_SetVROM1KBank(mmu, 3, 0x6B);
            }
            else
            {
                mmEmuMmu_SetVROM1KBank(mmu, 0, 0x6C);
                mmEmuMmu_SetVROM1KBank(mmu, 1, 0x6D);
                mmEmuMmu_SetVROM1KBank(mmu, 2, 0x6E);
                mmEmuMmu_SetVROM1KBank(mmu, 3, 0x6F);
            }
        }
    }
    if (scanline == 128)
    {
        if (p->refresh_type == 4)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, 0x68);
            mmEmuMmu_SetVROM1KBank(mmu, 1, 0x69);
            mmEmuMmu_SetVROM1KBank(mmu, 2, 0x6A);
            mmEmuMmu_SetVROM1KBank(mmu, 3, 0x6B);
        }
        else if (p->refresh_type == 5)
        {
            mmEmuMmu_SetVROM8KBankArray(mmu, 0x74, 0x75, 0x76, 0x77, 0x74, 0x75, 0x76, 0x77);
        }
    }
    if (scanline == 160)
    {
        if (p->refresh_type == 6)
        {
            mmEmuMmu_SetVROM8KBankArray(mmu, 0x60, 0x61, 0x5E, 0x5F, 0x7C, 0x7D, 0x7E, 0x7F);
        }
    }

    if (p->irq_enable)
    {
        if (p->irq_counter == 0xFF)
        {
            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
            p->irq_enable = 0;
            p->irq_counter = 0;
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        else
        {
            p->irq_counter++;
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper160_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper160_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper160* p = (struct mmEmuMapper160*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_counter;
    buffer[2] = p->irq_latch;
    buffer[3] = p->refresh_type;
}
MM_EXPORT_EMU void mmEmuMapper160_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper160* p = (struct mmEmuMapper160*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = buffer[1];
    p->irq_latch = buffer[2];
    p->refresh_type = buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper160_Produce(void)
{
    struct mmEmuMapper160* p = (struct mmEmuMapper160*)mmMalloc(sizeof(struct mmEmuMapper160));
    mmEmuMapper160_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper160_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper160* p = (struct mmEmuMapper160*)(m);
    mmEmuMapper160_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER160 =
{
    &__static_mmEmuMapper160_Produce,
    &__static_mmEmuMapper160_Recycle,
};
