/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper110.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper110_Init(struct mmEmuMapper110* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg0 = 0;
    p->reg1 = 0;

    //
    p->super.Reset = &mmEmuMapper110_Reset;
    //p->super.Write = &mmEmuMapper110_Write;
    //p->super.Read = &mmEmuMapper110_Read;
    //p->super.ReadLow = &mmEmuMapper110_ReadLow;
    p->super.WriteLow = &mmEmuMapper110_WriteLow;
    //p->super.ExRead = &mmEmuMapper110_ExRead;
    //p->super.ExWrite = &mmEmuMapper110_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper110_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper110_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper110_HSync;
    //p->super.VSync = &mmEmuMapper110_VSync;
    //p->super.Clock = &mmEmuMapper110_Clock;
    //p->super.PPULatch = &mmEmuMapper110_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper110_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper110_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper110_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper110_IsStateSave;
    p->super.SaveState = &mmEmuMapper110_SaveState;
    p->super.LoadState = &mmEmuMapper110_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper110_Destroy(struct mmEmuMapper110* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg0 = 0;
    p->reg1 = 0;
}

MM_EXPORT_EMU void mmEmuMapper110_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper110* p = (struct mmEmuMapper110*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    p->reg0 = 0;
    p->reg1 = 0;
}
MM_EXPORT_EMU void mmEmuMapper110_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper110* p = (struct mmEmuMapper110*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x4100:
        p->reg1 = data & 0x07;
        break;
    case 0x4101:
        switch (p->reg1)
        {
        case 5:
            mmEmuMmu_SetPROM32KBank(mmu, data);
            break;
        case 0:
            p->reg0 = data & 0x01;
            mmEmuMmu_SetVROM8KBank(mmu, p->reg0);
            break;
        case 2:
            p->reg0 = data;
            mmEmuMmu_SetVROM8KBank(mmu, p->reg0);
            break;
        case 4:
            p->reg0 = p->reg0 | (data << 1);
            mmEmuMmu_SetVROM8KBank(mmu, p->reg0);
            break;
        case 6:
            p->reg0 = p->reg0 | (data << 2);
            mmEmuMmu_SetVROM8KBank(mmu, p->reg0);
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper110_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper110_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper110* p = (struct mmEmuMapper110*)(super);

    buffer[0] = p->reg0;
    buffer[1] = p->reg1;
}
MM_EXPORT_EMU void mmEmuMapper110_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper110* p = (struct mmEmuMapper110*)(super);

    p->reg0 = buffer[0];
    p->reg1 = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper110_Produce(void)
{
    struct mmEmuMapper110* p = (struct mmEmuMapper110*)mmMalloc(sizeof(struct mmEmuMapper110));
    mmEmuMapper110_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper110_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper110* p = (struct mmEmuMapper110*)(m);
    mmEmuMapper110_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER110 =
{
    &__static_mmEmuMapper110_Produce,
    &__static_mmEmuMapper110_Recycle,
};
