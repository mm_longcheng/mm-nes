/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuCpu_h__
#define __mmEmuCpu_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"
#include "mmEmu6502.h"

#include "core/mmPrefix.h"

struct mmEmuNes;
struct mmEmuApu;
struct mmEmuMmu;
struct mmEmuMapper;

struct mmEmuCpu
{
    struct mmEmuNes* nes;

    struct mmEmuX6502 x6502;

    mmUInt64_t TOTAL_cycles;   // CPU TOTAL cycles
    mmInt_t    DMA_cycles;     // DMA cycles

    mmInt_t    nmicount;

    // PTR for quick access.
    mmByte_t** MEM;
    mmByte_t*  RAM;
    mmByte_t*  STACK;

    // Clock process
    mmBool_t   m_bClockProcess;
};

MM_EXPORT_EMU void mmEmuCpu_Init(struct mmEmuCpu* p);
MM_EXPORT_EMU void mmEmuCpu_Destroy(struct mmEmuCpu* p);

MM_EXPORT_EMU void mmEmuCpu_SetParent(struct mmEmuCpu* p, struct mmEmuNes* parent);

MM_EXPORT_EMU mmByte_t mmEmuCpu_RD6502(struct mmEmuCpu* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuCpu_WR6502(struct mmEmuCpu* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmWord_t mmEmuCpu_RD6502W(struct mmEmuCpu* p, mmWord_t addr);

MM_EXPORT_EMU void mmEmuCpu_HardwareReset(struct mmEmuCpu* p);
MM_EXPORT_EMU void mmEmuCpu_SoftwareReset(struct mmEmuCpu* p);

MM_EXPORT_EMU void mmEmuCpu_NMI(struct mmEmuCpu* p);
MM_EXPORT_EMU void mmEmuCpu_SetIRQ(struct mmEmuCpu* p, mmByte_t mask);
MM_EXPORT_EMU void mmEmuCpu_ClrIRQ(struct mmEmuCpu* p, mmByte_t mask);

MM_EXPORT_EMU void mmEmuCpu_DMA(struct mmEmuCpu* p, mmInt_t cycles);

//
// Instruction execution.
//
MM_EXPORT_EMU mmInt_t mmEmuCpu_EXEC(struct mmEmuCpu* p, mmInt_t request_cycles);

MM_EXPORT_EMU mmInt_t mmEmuCpu_GetDmaCycles(const struct mmEmuCpu* p);
MM_EXPORT_EMU void mmEmuCpu_SetDmaCycles(struct mmEmuCpu* p, mmInt_t cycles);

MM_EXPORT_EMU mmUInt64_t mmEmuCpu_GetTotalCycles(const struct mmEmuCpu* p);
MM_EXPORT_EMU void mmEmuCpu_SetTotalCycles(struct mmEmuCpu* p, mmUInt64_t cycles);

static mmInline void mmEmuCpu_SetContext(struct mmEmuCpu* p, const struct mmEmuX6502State* r) { p->x6502.R = *r; }
static mmInline void mmEmuCpu_GetContext(const struct mmEmuCpu* p, struct mmEmuX6502State* r) { *r = p->x6502.R; }

static mmInline void mmEmuCpu_SetClockProcess(struct mmEmuCpu* p, mmBool_t bEnable) { p->m_bClockProcess = bEnable; }

#include "core/mmSuffix.h"

#endif//__mmEmuCpu_h__
