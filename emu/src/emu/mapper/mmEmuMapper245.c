/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper245.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper245_Init(struct mmEmuMapper245* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->we_sram = 0;

    p->irq_type = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    p->MMC4prg = 0;
    p->MMC4chr = 0;

    //
    p->super.Reset = &mmEmuMapper245_Reset;
    p->super.Write = &mmEmuMapper245_Write;
    //p->super.Read = &mmEmuMapper245_Read;
    //p->super.ReadLow = &mmEmuMapper245_ReadLow;
    //p->super.WriteLow = &mmEmuMapper245_WriteLow;
    //p->super.ExRead = &mmEmuMapper245_ExRead;
    //p->super.ExWrite = &mmEmuMapper245_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper245_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper245_ExCmdWrite;
    p->super.HSync = &mmEmuMapper245_HSync;
    //p->super.VSync = &mmEmuMapper245_VSync;
    p->super.Clock = &mmEmuMapper245_Clock;
    //p->super.PPULatch = &mmEmuMapper245_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper245_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper245_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper245_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper245_IsStateSave;
    p->super.SaveState = &mmEmuMapper245_SaveState;
    p->super.LoadState = &mmEmuMapper245_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper245_Destroy(struct mmEmuMapper245* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->we_sram = 0;

    p->irq_type = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    p->MMC4prg = 0;
    p->MMC4chr = 0;
}

MM_EXPORT_EMU void mmEmuMapper245_SetBankCPU(struct mmEmuMapper245* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper245_SetBankPPU(struct mmEmuMapper245* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr4, p->chr5, p->chr6, p->chr7,
                p->chr23 + 1, p->chr23, p->chr01 + 1, p->chr01);
        }
        else
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1,
                p->chr4, p->chr5, p->chr6, p->chr7);
        }
    }
    else
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 4, (p->chr01 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 5, (p->chr01 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 6, (p->chr23 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 7, (p->chr23 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 0, p->chr4 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 1, p->chr5 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 2, p->chr6 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 3, p->chr7 & 0x07);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 0, (p->chr01 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 1, (p->chr01 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 2, (p->chr23 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 3, (p->chr23 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 4, p->chr4 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 5, p->chr5 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 6, p->chr6 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 7, p->chr7 & 0x07);
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper245_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper245* p = (struct mmEmuMapper245*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->we_sram = 0; // Disable
    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_CLOCK);
}
MM_EXPORT_EMU void mmEmuMapper245_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper245* p = (struct mmEmuMapper245*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF7FF)
    {
    case 0x8000:
        p->reg[0] = data;
        break;
    case 0x8001:
        p->reg[1] = data;
        switch (p->reg[0])
        {
        case 0x00:
            p->reg[3] = (data & 2) << 5;
            mmEmuMmu_SetPROM8KBank(mmu, 6, 0x3E | p->reg[3]);
            mmEmuMmu_SetPROM8KBank(mmu, 7, 0x3F | p->reg[3]);
            break;
        case 0x06:
            p->prg0 = data;
            break;
        case 0x07:
            p->prg1 = data;
            break;
        default:
            break;
        }
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->prg0 | p->reg[3]);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->prg1 | p->reg[3]);
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xA001:

        break;
    case 0xC000:
        p->reg[4] = data;
        p->irq_counter = data;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xC001:
        p->reg[5] = data;
        p->irq_latch = data;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_enable = 0;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 1;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper245_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    //struct mmEmuMapper245* p = (struct mmEmuMapper245*)(super);

    //struct mmEmuNes* nes = p->super.nes;

    //if (p->irq_request && (mm_emu_nes_(nes) == MM_EMU_NES_IRQ_CLOCK))
    //{
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    //}
}
MM_EXPORT_EMU void mmEmuMapper245_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper245* p = (struct mmEmuMapper245*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable && !p->irq_request)
            {
                if (scanline == 0)
                {
                    if (p->irq_counter)
                    {
                        p->irq_counter--;
                    }
                }
                if (!(p->irq_counter--))
                {
                    p->irq_request = 0xFF;
                    p->irq_counter = p->irq_latch;
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
    //if (p->irq_request && (mmEmuNes_GetIrqType(nes) == MM_EMU_NES_IRQ_HSYNC))
    //{
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    //}
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper245_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper245_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper245* p = (struct mmEmuMapper245*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
    buffer[16] = p->irq_enable;
    buffer[17] = (mmByte_t)p->irq_counter;
    buffer[18] = p->irq_latch;
    buffer[19] = p->irq_request;
}
MM_EXPORT_EMU void mmEmuMapper245_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper245* p = (struct mmEmuMapper245*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
    p->irq_enable = buffer[16];
    p->irq_counter = (mmInt_t)buffer[17];
    p->irq_latch = buffer[18];
    p->irq_request = buffer[19];
}

static struct mmEmuMapper* __static_mmEmuMapper245_Produce(void)
{
    struct mmEmuMapper245* p = (struct mmEmuMapper245*)mmMalloc(sizeof(struct mmEmuMapper245));
    mmEmuMapper245_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper245_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper245* p = (struct mmEmuMapper245*)(m);
    mmEmuMapper245_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER245 =
{
    &__static_mmEmuMapper245_Produce,
    &__static_mmEmuMapper245_Recycle,
};
