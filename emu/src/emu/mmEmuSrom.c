/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuSrom.h"
#include "mmEmuNes.h"
#include "mmEmuErrorCode.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU int mmEmuSrom_LoadSRAM(struct mmEmuNes* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEmuAssets* assets = &p->assets;

    FILE* fp = NULL;
    mmLong_t size = 0;
    struct mmString szSaveFile;

    int code = MM_UNKNOWN;

    mmString_Init(&szSaveFile);

    do
    {
        if (mmEmuRom_IsNSF(&p->rom))
        {
            code = MM_SUCCESS;
            break;
        }

        mmMemset(p->mmu.WRAM, 0, sizeof(p->mmu.WRAM));

        if (!mmEmuRom_IsBATTERY(&p->rom))
        {
            code = MM_SUCCESS;
            break;
        }

        mmEmuAssets_GetSaveFile(assets, &szSaveFile);

        mmLogger_LogD(gLogger, "%s %d Path: %s", __FUNCTION__, __LINE__, mmString_CStr(&szSaveFile));

        if (!(fp = fopen(mmString_CStr(&szSaveFile), "rb")))
        {
            // Can not open xxx file. 
            // this error is not serious.
            mmLogger_LogD(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, mmString_CStr(&szSaveFile));
            code = MM_SUCCESS;
            break;
        }

        mmLogger_LogI(gLogger, "%s %d Loading SAVERAM...", __FUNCTION__, __LINE__);

        // Get file size.
        fseek(fp, 0, SEEK_END);
        size = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        if (size <= 128 * 1024)
        {
            if (fread(p->mmu.WRAM, size, 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
        }

        code = MM_SUCCESS;

        mmLogger_LogI(gLogger, "%s %d Loading SRAM complete.", __FUNCTION__, __LINE__);
    } while (0);

    mmString_Destroy(&szSaveFile);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    return code;
}
MM_EXPORT_EMU int mmEmuSrom_SaveSRAM(struct mmEmuNes* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEmuAssets* assets = &p->assets;

    FILE* fp = NULL;
    mmInt_t i;
    struct mmString szSaveFile;

    int code = MM_UNKNOWN;

    mmString_Init(&szSaveFile);

    do
    {
        if (mmEmuRom_IsNSF(&p->rom))
        {
            code = MM_SUCCESS;
            break;
        }

        if (!mmEmuRom_IsBATTERY(&p->rom))
        {
            code = MM_SUCCESS;
            break;
        }

        for (i = 0; i < p->SAVERAM_SIZE; i++)
        {
            if (p->mmu.WRAM[i] != 0x00)
            {
                break;
            }
        }

        if (i < p->SAVERAM_SIZE)
        {
            mmEmuAssets_GetSaveFile(assets, &szSaveFile);

            mmLogger_LogD(gLogger, "%s %d Path: %s", __FUNCTION__, __LINE__, mmString_CStr(&szSaveFile));

            if (!(fp = fopen(mmString_CStr(&szSaveFile), "wb")))
            {
                // Can not open xxx file.
                mmLogger_LogE(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, mmString_CStr(&szSaveFile));
                code = MM_ERR_EMU_FILE_NOT_EXIST;
                break;
            }

            mmLogger_LogI(gLogger, "%s %d Saving SRAM...", __FUNCTION__, __LINE__);

            if (fwrite(p->mmu.WRAM, p->SAVERAM_SIZE, 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            code = MM_SUCCESS;

            mmLogger_LogI(gLogger, "%s %d Saving SRAM complete.", __FUNCTION__, __LINE__);
        }
    } while (0);

    mmString_Destroy(&szSaveFile);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    return code;
}

