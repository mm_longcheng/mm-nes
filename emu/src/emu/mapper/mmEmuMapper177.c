/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper177.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper177_Init(struct mmEmuMapper177* p)
{
    mmEmuMapper_Init(&p->super);

    p->HengGe_TYPE = 0;

    //
    p->super.Reset = &mmEmuMapper177_Reset;
    p->super.Write = &mmEmuMapper177_Write;
    //p->super.Read = &mmEmuMapper177_Read;
    //p->super.ReadLow = &mmEmuMapper177_ReadLow;
    p->super.WriteLow = &mmEmuMapper177_WriteLow;
    //p->super.ExRead = &mmEmuMapper177_ExRead;
    //p->super.ExWrite = &mmEmuMapper177_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper177_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper177_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper177_HSync;
    //p->super.VSync = &mmEmuMapper177_VSync;
    //p->super.Clock = &mmEmuMapper177_Clock;
    //p->super.PPULatch = &mmEmuMapper177_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper177_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper177_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper177_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper177_IsStateSave;
    p->super.SaveState = &mmEmuMapper177_SaveState;
    p->super.LoadState = &mmEmuMapper177_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper177_Destroy(struct mmEmuMapper177* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->HengGe_TYPE = 0;
}

MM_EXPORT_EMU void mmEmuMapper177_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper177* p = (struct mmEmuMapper177*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    p->HengGe_TYPE = 0;

    crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);

    if (crc == 0x02c41438)          // Xing He Zhan Shi (C)
    {
        p->HengGe_TYPE = 1;
    }
}
MM_EXPORT_EMU void mmEmuMapper177_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper177* p = (struct mmEmuMapper177*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->HengGe_TYPE == 0)
    {
        mmEmuMmu_SetPROM32KBank(mmu, data);
        if (data & 0x20)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
    }
    if (p->HengGe_TYPE == 1)
    {
        if (addr == 0xa000)
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        if (addr == 0xa001)
        {
            //unknown
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper177_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper177* p = (struct mmEmuMapper177*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->HengGe_TYPE == 1)
    {
        if (addr == 0x5ff1)
        {
            mmEmuMmu_SetPROM32KBank(mmu, (data >> 1));
        }
    }
    if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper177_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper177_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper177* p = (struct mmEmuMapper177*)(super);

    buffer[0] = p->HengGe_TYPE;
}
MM_EXPORT_EMU void mmEmuMapper177_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper177* p = (struct mmEmuMapper177*)(super);

    p->HengGe_TYPE = buffer[0];
}

static struct mmEmuMapper* __static_mmEmuMapper177_Produce(void)
{
    struct mmEmuMapper177* p = (struct mmEmuMapper177*)mmMalloc(sizeof(struct mmEmuMapper177));
    mmEmuMapper177_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper177_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper177* p = (struct mmEmuMapper177*)(m);
    mmEmuMapper177_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER177 =
{
    &__static_mmEmuMapper177_Produce,
    &__static_mmEmuMapper177_Recycle,
};
