/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper225.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper225_Init(struct mmEmuMapper225* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper225_Reset;
    p->super.Write = &mmEmuMapper225_Write;
    //p->super.Read = &mmEmuMapper225_Read;
    //p->super.ReadLow = &mmEmuMapper225_ReadLow;
    //p->super.WriteLow = &mmEmuMapper225_WriteLow;
    //p->super.ExRead = &mmEmuMapper225_ExRead;
    //p->super.ExWrite = &mmEmuMapper225_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper225_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper225_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper225_HSync;
    //p->super.VSync = &mmEmuMapper225_VSync;
    //p->super.Clock = &mmEmuMapper225_Clock;
    //p->super.PPULatch = &mmEmuMapper225_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper225_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper225_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper225_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper225_IsStateSave;
    //p->super.SaveState = &mmEmuMapper225_SaveState;
    //p->super.LoadState = &mmEmuMapper225_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper225_Destroy(struct mmEmuMapper225* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper225_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper225* p = (struct mmEmuMapper225*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper225_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper225* p = (struct mmEmuMapper225*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t prg_bank = (addr & 0x0F80) >> 7;
    mmByte_t chr_bank = addr & 0x003F;

    mmEmuMmu_SetVROM1KBank(mmu, 0, (chr_bank * 8 + 0));
    mmEmuMmu_SetVROM1KBank(mmu, 1, (chr_bank * 8 + 1));
    mmEmuMmu_SetVROM1KBank(mmu, 2, (chr_bank * 8 + 2));
    mmEmuMmu_SetVROM1KBank(mmu, 3, (chr_bank * 8 + 3));
    mmEmuMmu_SetVROM1KBank(mmu, 4, (chr_bank * 8 + 4));
    mmEmuMmu_SetVROM1KBank(mmu, 5, (chr_bank * 8 + 5));
    mmEmuMmu_SetVROM1KBank(mmu, 6, (chr_bank * 8 + 6));
    mmEmuMmu_SetVROM1KBank(mmu, 7, (chr_bank * 8 + 7));

    if (addr & 0x2000)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }

    if (addr & 0x1000)
    {
        // 16KBbank
        if (addr & 0x0040)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, (prg_bank * 4 + 2));
            mmEmuMmu_SetPROM8KBank(mmu, 5, (prg_bank * 4 + 3));
            mmEmuMmu_SetPROM8KBank(mmu, 6, (prg_bank * 4 + 2));
            mmEmuMmu_SetPROM8KBank(mmu, 7, (prg_bank * 4 + 3));
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, (prg_bank * 4 + 0));
            mmEmuMmu_SetPROM8KBank(mmu, 5, (prg_bank * 4 + 1));
            mmEmuMmu_SetPROM8KBank(mmu, 6, (prg_bank * 4 + 0));
            mmEmuMmu_SetPROM8KBank(mmu, 7, (prg_bank * 4 + 1));
        }
    }
    else
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, (prg_bank * 4 + 0));
        mmEmuMmu_SetPROM8KBank(mmu, 5, (prg_bank * 4 + 1));
        mmEmuMmu_SetPROM8KBank(mmu, 6, (prg_bank * 4 + 2));
        mmEmuMmu_SetPROM8KBank(mmu, 7, (prg_bank * 4 + 3));
    }
}

static struct mmEmuMapper* __static_mmEmuMapper225_Produce(void)
{
    struct mmEmuMapper225* p = (struct mmEmuMapper225*)mmMalloc(sizeof(struct mmEmuMapper225));
    mmEmuMapper225_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper225_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper225* p = (struct mmEmuMapper225*)(m);
    mmEmuMapper225_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER225 =
{
    &__static_mmEmuMapper225_Produce,
    &__static_mmEmuMapper225_Recycle,
};
