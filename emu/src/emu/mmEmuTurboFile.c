/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuTurboFile.h"
#include "mmEmuNes.h"
#include "mmEmuErrorCode.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU int mmEmuTurboFile_LoadTurboFile(struct mmEmuNes* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEmuAssets* assets = &p->assets;

    int code = MM_UNKNOWN;

    FILE* fp = NULL;
    mmLong_t size;

    struct mmString szTurboFile;

    mmString_Init(&szTurboFile);

    mmMemset(p->mmu.ERAM, 0, sizeof(p->mmu.ERAM));

    do
    {
        if (mmEmuPad_GetExController(&p->pad) != MM_EMU_PAD_EXCONTROLLER_TURBOFILE)
        {
            code = MM_SUCCESS;
            break;
        }

        mmEmuAssets_GetTurboFile(assets, &szTurboFile);

        mmLogger_LogD(gLogger, "%s %d Path: %s", __FUNCTION__, __LINE__, mmString_CStr(&szTurboFile));

        if (!(fp = fopen(mmString_CStr(&szTurboFile), "rb")))
        {
            // Can not open xxx file.
            // this error is not serious.
            mmLogger_LogD(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, mmString_CStr(&szTurboFile));
            code = MM_SUCCESS;
            break;
        }

        mmLogger_LogI(gLogger, "%s %d Loading TURBOFILE...", __FUNCTION__, __LINE__);

        // Get file size
        fseek(fp, 0, SEEK_END);
        size = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        if (size > 32 * 1024)
        {
            size = 32 * 1024;
        }
        if (fread(p->mmu.ERAM, size, 1, fp) != 1)
        {
            mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
            code = MM_ERR_EMU_FILE_READ;
            break;
        }

        code = MM_SUCCESS;

        mmLogger_LogI(gLogger, "%s %d Loading TURBOFILE complete.", __FUNCTION__, __LINE__);
    } while (0);

    mmString_Destroy(&szTurboFile);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    return code;
}
MM_EXPORT_EMU int mmEmuTurboFile_SaveTurboFile(struct mmEmuNes* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEmuAssets* assets = &p->assets;

    int code = MM_UNKNOWN;

    mmInt_t i;

    FILE* fp = NULL;

    struct mmString szTurboFile;

    mmString_Init(&szTurboFile);

    do
    {
        if (mmEmuPad_GetExController(&p->pad) != MM_EMU_PAD_EXCONTROLLER_TURBOFILE)
        {
            code = MM_SUCCESS;
            break;
        }

        for (i = 0; i < sizeof(p->mmu.ERAM); i++)
        {
            if (p->mmu.ERAM[i] != 0x00)
            {
                break;
            }
        }

        if (i < sizeof(p->mmu.ERAM))
        {
            mmLogger_LogI(gLogger, "%s %d Saving TURBOFILE...", __FUNCTION__, __LINE__);

            mmEmuAssets_GetTurboFile(assets, &szTurboFile);

            mmLogger_LogD(gLogger, "%s %d Path: %s", __FUNCTION__, __LINE__, mmString_CStr(&szTurboFile));

            if (!(fp = fopen(mmString_CStr(&szTurboFile), "wb")))
            {
                // Can not open xxx file.
                mmLogger_LogE(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, mmString_CStr(&szTurboFile));
                code = MM_ERR_EMU_FILE_NOT_EXIST;
                break;
            }

            if (fwrite(p->mmu.ERAM, sizeof(p->mmu.ERAM), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            code = MM_SUCCESS;

            mmLogger_LogI(gLogger, "%s %d Saving TURBOFILE complete.", __FUNCTION__, __LINE__);
        }
    } while (0);

    mmString_Destroy(&szTurboFile);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    return code;
}

