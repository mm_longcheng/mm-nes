/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper194_h__
#define __mmEmuMapper194_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper194                                                            //
//----------------------------------------------------------------------//
struct mmEmuMapper194
{
    struct mmEmuMapper super;

    mmByte_t    reg[8];
    mmByte_t    prg0, prg1;
    mmByte_t    chr01, chr23, chr4, chr5, chr6, chr7;
    mmByte_t    we_sram;

    mmByte_t    irq_type;
    mmByte_t    irq_enable;
    mmByte_t    irq_counter;
    mmByte_t    irq_latch;
    mmByte_t    irq_request;

    mmByte_t    patch;
};

MM_EXPORT_EMU void mmEmuMapper194_Init(struct mmEmuMapper194* p);
MM_EXPORT_EMU void mmEmuMapper194_Destroy(struct mmEmuMapper194* p);

MM_EXPORT_EMU void mmEmuMapper194_SetBankCPU(struct mmEmuMapper194* p);
MM_EXPORT_EMU void mmEmuMapper194_SetBankPPU(struct mmEmuMapper194* p);
MM_EXPORT_EMU void mmEmuMapper194_SetBankPPUSUB(struct mmEmuMapper194* p, int bank, int page);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper194_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper194_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper194_HSync(struct mmEmuMapper* super, mmInt_t scanline);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper194_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper194_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper194_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER194;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper194_h__
