/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmu2413_h__
#define __mmEmu2413_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"


enum { MM_EMU_OPLL_2413_TONE = 0, MM_EMU_OPLL_VRC7_TONE = 1, };

/* voice data */
struct mmEmuOpllPatch
{
    unsigned int TL, FB, EG, ML, AR, DR, SL, RR, KR, KL, AM, PM, WF;
};

MM_EXPORT_EMU void mmEmuOpllPatch_FromDump(struct mmEmuOpllPatch* p, const unsigned char* dump);

struct mmEmuOpll;

/* slot */
struct mmEmuOpllSlot
{
    int type;                          /* 0 : modulator 1 : carrier */

    /* OUTPUT */
    mmSInt32_t feedback;
    mmSInt32_t output[5];              /* Output value of slot */

    /* for Phase Generator (PG) */
    mmUInt32_t *sintbl;                /* Wavetable */
    mmUInt32_t phase;                  /* Phase */
    mmUInt32_t dphase;                 /* Phase increment amount */
    mmUInt32_t pgout;                  /* output */

    /* for Envelope Generator (EG) */
    int fnum;                          /* F-Number */
    int block;                         /* Block */
    int volume;                        /* Current volume */
    int sustine;                       /* Sustine 1 = ON, 0 = OFF */
    mmUInt32_t tll;                    /* Total Level + Key scale level*/
    mmUInt32_t rks;                    /* Key scale offset (Rks) */
    int eg_mode;                       /* Current state */
    mmUInt32_t eg_phase;               /* Phase */
    mmUInt32_t eg_dphase;              /* Phase increment amount */
    mmUInt32_t egout;                  /* output */

    struct mmEmuOpll* opll;            /* weak data point to struct mmEmuOpll. */
    struct mmEmuOpllPatch* patch;      /* weak data point to struct mmEmuOpll.patch[i]. */
};

MM_EXPORT_EMU void mmEmuOpllSlot_Init(struct mmEmuOpllSlot* p);
MM_EXPORT_EMU void mmEmuOpllSlot_Destroy(struct mmEmuOpllSlot* p);

MM_EXPORT_EMU void mmEmuOpllSlot_Reset(struct mmEmuOpllSlot* p);

MM_EXPORT_EMU void mmEmuOpllSlot_SetOpll(struct mmEmuOpllSlot* p, struct mmEmuOpll* opll);
/* Change a rythm voice */
MM_EXPORT_EMU void mmEmuOpllSlot_SetPatch(struct mmEmuOpllSlot* p, struct mmEmuOpllPatch* patch);
MM_EXPORT_EMU void mmEmuOpllSlot_SetVolume(struct mmEmuOpllSlot* p, int volume);

/* Channel */
struct mmEmuOpllCh
{
    struct mmEmuOpllSlot mod;
    struct mmEmuOpllSlot car;
    int patch_number;
    int key_status;
};

MM_EXPORT_EMU void mmEmuOpllCh_Init(struct mmEmuOpllCh* p);
MM_EXPORT_EMU void mmEmuOpllCh_Destroy(struct mmEmuOpllCh* p);

MM_EXPORT_EMU void mmEmuOpllCh_Reset(struct mmEmuOpllCh* p);

MM_EXPORT_EMU void mmEmuOpllCh_SetOpll(struct mmEmuOpllCh* p, struct mmEmuOpll* opll);

/* Mask */
#define MM_EMU_OPLL_MASK_CH(x) (1<<(x))
#define MM_EMU_OPLL_MASK_HH    (1<<(9))
#define MM_EMU_OPLL_MASK_CYM   (1<<(10))
#define MM_EMU_OPLL_MASK_TOM   (1<<(11))
#define MM_EMU_OPLL_MASK_SD    (1<<(12))
#define MM_EMU_OPLL_MASK_BD    (1<<(13))
// (MM_EMU_OPLL_MASK_HH | MM_EMU_OPLL_MASK_CYM | MM_EMU_OPLL_MASK_TOM | MM_EMU_OPLL_MASK_SD | MM_EMU_OPLL_MASK_BD)
#define MM_EMU_OPLL_MASK_RYTHM 0x00003E00

/* opll */
struct mmEmuOpll
{
    /* Channel & Slot */

    /* Channel strong data. */
    struct mmEmuOpllCh ch[9];
    /* Slot weak data point to ch[9]. */
    struct mmEmuOpllSlot* slot[18];

    /* Voice Data */
    struct mmEmuOpllPatch patch[19 * 2];

    mmUInt32_t adr;

    mmSInt32_t output[2];

    /* Register */
    unsigned char reg[0x40];
    int slot_on_flag[18];

    /* Rythm Mode : 0 = OFF, 1 = ON */
    int rythm_mode;

    /* Pitch Modulator */
    mmUInt32_t pm_phase;
    mmSInt32_t lfo_pm;

    /* Amp Modulator */
    mmSInt32_t am_phase;
    mmSInt32_t lfo_am;

    /* Noise Generator */
    mmUInt32_t noise_seed;
    mmUInt32_t whitenoise;
    mmUInt32_t noiseA;
    mmUInt32_t noiseB;
    mmUInt32_t noiseA_phase;
    mmUInt32_t noiseB_phase;
    mmUInt32_t noiseA_idx;
    mmUInt32_t noiseB_idx;
    mmUInt32_t noiseA_dphase;
    mmUInt32_t noiseB_dphase;

    /* flag for check patch update */
    int patch_update[2];

    mmUInt32_t mask;

    /* 0min -- 64 -- 127 max (Liner) */
    int masterVolume;


    /* Tables and config status. */

    /* Sampling rate */
    mmUInt32_t rate;
    /* Input clock */
    mmUInt32_t clk;

    /* Noise and LFO */
    mmUInt32_t pm_dphase;
    mmUInt32_t am_dphase;

    // we use malloc for less stack memory.

    /* Phase incr table for Attack */
    // mmUInt32_t dphaseARTable[16][16];
    mmUInt32_t* dphaseARTable;
    /* Phase incr table for Decay and Release */
    // mmUInt32_t dphaseDRTable[16][16];
    mmUInt32_t* dphaseDRTable;

    /* Phase incr table for PG */
    // mmUInt32_t dphaseTable[512][8][16];
    mmUInt32_t* dphaseTable;
};

MM_EXPORT_EMU void mmEmuOpll_Init(struct mmEmuOpll* p);
MM_EXPORT_EMU void mmEmuOpll_Destroy(struct mmEmuOpll* p);

MM_EXPORT_EMU void mmEmuOpll_Reset(struct mmEmuOpll* p);
MM_EXPORT_EMU void mmEmuOpll_ResetPatch(struct mmEmuOpll* p, int type);

MM_EXPORT_EMU void mmEmuOpll_SetClock(struct mmEmuOpll* p, mmUInt32_t c, mmUInt32_t r);
MM_EXPORT_EMU void mmEmuOpll_SetMasterVolume(struct mmEmuOpll* p, int masterVolume);

/* Port/Register access */
MM_EXPORT_EMU void mmEmuOpll_WriteIO(struct mmEmuOpll* p, mmUInt32_t adr, mmUInt32_t val);
MM_EXPORT_EMU void mmEmuOpll_WriteReg(struct mmEmuOpll* p, mmUInt32_t reg, mmUInt32_t data);

/* Synthsize */
MM_EXPORT_EMU mmSInt16_t mmEmuOpll_Calc(struct mmEmuOpll* p);

/* Misc */
MM_EXPORT_EMU void mmEmuOpll_CopyPatch(struct mmEmuOpll* p, int num, struct mmEmuOpllPatch* patch);
MM_EXPORT_EMU void mmEmuOpll_ForceRefresh(struct mmEmuOpll* p);

/* Channel Mask */
MM_EXPORT_EMU mmUInt32_t mmEmuOpll_SetMask(struct mmEmuOpll* p, mmUInt32_t mask);
MM_EXPORT_EMU mmUInt32_t mmEmuOpll_ToggleMask(struct mmEmuOpll* p, mmUInt32_t mask);

MM_EXPORT_EMU void mmEmuOpll_MakeTables(void);

#include "core/mmSuffix.h"

#endif//__mmEmu2413_h__
