/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper087.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper087_Init(struct mmEmuMapper087* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper087_Reset;
    //p->super.Write = &mmEmuMapper087_Write;
    //p->super.Read = &mmEmuMapper087_Read;
    //p->super.ReadLow = &mmEmuMapper087_ReadLow;
    p->super.WriteLow = &mmEmuMapper087_WriteLow;
    //p->super.ExRead = &mmEmuMapper087_ExRead;
    //p->super.ExWrite = &mmEmuMapper087_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper087_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper087_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper087_HSync;
    //p->super.VSync = &mmEmuMapper087_VSync;
    //p->super.Clock = &mmEmuMapper087_Clock;
    //p->super.PPULatch = &mmEmuMapper087_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper087_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper087_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper087_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper087_IsStateSave;
    //p->super.SaveState = &mmEmuMapper087_SaveState;
    //p->super.LoadState = &mmEmuMapper087_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper087_Destroy(struct mmEmuMapper087* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper087_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper087* p = (struct mmEmuMapper087*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper087_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper087* p = (struct mmEmuMapper087*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0x6000)
    {
        mmEmuMmu_SetVROM8KBank(mmu, (data & 0x02) >> 1);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper087_Produce(void)
{
    struct mmEmuMapper087* p = (struct mmEmuMapper087*)mmMalloc(sizeof(struct mmEmuMapper087));
    mmEmuMapper087_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper087_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper087* p = (struct mmEmuMapper087*)(m);
    mmEmuMapper087_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER087 =
{
    &__static_mmEmuMapper087_Produce,
    &__static_mmEmuMapper087_Recycle,
};
