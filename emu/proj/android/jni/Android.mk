LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libmm_emu_static.mk
include $(LOCAL_PATH)/libmm_emu_shared.mk

# include $(LOCAL_PATH)/libmm_emulator_static.mk
# include $(LOCAL_PATH)/libmm_emulator_shared.mk
