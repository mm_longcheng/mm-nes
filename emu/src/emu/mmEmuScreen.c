/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuScreen.h"
#include "mmEmuPalette.h"
#include "mmEmuNes.h"

#include "core/mmLogger.h"

static void __static_mmEmuScreen_ScreenMessage(struct mmEmuScreen* obj, const char* message)
{

}
MM_EXPORT_EMU void mmEmuScreenCallback_Init(struct mmEmuScreenCallback* p)
{
    p->ScreenMessage = &__static_mmEmuScreen_ScreenMessage;
    p->obj = NULL;
}
MM_EXPORT_EMU void mmEmuScreenCallback_Destroy(struct mmEmuScreenCallback* p)
{
    p->ScreenMessage = &__static_mmEmuScreen_ScreenMessage;
    p->obj = NULL;
}

static const float PalConvTbl[][3] =
{
    { 1.00f, 1.00f, 1.00f, },
    { 1.00f, 0.80f, 0.73f, },
    { 0.73f, 1.00f, 0.70f, },
    { 0.76f, 0.78f, 0.58f, },
    { 0.86f, 0.80f, 1.00f, },
    { 0.83f, 0.68f, 0.85f, },
    { 0.67f, 0.77f, 0.83f, },
    { 0.68f, 0.68f, 0.68f, },
    // 1.00f, 1.00f, 1.00f,
};

MM_EXPORT_EMU void mmEmuScreen_Init(struct mmEmuScreen* p)
{
    p->nes = NULL;

    mmEmuScreenCallback_Init(&p->callback);

    p->m_lpFramebuffer = (mmByte_t*)mmMalloc(sizeof(mmByte_t) * MM_EMU_SCREEN_RENDER_W * MM_EMU_SCREEN_RENDER_H);
    p->m_lpRender = (mmByte_t*)mmMalloc(sizeof(mmByte_t) * MM_EMU_SCREEN_W * MM_EMU_SCREEN_H);
    p->m_lpRenderDelta = (mmByte_t*)mmMalloc(MM_EMU_SCREEN_DELTA_W * MM_EMU_SCREEN_DELTA_H * sizeof(mmUInt32_t));
    mmMemset(p->m_LineColormode, 0, sizeof(mmByte_t) * MM_EMU_SCREEN_H);

    p->m_bScanlineMode = MM_FALSE;
    p->m_nScanlineColor = 75;

    p->m_bZapper = MM_FALSE;
    p->m_bZapperDraw = MM_FALSE;
    p->m_ZapperPosX = -1;
    p->m_ZapperPosY = -1;

    p->m_bDeltaUpdate = MM_FALSE;

    p->m_nPaletteMode = 0;
    p->m_bMonoMode = MM_FALSE;
    p->m_bPaletteUpdate = MM_FALSE;

    mmMemcpy(p->m_PaletteBuffer, MM_EMU_PALETTE_DEFAULT, sizeof(p->m_PaletteBuffer));

    mmMemset(p->m_cpPalette, 0xFF, sizeof(struct mmEmuRGBQuad) * 8 * 64 * 2);
    mmMemset(p->m_mpPalette, 0xFF, sizeof(struct mmEmuRGBQuad) * 8 * 64 * 2);

    mmMemset(p->m_cnPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    mmMemset(p->m_csPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    mmMemset(p->m_mnPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    mmMemset(p->m_msPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);

    mmMemset(p->m_cfPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    mmMemset(p->m_mfPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    //
    // 0x3F is black color index.
    mmMemset(p->m_lpRender, 0x3F, sizeof(mmByte_t) * MM_EMU_SCREEN_W * MM_EMU_SCREEN_H);
    mmMemset(p->m_lpRenderDelta, 0, sizeof(mmByte_t) * MM_EMU_SCREEN_DELTA_W * MM_EMU_SCREEN_DELTA_H * sizeof(mmUInt32_t));
}
MM_EXPORT_EMU void mmEmuScreen_Destroy(struct mmEmuScreen* p)
{
    mmFree(p->m_lpFramebuffer);
    mmFree(p->m_lpRender);
    mmFree(p->m_lpRenderDelta);
    // 
    p->nes = NULL;

    mmEmuScreenCallback_Destroy(&p->callback);

    p->m_lpFramebuffer = NULL;
    p->m_lpRender = NULL;
    p->m_lpRenderDelta = NULL;
    mmMemset(p->m_LineColormode, 0, sizeof(mmByte_t) * MM_EMU_SCREEN_H);

    p->m_bScanlineMode = MM_FALSE;
    p->m_nScanlineColor = 75;

    p->m_bZapper = MM_FALSE;
    p->m_bZapperDraw = MM_FALSE;
    p->m_ZapperPosX = -1;
    p->m_ZapperPosY = -1;

    p->m_bDeltaUpdate = MM_FALSE;

    p->m_nPaletteMode = 0;
    p->m_bMonoMode = MM_FALSE;
    p->m_bPaletteUpdate = MM_FALSE;

    mmMemcpy(p->m_PaletteBuffer, MM_EMU_PALETTE_DEFAULT, sizeof(p->m_PaletteBuffer));

    mmMemset(p->m_cpPalette, 0xFF, sizeof(struct mmEmuRGBQuad) * 8 * 64 * 2);
    mmMemset(p->m_mpPalette, 0xFF, sizeof(struct mmEmuRGBQuad) * 8 * 64 * 2);

    mmMemset(p->m_cnPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    mmMemset(p->m_csPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    mmMemset(p->m_mnPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    mmMemset(p->m_msPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);

    mmMemset(p->m_cfPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
    mmMemset(p->m_mfPalette, 0xFF, sizeof(mmUInt32_t) * 8 * 256);
}

MM_EXPORT_EMU void mmEmuScreen_SetParent(struct mmEmuScreen* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}
MM_EXPORT_EMU void mmEmuScreen_SetCallback(struct mmEmuScreen* p, struct mmEmuScreenCallback* callback)
{
    assert(NULL != callback && "NULL != callback is invalid.");
    p->callback = *callback;
}

MM_EXPORT_EMU void mmEmuScreen_ScreenClear(struct mmEmuScreen* p)
{
    // 0x3F is black color index.
    mmMemset(p->m_lpRender, 0x3F, sizeof(mmByte_t) * MM_EMU_SCREEN_W * MM_EMU_SCREEN_H);
}

MM_EXPORT_EMU void mmEmuScreen_SetPaletteMode(struct mmEmuScreen* p, mmInt_t nMode, mmBool_t bMono)
{
    if ((p->m_nPaletteMode != nMode) || (p->m_bMonoMode != bMono))
    {
        p->m_bPaletteUpdate = MM_TRUE;
    }
    p->m_nPaletteMode = nMode;
    p->m_bMonoMode = bMono;
}
// Obtaining the displayed palette table
MM_EXPORT_EMU void mmEmuScreen_GetPaletteData(const struct mmEmuScreen* p, struct mmEmuRGBQuad* rgb)
{
    mmInt_t i = 0;
    if (!p->m_bMonoMode)
    {
        for (i = 0; i < 64; i++)
        {
            rgb[i] = p->m_cpPalette[p->m_nPaletteMode][i];
            rgb[i + 0x40] = p->m_mpPalette[p->m_nPaletteMode][i];
        }
    }
    else
    {
        for (i = 0; i < 64; i++)
        {
            rgb[i] = p->m_mpPalette[p->m_nPaletteMode][i];
            rgb[i + 0x40] = p->m_mpPalette[p->m_nPaletteMode][i];
        }
    }
}
MM_EXPORT_EMU void mmEmuScreen_GetPaletteData2(const struct mmEmuScreen* p, struct mmEmuRGBQuad* rgb)
{
    mmInt_t i = 0;
    mmInt_t j = 0;

    for (j = 0; j < 8; j++)
    {
        for (i = 0; i < 64; i++)
        {
            rgb[j * 256 + i] = p->m_cpPalette[j][i];
            rgb[j * 256 + i + 0x40] = p->m_mpPalette[j][i];
        }
    }
}
MM_EXPORT_EMU void mmEmuScreen_GetPaletteR8G8B8A8(const struct mmEmuScreen* p, mmUInt32_t* palette)
{
    mmInt_t i = 0;
    mmUInt32_t r, g, b, a;
    const struct mmEmuRGBQuad* c = NULL;

    if (!p->m_bMonoMode)
    {
        for (i = 0; i < 64; i++)
        {
            c = &p->m_cpPalette[p->m_nPaletteMode][i];
            r = (mmUInt32_t)(c->rgbRed);
            g = (mmUInt32_t)(c->rgbGreen);
            b = (mmUInt32_t)(c->rgbBlue);
            a = (mmUInt32_t)(c->rgbReserved);
            mmEmuPaletteABGRMake(&palette[i       ], a, b, g, r);

            c = &p->m_mpPalette[p->m_nPaletteMode][i];
            r = (mmUInt32_t)(c->rgbRed);
            g = (mmUInt32_t)(c->rgbGreen);
            b = (mmUInt32_t)(c->rgbBlue);
            a = (mmUInt32_t)(c->rgbReserved);
            mmEmuPaletteABGRMake(&palette[i + 0x40], a, b, g, r);
        }
    }
    else
    {
        for (i = 0; i < 64; i++)
        {
            c = &p->m_mpPalette[p->m_nPaletteMode][i];
            r = (mmUInt32_t)(c->rgbRed);
            g = (mmUInt32_t)(c->rgbGreen);
            b = (mmUInt32_t)(c->rgbBlue);
            a = (mmUInt32_t)(c->rgbReserved);
            mmEmuPaletteABGRMake(&palette[i       ], a, b, g, r);

            c = &p->m_mpPalette[p->m_nPaletteMode][i];
            r = (mmUInt32_t)(c->rgbRed);
            g = (mmUInt32_t)(c->rgbGreen);
            b = (mmUInt32_t)(c->rgbBlue);
            a = (mmUInt32_t)(c->rgbReserved);
            mmEmuPaletteABGRMake(&palette[i + 0x40], a, b, g, r);
        }
    }
}
MM_EXPORT_EMU mmBool_t mmEmuScreen_CalcPaletteTable(struct mmEmuScreen* p)
{
    mmInt_t i, j;

    mmInt_t Rbit = 8;
    mmInt_t Gbit = 8;
    mmInt_t Bbit = 8;

    mmInt_t Rsft = 16;
    mmInt_t Gsft = 8;
    mmInt_t Bsft = 0;

    mmUInt32_t Rn, Gn, Bn;
    mmUInt32_t Rs, Gs, Bs;
    mmUInt32_t Vn, Vs;
    
    for (j = 0; j < 8; j++)
    {
        for (i = 0; i < 64; i++)
        {
            // Normal
            Rn = (mmUInt32_t)(PalConvTbl[j][0] * mmEmuPaletteARGBGetR(p->m_PaletteBuffer[i]));
            Gn = (mmUInt32_t)(PalConvTbl[j][1] * mmEmuPaletteARGBGetG(p->m_PaletteBuffer[i]));
            Bn = (mmUInt32_t)(PalConvTbl[j][2] * mmEmuPaletteARGBGetB(p->m_PaletteBuffer[i]));
            // Scanline
            Rs = (mmUInt32_t)(PalConvTbl[j][0] * mmEmuPaletteARGBGetR(p->m_PaletteBuffer[i]) * p->m_nScanlineColor / 100.0f);
            Gs = (mmUInt32_t)(PalConvTbl[j][1] * mmEmuPaletteARGBGetG(p->m_PaletteBuffer[i]) * p->m_nScanlineColor / 100.0f);
            Bs = (mmUInt32_t)(PalConvTbl[j][2] * mmEmuPaletteARGBGetB(p->m_PaletteBuffer[i]) * p->m_nScanlineColor / 100.0f);

            p->m_cpPalette[j][i + 0x00].rgbRed   = (mmByte_t)Rn;
            p->m_cpPalette[j][i + 0x00].rgbGreen = (mmByte_t)Gn;
            p->m_cpPalette[j][i + 0x00].rgbBlue  = (mmByte_t)Bn;
            p->m_cpPalette[j][i + 0x40].rgbRed   = (mmByte_t)Rs;
            p->m_cpPalette[j][i + 0x40].rgbGreen = (mmByte_t)Gs;
            p->m_cpPalette[j][i + 0x40].rgbBlue  = (mmByte_t)Bs;

            p->m_cnPalette[j][i] = ((Rn >> (8 - Rbit)) << Rsft) | ((Gn >> (8 - Gbit)) << Gsft) | ((Bn >> (8 - Bbit)) << Bsft);
            p->m_csPalette[j][i] = ((Rs >> (8 - Rbit)) << Rsft) | ((Gs >> (8 - Gbit)) << Gsft) | ((Bs >> (8 - Bbit)) << Bsft);

            // RGB555
            if (Rsft > Bsft)
            {
                // When RGB 555 -> RGB 888
                p->m_cfPalette[j][i] = ((Rn >> (8 - 5)) << 10) | ((Gn >> (8 - 5)) << 5) | ((Bn >> (8 - 5)) << 0);
            }
            else
            {
                // When BGR 555 -> BGR 888
                p->m_cfPalette[j][i] = ((Rn >> (8 - 5)) << 0) | ((Gn >> (8 - 5)) << 5) | ((Bn >> (8 - 5)) << 10);
            }

            // Monochrome
            Rn = (mmUInt32_t)(mmEmuPaletteARGBGetR(p->m_PaletteBuffer[i & 0x30]));
            Gn = (mmUInt32_t)(mmEmuPaletteARGBGetG(p->m_PaletteBuffer[i & 0x30]));
            Bn = (mmUInt32_t)(mmEmuPaletteARGBGetB(p->m_PaletteBuffer[i & 0x30]));
            Vn = (mmUInt32_t)(0.299f * Rn + 0.587f * Gn + 0.114f * Bn);
            Rn = (mmUInt32_t)(PalConvTbl[j][0] * Vn);
            Gn = (mmUInt32_t)(PalConvTbl[j][1] * Vn);
            Bn = (mmUInt32_t)(PalConvTbl[j][2] * Vn);
            if (Rn > 0xFF) Rs = 0xFF;
            if (Gn > 0xFF) Gs = 0xFF;
            if (Bn > 0xFF) Bs = 0xFF;
            // Scanline
            Rs = (mmUInt32_t)(mmEmuPaletteARGBGetR(p->m_PaletteBuffer[i & 0x30]) * p->m_nScanlineColor / 100.0f);
            Gs = (mmUInt32_t)(mmEmuPaletteARGBGetG(p->m_PaletteBuffer[i & 0x30]) * p->m_nScanlineColor / 100.0f);
            Bs = (mmUInt32_t)(mmEmuPaletteARGBGetB(p->m_PaletteBuffer[i & 0x30]) * p->m_nScanlineColor / 100.0f);
            Vs = (mmUInt32_t)(0.299f * Rs + 0.587f * Gs + 0.114f * Bs);
            Rs = (mmUInt32_t)(PalConvTbl[j][0] * Vs);
            Gs = (mmUInt32_t)(PalConvTbl[j][1] * Vs);
            Bs = (mmUInt32_t)(PalConvTbl[j][2] * Vs);
            if (Rs > 0xFF) Rs = 0xFF;
            if (Gs > 0xFF) Gs = 0xFF;
            if (Bs > 0xFF) Bs = 0xFF;

            p->m_mpPalette[j][i + 0x00].rgbRed   = (mmByte_t)Rn;
            p->m_mpPalette[j][i + 0x00].rgbGreen = (mmByte_t)Gn;
            p->m_mpPalette[j][i + 0x00].rgbBlue  = (mmByte_t)Bn;
            p->m_mpPalette[j][i + 0x40].rgbRed   = (mmByte_t)Rs;
            p->m_mpPalette[j][i + 0x40].rgbGreen = (mmByte_t)Gs;
            p->m_mpPalette[j][i + 0x40].rgbBlue  = (mmByte_t)Bs;

            p->m_mnPalette[j][i] = ((Rn >> (8 - Rbit)) << Rsft) | ((Gn >> (8 - Gbit)) << Gsft) | ((Bn >> (8 - Bbit)) << Bsft);
            p->m_msPalette[j][i] = ((Rs >> (8 - Rbit)) << Rsft) | ((Gs >> (8 - Gbit)) << Gsft) | ((Bs >> (8 - Bbit)) << Bsft);

            // RGB555
            if (Rsft > Bsft)
            {
                // When RGB 555 -> RGB 888
                p->m_mfPalette[j][i] = ((Rn >> (8 - 5)) << 10) | ((Gn >> (8 - 5)) << 5) | ((Bn >> (8 - 5)) << 0);
            }
            else
            {
                // When BGR 555 -> BGR 888
                p->m_mfPalette[j][i] = ((Rn >> (8 - 5)) << 0) | ((Gn >> (8 - 5)) << 5) | ((Bn >> (8 - 5)) << 10);
            }
        }
    }

    // For redrawing.
    p->m_bDeltaUpdate = MM_TRUE;

    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuScreen_SetPaletteTableByte(struct mmEmuScreen* p, const mmUInt32_t pal[64])
{
    if (pal)
    {
        mmMemcpy(p->m_PaletteBuffer, pal, sizeof(p->m_PaletteBuffer));
    }
    else
    {
        mmMemcpy(p->m_PaletteBuffer, MM_EMU_PALETTE_DEFAULT, sizeof(p->m_PaletteBuffer));
    }

    mmEmuScreen_CalcPaletteTable(p);

    p->m_bPaletteUpdate = MM_TRUE;
}
MM_EXPORT_EMU void mmEmuScreen_SetPaletteTableRGBQuad(struct mmEmuScreen* p, struct mmEmuRGBQuad* rgb)
{
    mmInt_t i = 0;

    for (i = 0; i < 64; i++)
    {
        mmUInt32_t r = (mmUInt32_t)(rgb[i].rgbRed     );
        mmUInt32_t g = (mmUInt32_t)(rgb[i].rgbGreen   );
        mmUInt32_t b = (mmUInt32_t)(rgb[i].rgbBlue    );
        mmUInt32_t a = (mmUInt32_t)(rgb[i].rgbReserved);
        mmEmuPaletteARGBMake(&p->m_PaletteBuffer[i], a, r, g, b);
    }

    mmEmuScreen_CalcPaletteTable(p);

    p->m_bPaletteUpdate = MM_TRUE;
}
MM_EXPORT_EMU void mmEmuScreen_GetPaletteTable(const struct mmEmuScreen* p, struct mmEmuRGBQuad* rgb)
{
    mmInt_t i = 0;

    for (i = 0; i < 64; i++)
    {
        rgb[i].rgbRed      = mmEmuPaletteARGBGetR(p->m_PaletteBuffer[i]);
        rgb[i].rgbGreen    = mmEmuPaletteARGBGetG(p->m_PaletteBuffer[i]);
        rgb[i].rgbBlue     = mmEmuPaletteARGBGetB(p->m_PaletteBuffer[i]);
        rgb[i].rgbReserved = mmEmuPaletteARGBGetA(p->m_PaletteBuffer[i]);
    }
}
MM_EXPORT_EMU void mmEmuScreen_SetPaletteFile(struct mmEmuScreen* p, const char* fname)
{
    // Update palette file.
    if (mmStrlen(fname) > 0)
    {
        FILE* fp = NULL;
        if ((fp = fopen(fname, "rb")))
        {
            mmUInt32_t palbuf[64];
            // Read in size.
            if (fread(palbuf, 192, 1, fp) == 1)
            {
                // Palette change and calculation.
                mmEmuScreen_SetPaletteTableByte(p, palbuf);
            }
            else
            {
                // Default when it can not be read.
                mmEmuScreen_SetPaletteTableByte(p, (mmUInt32_t*)NULL);
            }
            if (NULL != fp)
            {
                fclose(fp);
                fp = NULL;
            }
        }
        else
        {
            // Default when you can not open.
            mmEmuScreen_SetPaletteTableByte(p, (mmUInt32_t*)NULL);
        }
    }
    else
    {
        // Default when there is no name.
        mmEmuScreen_SetPaletteTableByte(p, (mmUInt32_t*)NULL);
    }
}

MM_EXPORT_EMU mmByte_t mmEmuScreen_GetZapperHit(const struct mmEmuScreen* p)
{
    if (p->m_bZapper)
    {
        if (p->m_ZapperPosX >= 0 && p->m_ZapperPosX < MM_EMU_SCREEN_RENDER_W &&
            p->m_ZapperPosY >= 0 && p->m_ZapperPosY < MM_EMU_SCREEN_RENDER_H)
        {
            mmByte_t c = p->m_lpRender[8 + p->m_ZapperPosX + MM_EMU_SCREEN_W * p->m_ZapperPosY];
            float r = 0.299f * p->m_cpPalette[0][c].rgbRed;
            float g = 0.587f * p->m_cpPalette[0][c].rgbGreen;
            float b = 0.114f * p->m_cpPalette[0][c].rgbBlue;
            mmUInt32_t Yn = (mmUInt32_t)(r + g + b);
            if (Yn > 0xFF) Yn = 0xFF;
            return (mmByte_t)Yn;
        }
    }
    return 0x00;
}
MM_EXPORT_EMU void mmEmuScreen_UpdateZapperPos(struct mmEmuScreen* p, mmLong_t* x, mmLong_t* y)
{
    const struct mmEmuControllerCursor* cursor = &p->nes->controller.cursor;

    (*x) = (*y) = -1;

    if (0 <= cursor->x && cursor->x < MM_EMU_SCREEN_RENDER_W &&
        0 <= cursor->y && cursor->y < MM_EMU_SCREEN_RENDER_H)
    {
        (*x) = (mmLong_t)cursor->x;
        (*y) = (mmLong_t)cursor->y;

        if ((*x) > MM_EMU_SCREEN_RENDER_W - 1)
        {
            (*x) = MM_EMU_SCREEN_RENDER_W - 1;
        }
        if ((*y) > MM_EMU_SCREEN_RENDER_H - 1)
        {
            (*y) = MM_EMU_SCREEN_RENDER_H - 1;
        }
    }
    else
    {
        (*x) = (*y) = -1;
    }

    p->m_ZapperPosX = (*x);
    p->m_ZapperPosY = (*y);
}
MM_EXPORT_EMU void mmEmuScreen_SetZapperPos(struct mmEmuScreen* p, mmLong_t x, mmLong_t y)
{
    p->m_ZapperPosX = x;
    p->m_ZapperPosY = y;
}

MM_EXPORT_EMU void mmEmuScreen_SetMessageString(struct mmEmuScreen* p, const char* message)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmLogger_LogI(gLogger, "emu screen message %s", message);
    (*(p->callback.ScreenMessage))(p, message);
}

MM_EXPORT_EMU void mmEmuScreen_BitBlockTransfer(struct mmEmuScreen* p)
{
    int i;

    mmByte_t* t = p->m_lpFramebuffer;
    mmByte_t* s = p->m_lpRender + 8;

    for (i = 0; i < MM_EMU_SCREEN_RENDER_H; ++i)
    {
        mmMemcpy(t, s, MM_EMU_SCREEN_RENDER_W);
        t += MM_EMU_SCREEN_RENDER_W;
        s += MM_EMU_SCREEN_W;
    }
}
