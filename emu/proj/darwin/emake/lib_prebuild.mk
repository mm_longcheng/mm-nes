LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libminizip_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libminizip_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libminizip_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libminizip_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_core_shared
LOCAL_SRC_FILES := ../../../../../mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_core_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_core_static
LOCAL_SRC_FILES := ../../../../../mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_core_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_dish_shared
LOCAL_SRC_FILES := ../../../../../mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_dish_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_dish_static
LOCAL_SRC_FILES := ../../../../../mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_dish_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libmm_nwsi_shared
# LOCAL_SRC_FILES := ../../../../../mm-core/nwsi/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_nwsi_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libmm_nwsi_static
# LOCAL_SRC_FILES := ../../../../../mm-core/nwsi/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_nwsi_static.a
# include $(PREBUILT_STATIC_LIBRARY)
# 
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libCEGUIOgreRenderer_shared
# LOCAL_SRC_FILES := ../../../../../mm-core/nwsi/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libCEGUIOgreRenderer_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libCEGUIOgreRenderer_static
# LOCAL_SRC_FILES := ../../../../../mm-core/nwsi/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libCEGUIOgreRenderer_static.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libtinyxml_shared
# LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/tinyxml/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libtinyxml_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libtinyxml_static
# LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/tinyxml/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libtinyxml_static.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libOpenAL_shared
# LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/OpenAL/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOpenAL_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libOpenAL_static
# LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/OpenAL/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOpenAL_static.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libOgreMain_shared
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/Ogre/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOgreMain_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libOgreMain_static
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/Ogre/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOgreMain_static.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libRenderSystem_GLES2_shared
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/Ogre/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libRenderSystem_GLES2_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libRenderSystem_GLES2_static
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/Ogre/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libRenderSystem_GLES2_static.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libCEGUIMain_shared
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/cegui/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libCEGUIMain_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libCEGUIMain_static
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/cegui/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libCEGUIMain_static.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libOgreComponents_shared
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/Ogre/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOgreComponents_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libOgreComponents_static
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/Ogre/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libOgreComponents_static.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libCEGUI_shared
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/cegui/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libCEGUI_shared.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libCEGUI_static
# LOCAL_SRC_FILES := ../../../../../mm-libx/build/cegui/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libCEGUI_static.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################