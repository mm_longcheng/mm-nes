/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper061.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper061_Init(struct mmEmuMapper061* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper061_Reset;
    p->super.Write = &mmEmuMapper061_Write;
    //p->super.Read = &mmEmuMapper061_Read;
    //p->super.ReadLow = &mmEmuMapper061_ReadLow;
    //p->super.WriteLow = &mmEmuMapper061_WriteLow;
    //p->super.ExRead = &mmEmuMapper061_ExRead;
    //p->super.ExWrite = &mmEmuMapper061_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper061_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper061_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper061_HSync;
    //p->super.VSync = &mmEmuMapper061_VSync;
    //p->super.Clock = &mmEmuMapper061_Clock;
    //p->super.PPULatch = &mmEmuMapper061_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper061_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper061_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper061_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper061_IsStateSave;
    //p->super.SaveState = &mmEmuMapper061_SaveState;
    //p->super.LoadState = &mmEmuMapper061_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper061_Destroy(struct mmEmuMapper061* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper061_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper061* p = (struct mmEmuMapper061*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper061_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper061* p = (struct mmEmuMapper061*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0x30)
    {
    case 0x00:
    case 0x30:
        mmEmuMmu_SetPROM32KBank(mmu, addr & 0x0F);
        break;
    case 0x10:
    case 0x20:
        mmEmuMmu_SetPROM16KBank(mmu, 4, ((addr & 0x0F) << 1) | ((addr & 0x20) >> 4));
        mmEmuMmu_SetPROM16KBank(mmu, 6, ((addr & 0x0F) << 1) | ((addr & 0x20) >> 4));
        break;
    default:
        break;
    }

    if (addr & 0x80)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper061_Produce(void)
{
    struct mmEmuMapper061* p = (struct mmEmuMapper061*)mmMalloc(sizeof(struct mmEmuMapper061));
    mmEmuMapper061_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper061_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper061* p = (struct mmEmuMapper061*)(m);
    mmEmuMapper061_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER061 =
{
    &__static_mmEmuMapper061_Produce,
    &__static_mmEmuMapper061_Recycle,
};
