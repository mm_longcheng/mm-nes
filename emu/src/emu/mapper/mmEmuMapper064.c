/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper064.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper064_Init(struct mmEmuMapper064* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);
    p->irq_enable = 0;
    p->irq_mode = 0;
    p->irq_counter = 0;
    p->irq_counter2 = 0;
    p->irq_latch = 0;
    p->irq_reset = 0;

    //
    p->super.Reset = &mmEmuMapper064_Reset;
    p->super.Write = &mmEmuMapper064_Write;
    //p->super.Read = &mmEmuMapper064_Read;
    //p->super.ReadLow = &mmEmuMapper064_ReadLow;
    //p->super.WriteLow = &mmEmuMapper064_WriteLow;
    //p->super.ExRead = &mmEmuMapper064_ExRead;
    //p->super.ExWrite = &mmEmuMapper064_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper064_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper064_ExCmdWrite;
    p->super.HSync = &mmEmuMapper064_HSync;
    //p->super.VSync = &mmEmuMapper064_VSync;
    p->super.Clock = &mmEmuMapper064_Clock;
    //p->super.PPULatch = &mmEmuMapper064_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper064_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper064_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper064_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper064_IsStateSave;
    p->super.SaveState = &mmEmuMapper064_SaveState;
    p->super.LoadState = &mmEmuMapper064_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper064_Destroy(struct mmEmuMapper064* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);
    p->irq_enable = 0;
    p->irq_mode = 0;
    p->irq_counter = 0;
    p->irq_counter2 = 0;
    p->irq_latch = 0;
    p->irq_reset = 0;
}

MM_EXPORT_EMU void mmEmuMapper064_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper064* p = (struct mmEmuMapper064*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 1, mmu->PROM_08K_SIZE - 1, mmu->PROM_08K_SIZE - 1, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->reg[0] = p->reg[1] = p->reg[2] = 0;

    p->irq_enable = 0;
    p->irq_mode = 0;
    p->irq_counter = 0;
    p->irq_counter2 = 0;
    p->irq_latch = 0;
    p->irq_reset = 0;
}
MM_EXPORT_EMU void mmEmuMapper064_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper064* p = (struct mmEmuMapper064*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "$%04X:$%02X\n", addr, data );
    switch (addr & 0xF003)
    {
    case 0x8000:
        p->reg[0] = data & 0x0F;
        p->reg[1] = data & 0x40;
        p->reg[2] = data & 0x80;
        break;

    case 0x8001:
        switch (p->reg[0])
        {
        case 0x00:
            if (p->reg[2])
            {
                mmEmuMmu_SetVROM1KBank(mmu, 4, data + 0);
                mmEmuMmu_SetVROM1KBank(mmu, 5, data + 1);
            }
            else
            {
                mmEmuMmu_SetVROM1KBank(mmu, 0, data + 0);
                mmEmuMmu_SetVROM1KBank(mmu, 1, data + 1);
            }
            break;
        case 0x01:
            if (p->reg[2])
            {
                mmEmuMmu_SetVROM1KBank(mmu, 6, data + 0);
                mmEmuMmu_SetVROM1KBank(mmu, 7, data + 1);
            }
            else
            {
                mmEmuMmu_SetVROM1KBank(mmu, 2, data + 0);
                mmEmuMmu_SetVROM1KBank(mmu, 3, data + 1);
            }
            break;
        case 0x02:
            if (p->reg[2])
            {
                mmEmuMmu_SetVROM1KBank(mmu, 0, data);
            }
            else
            {
                mmEmuMmu_SetVROM1KBank(mmu, 4, data);
            }
            break;
        case 0x03:
            if (p->reg[2])
            {
                mmEmuMmu_SetVROM1KBank(mmu, 1, data);
            }
            else
            {
                mmEmuMmu_SetVROM1KBank(mmu, 5, data);
            }
            break;
        case 0x04:
            if (p->reg[2])
            {
                mmEmuMmu_SetVROM1KBank(mmu, 2, data);
            }
            else
            {
                mmEmuMmu_SetVROM1KBank(mmu, 6, data);
            }
            break;
        case 0x05:
            if (p->reg[2])
            {
                mmEmuMmu_SetVROM1KBank(mmu, 3, data);
            }
            else
            {
                mmEmuMmu_SetVROM1KBank(mmu, 7, data);
            }
            break;
        case 0x06:
            if (p->reg[1])
            {
                mmEmuMmu_SetPROM8KBank(mmu, 5, data);
            }
            else
            {
                mmEmuMmu_SetPROM8KBank(mmu, 4, data);
            }
            break;
        case 0x07:
            if (p->reg[1])
            {
                mmEmuMmu_SetPROM8KBank(mmu, 6, data);
            }
            else
            {
                mmEmuMmu_SetPROM8KBank(mmu, 5, data);
            }
            break;
        case 0x08:
            mmEmuMmu_SetVROM1KBank(mmu, 1, data);
            break;
        case 0x09:
            mmEmuMmu_SetVROM1KBank(mmu, 3, data);
            break;
        case 0x0F:
            if (p->reg[1])
            {
                mmEmuMmu_SetPROM8KBank(mmu, 4, data);
            }
            else
            {
                mmEmuMmu_SetPROM8KBank(mmu, 6, data);
            }
            break;
        default:
            break;
        }
        break;

    case 0xA000:
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;

    case 0xC000:
        p->irq_latch = data;
        if (p->irq_reset)
        {
            p->irq_counter = p->irq_latch;
        }
        break;
    case 0xC001:
        p->irq_reset = 0xFF;
        p->irq_counter = p->irq_latch;
        p->irq_mode = data & 0x01;
        break;
    case 0xE000:
        p->irq_enable = 0;
        if (p->irq_reset)
        {
            p->irq_counter = p->irq_latch;
        }
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->irq_enable = 0xFF;
        if (p->irq_reset)
        {
            p->irq_counter = p->irq_latch;
        }
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper064_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper064* p = (struct mmEmuMapper064*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (!p->irq_mode)
    {
        return;
    }

    p->irq_counter2 += cycles;
    while (p->irq_counter2 >= 4)
    {
        p->irq_counter2 -= 4;
        if (p->irq_counter >= 0)
        {
            p->irq_counter--;
            if (p->irq_counter < 0)
            {
                if (p->irq_enable)
                {
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper064_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper064* p = (struct mmEmuMapper064*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_mode)
    {
        return;
    }

    p->irq_reset = 0;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_counter >= 0)
            {
                p->irq_counter--;
                if (p->irq_counter < 0)
                {
                    if (p->irq_enable)
                    {
                        p->irq_reset = 1;
                        mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                    }
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper064_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper064_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper064* p = (struct mmEmuMapper064*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->irq_enable;
    buffer[4] = p->irq_mode;
    buffer[5] = p->irq_latch;
    buffer[6] = p->irq_reset;
    *((mmInt_t*)&buffer[8]) = p->irq_counter;
    *((mmInt_t*)&buffer[12]) = p->irq_counter2;
}
MM_EXPORT_EMU void mmEmuMapper064_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper064* p = (struct mmEmuMapper064*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->irq_enable = buffer[3];
    p->irq_mode = buffer[4];
    p->irq_latch = buffer[5];
    p->irq_reset = buffer[6];
    p->irq_counter = *((mmInt_t*)&buffer[8]);
    p->irq_counter2 = *((mmInt_t*)&buffer[12]);
}

static struct mmEmuMapper* __static_mmEmuMapper064_Produce(void)
{
    struct mmEmuMapper064* p = (struct mmEmuMapper064*)mmMalloc(sizeof(struct mmEmuMapper064));
    mmEmuMapper064_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper064_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper064* p = (struct mmEmuMapper064*)(m);
    mmEmuMapper064_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER064 =
{
    &__static_mmEmuMapper064_Produce,
    &__static_mmEmuMapper064_Recycle,
};
