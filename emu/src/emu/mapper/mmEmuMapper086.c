/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper086.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuExtSoundFile.h"

MM_EXPORT_EMU void mmEmuMapper086_Init(struct mmEmuMapper086* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0xFF;
    p->cnt = 0;

    //
    p->super.Reset = &mmEmuMapper086_Reset;
    //p->super.Write = &mmEmuMapper086_Write;
    //p->super.Read = &mmEmuMapper086_Read;
    //p->super.ReadLow = &mmEmuMapper086_ReadLow;
    p->super.WriteLow = &mmEmuMapper086_WriteLow;
    //p->super.ExRead = &mmEmuMapper086_ExRead;
    //p->super.ExWrite = &mmEmuMapper086_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper086_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper086_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper086_HSync;
    p->super.VSync = &mmEmuMapper086_VSync;
    //p->super.Clock = &mmEmuMapper086_Clock;
    //p->super.PPULatch = &mmEmuMapper086_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper086_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper086_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper086_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper086_IsStateSave;
    //p->super.SaveState = &mmEmuMapper086_SaveState;
    //p->super.LoadState = &mmEmuMapper086_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper086_Destroy(struct mmEmuMapper086* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0xFF;
    p->cnt = 0;
}

MM_EXPORT_EMU void mmEmuMapper086_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper086* p = (struct mmEmuMapper086*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    p->reg = 0xFF;
    p->cnt = 0;
}
MM_EXPORT_EMU void mmEmuMapper086_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper086* p = (struct mmEmuMapper086*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    struct mmEmuAudioConfig* audio_config = &nes->config.audio;

    if (addr == 0x6000)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (data & 0x30) >> 4);

        mmEmuMmu_SetVROM8KBank(mmu, (data & 0x03) | ((data & 0x40) >> 4));
    }
    if (addr == 0x7000)
    {
        if (!(p->reg & 0x10) && (data & 0x10) && !p->cnt)
        {
            //DEBUGOUT( "WR:$%02X\n", data );
            if ((data & 0x0F) == 0 ||   // Strike
                (data & 0x0F) == 5)     // Foul
            {
                p->cnt = 60;        // Prohibit the next utterance for about 1 second.
            }

            // Should it be an OSD ...
            if (audio_config->bExtraSoundEnable)
            {
                mmEmuNes_EsfAllStop(nes);
                mmEmuNes_EsfPlay(nes, MM_EMU_ESF_MOEPRO_STRIKE + (data & 0x0F));
            }
        }
        p->reg = data;
    }
}

MM_EXPORT_EMU void mmEmuMapper086_VSync(struct mmEmuMapper* super)
{
    struct mmEmuMapper086* p = (struct mmEmuMapper086*)(super);

    if (p->cnt)
    {
        p->cnt--;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper086_Produce(void)
{
    struct mmEmuMapper086* p = (struct mmEmuMapper086*)mmMalloc(sizeof(struct mmEmuMapper086));
    mmEmuMapper086_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper086_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper086* p = (struct mmEmuMapper086*)(m);
    mmEmuMapper086_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER086 =
{
    &__static_mmEmuMapper086_Produce,
    &__static_mmEmuMapper086_Recycle,
};
