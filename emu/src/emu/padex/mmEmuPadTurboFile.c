/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadTurboFile.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuPadTurboFile_Init(struct mmEmuPadTurboFile* p)
{
    mmEmuExpad_Init(&p->super);

    p->tf_address = 0;
    p->tf_dataold = 0;
    p->tf_databit = 0;
    p->tf_data = 0;

    //
    p->super.Reset = &mmEmuPadTurboFile_Reset;
    //p->super.Strobe = &mmEmuPadTurboFile_Strobe;
    //p->super.Read4016 = &mmEmuPadTurboFile_Read4016;
    p->super.Read4017 = &mmEmuPadTurboFile_Read4017;
    p->super.Write4016 = &mmEmuPadTurboFile_Write4016;
    //p->super.Write4017 = &mmEmuPadTurboFile_Write4017;
    //p->super.Sync = &mmEmuPadTurboFile_Sync;
    //p->super.SetSyncData = &mmEmuPadTurboFile_SetSyncData;
    //p->super.GetSyncData = &mmEmuPadTurboFile_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadTurboFile_Destroy(struct mmEmuPadTurboFile* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->tf_address = 0;
    p->tf_dataold = 0;
    p->tf_databit = 0;
    p->tf_data = 0;
}

MM_EXPORT_EMU void mmEmuPadTurboFile_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadTurboFile* p = (struct mmEmuPadTurboFile*)(super);

    p->tf_address = 0;
    p->tf_dataold = 0;
    p->tf_databit = 0;
    p->tf_data = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadTurboFile_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadTurboFile* p = (struct mmEmuPadTurboFile*)(super);

    return p->tf_data;
}
MM_EXPORT_EMU void mmEmuPadTurboFile_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadTurboFile* p = (struct mmEmuPadTurboFile*)(super);

    mmByte_t* ERAM = p->super.nes->mmu.ERAM;

    mmInt_t bank = mmEmuNes_GetTurboFileBank(p->super.nes);

    // Reset
    if (!(data & 0x02))
    {
        p->tf_address = 0;
        p->tf_databit = 0x01;
    }
    // Write bit
    if (data & 0x04)
    {
        ERAM[bank * 0x2000 + p->tf_address] &= ~p->tf_databit;
        ERAM[bank * 0x2000 + p->tf_address] |= (data & 0x01) ? p->tf_databit : 0;
    }
    // Address inc/bit shift
    if ((p->tf_dataold&(~data)) & 0x04)
    {
        if (p->tf_databit == 0x80)
        {
            p->tf_address = (p->tf_address + 1) & 0x1FFF;
            p->tf_databit = 0x01;
        }
        else
        {
            p->tf_databit <<= 1;
        }
    }
    // Read bit
    if (ERAM[bank * 0x2000 + p->tf_address] & p->tf_databit)
    {
        p->tf_data = 0x04;
    }
    else
    {
        p->tf_data = 0x00;
    }
    p->tf_dataold = data;
}

static struct mmEmuExpad* __static_mmEmuPadTurboFile_Produce(void)
{
    struct mmEmuPadTurboFile* p = (struct mmEmuPadTurboFile*)mmMalloc(sizeof(struct mmEmuPadTurboFile));
    mmEmuPadTurboFile_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadTurboFile_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadTurboFile* p = (struct mmEmuPadTurboFile*)(m);
    mmEmuPadTurboFile_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_TURBOFILE =
{
    &__static_mmEmuPadTurboFile_Produce,
    &__static_mmEmuPadTurboFile_Recycle,
};
