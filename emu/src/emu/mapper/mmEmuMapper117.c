/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper117.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper117_Init(struct mmEmuMapper117* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper117_Reset;
    p->super.Write = &mmEmuMapper117_Write;
    //p->super.Read = &mmEmuMapper117_Read;
    //p->super.ReadLow = &mmEmuMapper117_ReadLow;
    //p->super.WriteLow = &mmEmuMapper117_WriteLow;
    //p->super.ExRead = &mmEmuMapper117_ExRead;
    //p->super.ExWrite = &mmEmuMapper117_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper117_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper117_ExCmdWrite;
    p->super.HSync = &mmEmuMapper117_HSync;
    //p->super.VSync = &mmEmuMapper117_VSync;
    //p->super.Clock = &mmEmuMapper117_Clock;
    //p->super.PPULatch = &mmEmuMapper117_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper117_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper117_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper117_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper117_IsStateSave;
    p->super.SaveState = &mmEmuMapper117_SaveState;
    p->super.LoadState = &mmEmuMapper117_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper117_Destroy(struct mmEmuMapper117* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper117_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper117* p = (struct mmEmuMapper117*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper117_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper117* p = (struct mmEmuMapper117*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0x8001:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0x8002:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;
    case 0xA000:
        mmEmuMmu_SetVROM1KBank(mmu, 0, data);
        break;
    case 0xA001:
        mmEmuMmu_SetVROM1KBank(mmu, 1, data);
        break;
    case 0xA002:
        mmEmuMmu_SetVROM1KBank(mmu, 2, data);
        break;
    case 0xA003:
        mmEmuMmu_SetVROM1KBank(mmu, 3, data);
        break;
    case 0xA004:
        mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        break;
    case 0xA005:
        mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        break;
    case 0xA006:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        break;
    case 0xA007:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        break;
    case 0xC001:
    case 0xC002:
    case 0xC003:
        p->irq_counter = data;
        break;
    case 0xE000:
        p->irq_enable = data & 1;
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper117_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper117* p = (struct mmEmuMapper117*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (p->irq_counter == scanline)
                {
                    p->irq_counter = 0;
                    //mm_emu_cpu_IRQ(&nes->cpu);
                    //mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_TRIGGER);
                }
            }
        }
    }
}
// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper117_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper117_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper117* p = (struct mmEmuMapper117*)(super);

    buffer[0] = p->irq_counter;
    buffer[1] = p->irq_enable;
}
MM_EXPORT_EMU void mmEmuMapper117_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper117* p = (struct mmEmuMapper117*)(super);

    p->irq_counter = buffer[0];
    p->irq_enable = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper117_Produce(void)
{
    struct mmEmuMapper117* p = (struct mmEmuMapper117*)mmMalloc(sizeof(struct mmEmuMapper117));
    mmEmuMapper117_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper117_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper117* p = (struct mmEmuMapper117*)(m);
    mmEmuMapper117_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER117 =
{
    &__static_mmEmuMapper117_Produce,
    &__static_mmEmuMapper117_Recycle,
};
