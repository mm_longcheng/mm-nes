/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper105.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper105_Init(struct mmEmuMapper105* p)
{
    mmEmuMapper_Init(&p->super);

    p->init_state = 0;
    p->write_count = 0;
    p->bits = 0;

    p->reg[0] = 0x0C;
    p->reg[1] = 0x00;
    p->reg[2] = 0x00;
    p->reg[3] = 0x10;

    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper105_Reset;
    p->super.Write = &mmEmuMapper105_Write;
    //p->super.Read = &mmEmuMapper105_Read;
    //p->super.ReadLow = &mmEmuMapper105_ReadLow;
    //p->super.WriteLow = &mmEmuMapper105_WriteLow;
    //p->super.ExRead = &mmEmuMapper105_ExRead;
    //p->super.ExWrite = &mmEmuMapper105_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper105_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper105_ExCmdWrite;
    p->super.HSync = &mmEmuMapper105_HSync;
    //p->super.VSync = &mmEmuMapper105_VSync;
    //p->super.Clock = &mmEmuMapper105_Clock;
    //p->super.PPULatch = &mmEmuMapper105_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper105_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper105_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper105_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper105_IsStateSave;
    p->super.SaveState = &mmEmuMapper105_SaveState;
    p->super.LoadState = &mmEmuMapper105_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper105_Destroy(struct mmEmuMapper105* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->init_state = 0;
    p->write_count = 0;
    p->bits = 0;

    p->reg[0] = 0x0C;
    p->reg[1] = 0x00;
    p->reg[2] = 0x00;
    p->reg[3] = 0x10;

    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper105_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper105* p = (struct mmEmuMapper105*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);

    p->reg[0] = 0x0C;
    p->reg[1] = 0x00;
    p->reg[2] = 0x00;
    p->reg[3] = 0x10;

    p->bits = 0;
    p->write_count = 0;

    p->irq_counter = 0;
    p->irq_enable = 0;
    p->init_state = 0;
}
MM_EXPORT_EMU void mmEmuMapper105_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper105* p = (struct mmEmuMapper105*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmWord_t reg_num = (addr & 0x7FFF) >> 13;

    if (data & 0x80)
    {
        p->bits = p->write_count = 0;
        if (reg_num == 0)
        {
            p->reg[reg_num] |= 0x0C;
        }
    }
    else
    {
        p->bits |= (data & 1) << p->write_count++;
        if (p->write_count == 5)
        {
            p->reg[reg_num] = p->bits & 0x1F;
            p->bits = p->write_count = 0;
        }
    }

    if (p->reg[0] & 0x02)
    {
        if (p->reg[0] & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
    }
    else
    {
        if (p->reg[0] & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
    }

    switch (p->init_state)
    {
    case 0:
    case 1:
        p->init_state++;
        break;
    case 2:
        if (p->reg[1] & 0x08)
        {
            if (p->reg[0] & 0x08)
            {
                if (p->reg[0] & 0x04)
                {
                    mmEmuMmu_SetPROM8KBank(mmu, 4, ((p->reg[3] & 0x07) * 2 + 16));
                    mmEmuMmu_SetPROM8KBank(mmu, 5, ((p->reg[3] & 0x07) * 2 + 17));
                    mmEmuMmu_SetPROM8KBank(mmu, 6, 30);
                    mmEmuMmu_SetPROM8KBank(mmu, 7, 31);
                }
                else
                {
                    mmEmuMmu_SetPROM8KBank(mmu, 4, 16);
                    mmEmuMmu_SetPROM8KBank(mmu, 5, 17);
                    mmEmuMmu_SetPROM8KBank(mmu, 6, ((p->reg[3] & 0x07) * 2 + 16));
                    mmEmuMmu_SetPROM8KBank(mmu, 7, ((p->reg[3] & 0x07) * 2 + 17));
                }
            }
            else
            {
                mmEmuMmu_SetPROM8KBank(mmu, 4, ((p->reg[3] & 0x06) * 2 + 16));
                mmEmuMmu_SetPROM8KBank(mmu, 5, ((p->reg[3] & 0x06) * 2 + 17));
                mmEmuMmu_SetPROM8KBank(mmu, 6, ((p->reg[3] & 0x06) * 2 + 18));
                mmEmuMmu_SetPROM8KBank(mmu, 7, ((p->reg[3] & 0x06) * 2 + 19));
            }
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, ((p->reg[1] & 0x06) * 2 + 0));
            mmEmuMmu_SetPROM8KBank(mmu, 5, ((p->reg[1] & 0x06) * 2 + 1));
            mmEmuMmu_SetPROM8KBank(mmu, 6, ((p->reg[1] & 0x06) * 2 + 2));
            mmEmuMmu_SetPROM8KBank(mmu, 7, ((p->reg[1] & 0x06) * 2 + 3));
        }

        if (p->reg[1] & 0x10)
        {
            p->irq_counter = 0;
            p->irq_enable = 0;
        }
        else
        {
            p->irq_enable = 1;
        }
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper105_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper105* p = (struct mmEmuMapper105*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (!scanline)
    {
        if (p->irq_enable)
        {
            p->irq_counter += 29781;
        }
        if (((p->irq_counter | 0x21FFFFFF) & 0x3E000000) == 0x3E000000)
        {
            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
            //mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_TRIGGER2);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper105_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper105_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper105* p = (struct mmEmuMapper105*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 4);
    buffer[8] = p->init_state;
    buffer[9] = p->write_count;
    buffer[10] = p->bits;
    buffer[11] = p->irq_enable;
    *((mmInt_t*)&buffer[12]) = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper105_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper105* p = (struct mmEmuMapper105*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 4);
    p->init_state = buffer[8];
    p->write_count = buffer[9];
    p->bits = buffer[10];
    p->irq_enable = buffer[11];
    p->irq_counter = *((mmInt_t*)&buffer[12]);
}

static struct mmEmuMapper* __static_mmEmuMapper105_Produce(void)
{
    struct mmEmuMapper105* p = (struct mmEmuMapper105*)mmMalloc(sizeof(struct mmEmuMapper105));
    mmEmuMapper105_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper105_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper105* p = (struct mmEmuMapper105*)(m);
    mmEmuMapper105_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER105 =
{
    &__static_mmEmuMapper105_Produce,
    &__static_mmEmuMapper105_Recycle,
};
