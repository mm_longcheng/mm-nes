/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper099.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper099_Init(struct mmEmuMapper099* p)
{
    mmEmuMapper_Init(&p->super);

    p->coin = 0;

    //
    p->super.Reset = &mmEmuMapper099_Reset;
    //p->super.Write = &mmEmuMapper099_Write;
    //p->super.Read = &mmEmuMapper099_Read;
    //p->super.ReadLow = &mmEmuMapper099_ReadLow;
    //p->super.WriteLow = &mmEmuMapper099_WriteLow;
    p->super.ExRead = &mmEmuMapper099_ExRead;
    p->super.ExWrite = &mmEmuMapper099_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper099_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper099_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper099_HSync;
    //p->super.VSync = &mmEmuMapper099_VSync;
    //p->super.Clock = &mmEmuMapper099_Clock;
    //p->super.PPULatch = &mmEmuMapper099_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper099_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper099_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper099_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper099_IsStateSave;
    //p->super.SaveState = &mmEmuMapper099_SaveState;
    //p->super.LoadState = &mmEmuMapper099_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper099_Destroy(struct mmEmuMapper099* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->coin = 0;
}

MM_EXPORT_EMU void mmEmuMapper099_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper099* p = (struct mmEmuMapper099*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // set CPU bank pointers
    if (mmu->PROM_08K_SIZE > 2)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
    }
    else if (mmu->PROM_08K_SIZE > 1)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 0, 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 0, 0, 0);
    }

    // set VROM bank
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->coin = 0;
}
MM_EXPORT_EMU mmByte_t mmEmuMapper099_ExRead(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper099* p = (struct mmEmuMapper099*)(super);

    if (addr == 0x4020)
    {
        return p->coin;
    }

    return addr >> 8;
}
MM_EXPORT_EMU void mmEmuMapper099_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper099* p = (struct mmEmuMapper099*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    if (addr == 0x4016)
    {
        if (data & 0x04)
        {
            mmEmuMmu_SetVROM8KBank(mmu, 1);
        }
        else
        {
            mmEmuMmu_SetVROM8KBank(mmu, 0);
        }

        crc = mmEmuRom_GetPROMCRC(&nes->rom);

        if (crc == 0xC99EC059)      // VS Raid on Bungeling Bay(J)
        {
            if (data & 0x02)
            {
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
            else
            {
                mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
        }
    }

    if (addr == 0x4020)
    {
        p->coin = data;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper099_Produce(void)
{
    struct mmEmuMapper099* p = (struct mmEmuMapper099*)mmMalloc(sizeof(struct mmEmuMapper099));
    mmEmuMapper099_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper099_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper099* p = (struct mmEmuMapper099*)(m);
    mmEmuMapper099_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER099 =
{
    &__static_mmEmuMapper099_Produce,
    &__static_mmEmuMapper099_Recycle,
};
