/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuRomNesHeader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

MM_EXPORT_EMU const char* MM_NESHEADER_VERSION[3] =
{
    "iNes old",
    "iNes 1.0",
    "iNes 2.0",
};
MM_EXPORT_EMU const char* mmEmuRomNesHeader_VersionToString(int version)
{
    if (MM_EMU_ROMHEADERVERSION_ONES <= version && version <= MM_EMU_ROMHEADERVERSION_NES2)
    {
        return MM_NESHEADER_VERSION[version];
    }
    else
    {
        return "Unknown";
    }
}
MM_EXPORT_EMU const char* MM_NESHEADER_GAMESYSTEM[8] =
{
    "NesNtsc",
    "NesPal",
    "Famicom",
    "Dendy",
    "VsSystem",
    "PlayChoice",
    "FDS",
    "Unknown",
};
MM_EXPORT_EMU const char* mmEmuRomNesHeader_GameSystemToString(int type)
{
    if (MM_EMU_GAMESYSTEM_NESNTSC <= type && type <= MM_EMU_GAMESYSTEM_UNKNOWN)
    {
        return MM_NESHEADER_GAMESYSTEM[type];
    }
    else
    {
        return "Unknown";
    }
}
MM_EXPORT_EMU const char* MM_NESHEADER_MIRRORING[5] =
{
    "Horizontal",
    "Vertical",
    "ScreenAOnly",
    "ScreenBOnly",
    "FourScreens",
};
MM_EXPORT_EMU const char* mmEmuRomNesHeader_MirroringToString(int type)
{
    if (MM_EMU_MIRRORING_HORIZONTAL <= type && type <= MM_EMU_MIRRORING_FOURSCREENS)
    {
        return MM_NESHEADER_MIRRORING[type];
    }
    else
    {
        return "Unknown";
    }
}

MM_EXPORT_EMU void mmEmuRomNesHeader_Init(struct mmEmuRomNesHeader* p)
{
    mmMemset(p, 0, sizeof(struct mmEmuRomNesHeader));
}
MM_EXPORT_EMU void mmEmuRomNesHeader_Destroy(struct mmEmuRomNesHeader* p)
{
    mmMemset(p, 0, sizeof(struct mmEmuRomNesHeader));
}

MM_EXPORT_EMU mmBool_t mm_emu_rom_nesheader_Validity(struct mmEmuRomNesHeader* p)
{
    return
        (p->ID[0] == 'N' && p->ID[1] == 'E' && p->ID[2] == 'S' && p->ID[3] == 0x1A) ||
        (p->ID[0] == 'F' && p->ID[1] == 'D' && p->ID[2] == 'S' && p->ID[3] == 0x1A) ||
        (p->ID[0] == 'N' && p->ID[1] == 'E' && p->ID[2] == 'S' && p->ID[3] == 'M');
}

MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetRomHeaderVersion(const struct mmEmuRomNesHeader* p)
{
    if ((p->control_07 & 0x0C) == 0x08)
    {
        return MM_EMU_ROMHEADERVERSION_NES2;
    }
    else if ((p->control_07 & 0x0C) == 0x00)
    {
        return MM_EMU_ROMHEADERVERSION_INES;
    }
    else
    {
        return MM_EMU_ROMHEADERVERSION_ONES;
    }
}
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetMapperID(const struct mmEmuRomNesHeader* p)
{
    switch (mmEmuRomNesHeader_GetRomHeaderVersion(p))
    {
    case MM_EMU_ROMHEADERVERSION_NES2:
        return ((p->control_08 & 0x0F) << 8) | (p->control_07 & 0xF0) | (p->control_06 >> 4);
    default:
    case MM_EMU_ROMHEADERVERSION_INES:
        return (p->control_07 & 0xF0) | (p->control_06 >> 4);
    case MM_EMU_ROMHEADERVERSION_ONES:
        return (p->control_06 >> 4);
    }
}
MM_EXPORT_EMU mmBool_t mmEmuRomNesHeader_HasBattery(const struct mmEmuRomNesHeader* p)
{
    return (p->control_06 & 0x02) == 0x02;
}
MM_EXPORT_EMU mmBool_t mmEmuRomNesHeader_HasTrainer(const struct mmEmuRomNesHeader* p)
{
    return (p->control_06 & 0x04) == 0x04;
}
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetNesGameSystem(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        switch (p->control_12 & 0x03)
        {
        case 0: return MM_EMU_GAMESYSTEM_NESNTSC;
        case 1: return MM_EMU_GAMESYSTEM_NESPAL;
        case 2: return MM_EMU_GAMESYSTEM_NESNTSC; //Game works with both NTSC/PAL, pick NTSC by default
        case 3: return MM_EMU_GAMESYSTEM_DENDY;
        default:
            break;
        }
    }
    else if (version == MM_EMU_ROMHEADERVERSION_INES)
    {
        return (p->control_09 & 0x01) ? MM_EMU_GAMESYSTEM_NESPAL : MM_EMU_GAMESYSTEM_NESNTSC;
    }
    return MM_EMU_GAMESYSTEM_NESNTSC;
}
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetGameSystem(const struct mmEmuRomNesHeader* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        switch (p->control_07 & 0x03)
        {
        case 0: return mmEmuRomNesHeader_GetNesGameSystem(p);
        case 1: return MM_EMU_GAMESYSTEM_VSSYSTEM;
        case 2: return MM_EMU_GAMESYSTEM_PLAYCHOICE;
        case 3:
            switch (p->control_13)
            {
            case 0: return mmEmuRomNesHeader_GetNesGameSystem(p);
            case 1: return MM_EMU_GAMESYSTEM_VSSYSTEM;
            case 2: return MM_EMU_GAMESYSTEM_PLAYCHOICE;
            default:
                mmLogger_LogI(gLogger, "%s %d [iNes] Unsupported console type detected (using NES NTSC instead)", __FUNCTION__, __LINE__);
                return MM_EMU_GAMESYSTEM_NESNTSC;

            }
            break;
        }
    }
    else if (version == MM_EMU_ROMHEADERVERSION_INES)
    {
        if (p->control_07 & 0x01)
        {
            return MM_EMU_GAMESYSTEM_VSSYSTEM;
        }
        else if (p->control_07 & 0x02)
        {
            return MM_EMU_GAMESYSTEM_PLAYCHOICE;
        }
    }
    return mmEmuRomNesHeader_GetNesGameSystem(p);
}
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetSizeValue(const struct mmEmuRomNesHeader* p, mmUInt32_t exponent, mmUInt32_t multiplier)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt64_t size = 0;

    if (exponent > 60)
    {
        //Restrict max size to avoid overflow in a 64-bit value
        exponent = 60;
        mmLogger_LogI(gLogger, "%s %d [iNes] Unsupported size value.", __FUNCTION__, __LINE__);
    }

    multiplier = multiplier * 2 + 1;

    size = multiplier * (mmUInt64_t)1 << exponent;
    if (size >= ((mmUInt64_t)1 << 32))
    {
        mmLogger_LogI(gLogger, "%s %d [iNes] Unsupported size value.", __FUNCTION__, __LINE__);
    }
    return (mmUInt32_t)size;
}
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetPrgSize(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        if ((p->control_09 & 0x0F) == 0x0F)
        {
            return (mmUInt32_t)mmEmuRomNesHeader_GetSizeValue(p, p->PRG_PAGE_SIZE >> 2, p->PRG_PAGE_SIZE & 0x03);
        }
        else
        {
            return (((p->control_09 & 0x0F) << 8) | p->PRG_PAGE_SIZE) * 0x4000;
        }
    }
    else
    {
        if (p->PRG_PAGE_SIZE == 0)
        {
            return 256 * 0x4000; //0 is a special value and means 256
        }
        else
        {
            return p->PRG_PAGE_SIZE * 0x4000;
        }
    }
}
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetChrSize(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        if ((p->control_09 & 0xF0) == 0xF0)
        {
            return (mmUInt32_t)mmEmuRomNesHeader_GetSizeValue(p, p->CHR_PAGE_SIZE >> 2, p->CHR_PAGE_SIZE & 0x03);
        }
        else
        {
            return (((p->control_09 & 0xF0) << 4) | p->CHR_PAGE_SIZE) * 0x2000;
        }
    }
    else
    {
        return p->CHR_PAGE_SIZE * 0x2000;
    }
}
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetWorkRamSize(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        mmUInt8_t value = p->control_10 & 0x0F;
        return value == 0 ? 0 : 128 * (mmUInt32_t)pow(2, value - 1);
    }
    else
    {
        return -1;
    }
}
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetSaveRamSize(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        mmUInt8_t value = (p->control_10 & 0xF0) >> 4;
        return value == 0 ? 0 : 128 * (mmUInt32_t)pow(2, value - 1);
    }
    else
    {
        return -1;
    }
}
MM_EXPORT_EMU mmSInt32_t mmEmuRomNesHeader_GetChrRamSize(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        mmUInt8_t value = p->control_11 & 0x0F;
        return value == 0 ? 0 : 128 * (mmUInt32_t)pow(2, value - 1);
    }
    else
    {
        return -1;
    }
}
MM_EXPORT_EMU mmUInt32_t mmEmuRomNesHeader_GetSaveChrRamSize(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        mmUInt8_t value = (p->control_11 & 0xF0) >> 4;
        return value == 0 ? 0 : 128 * (mmUInt32_t)pow(2, value - 1);
    }
    else
    {
        return -1;
    }
}
MM_EXPORT_EMU mmUInt8_t mmEmuRomNesHeader_GetSubMapper(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        return (p->control_08 & 0xF0) >> 4;
    }
    else
    {
        return 0;
    }
}
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetMirroringType(const struct mmEmuRomNesHeader* p)
{
    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (p->control_06 & 0x08)
    {
        if (version == MM_EMU_ROMHEADERVERSION_NES2)
        {
            if (p->control_06 & 0x01)
            {
                //Based on proposal by rainwarrior/Myask: http://wiki.nesdev.com/w/index.php/Talk:NES_2.0
                return MM_EMU_MIRRORING_SCREENAONLY;
            }
            else
            {
                return MM_EMU_MIRRORING_FOURSCREENS;
            }
        }
        else
        {
            return MM_EMU_MIRRORING_FOURSCREENS;
        }
    }
    else
    {
        return p->control_06 & 0x01 ? MM_EMU_MIRRORING_VERTICAL : MM_EMU_MIRRORING_HORIZONTAL;
    }
}
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetInputType(const struct mmEmuRomNesHeader* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        if (p->control_15 < (mmUInt8_t)MM_EMU_INPUT_LASTENTRY)
        {
            return (mmUInt16_t)p->control_15;
        }

        mmLogger_LogI(gLogger, "%s %d [iNes] Unknown controller type.", __FUNCTION__, __LINE__);

        return MM_EMU_INPUT_UNSPECIFIED;
    }
    else
    {
        return MM_EMU_INPUT_UNSPECIFIED;
    }
}
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetVsSystemType(const struct mmEmuRomNesHeader* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        if ((p->control_13 >> 4) <= 0x06)
        {
            return (mmUInt16_t)(p->control_13 >> 4);
        }

        mmLogger_LogI(gLogger, "%s %d [iNes] Unknown VS System Type specified.", __FUNCTION__, __LINE__);
    }
    return MM_EMU_VSSYSTEM_DEFAULT;
}
MM_EXPORT_EMU mmUInt16_t mmEmuRomNesHeader_GetVsSystemPpuModel(const struct mmEmuRomNesHeader* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt16_t version = mmEmuRomNesHeader_GetRomHeaderVersion(p);

    if (version == MM_EMU_ROMHEADERVERSION_NES2)
    {
        switch (p->control_13 & 0x0F)
        {
        case 0: return MM_EMU_PPU2C03;
        case 1:
            mmLogger_LogI(gLogger, "%s %d [iNes] Unsupport VS System Palette specified (2C03G).", __FUNCTION__, __LINE__);
            return MM_EMU_PPU2C03;

        case 2: return MM_EMU_PPU2C04A;
        case 3: return MM_EMU_PPU2C04B;
        case 4: return MM_EMU_PPU2C04C;
        case 5: return MM_EMU_PPU2C04D;
        case 6: return MM_EMU_PPU2C03;
        case 7: return MM_EMU_PPU2C03;
        case 8: return MM_EMU_PPU2C05A;
        case 9: return MM_EMU_PPU2C05B;
        case 10: return MM_EMU_PPU2C05C;
        case 11: return MM_EMU_PPU2C05D;
        case 12: return MM_EMU_PPU2C05E;

        default:
            mmLogger_LogI(gLogger, "%s %d [iNes] Unknown VS System Palette specified.", __FUNCTION__, __LINE__);
            break;
        }
    }
    return MM_EMU_PPU2C03;
}

MM_EXPORT_EMU void mmEmuRomNesHeader_LoggerInformation(const struct mmEmuRomNesHeader* p, struct mmLogger* logger)
{
    mmUInt16_t RomHeaderVersion = mmEmuRomNesHeader_GetRomHeaderVersion(p);
    mmUInt16_t GameSystem = mmEmuRomNesHeader_GetGameSystem(p);
    mmUInt16_t MirroringType = mmEmuRomNesHeader_GetMirroringType(p);
    mmUInt32_t PrgSize = mmEmuRomNesHeader_GetPrgSize(p);
    mmUInt32_t ChrSize = mmEmuRomNesHeader_GetChrSize(p);

    mmUInt32_t WorkRamSize = mmEmuRomNesHeader_GetWorkRamSize(p);
    mmUInt32_t SaveRamSize = mmEmuRomNesHeader_GetSaveRamSize(p);
    mmUInt32_t ChrRamSize = mmEmuRomNesHeader_GetChrRamSize(p);
    mmUInt32_t SaveChrRamSize = mmEmuRomNesHeader_GetSaveChrRamSize(p);

    // we not need logger the -1 value.
    PrgSize = -1 == PrgSize ? 0 : PrgSize;
    ChrSize = -1 == ChrSize ? 0 : ChrSize;

    WorkRamSize = -1 == WorkRamSize ? 0 : WorkRamSize;
    SaveRamSize = -1 == SaveRamSize ? 0 : SaveRamSize;
    ChrRamSize = -1 == ChrRamSize ? 0 : ChrRamSize;
    SaveChrRamSize = -1 == SaveChrRamSize ? 0 : SaveChrRamSize;

    mmLogger_LogI(logger, "NES Header -----------------------------------");
    mmLogger_LogI(logger, "NES Header RomHeaderVersion: %s", mmEmuRomNesHeader_VersionToString(RomHeaderVersion));
    mmLogger_LogI(logger, "NES Header         MapperID: %u", (mmUInt32_t)mmEmuRomNesHeader_GetMapperID(p));
    mmLogger_LogI(logger, "NES Header        SubMapper: %u", (mmUInt32_t)mmEmuRomNesHeader_GetSubMapper(p));
    mmLogger_LogI(logger, "NES Header       HasBattery: %s", mmEmuRomNesHeader_HasBattery(p) ? "Yes" : "No");
    mmLogger_LogI(logger, "NES Header       HasTrainer: %s", mmEmuRomNesHeader_HasTrainer(p) ? "Yes" : "No");
    mmLogger_LogI(logger, "NES Header    MirroringType: %s", mmEmuRomNesHeader_MirroringToString(MirroringType));
    mmLogger_LogI(logger, "NES Header       GameSystem: %s", mmEmuRomNesHeader_GameSystemToString(GameSystem));
    mmLogger_LogI(logger, "NES Header          PrgSize: (%4u K) %u", PrgSize / 0x0400, PrgSize);
    mmLogger_LogI(logger, "NES Header          ChrSize: (%4u K) %u", ChrSize / 0x0400, ChrSize);
    mmLogger_LogI(logger, "NES Header      WorkRamSize: (%4u K) %u", WorkRamSize / 0x0400, WorkRamSize);
    mmLogger_LogI(logger, "NES Header      SaveRamSize: (%4u K) %u", SaveRamSize / 0x0400, SaveRamSize);
    mmLogger_LogI(logger, "NES Header       ChrRamSize: (%4u K) %u", ChrRamSize / 0x0400, ChrRamSize);
    mmLogger_LogI(logger, "NES Header   SaveChrRamSize: (%4u K) %u", SaveChrRamSize / 0x0400, SaveChrRamSize);
    mmLogger_LogI(logger, "NES Header        InputType: %u", (mmUInt32_t)mmEmuRomNesHeader_GetInputType(p));
    mmLogger_LogI(logger, "NES Header     VsSystemType: %u", (mmUInt32_t)mmEmuRomNesHeader_GetVsSystemType(p));
    mmLogger_LogI(logger, "NES Header VsSystemPpuModel: %u", (mmUInt32_t)mmEmuRomNesHeader_GetVsSystemPpuModel(p));
    mmLogger_LogI(logger, "NES Header -----------------------------------");
}

