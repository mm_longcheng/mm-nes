/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper226.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper226_Init(struct mmEmuMapper226* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 0;

    //
    p->super.Reset = &mmEmuMapper226_Reset;
    p->super.Write = &mmEmuMapper226_Write;
    //p->super.Read = &mmEmuMapper226_Read;
    //p->super.ReadLow = &mmEmuMapper226_ReadLow;
    //p->super.WriteLow = &mmEmuMapper226_WriteLow;
    //p->super.ExRead = &mmEmuMapper226_ExRead;
    //p->super.ExWrite = &mmEmuMapper226_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper226_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper226_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper226_HSync;
    //p->super.VSync = &mmEmuMapper226_VSync;
    //p->super.Clock = &mmEmuMapper226_Clock;
    //p->super.PPULatch = &mmEmuMapper226_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper226_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper226_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper226_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper226_IsStateSave;
    p->super.SaveState = &mmEmuMapper226_SaveState;
    p->super.LoadState = &mmEmuMapper226_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper226_Destroy(struct mmEmuMapper226* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 0;
}

MM_EXPORT_EMU void mmEmuMapper226_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper226* p = (struct mmEmuMapper226*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);

    p->reg[0] = 0;
    p->reg[1] = 0;
}
MM_EXPORT_EMU void mmEmuMapper226_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper226* p = (struct mmEmuMapper226*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t bank = 0;

    if (addr & 0x001)
    {
        p->reg[1] = data;
    }
    else
    {
        p->reg[0] = data;
    }

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }

    bank = ((p->reg[0] & 0x1E) >> 1) | ((p->reg[0] & 0x80) >> 3) | ((p->reg[1] & 0x01) << 5);

    if (p->reg[0] & 0x20)
    {
        if (p->reg[0] & 0x01)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, bank * 4 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 5, bank * 4 + 3);
            mmEmuMmu_SetPROM8KBank(mmu, 6, bank * 4 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 7, bank * 4 + 3);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, bank * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 5, bank * 4 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 6, bank * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, bank * 4 + 1);
        }
    }
    else
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, bank * 4 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, bank * 4 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, bank * 4 + 2);
        mmEmuMmu_SetPROM8KBank(mmu, 7, bank * 4 + 3);
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper226_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper226_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper226* p = (struct mmEmuMapper226*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
}
MM_EXPORT_EMU void mmEmuMapper226_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper226* p = (struct mmEmuMapper226*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper226_Produce(void)
{
    struct mmEmuMapper226* p = (struct mmEmuMapper226*)mmMalloc(sizeof(struct mmEmuMapper226));
    mmEmuMapper226_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper226_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper226* p = (struct mmEmuMapper226*)(m);
    mmEmuMapper226_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER226 =
{
    &__static_mmEmuMapper226_Produce,
    &__static_mmEmuMapper226_Recycle,
};
