/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadPaddle.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuPadPaddle_Init(struct mmEmuPadPaddle* p)
{
    mmEmuExpad_Init(&p->super);

    p->paddle_bits = 0;
    p->paddle_data = 0;

    p->paddle_posold = 0;

    p->paddle_x = 0;
    p->paddle_button = 0;

    //
    p->super.Reset = &mmEmuPadPaddle_Reset;
    //p->super.Strobe = &mmEmuPadPaddle_Strobe;
    p->super.Read4016 = &mmEmuPadPaddle_Read4016;
    p->super.Read4017 = &mmEmuPadPaddle_Read4017;
    p->super.Write4016 = &mmEmuPadPaddle_Write4016;
    //p->super.Write4017 = &mmEmuPadPaddle_Write4017;
    p->super.Sync = &mmEmuPadPaddle_Sync;
    p->super.SetSyncData = &mmEmuPadPaddle_SetSyncData;
    p->super.GetSyncData = &mmEmuPadPaddle_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadPaddle_Destroy(struct mmEmuPadPaddle* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->paddle_bits = 0;
    p->paddle_data = 0;

    p->paddle_posold = 0;

    p->paddle_x = 0;
    p->paddle_button = 0;
}

MM_EXPORT_EMU void mmEmuPadPaddle_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)(super);

    p->paddle_bits = 0;
    p->paddle_data = 0;
    p->paddle_posold = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadPaddle_Read4016(struct mmEmuExpad* super)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)(super);

    return p->paddle_bits;
}
MM_EXPORT_EMU mmByte_t mmEmuPadPaddle_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)(super);

    mmByte_t data = (p->paddle_data & 0x01) << 1;
    p->paddle_data >>= 1;
    return data;
}
MM_EXPORT_EMU void mmEmuPadPaddle_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)(super);

    mmLong_t x;

    mmByte_t px = 0;
    mmInt_t i = 0;

    p->paddle_bits = 0;
    if (p->paddle_button)
    {
        p->paddle_bits |= 0x02;
    }

    x = p->paddle_x;
    if (x < 0)
    {
        p->paddle_data = p->paddle_posold;
        return;
    }

    if (x < 32) x = 32;
    if (x > 223) x = 223;
    x = (192 * (x - 32)) / 192;

    px = 0xFF - (mmByte_t)(0x52 + 172 * x / 192);

    //
    p->paddle_data = 0;
    for (i = 0; i < 8; i++)
    {
        p->paddle_data |= (px&(1 << i)) ? (0x80 >> i) : 0;
    }
    p->paddle_posold = p->paddle_data;
}

MM_EXPORT_EMU void mmEmuPadPaddle_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmLong_t y;

    mmEmuNes_GetZapperPos(p->super.nes, &p->paddle_x, &y);

    p->paddle_button = 0;
    if (mmEmuController_MouseButtonStatus(controller, MM_MB_LBUTTON))
    {
        p->paddle_button = 0xFF;
    }
}
MM_EXPORT_EMU void mmEmuPadPaddle_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)(super);

    if (type == 0)
    {
        p->paddle_x = data;
    }
    else if (type == 2)
    {
        p->paddle_button = data ? 0xFF : 0x00;
    }
}
MM_EXPORT_EMU mmLong_t mmEmuPadPaddle_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)(super);

    mmLong_t data = 0;

    if (type == 0)
    {
        data = p->paddle_x;
    }
    else if (type == 2)
    {
        data = p->paddle_button ? 0xFF : 0x00;
    }
    return data;
}

static struct mmEmuExpad* __static_mmEmuPadPaddle_Produce(void)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)mmMalloc(sizeof(struct mmEmuPadPaddle));
    mmEmuPadPaddle_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadPaddle_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadPaddle* p = (struct mmEmuPadPaddle*)(m);
    mmEmuPadPaddle_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_PADDLE =
{
    &__static_mmEmuPadPaddle_Produce,
    &__static_mmEmuPadPaddle_Recycle,
};
