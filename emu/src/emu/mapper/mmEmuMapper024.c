/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper024.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper024_Init(struct mmEmuMapper024* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    //
    p->super.Reset = &mmEmuMapper024_Reset;
    p->super.Write = &mmEmuMapper024_Write;
    //p->super.Read = &mmEmuMapper024_Read;
    //p->super.ReadLow = &mmEmuMapper024_ReadLow;
    //p->super.WriteLow = &mmEmuMapper024_WriteLow;
    //p->super.ExRead = &mmEmuMapper024_ExRead;
    //p->super.ExWrite = &mmEmuMapper024_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper024_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper024_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper024_HSync;
    //p->super.VSync = &mmEmuMapper024_VSync;
    p->super.Clock = &mmEmuMapper024_Clock;
    //p->super.PPULatch = &mmEmuMapper024_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper024_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper024_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper024_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper024_IsStateSave;
    p->super.SaveState = &mmEmuMapper024_SaveState;
    p->super.LoadState = &mmEmuMapper024_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper024_Destroy(struct mmEmuMapper024* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;
}

MM_EXPORT_EMU void mmEmuMapper024_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper024* p = (struct mmEmuMapper024*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    //mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_RENDER);

    mmEmuApu_SelectExSound(&nes->apu, 1);
}
MM_EXPORT_EMU void mmEmuMapper024_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper024* p = (struct mmEmuMapper024*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF003)
    {
    case 0x8000:
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        break;

    case 0x9000: case 0x9001: case 0x9002:
    case 0xA000: case 0xA001: case 0xA002:
    case 0xB000: case 0xB001: case 0xB002:
        mmEmuApu_ExWrite(&nes->apu, addr, data);
        break;

    case 0xB003:
        data = data & 0x0C;
        if (data == 0x00)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 0x04)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 0x08)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else if (data == 0x0C)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0xC000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;

    case 0xD000:
        mmEmuMmu_SetVROM1KBank(mmu, 0, data);
        break;

    case 0xD001:
        mmEmuMmu_SetVROM1KBank(mmu, 1, data);
        break;

    case 0xD002:
        mmEmuMmu_SetVROM1KBank(mmu, 2, data);
        break;

    case 0xD003:
        mmEmuMmu_SetVROM1KBank(mmu, 3, data);
        break;

    case 0xE000:
        mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        break;

    case 0xE001:
        mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        break;

    case 0xE002:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        break;

    case 0xE003:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        break;

    case 0xF000:
        p->irq_latch = data;
        break;
    case 0xF001:
        p->irq_enable = data & 0x03;
        if (p->irq_enable & 0x02)
        {
            p->irq_counter = p->irq_latch;
            p->irq_clock = 0;
        }
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xF002:
        p->irq_enable = (p->irq_enable & 0x01) * 3;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper024_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper024* p = (struct mmEmuMapper024*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable & 0x02)
    {
        if ((p->irq_clock += cycles) >= 0x72)
        {
            p->irq_clock -= 0x72;
            if (p->irq_counter == 0xFF)
            {
                p->irq_counter = p->irq_latch;
                //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
            else
            {
                p->irq_counter++;
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper024_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper024_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper024* p = (struct mmEmuMapper024*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_counter;
    buffer[2] = p->irq_latch;
    *(mmInt_t*)&buffer[3] = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper024_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper024* p = (struct mmEmuMapper024*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = buffer[1];
    p->irq_latch = buffer[2];
    p->irq_clock = *(mmInt_t*)&buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper024_Produce(void)
{
    struct mmEmuMapper024* p = (struct mmEmuMapper024*)mmMalloc(sizeof(struct mmEmuMapper024));
    mmEmuMapper024_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper024_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper024* p = (struct mmEmuMapper024*)(m);
    mmEmuMapper024_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER024 =
{
    &__static_mmEmuMapper024_Produce,
    &__static_mmEmuMapper024_Recycle,
};
