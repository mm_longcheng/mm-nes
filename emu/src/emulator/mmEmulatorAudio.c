/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmulatorAudio.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static void __static_mmEmulatorAudio_ErrorCheck(int lvl, ALenum code, const char* func, int line);

#define __static_mmEmulatorAudio_Check(lvl, code) __static_mmEmulatorAudio_ErrorCheck(lvl, code, __FUNCTION__, __LINE__)

MM_EXPORT_EMULATOR void mmEmulatorAudio_Init(struct mmEmulatorAudio* p)
{
    mmString_Init(&p->device_name);

    mmRbtsetString_Init(&p->devices);

    mmRbtsetU32_Init(&p->pool);
    mmRbtsetU32_Init(&p->used);
    mmRbtsetU32_Init(&p->idle);
    p->rate_trim_border = MM_EMULATOR_AUDIO_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_EMULATOR_AUDIO_RATE_TRIM_NUMBER_DEFAULT;

    p->accumulation_limit = MM_EMULATOR_AUDIO_ACCUMULATION_LIMIT_DEFAULT;

    p->mute = MM_FALSE;
    p->background = MM_FALSE;

    p->volume = 1.0f;

    p->nRate = 22050;
    p->nFormat = AL_FORMAT_MONO8;
    p->device = NULL;
    p->context = NULL;
    p->source = AL_NONE;

    mmMemset(p->ListenerOrientation, 0, sizeof(ALfloat) * 6);
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_Destroy(struct mmEmulatorAudio* p)
{
    mmEmulatorAudio_BufferClear(p);

    assert(NULL == p->context && NULL == p->device && "you must DeleteSource.");
    //
    mmString_Destroy(&p->device_name);

    mmRbtsetString_Destroy(&p->devices);

    mmRbtsetU32_Destroy(&p->pool);
    mmRbtsetU32_Destroy(&p->used);
    mmRbtsetU32_Destroy(&p->idle);
    p->rate_trim_border = MM_EMULATOR_AUDIO_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_EMULATOR_AUDIO_RATE_TRIM_NUMBER_DEFAULT;

    p->accumulation_limit = MM_EMULATOR_AUDIO_ACCUMULATION_LIMIT_DEFAULT;

    p->mute = MM_FALSE;
    p->background = MM_FALSE;

    p->volume = 1.0f;

    p->nRate = 22050;
    p->nFormat = AL_FORMAT_MONO8;
    p->device = NULL;
    p->context = NULL;
    p->source = AL_NONE;

    mmMemset(p->ListenerOrientation, 0, sizeof(ALfloat) * 6);
}

MM_EXPORT_EMULATOR void mmEmulatorAudio_SetDeviceName(struct mmEmulatorAudio* p, const char* device_name)
{
    mmString_Assigns(&p->device_name, device_name);
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetRate(struct mmEmulatorAudio* p, int nRate)
{
    p->nRate = nRate;
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetFormat(struct mmEmulatorAudio* p, ALenum nFormat)
{
    p->nFormat = nFormat;
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetMute(struct mmEmulatorAudio* p, mmBool_t mute)
{
    p->mute = mute;
}

MM_EXPORT_EMULATOR ALuint mmEmulatorAudio_BufferAdd(struct mmEmulatorAudio* p)
{
    ALuint e = 0;

    alGenBuffers(1, &e);
    __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

    // add to pool set.
    mmRbtsetU32_Add(&p->pool, (mmUInt32_t)e);
    return e;
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_BufferRmv(struct mmEmulatorAudio* p, ALuint e)
{
    // rmv to pool set.
    mmRbtsetU32_Rmv(&p->pool, (mmUInt32_t)e);

    alDeleteBuffers(1, &e);
    __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_BufferClear(struct mmEmulatorAudio* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    ALuint e = 0;
    //
    n = mmRb_First(&p->pool.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        e = (ALuint)(it->k);
        n = mmRb_Next(n);
        mmRbtsetU32_Erase(&p->pool, it);

        alDeleteBuffers(1, &e);
        __static_mmEmulatorAudio_Check(MM_LOG_TRACE, alGetError());
    }
    mmRbtsetU32_Clear(&p->idle);
    mmRbtsetU32_Clear(&p->used);
}
MM_EXPORT_EMULATOR ALuint mmEmulatorAudio_BufferProduce(struct mmEmulatorAudio* p)
{
    ALuint v = 0;

    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    // min.
    n = mmRb_Last(&p->idle.rbt);
    if (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        v = (ALuint)it->k;
        // rmv to idle set.
        mmRbtsetU32_Rmv(&p->idle, v);
        // add to used set.
        mmRbtsetU32_Add(&p->used, (mmUInt32_t)v);
    }
    else
    {
        // add new.
        v = mmEmulatorAudio_BufferAdd(p);
        // add to used set.
        mmRbtsetU32_Add(&p->used, (mmUInt32_t)v);
    }
    return v;
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_BufferRecycle(struct mmEmulatorAudio* p, ALuint v)
{
    float idle_sz = 0;
    float memb_sz = 0;
    int trim_number = 0;

    // rmv to used set.
    mmRbtsetU32_Rmv(&p->used, (mmUInt32_t)v);
    // add to idle set.
    mmRbtsetU32_Add(&p->idle, (mmUInt32_t)v);
    // 
    idle_sz = (float)p->idle.size;
    memb_sz = (float)p->pool.size;
    trim_number = (int)(memb_sz * p->rate_trim_number);

    if (idle_sz / memb_sz > p->rate_trim_border)
    {
        int i = 0;
        // 
        struct mmRbNode* n = NULL;
        struct mmRbtsetU32Iterator* it = NULL;
        // max
        n = mmRb_First(&p->idle.rbt);
        while (NULL != n && i < trim_number)
        {
            it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
            v = (ALuint)it->k;
            n = mmRb_Next(n);
            mmRbtsetU32_Erase(&p->idle, it);
            mmEmulatorAudio_BufferRmv(p, v);
            i++;
        }
    }
}

MM_EXPORT_EMULATOR void mmEmulatorAudio_DeviceList(struct mmEmulatorAudio* p)
{
    struct mmString device_name;

    const ALCchar* deviceList = alcGetString(NULL, ALC_ALL_DEVICES_SPECIFIER);
    
    if(NULL != deviceList)
    {
        /*
        ** The list returned by the call to alcGetString has the names of the
        ** devices seperated by NULL characters and the list is terminated by
        ** two NULL characters, so we can cast the list into a string and it
        ** will automatically stop at the first NULL that it sees, then we
        ** can move the pointer ahead by the lenght of that string + 1 and we
        ** will be at the begining of the next string.  Once we hit an empty
        ** string we know that we've found the double NULL that terminates the
        ** list and we can stop there.
        */

        mmRbtsetString_Clear(&p->devices);

        while (*deviceList != 0)
        {
            mmString_MakeWeaks(&device_name, deviceList);

            mmRbtsetString_Add(&p->devices, &device_name);

            deviceList += strlen(deviceList) + 1;
        }
    }
}

MM_EXPORT_EMULATOR void mmEmulatorAudio_FinishLaunching(struct mmEmulatorAudio* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    do
    {
        mmEmulatorAudio_DeviceList(p);

        if (0 == p->devices.size)
        {
            // IOS alcGetString ALC_ALL_DEVICES_SPECIFIER devices may be empty.
            //
            // https://stackoverflow.com/questions/24067201/getting-an-audio-device-with-openal
            // Apple's implementation of alcOpenDevice() only returns the device once.
            // Every subsequent call returns NULL. This function can be called by a lot of
            // Apple audio code, so take out EVERY TRACE of audio code before using OpenAL
            // and manually calling that function yourself.
            mmLogger_LogI(gLogger, "%s %d audio enumerate devices is empty.", __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d audio enumerate devices success size: %u.", __FUNCTION__, __LINE__, (unsigned int)p->devices.size);
        }

        if (mmString_Empty(&p->device_name) || 0 == mmRbtsetString_Get(&p->devices, &p->device_name))
        {
            // If the suggested device is in the list we use it, otherwise select the default device.
            p->device = alcOpenDevice((const ALCchar*)NULL);

            if (NULL == p->device)
            {
                mmLogger_LogE(gLogger, "%s %d audio device: default open failure.", __FUNCTION__, __LINE__);
                break;
            }
            else
            {
                mmLogger_LogI(gLogger, "%s %d audio device: default open success.", __FUNCTION__, __LINE__);
            }
        }
        else
        {
            const char* pDeviceName = mmString_CStr(&p->device_name);
            p->device = alcOpenDevice((const ALCchar*)pDeviceName);

            if (NULL == p->device)
            {
                mmLogger_LogE(gLogger, "%s %d audio device: %s open failure.", __FUNCTION__, __LINE__, pDeviceName);
                break;
            }
            else
            {
                mmLogger_LogI(gLogger, "%s %d audio device: %s open success.", __FUNCTION__, __LINE__, pDeviceName);
            }
        }

        p->context = alcCreateContext(p->device, NULL);
        if (NULL == p->context)
        {
            mmLogger_LogE(gLogger, "%s %d audio context create failure.", __FUNCTION__, __LINE__);
            break;
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d audio context create success.", __FUNCTION__, __LINE__);
        }

        // alcMakeContextCurrent is used when cross thread.
        // The code at ios may be AL_INVALID_OPERATION Illegal AL call.
        // It needs to be called once, otherwise there may be no sound.
        alcMakeContextCurrent(p->context);
        __static_mmEmulatorAudio_Check(MM_LOG_TRACE, alGetError());

        alGenSources(1, &p->source);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

        if (AL_NONE == p->source)
        {
            mmLogger_LogE(gLogger, "%s %d audio source create failure.", __FUNCTION__, __LINE__);
            break;
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d audio source create success.", __FUNCTION__, __LINE__);
        }

        alSourcei(p->source, AL_LOOPING, AL_FALSE);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
        
        alSourcef(p->source, AL_GAIN, p->volume);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    } while (0);
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_BeforeTerminate(struct mmEmulatorAudio* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmEmulatorAudio_BufferClear(p);
    mmLogger_LogI(gLogger, "%s %d audio buffer delete success.", __FUNCTION__, __LINE__);

    alDeleteSources(1, &p->source);
    __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    p->source = AL_NONE;
    mmLogger_LogI(gLogger, "%s %d audio source delete success.", __FUNCTION__, __LINE__);

    alcDestroyContext(p->context);
    __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    p->context = NULL;
    mmLogger_LogI(gLogger, "%s %d audio context delete success.", __FUNCTION__, __LINE__);

    alcCloseDevice(p->device);
    __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    p->device = NULL;
    mmLogger_LogI(gLogger, "%s %d audio device delete success.", __FUNCTION__, __LINE__);
}

MM_EXPORT_EMULATOR void mmEmulatorAudio_EnterBackground(struct mmEmulatorAudio* p)
{
    p->background = MM_TRUE;

    alcMakeContextCurrent(p->context);
    __static_mmEmulatorAudio_Check(MM_LOG_TRACE, alGetError());

    mmEmulatorAudio_SetSourceVolume(p, 0.0f);
    mmEmulatorAudio_Pause(p);
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_EnterForeground(struct mmEmulatorAudio* p)
{
    p->background = MM_FALSE;

    alcMakeContextCurrent(p->context);
    __static_mmEmulatorAudio_Check(MM_LOG_TRACE, alGetError());

    mmEmulatorAudio_Play(p);
    mmEmulatorAudio_SetSourceVolume(p, p->volume);
}

MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourcePosition(struct mmEmulatorAudio* p, float x, float y, float z)
{
    if (AL_NONE != p->source)
    {
        alSource3f(p->source, AL_POSITION, x, y, z);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceDirection(struct mmEmulatorAudio* p, float dirx, float diry, float dirz)
{
    if (AL_NONE != p->source)
    {
        alSource3f(p->source, AL_DIRECTION, dirx, diry, dirz);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceVelocity(struct mmEmulatorAudio* p, float velx, float vely, float velz)
{
    if (AL_NONE != p->source)
    {
        alSource3f(p->source, AL_VELOCITY, velx, vely, velz);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceVolume(struct mmEmulatorAudio* p, float gain)
{
    if (gain < 0) return;

    if (AL_NONE != p->source)
    {
        alSourcef(p->source, AL_GAIN, gain);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceMaxVolume(struct mmEmulatorAudio* p, float maxGain)
{
    if (maxGain < 0 || maxGain > 1) return;

    if (AL_NONE != p->source)
    {
        alSourcef(p->source, AL_MAX_GAIN, maxGain);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceMinVolume(struct mmEmulatorAudio* p, float minGain)
{
    if (minGain < 0 || minGain > 1) return;

    if (AL_NONE != p->source)
    {

        alSourcef(p->source, AL_MIN_GAIN, minGain);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceConeAngles(struct mmEmulatorAudio* p, float innerConeAngle, float outerConeAngle)
{
    if (innerConeAngle < 0 || innerConeAngle > 360) return;
    if (outerConeAngle < 0 || outerConeAngle > 360) return;

    if (AL_NONE != p->source)
    {
        alSourcef(p->source, AL_CONE_INNER_ANGLE, innerConeAngle);
        alSourcef(p->source, AL_CONE_OUTER_ANGLE, outerConeAngle);
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceOuterConeVolume(struct mmEmulatorAudio* p, float gain)
{
    if (gain < 0 || gain > 1)   return;

    if (AL_NONE != p->source)
    {
        alSourcef(p->source, AL_CONE_OUTER_GAIN, gain);
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceMaxDistance(struct mmEmulatorAudio* p, float maxDistance)
{
    if (maxDistance < 0) return;

    if (AL_NONE != p->source)
    {
        alSourcef(p->source, AL_MAX_DISTANCE, maxDistance);
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceRolloffFactor(struct mmEmulatorAudio* p, float rolloffFactor)
{
    if (rolloffFactor < 0) return;

    if (AL_NONE != p->source)
    {
        alSourcef(p->source, AL_ROLLOFF_FACTOR, rolloffFactor);
    }
}

MM_EXPORT_EMULATOR void mmEmulatorAudio_SetListenerPosition(struct mmEmulatorAudio* p, float x, float y, float z)
{
    alListener3f(AL_POSITION, x, y, z);
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetListenerVelocity(struct mmEmulatorAudio* p, float velx, float vely, float velz)
{
    alListener3f(AL_VELOCITY, velx, vely, velz);
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetListenerOrientation(struct mmEmulatorAudio* p, float x, float y, float z, float upx, float upy, float upz)
{
    p->ListenerOrientation[0] = x;
    p->ListenerOrientation[1] = y;
    p->ListenerOrientation[2] = z;
    p->ListenerOrientation[3] = upx;
    p->ListenerOrientation[4] = upy;
    p->ListenerOrientation[5] = upz;
    alListenerfv(AL_ORIENTATION, p->ListenerOrientation);
}

MM_EXPORT_EMULATOR void mmEmulatorAudio_Play(struct mmEmulatorAudio* p)
{
    if (AL_NONE != p->source)
    {
        alSourcePlay(p->source);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}
MM_EXPORT_EMULATOR void mmEmulatorAudio_Stop(struct mmEmulatorAudio* p)
{
    if (AL_NONE != p->source)
    {
        alSourceStop(p->source);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_EMULATOR void mmEmulatorAudio_Pause(struct mmEmulatorAudio* p)
{
    if (AL_NONE != p->source)
    {
        alSourcePause(p->source);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());
    }
}

MM_EXPORT_EMULATOR int mmEmulatorAudio_ProcessBuffer(struct mmEmulatorAudio* p, mmByte_t* buffer, size_t length)
{
    if (MM_FALSE == p->mute && AL_NONE != p->source && MM_FALSE == p->background)
    {
        int processed;
        int queued;
        int remaining = 0;

        ALuint al_buffer;
        ALint source_state_value = 0;

        alGetSourcei(p->source, AL_BUFFERS_PROCESSED, &processed);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

        alGetSourcei(p->source, AL_BUFFERS_QUEUED, &queued);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

        remaining = queued - processed;
        if (remaining > p->accumulation_limit)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogW(gLogger, "%s %d audio block number accumulation queued: %d processed: %d.", 
                __FUNCTION__, __LINE__, queued, processed);
        }

// #define DEBUG_AUDIO
#if defined(DEBUG_AUDIO)
        static int aidx = 0;
        if ((++aidx) >= 60)
        {
            int s = 0;
            int b = 0;

            alGetSourcei(p->source, AL_SAMPLE_OFFSET, &s);
            __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

            alGetSourcei(p->source, AL_BYTE_OFFSET, &b);
            __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

            printf("Audio queue length: %d remaining: %d queued: %d processed: %d s: %d b: %d\n",
                (int)length, remaining, queued, processed, s, b);

            aidx = 0;
        }
#endif//DEBUG_AUDIO

        while (processed--)
        {
            alSourceUnqueueBuffers(p->source, 1, &al_buffer);
            __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

            mmEmulatorAudio_BufferRecycle(p, al_buffer);
        }

        al_buffer = mmEmulatorAudio_BufferProduce(p);

        alBufferData(al_buffer, (ALenum)p->nFormat, (const ALvoid*)buffer, (ALsizei)length, (ALsizei)p->nRate);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

        alSourceQueueBuffers(p->source, 1, &al_buffer);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

        alGetSourcei(p->source, AL_SOURCE_STATE, &source_state_value);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

        // AL_INITIAL is init finish, but not play.
        // AL_STOPPED is playing but QueueBuffers empty.
        if (AL_STOPPED == source_state_value || AL_INITIAL == source_state_value)
        {
            mmEmulatorAudio_Play(p);
        }

        return remaining;
    }
    else
    {
        int processed;
        int queued;
        int remaining = 0;

        alGetSourcei(p->source, AL_BUFFERS_PROCESSED, &processed);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

        alGetSourcei(p->source, AL_BUFFERS_QUEUED, &queued);
        __static_mmEmulatorAudio_Check(MM_LOG_ERROR, alGetError());

        remaining = queued - processed;
        return remaining;
    }
}

static void __static_mmEmulatorAudio_ErrorCheck(int lvl, ALenum code, const char* func, int line)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    switch (code)
    {
    case AL_INVALID_NAME:
        mmLogger_Message(gLogger, lvl, "%s %d Invalid name paramater passed to AL call.", func, line);
        break;
    case AL_INVALID_ENUM:
        mmLogger_Message(gLogger, lvl, "%s %d Invalid enum parameter passed to AL call.", func, line);
        break;
    case AL_INVALID_VALUE:
        mmLogger_Message(gLogger, lvl, "%s %d Invalid value parameter passed to AL call.", func, line);
        break;
    case AL_INVALID_OPERATION:
        mmLogger_Message(gLogger, lvl, "%s %d Illegal AL call.", func, line);
        break;
    case AL_OUT_OF_MEMORY:
        mmLogger_Message(gLogger, lvl, "%s %d Not enough memory.", func, line);
        break;
    default:
        break;
    }
}
