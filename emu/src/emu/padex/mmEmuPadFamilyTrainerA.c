/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadFamilyTrainerA.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuPad.h"

MM_EXPORT_EMU void mmEmuPadFamilyTrainerA_Init(struct mmEmuPadFamilyTrainerA* p)
{
    mmEmuExpad_Init(&p->super);

    p->outbits = 0;
    p->padbits = 0;

    //
    p->super.Reset = &mmEmuPadFamilyTrainerA_Reset;
    //p->super.Strobe = &mmEmuPadFamilyTrainerA_Strobe;
    //p->super.Read4016 = &mmEmuPadFamilyTrainerA_Read4016;
    p->super.Read4017 = &mmEmuPadFamilyTrainerA_Read4017;
    p->super.Write4016 = &mmEmuPadFamilyTrainerA_Write4016;
    //p->super.Write4017 = &mmEmuPadFamilyTrainerA_Write4017;
    p->super.Sync = &mmEmuPadFamilyTrainerA_Sync;
    p->super.SetSyncData = &mmEmuPadFamilyTrainerA_SetSyncData;
    p->super.GetSyncData = &mmEmuPadFamilyTrainerA_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadFamilyTrainerA_Destroy(struct mmEmuPadFamilyTrainerA* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->outbits = 0;
    p->padbits = 0;
}

MM_EXPORT_EMU void mmEmuPadFamilyTrainerA_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadFamilyTrainerA* p = (struct mmEmuPadFamilyTrainerA*)(super);

    p->outbits = 0;
    p->padbits = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadFamilyTrainerA_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadFamilyTrainerA* p = (struct mmEmuPadFamilyTrainerA*)(super);

    return p->outbits;
}
MM_EXPORT_EMU void mmEmuPadFamilyTrainerA_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadFamilyTrainerA* p = (struct mmEmuPadFamilyTrainerA*)(super);

    p->outbits = 0;
    if (!(data & 0x04))
    {
        p->outbits = (mmByte_t)((~p->padbits << 1) & 0x1E);
    }
    if (!(data & 0x02))
    {
        p->outbits = (mmByte_t)((~p->padbits >> 3) & 0x1E);
    }
    if (!(data & 0x01))
    {
        p->outbits = (mmByte_t)((~p->padbits >> 7) & 0x1E);
    }
}

MM_EXPORT_EMU void mmEmuPadFamilyTrainerA_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadFamilyTrainerA* p = (struct mmEmuPadFamilyTrainerA*)(super);

    // struct mmEmuPad* pad = &p->super.nes->pad;
    struct mmEmuController* controller = &p->super.nes->controller;

    p->padbits = 0;

    if (mmEmuController_ExButtonCheck(controller, 1, 3)) p->padbits |= 1 << 3;
    if (mmEmuController_ExButtonCheck(controller, 1, 2)) p->padbits |= 1 << 2;
    if (mmEmuController_ExButtonCheck(controller, 1, 1)) p->padbits |= 1 << 1;
    if (mmEmuController_ExButtonCheck(controller, 1, 0)) p->padbits |= 1 << 0;
    if (mmEmuController_ExButtonCheck(controller, 1, 7)) p->padbits |= 1 << 7;
    if (mmEmuController_ExButtonCheck(controller, 1, 6)) p->padbits |= 1 << 6;
    if (mmEmuController_ExButtonCheck(controller, 1, 5)) p->padbits |= 1 << 5;
    if (mmEmuController_ExButtonCheck(controller, 1, 4)) p->padbits |= 1 << 4;
    if (mmEmuController_ExButtonCheck(controller, 1, 11)) p->padbits |= 1 << 11;
    if (mmEmuController_ExButtonCheck(controller, 1, 10)) p->padbits |= 1 << 10;
    if (mmEmuController_ExButtonCheck(controller, 1, 9)) p->padbits |= 1 << 9;
    if (mmEmuController_ExButtonCheck(controller, 1, 8)) p->padbits |= 1 << 8;
}
MM_EXPORT_EMU void mmEmuPadFamilyTrainerA_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadFamilyTrainerA* p = (struct mmEmuPadFamilyTrainerA*)(super);

    p->padbits = (mmUInt32_t)data;
}
MM_EXPORT_EMU mmLong_t mmEmuPadFamilyTrainerA_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadFamilyTrainerA* p = (struct mmEmuPadFamilyTrainerA*)(super);

    return (mmLong_t)p->padbits;
}

static struct mmEmuExpad* __static_mmEmuPadFamilyTrainerA_Produce(void)
{
    struct mmEmuPadFamilyTrainerA* p = (struct mmEmuPadFamilyTrainerA*)mmMalloc(sizeof(struct mmEmuPadFamilyTrainerA));
    mmEmuPadFamilyTrainerA_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadFamilyTrainerA_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadFamilyTrainerA* p = (struct mmEmuPadFamilyTrainerA*)(m);
    mmEmuPadFamilyTrainerA_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_FAMLYTRAINERA =
{
    &__static_mmEmuPadFamilyTrainerA_Produce,
    &__static_mmEmuPadFamilyTrainerA_Recycle,
};
