/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApuMmc5_h__
#define __mmEmuApuMmc5_h__

#include "core/mmCore.h"

#include "emu/mmEmuApuInterface.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuApuMmc5Rectangle
{
    mmByte_t    reg[4];
    mmByte_t    enable;

    mmInt_t     vbl_length;

    mmInt_t     phaseacc;
    mmInt_t     freq;

    mmInt_t     output_vol;
    mmByte_t    fixed_envelope;
    mmByte_t    holdnote;
    mmByte_t    volume;

    mmByte_t    env_vol;
    mmInt_t     env_phase;
    mmInt_t     env_decay;

    mmInt_t     adder;
    mmInt_t     duty_flip;
};

struct mmEmuApuMmc5SyncRectangle
{
    // For sync
    mmByte_t    reg[4];
    mmByte_t    enable;
    mmByte_t    holdnote;
    mmByte_t    dummy[2];
    mmInt_t     vbl_length;
};

// Tables
MM_EXPORT_EMU extern const mmInt_t MM_EMU_APU_MMC5_VBL_LENGTH[32];
MM_EXPORT_EMU extern const mmInt_t MM_EMU_APU_MMC5_DUTY_LUT[4];

struct mmEmuApuMmc5
{
    struct mmEmuApuInterface super;

    struct mmEmuApuMmc5Rectangle        ch0, ch1;
    struct mmEmuApuMmc5SyncRectangle    sch0, sch1;

    mmByte_t    reg5010;
    mmByte_t    reg5011;
    mmByte_t    reg5015;
    mmInt_t     cycle_rate;

    mmInt_t     FrameCycle;
    mmByte_t    sync_reg5015;

    double      cpu_clock;

    // Tables
    mmInt_t     decay_lut[16];
    mmInt_t     vbl_lut[32];
};

MM_EXPORT_EMU void mmEmuApuMmc5_Init(struct mmEmuApuMmc5* p);
MM_EXPORT_EMU void mmEmuApuMmc5_Destroy(struct mmEmuApuMmc5* p);

MM_EXPORT_EMU mmInt_t mmEmuApuMmc5_RectangleRender(struct mmEmuApuMmc5* p, struct mmEmuApuMmc5Rectangle* ch);

// virtual function for super.
MM_EXPORT_EMU void mmEmuApuMmc5_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuMmc5_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuMmc5_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmInt_t mmEmuApuMmc5_Process(struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU void mmEmuApuMmc5_SyncWrite(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuApuMmc5_SyncRead(struct mmEmuApuInterface* super, mmWord_t addr);
MM_EXPORT_EMU mmBool_t mmEmuApuMmc5_Sync(struct mmEmuApuInterface* super, mmInt_t cycles);

MM_EXPORT_EMU mmInt_t mmEmuApuMmc5_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuMmc5_GetStateSize(const struct mmEmuApuInterface* super);
MM_EXPORT_EMU void mmEmuApuMmc5_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuApuMmc5_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer);

#include "core/mmSuffix.h"

#endif//__mmEmuApuMmc5_h__
