/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper101.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper101_Init(struct mmEmuMapper101* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper101_Reset;
    p->super.Write = &mmEmuMapper101_Write;
    //p->super.Read = &mmEmuMapper101_Read;
    //p->super.ReadLow = &mmEmuMapper101_ReadLow;
    p->super.WriteLow = &mmEmuMapper101_WriteLow;
    //p->super.ExRead = &mmEmuMapper101_ExRead;
    //p->super.ExWrite = &mmEmuMapper101_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper101_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper101_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper101_HSync;
    //p->super.VSync = &mmEmuMapper101_VSync;
    //p->super.Clock = &mmEmuMapper101_Clock;
    //p->super.PPULatch = &mmEmuMapper101_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper101_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper101_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper101_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper101_IsStateSave;
    //p->super.SaveState = &mmEmuMapper101_SaveState;
    //p->super.LoadState = &mmEmuMapper101_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper101_Destroy(struct mmEmuMapper101* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper101_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper101* p = (struct mmEmuMapper101*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper101_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper101* p = (struct mmEmuMapper101*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x6000)
    {
        mmEmuMmu_SetVROM8KBank(mmu, data & 0x03);
    }
}
MM_EXPORT_EMU void mmEmuMapper101_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper101* p = (struct mmEmuMapper101*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetVROM8KBank(mmu, data & 0x03);
}

static struct mmEmuMapper* __static_mmEmuMapper101_Produce(void)
{
    struct mmEmuMapper101* p = (struct mmEmuMapper101*)mmMalloc(sizeof(struct mmEmuMapper101));
    mmEmuMapper101_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper101_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper101* p = (struct mmEmuMapper101*)(m);
    mmEmuMapper101_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER101 =
{
    &__static_mmEmuMapper101_Produce,
    &__static_mmEmuMapper101_Recycle,
};
