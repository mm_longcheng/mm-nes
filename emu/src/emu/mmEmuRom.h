/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuRom_h__
#define __mmEmuRom_h__

#include "core/mmCore.h"
#include "core/mmByte.h"
#include "core/mmStreambuf.h"

#include "mmEmuRomNesHeader.h"
#include "mmEmuRomNsfHeader.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

enum mmEmuRomFormat_t
{
    MM_EMU_ROM_FORMAT_NONE = 0,
    MM_EMU_ROM_FORMAT_NESA = 1,
    MM_EMU_ROM_FORMAT_FDSA = 2,
    MM_EMU_ROM_FORMAT_NSFM = 3,
};

// ROM control byte #1
enum
{
    MM_EMU_ROM_VMIRROR = 0x01, MM_EMU_ROM_BATTERY = 0x02,
    MM_EMU_ROM_TRAINER = 0x04, MM_EMU_ROM_4SCREEN = 0x08,
};

// ROM control byte #2
enum { MM_EMU_ROM_VSUNISYSTEM = 0x01, };

/*
A. iNES Format (.NES)
---------------------
+--------+------+------------------------------------------+
| Offset | Size | Content(s)                               |
+--------+------+------------------------------------------+
|   0    |  3   | 'NES'                                    |
|   3    |  1   | $1A                                      |
|   4    |  1   | 16K PRG-ROM page count                   |
|   5    |  1   |  8K CHR-ROM page count                   |
|   6    |  1   | ROM Control Byte #1                      |
|        |      |   %####vTsM                              |
|        |      |    |  ||||+- 0=Horizontal mirroring      |
|        |      |    |  ||||   1=Vertical mirroring        |
|        |      |    |  |||+-- 1=SRAM enabled              |
|        |      |    |  ||+--- 1=512-byte trainer present  |
|        |      |    |  |+---- 1=Four-screen mirroring     |
|        |      |    |  |                                  |
|        |      |    +--+----- Mapper # (lower 4-bits)     |
|   7    |  1   | ROM Control Byte #2                      |
|        |      |   %####00PV                              |
|        |      |    |  |  |+- 1=VS-Unisystem Images       |
|        |      |    |  |  +-- 1=PlayChoice10 Images       |
|        |      |    |  |                                  |
|        |      |    +--+----- Mapper # (upper 4-bits)     |
|  8-15  |  8   | $00                                      |
| 16-..  |      | Actual 16K PRG-ROM pages (in linear      |
|  ...   |      | order). If a trainer exists, it precedes |
|  ...   |      | the first PRG-ROM page.                  |
| ..-EOF |      | CHR-ROM pages (in ascending order).      |
+--------+------+------------------------------------------+
*/

struct mmEmuNes;

struct mmEmuRom
{
    struct mmEmuNes* nes;

    struct mmEmuRomNesHeader    nesheader;
    struct mmEmuRomNsfHeader    nsfheader;

    struct mmStreambuf buffer_PRG;
    struct mmStreambuf buffer_CHR;
    struct mmStreambuf buffer_Trainer;
    struct mmStreambuf buffer_DiskBios;
    struct mmStreambuf buffer_Disk;

    // PROM CRC
    mmUInt32_t crcprom;
    mmUInt32_t crcarom;
    mmUInt32_t crcvrom;

    mmUInt32_t fdsmakerID;
    mmUInt32_t fdsgameID;

    mmInt_t mapper;
    mmInt_t diskno;

    // For NSF
    mmBool_t bNSF;
    mmInt_t NSF_PAGE_SIZE;
};

MM_EXPORT_EMU void mmEmuRom_Init(struct mmEmuRom* p);
MM_EXPORT_EMU void mmEmuRom_Destroy(struct mmEmuRom* p);

MM_EXPORT_EMU void mmEmuRom_SetParent(struct mmEmuRom* p, struct mmEmuNes* parent);

// rom rom.nes
// ips rom.ips
// sys DISKSYS.ROM
//
// return MM_SUCCESS is success.
MM_EXPORT_EMU int mmEmuRom_LoadBuffer(struct mmEmuRom* p);

// Get NESHEADER
static mmInline const struct mmEmuRomNesHeader* mmEmuRom_GetNesHeader(const struct mmEmuRom* p) { return &p->nesheader; }
// Get NSFHEADER
static mmInline const struct mmEmuRomNsfHeader* mmEmuRom_GetNsfHeader(const struct mmEmuRom* p) { return &p->nsfheader; }

// Get ROM buffer pointer
static mmInline mmByte_t* mmEmuRom_GetPROM(const struct mmEmuRom* p) { return p->buffer_PRG.buff + p->buffer_PRG.gptr; }
static mmInline mmByte_t* mmEmuRom_GetVROM(const struct mmEmuRom* p) { return p->buffer_CHR.buff + p->buffer_CHR.gptr; }
static mmInline mmByte_t* mmEmuRom_GetTRAINER(const struct mmEmuRom* p) { return p->buffer_Trainer.buff + p->buffer_Trainer.gptr; }
static mmInline mmByte_t* mmEmuRom_GetDISKBIOS(const struct mmEmuRom* p) { return p->buffer_DiskBios.buff + p->buffer_DiskBios.gptr; }
static mmInline mmByte_t* mmEmuRom_GetDISK(const struct mmEmuRom* p) { return p->buffer_Disk.buff + p->buffer_Disk.gptr; }

// Get ROM size
static mmInline mmUInt32_t mmEmuRom_GetPROMSIZE(const struct mmEmuRom* p) { return p->nesheader.PRG_PAGE_SIZE; }
static mmInline mmUInt32_t mmEmuRom_GetVROMSIZE(const struct mmEmuRom* p) { return p->nesheader.CHR_PAGE_SIZE; }

// Get PROM
static mmInline mmUInt32_t mmEmuRom_GetPROMCRC(const struct mmEmuRom* p) { return p->crcprom; }
static mmInline mmUInt32_t mmEmuRom_GetAROMCRC(const struct mmEmuRom* p) { return p->crcarom; }
static mmInline mmUInt32_t mmEmuRom_GetVROMCRC(const struct mmEmuRom* p) { return p->crcvrom; }

// Get FDS ID
static mmInline mmUInt32_t mmEmuRom_GetMakerID(const struct mmEmuRom* p) { return p->fdsmakerID; }
static mmInline mmUInt32_t mmEmuRom_GetGameID(const struct mmEmuRom* p) { return p->fdsgameID; }

// ROM control
static mmInline mmBool_t mmEmuRom_IsVMIRROR(const struct mmEmuRom* p) { return p->nesheader.control_06 & MM_EMU_ROM_VMIRROR; }
static mmInline mmBool_t mmEmuRom_Is4SCREEN(const struct mmEmuRom* p) { return p->nesheader.control_06 & MM_EMU_ROM_4SCREEN; }
static mmInline mmBool_t mmEmuRom_IsBATTERY(const struct mmEmuRom* p) { return p->nesheader.control_06 & MM_EMU_ROM_BATTERY; }
static mmInline mmBool_t mmEmuRom_IsTRAINER(const struct mmEmuRom* p) { return p->nesheader.control_06 & MM_EMU_ROM_TRAINER; }
static mmInline mmBool_t mmEmuRom_IsVSUNISYSTEM(const struct mmEmuRom* p) { return p->nesheader.control_07 & MM_EMU_ROM_VSUNISYSTEM; }

// Mapper
static mmInline mmInt_t mmEmuRom_GetMapperNo(const struct mmEmuRom* p) { return p->mapper; }

// Disks
static mmInline mmInt_t mmEmuRom_GetDiskNo(const struct mmEmuRom* p) { return p->diskno; }

// VideoMode
MM_EXPORT_EMU mmInt_t mmEmuRom_GetVideoMode(const struct mmEmuRom* p);

// NSF
static mmInline mmBool_t mmEmuRom_IsNSF(const struct mmEmuRom* p) { return p->bNSF; }
static mmInline mmByte_t mmEmuRom_GetNSFSIZE(const struct mmEmuRom* p) { return p->NSF_PAGE_SIZE; }

// File check
// 0: success 
// 1: failure 
//-1: unknown 
MM_EXPORT_EMU mmInt_t mmEmuRom_IsRomBuffer(const char* buffer, size_t offset, size_t length);
MM_EXPORT_EMU mmUInt16_t mmEmuRom_GetRomFormatType(const mmUInt8_t* buffer, size_t offset, size_t length);

#include "core/mmSuffix.h"

#endif//__mmEmuRom_h__
