/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper088.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper088_Init(struct mmEmuMapper088* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0;
    p->patch = 0;

    //
    p->super.Reset = &mmEmuMapper088_Reset;
    p->super.Write = &mmEmuMapper088_Write;
    //p->super.Read = &mmEmuMapper088_Read;
    //p->super.ReadLow = &mmEmuMapper088_ReadLow;
    //p->super.WriteLow = &mmEmuMapper088_WriteLow;
    //p->super.ExRead = &mmEmuMapper088_ExRead;
    //p->super.ExWrite = &mmEmuMapper088_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper088_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper088_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper088_HSync;
    //p->super.VSync = &mmEmuMapper088_VSync;
    //p->super.Clock = &mmEmuMapper088_Clock;
    //p->super.PPULatch = &mmEmuMapper088_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper088_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper088_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper088_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper088_IsStateSave;
    p->super.SaveState = &mmEmuMapper088_SaveState;
    p->super.LoadState = &mmEmuMapper088_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper088_Destroy(struct mmEmuMapper088* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0;
    p->patch = 0;
}

MM_EXPORT_EMU void mmEmuMapper088_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper088* p = (struct mmEmuMapper088*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE >= 8)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xc1b6b2a6)          // Devil Man(J)
    {
        p->patch = 1;
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
    if (crc == 0xd9803a35)          // Quinty(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
}
MM_EXPORT_EMU void mmEmuMapper088_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper088* p = (struct mmEmuMapper088*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        p->reg = data;
        if (p->patch)
        {
            if (data & 0x40)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
            }
        }
        break;
    case 0x8001:
        switch (p->reg & 0x07)
        {
        case 0:
            mmEmuMmu_SetVROM2KBank(mmu, 0, data >> 1);
            break;
        case 1:
            mmEmuMmu_SetVROM2KBank(mmu, 2, data >> 1);
            break;
        case 2:
            mmEmuMmu_SetVROM1KBank(mmu, 4, data + 0x40);
            break;
        case 3:
            mmEmuMmu_SetVROM1KBank(mmu, 5, data + 0x40);
            break;
        case 4:
            mmEmuMmu_SetVROM1KBank(mmu, 6, data + 0x40);
            break;
        case 5:
            mmEmuMmu_SetVROM1KBank(mmu, 7, data + 0x40);
            break;
        case 6:
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
            break;
        case 7:
            mmEmuMmu_SetPROM8KBank(mmu, 5, data);
            break;
        default:
            break;
        }
        break;
    case 0xC000:
        if (data)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper088_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper088_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper088* p = (struct mmEmuMapper088*)(super);

    buffer[0] = p->reg;
}
MM_EXPORT_EMU void mmEmuMapper088_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper088* p = (struct mmEmuMapper088*)(super);

    p->reg = buffer[0];
}

static struct mmEmuMapper* __static_mmEmuMapper088_Produce(void)
{
    struct mmEmuMapper088* p = (struct mmEmuMapper088*)mmMalloc(sizeof(struct mmEmuMapper088));
    mmEmuMapper088_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper088_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper088* p = (struct mmEmuMapper088*)(m);
    mmEmuMapper088_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER088 =
{
    &__static_mmEmuMapper088_Produce,
    &__static_mmEmuMapper088_Recycle,
};
