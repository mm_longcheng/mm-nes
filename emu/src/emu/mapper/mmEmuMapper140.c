/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper140.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper140_Init(struct mmEmuMapper140* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper140_Reset;
    //p->super.Write = &mmEmuMapper140_Write;
    //p->super.Read = &mmEmuMapper140_Read;
    //p->super.ReadLow = &mmEmuMapper140_ReadLow;
    p->super.WriteLow = &mmEmuMapper140_WriteLow;
    //p->super.ExRead = &mmEmuMapper140_ExRead;
    //p->super.ExWrite = &mmEmuMapper140_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper140_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper140_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper140_HSync;
    //p->super.VSync = &mmEmuMapper140_VSync;
    //p->super.Clock = &mmEmuMapper140_Clock;
    //p->super.PPULatch = &mmEmuMapper140_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper140_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper140_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper140_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper140_IsStateSave;
    //p->super.SaveState = &mmEmuMapper140_SaveState;
    //p->super.LoadState = &mmEmuMapper140_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper140_Destroy(struct mmEmuMapper140* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper140_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper140* p = (struct mmEmuMapper140*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper140_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper140* p = (struct mmEmuMapper140*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, (data & 0xF0) >> 4);
    mmEmuMmu_SetVROM8KBank(mmu, data & 0x0F);
}

static struct mmEmuMapper* __static_mmEmuMapper140_Produce(void)
{
    struct mmEmuMapper140* p = (struct mmEmuMapper140*)mmMalloc(sizeof(struct mmEmuMapper140));
    mmEmuMapper140_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper140_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper140* p = (struct mmEmuMapper140*)(m);
    mmEmuMapper140_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER140 =
{
    &__static_mmEmuMapper140_Produce,
    &__static_mmEmuMapper140_Recycle,
};
