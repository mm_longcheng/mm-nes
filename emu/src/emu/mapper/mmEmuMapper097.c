/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper097.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper097_Init(struct mmEmuMapper097* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper097_Reset;
    p->super.Write = &mmEmuMapper097_Write;
    //p->super.Read = &mmEmuMapper097_Read;
    //p->super.ReadLow = &mmEmuMapper097_ReadLow;
    //p->super.WriteLow = &mmEmuMapper097_WriteLow;
    //p->super.ExRead = &mmEmuMapper097_ExRead;
    //p->super.ExWrite = &mmEmuMapper097_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper097_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper097_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper097_HSync;
    //p->super.VSync = &mmEmuMapper097_VSync;
    //p->super.Clock = &mmEmuMapper097_Clock;
    //p->super.PPULatch = &mmEmuMapper097_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper097_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper097_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper097_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper097_IsStateSave;
    //p->super.SaveState = &mmEmuMapper097_SaveState;
    //p->super.LoadState = &mmEmuMapper097_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper097_Destroy(struct mmEmuMapper097* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper097_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper097* p = (struct mmEmuMapper097*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1, 0, 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper097_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper097* p = (struct mmEmuMapper097*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr < 0xC000)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 6, data & 0x0F);

        if (data & 0x80)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper097_Produce(void)
{
    struct mmEmuMapper097* p = (struct mmEmuMapper097*)mmMalloc(sizeof(struct mmEmuMapper097));
    mmEmuMapper097_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper097_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper097* p = (struct mmEmuMapper097*)(m);
    mmEmuMapper097_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER097 =
{
    &__static_mmEmuMapper097_Produce,
    &__static_mmEmuMapper097_Recycle,
};
