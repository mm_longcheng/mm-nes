/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper254_h__
#define __mmEmuMapper254_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper254  Pokemon Pirate Cart                                       //
//----------------------------------------------------------------------//
struct mmEmuMapper254
{
    struct mmEmuMapper super;

    mmByte_t    reg[8];
    mmByte_t    prg0, prg1;
    mmByte_t    chr01, chr23, chr4, chr5, chr6, chr7;

    mmByte_t    irq_type;
    mmByte_t    irq_enable;
    mmByte_t    irq_counter;
    mmByte_t    irq_latch;
    mmByte_t    irq_request;
    mmByte_t    protectflag;
};

MM_EXPORT_EMU void mmEmuMapper254_Init(struct mmEmuMapper254* p);
MM_EXPORT_EMU void mmEmuMapper254_Destroy(struct mmEmuMapper254* p);

MM_EXPORT_EMU void mmEmuMapper254_SetBankCPU(struct mmEmuMapper254* p);
MM_EXPORT_EMU void mmEmuMapper254_SetBankPPU(struct mmEmuMapper254* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper254_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper254_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuMapper254_ReadLow(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper254_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper254_Clock(struct mmEmuMapper* super, mmInt_t cycles);
MM_EXPORT_EMU void mmEmuMapper254_HSync(struct mmEmuMapper* super, mmInt_t scanline);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper254_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper254_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper254_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER254;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper254_h__
