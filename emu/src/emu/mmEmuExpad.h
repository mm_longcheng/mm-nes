/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuExpad_h__
#define __mmEmuExpad_h__

#include "core/mmCore.h"
#include "core/mmTypes.h"

#include "container/mmRbtreeU32.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuNes;

struct mmEmuExpad
{
    struct mmEmuNes* nes;

    // Member function.
    void(*Reset)(struct mmEmuExpad* p);
    void(*Strobe)(struct mmEmuExpad* p);
    mmByte_t(*Read4016)(struct mmEmuExpad* p);
    mmByte_t(*Read4017)(struct mmEmuExpad* p);
    void(*Write4016)(struct mmEmuExpad* p, mmByte_t data);
    void(*Write4017)(struct mmEmuExpad* p, mmByte_t data);

    // For synchronized.
    void(*Sync)(struct mmEmuExpad* p);

    void(*SetSyncData)(struct mmEmuExpad* p, mmInt_t type, mmLong_t data);
    mmLong_t(*GetSyncData)(const struct mmEmuExpad* p, mmInt_t type);
};

MM_EXPORT_EMU void mmEmuExpad_Init(struct mmEmuExpad* p);
MM_EXPORT_EMU void mmEmuExpad_Destroy(struct mmEmuExpad* p);

MM_EXPORT_EMU void mmEmuExpad_SetParent(struct mmEmuExpad* p, struct mmEmuNes* parent);

// virtual function for super.
MM_EXPORT_EMU void mmEmuExpad_Reset(struct mmEmuExpad* p);
MM_EXPORT_EMU void mmEmuExpad_Strobe(struct mmEmuExpad* p);
MM_EXPORT_EMU mmByte_t mmEmuExpad_Read4016(struct mmEmuExpad* p);
MM_EXPORT_EMU mmByte_t mmEmuExpad_Read4017(struct mmEmuExpad* p);
MM_EXPORT_EMU void mmEmuExpad_Write4016(struct mmEmuExpad* p, mmByte_t data);
MM_EXPORT_EMU void mmEmuExpad_Write4017(struct mmEmuExpad* p, mmByte_t data);
MM_EXPORT_EMU void mmEmuExpad_Sync(struct mmEmuExpad* p);
MM_EXPORT_EMU void mmEmuExpad_SetSyncData(struct mmEmuExpad* p, mmInt_t type, mmLong_t data);
MM_EXPORT_EMU mmLong_t mmEmuExpad_GetSyncData(const struct mmEmuExpad* p, mmInt_t type);

struct mmEmuExpadCreator
{
    struct mmEmuExpad*(*Produce)(void);
    void(*Recycle)(struct mmEmuExpad* m);
};

struct mmEmuExpadFactory
{
    struct mmRbtreeU32Vpt rbtree;
};

MM_EXPORT_EMU void mmEmuExpadFactory_Init(struct mmEmuExpadFactory* p);
MM_EXPORT_EMU void mmEmuExpadFactory_Destroy(struct mmEmuExpadFactory* p);

MM_EXPORT_EMU void mmEmuExpadFactory_Register(struct mmEmuExpadFactory* p, mmUInt32_t hId, const struct mmEmuExpadCreator* creator);
MM_EXPORT_EMU void mmEmuExpadFactory_Unregister(struct mmEmuExpadFactory* p, mmUInt32_t hId);

MM_EXPORT_EMU void mmEmuExpadFactory_Clear(struct mmEmuExpadFactory* p);

MM_EXPORT_EMU struct mmEmuExpad* mmEmuExpadFactory_Produce(struct mmEmuExpadFactory* p, mmUInt32_t hId);
MM_EXPORT_EMU void mmEmuExpadFactory_Recycle(struct mmEmuExpadFactory* p, mmUInt32_t hId, struct mmEmuExpad* m);

#include "core/mmSuffix.h"

#endif//__mmEmuExpad_h__
