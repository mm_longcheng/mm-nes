/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper099_h__
#define __mmEmuMapper099_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper099  VS-Unisystem                                              //
//----------------------------------------------------------------------//
struct mmEmuMapper099
{
    struct mmEmuMapper super;

    mmByte_t    coin;
};

MM_EXPORT_EMU void mmEmuMapper099_Init(struct mmEmuMapper099* p);
MM_EXPORT_EMU void mmEmuMapper099_Destroy(struct mmEmuMapper099* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper099_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU mmByte_t mmEmuMapper099_ExRead(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper099_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER099;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper099_h__
