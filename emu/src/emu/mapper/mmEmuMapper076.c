/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper076.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper076_Init(struct mmEmuMapper076* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0;

    //
    p->super.Reset = &mmEmuMapper076_Reset;
    p->super.Write = &mmEmuMapper076_Write;
    //p->super.Read = &mmEmuMapper076_Read;
    //p->super.ReadLow = &mmEmuMapper076_ReadLow;
    //p->super.WriteLow = &mmEmuMapper076_WriteLow;
    //p->super.ExRead = &mmEmuMapper076_ExRead;
    //p->super.ExWrite = &mmEmuMapper076_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper076_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper076_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper076_HSync;
    //p->super.VSync = &mmEmuMapper076_VSync;
    //p->super.Clock = &mmEmuMapper076_Clock;
    //p->super.PPULatch = &mmEmuMapper076_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper076_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper076_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper076_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper076_IsStateSave;
    p->super.SaveState = &mmEmuMapper076_SaveState;
    p->super.LoadState = &mmEmuMapper076_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper076_Destroy(struct mmEmuMapper076* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0;
}

MM_EXPORT_EMU void mmEmuMapper076_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper076* p = (struct mmEmuMapper076*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE >= 8)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper076_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper076* p = (struct mmEmuMapper076*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        p->reg = data;
        break;
    case 0x8001:
        switch (p->reg & 0x07)
        {
        case 2:
            mmEmuMmu_SetVROM2KBank(mmu, 0, data);
            break;
        case 3:
            mmEmuMmu_SetVROM2KBank(mmu, 2, data);
            break;
        case 4:
            mmEmuMmu_SetVROM2KBank(mmu, 4, data);
            break;
        case 5:
            mmEmuMmu_SetVROM2KBank(mmu, 6, data);
            break;
        case 6:
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
            break;
        case 7:
            mmEmuMmu_SetPROM8KBank(mmu, 5, data);
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper076_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper076_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper076* p = (struct mmEmuMapper076*)(super);

    buffer[0] = p->reg;
}
MM_EXPORT_EMU void mmEmuMapper076_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper076* p = (struct mmEmuMapper076*)(super);

    p->reg = buffer[0];
}

static struct mmEmuMapper* __static_mmEmuMapper076_Produce(void)
{
    struct mmEmuMapper076* p = (struct mmEmuMapper076*)mmMalloc(sizeof(struct mmEmuMapper076));
    mmEmuMapper076_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper076_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper076* p = (struct mmEmuMapper076*)(m);
    mmEmuMapper076_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER076 =
{
    &__static_mmEmuMapper076_Produce,
    &__static_mmEmuMapper076_Recycle,
};
