/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadOekakidsTablet.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Init(struct mmEmuPadOekakidsTablet* p)
{
    mmEmuExpad_Init(&p->super);

    p->olddata = 0;
    p->outbits = 0;
    p->databits = 0;

    p->zapper_x = 0;
    p->zapper_y = 0;
    p->zapper_button = 0;

    //
    p->super.Reset = &mmEmuPadOekakidsTablet_Reset;
    //p->super.Strobe = &mmEmuPadOekakidsTablet_Strobe;
    //p->super.Read4016 = &mmEmuPadOekakidsTablet_Read4016;
    p->super.Read4017 = &mmEmuPadOekakidsTablet_Read4017;
    p->super.Write4016 = &mmEmuPadOekakidsTablet_Write4016;
    //p->super.Write4017 = &mmEmuPadOekakidsTablet_Write4017;
    p->super.Sync = &mmEmuPadOekakidsTablet_Sync;
    p->super.SetSyncData = &mmEmuPadOekakidsTablet_SetSyncData;
    p->super.GetSyncData = &mmEmuPadOekakidsTablet_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Destroy(struct mmEmuPadOekakidsTablet* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->olddata = 0;
    p->outbits = 0;
    p->databits = 0;

    p->zapper_x = 0;
    p->zapper_y = 0;
    p->zapper_button = 0;
}

MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadOekakidsTablet* p = (struct mmEmuPadOekakidsTablet*)(super);

    p->olddata = 0;
    p->outbits = 0;
    p->databits = 0;

    p->zapper_x = 0;
    p->zapper_y = 0;
    p->zapper_button = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadOekakidsTablet_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadOekakidsTablet* p = (struct mmEmuPadOekakidsTablet*)(super);

    return p->outbits;
}
MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadOekakidsTablet* p = (struct mmEmuPadOekakidsTablet*)(super);

    if (p->zapper_y < 48)
    {
        mmEmuScreen_SetZapperDrawMode(&p->super.nes->screen, MM_TRUE);
    }
    else
    {
        mmEmuScreen_SetZapperDrawMode(&p->super.nes->screen, MM_FALSE);
    }

    if (!(data & 0x01))
    {
        mmLong_t x, y;

        p->outbits = 0;
        p->databits = 0;

        if (p->zapper_button)
        {
            p->databits |= 0x0001;
        }

        if (p->zapper_y >= 48)
        {
            p->databits |= 0x0002;
        }
        else if (p->zapper_button)
        {
            p->databits |= 0x0003;
        }

        if (p->zapper_x < 0)
        {
            x = 0;
        }
        else
        {
            x = ((240 * p->zapper_x) / 256) + 8;
        }
        if (p->zapper_y < 0)
        {
            y = 0;
        }
        else
        {
            y = ((256 * p->zapper_y) / 240) - 12;
            if (y < 0)
            {
                y = 0;
            }
        }

        p->databits = p->databits | ((mmUInt32_t)x << 10) | ((mmUInt32_t)y << 2);
    }
    else
    {
        // L->H
        if (((~p->olddata)&data) & 0x02)
        {
            p->databits <<= 1;
        }
        if (!(data & 0x02))
        {
            p->outbits = 0x04;
        }
        else
        {
            if (p->databits & 0x40000)
            {
                p->outbits = 0x00;
            }
            else
            {
                p->outbits = 0x08;
            }
        }
        p->olddata = data;
    }
}

MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadOekakidsTablet* p = (struct mmEmuPadOekakidsTablet*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmEmuNes_GetZapperPos(p->super.nes, &p->zapper_x, &p->zapper_y);

    p->zapper_button = 0;
    if (mmEmuController_MouseButtonStatus(controller, MM_MB_LBUTTON))
    {
        p->zapper_button = 0xFF;
    }
}
MM_EXPORT_EMU void mmEmuPadOekakidsTablet_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadOekakidsTablet* p = (struct mmEmuPadOekakidsTablet*)(super);

    if (type == 0)
    {
        p->zapper_x = data;
    }
    else if (type == 1)
    {
        p->zapper_y = data;
    }
    else if (type == 2)
    {
        p->zapper_button = data ? 0xFF : 0x00;
    }
}
MM_EXPORT_EMU mmLong_t mmEmuPadOekakidsTablet_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadOekakidsTablet* p = (struct mmEmuPadOekakidsTablet*)(super);

    mmLong_t data = 0;

    if (type == 0)
    {
        data = p->zapper_x;
    }
    else if (type == 1)
    {
        data = p->zapper_y;
    }
    else if (type == 2)
    {
        data = p->zapper_button ? 0xFF : 0x00;
    }
    return data;
}

static struct mmEmuExpad* __static_mmEmuPadOekakidsTablet_Produce(void)
{
    struct mmEmuPadOekakidsTablet* p = (struct mmEmuPadOekakidsTablet*)mmMalloc(sizeof(struct mmEmuPadOekakidsTablet));
    mmEmuPadOekakidsTablet_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadOekakidsTablet_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadOekakidsTablet* p = (struct mmEmuPadOekakidsTablet*)(m);
    mmEmuPadOekakidsTablet_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_OEKAKIDSTABLET =
{
    &__static_mmEmuPadOekakidsTablet_Produce,
    &__static_mmEmuPadOekakidsTablet_Recycle,
};
