/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper230.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper230_Init(struct mmEmuMapper230* p)
{
    mmEmuMapper_Init(&p->super);

    p->rom_sw = 0;

    //
    p->super.Reset = &mmEmuMapper230_Reset;
    p->super.Write = &mmEmuMapper230_Write;
    //p->super.Read = &mmEmuMapper230_Read;
    //p->super.ReadLow = &mmEmuMapper230_ReadLow;
    //p->super.WriteLow = &mmEmuMapper230_WriteLow;
    //p->super.ExRead = &mmEmuMapper230_ExRead;
    //p->super.ExWrite = &mmEmuMapper230_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper230_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper230_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper230_HSync;
    //p->super.VSync = &mmEmuMapper230_VSync;
    //p->super.Clock = &mmEmuMapper230_Clock;
    //p->super.PPULatch = &mmEmuMapper230_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper230_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper230_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper230_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper230_IsStateSave;
    //p->super.SaveState = &mmEmuMapper230_SaveState;
    //p->super.LoadState = &mmEmuMapper230_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper230_Destroy(struct mmEmuMapper230* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->rom_sw = 0;
}

MM_EXPORT_EMU void mmEmuMapper230_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper230* p = (struct mmEmuMapper230*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->rom_sw)
    {
        p->rom_sw = 0;
    }
    else
    {
        p->rom_sw = 1;
    }
    if (p->rom_sw)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 14, 15);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 16, 17, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper230_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper230* p = (struct mmEmuMapper230*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->rom_sw)
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, (data & 0x07) * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x07) * 2 + 1);
    }
    else
    {
        if (data & 0x20)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, (data & 0x1F) * 2 + 16);
            mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x1F) * 2 + 17);
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x1F) * 2 + 16);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x1F) * 2 + 17);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, (data & 0x1E) * 2 + 16);
            mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x1E) * 2 + 17);
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x1E) * 2 + 18);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x1E) * 2 + 19);
        }
        if (data & 0x40)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper230_Produce(void)
{
    struct mmEmuMapper230* p = (struct mmEmuMapper230*)mmMalloc(sizeof(struct mmEmuMapper230));
    mmEmuMapper230_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper230_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper230* p = (struct mmEmuMapper230*)(m);
    mmEmuMapper230_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER230 =
{
    &__static_mmEmuMapper230_Produce,
    &__static_mmEmuMapper230_Recycle,
};
