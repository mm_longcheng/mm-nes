/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmulatorMachine.h"

#include "emu/mmEmuNes.h"

#include "dish/mmFilePath.h"

MM_EXPORT_EMULATOR const std::string Event_MM_EMU_UNKNOWN("Event_MM_EMU_UNKNOWN");

MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_EXIT("Event_MM_EMU_RS_EXIT");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_NONE("Event_MM_EMU_RS_NONE");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_INITIAL("Event_MM_EMU_RS_INITIAL");

MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_PLAY("Event_MM_EMU_RS_PLAY");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_STOP("Event_MM_EMU_RS_STOP");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_PAUSE("Event_MM_EMU_RS_PAUSE");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_RESUME("Event_MM_EMU_RS_RESUME");

MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_VIDEOMODE("Event_MM_EMU_RS_VIDEOMODE");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_LOADROM("Event_MM_EMU_RS_LOADROM");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_SCREENCLEAR("Event_MM_EMU_RS_SCREENCLEAR");
// For Logger
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_SETMESSAGESTRING("Event_MM_EMU_RS_SETMESSAGESTRING");

MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_FULLSCREEN_GDI("Event_MM_EMU_RS_FULLSCREEN_GDI");
//
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_EMUPAUSE("Event_MM_EMU_RS_EMUPAUSE");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_ONEFRAME("Event_MM_EMU_RS_ONEFRAME");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_THROTTLE("Event_MM_EMU_RS_THROTTLE");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_FRAMESKIP_AUTO("Event_MM_EMU_RS_FRAMESKIP_AUTO");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_FRAMESKIP_UP("Event_MM_EMU_RS_FRAMESKIP_UP");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_FRAMESKIP_DOWN("Event_MM_EMU_RS_FRAMESKIP_DOWN");
//
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_HWRESET("Event_MM_EMU_RS_HWRESET");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_SWRESET("Event_MM_EMU_RS_SWRESET");
//
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_NETPLAY_START("Event_MM_EMU_RS_NETPLAY_START");
//
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_STATE_LOAD("Event_MM_EMU_RS_STATE_LOAD");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_STATE_SAVE("Event_MM_EMU_RS_STATE_SAVE");
//
// For Disk system
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_DISK_COMMAND("Event_MM_EMU_RS_DISK_COMMAND");
// For ExController
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_EXCONTROLLER("Event_MM_EMU_RS_EXCONTROLLER");
// For Sound
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_SOUND_MUTE("Event_MM_EMU_RS_SOUND_MUTE");
//
// For Snapshot
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_SNAPSHOT("Event_MM_EMU_RS_SNAPSHOT");
// For Movie
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_MOVIE_PLAY("Event_MM_EMU_RS_MOVIE_PLAY");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_MOVIE_REC("Event_MM_EMU_RS_MOVIE_REC");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_MOVIE_RECAPPEND("Event_MM_EMU_RS_MOVIE_RECAPPEND");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_MOVIE_STOP("Event_MM_EMU_RS_MOVIE_STOP");
//
// For Wave recording
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_WAVEREC_START("Event_MM_EMU_RS_WAVEREC_START");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_WAVEREC_STOP("Event_MM_EMU_RS_WAVEREC_STOP");
// For Tape recording
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_TAPE_PLAY("Event_MM_EMU_RS_TAPE_PLAY");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_TAPE_REC("Event_MM_EMU_RS_TAPE_REC");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_TAPE_STOP("Event_MM_EMU_RS_TAPE_STOP");
//
// For Barcode
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_BARCODE("Event_MM_EMU_RS_BARCODE");
//
// For TurboFile
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_TURBOFILE("Event_MM_EMU_RS_TURBOFILE");
//
// For Debugger
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_DEBUG_RUN("Event_MM_EMU_RS_DEBUG_RUN");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_DEBUG_BRAKE("Event_MM_EMU_RS_DEBUG_BRAKE");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_DEBUG_STEP("Event_MM_EMU_RS_DEBUG_STEP");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_RS_DEBUG_COMMAND("Event_MM_EMU_RS_DEBUG_COMMAND");

MM_EXPORT_EMULATOR const std::string Event_MM_EMU_NT_NONE("Event_MM_EMU_NT_NONE");
//
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_NT_PLAY("Event_MM_EMU_NT_PLAY");
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_NT_STOP("Event_MM_EMU_NT_STOP");
// For Screen
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_NT_SCREENMESSAGE("Event_MM_EMU_NT_SCREENMESSAGE");
// For Movie
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_NT_MOVIE_FINISH("Event_MM_EMU_NT_MOVIE_FINISH");
// For VideoMode
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_NT_VIDEOMODE("Event_MM_EMU_NT_VIDEOMODE");
// For ExController
MM_EXPORT_EMULATOR const std::string Event_MM_EMU_NT_EXCONTROLLER("Event_MM_EMU_NT_EXCONTROLLER");

MM_EXPORT_EMULATOR const mmEmulatorEventMidName MM_EMULATOR_EVENT_NAME[] =
{
    { MM_EMU_RS_EXIT            , &Event_MM_EMU_RS_EXIT             , },
    { MM_EMU_RS_NONE            , &Event_MM_EMU_RS_NONE             , },
    { MM_EMU_RS_INITIAL         , &Event_MM_EMU_RS_INITIAL          , },

    { MM_EMU_RS_PLAY            , &Event_MM_EMU_RS_PLAY             , },
    { MM_EMU_RS_STOP            , &Event_MM_EMU_RS_STOP             , },
    { MM_EMU_RS_PAUSE           , &Event_MM_EMU_RS_PAUSE            , },
    { MM_EMU_RS_RESUME          , &Event_MM_EMU_RS_RESUME           , },

    { MM_EMU_RS_VIDEOMODE       , &Event_MM_EMU_RS_VIDEOMODE        , },
    { MM_EMU_RS_LOADROM         , &Event_MM_EMU_RS_LOADROM          , },
    { MM_EMU_RS_SCREENCLEAR     , &Event_MM_EMU_RS_SCREENCLEAR      , },
    // For Logger
    { MM_EMU_RS_SETMESSAGESTRING, &Event_MM_EMU_RS_SETMESSAGESTRING , },

    { MM_EMU_RS_FULLSCREEN_GDI  , &Event_MM_EMU_RS_FULLSCREEN_GDI   , },
    //
    { MM_EMU_RS_EMUPAUSE        , &Event_MM_EMU_RS_EMUPAUSE         , },
    { MM_EMU_RS_ONEFRAME        , &Event_MM_EMU_RS_ONEFRAME         , },
    { MM_EMU_RS_THROTTLE        , &Event_MM_EMU_RS_THROTTLE         , },
    { MM_EMU_RS_FRAMESKIP_AUTO  , &Event_MM_EMU_RS_FRAMESKIP_AUTO   , },
    { MM_EMU_RS_FRAMESKIP_UP    , &Event_MM_EMU_RS_FRAMESKIP_UP     , },
    { MM_EMU_RS_FRAMESKIP_DOWN  , &Event_MM_EMU_RS_FRAMESKIP_DOWN   , },
    //
    { MM_EMU_RS_HWRESET         , &Event_MM_EMU_RS_HWRESET          , },
    { MM_EMU_RS_SWRESET         , &Event_MM_EMU_RS_SWRESET          , },
    //
    { MM_EMU_RS_NETPLAY_START   , &Event_MM_EMU_RS_NETPLAY_START    , },
    //
    { MM_EMU_RS_STATE_LOAD      , &Event_MM_EMU_RS_STATE_LOAD       , },
    { MM_EMU_RS_STATE_SAVE      , &Event_MM_EMU_RS_STATE_SAVE       , },
    //
    // For Disk system
    { MM_EMU_RS_DISK_COMMAND    , &Event_MM_EMU_RS_DISK_COMMAND     , },
    // For ExController
    { MM_EMU_RS_EXCONTROLLER    , &Event_MM_EMU_RS_EXCONTROLLER     , },
    // For Sound
    { MM_EMU_RS_SOUND_MUTE      , &Event_MM_EMU_RS_SOUND_MUTE       , },
    //
    // For Snapshot
    { MM_EMU_RS_SNAPSHOT        , &Event_MM_EMU_RS_SNAPSHOT         , },
    // For Movie
    { MM_EMU_RS_MOVIE_PLAY      , &Event_MM_EMU_RS_MOVIE_PLAY       , },
    { MM_EMU_RS_MOVIE_REC       , &Event_MM_EMU_RS_MOVIE_REC        , },
    { MM_EMU_RS_MOVIE_RECAPPEND , &Event_MM_EMU_RS_MOVIE_RECAPPEND  , },
    { MM_EMU_RS_MOVIE_STOP      , &Event_MM_EMU_RS_MOVIE_STOP       , },
    //
    // For Wave recording
    { MM_EMU_RS_WAVEREC_START   , &Event_MM_EMU_RS_WAVEREC_START    , },
    { MM_EMU_RS_WAVEREC_STOP    , &Event_MM_EMU_RS_WAVEREC_STOP     , },
    // For Tape recording
    { MM_EMU_RS_TAPE_PLAY       , &Event_MM_EMU_RS_TAPE_PLAY        , },
    { MM_EMU_RS_TAPE_REC        , &Event_MM_EMU_RS_TAPE_REC         , },
    { MM_EMU_RS_TAPE_STOP       , &Event_MM_EMU_RS_TAPE_STOP        , },
    //
    // For Barcode
    { MM_EMU_RS_BARCODE         , &Event_MM_EMU_RS_BARCODE          , },
    //
    // For TurboFile
    { MM_EMU_RS_TURBOFILE       , &Event_MM_EMU_RS_TURBOFILE        , },
    //
    // For Debugger
    { MM_EMU_RS_DEBUG_RUN       , &Event_MM_EMU_RS_DEBUG_RUN        , },
    { MM_EMU_RS_DEBUG_BRAKE     , &Event_MM_EMU_RS_DEBUG_BRAKE      , },
    { MM_EMU_RS_DEBUG_STEP      , &Event_MM_EMU_RS_DEBUG_STEP       , },
    { MM_EMU_RS_DEBUG_COMMAND   , &Event_MM_EMU_RS_DEBUG_COMMAND    , },

    { MM_EMU_NT_NONE            , &Event_MM_EMU_NT_NONE             , },

    { MM_EMU_NT_PLAY            , &Event_MM_EMU_NT_PLAY             , },
    { MM_EMU_NT_STOP            , &Event_MM_EMU_NT_STOP             , },
    // For Screen
    { MM_EMU_NT_SCREENMESSAGE   , &Event_MM_EMU_NT_SCREENMESSAGE    , },
    // For Movie
    { MM_EMU_NT_MOVIE_FINISH    , &Event_MM_EMU_NT_MOVIE_FINISH     , },
    // For VideoMode
    { MM_EMU_NT_VIDEOMODE       , &Event_MM_EMU_NT_VIDEOMODE        , },
    // For ExController
    { MM_EMU_NT_EXCONTROLLER    , &Event_MM_EMU_NT_EXCONTROLLER     , },
};

static const size_t MM_EMULATOR_EVENT_NAME_SIZE = MM_ARRAY_SIZE(MM_EMULATOR_EVENT_NAME);

static size_t __static_EventNameBinarySerachFindEqual(size_t o, size_t l, mmUInt32_t key)
{
    size_t mid = 0;
    size_t left = o;
    size_t right = o + l - 1;
    mmSInt32_t result = 0;

    if (0 == l)
    {
        return (size_t)-1;
    }
    else
    {
        while (left <= right && (size_t)(-1) != right)
        {
            // Avoid overflow problems
            mid = left + (right - left) / 2;

            result = MM_EMULATOR_EVENT_NAME[mid].mid - key;

            if (result > 0)
            {
                right = mid - 1;
            }
            else if (result < 0)
            {
                left = mid + 1;
            }
            else
            {
                return mid;
            }
        }

        return (size_t)-1;
    }
}

static void __static_mmEmulatorMachine_EmuEmulatorHandleDefault(void* obj, void* u, mmUInt32_t mid, struct mmEmuEmulatorEvent* emulator_event)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmulatorMachine* _emulator_machine = (struct mmEmulatorMachine*)(emulator->callback_q.obj);
    size_t index = 0;

    mm::mmEventEmulatorArgs _args;

    _args.mid = mid;
    _args.p0 = emulator_event->p0;
    _args.p1 = emulator_event->p1;
    _args.ps = mmString_CStr(&emulator_event->ps);

    index = __static_EventNameBinarySerachFindEqual(0, MM_EMULATOR_EVENT_NAME_SIZE, mid);

    if ((size_t)(-1) != index)
    {
        const struct mmEmulatorEventMidName* pair = &MM_EMULATOR_EVENT_NAME[index];
        _emulator_machine->hEventSet.FireEvent((*(pair->name)), _args);
    }
    else
    {
        _emulator_machine->hEventSet.FireEvent(Event_MM_EMU_UNKNOWN, _args);
    }
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_Init(struct mmEmulatorMachine* p)
{
    struct mmEmuEmulatorCallback hEmulatorCallback;

    // c++ struct can not internal structure life cycle manual, do it external.
    // new (&p->hEventSet) mm::mmEventSet();

    mmEmuEmulatorProcessor_Init(&p->super);

    p->pContextMaster = NULL;
    p->pFileContext = NULL;

    // c++ struct can not internal structure life cycle manual, do it external.
    // new (&p->hName) std::string();
    // new (&p->hDataPath) std::string();
    // new (&p->hWritablePath) std::string();

    mmEmuEmulator_Init(&p->hEmulator);
    mmEmulatorAudio_Init(&p->hAudio);
    mmEmulatorFrame_Init(&p->hFrame);

    mmEmulatorAssets_Init(&p->hEmulatorAssets);
    //
    p->hName = "mm";
    p->hDataPath = "emulator";
    p->hWritablePath = "emulator";

    mmEmuEmulator_SetProcessor(&p->hEmulator, &p->super);

    hEmulatorCallback.Handle = &__static_mmEmulatorMachine_EmuEmulatorHandleDefault;
    hEmulatorCallback.obj = p;
    mmEmuEmulator_SetCallbackDefault(&p->hEmulator, &hEmulatorCallback);

    mmEmulatorFrame_SetPaletteRGB(&p->hFrame, p->hEmulator.palette_rgb);

    mmEmulatorAudio_SetRate(&p->hAudio, p->hEmulator.emu->config.audio.nRate);
    mmEmulatorAudio_SetFormat(&p->hAudio, AL_FORMAT_MONO8);

    //
    p->super.ThreadEnter = &mmEmulatorMachine_ThreadEnter;
    p->super.ThreadLeave = &mmEmulatorMachine_ThreadLeave;
    p->super.ProcessAudio = &mmEmulatorMachine_ProcessAudio;
    p->super.ProcessFrame = &mmEmulatorMachine_ProcessFrame;
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_Destroy(struct mmEmulatorMachine* p)
{
    mmEmuEmulatorProcessor_Destroy(&p->super);

    // c++ struct can not internal structure life cycle manual, do it external.
    // p->hEventSet.~mmEventSet();

    p->pContextMaster = NULL;
    p->pFileContext = NULL;

    // c++ struct can not internal structure life cycle manual, do it external.
    // p->hName.~basic_string();
    // p->hDataPath.~basic_string();
    // p->hWritablePath.~basic_string();

    mmEmuEmulator_Destroy(&p->hEmulator);
    mmEmulatorAudio_Destroy(&p->hAudio);
    mmEmulatorFrame_Destroy(&p->hFrame);

    mmEmulatorAssets_Destroy(&p->hEmulatorAssets);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetContextMaster(struct mmEmulatorMachine* p, struct mmContextMaster* pContextMaster)
{
    p->pContextMaster = pContextMaster;

    mmEmulatorFrame_SetContextMaster(&p->hFrame, p->pContextMaster);

    mmEmulatorMachine_SetFileContext(p, &p->pContextMaster->hFileContext);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetName(struct mmEmulatorMachine* p, const std::string& name)
{
    p->hName = name;

    mmEmulatorFrame_SetName(&p->hFrame, p->hName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetMaterialPathPrefix(struct mmEmulatorMachine* p, const std::string& path)
{
    mmEmulatorFrame_SetMaterialPathPrefix(&p->hFrame, path);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetRomPath(struct mmEmulatorMachine* p, const char* szRomPath)
{
    mmEmuEmulator_SetRomPath(&p->hEmulator, szRomPath);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetDataPath(struct mmEmulatorMachine* p, const std::string& path)
{
    p->hDataPath = path;
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetWritablePath(struct mmEmulatorMachine* p, const std::string& path)
{
    p->hWritablePath = path;
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetFileContext(struct mmEmulatorMachine* p, struct mmFileContext* _file_context)
{
    p->pFileContext = _file_context;
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetTaskBackground(struct mmEmulatorMachine* p, mmUInt8_t background)
{
    mmEmuEmulator_SetTaskBackground(&p->hEmulator, background);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetAssets(struct mmEmulatorMachine* p, struct mmEmulatorAssets* assets)
{
    // cache current value.
    mmEmulatorAssets_Copy(&p->hEmulatorAssets, assets);

    mmMkdirIfNotExist(mmString_CStr(&assets->writable_directory));

    mmEmulatorMachine_SetFileContext(p, assets->file_context);
    mmEmulatorMachine_SetRomPath(p, mmString_CStr(&assets->assets_name));
    mmEmulatorMachine_SetWritablePath(p, mmString_CStr(&assets->emulator_directory));

    mmEmulatorMachine_UpdateAssetsPath(p);

    mmEmulatorMachine_SetResourceRoot(p, assets->assets_type, mmString_CStr(&assets->rootpath), "");
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetMute(struct mmEmulatorMachine* p, mmBool_t mute)
{
    mmEmulatorAudio_SetMute(&p->hAudio, mute);
}
MM_EXPORT_EMULATOR mmBool_t mmEmulatorMachine_GetMute(struct mmEmulatorMachine* p)
{
    return p->hAudio.mute;
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetExControllerType(struct mmEmulatorMachine* p, mmInt_t type)
{
    mmEmuEmulator_ExController(&p->hEmulator, type);
}
MM_EXPORT_EMULATOR mmInt_t mmEmulatorMachine_GetExControllerType(struct mmEmulatorMachine* p)
{
    return mmEmuEmulator_GetExControllerType(&p->hEmulator);
}

MM_EXPORT_EMULATOR mmInt_t mmEmulatorMachine_GetVideoMode(struct mmEmulatorMachine* p)
{
    return mmEmuEmulator_GetVideoMode(&p->hEmulator);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetRapidSpeed(struct mmEmulatorMachine* p, mmInt_t no, mmWord_t speed[2])
{
    mmEmuEmulator_SetRapidSpeed(&p->hEmulator, no, speed);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_GetRapidSpeed(struct mmEmulatorMachine* p, mmInt_t no, mmWord_t speed[2])
{
    mmEmuEmulator_GetRapidSpeed(&p->hEmulator, no, speed);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetResourceRoot(struct mmEmulatorMachine* p, mmUInt32_t type_assets, const char* root_path, const char* root_base)
{
    mmEmuEmulator_SetResourceRoot(&p->hEmulator, type_assets, root_path, root_base);
}

MM_EXPORT_EMULATOR mmUInt32_t mmEmulatorMachine_GetRomLoadCode(struct mmEmulatorMachine* p)
{
    return mmEmuEmulator_GetRomLoadCode(&p->hEmulator);
}
MM_EXPORT_EMULATOR struct mmFileContext* mmEmulatorMachine_GetAssetsFileContext(struct mmEmulatorMachine* p)
{
    return &p->hEmulator.emu->assets.file_context;
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_UpdateAssetsPath(struct mmEmulatorMachine* p)
{
    struct mmFileContext* _file_context = p->pFileContext;
    struct mmPackageAssets* _PackageAssets = &p->pContextMaster->hPackageAssets;

    std::string external_files_directory;
    std::string external_files_directory_writable_path;

    struct mmString emu_writable_path;
    struct mmString emu_root_folder_base;
    struct mmString emu_root_source_base;

    mmString_Init(&emu_writable_path);
    mmString_Init(&emu_root_folder_base);
    mmString_Init(&emu_root_source_base);

    external_files_directory = mmString_CStr(&_PackageAssets->hExternalFilesDirectory);
    //
    external_files_directory_writable_path += external_files_directory;
    external_files_directory_writable_path += external_files_directory.empty() ? "" : "/";
    external_files_directory_writable_path += p->hWritablePath;
    mmString_Assigns(&emu_writable_path, external_files_directory_writable_path.c_str());
    // use little base.
    mmString_Assign(&emu_root_folder_base, &_file_context->assets_root_folder_base);
    mmString_Appends(&emu_root_folder_base, 0 == mmString_Size(&_file_context->assets_root_folder_base) ? "" : "/");
    mmString_Appends(&emu_root_folder_base, p->hDataPath.c_str());

    mmString_Assign(&emu_root_source_base, &_file_context->assets_root_source_base);
    mmString_Appends(&emu_root_source_base, 0 == mmString_Size(&_file_context->assets_root_source_base) ? "" : "/");
    mmString_Appends(&emu_root_source_base, p->hDataPath.c_str());
    //
    mmMkdirIfNotExist(mmString_CStr(&emu_writable_path));
    // use the main file_context.
    mmEmuEmulator_SetResourceRoot(&p->hEmulator, MM_EMU_ASSETS_FOLDER, mmString_CStr(&_file_context->assets_root_folder_path), mmString_CStr(&emu_root_folder_base));
    mmEmuEmulator_SetResourceRoot(&p->hEmulator, MM_EMU_ASSETS_SOURCE, mmString_CStr(&_file_context->assets_root_source_path), mmString_CStr(&emu_root_source_base));
    //
    mmEmuEmulator_SetWritablePath(&p->hEmulator, mmString_CStr(&emu_writable_path));
    //
    mmString_Destroy(&emu_writable_path);
    mmString_Destroy(&emu_root_folder_base);
    mmString_Destroy(&emu_root_source_base);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_FinishLaunching(struct mmEmulatorMachine* p)
{
    mmEmulatorFrame_FinishLaunching(&p->hFrame);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_BeforeTerminate(struct mmEmulatorMachine* p)
{
    mmEmulatorFrame_BeforeTerminate(&p->hFrame);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetVideoMode(struct mmEmulatorMachine* p, mmInt_t nMode)
{
    mmEmuEmulator_SetVideoMode(&p->hEmulator, nMode);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_LoadRom(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_LoadRom(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_ScreenClear(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_ScreenClear(&p->hEmulator);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetVideoModeImmediately(struct mmEmulatorMachine* p, mmInt_t nMode)
{
    mmEmuEmulator_SetVideoModeImmediately(&p->hEmulator, nMode);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_LoadRomImmediately(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_LoadRomImmediately(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_ScreenClearImmediately(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_ScreenClearImmediately(&p->hEmulator);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_Play(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Play(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_Stop(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Stop(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_Pause(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Pause(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_Resume(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Resume(&p->hEmulator);
}
MM_EXPORT_EMULATOR mmInt_t mmEmulatorMachine_GetState(struct mmEmulatorMachine* p)
{
    return mmEmuEmulator_GetState(&p->hEmulator);
}

// event keyboard.
MM_EXPORT_EMULATOR void mmEmulatorMachine_KeyboardPressed(struct mmEmulatorMachine* p, int id)
{
    mmEmuEmulator_KeyboardPressed(&p->hEmulator, id);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_KeyboardRelease(struct mmEmulatorMachine* p, int id)
{
    mmEmuEmulator_KeyboardRelease(&p->hEmulator, id);
}
// event Mouse.
MM_EXPORT_EMULATOR void mmEmulatorMachine_MouseBegan(struct mmEmulatorMachine* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuEmulator_MouseBegan(&p->hEmulator, x, y, button_mask);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_MouseMoved(struct mmEmulatorMachine* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuEmulator_MouseMoved(&p->hEmulator, x, y, button_mask);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_MouseEnded(struct mmEmulatorMachine* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuEmulator_MouseEnded(&p->hEmulator, x, y, button_mask);
}
// event window.
MM_EXPORT_EMULATOR void mmEmulatorMachine_EnterBackground(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_EnterBackground(&p->hEmulator);
    mmEmulatorFrame_EnterBackground(&p->hFrame);
    mmEmulatorAudio_EnterBackground(&p->hAudio);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_EnterForeground(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_EnterForeground(&p->hEmulator);
    mmEmulatorFrame_EnterForeground(&p->hFrame);
    mmEmulatorAudio_EnterForeground(&p->hAudio);
}

// For Manual bit controller Joypad.
MM_EXPORT_EMULATOR void mmEmulatorMachine_JoypadBitSetData(struct mmEmulatorMachine* p, int n, mmWord_t data)
{
    mmEmuEmulator_JoypadBitSetData(&p->hEmulator, n, data);
}
MM_EXPORT_EMULATOR mmWord_t mmEmulatorMachine_JoypadBitGetData(struct mmEmulatorMachine* p, int n)
{
    return mmEmuEmulator_JoypadBitGetData(&p->hEmulator, n);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_JoypadBitPressed(struct mmEmulatorMachine* p, int n, int id)
{
    mmEmuEmulator_JoypadBitPressed(&p->hEmulator, n, id);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_JoypadBitRelease(struct mmEmulatorMachine* p, int n, int id)
{
    mmEmuEmulator_JoypadBitRelease(&p->hEmulator, n, id);
}
// For Manual bit controller Nsf.
MM_EXPORT_EMULATOR void mmEmulatorMachine_NsfBitSetData(struct mmEmulatorMachine* p, mmByte_t data)
{
    mmEmuEmulator_NsfBitSetData(&p->hEmulator, data);
}
MM_EXPORT_EMULATOR mmByte_t mmEmulatorMachine_NsfBitGetData(struct mmEmulatorMachine* p)
{
    return mmEmuEmulator_NsfBitGetData(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_NsfBitPressed(struct mmEmulatorMachine* p, int id)
{
    mmEmuEmulator_NsfBitPressed(&p->hEmulator, id);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_NsfBitRelease(struct mmEmulatorMachine* p, int id)
{
    mmEmuEmulator_NsfBitRelease(&p->hEmulator, id);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_HardwareReset(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_HardwareReset(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SoftwareReset(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_SoftwareReset(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_StateLoad(struct mmEmulatorMachine* p, const char* szFName)
{
    mmEmuEmulator_StateLoad(&p->hEmulator, szFName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_StateSave(struct mmEmulatorMachine* p, const char* szFName)
{
    mmEmuEmulator_StateSave(&p->hEmulator, szFName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_DiskCommand(struct mmEmulatorMachine* p, mmInt_t cmd)
{
    mmEmuEmulator_DiskCommand(&p->hEmulator, cmd);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_Snapshot(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Snapshot(&p->hEmulator);
}
MM_EXPORT_EMULATOR mmBool_t mmEmulatorMachine_IsMoviePlay(struct mmEmulatorMachine* p)
{
    return mmEmuEmulator_IsMoviePlay(&p->hEmulator);
}
MM_EXPORT_EMULATOR mmBool_t mmEmulatorMachine_IsMovieRec(struct mmEmulatorMachine* p)
{
    return mmEmuEmulator_IsMovieRec(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_MoviePlay(struct mmEmulatorMachine* p, const char* szFName)
{
    mmEmuEmulator_MoviePlay(&p->hEmulator, szFName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_MovieRec(struct mmEmulatorMachine* p, const char* szFName)
{
    mmEmuEmulator_MovieRec(&p->hEmulator, szFName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_MovieRecAppend(struct mmEmulatorMachine* p, const char* szFName)
{
    mmEmuEmulator_MovieRecAppend(&p->hEmulator, szFName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_MovieStop(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_MovieStop(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_WaveRecStart(struct mmEmulatorMachine* p, const char* szFName)
{
    mmEmuEmulator_WaveRecStart(&p->hEmulator, szFName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_WaveRecStop(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_WaveRecStop(&p->hEmulator);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_TapePlay(struct mmEmulatorMachine* p, const char* szFName)
{
    mmEmuEmulator_TapePlay(&p->hEmulator, szFName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_TapeRec(struct mmEmulatorMachine* p, const char* szFName)
{
    mmEmuEmulator_TapeRec(&p->hEmulator, szFName);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_TapeStop(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_TapeStop(&p->hEmulator);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetBarcodeData(struct mmEmulatorMachine* p, mmByte_t* code, mmInt_t len)
{
    mmEmuEmulator_SetBarcodeData(&p->hEmulator, code, len);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetTurboFileBank(struct mmEmulatorMachine* p, mmInt_t bank)
{
    mmEmuEmulator_SetTurboFileBank(&p->hEmulator, bank);
}

MM_EXPORT_EMULATOR const std::string& mmEmulatorMachine_GetImageName(struct mmEmulatorMachine* p)
{
    return p->hFrame.hNameCEGUIImageryEmu;
}
MM_EXPORT_EMULATOR const std::string& mmEmulatorMachine_GetAliasTextureName0(struct mmEmulatorMachine* p)
{
    return p->hFrame.hNameOgreTextureEmu0;
}
MM_EXPORT_EMULATOR const std::string& mmEmulatorMachine_GetAliasTextureName1(struct mmEmulatorMachine* p)
{
    return p->hFrame.hNameOgreTextureEmu1;
}
MM_EXPORT_EMULATOR Ogre::MaterialPtr mmEmulatorMachine_GetScreenMaterial(struct mmEmulatorMachine* p)
{
    return p->hFrame.hMaterialPtr;
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_UpdateFrameBitmap(struct mmEmulatorMachine* p)
{
    mmEmulatorFrame_UpdateFrameBitmap(&p->hFrame);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_UpdateRenderer(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_UpdateRenderer(&p->hEmulator);
    mmEmulatorMachine_UpdateFrameBitmap(p);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_Start(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Start(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_Interrupt(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Interrupt(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_Shutdown(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Shutdown(&p->hEmulator);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_Join(struct mmEmulatorMachine* p)
{
    mmEmuEmulator_Join(&p->hEmulator);
}

MM_EXPORT_EMULATOR void mmEmulatorMachine_ThreadEnter(struct mmEmuEmulatorProcessor* super)
{
    struct mmEmulatorMachine* p = (struct mmEmulatorMachine*)(super);

    mmEmulatorAudio_FinishLaunching(&p->hAudio);
    mmEmulatorAudio_Play(&p->hAudio);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_ThreadLeave(struct mmEmuEmulatorProcessor* super)
{
    struct mmEmulatorMachine* p = (struct mmEmulatorMachine*)(super);

    mmEmulatorAudio_Stop(&p->hAudio);
    mmEmulatorAudio_BeforeTerminate(&p->hAudio);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_ProcessAudio(struct mmEmuEmulatorProcessor* super, mmByte_t* buffer, size_t length)
{
    struct mmEmulatorMachine* p = (struct mmEmulatorMachine*)(super);

    mmEmulatorAudio_ProcessBuffer(&p->hAudio, buffer, length);
}
MM_EXPORT_EMULATOR void mmEmulatorMachine_ProcessFrame(struct mmEmuEmulatorProcessor* super, mmByte_t* buffer, size_t length)
{
    struct mmEmulatorMachine* p = (struct mmEmulatorMachine*)(super);

    mmEmulatorFrame_ProcessBuffer(&p->hFrame, buffer, length);
}

