/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApuInterface.h"

MM_EXPORT_EMU void mmEmuApuInterface_Init(struct mmEmuApuInterface* p)
{
    p->nes = NULL;

    p->Reset = &mmEmuApuInterface_Reset;
    p->Setup = &mmEmuApuInterface_Setup;
    p->Write = &mmEmuApuInterface_Write;
    p->Process = &mmEmuApuInterface_Process;

    p->Read = &mmEmuApuInterface_Read;

    p->SyncWrite = &mmEmuApuInterface_SyncWrite;
    p->SyncRead = &mmEmuApuInterface_SyncRead;
    p->VSync = &mmEmuApuInterface_VSync;
    p->Sync = &mmEmuApuInterface_Sync;

    p->GetFreq = &mmEmuApuInterface_GetFreq;

    // for state save
    p->GetStateSize = &mmEmuApuInterface_GetStateSize;
    p->SaveState = &mmEmuApuInterface_SaveState;
    p->LoadState = &mmEmuApuInterface_LoadState;
}
MM_EXPORT_EMU void mmEmuApuInterface_Destroy(struct mmEmuApuInterface* p)
{
    p->nes = NULL;

    p->Reset = &mmEmuApuInterface_Reset;
    p->Setup = &mmEmuApuInterface_Setup;
    p->Write = &mmEmuApuInterface_Write;
    p->Process = &mmEmuApuInterface_Process;

    p->Read = &mmEmuApuInterface_Read;

    p->SyncWrite = &mmEmuApuInterface_SyncWrite;
    p->SyncRead = &mmEmuApuInterface_SyncRead;
    p->VSync = &mmEmuApuInterface_VSync;
    p->Sync = &mmEmuApuInterface_Sync;

    p->GetFreq = &mmEmuApuInterface_GetFreq;

    // for state save
    p->GetStateSize = &mmEmuApuInterface_GetStateSize;
    p->SaveState = &mmEmuApuInterface_SaveState;
    p->LoadState = &mmEmuApuInterface_LoadState;
}

MM_EXPORT_EMU void mmEmuApuInterface_SetParent(struct mmEmuApuInterface* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU void mmEmuApuInterface_Reset(struct mmEmuApuInterface* p, double fclock, mmInt_t nrate)
{

}
MM_EXPORT_EMU void mmEmuApuInterface_Setup(struct mmEmuApuInterface* p, double fclock, mmInt_t nrate)
{

}
MM_EXPORT_EMU void mmEmuApuInterface_Write(struct mmEmuApuInterface* p, mmWord_t addr, mmByte_t data)
{

}
MM_EXPORT_EMU mmInt_t mmEmuApuInterface_Process(struct mmEmuApuInterface* p, mmInt_t channel)
{
    return 0;
}

MM_EXPORT_EMU mmByte_t mmEmuApuInterface_Read(struct mmEmuApuInterface* p, mmWord_t addr)
{
    return 0;
}

MM_EXPORT_EMU void mmEmuApuInterface_SyncWrite(struct mmEmuApuInterface* p, mmWord_t addr, mmByte_t data)
{

}
MM_EXPORT_EMU mmByte_t mmEmuApuInterface_SyncRead(struct mmEmuApuInterface* p, mmWord_t addr)
{
    return 0;
}
MM_EXPORT_EMU void mmEmuApuInterface_VSync(struct mmEmuApuInterface* p)
{

}
MM_EXPORT_EMU mmBool_t mmEmuApuInterface_Sync(struct mmEmuApuInterface* p, mmInt_t cycles)
{
    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuInterface_GetFreq(const struct mmEmuApuInterface* p, mmInt_t channel)
{
    return 0;
}

// for state save
MM_EXPORT_EMU mmInt_t mmEmuApuInterface_GetStateSize(const struct mmEmuApuInterface* p)
{
    return 0;
}
MM_EXPORT_EMU void mmEmuApuInterface_SaveState(struct mmEmuApuInterface* p, mmByte_t* buffer)
{

}
MM_EXPORT_EMU void mmEmuApuInterface_LoadState(struct mmEmuApuInterface* p, mmByte_t* buffer)
{

}
