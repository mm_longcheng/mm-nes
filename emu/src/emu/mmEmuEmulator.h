/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuEmulator_h__
#define __mmEmuEmulator_h__

#include "core/mmCore.h"
#include "core/mmFrameTimer.h"
#include "core/mmStreambuf.h"
#include "core/mmMessageQueue.h"

#include "container/mmConcurrentQueue.h"

#include "dish/mmFileContext.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

enum mmEmuEmulatorRqMid_t
{
    MM_EMU_RQ_EXIT = -1,        // EventParam0
    MM_EMU_RQ_NONE = 0,         // EventParam0
    MM_EMU_RQ_INITIAL,          // EventParam0
    //
    MM_EMU_RQ_PLAY,             // EventParam0
    MM_EMU_RQ_STOP,             // EventParam0
    MM_EMU_RQ_PAUSE,            // EventParam0
    MM_EMU_RQ_RESUME,           // EventParam0
    // Use the following in Event.
    MM_EMU_RQ_VIDEOMODE,        // EventParam1
    MM_EMU_RQ_LOADROM,          // EventParam0
    MM_EMU_RQ_SCREENCLEAR,      // EventParam0

    MM_EMU_RQ_SETMESSAGESTRING, // Used to output only messages.

    MM_EMU_RQ_FULLSCREEN_GDI,   // EventParam1

    MM_EMU_RQ_EMUPAUSE,         // EventParam0
    MM_EMU_RQ_ONEFRAME,         // EventParam0
    MM_EMU_RQ_THROTTLE,         // EventParam0
    MM_EMU_RQ_FRAMESKIP_AUTO,   // EventParam0
    MM_EMU_RQ_FRAMESKIP_UP,     // EventParam0
    MM_EMU_RQ_FRAMESKIP_DOWN,   // EventParam0

    MM_EMU_RQ_HWRESET,          // EventParam0
    MM_EMU_RQ_SWRESET,          // EventParam0

    MM_EMU_RQ_NETPLAY_START,    // EventParam0

    MM_EMU_RQ_STATE_LOAD,       // EventParam1
    MM_EMU_RQ_STATE_SAVE,       // EventParam1

    // For Disk system
    MM_EMU_RQ_DISK_COMMAND,     // EventParam1
    // For ExController
    MM_EMU_RQ_EXCONTROLLER,     // EventParam1
    // For Sound
    MM_EMU_RQ_SOUND_MUTE,       // EventParam1

    // For Snapshot
    MM_EMU_RQ_SNAPSHOT,         // EventParam0
    // For Movie
    MM_EMU_RQ_MOVIE_PLAY,       // EventParam1
    MM_EMU_RQ_MOVIE_REC,        // EventParam1
    MM_EMU_RQ_MOVIE_RECAPPEND,  // EventParam1
    MM_EMU_RQ_MOVIE_STOP,       // EventParam0

    // For Wave recording
    MM_EMU_RQ_WAVEREC_START,    // EventParam1
    MM_EMU_RQ_WAVEREC_STOP,     // EventParam0

    // For Tape recording
    MM_EMU_RQ_TAPE_PLAY,        // EventParam1
    MM_EMU_RQ_TAPE_REC,         // EventParam1
    MM_EMU_RQ_TAPE_STOP,        // EventParam0

    // For Barcode
    MM_EMU_RQ_BARCODE,          // EventParam2

    // For TurboFile
    MM_EMU_RQ_TURBOFILE,        // EventParam1

    // For Debugger
    MM_EMU_RQ_DEBUG_RUN,        // EventParam0
    MM_EMU_RQ_DEBUG_BRAKE,      // EventParam0
    MM_EMU_RQ_DEBUG_STEP,       // EventParam0
    MM_EMU_RQ_DEBUG_COMMAND,    // EventParam1
};

enum mmEmuEmulatorRsMid_t
{
    MM_EMU_RS_EXIT = 0x00000FFF,// EventParam0
    MM_EMU_RS_NONE = 0x00001000,// EventParam0
    MM_EMU_RS_INITIAL,          // EventParam0
    //
    MM_EMU_RS_PLAY,             // EventParam0
    MM_EMU_RS_STOP,             // EventParam0
    MM_EMU_RS_PAUSE,            // EventParam0
    MM_EMU_RS_RESUME,           // EventParam0
    // Use the following in Event.
    MM_EMU_RS_VIDEOMODE,        // EventParam1
    MM_EMU_RS_LOADROM,          // EventParam0
    MM_EMU_RS_SCREENCLEAR,      // EventParam0
    // For Logger
    MM_EMU_RS_SETMESSAGESTRING, // EventParam1

    MM_EMU_RS_FULLSCREEN_GDI,   // EventParam1

    MM_EMU_RS_EMUPAUSE,         // EventParam0
    MM_EMU_RS_ONEFRAME,         // EventParam0
    MM_EMU_RS_THROTTLE,         // EventParam0
    MM_EMU_RS_FRAMESKIP_AUTO,   // EventParam0
    MM_EMU_RS_FRAMESKIP_UP,     // EventParam0
    MM_EMU_RS_FRAMESKIP_DOWN,   // EventParam0

    MM_EMU_RS_HWRESET,          // EventParam0
    MM_EMU_RS_SWRESET,          // EventParam0

    MM_EMU_RS_NETPLAY_START,    // EventParam0

    MM_EMU_RS_STATE_LOAD,       // EventParam1
    MM_EMU_RS_STATE_SAVE,       // EventParam1

    // For Disk system
    MM_EMU_RS_DISK_COMMAND,     // EventParam1
    // For ExController
    MM_EMU_RS_EXCONTROLLER,     // EventParam1
    // For Sound
    MM_EMU_RS_SOUND_MUTE,       // EventParam1

    // For Snapshot
    MM_EMU_RS_SNAPSHOT,         // EventParam1
    // For Movie
    MM_EMU_RS_MOVIE_PLAY,       // EventParam0
    MM_EMU_RS_MOVIE_REC,        // EventParam0
    MM_EMU_RS_MOVIE_RECAPPEND,  // EventParam0
    MM_EMU_RS_MOVIE_STOP,       // EventParam0

    // For Wave recording
    MM_EMU_RS_WAVEREC_START,    // EventParam1
    MM_EMU_RS_WAVEREC_STOP,     // EventParam0

    // For Tape recording
    MM_EMU_RS_TAPE_PLAY,        // EventParam1
    MM_EMU_RS_TAPE_REC,         // EventParam1
    MM_EMU_RS_TAPE_STOP,        // EventParam0

    // For Barcode
    MM_EMU_RS_BARCODE,          // EventParam2

    // For TurboFile
    MM_EMU_RS_TURBOFILE,        // EventParam1

    // For Debugger
    MM_EMU_RS_DEBUG_RUN,        // EventParam0
    MM_EMU_RS_DEBUG_BRAKE,      // EventParam0
    MM_EMU_RS_DEBUG_STEP,       // EventParam0
    MM_EMU_RS_DEBUG_COMMAND,    // EventParam1
};

enum mmEmuEmulatorNtMid_t
{
    MM_EMU_NT_NONE = 0x00002000,// EventParam0
    //
    MM_EMU_NT_PLAY,             // EventParam0
    MM_EMU_NT_STOP,             // EventParam0
    // For Screen
    MM_EMU_NT_SCREENMESSAGE,    // EventParam1
    // For Movie
    MM_EMU_NT_MOVIE_FINISH,     // EventParam0
    // For VideoMode
    MM_EMU_NT_VIDEOMODE,        // EventParam1
    // For ExController
    MM_EMU_NT_EXCONTROLLER,     // EventParam1
};

struct mmEmuEmulatorEvent
{
    mmUInt32_t p0;
    mmUInt32_t p1;
    //
    struct mmString ps;
};

MM_EXPORT_EMU void mmEmuEmulatorEvent_Init(struct mmEmuEmulatorEvent* p);
MM_EXPORT_EMU void mmEmuEmulatorEvent_Destroy(struct mmEmuEmulatorEvent* p);

MM_EXPORT_EMU void mmEmuEmulatorEvent_Reset(struct mmEmuEmulatorEvent* p);

MM_EXPORT_EMU size_t mmEmuEmulatorEvent_BufferSize(const struct mmEmuEmulatorEvent* p);

MM_EXPORT_EMU void mmEmuEmulatorEvent_Encode(const struct mmEmuEmulatorEvent* p, struct mmByteBuffer* b, size_t* offset);
MM_EXPORT_EMU void mmEmuEmulatorEvent_Decode(struct mmEmuEmulatorEvent* p, const struct mmByteBuffer* b, size_t* offset);

MM_EXPORT_EMU void mmEmuEmulatorEvent_SetP0(struct mmEmuEmulatorEvent* p, mmUInt32_t p0);
MM_EXPORT_EMU void mmEmuEmulatorEvent_SetP1(struct mmEmuEmulatorEvent* p, mmUInt32_t p1);
MM_EXPORT_EMU void mmEmuEmulatorEvent_SetPs(struct mmEmuEmulatorEvent* p, const char* ps);

// MM_SUCCESS MM_FAILURE
MM_EXPORT_EMU int mmEmuEmulatorEvent_Validity(const struct mmEmuEmulatorEvent* p);

struct mmEmuEmulator;
// emulator handle callback.
typedef void(*mmEmuEmulatorHandleFunc)(void* obj, void* u, mmUInt32_t mid, struct mmEmuEmulatorEvent* pEvent);
struct mmEmuEmulatorCallback
{
    mmEmuEmulatorHandleFunc Handle;
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_EMU void mmEmuEmulatorCallback_Init(struct mmEmuEmulatorCallback* p);
MM_EXPORT_EMU void mmEmuEmulatorCallback_Destroy(struct mmEmuEmulatorCallback* p);

// emulator thread processor.
struct mmEmuEmulatorProcessor
{
    struct mmEmuEmulator* emulator;

    void(*ThreadEnter)(struct mmEmuEmulatorProcessor* obj);
    void(*ThreadLeave)(struct mmEmuEmulatorProcessor* obj);

    int(*ProcessAudio)(struct mmEmuEmulatorProcessor* obj, mmByte_t* buffer, size_t length);
    int(*ProcessFrame)(struct mmEmuEmulatorProcessor* obj, mmByte_t* buffer, size_t length);
};
MM_EXPORT_EMU void mmEmuEmulatorProcessor_Init(struct mmEmuEmulatorProcessor* p);
MM_EXPORT_EMU void mmEmuEmulatorProcessor_Destroy(struct mmEmuEmulatorProcessor* p);
MM_EXPORT_EMU void mmEmuEmulatorProcessor_SetParent(struct mmEmuEmulatorProcessor* p, struct mmEmuEmulator* parent);

MM_EXPORT_EMU extern const struct mmEmuEmulatorProcessor MM_EMU_EMULATOR_PROCESSOR_DEFAULT;

enum
{
    MM_EMU_ASSETS_FOLDER = MM_FILE_ASSETS_TYPE_FOLDER,
    MM_EMU_ASSETS_SOURCE = MM_FILE_ASSETS_TYPE_SOURCE,
};

enum mmEmuEmulatorState_t
{
    MM_EMULATOR_NONE,
    MM_EMULATOR_PLAYING,
    MM_EMULATOR_PAUSED,
    MM_EMULATOR_STOPPED,
};

#define MM_EMU_EMULATOR_FREQUENCY_DEFAULT 5

// [-512, +512] 1470 + 512 = 1982 < 2048    nRate: 29730
#define MM_EMU_EMULATOR_AUDIO_MAX_BALANCE 512

struct mmEmuEmulator
{
    // mmEmuNes size little big.
    // we use malloc for less stack memory.
    struct mmEmuNes* emu;
    struct mmFrameTask frame_task;
    struct mmEmuEmulatorCallback callback_q;

    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_q;

    mmAtomic_t locker_rbtree_q;

    pthread_t timer_thread;

    double frequency;

    // R8G8B8A8 size = 256x1 or 16x16
    mmUInt32_t* palette;

    // rom load code.
    mmUInt32_t rom_load;

    mmBool_t background;
    mmBool_t background_runing;

    mmUInt8_t audio_buffer_idx;
    struct mmStreambuf audio_block;
    mmUInt32_t audio_block_size;
    mmUInt32_t audio_frame_size;
    double audio_block_k;
    int audio_frame_index;
    int audio_queue_balance;

    // weak ref.
    struct mmEmuEmulatorProcessor* processor;

    // renderer => emulator
    struct mmMessageQueue mq_ev;
    // emulator => renderer
    struct mmMessageQueue mq_nt;

    // default is MM_EMULATOR_NONE.
    mmInt_t emulator_state;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;

    // weak ref user pointer.
    void* u;
};

MM_EXPORT_EMU void mmEmuEmulator_Init(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_Destroy(struct mmEmuEmulator* p);

MM_EXPORT_EMU void mmEmuEmulator_SetRomPath(struct mmEmuEmulator* p, const char* szRomPath);
MM_EXPORT_EMU void mmEmuEmulator_SetRomName(struct mmEmuEmulator* p, const char* szRomName);
MM_EXPORT_EMU void mmEmuEmulator_SetWritablePath(struct mmEmuEmulator* p, const char* szWritablePath);
MM_EXPORT_EMU void mmEmuEmulator_SetResourceRoot(struct mmEmuEmulator* p, mmUInt32_t type_assets, const char* root_path, const char* root_base);
MM_EXPORT_EMU void mmEmuEmulator_SetPaletteTableByte(struct mmEmuEmulator* p, const mmUInt32_t pal[64]);

MM_EXPORT_EMU void mmEmuEmulator_SetTaskBackground(struct mmEmuEmulator* p, mmUInt8_t background);

MM_EXPORT_EMU mmUInt32_t mmEmuEmulator_GetRomLoadCode(const struct mmEmuEmulator* p);

MM_EXPORT_EMU void mmEmuEmulator_SetProcessor(struct mmEmuEmulator* p, struct mmEmuEmulatorProcessor* processor);

MM_EXPORT_EMU void mmEmuEmulator_SetContext(struct mmEmuEmulator* p, void* u);
// emulator -> renderer event
MM_EXPORT_EMU void mmEmuEmulator_SetCallbackDefault(struct mmEmuEmulator* p, struct mmEmuEmulatorCallback* callback);
MM_EXPORT_EMU void mmEmuEmulator_SetHandle(struct mmEmuEmulator* p, mmUInt32_t mid, mmEmuEmulatorHandleFunc callback);
MM_EXPORT_EMU void mmEmuEmulator_ClearHandleRbtree(struct mmEmuEmulator* p);

// NTSC/PAL/Dendy
MM_EXPORT_EMU void mmEmuEmulator_SetVideoModeImmediately(struct mmEmuEmulator* p, mmInt_t nMode);
// Load rom Immediately, not use the event queue.
MM_EXPORT_EMU void mmEmuEmulator_LoadRomImmediately(struct mmEmuEmulator* p);
// Screen clear Immediately, not use the event queue.
MM_EXPORT_EMU void mmEmuEmulator_ScreenClearImmediately(struct mmEmuEmulator* p);

// the status is thread safe.
MM_EXPORT_EMU mmInt_t mmEmuEmulator_GetVideoMode(const struct mmEmuEmulator* p);
MM_EXPORT_EMU mmInt_t mmEmuEmulator_GetExControllerType(const struct mmEmuEmulator* p);
// A Rapid speed, B Rapid speed
MM_EXPORT_EMU void mmEmuEmulator_SetRapidSpeed(struct mmEmuEmulator* p, mmInt_t no, const mmWord_t speed[2]);
MM_EXPORT_EMU void mmEmuEmulator_GetRapidSpeed(const struct mmEmuEmulator* p, mmInt_t no, mmWord_t speed[2]);

MM_EXPORT_EMU void mmEmuEmulator_Play(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_Stop(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_Pause(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_Resume(struct mmEmuEmulator* p);
MM_EXPORT_EMU mmInt_t mmEmuEmulator_GetState(const struct mmEmuEmulator* p);

// event Keyboard.
MM_EXPORT_EMU void mmEmuEmulator_KeyboardPressed(struct mmEmuEmulator* p, int hId);
MM_EXPORT_EMU void mmEmuEmulator_KeyboardRelease(struct mmEmuEmulator* p, int hId);
// event Mouse.
MM_EXPORT_EMU void mmEmuEmulator_MouseBegan(struct mmEmuEmulator* p, mmLong_t x, mmLong_t y, int button_mask);
MM_EXPORT_EMU void mmEmuEmulator_MouseMoved(struct mmEmuEmulator* p, mmLong_t x, mmLong_t y, int button_mask);
MM_EXPORT_EMU void mmEmuEmulator_MouseEnded(struct mmEmuEmulator* p, mmLong_t x, mmLong_t y, int button_mask);
// event window.
MM_EXPORT_EMU void mmEmuEmulator_EnterBackground(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_EnterForeground(struct mmEmuEmulator* p);

// For Manual bit controller Joypad.
MM_EXPORT_EMU void mmEmuEmulator_JoypadBitSetData(struct mmEmuEmulator* p, int n, mmWord_t data);
MM_EXPORT_EMU mmWord_t mmEmuEmulator_JoypadBitGetData(struct mmEmuEmulator* p, int n);
MM_EXPORT_EMU void mmEmuEmulator_JoypadBitPressed(struct mmEmuEmulator* p, int n, int hId);
MM_EXPORT_EMU void mmEmuEmulator_JoypadBitRelease(struct mmEmuEmulator* p, int n, int hId);
// For Manual bit controller Nsf.
MM_EXPORT_EMU void mmEmuEmulator_NsfBitSetData(struct mmEmuEmulator* p, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuEmulator_NsfBitGetData(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_NsfBitPressed(struct mmEmuEmulator* p, int hId);
MM_EXPORT_EMU void mmEmuEmulator_NsfBitRelease(struct mmEmuEmulator* p, int hId);

MM_EXPORT_EMU void mmEmuEmulator_UpdateEmulator(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_UpdateRenderer(struct mmEmuEmulator* p);

MM_EXPORT_EMU void mmEmuEmulator_UpdateSynchronize(struct mmEmuEmulator* p, double interval);
//
MM_EXPORT_EMU void mmEmuEmulator_UpdateAudio(struct mmEmuEmulator* p, double interval);
MM_EXPORT_EMU void mmEmuEmulator_UpdateFrame(struct mmEmuEmulator* p, double interval);

// message event => emulator thread api.
MM_EXPORT_EMU void mmEmuEmulator_CommandEv(struct mmEmuEmulator* p, mmUInt32_t mid, struct mmEmuEmulatorEvent* pEvent);
// message event => renderer thread api.
MM_EXPORT_EMU void mmEmuEmulator_CommandNt(struct mmEmuEmulator* p, mmUInt32_t mid, struct mmEmuEmulatorEvent* pEvent);

// NTSC/PAL/Dendy
MM_EXPORT_EMU void mmEmuEmulator_SetVideoMode(struct mmEmuEmulator* p, mmInt_t nMode);
// Load Rom.
MM_EXPORT_EMU void mmEmuEmulator_LoadRom(struct mmEmuEmulator* p);
// Screen clear.
MM_EXPORT_EMU void mmEmuEmulator_ScreenClear(struct mmEmuEmulator* p);
//
MM_EXPORT_EMU void mmEmuEmulator_HardwareReset(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_SoftwareReset(struct mmEmuEmulator* p);
//
MM_EXPORT_EMU void mmEmuEmulator_SetMessageString(struct mmEmuEmulator* p, const char* message);
//
MM_EXPORT_EMU void mmEmuEmulator_StateLoad(struct mmEmuEmulator* p, const char* szFName);
MM_EXPORT_EMU void mmEmuEmulator_StateSave(struct mmEmuEmulator* p, const char* szFName);
//
MM_EXPORT_EMU void mmEmuEmulator_DiskCommand(struct mmEmuEmulator* p, mmInt_t cmd);
MM_EXPORT_EMU void mmEmuEmulator_ExController(struct mmEmuEmulator* p, mmInt_t param);
MM_EXPORT_EMU void mmEmuEmulator_SoundMute(struct mmEmuEmulator* p, mmInt_t param);
//
MM_EXPORT_EMU void mmEmuEmulator_Snapshot(struct mmEmuEmulator* p);
//
MM_EXPORT_EMU mmBool_t mmEmuEmulator_IsMoviePlay(const struct mmEmuEmulator* p);
MM_EXPORT_EMU mmBool_t mmEmuEmulator_IsMovieRec(const struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_MoviePlay(struct mmEmuEmulator* p, const char* szFName);
MM_EXPORT_EMU void mmEmuEmulator_MovieRec(struct mmEmuEmulator* p, const char* szFName);
MM_EXPORT_EMU void mmEmuEmulator_MovieRecAppend(struct mmEmuEmulator* p, const char* szFName);
MM_EXPORT_EMU void mmEmuEmulator_MovieStop(struct mmEmuEmulator* p);
// 
MM_EXPORT_EMU void mmEmuEmulator_WaveRecStart(struct mmEmuEmulator* p, const char* szFName);
MM_EXPORT_EMU void mmEmuEmulator_WaveRecStop(struct mmEmuEmulator* p);
//
MM_EXPORT_EMU void mmEmuEmulator_TapePlay(struct mmEmuEmulator* p, const char* szFName);
MM_EXPORT_EMU void mmEmuEmulator_TapeRec(struct mmEmuEmulator* p, const char* szFName);
MM_EXPORT_EMU void mmEmuEmulator_TapeStop(struct mmEmuEmulator* p);
//
MM_EXPORT_EMU void mmEmuEmulator_SetBarcodeData(struct mmEmuEmulator* p, mmByte_t* code, mmInt_t len);
MM_EXPORT_EMU void mmEmuEmulator_SetTurboFileBank(struct mmEmuEmulator* p, mmInt_t bank);

MM_EXPORT_EMU void mmEmuEmulator_PollWait(struct mmEmuEmulator* p);

MM_EXPORT_EMU void mmEmuEmulator_Start(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_Interrupt(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_Shutdown(struct mmEmuEmulator* p);
MM_EXPORT_EMU void mmEmuEmulator_Join(struct mmEmuEmulator* p);

#include "core/mmSuffix.h"

#endif//__mmEmuEmulator_h__
