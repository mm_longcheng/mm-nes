Pin out
        .--\/--.
 AD1 <- |01  40| -- +5V
 AD2 <- |02  39| -> OUT0
/RST -> |03  38| -> OUT1
 A00 <- |04  37| -> OUT2
 A01 <- |05  36| -> /OE1
 A02 <- |06  35| -> /OE2
 A03 <- |07  34| -> R/W
 A04 <- |08  33| <- /NMI
 A05 <- |09  32| <- /IRQ
 A06 <- |10  31| -> M2
 A07 <- |11  30| <- TST (usually GND)
 A08 <- |12  29| <- CLK
 A09 <- |13  28| <> D0
 A10 <- |14  27| <> D1
 A11 <- |15  26| <> D2
 A12 <- |16  25| <> D3
 A13 <- |17  24| <> D4
 A14 <- |18  23| <> D5
 A15 <- |19  22| <> D6
 GND -- |20  21| <> D7
        `------'
		
		
Signal description

Active-Low signals are prefixed by a / (slash) symbol. Every cycle is either a read or a write cycle.
CLK : 21.47727 MHz (NTSC) or 26.6017 MHz (PAL) clock input. Internally, this clock is divided by 12 (NTSC 2A03) or 16 (PAL 2A07) to feed the 6502's clock input ��0, which is in turn inverted to form ��1, which is then inverted to form ��2. ��1 is high during the first phase (half-cycle) of each CPU cycle, while ��2 is high during the second phase.
AD1 : Audio out pin (both pulse waves).
AD2 : Audio out pin (triangle, noise, and DPCM).
Axx and Dx : Address and data bus, respectively. Axx holds the target address during the entire read/write cycle. For reads, the value is read from Dx during ��2. For writes, the value appears on Dx during ��2 (and no sooner).
OUT0..OUT2 : Output pins used by the controllers ($4016 output latch bits 0-2). These 3 pins are connected to either the NES or Famicom's expansion port, and OUT0 is additionally used as the "strobe" signal (OUT) on both controller ports.
/OE1 and /OE2 : Controller ports (for controller #1 and #2 respectively). Each enable the output of their respective controller, if present.
R/W : Read/write signal, which is used to indicate operations of the same names. Low is write. R/W stays high/low during the entire read/write cycle.
/NMI : Non-maskable interrupt pin. See the 6502 manual and CPU interrupts for more details.
/IRQ : Interrupt pin. See the 6502 manual and CPU interrupts for more details.
M2 : Can be considered as a "signals ready" pin. It is a modified version the 6502's ��2 (which roughly corresponds to the CPU input clock ��0) that allows for slower ROMs. CPU cycles begin at the point where M2 goes low.
In the NTSC 2A03, M2 has a duty cycle of 5/8th, or 350ns/559ns. Equivalently, a CPU read (which happens during the second, high phase of M2) takes 1 and 7/8th PPU cycles. The internal ��2 duty cycle is exactly 1/2 (one half).
In the PAL 2A07, the duty cycle is not known, but suspected to be 19/32.
TST : (tentative name) Pin 30 is special: normally it is grounded in the NES, Famicom, PC10/VS. NES and other Nintendo Arcade Boards (Punch-Out!! and Donkey Kong 3). But if it is pulled high on the RP2A03G, extra diagnostic registers to test the sound hardware are enabled from $4018 through $401A, and the joystick ports $4016 and $4017 become open bus. On older revisions of the CPU, pulling pin 30 high instead causes the CPU to stop execution.
/RST : When low, holds CPU in reset state, during which all CPU pins (except pin 2) are in high impedance state. When released, CPU starts executing code (read $FFFC, read $FFFD, ...) after 6 M2 clocks.