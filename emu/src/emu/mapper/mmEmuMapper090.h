/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper090_h__
#define __mmEmuMapper090_h__


#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper090  PC-JY-??                                                  //
//----------------------------------------------------------------------//
struct mmEmuMapper090
{
    struct mmEmuMapper super;

    mmByte_t    patch;

    mmByte_t    prg_reg[4];
    mmByte_t    nth_reg[4], ntl_reg[4];
    mmByte_t    chh_reg[8], chl_reg[8];

    mmByte_t    irq_enable;
    mmByte_t    irq_counter;
    mmByte_t    irq_latch;
    mmByte_t    irq_occur;
    mmByte_t    irq_preset;
    mmByte_t    irq_offset;

    mmByte_t    prg_6000, prg_E000;
    mmByte_t    prg_size, chr_size;
    mmByte_t    mir_mode, mir_type;

    mmByte_t    key_val;
    mmByte_t    mul_val1, mul_val2;
    mmByte_t    sw_val;
};

MM_EXPORT_EMU void mmEmuMapper090_Init(struct mmEmuMapper090* p);
MM_EXPORT_EMU void mmEmuMapper090_Destroy(struct mmEmuMapper090* p);

MM_EXPORT_EMU void mmEmuMapper090_SetBankCPU(struct mmEmuMapper090* p);
MM_EXPORT_EMU void mmEmuMapper090_SetBankPPU(struct mmEmuMapper090* p);
MM_EXPORT_EMU void mmEmuMapper090_SetBankVRAM(struct mmEmuMapper090* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper090_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU mmByte_t mmEmuMapper090_ReadLow(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper090_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper090_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper090_HSync(struct mmEmuMapper* super, mmInt_t scanline);
MM_EXPORT_EMU void mmEmuMapper090_Clock(struct mmEmuMapper* super, mmInt_t cycles);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper090_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper090_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper090_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER090;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper090_h__
