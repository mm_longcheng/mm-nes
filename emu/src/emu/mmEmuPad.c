/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPad.h"
#include "mmEmuNes.h"
#include "mmEmuRom.h"

#include "mmEmuExpadRegister.h"

#include "core/mmAlloc.h"

MM_EXPORT_EMU const char* MM_EMU_EXCONTROLLER_TYPE[19] =
{
    "None",

    "ArkanoidPaddle",
    "HyperShot",
    "Zapper",
    "F-BasicKeyboard",
    "CrazyClimber",
    "TopRider",
    "SpaceShadowGun",

    "FamilyTrainerA",
    "FamilyTrainerB",
    "ExcitingBoxing",
    "Mahjang",
    "OekakidsTablet",
    "TurboFile",

    "VS-UniSystem",
    "VS-UniZapper",

    "Gyromite",
    "Stackup",

    "SuporKeyboard",
};

MM_EXPORT_EMU const char* mmEmuPad_ExControllerTypeToString(int type)
{
    if (MM_EMU_PAD_EXCONTROLLER_NONE <= type && type <= MM_EMU_PAD_EXCONTROLLER_SUPORKEYBOARD)
    {
        return MM_EMU_EXCONTROLLER_TYPE[type];
    }
    else
    {
        return "Unknown";
    }
}

static const mmInt_t ren30fps[] =
{
    1, 0,
};
static const mmInt_t ren20fps[] =
{
    1, 1, 0,
};
static const mmInt_t ren15fps[] =
{
    1, 1, 0, 0,
};
static const mmInt_t ren10fps[] =
{
    1, 1, 1, 0, 0, 0,
};

static const mmInt_t renmask[] =
{
    6, 4, 3, 2,
};
static const mmInt_t* rentbl[] =
{
    ren10fps, ren15fps, ren20fps, ren30fps,
};

MM_EXPORT_EMU const char* MM_EMU_PAD_RAPID_SPEED[4] =
{
    "10fps",
    "15fps",
    "20fps",
    "30fps",
};

MM_EXPORT_EMU const char* mmEmuPad_RapidSpeedToString(mmWord_t speed)
{
    if (MM_EMU_PAD_RAPID_SPEED_10FPS <= speed && speed <= MM_EMU_PAD_RAPID_SPEED_30FPS)
    {
        return MM_EMU_PAD_RAPID_SPEED[speed];
    }
    else
    {
        return "Unknown";
    }
}

MM_EXPORT_EMU void mmEmuPad_Init(struct mmEmuPad* p)
{
    p->nes = NULL;

    p->pad1bit = 0;
    p->pad2bit = 0;
    p->pad3bit = 0;
    p->pad4bit = 0;

    p->zapperx = 0;
    p->zappery = 0;
    p->zapperbit = 0;
    p->crazyclimberbit = 0;

    mmEmuExpadFactory_Init(&p->expad_factory);
    p->expad = NULL;

    mmMemset(p->padbit, 0, sizeof(mmByte_t) * 4);
    p->micbit = 0;

    mmMemset(p->padbitsync, 0, sizeof(mmByte_t) * 4);
    p->micbitsync = 0;

    p->excontroller_select = 0;
    mmMemset(p->padcnt, 0, sizeof(mmInt_t) * 4 * 2);

    p->bStrobe = MM_FALSE;
    p->bZapperMode = MM_FALSE;

    p->nVSSwapType = MM_EMU_PAD_VS_TYPE0;
    p->bSwapPlayer = MM_FALSE;
    p->bSwapButton = MM_FALSE;

    p->bBarcodeWorld = 0;

    p->nsfbit = 0;

    mmMemset(p->nManualPadbit, 0, sizeof(mmWord_t) * 4);
    p->nManualNsfbit = 0;

    //
    mmEmuExpadRegister_Internal(&p->expad_factory);
}
MM_EXPORT_EMU void mmEmuPad_Destroy(struct mmEmuPad* p)
{
    mmEmuExpadFactory_Recycle(&p->expad_factory, (mmUInt32_t)(p->excontroller_select), p->expad);

    mmEmuScreen_SetZapperMode(&p->nes->screen, MM_FALSE);
    mmEmuScreen_SetZapperDrawMode(&p->nes->screen, MM_FALSE);

    p->nes = NULL;

    p->pad1bit = 0;
    p->pad2bit = 0;
    p->pad3bit = 0;
    p->pad4bit = 0;

    p->zapperx = 0;
    p->zappery = 0;
    p->zapperbit = 0;
    p->crazyclimberbit = 0;

    mmEmuExpadFactory_Destroy(&p->expad_factory);
    p->expad = NULL;

    mmMemset(p->padbit, 0, sizeof(mmByte_t) * 4);
    p->micbit = 0;

    mmMemset(p->padbitsync, 0, sizeof(mmByte_t) * 4);
    p->micbitsync = 0;

    p->excontroller_select = 0;
    mmMemset(p->padcnt, 0, sizeof(mmInt_t) * 4 * 2);

    p->bStrobe = MM_FALSE;
    p->bZapperMode = MM_FALSE;

    p->nVSSwapType = MM_EMU_PAD_VS_TYPE0;
    p->bSwapPlayer = MM_FALSE;
    p->bSwapButton = MM_FALSE;

    p->bBarcodeWorld = 0;

    p->nsfbit = 0;

    mmMemset(p->nManualPadbit, 0, sizeof(mmWord_t) * 4);
    p->nManualNsfbit = 0;
}

MM_EXPORT_EMU void mmEmuPad_SetParent(struct mmEmuPad* p, struct mmEmuNes* parent)
{
    p->nes = parent;
    if (p->expad)
    {
        mmEmuExpad_SetParent(p->expad, p->nes);
    }
}

MM_EXPORT_EMU void mmEmuPad_Reset(struct mmEmuPad* p)
{
    mmInt_t excontroller_type = MM_EMU_PAD_EXCONTROLLER_NONE;

    mmUInt32_t crc = 0;

    p->pad1bit = p->pad2bit = 0;
    p->bStrobe = MM_FALSE;

    //if (!p->bZapperMode)
    //{
    //  p->bZapperMode = FALSE;
    //}

    p->bBarcodeWorld = MM_FALSE;

    mmMemset(p->padcnt, 0, sizeof(mmInt_t) * 4 * 2);

    mmMemset(p->nManualPadbit, 0, sizeof(mmWord_t) * 4);
    p->nManualNsfbit = 0;

    // Select Extension Devices
    crc = mmEmuRom_GetPROMCRC(&p->nes->rom);

    if (crc == 0xfbfc6a6c ||        // Adventures of Bayou Billy, The(E)
        crc == 0xcb275051 ||        // Adventures of Bayou Billy, The(U)
        crc == 0xfb69c131 ||        // Baby Boomer(Unl)(U)
        crc == 0xf2641ad0 ||        // Barker Bill's Trick Shooting(U)
        crc == 0xbc1dce96 ||        // Chiller (Unl)(U)
        crc == 0x90ca616d ||        // Duck Hunt(JUE)
        crc == 0xeeb7a62b ||        // Duck Hunt(JUE)(p1)
        crc == 0x90ca616d ||        // Duck Hunt(PC10)
        crc == 0x59e3343f ||        // Freedom Force(U)
        crc == 0x242a270c ||        // Gotcha!(U)
        crc == 0x7b5bd2de ||        // Gumshoe(UE)
        crc == 0x255b129c ||        // Gun Sight(J)
        crc == 0x8963ae6e ||        // Hogan's Alley(JU)
        crc == 0x51d2112f ||        // Laser Invasion(U)
        crc == 0x0a866c94 ||        // Lone Ranger, The(U)
        // crc == 0xe4c04eea ||     // Mad City(J)
        crc == 0x9eef47aa ||        // Mechanized Attack(U)
        crc == 0xc2db7551 ||        // Shooting Range(U)
        crc == 0x163e86c0 ||        // To The Earth(U)
        crc == 0x42d893e4 ||        // Operation Wolf(J)
        crc == 0x1388aeb9 ||        // Operation Wolf(U)
        crc == 0x0d3cf705 ||        // Wild Gunman(J)
        crc == 0x389960db)          // Wild Gunman(JUE)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_ZAPPER;
    }
    if (crc == 0x35893b67 ||        // Arkanoid(J)
        crc == 0x6267fbd1)          // Arkanoid 2(J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_PADDLE;
    }
    if (crc == 0xff6621ce ||        // Hyper Olympic(J)
        crc == 0xdb9418e8 ||        // Hyper Olympic(Tonosama Ban)(J)
        crc == 0xac98cd70)          // Hyper Sports(J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_HYPERSHOT;
    }
    if (crc == 0xf9def527 ||        // Family BASIC(Ver2.0)
        crc == 0xde34526e ||        // Family BASIC(Ver2.1a)
        crc == 0xf050b611 ||        // Family BASIC(Ver3)
        crc == 0x3aaeed3f ||        // Family BASIC(Ver3)(Alt)
        crc == 0x868FCD89 ||        // Family BASIC(Ver1.0)
        crc == 0x2D6B7E5A ||        // PLAYBOX BASIC(J) (Prototype_v0.0)
        crc == 0xDA03D908)          // PLAYBOX BASIC (J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_KEYBOARD;
    }
    if (crc == 0x589b6b0d ||        // Supor Computer V3.0
        crc == 0x8b265862 ||        // Supor English
        crc == 0x41401c6d ||        // Supor Computer V4.0
        crc == 0x82F1Fb96 ||        // Supor Computer(Russia) V1.0
        crc == 0x951D82ED ||        // FP-BASIC(V3.3)
        crc == 0xd5d6eac4)          // EDU(C) Computer
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_SUPORKEYBOARD;
        mmEmuNes_SetVideoMode(p->nes, MM_EMU_NES_MODEL_DENDY);
    }
    if (crc == 0xc68363f6 ||        // Crazy Climber(J)
        crc == 0x2989ead6 ||        // Smash TV(U) [!]
        crc == 0x0b8f8128)          // Smash TV(E) [!]
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_CRAZYCLIMBER;
    }
    if (crc == 0x20d22251)          // Top rider(J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_TOPRIDER;
    }
    if (crc == 0x0cd00488)          // Space Shadow(J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN;
    }

    if (crc == 0x8c8fa83b ||        // Family Trainer - Athletic World (J)
        crc == 0x7e704a14 ||        // Family Trainer - Jogging Race (J)
        crc == 0x2330a5d3)          // Family Trainer - Rairai Kyonshiizu (J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERA;
    }
    if (crc == 0xf8da2506 ||        // Family Trainer - Aerobics Studio (J)
        crc == 0xca26a0f1 ||        // Family Trainer - Dai Undoukai (J)
        crc == 0x28068b8c ||        // Family Trainer - Fuuun Takeshi Jou 2 (J)
        crc == 0x10bb8f9a ||        // Family Trainer - Manhattan Police (J)
        crc == 0xad3df455 ||        // Family Trainer - Meiro Dai Sakusen (J)
        crc == 0x8a5b72c0 ||        // Family Trainer - Running Stadium (J)
        crc == 0x59794f2d)          // Family Trainer - Totsugeki Fuuun Takeshi Jou (J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERB;
    }
    if (crc == 0x9fae4d46 ||        // Ide Yousuke Meijin no Jissen Mahjong (J)
        crc == 0x7b44fb2a)          // Ide Yousuke Meijin no Jissen Mahjong 2 (J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_MAHJANG;
    }
    if (crc == 0x786148b6)          // Exciting Boxing (J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_EXCITINGBOXING;
    }
    if (crc == 0xc3c0811d ||        // Oeka Kids - Anpanman no Hiragana Daisuki (J)
        crc == 0x9d048ea4)          // Oeka Kids - Anpanman to Oekaki Shiyou!! (J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_OEKAKIDSTABLET;
    }
#if 0
    if (crc == 0xe792de94 ||        // Best Play - Pro Yakyuu (New) (J)
        crc == 0xf79d684a ||        // Best Play - Pro Yakyuu (Old) (J)
        crc == 0xc2ef3422 ||        // Best Play - Pro Yakyuu 2 (J)
        crc == 0x974e8840 ||        // Best Play - Pro Yakyuu '90 (J)
        crc == 0xb8747abf ||        // Best Play - Pro Yakyuu Special (J)
        crc == 0x9fa1c11f ||        // Castle Excellent (J)
        crc == 0x0b0d4d1b ||        // Derby Stallion - Zenkoku Ban (J)
        crc == 0x728c3d98 ||        // Downtown - Nekketsu Monogatari (J)
        crc == 0xd68a6f33 ||        // Dungeon Kid (J)
        crc == 0x3a51eb04 ||        // Fleet Commander (J)
        crc == 0x7c46998b ||        // Haja no Fuuin (J)
        crc == 0x7e5d2f1a ||        // Itadaki Street - Watashi no Mise ni Yottette (J)
        crc == 0xcee5857b ||        // Ninjara Hoi! (J)
        crc == 0x50ec5e8b ||        // Wizardry - Legacy of Llylgamyn (J)
        crc == 0x343e9146 ||        // Wizardry - Proving Grounds of the Mad Overlord (J)
        crc == 0x33d07e45)          // Wizardry - The Knight of Diamonds (J)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_TURBOFILE;
    }
#endif

    if (crc == 0x67898319)          // Barcode World (J)
    {
        p->bBarcodeWorld = MM_TRUE;
    }

    // VS-Unisystem
    if (mmEmuRom_IsVSUNISYSTEM(&p->nes->rom))
    {
        if (crc == 0xff5135a3 ||    // VS Hogan's Alley
            crc == 0xed588f00 ||    // VS Duck Hunt
            crc == 0x17ae56be)      // VS Freedom Force
        {
            excontroller_type = MM_EMU_PAD_EXCONTROLLER_VSZAPPER;
        }
        else
        {
            excontroller_type = MM_EMU_PAD_EXCONTROLLER_VSUNISYSTEM;
        }
    }

    if (crc == 0x21b099f3)          // Gyromite (JUE)
    {
        excontroller_type = MM_EMU_PAD_EXCONTROLLER_GYROMITE;
    }

    //if (crc == 0x4ee735c1)        // Stack-Up (JU)
    //{ 
    //  mmEmuPad_SetExController(p, MM_EMU_PAD_EXCONTROLLER_STACKUP);
    //}
    mmEmuPad_SetExController(p, excontroller_type);
}

MM_EXPORT_EMU mmUInt32_t mmEmuPad_GetSyncData(const struct mmEmuPad* p)
{
    mmUInt32_t  ret;

    ret =
        ((mmUInt32_t)p->padbit[0]) |
        ((mmUInt32_t)p->padbit[1] << 8) |
        ((mmUInt32_t)p->padbit[2] << 16) |
        ((mmUInt32_t)p->padbit[3] << 24);

    ret |= (mmUInt32_t)p->micbit << 8;

    return  ret;
}
MM_EXPORT_EMU void mmEmuPad_SetSyncData(struct mmEmuPad* p, mmUInt32_t data)
{
    p->micbit = (mmByte_t)((data & 0x00000400) >> 8);
    p->padbit[0] = (mmByte_t)(data);
    p->padbit[1] = (mmByte_t)(data >> 8);
    p->padbit[2] = (mmByte_t)(data >> 16);
    p->padbit[3] = (mmByte_t)(data >> 24);
}

MM_EXPORT_EMU mmUInt32_t mmEmuPad_GetSyncExData(const struct mmEmuPad* p)
{
    mmUInt32_t  data = 0;

    switch (p->excontroller_select)
    {
    case MM_EMU_PAD_EXCONTROLLER_ZAPPER:
    case MM_EMU_PAD_EXCONTROLLER_PADDLE:
    case MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN:
    case MM_EMU_PAD_EXCONTROLLER_OEKAKIDSTABLET:
    case MM_EMU_PAD_EXCONTROLLER_VSZAPPER:
    {
        mmLong_t x, y;
        x = (*(p->expad->GetSyncData))(p->expad, 0);
        y = (*(p->expad->GetSyncData))(p->expad, 1);
        if (x == -1 || y == -1)
        {
            data = 0x80000000;
        }
        else
        {
            data = ((mmUInt32_t)x & 0xFF) | (((mmUInt32_t)y & 0xFF) << 8);
        }
    }
    if (p->excontroller_select != MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN)
    {
        if ((*(p->expad->GetSyncData))(p->expad, 2))
        {
            data |= 0x0010000;
        }
    }
    else
    {
        data |= (mmUInt32_t)(*(p->expad->GetSyncData))(p->expad, 2) << 16;
    }
    break;
    case MM_EMU_PAD_EXCONTROLLER_CRAZYCLIMBER:
        data = (mmUInt32_t)(*(p->expad->GetSyncData))(p->expad, 0);
        break;
    case MM_EMU_PAD_EXCONTROLLER_TOPRIDER:
        data = (mmUInt32_t)(*(p->expad->GetSyncData))(p->expad, 0);
        break;
    case MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERA:
    case MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERB:
        data = (mmUInt32_t)(*(p->expad->GetSyncData))(p->expad, 0);
        break;
    case MM_EMU_PAD_EXCONTROLLER_EXCITINGBOXING:
        data = (mmUInt32_t)(*(p->expad->GetSyncData))(p->expad, 0);
        break;
    case MM_EMU_PAD_EXCONTROLLER_MAHJANG:
        data = (mmUInt32_t)(*(p->expad->GetSyncData))(p->expad, 0);
        break;

    default:
        break;
    }
    return  data;
}
MM_EXPORT_EMU void mmEmuPad_SetSyncExData(struct mmEmuPad* p, mmUInt32_t data)
{
    // DEBUGOUT( "PAD::SetSyncExData\n" );
    switch (p->excontroller_select)
    {
    case MM_EMU_PAD_EXCONTROLLER_ZAPPER:
    case MM_EMU_PAD_EXCONTROLLER_PADDLE:
    case MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN:
    case MM_EMU_PAD_EXCONTROLLER_OEKAKIDSTABLET:
    case MM_EMU_PAD_EXCONTROLLER_VSZAPPER:
    {
        mmLong_t x, y;
        if (data & 0x80000000)
        {
            x = -1;
            y = -1;
        }
        else
        {
            x = (data & 0xFF);
            y = (data & 0xFF00) >> 8;
        }
        (*(p->expad->SetSyncData))(p->expad, 0, x);
        (*(p->expad->SetSyncData))(p->expad, 1, y);
        mmEmuNes_SetZapperPos(p->nes, x, y);

        mmEmuScreen_SetZapperPos(&p->nes->screen, x, y);
    }
    if (p->excontroller_select != MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN)
    {
        if (data & 0x0010000)
        {
            (*(p->expad->SetSyncData))(p->expad, 2, 1);
        }
        else
        {
            (*(p->expad->SetSyncData))(p->expad, 2, 0);
        }
    }
    else
    {
        (*(p->expad->SetSyncData))(p->expad, 2, (mmByte_t)(data >> 16));
    }
    break;
    case MM_EMU_PAD_EXCONTROLLER_CRAZYCLIMBER:
        (*(p->expad->SetSyncData))(p->expad, 0, (mmLong_t)data);
        break;
    case MM_EMU_PAD_EXCONTROLLER_TOPRIDER:
        (*(p->expad->SetSyncData))(p->expad, 0, (mmLong_t)data);
        break;
    case MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERA:
    case MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERB:
        (*(p->expad->SetSyncData))(p->expad, 0, (mmLong_t)data);
        break;
    case MM_EMU_PAD_EXCONTROLLER_EXCITINGBOXING:
        (*(p->expad->SetSyncData))(p->expad, 0, (mmLong_t)data);
        break;
    case MM_EMU_PAD_EXCONTROLLER_MAHJANG:
        (*(p->expad->SetSyncData))(p->expad, 0, (mmLong_t)data);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuPad_VSync(struct mmEmuPad* p)
{
    p->padbitsync[0] = p->padbit[0];
    p->padbitsync[1] = p->padbit[1];
    p->padbitsync[2] = p->padbit[2];
    p->padbitsync[3] = p->padbit[3];
    p->micbitsync = p->micbit;
}

MM_EXPORT_EMU void mmEmuPad_Sync(struct mmEmuPad* p)
{
    struct mmEmuController* controller = &p->nes->controller;

    p->padbit[0] = mmEmuPad_SyncSub(p, 0);
    p->padbit[1] = mmEmuPad_SyncSub(p, 1);
    p->padbit[2] = mmEmuPad_SyncSub(p, 2);
    p->padbit[3] = mmEmuPad_SyncSub(p, 3);

    // Mic
    p->micbit = 0;
    if (mmEmuController_ButtonCheck(controller, 1, 10)) p->micbit |= 4;

    // For Excontroller
    if (p->expad)
    {
        (*(p->expad->Sync))(p->expad);
    }

    // For NSF
    mmEmuPad_NsfSub(p);
}
MM_EXPORT_EMU mmByte_t mmEmuPad_SyncSub(struct mmEmuPad* p, mmInt_t no)
{
    struct mmEmuController* controller = &p->nes->controller;

    mmWord_t bit = p->nManualPadbit[no];

    // Up
    if (mmEmuController_ButtonCheck(controller, no, 0)) bit |= 1 << 4;
    // Down
    if (mmEmuController_ButtonCheck(controller, no, 1)) bit |= 1 << 5;
    // Left
    if (mmEmuController_ButtonCheck(controller, no, 2)) bit |= 1 << 6;
    // Right
    if (mmEmuController_ButtonCheck(controller, no, 3)) bit |= 1 << 7;

    // Disable simultaneous input.
    //if ((bit&((1 << 4) | (1 << 5))) == ((1 << 4) | (1 << 5)))
    //{
    //  bit &= ~((1 << 4) | (1 << 5));
    //}
    if ((bit&((1 << 6) | (1 << 7))) == ((1 << 6) | (1 << 7)))
    {
        bit &= ~((1 << 6) | (1 << 7));
    }

    // A
    if (mmEmuController_ButtonCheck(controller, no, 4)) bit |= 1 << 0;
    // B
    if (mmEmuController_ButtonCheck(controller, no, 5)) bit |= 1 << 1;

    // A,B Rapid
    if (mmEmuController_ButtonCheck(controller, no, 6)) bit |= 1 << 8;
    if (mmEmuController_ButtonCheck(controller, no, 7)) bit |= 1 << 9;

    // Select
    if (mmEmuController_ButtonCheck(controller, no, 8)) bit |= 1 << 2;
    // Start
    if (mmEmuController_ButtonCheck(controller, no, 9)) bit |= 1 << 3;

    // A rapid setup
    if (bit&(1 << 8))
    {
        mmInt_t spd = 0;
        const mmInt_t* tbl = NULL;

        spd = p->nes->config.controller.nRapid[no][0];
        if (spd >= 3) spd = 3;
        tbl = rentbl[spd];

        if (p->padcnt[no][0] >= renmask[spd])
        {
            p->padcnt[no][0] = 0;
        }

        if (tbl[p->padcnt[no][0]])
        {
            bit |= (1 << 0);
        }
        else
        {
            bit &= ~(1 << 0);
        }

        p->padcnt[no][0]++;
    }
    else
    {
        p->padcnt[no][0] = 0;
    }
    // B rapid setup
    if (bit&(1 << 9))
    {
        mmInt_t spd = 0;
        const mmInt_t* tbl = NULL;

        spd = p->nes->config.controller.nRapid[no][1];
        if (spd >= 3) spd = 3;
        tbl = rentbl[spd];

        if (p->padcnt[no][1] >= renmask[spd])
        {
            p->padcnt[no][1] = 0;
        }

        if (tbl[p->padcnt[no][1]])
        {
            bit |= (1 << 1);
        }
        else
        {
            bit &= ~(1 << 1);
        }

        p->padcnt[no][1]++;
    }
    else
    {
        p->padcnt[no][1] = 0;
    }

    return (mmByte_t)(bit & 0xFF);
}

MM_EXPORT_EMU void mmEmuPad_Strobe(struct mmEmuPad* p)
{
    // For VS-Unisystem 
    if (mmEmuRom_IsVSUNISYSTEM(&p->nes->rom))
    {
        mmUInt32_t pad1 = (mmUInt32_t)p->padbitsync[0] & 0xF3;
        mmUInt32_t pad2 = (mmUInt32_t)p->padbitsync[1] & 0xF3;
        mmUInt32_t st1 = ((mmUInt32_t)p->padbitsync[0] & 0x08) >> 3;
        mmUInt32_t st2 = ((mmUInt32_t)p->padbitsync[1] & 0x08) >> 3;

        switch (p->nVSSwapType)
        {
        case MM_EMU_PAD_VS_TYPE0:
            p->pad1bit = pad1 | (st1 << 2);
            p->pad2bit = pad2 | (st2 << 2);
            break;
        case MM_EMU_PAD_VS_TYPE1:
            p->pad1bit = pad2 | (st1 << 2);
            p->pad2bit = pad1 | (st2 << 2);
            break;
        case MM_EMU_PAD_VS_TYPE2:
            p->pad1bit = pad1 | (st1 << 2) | (st2 << 3);
            p->pad2bit = pad2;
            break;
        case MM_EMU_PAD_VS_TYPE3:
            p->pad1bit = pad2 | (st1 << 2) | (st2 << 3);
            p->pad2bit = pad1;
            break;
        case MM_EMU_PAD_VS_TYPE4:
            p->pad1bit = pad1 | (st1 << 2) | 0x08;  // 0x08=Start Protect
            p->pad2bit = pad2 | (st2 << 2) | 0x08;  // 0x08=Start Protect
            break;
        case MM_EMU_PAD_VS_TYPE5:
            p->pad1bit = pad2 | (st1 << 2) | 0x08;  // 0x08=Start Protect
            p->pad2bit = pad1 | (st2 << 2) | 0x08;  // 0x08=Start Protect
            break;
        case MM_EMU_PAD_VS_TYPE6:
            p->pad1bit = pad1 | (st1 << 2) | (((mmUInt32_t)p->padbitsync[0] & 0x04) << 1);
            p->pad2bit = pad2 | (st2 << 2) | (((mmUInt32_t)p->padbitsync[1] & 0x04) << 1);
            break;
        case MM_EMU_PAD_VS_TYPEZ:
            p->pad1bit = 0;
            p->pad2bit = 0;
            break;
        default:
            break;
        }

        // Coin 2 and erase to wear.
        p->micbit = 0;
    }
    else
    {
        if (p->nes->config.emulator.bFourPlayer)
        {
            // NES type
            p->pad1bit = (mmUInt32_t)p->padbitsync[0] | ((mmUInt32_t)p->padbitsync[2] << 8) | 0x00080000;
            p->pad2bit = (mmUInt32_t)p->padbitsync[1] | ((mmUInt32_t)p->padbitsync[3] << 8) | 0x00040000;
        }
        else
        {
            // Famicom type
            p->pad1bit = (mmUInt32_t)p->padbitsync[0];
            p->pad2bit = (mmUInt32_t)p->padbitsync[1];
        }
    }
    p->pad3bit = (mmUInt32_t)p->padbitsync[2];
    p->pad4bit = (mmUInt32_t)p->padbitsync[3];
}
MM_EXPORT_EMU mmWord_t mmEmuPad_StrobeSub(struct mmEmuPad* p, mmInt_t no)
{
    // do noting here.
    return 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPad_Read(struct mmEmuPad* p, mmWord_t addr)
{
    mmByte_t data = 0x00;

    if (addr == 0x4016)
    {
        data = (mmByte_t)p->pad1bit & 1;
        p->pad1bit >>= 1;
        data |= ((mmByte_t)p->pad3bit & 1) << 1;
        p->pad3bit >>= 1;
        // Mic
        if (!mmEmuRom_IsVSUNISYSTEM(&p->nes->rom))
        {
            data |= p->micbitsync;
        }
        if (p->expad)
        {
            data |= (*(p->expad->Read4016))(p->expad);
        }
    }
    if (addr == 0x4017)
    {
        data = (mmByte_t)p->pad2bit & 1;
        p->pad2bit >>= 1;
        data |= ((mmByte_t)p->pad4bit & 1) << 1;
        p->pad4bit >>= 1;

        if (p->expad)
        {
            data |= (*(p->expad->Read4017))(p->expad);
            // DEBUGOUT( "4017:%02X\n", data );
        }

        if (p->bBarcodeWorld)
        {
            data |= mmEmuNes_Barcode2(p->nes);
        }
    }

    return  data;
}
MM_EXPORT_EMU void mmEmuPad_Write(struct mmEmuPad* p, mmWord_t addr, mmByte_t data)
{
    if (addr == 0x4016)
    {
        if (data & 0x01)
        {
            p->bStrobe = MM_TRUE;
        }
        else if (p->bStrobe)
        {
            p->bStrobe = MM_FALSE;

            mmEmuPad_Strobe(p);
            if (p->expad)
            {
                (*(p->expad->Strobe))(p->expad);
            }
        }

        if (p->expad)
        {
            (*(p->expad->Write4016))(p->expad, data);
        }
    }
    if (addr == 0x4017)
    {
        if (p->expad)
        {
            (*(p->expad->Write4017))(p->expad, data);
        }
    }
}

MM_EXPORT_EMU void mmEmuPad_SetExController(struct mmEmuPad* p, mmInt_t type)
{
    if (MM_EMU_PAD_EXCONTROLLER_NONE == type)
    {
        if (NULL != p->expad)
        {
            mmEmuExpadFactory_Recycle(&p->expad_factory, (mmUInt32_t)(p->excontroller_select), p->expad);
            p->expad = NULL;
        }

        p->excontroller_select = type;

        p->bZapperMode = MM_FALSE;
    }
    else
    {
        if (NULL != p->expad)
        {
            mmEmuExpadFactory_Recycle(&p->expad_factory, (mmUInt32_t)(p->excontroller_select), p->expad);
            p->expad = NULL;
        }

        p->excontroller_select = type;

        p->bZapperMode = MM_FALSE;

        mmEmuScreen_SetZapperMode(&p->nes->screen, MM_FALSE);
        mmEmuScreen_SetZapperDrawMode(&p->nes->screen, MM_FALSE);

        // ExPad Instance create
        p->expad = mmEmuExpadFactory_Produce(&p->expad_factory, (mmUInt32_t)(p->excontroller_select));

        switch (type)
        {
        case MM_EMU_PAD_EXCONTROLLER_ZAPPER:
            p->bZapperMode = MM_TRUE;

            mmEmuScreen_SetZapperMode(&p->nes->screen, MM_TRUE);
            mmEmuScreen_SetZapperDrawMode(&p->nes->screen, MM_TRUE);
            break;
        case MM_EMU_PAD_EXCONTROLLER_PADDLE:

            mmEmuScreen_SetZapperMode(&p->nes->screen, MM_TRUE);
            break;
        case MM_EMU_PAD_EXCONTROLLER_HYPERSHOT:
            break;
        case MM_EMU_PAD_EXCONTROLLER_KEYBOARD:
            break;
        case MM_EMU_PAD_EXCONTROLLER_SUPORKEYBOARD:
            break;
        case MM_EMU_PAD_EXCONTROLLER_CRAZYCLIMBER:
            break;
        case MM_EMU_PAD_EXCONTROLLER_TOPRIDER:
            break;
        case MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN:
            p->bZapperMode = MM_TRUE;
            mmEmuScreen_SetZapperMode(&p->nes->screen, MM_TRUE);
            mmEmuScreen_SetZapperDrawMode(&p->nes->screen, MM_TRUE);
            break;

        case MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERA:
        case MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERB:
            break;
        case MM_EMU_PAD_EXCONTROLLER_EXCITINGBOXING:
            break;
        case MM_EMU_PAD_EXCONTROLLER_MAHJANG:
            break;
        case MM_EMU_PAD_EXCONTROLLER_OEKAKIDSTABLET:
            mmEmuScreen_SetZapperMode(&p->nes->screen, MM_TRUE);
            mmEmuScreen_SetZapperDrawMode(&p->nes->screen, MM_FALSE);
            break;
        case MM_EMU_PAD_EXCONTROLLER_TURBOFILE:
            break;

        case MM_EMU_PAD_EXCONTROLLER_VSUNISYSTEM:
            break;
        case MM_EMU_PAD_EXCONTROLLER_VSZAPPER:
            p->bZapperMode = MM_TRUE;
            mmEmuScreen_SetZapperMode(&p->nes->screen, MM_TRUE);
            mmEmuScreen_SetZapperDrawMode(&p->nes->screen, MM_TRUE);
            break;

        case MM_EMU_PAD_EXCONTROLLER_GYROMITE:
            break;

            // Buggy:(
            //      case    EXCONTROLLER_STACKUP:
            //          expad = new EXPAD_StackUp( nes );
            //          break;

        default:
            break;
        }

        if (p->expad)
        {
            mmEmuExpad_SetParent(p->expad, p->nes);
            (*(p->expad->Reset))(p->expad);
        }
    }
}

MM_EXPORT_EMU void mmEmuPad_NsfSub(struct mmEmuPad* p)
{
    struct mmEmuController* controller = &p->nes->controller;

    mmWord_t* nNsfButton = p->nes->config.controller.nNsfButton;

    p->nsfbit = p->nManualNsfbit;

    // Play
    if (mmEmuController_ButtonCheckKeys(controller, 0, nNsfButton)) p->nsfbit |= 1 << 0;
    // Stop
    if (mmEmuController_ButtonCheckKeys(controller, 1, nNsfButton)) p->nsfbit |= 1 << 1;

    // Number -1
    if (mmEmuController_ButtonCheckKeys(controller, 2, nNsfButton)) p->nsfbit |= 1 << 2;
    // Number +1
    if (mmEmuController_ButtonCheckKeys(controller, 3, nNsfButton)) p->nsfbit |= 1 << 3;
    // Number -16
    if (mmEmuController_ButtonCheckKeys(controller, 4, nNsfButton)) p->nsfbit |= 1 << 4;
    // Number +16
    if (mmEmuController_ButtonCheckKeys(controller, 5, nNsfButton)) p->nsfbit |= 1 << 5;

    // Disable simultaneous input.
    if ((p->nsfbit&((1 << 2) | (1 << 3))) == ((1 << 2) | (1 << 3)))
    {
        p->nsfbit &= ~((1 << 2) | (1 << 3));
    }
    if ((p->nsfbit&((1 << 4) | (1 << 5))) == ((1 << 4) | (1 << 5)))
    {
        p->nsfbit &= ~((1 << 4) | (1 << 5));
    }
}

MM_EXPORT_EMU void mmEmuPad_JoypadBitSetData(struct mmEmuPad* p, int n, mmWord_t data)
{
    assert(0 <= n && n < 4 && "Joypad index n = [0, 4)");
    p->nManualPadbit[n] = data;
}
MM_EXPORT_EMU mmWord_t mmEmuPad_JoypadBitGetData(const struct mmEmuPad* p, int n)
{
    assert(0 <= n && n < 4 && "Joypad index n = [0, 4)");
    return p->nManualPadbit[n];
}
MM_EXPORT_EMU void mmEmuPad_JoypadBitPressed(struct mmEmuPad* p, int n, int hId)
{
    assert(0 <= n && n < 4 && "Joypad index n = [0, 4)");
    p->nManualPadbit[n] |= 1 << hId;
}
MM_EXPORT_EMU void mmEmuPad_JoypadBitRelease(struct mmEmuPad* p, int n, int hId)
{
    assert(0 <= n && n < 4 && "Joypad index n = [0, 4)");
    p->nManualPadbit[n] &= ~(1 << hId);
}
//
MM_EXPORT_EMU void mmEmuPad_NsfBitSetData(struct mmEmuPad* p, mmByte_t data)
{
    p->nManualNsfbit = data;
}
MM_EXPORT_EMU mmByte_t mmEmuPad_NsfBitGetData(const struct mmEmuPad* p)
{
    return p->nManualNsfbit;
}
MM_EXPORT_EMU void mmEmuPad_NsfBitPressed(struct mmEmuPad* p, int hId)
{
    p->nManualNsfbit |= 1 << hId;
}
MM_EXPORT_EMU void mmEmuPad_NsfBitRelease(struct mmEmuPad* p, int hId)
{
    p->nManualNsfbit &= ~(1 << hId);
}

