/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper071.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper071_Init(struct mmEmuMapper071* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper071_Reset;
    p->super.Write = &mmEmuMapper071_Write;
    //p->super.Read = &mmEmuMapper071_Read;
    //p->super.ReadLow = &mmEmuMapper071_ReadLow;
    p->super.WriteLow = &mmEmuMapper071_WriteLow;
    //p->super.ExRead = &mmEmuMapper071_ExRead;
    //p->super.ExWrite = &mmEmuMapper071_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper071_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper071_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper071_HSync;
    //p->super.VSync = &mmEmuMapper071_VSync;
    //p->super.Clock = &mmEmuMapper071_Clock;
    //p->super.PPULatch = &mmEmuMapper071_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper071_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper071_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper071_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper071_IsStateSave;
    //p->super.SaveState = &mmEmuMapper071_SaveState;
    //p->super.LoadState = &mmEmuMapper071_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper071_Destroy(struct mmEmuMapper071* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper071_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper071* p = (struct mmEmuMapper071*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper071_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper071* p = (struct mmEmuMapper071*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xE000) == 0x6000)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper071_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper071* p = (struct mmEmuMapper071*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF000)
    {
    case 0x9000:
        if (data & 0x10)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        break;

    case 0xC000:
    case 0xD000:
    case 0xE000:
    case 0xF000:
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        break;
    default:
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper071_Produce(void)
{
    struct mmEmuMapper071* p = (struct mmEmuMapper071*)mmMalloc(sizeof(struct mmEmuMapper071));
    mmEmuMapper071_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper071_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper071* p = (struct mmEmuMapper071*)(m);
    mmEmuMapper071_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER071 =
{
    &__static_mmEmuMapper071_Produce,
    &__static_mmEmuMapper071_Recycle,
};
