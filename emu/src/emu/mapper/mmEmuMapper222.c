/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper222.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper222_Init(struct mmEmuMapper222* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper222_Reset;
    p->super.Write = &mmEmuMapper222_Write;
    //p->super.Read = &mmEmuMapper222_Read;
    //p->super.ReadLow = &mmEmuMapper222_ReadLow;
    //p->super.WriteLow = &mmEmuMapper222_WriteLow;
    //p->super.ExRead = &mmEmuMapper222_ExRead;
    //p->super.ExWrite = &mmEmuMapper222_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper222_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper222_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper222_HSync;
    //p->super.VSync = &mmEmuMapper222_VSync;
    //p->super.Clock = &mmEmuMapper222_Clock;
    //p->super.PPULatch = &mmEmuMapper222_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper222_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper222_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper222_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper222_IsStateSave;
    //p->super.SaveState = &mmEmuMapper222_SaveState;
    //p->super.LoadState = &mmEmuMapper222_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper222_Destroy(struct mmEmuMapper222* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper222_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper222* p = (struct mmEmuMapper222*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
}
MM_EXPORT_EMU void mmEmuMapper222_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper222* p = (struct mmEmuMapper222*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF003)
    {
    case 0x8000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0xB000:
        mmEmuMmu_SetVROM1KBank(mmu, 0, data);
        break;
    case 0xB002:
        mmEmuMmu_SetVROM1KBank(mmu, 1, data);
        break;
    case 0xC000:
        mmEmuMmu_SetVROM1KBank(mmu, 2, data);
        break;
    case 0xC002:
        mmEmuMmu_SetVROM1KBank(mmu, 3, data);
        break;
    case 0xD000:
        mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        break;
    case 0xD002:
        mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        break;
    case 0xE000:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        break;
    case 0xE002:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        break;
    default:
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper222_Produce(void)
{
    struct mmEmuMapper222* p = (struct mmEmuMapper222*)mmMalloc(sizeof(struct mmEmuMapper222));
    mmEmuMapper222_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper222_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper222* p = (struct mmEmuMapper222*)(m);
    mmEmuMapper222_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER222 =
{
    &__static_mmEmuMapper222_Produce,
    &__static_mmEmuMapper222_Recycle,
};
