/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper020_h__
#define __mmEmuMapper020_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

enum
{
    MM_EMU_MAPPER020_BLOCK_READY = 0,
    MM_EMU_MAPPER020_BLOCK_VOLUME_LABEL,
    MM_EMU_MAPPER020_BLOCK_FILE_AMOUNT,
    MM_EMU_MAPPER020_BLOCK_FILE_HEADER,
    MM_EMU_MAPPER020_BLOCK_FILE_DATA,
};
enum
{
    MM_EMU_MAPPER020_SIZE_VOLUME_LABEL = 56,
    MM_EMU_MAPPER020_SIZE_FILE_AMOUNT = 2,
    MM_EMU_MAPPER020_SIZE_FILE_HEADER = 16,
};
enum
{
    MM_EMU_MAPPER020_OFFSET_VOLUME_LABEL = 0,
    MM_EMU_MAPPER020_OFFSET_FILE_AMOUNT = 56,
    MM_EMU_MAPPER020_OFFSET_FILE_HEADER = 58,
    MM_EMU_MAPPER020_OFFSET_FILE_DATA = 74,
};

enum
{
    MM_EMU_MAPPER020_MECHANICAL_SOUND_BOOT = 0,
    MM_EMU_MAPPER020_MECHANICAL_SOUND_SEEKEND,
    MM_EMU_MAPPER020_MECHANICAL_SOUND_MOTOR_ON,
    MM_EMU_MAPPER020_MECHANICAL_SOUND_MOTOR_OFF,
    MM_EMU_MAPPER020_MECHANICAL_SOUND_ALLSTOP,
};

//----------------------------------------------------------------------//
// Mapper020  Nintendo Disk System(FDS)                                 //
//----------------------------------------------------------------------//
struct mmEmuMapper020
{
    struct mmEmuMapper super;

    mmBool_t    bDiskThrottle;
    mmInt_t     DiskThrottleTime;

    mmByte_t*   disk;
    mmByte_t*   disk_w;

    mmInt_t     irq_counter, irq_latch; // $4020-$4021
    mmByte_t    irq_enable, irq_repeat; // $4022
    mmByte_t    irq_occur;              // It is not 0 when IRQ occurs.
    mmByte_t    irq_transfer;           // Interrupt transfer flag.

    mmByte_t    disk_enable;            // Disk I/O enable
    mmByte_t    sound_enable;           // Sound I/O enable
    mmByte_t    RW_start;               // IRQ occurrence when reading and writing become available.
    mmByte_t    RW_mode;                // Read / write mode.
    mmByte_t    disk_motor_mode;        // Disk motor.
    mmByte_t    disk_eject;             // Insert / not insert a disk card.
    mmByte_t    drive_ready;            // Whether it is reading or writing.
    mmByte_t    drive_reset;            // Drive reset state.

    mmInt_t     block_point;
    mmInt_t     block_mode;
    mmInt_t     size_file_data;
    mmInt_t     file_amount;
    mmInt_t     point;
    mmByte_t    first_access;

    mmByte_t    disk_side;
    mmByte_t    disk_mount_count;

    mmByte_t    irq_type;

    // For mechanical sound
    mmByte_t    sound_startup_flag;
    mmInt_t     sound_startup_timer;
    mmInt_t     sound_seekend_timer;
};

MM_EXPORT_EMU void mmEmuMapper020_Init(struct mmEmuMapper020* p);
MM_EXPORT_EMU void mmEmuMapper020_Destroy(struct mmEmuMapper020* p);

MM_EXPORT_EMU void mmEmuMapper020_MechanicalSound(struct mmEmuMapper020* p, mmInt_t type);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper020_Reset(struct mmEmuMapper* super);

MM_EXPORT_EMU mmByte_t mmEmuMapper020_ExRead(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper020_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper020_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper020_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper020_Clock(struct mmEmuMapper* super, mmInt_t cycles);
MM_EXPORT_EMU void mmEmuMapper020_HSync(struct mmEmuMapper* super, mmInt_t scanline);
MM_EXPORT_EMU void mmEmuMapper020_VSync(struct mmEmuMapper* super);

MM_EXPORT_EMU mmByte_t mmEmuMapper020_ExCmdRead(struct mmEmuMapper* super, mmInt_t cmd);
MM_EXPORT_EMU void mmEmuMapper020_ExCmdWrite(struct mmEmuMapper* super, mmInt_t cmd, mmByte_t data);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper020_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper020_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper020_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER020;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper020_h__
