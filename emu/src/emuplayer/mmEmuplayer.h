/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuplayer_h__
#define __mmEmuplayer_h__

#include "core/mmCore.h"

#include "random/mmXoshiro.h"

#include "emu/mmEmuEmulator.h"

#include "mmEmuplayerProcessor.h"

#include "core/mmPrefix.h"

struct mmEmuplayer
{
    struct mmEmuplayerProcessor processor;
    struct mmEmuEmulator emulator;
};

extern void mmEmuplayer_Init(struct mmEmuplayer* p);
extern void mmEmuplayer_Destroy(struct mmEmuplayer* p);

extern void mmEmuplayer_Start(struct mmEmuplayer* p);
extern void mmEmuplayer_Interrupt(struct mmEmuplayer* p);
extern void mmEmuplayer_Shutdown(struct mmEmuplayer* p);
extern void mmEmuplayer_Join(struct mmEmuplayer* p);

#include "core/mmSuffix.h"

#endif//__mmEmuplayer_h__