/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper046.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper046_Init(struct mmEmuMapper046* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);

    //
    p->super.Reset = &mmEmuMapper046_Reset;
    p->super.Write = &mmEmuMapper046_Write;
    //p->super.Read = &mmEmuMapper046_Read;
    //p->super.ReadLow = &mmEmuMapper046_ReadLow;
    p->super.WriteLow = &mmEmuMapper046_WriteLow;
    //p->super.ExRead = &mmEmuMapper046_ExRead;
    //p->super.ExWrite = &mmEmuMapper046_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper046_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper046_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper046_HSync;
    //p->super.VSync = &mmEmuMapper046_VSync;
    //p->super.Clock = &mmEmuMapper046_Clock;
    //p->super.PPULatch = &mmEmuMapper046_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper046_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper046_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper046_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper046_IsStateSave;
    p->super.SaveState = &mmEmuMapper046_SaveState;
    p->super.LoadState = &mmEmuMapper046_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper046_Destroy(struct mmEmuMapper046* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);
}

MM_EXPORT_EMU void mmEmuMapper046_SetBank(struct mmEmuMapper046* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM8KBank(mmu, 4, p->reg[0] * 8 + p->reg[2] * 4 + 0);
    mmEmuMmu_SetPROM8KBank(mmu, 5, p->reg[0] * 8 + p->reg[2] * 4 + 1);
    mmEmuMmu_SetPROM8KBank(mmu, 6, p->reg[0] * 8 + p->reg[2] * 4 + 2);
    mmEmuMmu_SetPROM8KBank(mmu, 7, p->reg[0] * 8 + p->reg[2] * 4 + 3);

    mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[1] * 64 + p->reg[3] * 8 + 0);
    mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1] * 64 + p->reg[3] * 8 + 1);
    mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[1] * 64 + p->reg[3] * 8 + 2);
    mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[1] * 64 + p->reg[3] * 8 + 3);
    mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[1] * 64 + p->reg[3] * 8 + 4);
    mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[1] * 64 + p->reg[3] * 8 + 5);
    mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[1] * 64 + p->reg[3] * 8 + 6);
    mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[1] * 64 + p->reg[3] * 8 + 7);
}

MM_EXPORT_EMU void mmEmuMapper046_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper046* p = (struct mmEmuMapper046*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->reg[0] = 0;
    p->reg[1] = 0;
    p->reg[2] = 0;
    p->reg[3] = 0;

    mmEmuMapper046_SetBank(p);
    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
}
MM_EXPORT_EMU void mmEmuMapper046_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper046* p = (struct mmEmuMapper046*)(super);

    p->reg[0] = data & 0x0F;
    p->reg[1] = (data & 0xF0) >> 4;
    mmEmuMapper046_SetBank(p);
}
MM_EXPORT_EMU void mmEmuMapper046_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper046* p = (struct mmEmuMapper046*)(super);

    p->reg[2] = data & 0x01;
    p->reg[3] = (data & 0x70) >> 4;
    mmEmuMapper046_SetBank(p);
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper046_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper046_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper046* p = (struct mmEmuMapper046*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->reg[3];
}
MM_EXPORT_EMU void mmEmuMapper046_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper046* p = (struct mmEmuMapper046*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->reg[3] = buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper046_Produce(void)
{
    struct mmEmuMapper046* p = (struct mmEmuMapper046*)mmMalloc(sizeof(struct mmEmuMapper046));
    mmEmuMapper046_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper046_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper046* p = (struct mmEmuMapper046*)(m);
    mmEmuMapper046_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER046 =
{
    &__static_mmEmuMapper046_Produce,
    &__static_mmEmuMapper046_Recycle,
};
