/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMovie_h__
#define __mmEmuMovie_h__

#include "core/mmCore.h"

#include "mmEmuState.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuMovie;

struct mmEmuMovieCallback
{
    void(*MovieFinish)(struct mmEmuMovie* obj);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_EMU void mmEmuMovieCallback_Init(struct mmEmuMovieCallback* p);
MM_EXPORT_EMU void mmEmuMovieCallback_Destroy(struct mmEmuMovieCallback* p);


struct mmEmuMovieInfo
{
    mmWord_t wRecVersion;
    mmWord_t wVersion;
    mmSInt32_t dwRecordFrames;
    mmUInt32_t dwRecordTimes;
};

struct mmEmuNes;

struct mmEmuMovie
{
    struct mmEmuNes* nes;

    struct mmEmuMovieCallback callback;

    mmBool_t    m_bMoviePlay;
    mmBool_t    m_bMovieRec;
    mmWord_t    m_MovieVersion;

    FILE*       m_fpMovie;
    struct mmEmuMovieFileHdr  m_hedMovie;
    mmUInt32_t  m_MovieControl;
    mmSInt32_t  m_MovieStepTotal;
    mmSInt32_t  m_MovieStep;
};
MM_EXPORT_EMU void mmEmuMovie_Init(struct mmEmuMovie* p);
MM_EXPORT_EMU void mmEmuMovie_Destroy(struct mmEmuMovie* p);

MM_EXPORT_EMU void mmEmuMovie_SetParent(struct mmEmuMovie* p, struct mmEmuNes* parent);
MM_EXPORT_EMU void mmEmuMovie_SetCallback(struct mmEmuMovie* p, struct mmEmuMovieCallback* callback);

// 0:ERROR 1:CRC OK -1:CRC ERR
MM_EXPORT_EMU mmInt_t mmEmuMovie_IsMovieFile(const struct mmEmuMovie* p, const char* fname);

static mmInline mmBool_t mmEmuMovie_IsMoviePlay(const struct mmEmuMovie* p) { return p->m_bMoviePlay; }
static mmInline mmBool_t mmEmuMovie_IsMovieRec(const struct mmEmuMovie* p) { return p->m_bMovieRec; }
MM_EXPORT_EMU int mmEmuMovie_MoviePlay(struct mmEmuMovie* p, const char* fname);
MM_EXPORT_EMU int mmEmuMovie_MovieRec(struct mmEmuMovie* p, const char* fname);
MM_EXPORT_EMU int mmEmuMovie_MovieRecAppend(struct mmEmuMovie* p, const char* fname);
MM_EXPORT_EMU mmBool_t mmEmuMovie_MovieStop(struct mmEmuMovie* p);
// For Movie
MM_EXPORT_EMU void mmEmuMovie_Movie(struct mmEmuMovie* p);
MM_EXPORT_EMU void mmEmuMovie_GetMovieInfo(const struct mmEmuMovie* p, struct mmEmuMovieInfo* info);

#include "core/mmSuffix.h"

#endif//__mmEmuMovie_h__
