/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper091.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper091_Init(struct mmEmuMapper091* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper091_Reset;
    //p->super.Write = &mmEmuMapper091_Write;
    //p->super.Read = &mmEmuMapper091_Read;
    //p->super.ReadLow = &mmEmuMapper091_ReadLow;
    p->super.WriteLow = &mmEmuMapper091_WriteLow;
    //p->super.ExRead = &mmEmuMapper091_ExRead;
    //p->super.ExWrite = &mmEmuMapper091_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper091_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper091_ExCmdWrite;
    p->super.HSync = &mmEmuMapper091_HSync;
    //p->super.VSync = &mmEmuMapper091_VSync;
    //p->super.Clock = &mmEmuMapper091_Clock;
    //p->super.PPULatch = &mmEmuMapper091_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper091_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper091_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper091_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper091_IsStateSave;
    p->super.SaveState = &mmEmuMapper091_SaveState;
    p->super.LoadState = &mmEmuMapper091_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper091_Destroy(struct mmEmuMapper091* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper091_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper091* p = (struct mmEmuMapper091*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBankArray(mmu, 0, 0, 0, 0, 0, 0, 0, 0);
    }

    p->irq_enable = 0;
    p->irq_counter = 0;

    mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
}
MM_EXPORT_EMU void mmEmuMapper091_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper091* p = (struct mmEmuMapper091*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "$%04X:$%02X(%3d) L=%3d\n", addr, data, data, nes->GetScanline() );

    switch (addr & 0xF003)
    {
    case 0x6000:
    case 0x6001:
    case 0x6002:
    case 0x6003:
        mmEmuMmu_SetVROM2KBank(mmu, (addr & 0x03) * 2, data);
        break;

    case 0x7000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0x7001:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;

    case 0x7002:
        p->irq_enable = 0;
        p->irq_counter = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x7003:
        p->irq_enable = 1;
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper091_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper091* p = (struct mmEmuMapper091*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline < 240) && mmEmuPpu_IsDispON(&nes->ppu))
    {
        if (p->irq_enable)
        {
            p->irq_counter++;
        }
        if (p->irq_counter >= 8)
        {
            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper091_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper091_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper091* p = (struct mmEmuMapper091*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper091_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper091* p = (struct mmEmuMapper091*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper091_Produce(void)
{
    struct mmEmuMapper091* p = (struct mmEmuMapper091*)mmMalloc(sizeof(struct mmEmuMapper091));
    mmEmuMapper091_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper091_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper091* p = (struct mmEmuMapper091*)(m);
    mmEmuMapper091_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER091 =
{
    &__static_mmEmuMapper091_Produce,
    &__static_mmEmuMapper091_Recycle,
};
