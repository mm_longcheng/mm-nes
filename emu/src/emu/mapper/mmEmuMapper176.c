/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper176.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper176_Init(struct mmEmuMapper176* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg5000 = 0;
    p->reg5001 = 0;
    p->reg5010 = 0;
    p->reg5011 = 0;
    p->reg5013 = 0;
    p->reg5FF1 = 0;
    p->reg5FF2 = 0;
    p->we_sram = 0;
    p->SBW = 0;

    //
    p->super.Reset = &mmEmuMapper176_Reset;
    p->super.Write = &mmEmuMapper176_Write;
    //p->super.Read = &mmEmuMapper176_Read;
    //p->super.ReadLow = &mmEmuMapper176_ReadLow;
    p->super.WriteLow = &mmEmuMapper176_WriteLow;
    //p->super.ExRead = &mmEmuMapper176_ExRead;
    //p->super.ExWrite = &mmEmuMapper176_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper176_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper176_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper176_HSync;
    //p->super.VSync = &mmEmuMapper176_VSync;
    //p->super.Clock = &mmEmuMapper176_Clock;
    //p->super.PPULatch = &mmEmuMapper176_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper176_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper176_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper176_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper176_IsStateSave;
    p->super.SaveState = &mmEmuMapper176_SaveState;
    p->super.LoadState = &mmEmuMapper176_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper176_Destroy(struct mmEmuMapper176* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg5000 = 0;
    p->reg5001 = 0;
    p->reg5010 = 0;
    p->reg5011 = 0;
    p->reg5013 = 0;
    p->reg5FF1 = 0;
    p->reg5FF2 = 0;
    p->we_sram = 0;
    p->SBW = 0;
}

MM_EXPORT_EMU void mmEmuMapper176_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper176* p = (struct mmEmuMapper176*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmUInt32_t crc = 0;

    if (mmu->PROM_16K_SIZE > 32)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, (mmu->PROM_08K_SIZE / 2) - 2, (mmu->PROM_08K_SIZE / 2) - 1);    //For 1M byte Cartridge
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
    p->reg5000 = 0;
    p->reg5001 = 0;
    p->reg5010 = 0;
    p->reg5011 = 0;
    p->reg5013 = 0;
    p->reg5FF1 = 0;
    p->reg5FF2 = 0;
    p->we_sram = 0;
    p->SBW = 0;

    //crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);

    /*
    crc == 0x416C07A1       //[ES-1006] Meng Huan Zhi Xing IV (C) & WXN-Meng Huan Zhi Xing  o
    crc == 0x94782FBD       //[ES-1057] San Guo Zhi - Xiong Ba Tian Xia (C)
    crc == 0xF9863ADF       //[ES-1066] Xi Chu Ba Wang (C)
    crc == 0xB511C04B       //[ES-1071] San Xia Wu Yi (C)
    crc == 0x1923A8C5       //[ES-1087] Shui Hu Shen Shou (C) & WXN-Shui Hu Shen Shou(fix)  o
    crc == 0x095D8678       //[ES-0122] Shuang Yue Zhuan (C) --> fecumm-mapper176
    crc == 0x7696573A       //[ES-0122] Ya Te Lu Zhan Ji (C) --> fecumm-mapper176
    crc == 0x8f6ab5ac       //WXN-San Guo Zhong Lie Zhuan          o
    crc == 0x99051cb5       //WXN-Xiong Ba Tian Xia                o
    crc == 0xf29c8186       //WXN-Da Fu Weng 2 - Shang Hai Da Heng o
    crc == 0xc768098b       //WXN-San Xia Wu Yi                    o
    crc == 0x49f22159       //WXN-Chao Ji Da Fu Weng               o
    crc == 0xf354d847       //WXN-Ge Lan Di Ya                     o
    crc == 0x5ee2ef97       //WXN-Di Guo Shi Dai(fix)              o
    crc == 0x977d22c3       //WXN-Po Fu Cheng Zhou(fix)            o
    crc == 0xf1d803f3       //WXN-Xi Chu Ba Wang(fix)              o
    crc == 0x85dd49b6       //WXN-Kou Dai Jing(fix)                o
    crc == 0x97b82f53       //WXN-Bao Xiao San Guo(fix)            o
    crc == 0xce2ea530       //WXN-Chong Wu Fei Chui(fix)           o
    */
}
MM_EXPORT_EMU void mmEmuMapper176_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper176* p = (struct mmEmuMapper176*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //  DEBUGOUT("Address=%04X Data=%02X\n", addr&0xFFFF, data&0xFF );

    if (addr == 0xa000)
    {
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
    }
    if (addr == 0xa001)
    {
        p->we_sram = data & 0x03;
    }
}

MM_EXPORT_EMU void mmEmuMapper176_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper176* p = (struct mmEmuMapper176*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //  DEBUGOUT("Address=%04X Data=%02X\n", addr&0xFFFF, data&0xFF );

    switch (addr)
    {
    case 0x5000:
        p->reg5000 = data;
        break;
    case 0x5001:            //[ES-1006] Meng Huan Zhi Xing IV (C)
        p->reg5001 = data;
        if (p->SBW)
        {
            mmEmuMmu_SetPROM32KBank(mmu, p->reg5001);
        }
        break;
    case 0x5010:
        p->reg5010 = data;
        if (p->reg5010 == 0x24)
        {
            p->SBW = 1;
        }
        break;
    case 0x5011:
        p->reg5011 = data >> 1;
        if (p->SBW)
        {
            mmEmuMmu_SetPROM32KBank(mmu, p->reg5011);
        }
        break;
    case 0x5013:
        p->reg5013 = data;
        break;
    case 0x5ff1:
        p->reg5FF1 = data >> 1;
        mmEmuMmu_SetPROM32KBank(mmu, p->reg5FF1);
        break;
    case 0x5ff2:
        p->reg5FF2 = data;
        mmEmuMmu_SetVROM8KBank(mmu, p->reg5FF2);
        break;
    default:
        break;
    }

    if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;

        //if (p->we_sram == 0)
        //{
        //  mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
        //}
    }

}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper176_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper176_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper176* p = (struct mmEmuMapper176*)(super);

    buffer[0] = p->reg5000;
    buffer[1] = p->reg5001;
    buffer[2] = p->reg5010;
    buffer[3] = p->reg5011;
    buffer[4] = p->reg5013;
    buffer[5] = p->reg5FF1;
    buffer[6] = p->reg5FF2;
    buffer[7] = p->we_sram;
    buffer[8] = p->SBW;
}
MM_EXPORT_EMU void mmEmuMapper176_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper176* p = (struct mmEmuMapper176*)(super);

    p->reg5000 = buffer[0];
    p->reg5001 = buffer[1];
    p->reg5010 = buffer[2];
    p->reg5011 = buffer[3];
    p->reg5013 = buffer[4];
    p->reg5FF1 = buffer[5];
    p->reg5FF2 = buffer[6];
    p->we_sram = buffer[7];
    p->SBW = buffer[8];
}

static struct mmEmuMapper* __static_mmEmuMapper176_Produce(void)
{
    struct mmEmuMapper176* p = (struct mmEmuMapper176*)mmMalloc(sizeof(struct mmEmuMapper176));
    mmEmuMapper176_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper176_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper176* p = (struct mmEmuMapper176*)(m);
    mmEmuMapper176_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER176 =
{
    &__static_mmEmuMapper176_Produce,
    &__static_mmEmuMapper176_Recycle,
};
