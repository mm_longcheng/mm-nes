/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper082.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper082_Init(struct mmEmuMapper082* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0;

    //
    p->super.Reset = &mmEmuMapper082_Reset;
    //p->super.Write = &mmEmuMapper082_Write;
    //p->super.Read = &mmEmuMapper082_Read;
    //p->super.ReadLow = &mmEmuMapper082_ReadLow;
    p->super.WriteLow = &mmEmuMapper082_WriteLow;
    //p->super.ExRead = &mmEmuMapper082_ExRead;
    //p->super.ExWrite = &mmEmuMapper082_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper082_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper082_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper082_HSync;
    //p->super.VSync = &mmEmuMapper082_VSync;
    //p->super.Clock = &mmEmuMapper082_Clock;
    //p->super.PPULatch = &mmEmuMapper082_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper082_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper082_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper082_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper082_IsStateSave;
    p->super.SaveState = &mmEmuMapper082_SaveState;
    p->super.LoadState = &mmEmuMapper082_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper082_Destroy(struct mmEmuMapper082* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0;
}

MM_EXPORT_EMU void mmEmuMapper082_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper082* p = (struct mmEmuMapper082*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->reg = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
}
MM_EXPORT_EMU void mmEmuMapper082_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper082* p = (struct mmEmuMapper082*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x7EF0:
        if (p->reg)
        {
            mmEmuMmu_SetVROM2KBank(mmu, 4, data >> 1);
        }
        else
        {
            mmEmuMmu_SetVROM2KBank(mmu, 0, data >> 1);
        }
        break;

    case 0x7EF1:
        if (p->reg)
        {
            mmEmuMmu_SetVROM2KBank(mmu, 6, data >> 1);
        }
        else
        {
            mmEmuMmu_SetVROM2KBank(mmu, 2, data >> 1);
        }
        break;

    case 0x7EF2:
        if (p->reg)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, data);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        }
        break;
    case 0x7EF3:
        if (p->reg)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 1, data);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        }
        break;
    case 0x7EF4:
        if (p->reg)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 2, data);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        }
        break;
    case 0x7EF5:
        if (p->reg)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 3, data);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        }
        break;

    case 0x7EF6:
        p->reg = data & 0x02;
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        break;

    case 0x7EFA:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data >> 2);
        break;
    case 0x7EFB:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data >> 2);
        break;
    case 0x7EFC:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data >> 2);
        break;
    default:
        mmEmuMapper_WriteLow(super, addr, data);
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper082_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper082_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper082* p = (struct mmEmuMapper082*)(super);

    buffer[0] = p->reg;
}
MM_EXPORT_EMU void mmEmuMapper082_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper082* p = (struct mmEmuMapper082*)(super);

    p->reg = buffer[0];
}

static struct mmEmuMapper* __static_mmEmuMapper082_Produce(void)
{
    struct mmEmuMapper082* p = (struct mmEmuMapper082*)mmMalloc(sizeof(struct mmEmuMapper082));
    mmEmuMapper082_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper082_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper082* p = (struct mmEmuMapper082*)(m);
    mmEmuMapper082_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER082 =
{
    &__static_mmEmuMapper082_Produce,
    &__static_mmEmuMapper082_Recycle,
};
