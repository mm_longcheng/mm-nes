/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper005_h__
#define __mmEmuMapper005_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper005  Nintendo MMC5                                             //
//----------------------------------------------------------------------//
struct mmEmuMapper005
{
    struct mmEmuMapper super;

    mmByte_t    sram_size;

    mmByte_t    prg_size;               // $5100
    mmByte_t    chr_size;               // $5101
    mmByte_t    sram_we_a, sram_we_b;   // $5102-$5103
    mmByte_t    graphic_mode;           // $5104
    mmByte_t    nametable_mode;         // $5105
    mmByte_t    nametable_type[4];      // $5105 use

    mmByte_t    sram_page;              // $5113

    mmByte_t    fill_chr, fill_pal;     // $5106-$5107
    mmByte_t    split_control;          // $5200
    mmByte_t    split_scroll;           // $5201
    mmByte_t    split_page;             // $5202

    mmByte_t    split_x;
    mmWord_t    split_addr;
    mmWord_t    split_yofs;

    mmByte_t    chr_type;
    mmByte_t    chr_mode;               // $5120-$512B use
    mmByte_t    chr_page[2][8];         // $5120-$512B
    mmByte_t*   BG_MEM_BANK[8];         // Bank for BG pattern.
    mmByte_t    BG_MEM_PAGE[8];

    mmByte_t    irq_status;             // $5204(R)
    mmByte_t    irq_enable;             // $5204(W)
    mmByte_t    irq_line;               // $5203
    mmByte_t    irq_scanline;
    mmByte_t    irq_clear;              // Used in HSync.
    mmByte_t    irq_type;

    mmByte_t    mult_a, mult_b;         // $5205-$5206
};

MM_EXPORT_EMU void mmEmuMapper005_Init(struct mmEmuMapper005* p);
MM_EXPORT_EMU void mmEmuMapper005_Destroy(struct mmEmuMapper005* p);

MM_EXPORT_EMU void mmEmuMapper005_SetBankCPU(struct mmEmuMapper005* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper005_SetBankSRAM(struct mmEmuMapper005* p, mmByte_t page, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper005_SetBankPPU(struct mmEmuMapper005* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper005_Reset(struct mmEmuMapper* super);

MM_EXPORT_EMU mmByte_t mmEmuMapper005_ReadLow(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper005_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper005_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper005_HSync(struct mmEmuMapper* super, mmInt_t scanline);

MM_EXPORT_EMU void mmEmuMapper005_PPUExtLatchX(struct mmEmuMapper* super, mmInt_t x);
MM_EXPORT_EMU void mmEmuMapper005_PPUExtLatch(struct mmEmuMapper* super, mmWord_t addr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper005_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper005_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper005_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER005;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper005_h__
