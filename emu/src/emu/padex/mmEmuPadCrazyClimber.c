/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadCrazyClimber.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuPad.h"
#include "emu/mmEmuController.h"

MM_EXPORT_EMU void mmEmuPadCrazyClimber_Init(struct mmEmuPadCrazyClimber* p)
{
    mmEmuExpad_Init(&p->super);

    p->bits = 0;

    //
    //p->super.Reset = &mm_emu_pad_crazyclimber_Reset;
    p->super.Strobe = &mmEmuPadCrazyClimber_Strobe;
    //p->super.Read4016 = &mm_emu_pad_crazyclimber_Read4016;
    //p->super.Read4017 = &mm_emu_pad_crazyclimber_Read4017;
    //p->super.Write4016 = &mm_emu_pad_crazyclimber_Write4016;
    //p->super.Write4017 = &mm_emu_pad_crazyclimber_Write4017;
    p->super.Sync = &mmEmuPadCrazyClimber_Sync;
    p->super.SetSyncData = &mmEmuPadCrazyClimber_SetSyncData;
    p->super.GetSyncData = &mmEmuPadCrazyClimber_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadCrazyClimber_Destroy(struct mmEmuPadCrazyClimber* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->bits = 0;
}

MM_EXPORT_EMU void mmEmuPadCrazyClimber_Strobe(struct mmEmuExpad* super)
{
    struct mmEmuPadCrazyClimber* p = (struct mmEmuPadCrazyClimber*)(super);

    struct mmEmuPad* pad = &p->super.nes->pad;

    pad->pad1bit &= ~0xF0;
    pad->pad1bit |= p->bits & 0xF0;
    pad->pad2bit &= ~0xF0;
    pad->pad2bit |= (p->bits & 0x0F) << 4;
}

MM_EXPORT_EMU void mmEmuPadCrazyClimber_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadCrazyClimber* p = (struct mmEmuPadCrazyClimber*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    p->bits = 0;

    // Left
    if (mmEmuController_ExButtonCheck(controller, 0, 0)) p->bits |= 1 << 6;
    if (mmEmuController_ExButtonCheck(controller, 0, 1)) p->bits |= 1 << 7;
    if (mmEmuController_ExButtonCheck(controller, 0, 2)) p->bits |= 1 << 5;
    if (mmEmuController_ExButtonCheck(controller, 0, 3)) p->bits |= 1 << 4;

    // Right
    if (mmEmuController_ExButtonCheck(controller, 0, 4)) p->bits |= 1 << 2;
    if (mmEmuController_ExButtonCheck(controller, 0, 5)) p->bits |= 1 << 3;
    if (mmEmuController_ExButtonCheck(controller, 0, 6)) p->bits |= 1 << 1;
    if (mmEmuController_ExButtonCheck(controller, 0, 7)) p->bits |= 1 << 0;

    // Disable simultaneous input.
    if ((p->bits&((1 << 0) | (1 << 1))) == ((1 << 0) | (1 << 1)))
    {
        p->bits &= ~((1 << 0) | (1 << 1));
    }
    if ((p->bits&((1 << 2) | (1 << 3))) == ((1 << 2) | (1 << 3)))
    {
        p->bits &= ~((1 << 2) | (1 << 3));
    }
    if ((p->bits&((1 << 4) | (1 << 5))) == ((1 << 4) | (1 << 5)))
    {
        p->bits &= ~((1 << 4) | (1 << 5));
    }
    if ((p->bits&((1 << 6) | (1 << 7))) == ((1 << 6) | (1 << 7)))
    {
        p->bits &= ~((1 << 6) | (1 << 7));
    }
}
MM_EXPORT_EMU void mmEmuPadCrazyClimber_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadCrazyClimber* p = (struct mmEmuPadCrazyClimber*)(super);

    p->bits = (mmByte_t)data;
}
MM_EXPORT_EMU mmLong_t mmEmuPadCrazyClimber_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadCrazyClimber* p = (struct mmEmuPadCrazyClimber*)(super);

    return p->bits;
}

static struct mmEmuExpad* __static_mmEmuPadCrazyClimber_Produce(void)
{
    struct mmEmuPadCrazyClimber* p = (struct mmEmuPadCrazyClimber*)mmMalloc(sizeof(struct mmEmuPadCrazyClimber));
    mmEmuPadCrazyClimber_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadCrazyClimber_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadCrazyClimber* p = (struct mmEmuPadCrazyClimber*)(m);
    mmEmuPadCrazyClimber_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_CRAZYCLIMBER =
{
    &__static_mmEmuPadCrazyClimber_Produce,
    &__static_mmEmuPadCrazyClimber_Recycle,
};
