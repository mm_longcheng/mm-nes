/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuCpu.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuMmu.h"
#include "emu/mmEmuMapper.h"
#include "emu/mmEmuErrorCode.h"

#include "core/mmLogger.h"


/*--------------[ DEFINE                ]-------------------------------*/
#define DPCM_SYNCCLOCK  MM_FALSE
/*--------------[ EXTERNAL PROGRAM      ]-------------------------------*/
/*--------------[ EXTERNAL WORK         ]-------------------------------*/
/*--------------[ WORK                  ]-------------------------------*/
/*--------------[ CONST                 ]-------------------------------*/
/*--------------[ PROTOTYPE             ]-------------------------------*/
/*--------------[ PROGRAM               ]-------------------------------*/


static void __static_CpuX6502_BeforeExecute(void* cpu)
{
#ifdef MM_EMNU_DISASSEMBLY_LOGGER
    struct mmEmuCpu* c = (struct mmEmuCpu*)(cpu);
    char buffer[128] = { 0 };
    mmUInt32_t line = 0;
    mmEmuX6502_LoggerEXEC(&c->x6502, &line, buffer);
    mmPrintf("%s\n", buffer);
#endif// MM_EMNU_DISASSEMBLY_LOGGER
}
//static mmByte_t __static_CpuX6502_OpcodeRdByte(void* cpu, mmWord_t addr)
//{
//    return ((struct mmEmuCpu*)(cpu))->MEM[addr >> 13][addr & 0x1FFF];
//}
//
static mmByte_t __static_CpuX6502_MemoryRdByte(void* cpu, mmWord_t addr)
{
    return mmEmuCpu_RD6502((struct mmEmuCpu*)cpu, addr);
}
static void __static_CpuX6502_MemoryWrByte(void* cpu, mmWord_t addr, mmByte_t data)
{
    mmEmuCpu_WR6502((struct mmEmuCpu*)cpu, addr, data);
}
//
static void __static_CpuX6502_StackPush(void* cpu, mmByte_t data)
{
    struct mmEmuCpu* c = (struct mmEmuCpu*)(cpu);
    c->STACK[(c->x6502.R.S--) & 0xFF] = data;
}
static mmByte_t __static_CpuX6502_StackPop(void* cpu)
{
    struct mmEmuCpu* c = (struct mmEmuCpu*)(cpu);
    return c->STACK[(++c->x6502.R.S) & 0xFF];
}

MM_EXPORT_EMU void mmEmuCpu_Init(struct mmEmuCpu* p)
{
    struct mmEmuX6502Callback callback;

    p->nes = NULL;

    mmEmuX6502_Init(&p->x6502);

    p->TOTAL_cycles = 0;
    p->DMA_cycles = 0;

    p->nmicount = 0;

    p->MEM = NULL;
    p->RAM = NULL;
    p->STACK = NULL;

    p->m_bClockProcess = MM_FALSE;
    //
    callback.BeforeExecute = &__static_CpuX6502_BeforeExecute;
    // callback.OpcodeRdByte = &__static_CpuX6502_OpcodeRdByte;
    callback.MemoryRdByte = &__static_CpuX6502_MemoryRdByte;
    callback.MemoryWrByte = &__static_CpuX6502_MemoryWrByte;
    callback.StackPush = &__static_CpuX6502_StackPush;
    callback.StackPop = &__static_CpuX6502_StackPop;
    mmEmuX6502_SetCallback(&p->x6502, &callback);
    mmEmuX6502_SetCPU(&p->x6502, p);
}
MM_EXPORT_EMU void mmEmuCpu_Destroy(struct mmEmuCpu* p)
{
    p->nes = NULL;

    mmEmuX6502_Destroy(&p->x6502);

    p->TOTAL_cycles = 0;
    p->DMA_cycles = 0;

    p->nmicount = 0;

    p->MEM = NULL;
    p->RAM = NULL;
    p->STACK = NULL;

    p->m_bClockProcess = MM_FALSE;
}

MM_EXPORT_EMU void mmEmuCpu_SetParent(struct mmEmuCpu* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU mmByte_t mmEmuCpu_RD6502(struct mmEmuCpu* p, mmWord_t addr)
{
    if (addr < 0x2000)
    {
        // RAM (Mirror $0800, $1000, $1800)
        // return p->nes->mmu.RAM[addr & 0x07FF];
        return p->RAM[addr & 0x07FF];
    }
    else if (addr < 0x8000)
    {
        // Others
        return mmEmuNes_Read(p->nes, addr);
    }
    else
    {
        // Dummy access
        // (*(p->nes->mapper->Read))(p->nes->mapper, addr, p->nes->mmu.CPU_MEM_BANK[addr >> 13][addr & 0x1FFF]);
        (*(p->nes->mapper->Read))(p->nes->mapper, addr, p->MEM[addr >> 13][addr & 0x1FFF]);
    }

    // Quick bank read
    // return p->nes->mmu.CPU_MEM_BANK[addr >> 13][addr & 0x1FFF];
    return p->MEM[addr >> 13][addr & 0x1FFF];
}
MM_EXPORT_EMU void mmEmuCpu_WR6502(struct mmEmuCpu* p, mmWord_t addr, mmByte_t data)
{
    if (addr < 0x2000)
    {
        // RAM (Mirror $0800, $1000, $1800)
        // p->nes->mmu.RAM[addr & 0x07FF] = data;
        p->RAM[addr & 0x07FF] = data;
    }
    else
    {
        // Others
        mmEmuNes_Write(p->nes, addr, data);
    }
}
MM_EXPORT_EMU mmWord_t mmEmuCpu_RD6502W(struct mmEmuCpu* p, mmWord_t addr)
{
    if (addr < 0x2000)
    {
        // RAM (Mirror $0800, $1000, $1800)
        // return *((mmWord_t*)&p->nes->mmu.RAM[addr & 0x07FF]);
        return *((mmWord_t*)&p->RAM[addr & 0x07FF]);
    }
    else if (addr < 0x8000)
    {
        // Others
        return (mmWord_t)mmEmuNes_Read(p->nes, addr) + (mmWord_t)mmEmuNes_Read(p->nes, addr + 1) * 0x100;
    }

    // Quick bank read
#if 0
    mmWord_t ret = 0;
    // ret |= (mmWord_t)p->nes->mmu.CPU_MEM_BANK[(addr + 0) >> 13][(addr + 0) & 0x1FFF];
    // ret |= (mmWord_t)p->nes->mmu.CPU_MEM_BANK[(addr + 1) >> 13][(addr + 1) & 0x1FFF] << 8;
    ret |= (mmWord_t)p->MEM[(addr + 0) >> 13][(addr + 0) & 0x1FFF];
    ret |= (mmWord_t)p->MEM[(addr + 1) >> 13][(addr + 1) & 0x1FFF] << 8;
    return ret;
#else
    // return *((mmWord_t*)&p->nes->mmu.CPU_MEM_BANK[addr >> 13][addr & 0x1FFF]);
    return *((mmWord_t*)&p->MEM[addr >> 13][addr & 0x1FFF]);
#endif
}

MM_EXPORT_EMU void mmEmuCpu_HardwareReset(struct mmEmuCpu* p)
{
    // Cache value. Memery quick access.
    p->MEM = p->nes->mmu.CPU_MEM_BANK;
    // Cache value. RAM quick access.
    p->RAM = p->nes->mmu.RAM;
    // Cache value. STACK quick access.
    p->STACK = &p->nes->mmu.RAM[0x0100];

    mmEmuX6502_SetMEM(&p->x6502, p->MEM);
    
    mmEmuX6502_HardwareReset(&p->x6502);

    p->TOTAL_cycles = 0;
    p->DMA_cycles = 0;

    p->nmicount = 0;
}
MM_EXPORT_EMU void mmEmuCpu_SoftwareReset(struct mmEmuCpu* p)
{
    mmEmuX6502_SoftwareReset(&p->x6502);
}

MM_EXPORT_EMU void mmEmuCpu_NMI(struct mmEmuCpu* p)
{
    p->x6502.R.pending |= MM_EMU_CPU_NMI_FLAG;
    p->nmicount = 0;
}
MM_EXPORT_EMU void mmEmuCpu_SetIRQ(struct mmEmuCpu* p, mmByte_t mask)
{
    p->x6502.R.pending |= mask;
}
MM_EXPORT_EMU void mmEmuCpu_ClrIRQ(struct mmEmuCpu* p, mmByte_t mask)
{
    p->x6502.R.pending &= ~mask;
}

MM_EXPORT_EMU void mmEmuCpu_DMA(struct mmEmuCpu* p, mmInt_t cycles)
{
    p->DMA_cycles += cycles;
}

//
// Instruction execution.
//
MM_EXPORT_EMU mmInt_t mmEmuCpu_EXEC(struct mmEmuCpu* p, mmInt_t request_cycles)
{
    int code = MM_SUCCESS;

    mmUInt64_t  OLD_cycles = p->TOTAL_cycles;
    mmInt_t exec_cycles;
    mmBool_t    bClockProcess = p->m_bClockProcess;

    struct mmEmuMapper* mapper = p->nes->mapper;
    
    while (request_cycles > 0)
    {
        exec_cycles = 0;

        if (p->DMA_cycles)
        {
            if (request_cycles <= p->DMA_cycles)
            {
                p->DMA_cycles -= request_cycles;
                p->TOTAL_cycles += request_cycles;

                // Clock synchronization processing.
                (*(mapper->Clock))(mapper, request_cycles);
#if DPCM_SYNCCLOCK
                mmEmuApu_SyncDPCM(&p->nes->apu, request_cycles);
#endif
                if (bClockProcess)
                {
                    mmEmuNes_Clock(p->nes, request_cycles);
                }
                break;
            }
            else
            {
                exec_cycles += p->DMA_cycles;
                p->DMA_cycles = 0;
            }
        }

        mmEmuX6502_EXEC(&p->x6502);

        if (1 == p->x6502.illegal_opcode)
        {
            if (!p->nes->config.emulator.bIllegalOp)
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmLogger_LogE(gLogger, "%s %d illegal opcode.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_ILLEGAL_OPCODE;
            }
        }

        if (MM_SUCCESS != code)
        {
            break;
        }

        exec_cycles = p->x6502.cycles;

        request_cycles -= exec_cycles;
        p->TOTAL_cycles += exec_cycles;

        // Clock synchronization processing.
        (*(mapper->Clock))(mapper, exec_cycles);
#if DPCM_SYNCCLOCK
        mmEmuApu_SyncDPCM(&p->nes->apu, exec_cycles);
#endif
        if (bClockProcess)
        {
            mmEmuNes_Clock(p->nes, exec_cycles);
        }
    }

#if !DPCM_SYNCCLOCK
    mmEmuApu_SyncDPCM(&p->nes->apu, (mmInt_t)(p->TOTAL_cycles - OLD_cycles));
#endif

    return (mmInt_t)(p->TOTAL_cycles - OLD_cycles);
}

MM_EXPORT_EMU mmInt_t mmEmuCpu_GetDmaCycles(const struct mmEmuCpu* p)
{
    return p->DMA_cycles;
}
MM_EXPORT_EMU void mmEmuCpu_SetDmaCycles(struct mmEmuCpu* p, mmInt_t cycles)
{
    p->DMA_cycles = cycles;
}

MM_EXPORT_EMU mmUInt64_t mmEmuCpu_GetTotalCycles(const struct mmEmuCpu* p)
{
    return p->TOTAL_cycles;
}
MM_EXPORT_EMU void mmEmuCpu_SetTotalCycles(struct mmEmuCpu* p, mmUInt64_t cycles)
{
    p->TOTAL_cycles = cycles;
}

