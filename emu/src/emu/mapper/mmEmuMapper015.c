/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper015.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper015_Init(struct mmEmuMapper015* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper015_Reset;
    p->super.Write = &mmEmuMapper015_Write;
    //p->super.Read = &mmEmuMapper015_Read;
    //p->super.ReadLow = &mmEmuMapper015_ReadLow;
    //p->super.WriteLow = &mmEmuMapper015_WriteLow;
    //p->super.ExRead = &mmEmuMapper015_ExRead;
    //p->super.ExWrite = &mmEmuMapper015_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper015_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper015_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper015_HSync;
    //p->super.VSync = &mmEmuMapper015_VSync;
    //p->super.Clock = &mmEmuMapper015_Clock;
    //p->super.PPULatch = &mmEmuMapper015_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper015_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper015_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper015_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper015_IsStateSave;
    //p->super.SaveState = &mmEmuMapper015_SaveState;
    //p->super.LoadState = &mmEmuMapper015_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper015_Destroy(struct mmEmuMapper015* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper015_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper015* p = (struct mmEmuMapper015*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
}
MM_EXPORT_EMU void mmEmuMapper015_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper015* p = (struct mmEmuMapper015*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        if (data & 0x80)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, (data & 0x3F) * 2 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x3F) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x3F) * 2 + 3);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x3F) * 2 + 2);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, (data & 0x3F) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x3F) * 2 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x3F) * 2 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x3F) * 2 + 3);
        }
        if (data & 0x40)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    case 0x8001:
        if (data & 0x80)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x3F) * 2 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x3F) * 2 + 0);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x3F) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x3F) * 2 + 1);
        }
        break;
    case 0x8002:
        if (data & 0x80)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, (data & 0x3F) * 2 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x3F) * 2 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x3F) * 2 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x3F) * 2 + 1);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, (data & 0x3F) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x3F) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x3F) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x3F) * 2 + 0);
        }
        break;
    case 0x8003:
        if (data & 0x80)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x3F) * 2 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x3F) * 2 + 0);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x3F) * 2 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x3F) * 2 + 1);
        }
        if (data & 0x40)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    default:
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper015_Produce(void)
{
    struct mmEmuMapper015* p = (struct mmEmuMapper015*)mmMalloc(sizeof(struct mmEmuMapper015));
    mmEmuMapper015_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper015_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper015* p = (struct mmEmuMapper015*)(m);
    mmEmuMapper015_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER015 =
{
    &__static_mmEmuMapper015_Produce,
    &__static_mmEmuMapper015_Recycle,
};
