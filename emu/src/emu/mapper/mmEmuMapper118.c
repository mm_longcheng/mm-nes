/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper118.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper118_Init(struct mmEmuMapper118* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->we_sram = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    //
    p->super.Reset = &mmEmuMapper118_Reset;
    p->super.Write = &mmEmuMapper118_Write;
    //p->super.Read = &mmEmuMapper118_Read;
    //p->super.ReadLow = &mmEmuMapper118_ReadLow;
    //p->super.WriteLow = &mmEmuMapper118_WriteLow;
    //p->super.ExRead = &mmEmuMapper118_ExRead;
    //p->super.ExWrite = &mmEmuMapper118_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper118_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper118_ExCmdWrite;
    p->super.HSync = &mmEmuMapper118_HSync;
    //p->super.VSync = &mmEmuMapper118_VSync;
    //p->super.Clock = &mmEmuMapper118_Clock;
    //p->super.PPULatch = &mmEmuMapper118_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper118_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper118_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper118_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper118_IsStateSave;
    p->super.SaveState = &mmEmuMapper118_SaveState;
    p->super.LoadState = &mmEmuMapper118_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper118_Destroy(struct mmEmuMapper118* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->we_sram = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}

MM_EXPORT_EMU void mmEmuMapper118_SetBankCPU(struct mmEmuMapper118* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, p->prg1, p->prg0, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper118_SetBankPPU(struct mmEmuMapper118* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr4, p->chr5, p->chr6, p->chr7,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1);
        }
        else
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1,
                p->chr4, p->chr5, p->chr6, p->chr7);
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper118_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper118* p = (struct mmEmuMapper118*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper118_SetBankCPU(p);

    if (mmu->VROM_1K_SIZE)
    {
        p->chr01 = 0;
        p->chr23 = 2;
        p->chr4 = 4;
        p->chr5 = 5;
        p->chr6 = 6;
        p->chr7 = 7;

        mmEmuMapper118_SetBankPPU(p);
    }
    else
    {
        p->chr01 = 0;
        p->chr23 = 0;
        p->chr4 = 0;
        p->chr5 = 0;
        p->chr6 = 0;
        p->chr7 = 0;
    }

    p->we_sram = 0; // Disable
    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0;
}
MM_EXPORT_EMU void mmEmuMapper118_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper118* p = (struct mmEmuMapper118*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper118_SetBankCPU(p);
        mmEmuMapper118_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;

        if ((p->reg[0] & 0x80))
        {
            if ((p->reg[0] & 0x07) == 2)
            {
                if (data & 0x80)
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
                }
                else
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
                }
            }
        }
        else
        {
            if ((p->reg[0] & 0x07) == 0)
            {
                if (data & 0x80)
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
                }
                else
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
                }
            }
        }

        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr01 = data & 0xFE;
                mmEmuMapper118_SetBankPPU(p);
            }
            break;
        case 0x01:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr23 = data & 0xFE;
                mmEmuMapper118_SetBankPPU(p);
            }
            break;
        case 0x02:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr4 = data;
                mmEmuMapper118_SetBankPPU(p);
            }
            break;
        case 0x03:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr5 = data;
                mmEmuMapper118_SetBankPPU(p);
            }
            break;
        case 0x04:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr6 = data;
                mmEmuMapper118_SetBankPPU(p);
            }
            break;
        case 0x05:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr7 = data;
                mmEmuMapper118_SetBankPPU(p);
            }
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper118_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper118_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;

    case 0xC000:
        p->reg[4] = data;
        p->irq_counter = data;
        break;
    case 0xC001:
        p->reg[5] = data;
        p->irq_latch = data;
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 1;
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper118_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper118* p = (struct mmEmuMapper118*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(p->irq_counter--))
                {
                    p->irq_counter = p->irq_latch;
                    //mm_emu_cpu_IRQ(&nes->cpu);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper118_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper118_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper118* p = (struct mmEmuMapper118*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
    buffer[16] = p->irq_enable;
    buffer[17] = p->irq_counter;
    buffer[18] = p->irq_latch;
}
MM_EXPORT_EMU void mmEmuMapper118_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper118* p = (struct mmEmuMapper118*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
    p->irq_enable = buffer[16];
    p->irq_counter = buffer[17];
    p->irq_latch = buffer[18];
}

static struct mmEmuMapper* __static_mmEmuMapper118_Produce(void)
{
    struct mmEmuMapper118* p = (struct mmEmuMapper118*)mmMalloc(sizeof(struct mmEmuMapper118));
    mmEmuMapper118_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper118_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper118* p = (struct mmEmuMapper118*)(m);
    mmEmuMapper118_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER118 =
{
    &__static_mmEmuMapper118_Produce,
    &__static_mmEmuMapper118_Recycle,
};
