/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuNesProfiler_h__
#define __mmEmuNesProfiler_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuNesProfiler
{
    mmByte_t    m_ProfileEnable;

    mmUInt32_t  m_dwTotalCycle;
    mmUInt32_t  m_dwTotalTempCycle;
    mmUInt32_t  m_dwProfileTotalCycle;
    mmUInt32_t  m_dwProfileTotalCount;
    mmUInt64_t  m_dwProfileCycle;
    mmUInt64_t  m_dwProfileTempCycle;
    mmUInt64_t  m_dwProfileAveCycle;
    mmUInt64_t  m_dwProfileMaxCycle;
};

MM_EXPORT_EMU void mmEmuNesProfiler_Init(struct mmEmuNesProfiler* p);
MM_EXPORT_EMU void mmEmuNesProfiler_Destroy(struct mmEmuNesProfiler* p);

MM_EXPORT_EMU void mmEmuNesProfiler_Reset(struct mmEmuNesProfiler* p);

#include "core/mmSuffix.h"

#endif//__mmEmuNesProfiler_h__
