/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuExpad.h"

MM_EXPORT_EMU void mmEmuExpad_Init(struct mmEmuExpad* p)
{
    p->nes = NULL;

    p->Reset = &mmEmuExpad_Reset;
    p->Strobe = &mmEmuExpad_Strobe;
    p->Read4016 = &mmEmuExpad_Read4016;
    p->Read4017 = &mmEmuExpad_Read4017;
    p->Write4016 = &mmEmuExpad_Write4016;
    p->Write4017 = &mmEmuExpad_Write4017;
    p->Sync = &mmEmuExpad_Sync;
    p->SetSyncData = &mmEmuExpad_SetSyncData;
    p->GetSyncData = &mmEmuExpad_GetSyncData;
}
MM_EXPORT_EMU void mmEmuExpad_Destroy(struct mmEmuExpad* p)
{
    p->nes = NULL;

    p->Reset = &mmEmuExpad_Reset;
    p->Strobe = &mmEmuExpad_Strobe;
    p->Read4016 = &mmEmuExpad_Read4016;
    p->Read4017 = &mmEmuExpad_Read4017;
    p->Write4016 = &mmEmuExpad_Write4016;
    p->Write4017 = &mmEmuExpad_Write4017;
    p->Sync = &mmEmuExpad_Sync;
    p->SetSyncData = &mmEmuExpad_SetSyncData;
    p->GetSyncData = &mmEmuExpad_GetSyncData;
}

MM_EXPORT_EMU void mmEmuExpad_SetParent(struct mmEmuExpad* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU void mmEmuExpad_Reset(struct mmEmuExpad* p)
{

}
MM_EXPORT_EMU void mmEmuExpad_Strobe(struct mmEmuExpad* p)
{

}
MM_EXPORT_EMU mmByte_t mmEmuExpad_Read4016(struct mmEmuExpad* p)
{
    return 0x00;
}
MM_EXPORT_EMU mmByte_t mmEmuExpad_Read4017(struct mmEmuExpad* p)
{
    return 0x00;
}
MM_EXPORT_EMU void mmEmuExpad_Write4016(struct mmEmuExpad* p, mmByte_t data)
{

}
MM_EXPORT_EMU void mmEmuExpad_Write4017(struct mmEmuExpad* p, mmByte_t data)
{

}
MM_EXPORT_EMU void mmEmuExpad_Sync(struct mmEmuExpad* p)
{

}
MM_EXPORT_EMU void mmEmuExpad_SetSyncData(struct mmEmuExpad* p, mmInt_t type, mmLong_t data)
{

}
MM_EXPORT_EMU mmLong_t mmEmuExpad_GetSyncData(const struct mmEmuExpad* p, mmInt_t type)
{
    return 0x00;
}

MM_EXPORT_EMU void mmEmuExpadFactory_Init(struct mmEmuExpadFactory* p)
{
    mmRbtreeU32Vpt_Init(&p->rbtree);
}
MM_EXPORT_EMU void mmEmuExpadFactory_Destroy(struct mmEmuExpadFactory* p)
{
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
}
MM_EXPORT_EMU void mmEmuExpadFactory_Register(struct mmEmuExpadFactory* p, mmUInt32_t hId, const struct mmEmuExpadCreator* creator)
{
    mmRbtreeU32Vpt_Set(&p->rbtree, hId, (void*)creator);
}
MM_EXPORT_EMU void mmEmuExpadFactory_Unregister(struct mmEmuExpadFactory* p, mmUInt32_t hId)
{
    mmRbtreeU32Vpt_Rmv(&p->rbtree, hId);
}
MM_EXPORT_EMU void mmEmuExpadFactory_Clear(struct mmEmuExpadFactory* p)
{
    mmRbtreeU32Vpt_Clear(&p->rbtree);
}

MM_EXPORT_EMU struct mmEmuExpad* mmEmuExpadFactory_Produce(struct mmEmuExpadFactory* p, mmUInt32_t hId)
{
    struct mmEmuExpad* m = NULL;
    struct mmEmuExpadCreator* creator = NULL;
    creator = (struct mmEmuExpadCreator*)mmRbtreeU32Vpt_Get(&p->rbtree, hId);
    if (NULL != creator)
    {
        m = (*(creator->Produce))();
    }
    return m;
}
MM_EXPORT_EMU void mmEmuExpadFactory_Recycle(struct mmEmuExpadFactory* p, mmUInt32_t hId, struct mmEmuExpad* m)
{
    if (NULL != m)
    {
        struct mmEmuExpadCreator* creator = NULL;
        creator = (struct mmEmuExpadCreator*)mmRbtreeU32Vpt_Get(&p->rbtree, hId);
        if (NULL != creator)
        {
            (*(creator->Recycle))(m);
        }
    }
}

