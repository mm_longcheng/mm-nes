/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper032.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper032_Init(struct mmEmuMapper032* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;
    p->reg = 0;

    //
    p->super.Reset = &mmEmuMapper032_Reset;
    p->super.Write = &mmEmuMapper032_Write;
    //p->super.Read = &mmEmuMapper032_Read;
    //p->super.ReadLow = &mmEmuMapper032_ReadLow;
    //p->super.WriteLow = &mmEmuMapper032_WriteLow;
    //p->super.ExRead = &mmEmuMapper032_ExRead;
    //p->super.ExWrite = &mmEmuMapper032_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper032_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper032_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper032_HSync;
    //p->super.VSync = &mmEmuMapper032_VSync;
    //p->super.Clock = &mmEmuMapper032_Clock;
    //p->super.PPULatch = &mmEmuMapper032_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper032_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper032_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper032_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper032_IsStateSave;
    p->super.SaveState = &mmEmuMapper032_SaveState;
    p->super.LoadState = &mmEmuMapper032_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper032_Destroy(struct mmEmuMapper032* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;
    p->reg = 0;
}

MM_EXPORT_EMU void mmEmuMapper032_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper032* p = (struct mmEmuMapper032*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;
    p->reg = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xc0fed437)      // For Major League(J)
    {
        p->patch = 1;
    }

    if (crc == 0xfd3fc292)      // For Ai Sensei no Oshiete - Watashi no Hoshi(J)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 30, 31, 30, 31);
    }
}
MM_EXPORT_EMU void mmEmuMapper032_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper032* p = (struct mmEmuMapper032*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF000)
    {
    case 0x8000:
        if (p->reg & 0x02)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        }
        break;

    case 0x9000:
        p->reg = data;
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;

    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    default:
        break;
    }

    switch (addr & 0xF007)
    {
    case 0xB000:
    case 0xB001:
    case 0xB002:
    case 0xB003:
    case 0xB004:
    case 0xB005:
        mmEmuMmu_SetVROM1KBank(mmu, addr & 0x0007, data);
        break;
    case 0xB006:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data);

        if (p->patch && (data & 0x40))
        {
            mmEmuMmu_SetVRAMMirrorArray(mmu, 0, 0, 0, 1);
        }
        break;
    case 0xB007:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data);

        if (p->patch && (data & 0x40))
        {
            mmEmuMmu_SetVRAMMirrorArray(mmu, 0, 0, 0, 0);
        }
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper032_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper032_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper032* p = (struct mmEmuMapper032*)(super);

    buffer[0] = p->reg;
}
MM_EXPORT_EMU void mmEmuMapper032_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper032* p = (struct mmEmuMapper032*)(super);

    p->reg = buffer[0];
}

static struct mmEmuMapper* __static_mmEmuMapper032_Produce(void)
{
    struct mmEmuMapper032* p = (struct mmEmuMapper032*)mmMalloc(sizeof(struct mmEmuMapper032));
    mmEmuMapper032_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper032_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper032* p = (struct mmEmuMapper032*)(m);
    mmEmuMapper032_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER032 =
{
    &__static_mmEmuMapper032_Produce,
    &__static_mmEmuMapper032_Recycle,
};
