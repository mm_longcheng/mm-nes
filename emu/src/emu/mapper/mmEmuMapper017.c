/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper017.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper017_Init(struct mmEmuMapper017* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    //
    p->super.Reset = &mmEmuMapper017_Reset;
    //p->super.Write = &mmEmuMapper017_Write;
    //p->super.Read = &mmEmuMapper017_Read;
    //p->super.ReadLow = &mmEmuMapper017_ReadLow;
    p->super.WriteLow = &mmEmuMapper017_WriteLow;
    //p->super.ExRead = &mmEmuMapper017_ExRead;
    //p->super.ExWrite = &mmEmuMapper017_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper017_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper017_ExCmdWrite;
    p->super.HSync = &mmEmuMapper017_HSync;
    //p->super.VSync = &mmEmuMapper017_VSync;
    //p->super.Clock = &mmEmuMapper017_Clock;
    //p->super.PPULatch = &mmEmuMapper017_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper017_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper017_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper017_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper017_IsStateSave;
    p->super.SaveState = &mmEmuMapper017_SaveState;
    p->super.LoadState = &mmEmuMapper017_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper017_Destroy(struct mmEmuMapper017* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}

MM_EXPORT_EMU void mmEmuMapper017_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper017* p = (struct mmEmuMapper017*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}
MM_EXPORT_EMU void mmEmuMapper017_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper017* p = (struct mmEmuMapper017*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x42FE:
        if (data & 0x10)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        break;
    case 0x42FF:
        if (data & 0x10)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;

    case 0x4501:
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x4502:
        p->irq_latch = (p->irq_latch & 0xFF00) | data;
        break;
    case 0x4503:
        p->irq_latch = (p->irq_latch & 0x00FF) | ((mmInt_t)data << 8);
        p->irq_counter = p->irq_latch;
        p->irq_enable = 0xFF;
        break;

    case 0x4504:
    case 0x4505:
    case 0x4506:
    case 0x4507:
        mmEmuMmu_SetPROM8KBank(mmu, addr & 0x07, data);
        break;

    case 0x4510:
    case 0x4511:
    case 0x4512:
    case 0x4513:
    case 0x4514:
    case 0x4515:
    case 0x4516:
    case 0x4517:
        mmEmuMmu_SetVROM1KBank(mmu, addr & 0x07, data);
        break;

    default:
        mmEmuMapper_WriteLow(super, addr, data);
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper017_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper017* p = (struct mmEmuMapper017*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if (p->irq_counter >= 0xFFFF - 113)
        {
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);

            //mm_emu_cpu_IRQ(&nes->cpu);
            //p->irq_counter = 0;
            //p->irq_enable = 0;

            p->irq_counter &= 0xFFFF;
        }
        else
        {
            p->irq_counter += 113;
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper017_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper017_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper017* p = (struct mmEmuMapper017*)(super);

    buffer[0] = p->irq_enable;
    *(mmInt_t*)&buffer[1] = p->irq_counter;
    *(mmInt_t*)&buffer[5] = p->irq_latch;
}
MM_EXPORT_EMU void mmEmuMapper017_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper017* p = (struct mmEmuMapper017*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = *(mmInt_t*)&buffer[1];
    p->irq_latch = *(mmInt_t*)&buffer[5];
}

static struct mmEmuMapper* __static_mmEmuMapper017_Produce(void)
{
    struct mmEmuMapper017* p = (struct mmEmuMapper017*)mmMalloc(sizeof(struct mmEmuMapper017));
    mmEmuMapper017_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper017_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper017* p = (struct mmEmuMapper017*)(m);
    mmEmuMapper017_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER017 =
{
    &__static_mmEmuMapper017_Produce,
    &__static_mmEmuMapper017_Recycle,
};
