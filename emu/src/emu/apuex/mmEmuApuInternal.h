/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApuInternal_h__
#define __mmEmuApuInternal_h__

#include "core/mmCore.h"
#include "core/mmTypes.h"

#include "emu/mmEmuApuInterface.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuNes;

struct mmEmuApuInternalRectangle
{
    mmByte_t    reg[4];     // register

    mmByte_t    enable;     // enable
    mmByte_t    holdnote;   // holdnote
    mmByte_t    volume;     // volume
    mmByte_t    complement;

    // For Render
    mmInt_t     phaseacc;
    mmInt_t     freq;
    mmInt_t     freqlimit;
    mmInt_t     adder;
    mmInt_t     duty;
    mmInt_t     len_count;

    mmInt_t     nowvolume;

    // For Envelope
    mmByte_t    env_fixed;
    mmByte_t    env_decay;
    mmByte_t    env_count;
    mmByte_t    dummy0;
    mmInt_t     env_vol;

    // For Sweep
    mmByte_t    swp_on;
    mmByte_t    swp_inc;
    mmByte_t    swp_shift;
    mmByte_t    swp_decay;
    mmByte_t    swp_count;
    mmByte_t    dummy1[3];

    // For sync;
    mmByte_t    sync_reg[4];
    mmByte_t    sync_output_enable;
    mmByte_t    sync_enable;
    mmByte_t    sync_holdnote;
    mmByte_t    dummy2;
    mmInt_t     sync_len_count;
};

struct mmEmuApuInternalTriangle
{
    mmByte_t    reg[4];

    mmByte_t    enable;
    mmByte_t    holdnote;
    mmByte_t    counter_start;
    mmByte_t    dummy0;

    mmInt_t     phaseacc;
    mmInt_t     freq;
    mmInt_t     len_count;
    mmInt_t     lin_count;
    mmInt_t     adder;

    mmInt_t     nowvolume;

    // For sync;
    mmByte_t    sync_reg[4];
    mmByte_t    sync_enable;
    mmByte_t    sync_holdnote;
    mmByte_t    sync_counter_start;
    // mmByte_t dummy1;
    mmInt_t     sync_len_count;
    mmInt_t     sync_lin_count;
};

struct mmEmuApuInternalNoise
{
    mmByte_t    reg[4];     // register

    mmByte_t    enable;     // enable
    mmByte_t    holdnote;   // holdnote
    mmByte_t    volume;     // volume
    mmByte_t    xor_tap;
    mmInt_t     shift_reg;

    // For Render
    mmInt_t     phaseacc;
    mmInt_t     freq;
    mmInt_t     len_count;

    mmInt_t     nowvolume;
    mmInt_t     output;

    // For Envelope
    mmByte_t    env_fixed;
    mmByte_t    env_decay;
    mmByte_t    env_count;
    mmByte_t    dummy0;
    mmInt_t     env_vol;

    // For sync;
    mmByte_t    sync_reg[4];
    mmByte_t    sync_output_enable;
    mmByte_t    sync_enable;
    mmByte_t    sync_holdnote;
    mmByte_t    dummy1;
    mmInt_t     sync_len_count;
};

struct mmEmuApuInternalDpcm
{
    mmByte_t    reg[4];
    mmByte_t    enable;
    mmByte_t    looping;
    mmByte_t    cur_byte;
    mmByte_t    dpcm_value;

    mmInt_t     freq;
    mmInt_t     phaseacc;
    mmInt_t     output;

    mmWord_t    address, cache_addr;
    mmInt_t     dmalength, cache_dmalength;
    mmInt_t     dpcm_output_real, dpcm_output_fake;
    mmInt_t     dpcm_output_old, dpcm_output_offset;

    // For sync
    mmByte_t    sync_reg[4];
    mmByte_t    sync_enable;
    mmByte_t    sync_looping;
    mmByte_t    sync_irq_gen;
    mmByte_t    sync_irq_enable;
    mmInt_t     sync_cycles, sync_cache_cycles;
    mmInt_t     sync_dmalength, sync_cache_dmalength;
};

// WaveTables
enum
{
    MM_EMU_APU_INTERNAL_TONEDATA_MAX = 16,
    MM_EMU_APU_INTERNAL_TONEDATA_LEN = 32,
};
enum
{
    MM_EMU_APU_INTERNAL_CHANNEL_MAX = 3,
    MM_EMU_APU_INTERNAL_TONE_MAX = 4,
};

struct mmEmuApuInternal
{
    struct mmEmuApuInterface super;
    //
    // Frame Counter
    mmLong_t    FrameCycle;
    mmInt_t     FrameCount;
    mmInt_t     FrameType;
    mmByte_t    FrameIRQ;
    mmByte_t    FrameIRQoccur;

    // Channels
    struct mmEmuApuInternalRectangle    ch0;
    struct mmEmuApuInternalRectangle    ch1;
    struct mmEmuApuInternalTriangle     ch2;
    struct mmEmuApuInternalNoise        ch3;
    struct mmEmuApuInternalDpcm         ch4;

    // $4015 Reg
    mmByte_t    reg4015, sync_reg4015;

    // Sound
    double      cpu_clock;
    mmInt_t     sampling_rate;
    mmInt_t     cycle_rate;

    mmBool_t    bToneTableEnable[MM_EMU_APU_INTERNAL_TONEDATA_MAX];
    mmInt_t     ToneTable[MM_EMU_APU_INTERNAL_TONEDATA_MAX][MM_EMU_APU_INTERNAL_TONEDATA_LEN];
    mmInt_t     ChannelTone[MM_EMU_APU_INTERNAL_CHANNEL_MAX][MM_EMU_APU_INTERNAL_TONE_MAX];

    mmBool_t    bChangeTone;
    mmBool_t    bDisableVolumeEffect;
};

MM_EXPORT_EMU void mmEmuApuInternal_Init(struct mmEmuApuInternal* p);
MM_EXPORT_EMU void mmEmuApuInternal_Destroy(struct mmEmuApuInternal* p);

MM_EXPORT_EMU void mmEmuApuInternal_ToneTableLoad(struct mmEmuApuInternal* p);
MM_EXPORT_EMU void mmEmuApuInternal_ToneTableAnalysis(struct mmEmuApuInternal* p, mmUInt8_t* buffer, size_t offset, size_t length);

MM_EXPORT_EMU void mmEmuApuInternal_SetChangeTone(struct mmEmuApuInternal* p, mmBool_t bChangeTone);
MM_EXPORT_EMU void mmEmuApuInternal_SetDisableVolumeEffect(struct mmEmuApuInternal* p, mmBool_t bDisableVolumeEffect);

MM_EXPORT_EMU void mmEmuApuInternal_GetFrameIRQ(struct mmEmuApuInternal* p, mmInt_t* cycle, mmByte_t* count, mmByte_t* type, mmByte_t* irq, mmByte_t* occur);
MM_EXPORT_EMU void mmEmuApuInternal_SetFrameIRQ(struct mmEmuApuInternal* p, mmInt_t cycle, mmByte_t count, mmByte_t type, mmByte_t irq, mmByte_t occur);

MM_EXPORT_EMU void mmEmuApuInternal_SyncWrite4017(struct mmEmuApuInternal* p, mmByte_t data);
MM_EXPORT_EMU void mmEmuApuInternal_UpdateFrame(struct mmEmuApuInternal* p);
// rectangle
MM_EXPORT_EMU void mmEmuApuInternal_WriteRectangle(struct mmEmuApuInternal* p, mmInt_t no, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuApuInternal_UpdateRectangle(struct mmEmuApuInternal* p, struct mmEmuApuInternalRectangle* ch, mmInt_t type);
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_RenderRectangle(struct mmEmuApuInternal* p, struct mmEmuApuInternalRectangle* ch);
// for sync
MM_EXPORT_EMU void mmEmuApuInternal_SyncWriteRectangle(struct mmEmuApuInternal* p, mmInt_t no, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuApuInternal_SyncUpdateRectangle(struct mmEmuApuInternal* p, struct mmEmuApuInternalRectangle* ch, mmInt_t type);

// triangle
MM_EXPORT_EMU void mmEmuApuInternal_WriteTriangle(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuApuInternal_UpdateTriangle(struct mmEmuApuInternal* p, mmInt_t type);
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_RenderTriangle(struct mmEmuApuInternal* p);
// for sync
MM_EXPORT_EMU void mmEmuApuInternal_SyncWriteTriangle(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuApuInternal_SyncUpdateTriangle(struct mmEmuApuInternal* p, mmInt_t type);

// noise
MM_EXPORT_EMU void mmEmuApuInternal_WriteNoise(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuApuInternal_UpdateNoise(struct mmEmuApuInternal* p, mmInt_t type);
MM_EXPORT_EMU mmByte_t mmEmuApuInternal_NoiseShiftreg(struct mmEmuApuInternal* p, mmByte_t xor_tap);
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_RenderNoise(struct mmEmuApuInternal* p);
// for sync
MM_EXPORT_EMU void mmEmuApuInternal_SyncWriteNoise(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuApuInternal_SyncUpdateNoise(struct mmEmuApuInternal* p, mmInt_t type);

// dpcm
MM_EXPORT_EMU void mmEmuApuInternal_WriteDPCM(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_RenderDPCM(struct mmEmuApuInternal* p);
// for sync
MM_EXPORT_EMU void mmEmuApuInternal_SyncWriteDPCM(struct mmEmuApuInternal* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmBool_t mmEmuApuInternal_SyncUpdateDPCM(struct mmEmuApuInternal* p, mmInt_t cycles);

// virtual function for super.
MM_EXPORT_EMU void mmEmuApuInternal_Reset(struct mmEmuApuInterface* super, double fclock, mmInt_t nrate);
MM_EXPORT_EMU void mmEmuApuInternal_Setup(struct mmEmuApuInterface* super, double fclock, mmInt_t nrate);
MM_EXPORT_EMU mmInt_t   mmEmuApuInternal_Process(struct mmEmuApuInterface* super, mmInt_t channel);
MM_EXPORT_EMU void mmEmuApuInternal_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuApuInternal_Read(struct mmEmuApuInterface* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuApuInternal_SyncWrite(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuApuInternal_SyncRead(struct mmEmuApuInterface* super, mmWord_t addr);
MM_EXPORT_EMU mmBool_t mmEmuApuInternal_Sync(struct mmEmuApuInterface* super, mmInt_t cycles);
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel);
MM_EXPORT_EMU mmInt_t mmEmuApuInternal_GetStateSize(const struct mmEmuApuInterface* super);
MM_EXPORT_EMU void mmEmuApuInternal_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuApuInternal_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer);

#include "core/mmSuffix.h"

#endif//__mmEmuApuInternal_h__
