/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmulatorMachine_h__
#define __mmEmulatorMachine_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "emu/mmEmuEmulator.h"
#include "emu/mmEmuPad.h"

#include "emulator/mmEmulatorAudio.h"
#include "emulator/mmEmulatorFrame.h"

#include "emulator/mmEmulatorAssets.h"

#include "emulator/mmEmulatorExport.h"


namespace mm
{
    /*!
    \brief
    EventArgs based class that is used for objects passed to input event handlers
    concerning emulator machine notify event.
    */
    class mmEmulatorMachineEventArgs : public CEGUI::WindowEventArgs
    {
    public:
        mmEmulatorMachineEventArgs(CEGUI::Window* wnd)
            : CEGUI::WindowEventArgs(wnd)
            , mid(0)
            , p0(0)
            , p1(0)
            , ps("")
        {

        }
        mmUInt32_t mid;

        mmUInt32_t p0;//!< holds current event p0 information.
        mmUInt32_t p1;//!< holds current event p1 information.
        
        std::string ps;//!< holds current event ps information.
    };

    class mmEventEmulatorArgs : public mmEventArgs
    {
    public:
        mmUInt32_t mid;

        mmUInt32_t p0;//!< holds current event p0 information.
        mmUInt32_t p1;//!< holds current event p1 information.

        std::string ps;//!< holds current event ps information.
    public:
        mmEventEmulatorArgs()
            : mid(0)
            , p0(0)
            , p1(0)
            , ps("")
        {

        }
        virtual ~mmEventEmulatorArgs()
        {

        }
    };
}

struct mmEmulatorEventMidName
{
    mmUInt32_t mid;
    const std::string* name;
};

// use for unknown mid.
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_UNKNOWN;

MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_EXIT;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_NONE;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_INITIAL;
//
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_PLAY;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_STOP;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_PAUSE;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_RESUME;
// Use the following in Event.
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_VIDEOMODE;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_LOADROM;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_SCREENCLEAR;
// For Logger
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_SETMESSAGESTRING;

MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_FULLSCREEN_GDI;
//
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_EMUPAUSE;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_ONEFRAME;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_THROTTLE;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_FRAMESKIP_AUTO;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_FRAMESKIP_UP;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_FRAMESKIP_DOWN;
//
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_HWRESET;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_SWRESET;
//
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_NETPLAY_START;
//
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_STATE_LOAD;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_STATE_SAVE;
//
// For Disk system
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_DISK_COMMAND;
// For ExController
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_EXCONTROLLER;
// For Sound
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_SOUND_MUTE;
//
// For Snapshot
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_SNAPSHOT;
// For Movie
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_MOVIE_PLAY;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_MOVIE_REC;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_MOVIE_RECAPPEND;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_MOVIE_STOP;
//
// For Wave recording
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_WAVEREC_START;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_WAVEREC_STOP;
// For Tape recording
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_TAPE_PLAY;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_TAPE_REC;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_TAPE_STOP;
//
// For Barcode
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_BARCODE;
//
// For TurboFile
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_TURBOFILE;
//
// For Debugger
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_DEBUG_RUN;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_DEBUG_BRAKE;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_DEBUG_STEP;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_RS_DEBUG_COMMAND;

MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_NT_NONE;

MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_NT_PLAY;
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_NT_STOP;
// For Screen
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_NT_SCREENMESSAGE;
// For Movie
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_NT_MOVIE_FINISH;
// For VideoMode
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_NT_VIDEOMODE;
// For ExController
MM_EXPORT_EMULATOR extern const std::string Event_MM_EMU_NT_EXCONTROLLER;

MM_EXPORT_EMULATOR extern const mmEmulatorEventMidName MM_EMULATOR_EVENT_NAME[];

struct mmEmulatorMachine
{
    struct mmEmuEmulatorProcessor super;

    // this member is event drive.
    mm::mmEventSet hEventSet;

    // weak ref.
    struct mmContextMaster* pContextMaster;
    // weak ref.
    struct mmFileContext* pFileContext;

    // strong ref. name pattern resource. default is "mm".
    std::string hName;
    // default is "emulator"
    std::string hDataPath;
    // default is "emulator", mmFileContext writable path + "/" + d_WritablePath.
    std::string hWritablePath;

    struct mmEmuEmulator hEmulator;

    struct mmEmulatorAudio hAudio;
    struct mmEmulatorFrame hFrame;

    struct mmEmulatorAssets hEmulatorAssets;
};
MM_EXPORT_EMULATOR void mmEmulatorMachine_Init(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_Destroy(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetContextMaster(struct mmEmulatorMachine* p, struct mmContextMaster* pContextMaster);
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetName(struct mmEmulatorMachine* p, const std::string& name);
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetMaterialPathPrefix(struct mmEmulatorMachine* p, const std::string& path);
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetRomPath(struct mmEmulatorMachine* p, const char* szRomPath);
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetDataPath(struct mmEmulatorMachine* p, const std::string& path);
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetWritablePath(struct mmEmulatorMachine* p, const std::string& path);
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetFileContext(struct mmEmulatorMachine* p, struct mmFileContext* _file_context);

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetTaskBackground(struct mmEmulatorMachine* p, mmUInt8_t background);

// assign assets by struct mmEmulatorAssets.
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetAssets(struct mmEmulatorMachine* p, struct mmEmulatorAssets* assets);

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetMute(struct mmEmulatorMachine* p, mmBool_t mute);
MM_EXPORT_EMULATOR mmBool_t mmEmulatorMachine_GetMute(struct mmEmulatorMachine* p);
//
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetExControllerType(struct mmEmulatorMachine* p, mmInt_t type);
MM_EXPORT_EMULATOR mmInt_t mmEmulatorMachine_GetExControllerType(struct mmEmulatorMachine* p);
//
MM_EXPORT_EMULATOR mmInt_t mmEmulatorMachine_GetVideoMode(struct mmEmulatorMachine* p);
// A Rapid speed, B Rapid speed
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetRapidSpeed(struct mmEmulatorMachine* p, mmInt_t no, mmWord_t speed[2]);
MM_EXPORT_EMULATOR void mmEmulatorMachine_GetRapidSpeed(struct mmEmulatorMachine* p, mmInt_t no, mmWord_t speed[2]);

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetResourceRoot(struct mmEmulatorMachine* p, mmUInt32_t type_assets, const char* root_path, const char* root_base);

MM_EXPORT_EMULATOR mmUInt32_t mmEmulatorMachine_GetRomLoadCode(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR struct mmFileContext* mmEmulatorMachine_GetAssetsFileContext(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR void mmEmulatorMachine_UpdateAssetsPath(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_FinishLaunching(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_BeforeTerminate(struct mmEmulatorMachine* p);

// NTSC/PAL/Dendy
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetVideoMode(struct mmEmulatorMachine* p, mmInt_t nMode);
// Load rom use event queue.
MM_EXPORT_EMULATOR void mmEmulatorMachine_LoadRom(struct mmEmulatorMachine* p);
// Screen clear.
MM_EXPORT_EMULATOR void mmEmulatorMachine_ScreenClear(struct mmEmulatorMachine* p);

// NTSC/PAL/Dendy
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetVideoModeImmediately(struct mmEmulatorMachine* p, mmInt_t nMode);
// Load rom Immediately, not use the event queue.
MM_EXPORT_EMULATOR void mmEmulatorMachine_LoadRomImmediately(struct mmEmulatorMachine* p);
// Screen clear Immediately, not use the event queue.
MM_EXPORT_EMULATOR void mmEmulatorMachine_ScreenClearImmediately(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR void mmEmulatorMachine_Play(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_Stop(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_Pause(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_Resume(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR mmInt_t mmEmulatorMachine_GetState(struct mmEmulatorMachine* p);

// event keyboard.
MM_EXPORT_EMULATOR void mmEmulatorMachine_KeyboardPressed(struct mmEmulatorMachine* p, int id);
MM_EXPORT_EMULATOR void mmEmulatorMachine_KeyboardRelease(struct mmEmulatorMachine* p, int id);
// event Mouse.
MM_EXPORT_EMULATOR void mmEmulatorMachine_MouseBegan(struct mmEmulatorMachine* p, mmLong_t x, mmLong_t y, int button_mask);
MM_EXPORT_EMULATOR void mmEmulatorMachine_MouseMoved(struct mmEmulatorMachine* p, mmLong_t x, mmLong_t y, int button_mask);
MM_EXPORT_EMULATOR void mmEmulatorMachine_MouseEnded(struct mmEmulatorMachine* p, mmLong_t x, mmLong_t y, int button_mask);
// event window.
MM_EXPORT_EMULATOR void mmEmulatorMachine_EnterBackground(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_EnterForeground(struct mmEmulatorMachine* p);

// For Manual bit controller Joypad.
MM_EXPORT_EMULATOR void mmEmulatorMachine_JoypadBitSetData(struct mmEmulatorMachine* p, int n, mmWord_t data);
MM_EXPORT_EMULATOR mmWord_t mmEmulatorMachine_JoypadBitGetData(struct mmEmulatorMachine* p, int n);
MM_EXPORT_EMULATOR void mmEmulatorMachine_JoypadBitPressed(struct mmEmulatorMachine* p, int n, int id);
MM_EXPORT_EMULATOR void mmEmulatorMachine_JoypadBitRelease(struct mmEmulatorMachine* p, int n, int id);
// For Manual bit controller Nsf.
MM_EXPORT_EMULATOR void mmEmulatorMachine_NsfBitSetData(struct mmEmulatorMachine* p, mmByte_t data);
MM_EXPORT_EMULATOR mmByte_t mmEmulatorMachine_NsfBitGetData(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_NsfBitPressed(struct mmEmulatorMachine* p, int id);
MM_EXPORT_EMULATOR void mmEmulatorMachine_NsfBitRelease(struct mmEmulatorMachine* p, int id);

MM_EXPORT_EMULATOR void mmEmulatorMachine_HardwareReset(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_SoftwareReset(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR void mmEmulatorMachine_StateLoad(struct mmEmulatorMachine* p, const char* szFName);
MM_EXPORT_EMULATOR void mmEmulatorMachine_StateSave(struct mmEmulatorMachine* p, const char* szFName);

MM_EXPORT_EMULATOR void mmEmulatorMachine_DiskCommand(struct mmEmulatorMachine* p, mmInt_t cmd);

MM_EXPORT_EMULATOR void mmEmulatorMachine_Snapshot(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR mmBool_t mmEmulatorMachine_IsMoviePlay(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR mmBool_t mmEmulatorMachine_IsMovieRec(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_MoviePlay(struct mmEmulatorMachine* p, const char* szFName);
MM_EXPORT_EMULATOR void mmEmulatorMachine_MovieRec(struct mmEmulatorMachine* p, const char* szFName);
MM_EXPORT_EMULATOR void mmEmulatorMachine_MovieRecAppend(struct mmEmulatorMachine* p, const char* szFName);
MM_EXPORT_EMULATOR void mmEmulatorMachine_MovieStop(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR void mmEmulatorMachine_WaveRecStart(struct mmEmulatorMachine* p, const char* szFName);
MM_EXPORT_EMULATOR void mmEmulatorMachine_WaveRecStop(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR void mmEmulatorMachine_TapePlay(struct mmEmulatorMachine* p, const char* szFName);
MM_EXPORT_EMULATOR void mmEmulatorMachine_TapeRec(struct mmEmulatorMachine* p, const char* szFName);
MM_EXPORT_EMULATOR void mmEmulatorMachine_TapeStop(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR void mmEmulatorMachine_SetBarcodeData(struct mmEmulatorMachine* p, mmByte_t* code, mmInt_t len);
MM_EXPORT_EMULATOR void mmEmulatorMachine_SetTurboFileBank(struct mmEmulatorMachine* p, mmInt_t bank);

// render thread.
MM_EXPORT_EMULATOR const std::string& mmEmulatorMachine_GetImageName(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR const std::string& mmEmulatorMachine_GetAliasTextureName0(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR const std::string& mmEmulatorMachine_GetAliasTextureName1(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR Ogre::MaterialPtr mmEmulatorMachine_GetScreenMaterial(struct mmEmulatorMachine* p);
// update renderer.
MM_EXPORT_EMULATOR void mmEmulatorMachine_UpdateFrameBitmap(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_UpdateRenderer(struct mmEmulatorMachine* p);

MM_EXPORT_EMULATOR void mmEmulatorMachine_Start(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_Interrupt(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_Shutdown(struct mmEmulatorMachine* p);
MM_EXPORT_EMULATOR void mmEmulatorMachine_Join(struct mmEmulatorMachine* p);

// virtual function for super.
MM_EXPORT_EMULATOR void mmEmulatorMachine_ThreadEnter(struct mmEmuEmulatorProcessor* super);
MM_EXPORT_EMULATOR void mmEmulatorMachine_ThreadLeave(struct mmEmuEmulatorProcessor* super);
MM_EXPORT_EMULATOR void mmEmulatorMachine_ProcessAudio(struct mmEmuEmulatorProcessor* super, mmByte_t* buffer, size_t length);
MM_EXPORT_EMULATOR void mmEmulatorMachine_ProcessFrame(struct mmEmuEmulatorProcessor* super, mmByte_t* buffer, size_t length);

#endif//__mm_emu_audio_h__
