/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmApplication_h__
#define __mmApplication_h__

#include "mmEmuplayer.h"

#include "core/mmCore.h"

#include "core/mmArgument.h"

#include "core/mmPrefix.h"

// default Argument.
extern const struct mmArgument gArgument;

struct mmApplication
{
    struct mmEmuplayer impl;
    struct mmArgument argument;
    // run state.1 is run 0 is stop.
    mmSInt8_t state;
};

extern struct mmApplication* mmApplication_Instance(void);

extern void mmApplication_Init(struct mmApplication* p);
extern void mmApplication_Destroy(struct mmApplication* p);

extern void mmApplication_Initialize(struct mmApplication* p, int argc, const char **argv);
extern void mmApplication_Start(struct mmApplication* p);
extern void mmApplication_Interrupt(struct mmApplication* p);
extern void mmApplication_Shutdown(struct mmApplication* p);
extern void mmApplication_Join(struct mmApplication* p);

#include "core/mmSuffix.h"

#endif//__mmApplication_h__
