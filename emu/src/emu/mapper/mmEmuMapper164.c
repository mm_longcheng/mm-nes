/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper164.h"
#include "emu/mmEmuNes.h"

#include "core/mmLogger.h"


MM_EXPORT_EMU void mmEmuMapper164_Init(struct mmEmuMapper164* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg5000 = 0;
    p->reg5100 = 0;
    p->a3 = 0;
    p->p_mode = 0;

    //
    p->super.Reset = &mmEmuMapper164_Reset;
    //p->super.Write = &mmEmuMapper164_Write;
    //p->super.Read = &mmEmuMapper164_Read;
    //p->super.ReadLow = &mmEmuMapper164_ReadLow;
    p->super.WriteLow = &mmEmuMapper164_WriteLow;
    //p->super.ExRead = &mmEmuMapper164_ExRead;
    //p->super.ExWrite = &mmEmuMapper164_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper164_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper164_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper164_HSync;
    //p->super.VSync = &mmEmuMapper164_VSync;
    //p->super.Clock = &mmEmuMapper164_Clock;
    //p->super.PPULatch = &mmEmuMapper164_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper164_PPUChrLatch;
    p->super.PPUExtLatchX = &mmEmuMapper164_PPUExtLatchX;
    p->super.PPUExtLatch = &mmEmuMapper164_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper164_IsStateSave;
    p->super.SaveState = &mmEmuMapper164_SaveState;
    p->super.LoadState = &mmEmuMapper164_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper164_Destroy(struct mmEmuMapper164* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg5000 = 0;
    p->reg5100 = 0;
    p->a3 = 0;
    p->p_mode = 0;
}

MM_EXPORT_EMU void mmEmuMapper164_SetBankCPU(struct mmEmuMapper164* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // struct mmLogger* gLogger = mmLogger_Instance();

    int mode, base, bank;

    base = (p->reg5100 & 1) << 5;
    mode = (p->reg5000 >> 4) & 0x07;

    switch (mode)
    {
    case 0:
    case 2:
    case 4:
    case 6:             /* NORMAL MODE */
        bank = (p->reg5000 & 0x0f);
        bank += (p->reg5000 & 0x20) >> 1;
        mmEmuMmu_SetPROM16KBank(mmu, 4, bank + base);
        mmEmuMmu_SetPROM16KBank(mmu, 6, base + 0x1f);
        // mmLogger_LogD(gLogger, "%s %d -- normal mode: mode=%d, bank=%d --", __FUNCTION__, __LINE__, mode, bank);
        break;
    case 1:
    case 3:             /* REG MODE */
        // mmLogger_LogD(gLogger, "%s %d -- reg mode --", __FUNCTION__, __LINE__);
        break;
    case 5:             /* 32K MODE */
        bank = (p->reg5000 & 0x0f);
        mmEmuMmu_SetPROM32KBank(mmu, bank + (base >> 1));
        //mmLogger_LogD(gLogger, "%s %d -- 32K MODE: bank=%02x --", __FUNCTION__, __LINE__, bank);
        break;
    case 7:             /* HALF MODE */
        bank = (p->reg5000 & 0x0f);
        bank += (bank & 0x08) << 1;
        mmEmuMmu_SetPROM16KBank(mmu, 4, bank + base);
        bank = (bank & 0x10) + 0x0f;
        mmEmuMmu_SetPROM16KBank(mmu, 6, base + 0x1f);
        // mmLogger_LogD(gLogger, "%s %d -- half mode --", __FUNCTION__, __LINE__);
        break;
    default:
        break;
    };
}
MM_EXPORT_EMU void mmEmuMapper164_SetBankPPU(struct mmEmuMapper164* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetCRAM8KBank(mmu, 0);
}

MM_EXPORT_EMU void mmEmuMapper164_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper164* p = (struct mmEmuMapper164*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    p->reg5000 = 0;
    p->reg5100 = 0;
    mmEmuMapper164_SetBankCPU(p);
    mmEmuMapper164_SetBankPPU(p);
    mmEmuPpu_SetExtLatchMode(&nes->ppu, MM_TRUE);
}
MM_EXPORT_EMU void mmEmuMapper164_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper164* p = (struct mmEmuMapper164*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // struct mmLogger* gLogger = mmLogger_Instance();

    if (addr == 0x5000)
    {
        p->p_mode = data >> 7;
        p->reg5000 = data;
        mmEmuMapper164_SetBankCPU(p);
        mmEmuMapper164_SetBankPPU(p);
    }
    else if (addr == 0x5100)
    {
        p->reg5100 = data;
        mmEmuMapper164_SetBankCPU(p);
        mmEmuMapper164_SetBankPPU(p);
    }
    else if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }
    else
    {
        // mmLogger_LogD(gLogger, "%s %d write to %04x:%02x", __FUNCTION__, __LINE__, addr, data);
    }
}

MM_EXPORT_EMU void mmEmuMapper164_PPUExtLatchX(struct mmEmuMapper* super, mmInt_t x)
{
    struct mmEmuMapper164* p = (struct mmEmuMapper164*)(super);

    p->a3 = (x & 1) << 3;
}
MM_EXPORT_EMU void mmEmuMapper164_PPUExtLatch(struct mmEmuMapper* super, mmWord_t addr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr)
{
    struct mmEmuMapper164* p = (struct mmEmuMapper164*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t loopy_v = mmEmuPpu_GetPPUADDR(&nes->ppu);
    mmInt_t loopy_y = mmEmuPpu_GetTILEY(&nes->ppu);
    mmInt_t     tileofs = (mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) << 8;
    mmInt_t     attradr = 0x23C0 + (loopy_v & 0x0C00) + ((loopy_v & 0x0380) >> 4);
    mmInt_t     attrsft = (addr & 0x0040) >> 4;
    mmByte_t*   pNTBL = mmu->PPU_MEM_BANK[addr >> 10];
    mmInt_t     ntbl_x = addr & 0x001F;
    mmInt_t     tileadr;

    attradr &= 0x3FF;
    (*attr) = ((pNTBL[attradr + (ntbl_x >> 2)] >> ((ntbl_x & 2) + attrsft)) & 3) << 2;
    tileadr = tileofs + pNTBL[addr & 0x03FF] * 0x10 + loopy_y;

    if (p->p_mode)
    {
        tileadr = (tileadr & 0xfff7) | p->a3;
        (*chr_l) = (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
    }
    else
    {
        (*chr_l) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
        (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
    }

}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper164_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper164_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper164* p = (struct mmEmuMapper164*)(super);

    buffer[0] = p->reg5000;
    buffer[1] = p->reg5100;
    buffer[2] = p->a3;
    buffer[3] = p->p_mode;
}
MM_EXPORT_EMU void mmEmuMapper164_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper164* p = (struct mmEmuMapper164*)(super);

    p->reg5000 = buffer[0];
    p->reg5100 = buffer[1];
    p->a3 = buffer[2];
    p->p_mode = buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper164_Produce(void)
{
    struct mmEmuMapper164* p = (struct mmEmuMapper164*)mmMalloc(sizeof(struct mmEmuMapper164));
    mmEmuMapper164_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper164_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper164* p = (struct mmEmuMapper164*)(m);
    mmEmuMapper164_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER164 =
{
    &__static_mmEmuMapper164_Produce,
    &__static_mmEmuMapper164_Recycle,
};
