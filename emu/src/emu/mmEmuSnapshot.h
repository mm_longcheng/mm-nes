/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuSnapshot_h__
#define __mmEmuSnapshot_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "mmEmuColor.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( push, 1 )
#endif

struct MM_ALIGNED(1) mmEmuSnapshotBmpBitmapFileHeader
{
    mmWord_t    bfType;
    mmUInt32_t  bfSize;
    mmWord_t    bfReserved1;
    mmWord_t    bfReserved2;
    mmUInt32_t  bfOffBits;
};
struct MM_ALIGNED(1) mmEmuSnapshotBmpBitmapInfoHeader
{
    mmUInt32_t     biSize;
    mmLong_t       biWidth;
    mmLong_t       biHeight;
    mmWord_t       biPlanes;
    mmWord_t       biBitCount;
    mmUInt32_t     biCompression;
    mmUInt32_t     biSizeImage;
    mmLong_t       biXPelsPerMeter;
    mmLong_t       biYPelsPerMeter;
    mmUInt32_t     biClrUsed;
    mmUInt32_t     biClrImportant;
};

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( pop )
#endif

enum
{
    MM_EMU_PNG_CHUNK_IHDR = 0x49484452,
    MM_EMU_PNG_CHUNK_PLTE = 0x504C5445,
    MM_EMU_PNG_CHUNK_IDAT = 0x49444154,
    MM_EMU_PNG_CHUNK_IEND = 0x49454E44,
};

struct mmEmuSnapshot
{
    struct mmString fname;
    mmInt_t nWidth;
    mmInt_t nHeight;
    struct mmEmuRGBQuad* pRGB;
    mmByte_t* lpBitmap;
    mmUInt32_t dwScan;
};

MM_EXPORT_EMU void mmEmuSnapshot_Init(struct mmEmuSnapshot* p);
MM_EXPORT_EMU void mmEmuSnapshot_destroy(struct mmEmuSnapshot* p);

MM_EXPORT_EMU void mmEmuSnapshot_SetFilename(struct mmEmuSnapshot* p, const char* fname);
static mmInline void mmEmuSnapshot_SetWidth(struct mmEmuSnapshot* p, mmInt_t nWidth) { p->nWidth = nWidth; }
static mmInline void mmEmuSnapshot_SetHeight(struct mmEmuSnapshot* p, mmInt_t nHeight) { p->nHeight = nHeight; }
static mmInline void mmEmuSnapshot_SetRGB(struct mmEmuSnapshot* p, struct mmEmuRGBQuad* pRGB) { p->pRGB = pRGB; }
static mmInline void mmEmuSnapshot_SetBitmap(struct mmEmuSnapshot* p, mmByte_t* lpBitmap) { p->lpBitmap = lpBitmap; }
static mmInline void mmEmuSnapshot_SetScan(struct mmEmuSnapshot* p, mmUInt32_t dwScan) { p->dwScan = dwScan; }

MM_EXPORT_EMU int mmEmuSnapshot_WritePng(struct mmEmuSnapshot* p);
MM_EXPORT_EMU int mmEmuSnapshot_WriteBmp(struct mmEmuSnapshot* p);

#include "core/mmSuffix.h"

#endif//__mmEmuSnapshot_h__
