/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper142.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper142_Init(struct mmEmuMapper142* p)
{
    mmEmuMapper_Init(&p->super);

    p->prg_sel = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper142_Reset;
    p->super.Write = &mmEmuMapper142_Write;
    //p->super.Read = &mmEmuMapper142_Read;
    //p->super.ReadLow = &mmEmuMapper142_ReadLow;
    //p->super.WriteLow = &mmEmuMapper142_WriteLow;
    //p->super.ExRead = &mmEmuMapper142_ExRead;
    //p->super.ExWrite = &mmEmuMapper142_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper142_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper142_ExCmdWrite;
    p->super.HSync = &mmEmuMapper142_HSync;
    //p->super.VSync = &mmEmuMapper142_VSync;
    //p->super.Clock = &mmEmuMapper142_Clock;
    //p->super.PPULatch = &mmEmuMapper142_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper142_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper142_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper142_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper142_IsStateSave;
    p->super.SaveState = &mmEmuMapper142_SaveState;
    p->super.LoadState = &mmEmuMapper142_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper142_Destroy(struct mmEmuMapper142* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->prg_sel = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper142_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper142* p = (struct mmEmuMapper142*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->prg_sel = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;

    mmEmuMmu_SetPROM8KBank(mmu, 3, 0);
    mmEmuMmu_SetPROM8KBank(mmu, 7, 0x0F);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper142_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper142* p = (struct mmEmuMapper142*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF000)
    {
    case 0x8000:
        p->irq_counter = (p->irq_counter & 0xFFF0) | ((data & 0x0F) << 0);
        break;
    case 0x9000:
        p->irq_counter = (p->irq_counter & 0xFF0F) | ((data & 0x0F) << 4);
        break;
    case 0xA000:
        p->irq_counter = (p->irq_counter & 0xF0FF) | ((data & 0x0F) << 8);
        break;
    case 0xB000:
        p->irq_counter = (p->irq_counter & 0x0FFF) | ((data & 0x0F) << 12);
        break;
    case 0xC000:
        p->irq_enable = data & 0x0F;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE000:
        p->prg_sel = data & 0x0F;
        break;
    case 0xF000:
        switch (p->prg_sel)
        {
        case 1:
            mmEmuMmu_SetPROM8KBank(mmu, 4, data & 0x0F);
            break;
        case 2:
            mmEmuMmu_SetPROM8KBank(mmu, 5, data & 0x0F);
            break;
        case 3:
            mmEmuMmu_SetPROM8KBank(mmu, 6, data & 0x0F);
            break;
        case 4:
            mmEmuMmu_SetPROM8KBank(mmu, 3, data & 0x0F);
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper142_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper142* p = (struct mmEmuMapper142*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if (p->irq_counter > (0xFFFF - 113))
        {
            p->irq_counter = 0;
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        else
        {
            p->irq_counter += 113;
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper142_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper142_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper142* p = (struct mmEmuMapper142*)(super);

    buffer[0] = p->prg_sel;
    buffer[0] = p->irq_enable;
    *(mmInt_t*)&buffer[2] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper142_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper142* p = (struct mmEmuMapper142*)(super);

    p->prg_sel = buffer[0];
    p->irq_enable = buffer[1];
    p->irq_counter = *(mmInt_t*)&buffer[2];
}

static struct mmEmuMapper* __static_mmEmuMapper142_Produce(void)
{
    struct mmEmuMapper142* p = (struct mmEmuMapper142*)mmMalloc(sizeof(struct mmEmuMapper142));
    mmEmuMapper142_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper142_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper142* p = (struct mmEmuMapper142*)(m);
    mmEmuMapper142_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER142 =
{
    &__static_mmEmuMapper142_Produce,
    &__static_mmEmuMapper142_Recycle,
};
