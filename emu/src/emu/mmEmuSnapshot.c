/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuSnapshot.h"
#include "mmEmuErrorCode.h"

#include "core/mmAlloc.h"

#include "zlib.h"

#ifndef BI_RGB
// bmp biCompression.
#define BI_RGB        0L
#endif//BI_RGB

static void __static_ConvertNetworkOrder(mmUInt32_t uData, mmByte_t* pBuf)
{
    pBuf[0] = (mmByte_t)(uData >> 24);
    pBuf[1] = (mmByte_t)(uData >> 16);
    pBuf[2] = (mmByte_t)(uData >> 8);
    pBuf[3] = (mmByte_t)(uData);
}

static mmBool_t __static_WriteSignature(FILE* fp)
{
    static const char PNG_SIGNATURE[] = { "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A" };

    return fwrite(PNG_SIGNATURE, 8, 1, fp) == 1;
}
static mmBool_t __static_WriteChunk(FILE* fp, mmUInt32_t uType, mmByte_t* pData, mmUInt32_t uLength)
{
    mmBool_t code = MM_FALSE;
    mmByte_t u32_buffer[4] = { 0 };
    mmUInt32_t crc = 0;

    do
    {
        // Write Length
        __static_ConvertNetworkOrder(uLength, u32_buffer);
        if (fwrite(u32_buffer, sizeof(u32_buffer), 1, fp) != 1)
        {
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        // Write Chunk Type
        __static_ConvertNetworkOrder(uType, u32_buffer);
        if (fwrite(u32_buffer, sizeof(u32_buffer), 1, fp) != 1)
        {
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        crc = (mmUInt32_t)crc32(0, u32_buffer, (uInt)sizeof(u32_buffer));
        if (uLength)
        {
            if (fwrite(pData, uLength, 1, fp) != 1)
            {
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            crc = (mmUInt32_t)crc32(crc, pData, (uInt)uLength);
        }

        // Write CRC32
        __static_ConvertNetworkOrder(crc, u32_buffer);
        if (fwrite(u32_buffer, sizeof(u32_buffer), 1, fp) != 1)
        {
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        code = MM_TRUE;
    } while (0);
    return code;
}

MM_EXPORT_EMU void mmEmuSnapshot_Init(struct mmEmuSnapshot* p)
{
    mmString_Init(&p->fname);
    p->nWidth = 0;
    p->nHeight = 0;
    p->pRGB = 0;
    p->lpBitmap = 0;
    p->dwScan = 0;
}
MM_EXPORT_EMU void mmEmuSnapshot_destroy(struct mmEmuSnapshot* p)
{
    mmString_Destroy(&p->fname);
    p->nWidth = 0;
    p->nHeight = 0;
    p->pRGB = 0;
    p->lpBitmap = 0;
    p->dwScan = 0;
}

MM_EXPORT_EMU void mmEmuSnapshot_SetFilename(struct mmEmuSnapshot* p, const char* fname)
{
    mmString_Assigns(&p->fname, fname);
}

MM_EXPORT_EMU int mmEmuSnapshot_WritePng(struct mmEmuSnapshot* p)
{
    int code = MM_UNKNOWN;

    FILE* fp = NULL;
    mmByte_t* pBits = NULL;
    mmByte_t* pZbuf = NULL;

    mmByte_t* pSrc = NULL;
    mmByte_t* pDst = NULL;

    unsigned long zlibbuffersize;
    mmInt_t i;

    pBits = (mmByte_t*)mmMalloc((p->nWidth + 1) * p->nHeight * sizeof(mmByte_t));

    zlibbuffersize = (unsigned long)(((p->nWidth + 1) * p->nHeight) * 1.1 + 12);
    pZbuf = (mmByte_t*)mmMalloc(zlibbuffersize);

    do
    {
        if (mmString_Empty(&p->fname))
        {
            // fname is empty, need do nothing.
            code = MM_FAILURE;
            break;
        }
        //
        fp = fopen(mmString_CStr(&p->fname), "wb");
        if (NULL == fp)
        {
            // fname is can not open, need do nothing.
            code = MM_FAILURE;
            break;
        }
        if (!__static_WriteSignature(fp))
        {
            // WriteSignature error.
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        pSrc = p->lpBitmap;
        pDst = pBits;

        // No Filter
        for (i = 0; i < p->nHeight; i++)
        {
            *(pDst++) = 0;
            mmMemcpy(pDst, pSrc, p->nWidth);
            pSrc += p->dwScan;
            pDst += p->nWidth;
        }

        if (compress(pZbuf, &zlibbuffersize, pBits, ((p->nWidth + 1) * p->nHeight)) != Z_OK)
        {
            // compress error.
            code = MM_ERR_EMU_FILE_COMPRESS;
            break;
        }

        // write IHDR
        {
            mmByte_t ihdr[13] = { 0 };

            // Write Length
            __static_ConvertNetworkOrder(p->nWidth, ihdr);
            __static_ConvertNetworkOrder(p->nHeight, ihdr + 4);
            *(ihdr + 8) = 8;    // 8bpp
            *(ihdr + 9) = 3;    // Indexed color
            *(ihdr + 10) = 0;   // Compression method
            *(ihdr + 11) = 0;   // Filter method
            *(ihdr + 12) = 0;   // Interace method

            if (!__static_WriteChunk(fp, MM_EMU_PNG_CHUNK_IHDR, ihdr, 13))
            {
                // WriteChunk error.
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
        }

        // write PLTE
        {
            mmByte_t plte[256 * 3] = { 0 };
            mmInt_t i = 0;
            for (i = 0; i < 256; i++)
            {
                plte[i * 3 + 0] = p->pRGB[i].rgbRed;
                plte[i * 3 + 1] = p->pRGB[i].rgbGreen;
                plte[i * 3 + 2] = p->pRGB[i].rgbBlue;
            }

            if (!__static_WriteChunk(fp, MM_EMU_PNG_CHUNK_PLTE, plte, 256 * 3))
            {
                // WriteChunk error.
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
        }

        // write IDAT
        if (!__static_WriteChunk(fp, MM_EMU_PNG_CHUNK_IDAT, pZbuf, (mmUInt32_t)zlibbuffersize))
        {
            // WriteChunk error.
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        // write IEND
        if (!__static_WriteChunk(fp, MM_EMU_PNG_CHUNK_IEND, NULL, 0))
        {
            // WriteChunk error.
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        code = MM_SUCCESS;
    } while (0);

    if (NULL != pBits)
    {
        mmFree(pBits);
        pBits = NULL;
    }
    if (NULL != pZbuf)
    {
        mmFree(pZbuf);
        pZbuf = NULL;
    }
    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    return code;
}
MM_EXPORT_EMU int mmEmuSnapshot_WriteBmp(struct mmEmuSnapshot* p)
{
    int code = MM_UNKNOWN;

    FILE* fp = NULL;
    mmInt_t i = 0;

    struct mmEmuSnapshotBmpBitmapFileHeader bfh;
    struct mmEmuSnapshotBmpBitmapInfoHeader bih;

    mmMemset(&bfh, 0, sizeof(struct mmEmuSnapshotBmpBitmapFileHeader));
    mmMemset(&bih, 0, sizeof(struct mmEmuSnapshotBmpBitmapInfoHeader));

    bfh.bfType = 0x4D42;    // 'BM'
    bfh.bfOffBits =
        sizeof(struct mmEmuSnapshotBmpBitmapFileHeader) +
        sizeof(struct mmEmuSnapshotBmpBitmapInfoHeader) +
        sizeof(struct mmEmuRGBQuad) * p->nWidth;
    bfh.bfSize = bfh.bfOffBits + p->nWidth * p->nHeight;

    bih.biSize = sizeof(bih);
    bih.biWidth = p->nWidth;
    bih.biHeight = p->nHeight;
    bih.biPlanes = 1;
    bih.biBitCount = 8;
    bih.biCompression = BI_RGB;
    bih.biSizeImage = 0;
    bih.biXPelsPerMeter = 0;
    bih.biYPelsPerMeter = 0;
    bih.biClrUsed = 256;
    bih.biClrImportant = 0;

    do
    {
        if (mmString_Empty(&p->fname))
        {
            // fname is empty, need do nothing.
            code = MM_FAILURE;
            break;
        }
        //
        fp = fopen(mmString_CStr(&p->fname), "wb");
        if (NULL == fp)
        {
            // fname is can not open, need do nothing.
            code = MM_ERR_EMU_FILE_NOT_EXIST;
            break;
        }

        if (fwrite(&bfh, sizeof(bfh), 1, fp) != 1)
        {
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }
        if (fwrite(&bih, sizeof(bih), 1, fp) != 1)
        {
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }
        if (fwrite(p->pRGB, sizeof(struct mmEmuRGBQuad) * 256, 1, fp) != 1)
        {
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        for (i = p->nHeight - 1; i >= 0; i--)
        {
            if (fwrite(&p->lpBitmap[(p->nWidth + 16)*i], p->nWidth, 1, fp) != 1)
            {
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
        }

        code = MM_SUCCESS;
    } while (0);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    return code;
}

