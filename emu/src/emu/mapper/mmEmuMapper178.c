/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper178.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper178_Init(struct mmEmuMapper178* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);
    p->banknum = 0;

    //
    p->super.Reset = &mmEmuMapper178_Reset;
    p->super.Write = &mmEmuMapper178_Write;
    //p->super.Read = &mmEmuMapper178_Read;
    //p->super.ReadLow = &mmEmuMapper178_ReadLow;
    p->super.WriteLow = &mmEmuMapper178_WriteLow;
    //p->super.ExRead = &mmEmuMapper178_ExRead;
    //p->super.ExWrite = &mmEmuMapper178_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper178_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper178_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper178_HSync;
    //p->super.VSync = &mmEmuMapper178_VSync;
    //p->super.Clock = &mmEmuMapper178_Clock;
    //p->super.PPULatch = &mmEmuMapper178_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper178_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper178_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper178_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper178_IsStateSave;
    p->super.SaveState = &mmEmuMapper178_SaveState;
    p->super.LoadState = &mmEmuMapper178_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper178_Destroy(struct mmEmuMapper178* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);
    p->banknum = 0;
}

MM_EXPORT_EMU void mmEmuMapper178_SetBankCPU(struct mmEmuMapper178* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->banknum = p->reg[0] + p->reg[1];
    mmEmuMmu_SetPROM32KBank(mmu, p->banknum);
}

MM_EXPORT_EMU void mmEmuMapper178_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper178* p = (struct mmEmuMapper178*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    p->reg[0] = 0;
    p->reg[1] = 0;
    p->reg[2] = 0;
    p->banknum = 0;
    mmEmuMapper178_SetBankCPU(p);
}
MM_EXPORT_EMU void mmEmuMapper178_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    //struct mmEmuMapper178* p = (struct mmEmuMapper178*)(super);

    //struct mmEmuNes* nes = p->super.nes;
    //struct mmEmuMmu* mmu = &nes->mmu;

    //mmEmuMmu_SetPROM32KBank(mmu, data);
}
MM_EXPORT_EMU void mmEmuMapper178_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper178* p = (struct mmEmuMapper178*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0x4800)
    {
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
    }
    else if (addr == 0x4801)
    {
        p->reg[0] = (data >> 1) & 0x0f;
        mmEmuMapper178_SetBankCPU(p);
    }
    else if (addr == 0x4802)
    {
        p->reg[1] = data << 2;
        //mmEmuMapper178_SetBankCPU(p);
    }
    else if (addr == 0x4803)
    {
        //unknown
    }
    else if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper178_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper178_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper178* p = (struct mmEmuMapper178*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 3);
    buffer[3] = p->banknum;
}
MM_EXPORT_EMU void mmEmuMapper178_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper178* p = (struct mmEmuMapper178*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 3);
    p->banknum = buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper178_Produce(void)
{
    struct mmEmuMapper178* p = (struct mmEmuMapper178*)mmMalloc(sizeof(struct mmEmuMapper178));
    mmEmuMapper178_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper178_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper178* p = (struct mmEmuMapper178*)(m);
    mmEmuMapper178_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER178 =
{
    &__static_mmEmuMapper178_Produce,
    &__static_mmEmuMapper178_Recycle,
};
