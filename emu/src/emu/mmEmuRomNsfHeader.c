/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuRomNsfHeader.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuRomNsfHeader_Init(struct mmEmuRomNsfHeader* p)
{
    mmMemset(p, 0, sizeof(struct mmEmuRomNsfHeader));
}
MM_EXPORT_EMU void mmEmuRomNsfHeader_Destroy(struct mmEmuRomNsfHeader* p)
{
    mmMemset(p, 0, sizeof(struct mmEmuRomNsfHeader));
}

MM_EXPORT_EMU void mmEmuRomNsfHeader_LoggerInformation(const struct mmEmuRomNsfHeader* p, struct mmLogger* logger)
{
    mmLogger_LogI(logger, "NES Header -----------------------------------");
    mmLogger_LogI(logger, "NSF Header          Version: %u", (mmUInt32_t)p->Version);
    mmLogger_LogI(logger, "NSF Header        TotalSong: %u", (mmUInt32_t)p->TotalSong);
    mmLogger_LogI(logger, "NSF Header        StartSong: %u", (mmUInt32_t)p->StartSong);
    mmLogger_LogI(logger, "NSF Header      LoadAddress: 0x%04" PRIx16 "", (mmUInt16_t)p->LoadAddress);
    mmLogger_LogI(logger, "NSF Header      InitAddress: 0x%04" PRIx16 "", (mmUInt16_t)p->InitAddress);
    mmLogger_LogI(logger, "NSF Header      PlayAddress: 0x%04" PRIx16 "", (mmUInt16_t)p->PlayAddress);
    mmLogger_LogI(logger, "NSF Header         SongName: %s", p->SongName);
    mmLogger_LogI(logger, "NSF Header       ArtistName: %s", p->ArtistName);
    mmLogger_LogI(logger, "NSF Header    CopyrightName: %s", p->CopyrightName);
    mmLogger_LogI(logger, "NSF Header        SpeedNTSC: %u", (mmUInt32_t)p->SpeedNTSC);
    mmLogger_LogI(logger, "NSF Header         SpeedPAL: %u", (mmUInt32_t)p->SpeedPAL);
    mmLogger_LogI(logger, "NSF Header     NTSC_PALbits: %u", (mmUInt32_t)p->NTSC_PALbits);
    mmLogger_LogI(logger, "NSF Header  ExtraChipSelect: %u", (mmUInt32_t)p->ExtraChipSelect);
    mmLogger_LogI(logger, "NSF Header     BankSwitch 0: %u", (mmUInt32_t)p->BankSwitch[0]);
    mmLogger_LogI(logger, "NSF Header     BankSwitch 1: %u", (mmUInt32_t)p->BankSwitch[1]);
    mmLogger_LogI(logger, "NSF Header     BankSwitch 2: %u", (mmUInt32_t)p->BankSwitch[2]);
    mmLogger_LogI(logger, "NSF Header     BankSwitch 3: %u", (mmUInt32_t)p->BankSwitch[3]);
    mmLogger_LogI(logger, "NES Header -----------------------------------");
}

