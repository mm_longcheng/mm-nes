/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApuVrc7.h"

#include "core/mmString.h"

MM_EXPORT_EMU void mmEmuApuVrc7_Init(struct mmEmuApuVrc7* p)
{
    mmEmuApuInterface_Init(&p->super);

    mmEmuOpll_Init(&p->VRC7_OPLL);

    p->address = 0;
    //
    mmEmuOpll_SetClock(&p->VRC7_OPLL, 3579545, 22050);
    //
    mmEmuOpll_Reset(&p->VRC7_OPLL);
    mmEmuOpll_ResetPatch(&p->VRC7_OPLL, MM_EMU_OPLL_VRC7_TONE);
    mmEmuOpll_SetMasterVolume(&p->VRC7_OPLL, 128);

    //
    p->super.Reset = &mmEmuApuVrc7_Reset;
    p->super.Setup = &mmEmuApuVrc7_Setup;
    p->super.Process = &mmEmuApuVrc7_Process;
    p->super.Write = &mmEmuApuVrc7_Write;
    //p->super.Read = &mm_emu_apu_vrc7_Read;
    //p->super.SyncWrite = &mm_emu_apu_vrc7_SyncWrite;
    //p->super.SyncRead = &mm_emu_apu_vrc7_SyncRead;
    //p->super.Sync = &mm_emu_apu_vrc7_Sync;
    p->super.GetFreq = &mmEmuApuVrc7_GetFreq;
    //p->super.GetStateSize = &mm_emu_apu_vrc7_GetStateSize;
    //p->super.SaveState = &mm_emu_apu_vrc7_SaveState;
    //p->super.LoadState = &mm_emu_apu_vrc7_LoadState;

    (*(p->super.Reset))(&p->super, MM_EMU_APU_CLOCK, 22050);
}
MM_EXPORT_EMU void mmEmuApuVrc7_Destroy(struct mmEmuApuVrc7* p)
{
    mmEmuApuInterface_Destroy(&p->super);

    mmEmuOpll_Destroy(&p->VRC7_OPLL);

    p->address = 0;
}

MM_EXPORT_EMU void mmEmuApuVrc7_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuVrc7* p = (struct mmEmuApuVrc7*)(super);

    mmEmuOpll_Reset(&p->VRC7_OPLL);
    mmEmuOpll_ResetPatch(&p->VRC7_OPLL, MM_EMU_OPLL_VRC7_TONE);
    mmEmuOpll_SetMasterVolume(&p->VRC7_OPLL, 128);

    p->address = 0;

    (*(p->super.Setup))(&p->super, fClock, nRate);
}
MM_EXPORT_EMU void mmEmuApuVrc7_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuVrc7* p = (struct mmEmuApuVrc7*)(super);

    mmEmuOpll_SetClock(&p->VRC7_OPLL, (mmUInt32_t)(fClock*2.0f), (mmUInt32_t)nRate);
}
MM_EXPORT_EMU void mmEmuApuVrc7_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuVrc7* p = (struct mmEmuApuVrc7*)(super);

    if (addr == 0x9010)
    {
        p->address = data;
    }
    else if (addr == 0x9030)
    {
        mmEmuOpll_WriteReg(&p->VRC7_OPLL, p->address, data);
    }
}
MM_EXPORT_EMU mmInt_t mmEmuApuVrc7_Process(struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuVrc7* p = (struct mmEmuApuVrc7*)(super);

    return mmEmuOpll_Calc(&p->VRC7_OPLL);
}

MM_EXPORT_EMU mmInt_t mmEmuApuVrc7_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuVrc7* p = (struct mmEmuApuVrc7*)(super);

    if (channel < 8)
    {
        static const float blkmul[] = { 0.5f, 1.0f, 2.0f, 4.0f, 8.0f, 16.0f, 32.0f, 64.0f };

        mmInt_t fno = (((mmInt_t)p->VRC7_OPLL.reg[0x20 + channel] & 0x01) << 8) + (mmInt_t)p->VRC7_OPLL.reg[0x10 + channel];
        mmInt_t blk = (p->VRC7_OPLL.reg[0x20 + channel] >> 1) & 0x07;

        if (p->VRC7_OPLL.reg[0x20 + channel] & 0x10)
        {
            return (mmInt_t)((256.0*(double)fno*blkmul[blk]) / ((double)(1 << 18) / (3579545.0 / 72.0)));
        }
    }

    return 0;
}

