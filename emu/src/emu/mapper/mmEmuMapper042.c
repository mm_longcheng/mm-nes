/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper042.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper042_Init(struct mmEmuMapper042* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper042_Reset;
    p->super.Write = &mmEmuMapper042_Write;
    //p->super.Read = &mmEmuMapper042_Read;
    //p->super.ReadLow = &mmEmuMapper042_ReadLow;
    //p->super.WriteLow = &mmEmuMapper042_WriteLow;
    //p->super.ExRead = &mmEmuMapper042_ExRead;
    //p->super.ExWrite = &mmEmuMapper042_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper042_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper042_ExCmdWrite;
    p->super.HSync = &mmEmuMapper042_HSync;
    //p->super.VSync = &mmEmuMapper042_VSync;
    //p->super.Clock = &mmEmuMapper042_Clock;
    //p->super.PPULatch = &mmEmuMapper042_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper042_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper042_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper042_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper042_IsStateSave;
    p->super.SaveState = &mmEmuMapper042_SaveState;
    p->super.LoadState = &mmEmuMapper042_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper042_Destroy(struct mmEmuMapper042* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper042_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper042* p = (struct mmEmuMapper042*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->irq_enable = 0;
    p->irq_counter = 0;

    mmEmuMmu_SetPROM8KBank(mmu, 3, 0);
    mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 4, mmu->PROM_08K_SIZE - 3, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper042_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper042* p = (struct mmEmuMapper042*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE003)
    {
    case 0xE000:
        mmEmuMmu_SetPROM8KBank(mmu, 3, data & 0x0F);
        break;

    case 0xE001:
        if (data & 0x08)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;

    case 0xE002:
        if (data & 0x02)
        {
            p->irq_enable = 0xFF;
        }
        else
        {
            p->irq_enable = 0;
            p->irq_counter = 0;
        }
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper042_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper042* p = (struct mmEmuMapper042*)(super);

    struct mmEmuNes* nes = p->super.nes;

    mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);

    if (p->irq_enable)
    {
        if (p->irq_counter < 215)
        {
            p->irq_counter++;
        }
        if (p->irq_counter == 215)
        {
            p->irq_enable = 0;
            //mm_emu_cpu_IRQ(&nes->cpu);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper042_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper042_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper042* p = (struct mmEmuMapper042*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper042_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper042* p = (struct mmEmuMapper042*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper042_Produce(void)
{
    struct mmEmuMapper042* p = (struct mmEmuMapper042*)mmMalloc(sizeof(struct mmEmuMapper042));
    mmEmuMapper042_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper042_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper042* p = (struct mmEmuMapper042*)(m);
    mmEmuMapper042_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER042 =
{
    &__static_mmEmuMapper042_Produce,
    &__static_mmEmuMapper042_Recycle,
};
