/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper011.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper011_Init(struct mmEmuMapper011* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper011_Reset;
    p->super.Write = &mmEmuMapper011_Write;
    //p->super.Read = &mmEmuMapper011_Read;
    //p->super.ReadLow = &mmEmuMapper011_ReadLow;
    //p->super.WriteLow = &mmEmuMapper011_WriteLow;
    //p->super.ExRead = &mmEmuMapper011_ExRead;
    //p->super.ExWrite = &mmEmuMapper011_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper011_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper011_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper011_HSync;
    //p->super.VSync = &mmEmuMapper011_VSync;
    //p->super.Clock = &mmEmuMapper011_Clock;
    //p->super.PPULatch = &mmEmuMapper011_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper011_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper011_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper011_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper011_IsStateSave;
    //p->super.SaveState = &mmEmuMapper011_SaveState;
    //p->super.LoadState = &mmEmuMapper011_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper011_Destroy(struct mmEmuMapper011* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper011_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper011* p = (struct mmEmuMapper011*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
        // SetVROM_8K_Bank( 1 );
    }
    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
}
MM_EXPORT_EMU void mmEmuMapper011_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper011* p = (struct mmEmuMapper011*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // DEBUGOUT("WR A:%04X D:%02X\n", addr, data);
    mmEmuMmu_SetPROM32KBank(mmu, data);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, data >> 4);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper011_Produce(void)
{
    struct mmEmuMapper011* p = (struct mmEmuMapper011*)mmMalloc(sizeof(struct mmEmuMapper011));
    mmEmuMapper011_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper011_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper011* p = (struct mmEmuMapper011*)(m);
    mmEmuMapper011_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER011 =
{
    &__static_mmEmuMapper011_Produce,
    &__static_mmEmuMapper011_Recycle,
};
