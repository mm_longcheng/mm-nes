/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper_h__
#define __mmEmuMapper_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU32.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

// Extension commands
// For ExCmdRead command
enum mmEmuMapperExCmdRd_t
{
    MM_EMU_MAPPER_EXCMDRD_NONE = 0,
    MM_EMU_MAPPER_EXCMDRD_DISKACCESS,
};
// For ExCmdWrite command
enum mmEmuMapperExCmdWr_t
{
    MM_EMU_MAPPER_EXCMDWR_NONE = 0,
    MM_EMU_MAPPER_EXCMDWR_DISKINSERT,
    MM_EMU_MAPPER_EXCMDWR_DISKEJECT,
};

struct mmEmuNes;

struct mmEmuMapper
{
    struct mmEmuNes* nes;

    // For Mapper
    // Reset
    void(*Reset)(struct mmEmuMapper* p);

    // $8000-$FFFF Memory write
    void(*Write)(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data);

    // $8000-$FFFF Memory read(Dummy)
    void(*Read)(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data);

    // $4100-$7FFF Lower Memory read/write
    mmByte_t(*ReadLow)(struct mmEmuMapper* p, mmWord_t addr);
    void(*WriteLow)(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data);

    // $4018-$40FF Extention register read/write
    mmByte_t(*ExRead)(struct mmEmuMapper* p, mmWord_t addr);
    void(*ExWrite)(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data);

    // cmd mmEmuMapperExCmdRd_t
    mmByte_t(*ExCmdRead)(struct mmEmuMapper* p, mmInt_t cmd);
    void(*ExCmdWrite)(struct mmEmuMapper* p, mmInt_t cmd, mmByte_t data);

    // H sync/V sync/Clock sync
    void(*HSync)(struct mmEmuMapper* p, mmInt_t scanline);
    void(*VSync)(struct mmEmuMapper* p);
    void(*Clock)(struct mmEmuMapper* p, mmInt_t cycles);

    // PPU address bus latch
    void(*PPULatch)(struct mmEmuMapper* p, mmWord_t addr);

    // PPU Character latch
    void(*PPUChrLatch)(struct mmEmuMapper* p, mmWord_t addr);

    // PPU Extension character/palette
    void(*PPUExtLatchX)(struct mmEmuMapper* p, mmInt_t x);
    void(*PPUExtLatch)(struct mmEmuMapper* p, mmWord_t addr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr);

    // For State save
    mmBool_t(*IsStateSave)(const struct mmEmuMapper* p);
    void(*SaveState)(const struct mmEmuMapper* p, mmByte_t* buffer);
    void(*LoadState)(struct mmEmuMapper* p, const mmByte_t* buffer);
};

MM_EXPORT_EMU void mmEmuMapper_Init(struct mmEmuMapper* p);
MM_EXPORT_EMU void mmEmuMapper_Destroy(struct mmEmuMapper* p);

MM_EXPORT_EMU void mmEmuMapper_SetParent(struct mmEmuMapper* p, struct mmEmuNes* parent);

// For Mapper
// Reset
MM_EXPORT_EMU void mmEmuMapper_Reset(struct mmEmuMapper* p);

// $8000-$FFFF Memory write
MM_EXPORT_EMU void mmEmuMapper_Write(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data);

// $8000-$FFFF Memory read(Dummy)
MM_EXPORT_EMU void mmEmuMapper_Read(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data);

// $4100-$7FFF Lower Memory read/write
MM_EXPORT_EMU mmByte_t mmEmuMapper_ReadLow(struct mmEmuMapper* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper_WriteLow(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data);

// $4018-$40FF Extention register read/write
MM_EXPORT_EMU mmByte_t mmEmuMapper_ExRead(struct mmEmuMapper* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper_ExWrite(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data);

// cmd mmEmuMapperExCmdRd_t
MM_EXPORT_EMU mmByte_t mmEmuMapper_ExCmdRead(struct mmEmuMapper* p, mmInt_t cmd);
MM_EXPORT_EMU void mmEmuMapper_ExCmdWrite(struct mmEmuMapper* p, mmInt_t cmd, mmByte_t data);

// H sync/V sync/Clock sync
MM_EXPORT_EMU void mmEmuMapper_HSync(struct mmEmuMapper* p, mmInt_t scanline);
MM_EXPORT_EMU void mmEmuMapper_VSync(struct mmEmuMapper* p);
MM_EXPORT_EMU void mmEmuMapper_Clock(struct mmEmuMapper* p, mmInt_t cycles);

// PPU address bus latch
MM_EXPORT_EMU void mmEmuMapper_PPULatch(struct mmEmuMapper* p, mmWord_t addr);

// PPU Character latch
MM_EXPORT_EMU void mmEmuMapper_PPUChrLatch(struct mmEmuMapper* p, mmWord_t addr);

// PPU Extension character/palette
MM_EXPORT_EMU void mmEmuMapper_PPUExtLatchX(struct mmEmuMapper* p, mmInt_t x);
MM_EXPORT_EMU void mmEmuMapper_PPUExtLatch(struct mmEmuMapper* p, mmWord_t addr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr);

// For State save
MM_EXPORT_EMU mmBool_t mmEmuMapper_IsStateSave(const struct mmEmuMapper* p);
MM_EXPORT_EMU void mmEmuMapper_SaveState(const struct mmEmuMapper* p, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper_LoadState(struct mmEmuMapper* p, const mmByte_t* buffer);

struct mmEmuMapperCreator
{
    struct mmEmuMapper*(*Produce)(void);
    void(*Recycle)(struct mmEmuMapper* m);
};

struct mmEmuMapperFactory
{
    struct mmRbtreeU32Vpt rbtree;
};

MM_EXPORT_EMU void mmEmuMapperFactory_Init(struct mmEmuMapperFactory* p);
MM_EXPORT_EMU void mmEmuMapperFactory_Destroy(struct mmEmuMapperFactory* p);

MM_EXPORT_EMU void mmEmuMapperFactory_Register(struct mmEmuMapperFactory* p, mmUInt32_t id, const struct mmEmuMapperCreator* creator);
MM_EXPORT_EMU void mmEmuMapperFactory_Unregister(struct mmEmuMapperFactory* p, mmUInt32_t id);

MM_EXPORT_EMU void mmEmuMapperFactory_Clear(struct mmEmuMapperFactory* p);

MM_EXPORT_EMU struct mmEmuMapper* mmEmuMapperFactory_Produce(struct mmEmuMapperFactory* p, mmUInt32_t id);
MM_EXPORT_EMU void mmEmuMapperFactory_Recycle(struct mmEmuMapperFactory* p, mmUInt32_t id, struct mmEmuMapper* m);

#include "core/mmSuffix.h"

#endif//__mmEmuMapper_h__
