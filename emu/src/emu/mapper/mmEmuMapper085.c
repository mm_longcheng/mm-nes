/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper085.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper085_Init(struct mmEmuMapper085* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    //
    p->super.Reset = &mmEmuMapper085_Reset;
    p->super.Write = &mmEmuMapper085_Write;
    //p->super.Read = &mmEmuMapper085_Read;
    //p->super.ReadLow = &mmEmuMapper085_ReadLow;
    //p->super.WriteLow = &mmEmuMapper085_WriteLow;
    //p->super.ExRead = &mmEmuMapper085_ExRead;
    //p->super.ExWrite = &mmEmuMapper085_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper085_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper085_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper085_HSync;
    //p->super.VSync = &mmEmuMapper085_VSync;
    p->super.Clock = &mmEmuMapper085_Clock;
    //p->super.PPULatch = &mmEmuMapper085_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper085_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper085_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper085_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper085_IsStateSave;
    p->super.SaveState = &mmEmuMapper085_SaveState;
    p->super.LoadState = &mmEmuMapper085_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper085_Destroy(struct mmEmuMapper085* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;
}

MM_EXPORT_EMU void mmEmuMapper085_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper085* p = (struct mmEmuMapper085*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmUInt32_t crc = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
    else
    {
        mmEmuMmu_SetCRAM8KBank(mmu, 0);
    }

#if 0
    //crc = mmEmuRom_GetPROMCRC(&nes->rom);
    //if (crc == 0x1aa0479c)                // For Tiny Toon Adventures 2 - Montana Land he Youkoso(J)
    //{ 
    //  mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_RENDER);
    //}
    //if (crc == 0x33ce3ff0)                // For Lagrange Point(J)
    //{ 
    //  mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    //}
#endif

    mmEmuApu_SelectExSound(&nes->apu, 2);
}
MM_EXPORT_EMU void mmEmuMapper085_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper085* p = (struct mmEmuMapper085*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF038)
    {
    case 0x8000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0x8008:
    case 0x8010:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0x9000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;

    case 0x9010:
    case 0x9030:
        mmEmuApu_ExWrite(&nes->apu, addr, data);
        break;

    case 0xA000:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 0, data);
        }
        break;

    case 0xA008:
    case 0xA010:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 1, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 1, data);
        }
        break;

    case 0xB000:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 2, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 2, data);
        }
        break;

    case 0xB008:
    case 0xB010:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 3, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 3, data);
        }
        break;

    case 0xC000:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 4, data);
        }
        break;

    case 0xC008:
    case 0xC010:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 5, data);
        }
        break;

    case 0xD000:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 6, data);
        }
        break;

    case 0xD008:
    case 0xD010:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 7, data);
        }
        break;

    case 0xE000:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0xE008:
    case 0xE010:
        p->irq_latch = data;
        break;

    case 0xF000:
        p->irq_enable = data & 0x03;
        p->irq_counter = p->irq_latch;
        p->irq_clock = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF008:
    case 0xF010:
        p->irq_enable = (p->irq_enable & 0x01) * 3;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper085_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper085* p = (struct mmEmuMapper085*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable & 0x02)
    {
        p->irq_clock += cycles * 4;
        while (p->irq_clock >= 455)
        {
            p->irq_clock -= 455;
            p->irq_counter++;
            if (p->irq_counter == 0)
            {
                p->irq_counter = p->irq_latch;
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper085_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper085_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper085* p = (struct mmEmuMapper085*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_counter;
    buffer[2] = p->irq_latch;
    *((mmInt_t*)&buffer[4]) = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper085_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper085* p = (struct mmEmuMapper085*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = buffer[1];
    p->irq_latch = buffer[2];
    p->irq_clock = *((mmInt_t*)&buffer[4]);
}

static struct mmEmuMapper* __static_mmEmuMapper085_Produce(void)
{
    struct mmEmuMapper085* p = (struct mmEmuMapper085*)mmMalloc(sizeof(struct mmEmuMapper085));
    mmEmuMapper085_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper085_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper085* p = (struct mmEmuMapper085*)(m);
    mmEmuMapper085_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER085 =
{
    &__static_mmEmuMapper085_Produce,
    &__static_mmEmuMapper085_Recycle,
};
