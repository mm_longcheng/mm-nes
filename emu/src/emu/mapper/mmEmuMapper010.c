/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper010.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper010_Init(struct mmEmuMapper010* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[0] = 0; p->reg[1] = 4;
    p->reg[2] = 0; p->reg[3] = 0;

    p->latch_a = 0xFE;
    p->latch_b = 0xFE;

    //
    p->super.Reset = &mmEmuMapper010_Reset;
    p->super.Write = &mmEmuMapper010_Write;
    //p->super.Read = &mmEmuMapper010_Read;
    //p->super.ReadLow = &mmEmuMapper010_ReadLow;
    //p->super.WriteLow = &mmEmuMapper010_WriteLow;
    //p->super.ExRead = &mmEmuMapper010_ExRead;
    //p->super.ExWrite = &mmEmuMapper010_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper010_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper010_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper010_HSync;
    //p->super.VSync = &mmEmuMapper010_VSync;
    //p->super.Clock = &mmEmuMapper010_Clock;
    //p->super.PPULatch = &mmEmuMapper010_PPULatch;
    p->super.PPUChrLatch = &mmEmuMapper010_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper010_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper010_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper010_IsStateSave;
    p->super.SaveState = &mmEmuMapper010_SaveState;
    p->super.LoadState = &mmEmuMapper010_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper010_Destroy(struct mmEmuMapper010* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg[0] = 0; p->reg[1] = 4;
    p->reg[2] = 0; p->reg[3] = 0;

    p->latch_a = 0xFE;
    p->latch_b = 0xFE;
}

MM_EXPORT_EMU void mmEmuMapper010_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper010* p = (struct mmEmuMapper010*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    p->reg[0] = 0; p->reg[1] = 4;
    p->reg[2] = 0; p->reg[3] = 0;

    p->latch_a = 0xFE;
    p->latch_b = 0xFE;
    mmEmuMmu_SetVROM4KBank(mmu, 0, 4);
    mmEmuMmu_SetVROM4KBank(mmu, 4, 0);

    mmEmuPpu_SetChrLatchMode(&nes->ppu, MM_TRUE);
}
MM_EXPORT_EMU void mmEmuMapper010_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper010* p = (struct mmEmuMapper010*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF000)
    {
    case 0xA000:
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        break;
    case 0xB000:
        p->reg[0] = data;
        if (p->latch_a == 0xFD)
        {
            mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[0]);
        }
        break;
    case 0xC000:
        p->reg[1] = data;
        if (p->latch_a == 0xFE)
        {
            mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[1]);
        }
        break;
    case 0xD000:
        p->reg[2] = data;
        if (p->latch_b == 0xFD)
        {
            mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[2]);
        }
        break;
    case 0xE000:
        p->reg[3] = data;
        if (p->latch_b == 0xFE)
        {
            mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[3]);
        }
        break;
    case 0xF000:
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper010_PPUChrLatch(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper010* p = (struct mmEmuMapper010*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0x1FF0) == 0x0FD0 && p->latch_a != 0xFD)
    {
        p->latch_a = 0xFD;
        mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[0]);
    }
    else if ((addr & 0x1FF0) == 0x0FE0 && p->latch_a != 0xFE)
    {
        p->latch_a = 0xFE;
        mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[1]);
    }
    else if ((addr & 0x1FF0) == 0x1FD0 && p->latch_b != 0xFD)
    {
        p->latch_b = 0xFD;
        mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[2]);
    }
    else if ((addr & 0x1FF0) == 0x1FE0 && p->latch_b != 0xFE)
    {
        p->latch_b = 0xFE;
        mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[3]);
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper010_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper010_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper010* p = (struct mmEmuMapper010*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->reg[3];
    buffer[4] = p->latch_a;
    buffer[5] = p->latch_b;
}
MM_EXPORT_EMU void mmEmuMapper010_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper010* p = (struct mmEmuMapper010*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->reg[3] = buffer[3];
    p->latch_a = buffer[4];
    p->latch_b = buffer[5];
}

static struct mmEmuMapper* __static_mmEmuMapper010_Produce(void)
{
    struct mmEmuMapper010* p = (struct mmEmuMapper010*)mmMalloc(sizeof(struct mmEmuMapper010));
    mmEmuMapper010_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper010_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper010* p = (struct mmEmuMapper010*)(m);
    mmEmuMapper010_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER010 =
{
    &__static_mmEmuMapper010_Produce,
    &__static_mmEmuMapper010_Recycle,
};
