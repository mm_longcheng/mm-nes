/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuNes_h__
#define __mmEmuNes_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"

#include "mmEmuRom.h"
#include "mmEmuMmu.h"
#include "mmEmuCpu.h"
#include "mmEmuPpu.h"
#include "mmEmuApu.h"
#include "mmEmuPad.h"

#include "mmEmuNesConfig.h"

#include "mmEmuState.h"

#include "mmEmuMapper.h"

#include "mmEmuAssets.h"
#include "mmEmuController.h"
#include "mmEmuScreen.h"
#include "mmEmuWaveRecord.h"

#include "mmEmuMovie.h"
#include "mmEmuCheat.h"
#include "mmEmuNsf.h"
#include "mmEmuTape.h"
#include "mmEmuBarcode.h"
#include "mmEmuStateFile.h"

#include "mmEmuVSUnisystem.h"
#include "mmEmuNesProfiler.h"
#include "mmEmuNesCallback.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

// profiler
#ifndef MM_EMU_NES_PROFILER
#define MM_EMU_NES_PROFILER 1
#endif//MM_EMU_NES_PROFILER

// Config
#define MM_EMU_NES_FETCH_CYCLES 8

// IRQ type contorol.
enum mmEmuNesIrqMethod_t
{
    MM_EMU_NES_IRQ_HSYNC = 0,
    MM_EMU_NES_IRQ_CLOCK = 1,
};

// Render method.
enum mmEmuNesRenderMethod_t
{
    MM_EMU_NES_POST_ALL_RENDER = 0, // After execution of instructions for scan lines, rendering.
    MM_EMU_NES_PRE_ALL_RENDER  = 1, // Execution of instructions for scan lines after rendering.
    MM_EMU_NES_POST_RENDER     = 2, // After executing instructions for the display period, rendering.
    MM_EMU_NES_PRE_RENDER      = 3, // Execution of instructions for the display period after rendering execution.
    MM_EMU_NES_TILE_RENDER     = 4, // Tile based rendering.
};

// Command.
enum mmEmuNesNescommand_t
{
    MM_EMU_NES_NESCMD_NONE = 0,
    MM_EMU_NES_NESCMD_HWRESET,
    MM_EMU_NES_NESCMD_SWRESET,
    MM_EMU_NES_NESCMD_EXCONTROLLER, // Commandparam
    MM_EMU_NES_NESCMD_DISK_THROTTLE_ON,
    MM_EMU_NES_NESCMD_DISK_THROTTLE_OFF,
    MM_EMU_NES_NESCMD_DISK_EJECT,
    MM_EMU_NES_NESCMD_DISK_0A,
    MM_EMU_NES_NESCMD_DISK_0B,
    MM_EMU_NES_NESCMD_DISK_1A,
    MM_EMU_NES_NESCMD_DISK_1B,
    MM_EMU_NES_NESCMD_DISK_2A,
    MM_EMU_NES_NESCMD_DISK_2B,
    MM_EMU_NES_NESCMD_DISK_3A,
    MM_EMU_NES_NESCMD_DISK_3B,

    MM_EMU_NES_NESCMD_SOUND_MUTE,   // CommandParam
};

// most of time we need alloc at heap.
// if need alloc at stack you must make sure not stackoverflow.
struct mmEmuNes
{
    struct mmEmuRom rom;
    struct mmEmuMmu mmu;
    struct mmEmuCpu cpu;
    struct mmEmuPpu ppu;
    struct mmEmuApu apu;
    struct mmEmuPad pad;

    struct mmEmuNesConfig config;

    // asset
    struct mmEmuAssets assets;
    // controller.
    struct mmEmuController controller;
    // screen.
    struct mmEmuScreen screen;
    // wave record.
    struct mmEmuWaveRecord wave_record;

    struct mmEmuMapperFactory mapper_factory;
    struct mmEmuMapper* mapper;
    mmUInt32_t mapper_id;

    mmInt_t     nIRQtype;
    mmInt_t     nVideoMode;
    mmBool_t    bFrameIRQ;

    mmInt_t     NES_scanline;
    mmBool_t    bZapper;
    mmLong_t    ZapperX, ZapperY;

    // For Movie
    struct mmEmuMovie movie;

    mmInt_t     m_CommandRequest;

    mmBool_t    m_bDiskThrottle;

    mmInt_t     RenderMethod;

    mmSInt64_t  base_cycles;
    mmSInt64_t  emul_cycles;

    mmInt_t     SAVERAM_SIZE;

    // For VS-Unisystem
    mmByte_t    m_VSDipValue;
    const struct mmEmuVSDipSwitch* m_VSDipTable;

    // For Cheat
    struct mmEmuCheat cheat;

    // For NSF
    struct mmEmuNsf nsf;

    // For Tape
    struct mmEmuTape tape;

    // For Barcode
    struct mmEmuBarcode barcode;

    // For TurboFile
    mmInt_t m_TurboFileBank;

    // gameoption backup
    mmInt_t     m_saveRenderMethod;
    mmInt_t     m_saveIrqType;
    mmBool_t    m_saveFrameIRQ;
    mmBool_t    m_saveVideoMode;

    struct mmEmuNesProfiler m_Profiler;

    struct mmEmuNesCallback callback;
};

MM_EXPORT_EMU void mmEmuNes_Init(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_Destroy(struct mmEmuNes* p);

MM_EXPORT_EMU void mmEmuNes_SetRomPath(struct mmEmuNes* p, const char* szRomPath);
MM_EXPORT_EMU void mmEmuNes_SetRomName(struct mmEmuNes* p, const char* szRomName);
MM_EXPORT_EMU void mmEmuNes_SetWritablePath(struct mmEmuNes* p, const char* szWritablePath);
MM_EXPORT_EMU void mmEmuNes_SetResourceRoot(struct mmEmuNes* p, mmUInt32_t type_assets, const char* root_path, const char* root_base);
MM_EXPORT_EMU void mmEmuNes_SetPaletteTableByte(struct mmEmuNes* p, const mmUInt32_t pal[64]);
MM_EXPORT_EMU void mmEmuNes_SetCallback(struct mmEmuNes* p, struct mmEmuNesCallback* callback);

// return MM_SUCCESS is success.
MM_EXPORT_EMU int mmEmuNes_LoadRom(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_LoggerInformation(struct mmEmuNes* p, struct mmLogger* logger);

// Member function.
// Emulation.
MM_EXPORT_EMU void mmEmuNes_HardwareReset(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_SoftwareReset(struct mmEmuNes* p);

MM_EXPORT_EMU void mmEmuNes_Clock(struct mmEmuNes* p, mmInt_t cycles);

MM_EXPORT_EMU mmByte_t mmEmuNes_Read(struct mmEmuNes* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuNes_Write(struct mmEmuNes* p, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuNes_EmulationCPU(struct mmEmuNes* p, mmInt_t basecycles);
MM_EXPORT_EMU void mmEmuNes_EmulationCPUBeforeNMI(struct mmEmuNes* p, mmInt_t cycles);

MM_EXPORT_EMU void mmEmuNes_EmulateFrame(struct mmEmuNes* p, mmBool_t bDraw);

// For NSF
MM_EXPORT_EMU void mmEmuNes_EmulateNSF(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_SetNsfPlay(struct mmEmuNes* p, mmInt_t songno, mmInt_t songmode);
MM_EXPORT_EMU void mmEmuNes_SetNsfStop(struct mmEmuNes* p);
MM_EXPORT_EMU mmBool_t mmEmuNes_IsNsfPlaying(struct mmEmuNes* p);

// Pad Sync.
MM_EXPORT_EMU void mmEmuNes_PadSync(struct mmEmuNes* p);
// Pad Sync Data.
MM_EXPORT_EMU void mmEmuNes_SetPadSyncData(struct mmEmuNes* p, mmUInt32_t data);
MM_EXPORT_EMU mmUInt32_t mmEmuNes_GetPadSyncData(struct mmEmuNes* p);

// Audio buffer process.
MM_EXPORT_EMU void mmEmuNes_ApuProcess(struct mmEmuNes* p, mmByte_t* lpBuffer, mmUInt32_t dwSize, mmInt_t nRate);

// Screen
MM_EXPORT_EMU mmByte_t* mmEmuNes_GetFramebuffer(struct mmEmuNes* p);
MM_EXPORT_EMU mmByte_t* mmEmuNes_GetScreenPtr(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_GetPaletteData(struct mmEmuNes* p, struct mmEmuRGBQuad* rgb);
MM_EXPORT_EMU void mmEmuNes_GetPaletteR8G8B8A8(struct mmEmuNes* p, mmUInt32_t* palette);
MM_EXPORT_EMU void mmEmuNes_ScreenClear(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_ScreenBitBlockTransfer(struct mmEmuNes* p);

// event Keyboard.
MM_EXPORT_EMU void mmEmuNes_KeyboardPressed(struct mmEmuNes* p, int hId);
MM_EXPORT_EMU void mmEmuNes_KeyboardRelease(struct mmEmuNes* p, int hId);
// event Mouse.
MM_EXPORT_EMU void mmEmuNes_MouseBegan(struct mmEmuNes* p, mmLong_t x, mmLong_t y, int button_mask);
MM_EXPORT_EMU void mmEmuNes_MouseMoved(struct mmEmuNes* p, mmLong_t x, mmLong_t y, int button_mask);
MM_EXPORT_EMU void mmEmuNes_MouseEnded(struct mmEmuNes* p, mmLong_t x, mmLong_t y, int button_mask);

// For Manual bit controller Joypad.
MM_EXPORT_EMU void mmEmuNes_JoypadBitSetData(struct mmEmuNes* p, int n, mmWord_t data);
MM_EXPORT_EMU mmWord_t mmEmuNes_JoypadBitGetData(struct mmEmuNes* p, int n);
MM_EXPORT_EMU void mmEmuNes_JoypadBitPressed(struct mmEmuNes* p, int n, int hId);
MM_EXPORT_EMU void mmEmuNes_JoypadBitRelease(struct mmEmuNes* p, int n, int hId);
// For Manual bit controller Nsf.
MM_EXPORT_EMU void mmEmuNes_NsfBitSetData(struct mmEmuNes* p, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuNes_NsfBitGetData(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_NsfBitPressed(struct mmEmuNes* p, int hId);
MM_EXPORT_EMU void mmEmuNes_NsfBitRelease(struct mmEmuNes* p, int hId);

// Message string.
MM_EXPORT_EMU void mmEmuNes_SetMessageString(struct mmEmuNes* p, const char* message);

// Wave record.
MM_EXPORT_EMU void mmEmuNes_WaveRecordStart(struct mmEmuNes* p, const char* szFile);
MM_EXPORT_EMU void mmEmuNes_WaveRecordStop(struct mmEmuNes* p);
MM_EXPORT_EMU mmBool_t mmEmuNes_WaveRecordIsWaveRecord(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_WaveRecordOutput(struct mmEmuNes* p, void* lpBuf, mmUInt32_t dwSize);

// Support for external sound files.
MM_EXPORT_EMU mmBool_t mmEmuNes_LoadEsf(struct mmEmuNes* p, const char* szFileName, mmInt_t no);
MM_EXPORT_EMU mmBool_t mmEmuNes_EsfPlay(struct mmEmuNes* p, mmInt_t no);
MM_EXPORT_EMU mmBool_t mmEmuNes_EsfPlayLoop(struct mmEmuNes* p, mmInt_t no);
MM_EXPORT_EMU mmBool_t mmEmuNes_EsfStop(struct mmEmuNes* p, mmInt_t no);
MM_EXPORT_EMU void mmEmuNes_EsfAllStop(struct mmEmuNes* p);

static mmInline void mmEmuNes_SetIrqType(struct mmEmuNes* p, mmInt_t nType) { p->nIRQtype = nType; };
static mmInline mmInt_t mmEmuNes_GetIrqType(struct mmEmuNes* p) { return (mmInt_t)p->nIRQtype; };

// Frame-IRQ control (for Paris-Dakar Rally Special)
static mmInline void mmEmuNes_SetFrameIRQmode(struct mmEmuNes* p, mmBool_t bMode) { p->bFrameIRQ = bMode; }
static mmInline mmBool_t mmEmuNes_GetFrameIRQmode(struct mmEmuNes* p) { return p->bFrameIRQ; }

// NTSC/PAL/Dendy
MM_EXPORT_EMU void mmEmuNes_SetVideoMode(struct mmEmuNes* p, mmInt_t nMode);
static mmInline mmInt_t mmEmuNes_GetVideoMode(struct mmEmuNes* p) { return p->nVideoMode; }

//
MM_EXPORT_EMU mmInt_t mmEmuNes_GetDiskNo(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_SoundSetup(struct mmEmuNes* p);

static mmInline mmInt_t mmEmuNes_GetScanline(struct mmEmuNes* p) { return p->NES_scanline; }
static mmInline mmBool_t mmEmuNes_GetZapperHit(struct mmEmuNes* p) { return p->bZapper; }
static mmInline void mmEmuNes_GetZapperPos(struct mmEmuNes* p, mmLong_t* x, mmLong_t* y) { *x = p->ZapperX; *y = p->ZapperY; }
static mmInline void mmEmuNes_SetZapperPos(struct mmEmuNes* p, mmLong_t x, mmLong_t y) { p->ZapperX = x; p->ZapperY = y; }

// State file.
// 0:ERROR 1:CRC OK -1:CRC ERR
MM_EXPORT_EMU mmInt_t mmEmuNes_IsStateFile(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU mmUInt32_t mmEmuNes_LoadState(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU mmUInt32_t mmEmuNes_SaveState(struct mmEmuNes* p, const char* fname);

static mmInline mmInt_t mmEmuNes_GetSAVERAMSIZE(struct mmEmuNes* p) { return p->SAVERAM_SIZE; }
static mmInline void mmEmuNes_SetSAVERAMSIZE(struct mmEmuNes* p, mmInt_t size) { p->SAVERAM_SIZE = size; }

// VS-Unisystem
static mmInline mmByte_t mmEmuNes_GetVSDipSwitch(struct mmEmuNes* p) { return p->m_VSDipValue; }
static mmInline void mmEmuNes_SetVSDipSwitch(struct mmEmuNes* p, mmByte_t v) { p->m_VSDipValue = v; }
static mmInline const struct mmEmuVSDipSwitch* mmEmuNes_GetVSDipSwitchTable(struct mmEmuNes* p) { return p->m_VSDipTable; }

// snap shot.
MM_EXPORT_EMU mmUInt32_t mmEmuNes_Snapshot(struct mmEmuNes* p);

// For Movie
// 0:ERROR 1:CRC OK -1:CRC ERR
MM_EXPORT_EMU mmInt_t mmEmuNes_IsMovieFile(const struct mmEmuNes* p, const char* fname);

MM_EXPORT_EMU mmBool_t mmEmuNes_IsMoviePlay(const struct mmEmuNes* p);
MM_EXPORT_EMU mmBool_t mmEmuNes_IsMovieRec(const struct mmEmuNes* p);
MM_EXPORT_EMU int mmEmuNes_MoviePlay(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU int mmEmuNes_MovieRec(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU int mmEmuNes_MovieRecAppend(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU mmBool_t mmEmuNes_MovieStop(struct mmEmuNes* p);
// For Movie
MM_EXPORT_EMU void mmEmuNes_Movie(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_GetMovieInfo(const struct mmEmuNes* p, struct mmEmuMovieInfo* info);

MM_EXPORT_EMU void mmEmuNes_Command(struct mmEmuNes* p, mmInt_t cmd);
MM_EXPORT_EMU mmBool_t mmEmuNes_CommandParam(struct mmEmuNes* p, mmInt_t cmd, mmInt_t param);

// Other controls.
static mmInline mmBool_t mmEmuNes_IsDiskThrottle(const struct mmEmuNes* p) { return p->m_bDiskThrottle; }

static mmInline void mmEmuNes_SetRenderMethod(struct mmEmuNes* p, mmInt_t type) { p->RenderMethod = type; }
static mmInline mmInt_t mmEmuNes_GetRenderMethod(const struct mmEmuNes* p) { return p->RenderMethod; }

// For Cheat
MM_EXPORT_EMU void mmEmuNes_CheatInitial(struct mmEmuNes* p);

MM_EXPORT_EMU mmBool_t mmEmuNes_IsCheatCodeAdd(struct mmEmuNes* p);

MM_EXPORT_EMU mmInt_t mmEmuNes_GetCheatCodeNum(const struct mmEmuNes* p);
MM_EXPORT_EMU mmBool_t mmEmuNes_GetCheatCode(const struct mmEmuNes* p, mmInt_t no, struct mmEmuCheatcode* code);
MM_EXPORT_EMU void mmEmuNes_SetCheatCodeFlag(struct mmEmuNes* p, mmInt_t no, mmBool_t bEnable);
MM_EXPORT_EMU void mmEmuNes_SetCheatCodeAllFlag(struct mmEmuNes* p, mmBool_t bEnable, mmBool_t bKey);

MM_EXPORT_EMU void mmEmuNes_ReplaceCheatCode(struct mmEmuNes* p, mmInt_t no, struct mmEmuCheatcode* code);
MM_EXPORT_EMU void mmEmuNes_AddCheatCode(struct mmEmuNes* p, struct mmEmuCheatcode* code);
MM_EXPORT_EMU void mmEmuNes_DelCheatCode(struct mmEmuNes* p, mmInt_t no);

MM_EXPORT_EMU mmUInt32_t mmEmuNes_CheatRead(struct mmEmuNes* p, mmInt_t length, mmWord_t addr);
MM_EXPORT_EMU void mmEmuNes_CheatWrite(struct mmEmuNes* p, mmInt_t length, mmWord_t addr, mmUInt32_t data);
MM_EXPORT_EMU void mmEmuNes_CheatCodeProcess(struct mmEmuNes* p);

// For Genie
MM_EXPORT_EMU void mmEmuNes_GenieInitial(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_GenieLoad(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU void mmEmuNes_GenieCodeProcess(struct mmEmuNes* p);

// TapeDevice
MM_EXPORT_EMU mmBool_t mmEmuNes_IsTapePlay(const struct mmEmuNes* p);
MM_EXPORT_EMU mmBool_t mmEmuNes_IsTapeRec(const struct mmEmuNes* p);
MM_EXPORT_EMU int mmEmuNes_TapePlay(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU int mmEmuNes_TapeRec(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU void mmEmuNes_TapeStop(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_Tape(struct mmEmuNes* p, mmInt_t cycles);

// Barcode battler(Bandai)
MM_EXPORT_EMU void mmEmuNes_SetBarcodeData(struct mmEmuNes* p, mmByte_t* code, mmInt_t len);
MM_EXPORT_EMU void mmEmuNes_Barcode(struct mmEmuNes* p, mmInt_t cycles);
MM_EXPORT_EMU mmBool_t mmEmuNes_IsBarcodeEnable(const struct mmEmuNes* p);
MM_EXPORT_EMU mmByte_t mmEmuNes_GetBarcodeStatus(const struct mmEmuNes* p);

// Barcode world(Sunsoft/EPOCH)
MM_EXPORT_EMU void mmEmuNes_SetBarcode2Data(struct mmEmuNes* p, mmByte_t* code, mmInt_t len);
MM_EXPORT_EMU mmByte_t mmEmuNes_Barcode2(struct mmEmuNes* p);
MM_EXPORT_EMU mmBool_t mmEmuNes_IsBarcode2Enable(const struct mmEmuNes* p);

// TurboFile
static mmInline void mmEmuNes_SetTurboFileBank(struct mmEmuNes* p, mmInt_t bank) { p->m_TurboFileBank = bank; }
static mmInline mmInt_t mmEmuNes_GetTurboFileBank(const struct mmEmuNes* p) { return p->m_TurboFileBank; }

// profiler
static mmInline mmUInt32_t mmEmuNes_GetFrameTotalCycles(const struct mmEmuNes* p) { return p->m_Profiler.m_dwTotalCycle; }
static mmInline mmUInt32_t mmEmuNes_GetProfileTotalCycles(const struct mmEmuNes* p) { return p->m_Profiler.m_dwProfileTotalCycle; }
static mmInline mmUInt32_t mmEmuNes_GetProfileTotalCount(const struct mmEmuNes* p) { return p->m_Profiler.m_dwProfileTotalCount; }
static mmInline mmUInt64_t mmEmuNes_GetProfileCycles(const struct mmEmuNes* p) { return p->m_Profiler.m_dwProfileCycle; }

// Member function.
// Emulation.
MM_EXPORT_EMU mmByte_t mmEmuNes_ReadReg(struct mmEmuNes* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuNes_WriteReg(struct mmEmuNes* p, mmWord_t addr, mmByte_t data);

// State Sub
MM_EXPORT_EMU int mmEmuNes_ReadState(struct mmEmuNes* p, FILE* fp);
MM_EXPORT_EMU int mmEmuNes_WriteState(struct mmEmuNes* p, FILE* fp);

// return MM_SUCCESS is success.
MM_EXPORT_EMU int mmEmuNes_LoadSRAM(struct mmEmuNes* p);
MM_EXPORT_EMU int mmEmuNes_SaveSRAM(struct mmEmuNes* p);

// return MM_SUCCESS is success.
MM_EXPORT_EMU int mmEmuNes_LoadDISK(struct mmEmuNes* p);
MM_EXPORT_EMU int mmEmuNes_SaveDISK(struct mmEmuNes* p);

// return MM_SUCCESS is success.
MM_EXPORT_EMU int mmEmuNes_LoadTurboFile(struct mmEmuNes* p);
MM_EXPORT_EMU int mmEmuNes_SaveTurboFile(struct mmEmuNes* p);

// For Movie pad display
MM_EXPORT_EMU void mmEmuNes_DrawPad(struct mmEmuNes* p);
MM_EXPORT_EMU void mmEmuNes_DrawBitmap(struct mmEmuNes* p, mmInt_t x, mmInt_t y, const mmByte_t* lpBitmap);

MM_EXPORT_EMU void mmEmuNes_DrawFont(struct mmEmuNes* p, mmInt_t x, mmInt_t y, mmByte_t chr, mmByte_t col);
MM_EXPORT_EMU void mmEmuNes_DrawString(struct mmEmuNes* p, mmInt_t x, mmInt_t y, mmByte_t* str, mmByte_t col);

#include "core/mmSuffix.h"

#endif//__mmEmuNes_h__
