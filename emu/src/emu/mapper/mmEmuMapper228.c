/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper228.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper228_Init(struct mmEmuMapper228* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper228_Reset;
    p->super.Write = &mmEmuMapper228_Write;
    //p->super.Read = &mmEmuMapper228_Read;
    //p->super.ReadLow = &mmEmuMapper228_ReadLow;
    //p->super.WriteLow = &mmEmuMapper228_WriteLow;
    //p->super.ExRead = &mmEmuMapper228_ExRead;
    //p->super.ExWrite = &mmEmuMapper228_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper228_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper228_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper228_HSync;
    //p->super.VSync = &mmEmuMapper228_VSync;
    //p->super.Clock = &mmEmuMapper228_Clock;
    //p->super.PPULatch = &mmEmuMapper228_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper228_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper228_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper228_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper228_IsStateSave;
    //p->super.SaveState = &mmEmuMapper228_SaveState;
    //p->super.LoadState = &mmEmuMapper228_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper228_Destroy(struct mmEmuMapper228* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper228_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper228* p = (struct mmEmuMapper228*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper228_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper228* p = (struct mmEmuMapper228*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t prg = (addr & 0x0780) >> 7;

    switch ((addr & 0x1800) >> 11)
    {
    case 1:
        prg |= 0x10;
        break;
    case 3:
        prg |= 0x20;
        break;
    default:
        break;
    }

    if (addr & 0x0020)
    {
        prg <<= 1;
        if (addr & 0x0040)
        {
            prg++;
        }
        mmEmuMmu_SetPROM8KBank(mmu, 4, prg * 4 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, prg * 4 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, prg * 4 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 7, prg * 4 + 1);
    }
    else
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, prg * 4 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, prg * 4 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, prg * 4 + 2);
        mmEmuMmu_SetPROM8KBank(mmu, 7, prg * 4 + 3);
    }

    mmEmuMmu_SetVROM8KBank(mmu, ((addr & 0x000F) << 2) | (data & 0x03));

    if (addr & 0x2000)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper228_Produce(void)
{
    struct mmEmuMapper228* p = (struct mmEmuMapper228*)mmMalloc(sizeof(struct mmEmuMapper228));
    mmEmuMapper228_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper228_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper228* p = (struct mmEmuMapper228*)(m);
    mmEmuMapper228_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER228 =
{
    &__static_mmEmuMapper228_Produce,
    &__static_mmEmuMapper228_Recycle,
};
