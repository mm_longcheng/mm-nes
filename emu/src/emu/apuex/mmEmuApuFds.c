/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApuFds.h"

#include "emu/mmEmuState.h"

#include "core/mmString.h"

MM_EXPORT_EMU void mmEmuApuFds_Init(struct mmEmuApuFds* p)
{
    mmEmuApuInterface_Init(&p->super);

    mmMemset(&p->fds, 0, sizeof(struct mmEmuApuFdsSound));
    mmMemset(&p->fds_sync, 0, sizeof(struct mmEmuApuFdsSound));

    p->sampling_rate = 22050;

    mmMemset(&p->output_buf, 0, sizeof(mmInt_t) * 8);
    //
    p->super.Reset = &mmEmuApuFds_Reset;
    p->super.Setup = &mmEmuApuFds_Setup;
    p->super.Process = &mmEmuApuFds_Process;
    p->super.Write = &mmEmuApuFds_Write;
    p->super.Read = &mmEmuApuFds_Read;
    p->super.SyncWrite = &mmEmuApuFds_SyncWrite;
    p->super.SyncRead = &mmEmuApuFds_SyncRead;
    p->super.Sync = &mmEmuApuFds_Sync;
    p->super.GetFreq = &mmEmuApuFds_GetFreq;
    p->super.GetStateSize = &mmEmuApuFds_GetStateSize;
    p->super.SaveState = &mmEmuApuFds_SaveState;
    p->super.LoadState = &mmEmuApuFds_LoadState;
}
MM_EXPORT_EMU void mmEmuApuFds_Destroy(struct mmEmuApuFds* p)
{
    mmEmuApuInterface_Destroy(&p->super);

    mmMemset(&p->fds, 0, sizeof(struct mmEmuApuFdsSound));
    mmMemset(&p->fds_sync, 0, sizeof(struct mmEmuApuFdsSound));

    p->sampling_rate = 22050;

    mmMemset(&p->output_buf, 0, sizeof(mmInt_t) * 8);
}

MM_EXPORT_EMU void mmEmuApuFds_WriteSub(struct mmEmuApuFds* p, mmWord_t addr, mmByte_t data, struct mmEmuApuFdsSound* ch, double rate)
{
    if (addr < 0x4040 || addr > 0x40BF)
    {
        return;
    }

    ch->reg[addr - 0x4040] = data;
    if (addr >= 0x4040 && addr <= 0x407F)
    {
        if (ch->wave_setup)
        {
            ch->main_wavetable[addr - 0x4040] = 0x20 - ((mmInt_t)data & 0x3F);
        }
    }
    else
    {
        switch (addr)
        {
        case 0x4080:    // Volume Envelope
            ch->volenv_mode = data >> 6;
            if (data & 0x80)
            {
                ch->volenv_gain = data & 0x3F;

                // Immediate reflection.
                if (!ch->main_addr)
                {
                    ch->now_volume = (ch->volenv_gain < 0x21) ? ch->volenv_gain : 0x20;
                }
            }
            // Envelope one step operation.
            ch->volenv_decay = data & 0x3F;
            ch->volenv_phaseacc = (double)ch->envelope_speed * (double)(ch->volenv_decay + 1) * rate / (232.0*960.0);
            break;

        case 0x4082:    // Main Frequency(Low)
            ch->main_frequency = (ch->main_frequency&~0x00FF) | (mmInt_t)data;
            break;
        case 0x4083:    // Main Frequency(High)
            ch->main_enable = (~data)&(1 << 7);
            ch->envelope_enable = (~data)&(1 << 6);
            if (!ch->main_enable)
            {
                ch->main_addr = 0;
                ch->now_volume = (ch->volenv_gain < 0x21) ? ch->volenv_gain : 0x20;
            }
            // ch->main_frequency  = (ch->main_frequency&0x00FF)|(((mmInt_t)data&0x3F)<<8);
            ch->main_frequency = (ch->main_frequency & 0x00FF) | (((mmInt_t)data & 0x0F) << 8);
            break;

        case 0x4084:    // Sweep Envelope
            ch->swpenv_mode = data >> 6;
            if (data & 0x80)
            {
                ch->swpenv_gain = data & 0x3F;
            }
            // Envelope one step operation.
            ch->swpenv_decay = data & 0x3F;
            ch->swpenv_phaseacc = (double)ch->envelope_speed * (double)(ch->swpenv_decay + 1) * rate / (232.0*960.0);
            break;

        case 0x4085:    // Sweep Bias
            if (data & 0x40)
            {
                ch->sweep_bias = (data & 0x3f) - 0x40;
            }
            else
            {
                ch->sweep_bias = data & 0x3f;
            }
            ch->lfo_addr = 0;
            break;

        case 0x4086:    // Effector(LFO) Frequency(Low)
            ch->lfo_frequency = (ch->lfo_frequency&(~0x00FF)) | (mmInt_t)data;
            break;
        case 0x4087:    // Effector(LFO) Frequency(High)
            ch->lfo_enable = (~data & 0x80);
            ch->lfo_frequency = (ch->lfo_frequency & 0x00FF) | (((mmInt_t)data & 0x0F) << 8);
            break;

        case 0x4088:    // Effector(LFO) wavetable
            if (!ch->lfo_enable)
            {
                mmInt_t i = 0;
                // FIFO?
                for (i = 0; i < 31; i++)
                {
                    ch->lfo_wavetable[i * 2 + 0] = ch->lfo_wavetable[(i + 1) * 2 + 0];
                    ch->lfo_wavetable[i * 2 + 1] = ch->lfo_wavetable[(i + 1) * 2 + 1];
                }
                ch->lfo_wavetable[31 * 2 + 0] = data & 0x07;
                ch->lfo_wavetable[31 * 2 + 1] = data & 0x07;
            }
            break;

        case 0x4089:    // Sound control
        {
            static const mmInt_t tbl[] = { 30, 20, 15, 12 };
            ch->master_volume = tbl[data & 3];
            ch->wave_setup = data & 0x80;
        }
        break;

        case 0x408A:    // Sound control 2
            ch->envelope_speed = data;
            break;

        default:
            break;
        }
    }
}

MM_EXPORT_EMU void mmEmuApuFds_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    mmMemset(&p->fds, 0, sizeof(struct mmEmuApuFdsSound));
    mmMemset(&p->fds_sync, 0, sizeof(struct mmEmuApuFdsSound));

    p->sampling_rate = nRate;
}
MM_EXPORT_EMU void mmEmuApuFds_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    p->sampling_rate = nRate;
}
// Called from the APU renderer side.
MM_EXPORT_EMU void mmEmuApuFds_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);
    // Sampling rate standard.
    mmEmuApuFds_WriteSub(p, addr, data, &p->fds, (double)p->sampling_rate);
}
MM_EXPORT_EMU mmByte_t mmEmuApuFds_Read(struct mmEmuApuInterface* super, mmWord_t addr)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    mmByte_t data = addr >> 8;

    if (addr >= 0x4040 && addr <= 0x407F)
    {
        data = p->fds.main_wavetable[addr & 0x3F] | 0x40;
    }
    else
    {
        if (addr == 0x4090)
        {
            data = (p->fds.volenv_gain & 0x3F) | 0x40;
        }
        else
        {
            if (addr == 0x4092)
            {
                data = (p->fds.swpenv_gain & 0x3F) | 0x40;
            }
        }
    }
    return data;
}
MM_EXPORT_EMU mmInt_t mmEmuApuFds_Process(struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    mmInt_t sub_freq = 0;
    mmInt_t output = 0;

    // Envelope unit
    if (p->fds.envelope_enable && p->fds.envelope_speed)
    {
        // Volume envelope
        if (p->fds.volenv_mode < 2)
        {
            double decay = ((double)p->fds.envelope_speed * (double)(p->fds.volenv_decay + 1) * (double)p->sampling_rate) / (232.0*960.0);
            p->fds.volenv_phaseacc -= 1.0;
            while (p->fds.volenv_phaseacc < 0.0)
            {
                p->fds.volenv_phaseacc += decay;

                if (p->fds.volenv_mode == 0)
                {
                    // Decrement mode.
                    if (p->fds.volenv_gain)
                    {
                        p->fds.volenv_gain--;
                    }
                }
                else
                {
                    if (p->fds.volenv_mode == 1)
                    {
                        if (p->fds.volenv_gain < 0x20)
                        {
                            p->fds.volenv_gain++;
                        }
                    }
                }
            }
        }

        // Sweep envelope
        if (p->fds.swpenv_mode < 2)
        {
            double  decay = ((double)p->fds.envelope_speed * (double)(p->fds.swpenv_decay + 1) * (double)p->sampling_rate) / (232.0*960.0);
            p->fds.swpenv_phaseacc -= 1.0;
            while (p->fds.swpenv_phaseacc < 0.0)
            {
                p->fds.swpenv_phaseacc += decay;

                if (p->fds.swpenv_mode == 0)
                {
                    // Decrement mode.
                    if (p->fds.swpenv_gain)
                    {
                        p->fds.swpenv_gain--;
                    }
                }
                else
                {
                    if (p->fds.swpenv_mode == 1)
                    {
                        if (p->fds.swpenv_gain < 0x20)
                        {
                            p->fds.swpenv_gain++;
                        }
                    }
                }
            }
        }
    }

    // Effector(LFO) unit
    sub_freq = 0;
    //  if( fds.lfo_enable && fds.envelope_speed && fds.lfo_frequency ) {
    if (p->fds.lfo_enable)
    {
        mmInt_t sub_multi = 0;

        if (p->fds.lfo_frequency)
        {
            static const int tbl[8] = { 0, 1, 2, 4, 0, -4, -2, -1 };

            p->fds.lfo_phaseacc -= (1789772.5*(double)p->fds.lfo_frequency) / 65536.0;
            while (p->fds.lfo_phaseacc < 0.0)
            {
                p->fds.lfo_phaseacc += (double)p->sampling_rate;

                if (p->fds.lfo_wavetable[p->fds.lfo_addr] == 4)
                {
                    p->fds.sweep_bias = 0;
                }
                else
                {
                    p->fds.sweep_bias += tbl[p->fds.lfo_wavetable[p->fds.lfo_addr]];
                }

                p->fds.lfo_addr = (p->fds.lfo_addr + 1) & 63;
            }
        }

        if (p->fds.sweep_bias > 63)
        {
            p->fds.sweep_bias -= 128;
        }
        else if (p->fds.sweep_bias < -64)
        {
            p->fds.sweep_bias += 128;
        }

        sub_multi = p->fds.sweep_bias * p->fds.swpenv_gain;

        if (sub_multi & 0x0F)
        {
            // When it is not divisible by 16.
            sub_multi = (sub_multi / 16);
            if (p->fds.sweep_bias >= 0)
            {
                sub_multi += 2;    // In case of positive.
            }
            else
            {
                sub_multi -= 1;    // In case of negative.
            }
        }
        else
        {
            // When it is divisible by 16.
            sub_multi = (sub_multi / 16);
        }
        // If it exceeds 193 -258 (wrap to -64).
        if (sub_multi > 193)
        {
            sub_multi -= 258;
        }
        // If it falls below -64, it gets + 256 (wrap to 192).
        if (sub_multi < -64)
        {
            sub_multi += 256;
        }

        sub_freq = (p->fds.main_frequency) * sub_multi / 64;
    }

    // Main unit
    output = 0;
    if (p->fds.main_enable && p->fds.main_frequency && !p->fds.wave_setup)
    {
        mmInt_t freq;
        mmInt_t main_addr_old = p->fds.main_addr;

        freq = (mmInt_t)((p->fds.main_frequency + sub_freq) * 1789772.5 / 65536.0);

        p->fds.main_addr = (p->fds.main_addr + freq + 64 * p->sampling_rate) % (64 * p->sampling_rate);

        // Volume update after one cycle.
        if (main_addr_old > p->fds.main_addr)
        {
            p->fds.now_volume = (p->fds.volenv_gain < 0x21) ? p->fds.volenv_gain : 0x20;
        }

        output = p->fds.main_wavetable[(p->fds.main_addr / p->sampling_rate) & 0x3f] * 8 * p->fds.now_volume * p->fds.master_volume / 30;

        if (p->fds.now_volume)
        {
            p->fds.now_freq = freq * 4;
        }
        else
        {
            p->fds.now_freq = 0;
        }
    }
    else
    {
        p->fds.now_freq = 0;
        output = 0;
    }

    // LPF
#if 1
    output = (p->output_buf[0] * 2 + output) / 3;
    p->output_buf[0] = output;
#else
    output = (p->output_buf[0] + p->output_buf[1] + output) / 3;
    p->output_buf[0] = p->output_buf[1];
    p->output_buf[1] = output;
#endif

    p->fds.output = output;
    return p->fds.output;
}

MM_EXPORT_EMU void mmEmuApuFds_SyncWrite(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    // Clock reference.
    mmEmuApuFds_WriteSub(p, addr, data, &p->fds_sync, 1789772.5);
}
MM_EXPORT_EMU mmByte_t mmEmuApuFds_SyncRead(struct mmEmuApuInterface* super, mmWord_t addr)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    mmByte_t data = addr >> 8;

    if (addr >= 0x4040 && addr <= 0x407F)
    {
        data = p->fds_sync.main_wavetable[addr & 0x3F] | 0x40;
    }
    else
    {
        if (addr == 0x4090)
        {
            data = (p->fds_sync.volenv_gain & 0x3F) | 0x40;
        }
        else
        {
            if (addr == 0x4092)
            {
                data = (p->fds_sync.swpenv_gain & 0x3F) | 0x40;
            }
        }
    }
    return data;
}
MM_EXPORT_EMU mmBool_t mmEmuApuFds_Sync(struct mmEmuApuInterface* super, mmInt_t cycles)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    // Envelope unit
    if (p->fds_sync.envelope_enable && p->fds_sync.envelope_speed)
    {
        // Volume envelope
        double decay;
        if (p->fds_sync.volenv_mode < 2)
        {
            decay = ((double)p->fds_sync.envelope_speed * (double)(p->fds_sync.volenv_decay + 1) * 1789772.5) / (232.0*960.0);
            p->fds_sync.volenv_phaseacc -= (double)cycles;
            while (p->fds_sync.volenv_phaseacc < 0.0)
            {
                p->fds_sync.volenv_phaseacc += decay;

                if (p->fds_sync.volenv_mode == 0)
                {
                    // Decrement mode.
                    if (p->fds_sync.volenv_gain)
                    {
                        p->fds_sync.volenv_gain--;
                    }
                }
                else
                {
                    if (p->fds_sync.volenv_mode == 1)
                    {
                        // Increment mode.
                        if (p->fds_sync.volenv_gain < 0x20)
                        {
                            p->fds_sync.volenv_gain++;
                        }
                    }
                }
            }
        }

        // Sweep envelope
        if (p->fds_sync.swpenv_mode < 2)
        {
            decay = ((double)p->fds_sync.envelope_speed * (double)(p->fds_sync.swpenv_decay + 1) * 1789772.5) / (232.0*960.0);
            p->fds_sync.swpenv_phaseacc -= (double)cycles;
            while (p->fds_sync.swpenv_phaseacc < 0.0)
            {
                p->fds_sync.swpenv_phaseacc += decay;

                if (p->fds_sync.swpenv_mode == 0)
                {
                    // Decrement mode.
                    if (p->fds_sync.swpenv_gain)
                    {
                        p->fds_sync.swpenv_gain--;
                    }
                }
                else
                {
                    if (p->fds_sync.swpenv_mode == 1)
                    {
                        // Increment mode.
                        if (p->fds_sync.swpenv_gain < 0x20)
                        {
                            p->fds_sync.swpenv_gain++;
                        }
                    }
                }
            }
        }
    }
    return MM_FALSE;
}

MM_EXPORT_EMU mmInt_t mmEmuApuFds_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    return p->fds.now_freq;
}

MM_EXPORT_EMU mmInt_t mmEmuApuFds_GetStateSize(const struct mmEmuApuInterface* super)
{
    // struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    return
        sizeof(struct mmEmuApuFdsSound) +
        sizeof(struct mmEmuApuFdsSound);
}
MM_EXPORT_EMU void mmEmuApuFds_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    MM_EMU_SETBLOCK(buffer, &p->fds, sizeof(struct mmEmuApuFdsSound));
    MM_EMU_SETBLOCK(buffer, &p->fds_sync, sizeof(struct mmEmuApuFdsSound));
}
MM_EXPORT_EMU void mmEmuApuFds_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuFds* p = (struct mmEmuApuFds*)(super);

    MM_EMU_GETBLOCK(buffer, &p->fds, sizeof(struct mmEmuApuFdsSound));
    MM_EMU_GETBLOCK(buffer, &p->fds_sync, sizeof(struct mmEmuApuFdsSound));
}

