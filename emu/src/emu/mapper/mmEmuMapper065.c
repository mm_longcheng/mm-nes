/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper065.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper065_Init(struct mmEmuMapper065* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    //
    p->super.Reset = &mmEmuMapper065_Reset;
    p->super.Write = &mmEmuMapper065_Write;
    //p->super.Read = &mmEmuMapper065_Read;
    //p->super.ReadLow = &mmEmuMapper065_ReadLow;
    //p->super.WriteLow = &mmEmuMapper065_WriteLow;
    //p->super.ExRead = &mmEmuMapper065_ExRead;
    //p->super.ExWrite = &mmEmuMapper065_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper065_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper065_ExCmdWrite;
    p->super.HSync = &mmEmuMapper065_HSync;
    //p->super.VSync = &mmEmuMapper065_VSync;
    p->super.Clock = &mmEmuMapper065_Clock;
    //p->super.PPULatch = &mmEmuMapper065_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper065_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper065_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper065_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper065_IsStateSave;
    p->super.SaveState = &mmEmuMapper065_SaveState;
    p->super.LoadState = &mmEmuMapper065_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper065_Destroy(struct mmEmuMapper065* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}

MM_EXPORT_EMU void mmEmuMapper065_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper065* p = (struct mmEmuMapper065*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xe30b7f64)          // Kaiketsu Yanchamaru 3(J)
    {
        p->patch = 1;
    }

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->irq_enable = 0;
    p->irq_counter = 0;
}
MM_EXPORT_EMU void mmEmuMapper065_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper065* p = (struct mmEmuMapper065*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;

    case 0x9000:
        if (!p->patch)
        {
            if (data & 0x40)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
        }
        break;

    case 0x9001:
        if (p->patch)
        {
            if (data & 0x80)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;

    case 0x9003:
        if (!p->patch)
        {
            p->irq_enable = data & 0x80;
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        break;
    case 0x9004:
        if (!p->patch)
        {
            p->irq_counter = p->irq_latch;
        }
        break;
    case 0x9005:
        if (p->patch)
        {
            p->irq_counter = (mmByte_t)(data << 1);
            p->irq_enable = data;
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        else
        {
            p->irq_latch = (p->irq_latch & 0x00FF) | ((mmInt_t)data << 8);
        }
        break;

    case 0x9006:
        if (p->patch)
        {
            p->irq_enable = 1;
        }
        else
        {
            p->irq_latch = (p->irq_latch & 0xFF00) | data;
        }
        break;

    case 0xB000:
    case 0xB001:
    case 0xB002:
    case 0xB003:
    case 0xB004:
    case 0xB005:
    case 0xB006:
    case 0xB007:
        mmEmuMmu_SetVROM1KBank(mmu, addr & 0x0007, data);
        break;

    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0xC000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper065_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper065* p = (struct mmEmuMapper065*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (p->patch)
    {
        if (p->irq_enable)
        {
            if (p->irq_counter == 0)
            {
                //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
            else
            {
                p->irq_counter--;
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper065_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper065* p = (struct mmEmuMapper065*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (!p->patch)
    {
        if (p->irq_enable)
        {
            if (p->irq_counter <= 0)
            {
                //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
            else
            {
                p->irq_counter -= cycles;
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper065_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper065_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper065* p = (struct mmEmuMapper065*)(super);

    buffer[0] = p->irq_enable;
    *(mmInt_t*)&buffer[1] = p->irq_counter;
    *(mmInt_t*)&buffer[5] = p->irq_latch;
}
MM_EXPORT_EMU void mmEmuMapper065_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper065* p = (struct mmEmuMapper065*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = *(mmInt_t*)&buffer[1];
    p->irq_latch = *(mmInt_t*)&buffer[5];
}

static struct mmEmuMapper* __static_mmEmuMapper065_Produce(void)
{
    struct mmEmuMapper065* p = (struct mmEmuMapper065*)mmMalloc(sizeof(struct mmEmuMapper065));
    mmEmuMapper065_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper065_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper065* p = (struct mmEmuMapper065*)(m);
    mmEmuMapper065_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER065 =
{
    &__static_mmEmuMapper065_Produce,
    &__static_mmEmuMapper065_Recycle,
};
