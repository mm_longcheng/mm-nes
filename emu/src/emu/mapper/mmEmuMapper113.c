/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper113.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper113_Init(struct mmEmuMapper113* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper113_Reset;
    p->super.Write = &mmEmuMapper113_Write;
    //p->super.Read = &mmEmuMapper113_Read;
    //p->super.ReadLow = &mmEmuMapper113_ReadLow;
    p->super.WriteLow = &mmEmuMapper113_WriteLow;
    //p->super.ExRead = &mmEmuMapper113_ExRead;
    //p->super.ExWrite = &mmEmuMapper113_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper113_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper113_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper113_HSync;
    //p->super.VSync = &mmEmuMapper113_VSync;
    //p->super.Clock = &mmEmuMapper113_Clock;
    //p->super.PPULatch = &mmEmuMapper113_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper113_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper113_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper113_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper113_IsStateSave;
    //p->super.SaveState = &mmEmuMapper113_SaveState;
    //p->super.LoadState = &mmEmuMapper113_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper113_Destroy(struct mmEmuMapper113* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper113_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper113* p = (struct mmEmuMapper113*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmEmuMmu_SetPROM32KBank(mmu, 0, 1, mmu->PROM_08K_SIZE-2, mmu->PROM_08K_SIZE-1);
    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper113_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper113* p = (struct mmEmuMapper113*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = mmEmuRom_GetPROMCRC(&nes->rom);

    //DEBUGOUT( "$%04X:$%02X L=%3d\n", addr, data, nes->GetScanline() );
    switch (addr)
    {
    case 0x4100:
    case 0x4111:
    case 0x4120:
    case 0x4194:
    case 0x4195:
    case 0x4900:
        if (crc == 0xA75AEDE5)          // HES 6-in-1
        {
            if (data & 0x80)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
        }
        mmEmuMmu_SetPROM32KBank(mmu, data >> 3);
        mmEmuMmu_SetVROM8KBank(mmu, ((data >> 3) & 0x08) + (data & 0x07));
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper113_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper113* p = (struct mmEmuMapper113*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "$%04X:$%02X L=%3d\n", addr, data, nes->GetScanline() );
    switch (addr)
    {
    case 0x8008:
    case 0x8009:
        mmEmuMmu_SetPROM32KBank(mmu, data >> 3);
        mmEmuMmu_SetVROM8KBank(mmu, ((data >> 3) & 0x08) + (data & 0x07));
        break;
    case 0x8E66:
    case 0x8E67:
        mmEmuMmu_SetVROM8KBank(mmu, (data & 0x07) ? 0 : 1);
        break;
    case 0xE00A:
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper113_Produce(void)
{
    struct mmEmuMapper113* p = (struct mmEmuMapper113*)mmMalloc(sizeof(struct mmEmuMapper113));
    mmEmuMapper113_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper113_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper113* p = (struct mmEmuMapper113*)(m);
    mmEmuMapper113_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER113 =
{
    &__static_mmEmuMapper113_Produce,
    &__static_mmEmuMapper113_Recycle,
};
