/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper255.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper255_Init(struct mmEmuMapper255* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 0;
    p->reg[2] = 0;
    p->reg[3] = 0;

    //
    p->super.Reset = &mmEmuMapper255_Reset;
    p->super.Write = &mmEmuMapper255_Write;
    //p->super.Read = &mmEmuMapper255_Read;
    p->super.ReadLow = &mmEmuMapper255_ReadLow;
    p->super.WriteLow = &mmEmuMapper255_WriteLow;
    //p->super.ExRead = &mmEmuMapper255_ExRead;
    //p->super.ExWrite = &mmEmuMapper255_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper255_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper255_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper255_HSync;
    //p->super.VSync = &mmEmuMapper255_VSync;
    //p->super.Clock = &mmEmuMapper255_Clock;
    //p->super.PPULatch = &mmEmuMapper255_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper255_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper255_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper255_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper255_IsStateSave;
    p->super.SaveState = &mmEmuMapper255_SaveState;
    p->super.LoadState = &mmEmuMapper255_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper255_Destroy(struct mmEmuMapper255* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 0;
    p->reg[2] = 0;
    p->reg[3] = 0;
}

MM_EXPORT_EMU void mmEmuMapper255_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper255* p = (struct mmEmuMapper255*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);

    p->reg[0] = 0;
    p->reg[1] = 0;
    p->reg[2] = 0;
    p->reg[3] = 0;
}
MM_EXPORT_EMU mmByte_t mmEmuMapper255_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper255* p = (struct mmEmuMapper255*)(super);

    if (addr >= 0x5800)
    {
        return p->reg[addr & 0x0003] & 0x0F;
    }
    else
    {
        return addr >> 8;
    }
}
MM_EXPORT_EMU void mmEmuMapper255_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper255* p = (struct mmEmuMapper255*)(super);

    if (addr >= 0x5800)
    {
        p->reg[addr & 0x0003] = data & 0x0F;
    }
}
MM_EXPORT_EMU void mmEmuMapper255_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper255* p = (struct mmEmuMapper255*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t prg = (addr & 0x0F80) >> 7;
    mmInt_t chr = (addr & 0x003F);
    mmInt_t bank = (addr & 0x4000) >> 14;

    if (addr & 0x2000)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }

    if (addr & 0x1000)
    {
        if (addr & 0x0040)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, 0x80 * bank + prg * 4 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 5, 0x80 * bank + prg * 4 + 3);
            mmEmuMmu_SetPROM8KBank(mmu, 6, 0x80 * bank + prg * 4 + 2);
            mmEmuMmu_SetPROM8KBank(mmu, 7, 0x80 * bank + prg * 4 + 3);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, 0x80 * bank + prg * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 5, 0x80 * bank + prg * 4 + 1);
            mmEmuMmu_SetPROM8KBank(mmu, 6, 0x80 * bank + prg * 4 + 0);
            mmEmuMmu_SetPROM8KBank(mmu, 7, 0x80 * bank + prg * 4 + 1);
        }
    }
    else
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, 0x80 * bank + prg * 4 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, 0x80 * bank + prg * 4 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, 0x80 * bank + prg * 4 + 2);
        mmEmuMmu_SetPROM8KBank(mmu, 7, 0x80 * bank + prg * 4 + 3);
    }

    mmEmuMmu_SetVROM1KBank(mmu, 0, 0x200 * bank + chr * 8 + 0);
    mmEmuMmu_SetVROM1KBank(mmu, 1, 0x200 * bank + chr * 8 + 1);
    mmEmuMmu_SetVROM1KBank(mmu, 2, 0x200 * bank + chr * 8 + 2);
    mmEmuMmu_SetVROM1KBank(mmu, 3, 0x200 * bank + chr * 8 + 3);
    mmEmuMmu_SetVROM1KBank(mmu, 4, 0x200 * bank + chr * 8 + 4);
    mmEmuMmu_SetVROM1KBank(mmu, 5, 0x200 * bank + chr * 8 + 5);
    mmEmuMmu_SetVROM1KBank(mmu, 6, 0x200 * bank + chr * 8 + 6);
    mmEmuMmu_SetVROM1KBank(mmu, 7, 0x200 * bank + chr * 8 + 7);
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper255_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper255_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper255* p = (struct mmEmuMapper255*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->reg[3];
}
MM_EXPORT_EMU void mmEmuMapper255_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper255* p = (struct mmEmuMapper255*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->reg[3] = buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper255_Produce(void)
{
    struct mmEmuMapper255* p = (struct mmEmuMapper255*)mmMalloc(sizeof(struct mmEmuMapper255));
    mmEmuMapper255_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper255_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper255* p = (struct mmEmuMapper255*)(m);
    mmEmuMapper255_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER255 =
{
    &__static_mmEmuMapper255_Produce,
    &__static_mmEmuMapper255_Recycle,
};
