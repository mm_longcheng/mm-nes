/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadVSZapper.h"
#include "emu/mmEmuNes.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuPadVSZapper_Init(struct mmEmuPadVSZapper* p)
{
    mmEmuExpad_Init(&p->super);

    mmMemset(p->readlatch, 0, sizeof(mmByte_t) * 2);

    p->zapper_x = 0;
    p->zapper_y = 0;

    p->zapper_button = 0;
    p->zapper_offscreen = 0;

    //
    p->super.Reset = &mmEmuPadVSZapper_Reset;
    p->super.Strobe = &mmEmuPadVSZapper_Strobe;
    p->super.Read4016 = &mmEmuPadVSZapper_Read4016;
    p->super.Read4017 = &mmEmuPadVSZapper_Read4017;
    //p->super.Write4016 = &mmEmuPadVSZapper_Write4016;
    //p->super.Write4017 = &mmEmuPadVSZapper_Write4017;
    p->super.Sync = &mmEmuPadVSZapper_Sync;
    p->super.SetSyncData = &mmEmuPadVSZapper_SetSyncData;
    p->super.GetSyncData = &mmEmuPadVSZapper_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadVSZapper_Destroy(struct mmEmuPadVSZapper* p)
{
    mmEmuExpad_Destroy(&p->super);

    mmMemset(p->readlatch, 0, sizeof(mmByte_t) * 2);

    p->zapper_x = 0;
    p->zapper_y = 0;

    p->zapper_button = 0;
    p->zapper_offscreen = 0;
}

MM_EXPORT_EMU void mmEmuPadVSZapper_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)(super);

    struct mmLogger* gLogger = mmLogger_Instance();

    mmLogger_LogD(gLogger, "%s %d.", __FUNCTION__, __LINE__);

    p->readlatch[0] = 0;
    p->readlatch[1] = 0;
    p->zapper_button = 0;
    p->zapper_offscreen = 0;
}
MM_EXPORT_EMU void mmEmuPadVSZapper_Strobe(struct mmEmuExpad* super)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)(super);

    p->readlatch[0] = 0x10;
    p->readlatch[1] = 0x00;

    if (p->zapper_button)
    {
        p->readlatch[0] |= (p->zapper_button & 0x01) ? 0x80 : 0x00;
        p->zapper_offscreen = p->zapper_button & 0x02;
    }

    if (mmEmuNes_GetZapperHit(p->super.nes))
    {
        if (mmEmuScreen_GetZapperHit(&p->super.nes->screen) >= 0x40)
        {
            p->readlatch[0] |= 0x40;
        }
    }

    // Fire offscreen with right mouse button
    if (p->zapper_offscreen)
    {
        p->readlatch[0] &= ~0x40;
    }

    // Fire at the screen with left mouse button
    if (p->zapper_button & 0x01)
    {
        p->zapper_button = 0;
    }
}

MM_EXPORT_EMU mmByte_t mmEmuPadVSZapper_Read4016(struct mmEmuExpad* super)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmWord_t* nVSUnisystem = p->super.nes->config.controller.nVSUnisystem;

    mmByte_t data = 0x00;

    data = p->readlatch[0] & 0x01;
    p->readlatch[0] >>= 1;

    // Coin 1
    if (mmEmuController_ButtonCheckKeys(controller, 0, nVSUnisystem))
    {
        data |= 0x20;
    }
    // Coin 2
    if (mmEmuController_ButtonCheckKeys(controller, 1, nVSUnisystem))
    {
        data |= 0x40;
    }
    // Service
    if (mmEmuController_ButtonCheckKeys(controller, 2, nVSUnisystem))
    {
        data |= 0x04;
    }

    // Dip-Switch   
    data |= ((mmEmuNes_GetVSDipSwitch(p->super.nes) << 3) & 0x18);

    return data;
}
MM_EXPORT_EMU mmByte_t mmEmuPadVSZapper_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)(super);

    mmByte_t data = 0x00;

    data = p->readlatch[1] & 0x01;
    p->readlatch[1] >>= 1;

    // Dip-Switch
    data |= (mmEmuNes_GetVSDipSwitch(p->super.nes) & 0xFC);

    return data;
}

MM_EXPORT_EMU void mmEmuPadVSZapper_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmEmuNes_GetZapperPos(p->super.nes, &p->zapper_x, &p->zapper_y);

    p->zapper_button = 0;
    if (mmEmuController_MouseButtonStatus(controller, MM_MB_LBUTTON))
    {
        p->zapper_button |= 0x01;
    }
    if (mmEmuController_MouseButtonStatus(controller, MM_MB_RBUTTON))
    {
        p->zapper_button |= 0x02;
    }
}
MM_EXPORT_EMU void mmEmuPadVSZapper_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)(super);

    if (type == 0)
    {
        p->zapper_x = data;
    }
    else if (type == 1)
    {
        p->zapper_y = data;
    }
    else if (type == 2)
    {
        p->zapper_button = data & 0xFF;
        p->readlatch[0] = ((mmByte_t)((data >> 8) & 0x000000FF));
        p->readlatch[1] = ((mmByte_t)((data >> 16) & 0x000000FF));
    }
}
MM_EXPORT_EMU mmLong_t mmEmuPadVSZapper_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)(super);

    mmLong_t data = 0;

    if (type == 0)
    {
        data = p->zapper_x;
    }
    else if (type == 1)
    {
        data = p->zapper_y;
    }
    else if (type == 2)
    {
        data =
            (p->zapper_button & 0xFF) |
            ((p->readlatch[0] & 0xFF) << 8) |
            ((p->readlatch[1] & 0xFF) << 16);
    }
    return data;
}

static struct mmEmuExpad* __static_mmEmuPadVSZapper_Produce(void)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)mmMalloc(sizeof(struct mmEmuPadVSZapper));
    mmEmuPadVSZapper_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadVSZapper_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadVSZapper* p = (struct mmEmuPadVSZapper*)(m);
    mmEmuPadVSZapper_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_VSZAPPER =
{
    &__static_mmEmuPadVSZapper_Produce,
    &__static_mmEmuPadVSZapper_Recycle,
};
