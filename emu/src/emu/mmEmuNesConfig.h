/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuNesConfig_h__
#define __mmEmuNesConfig_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuPathConfig
{
    struct mmString szIpsPath;
    struct mmString szTonePath;
    struct mmString szGeniePath;

    struct mmString szSnapshotPath;
    struct mmString szCheatCodePath;
    struct mmString szTurboPath;
    struct mmString szMoviePath;
    struct mmString szStatePath;
    struct mmString szTapePath;
    struct mmString szSavePath;
    struct mmString szDiskPath;
    struct mmString szNetworkPath;
};

MM_EXPORT_EMU void mmEmuPathConfig_Init(struct mmEmuPathConfig* p);
MM_EXPORT_EMU void mmEmuPathConfig_Destroy(struct mmEmuPathConfig* p);

MM_EXPORT_EMU void mmEmuPathConfig_Default(struct mmEmuPathConfig* p);

struct mmEmuEmulatorConfig
{
    mmBool_t    bIllegalOp;
    mmBool_t    bAutoFrameSkip;
    mmBool_t    bThrottle;
    mmInt_t     nThrottleFPS;
    mmBool_t    bBackground;
    mmInt_t     nPriority;
    mmBool_t    bFourPlayer;
    mmBool_t    bCrcCheck;
    mmBool_t    bDiskThrottle;
    mmBool_t    bLoadFullscreen;
    mmBool_t    bPNGsnapshot;
    mmBool_t    bAutoIPS;
};

MM_EXPORT_EMU void mmEmuEmulatorConfig_Init(struct mmEmuEmulatorConfig* p);
MM_EXPORT_EMU void mmEmuEmulatorConfig_Destroy(struct mmEmuEmulatorConfig* p);

MM_EXPORT_EMU void mmEmuEmulatorConfig_Default(struct mmEmuEmulatorConfig* p);

struct mmEmuGraphicsConfig
{
    mmBool_t        bAspect;
    mmBool_t        bAllSprite;
    mmBool_t        bAllLine;
    mmBool_t        bFPSDisp;
    mmBool_t        bTVFrame;
    mmBool_t        bScanline;
    mmInt_t         nScanlineColor;
    mmBool_t        bSyncDraw;
    mmBool_t        bFitZoom;

    mmBool_t        bLeftClip;

    mmBool_t        bWindowVSync;

    mmBool_t        bSyncNoSleep;

    mmBool_t        bDiskAccessLamp;

    mmBool_t        bDoubleSize;
    mmBool_t        bSystemMemory;
    mmBool_t        bUseHEL;

    mmBool_t        bNoSquareList;

    mmInt_t         nGraphicsFilter;

    mmUInt32_t      dwDisplayWidth;
    mmUInt32_t      dwDisplayHeight;
    mmUInt32_t      dwDisplayDepth;
    mmUInt32_t      dwDisplayRate;

    mmBool_t        bPaletteFile;
    struct mmString szPaletteFile;
};

MM_EXPORT_EMU void mmEmuGraphicsConfig_Init(struct mmEmuGraphicsConfig* p);
MM_EXPORT_EMU void mmEmuGraphicsConfig_Destroy(struct mmEmuGraphicsConfig* p);

MM_EXPORT_EMU void mmEmuGraphicsConfig_Default(struct mmEmuGraphicsConfig* p);

struct mmEmuAudioConfig
{
    mmBool_t    bEnable;
    mmInt_t     nRate;
    mmInt_t     nBits;
    mmInt_t     nBufferSize;

    mmInt_t     nFilterType;

    mmBool_t    bChangeTone;

    mmBool_t    bDisableVolumeEffect;
    mmBool_t    bExtraSoundEnable;

    //  0:Master
    //  1:Rectangle 1
    //  2:Rectangle 2
    //  3:Triangle
    //  4:Noise
    //  5:DPCM
    //  6:VRC6
    //  7:VRC7
    //  8:FDS
    //  9:MMC5
    // 10:N106
    // 11:FME7
    mmShort_t   nVolume[16];
};

MM_EXPORT_EMU void mmEmuAudioConfig_Init(struct mmEmuAudioConfig* p);
MM_EXPORT_EMU void mmEmuAudioConfig_Destroy(struct mmEmuAudioConfig* p);

MM_EXPORT_EMU void mmEmuAudioConfig_Default(struct mmEmuAudioConfig* p);

// SetVolume for all.
MM_EXPORT_EMU void mmEmuAudioConfig_SetVolumeAll(struct mmEmuAudioConfig* p, mmShort_t nVolume);

enum
{
    MM_EMU_CONTROLLER_KEY_UP      =  0,
    MM_EMU_CONTROLLER_KEY_DOWN    =  1,
    MM_EMU_CONTROLLER_KEY_LIFT    =  2,
    MM_EMU_CONTROLLER_KEY_RIGHT   =  3,
    MM_EMU_CONTROLLER_KEY_A       =  4,
    MM_EMU_CONTROLLER_KEY_B       =  5,
    MM_EMU_CONTROLLER_KEY_A_RAPID =  6,
    MM_EMU_CONTROLLER_KEY_B_RAPID =  7,
    MM_EMU_CONTROLLER_KEY_SELECT  =  8,
    MM_EMU_CONTROLLER_KEY_START   =  9,
    MM_EMU_CONTROLLER_KEY_MIC     = 10,
};

struct mmEmuControllerConfig
{
    mmWord_t    nButton[4][64];
    mmWord_t    nRapid[4][2];
    // 0:Crazy Climber
    // 1:Famly Trainer
    // 2:Exciting Boxing
    // 3:Mahjang
    mmWord_t    nExButton[4][64];
    mmWord_t    nNsfButton[64];
    mmWord_t    nVSUnisystem[64];
};

MM_EXPORT_EMU void mmEmuControllerConfig_Init(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_Destroy(struct mmEmuControllerConfig* p);

MM_EXPORT_EMU void mmEmuControllerConfig_Default(struct mmEmuControllerConfig* p);
// 0 Up       W
// 1 Down     S
// 2 Lift     A
// 3 Right    D
// 4 A        K
// 5 B        J
// 6 A Rapid  I
// 7 B Rapid  U
// 8 Select   V
// 9 Start    B
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultController1(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultController2(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultController3(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultController4(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultExController0(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultExController1(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultExController2(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultExController3(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultNsfController(struct mmEmuControllerConfig* p);
MM_EXPORT_EMU void mmEmuControllerConfig_DefaultVSUnisystem(struct mmEmuControllerConfig* p);

struct mmEmuMovieConfig
{
    mmByte_t    bUsePlayer[4];
    mmBool_t    bRerecord;
    mmBool_t    bLoopPlay;
    mmBool_t    bResetRec;
    mmBool_t    bPadDisplay;
    mmBool_t    bTimeDisplay;
};

MM_EXPORT_EMU void mmEmuMovieConfig_Init(struct mmEmuMovieConfig* p);
MM_EXPORT_EMU void mmEmuMovieConfig_Destroy(struct mmEmuMovieConfig* p);

MM_EXPORT_EMU void mmEmuMovieConfig_Default(struct mmEmuMovieConfig* p);

/*
 * http://problemkaputt.de/everynes.htm#cpu65xxmicroprocessor
 * PPU Dimensions & Timings
 *
 * Below are timings for Nintendo's NTSC and PAL consoles,
 * and for the Dendy (a russian Famicom clone with PAL output and Famicom-like NTSC-style timings).
 *
 *
 * Clock Speeds
 *   Type               NTSC               PAL                 Dendy
 *   Master Clock (X1)  21.47727MHz        26.6017125MHz       Like PAL
 *   Color Clock        3.579545MHz=X1/6   4.43361875MHz=X1/6  Like PAL
 *   Dot Clock          5.3693175MHz=X1/4  5.3203425MHz=X1/5   Like PAL
 *   CPU Clock          1.7897725MHz=X1/12 1.66260703MHz=X1/16 1.773448MHz=X1/15
 *   Frame Rate         60.09914261Hz      50.00697891Hz       Like PAL
 * Note: Above NTSC values are rounded. The exact NTSC Color Clock should be 315/88MHz.
 *
 * Vertical Timings (in scanlines)
 *   NTSC PAL  Dendy
 *   ?    1    ?           Pre-Render Time
 *   240? 239  240?        Rendering Time
 *   1    1    51?         Post-Render Time
 *   20   70   20?         Vblank
 *   1    1    1?          Post-Blank Time
 *   ---- ---- ----        -------
 *   262  312  312?        Total number of Scanlines
 *
 * Horizontal Timings (in dots)
 *   NTSC PAL  Dendy
 *   25x  252  25x         Rendering Time
 *   xx   89   xx          Hblank
 *   ---- ---- ----        -------
 *   341  341  341?        Total number of dots per scanline
 * In each second frame, NTSC does output a short scanline with only 340 dots,
 * this happens only if BG and/or OBJ are enabled.

 * CPU vs PPU Timings
 *   Type                       NTSC            PAL             Dendy
 *   Dots per CPU Clk           3.0 (12/4)      3.2 (16/5)      3.0 (15/5)
 *   CPU Clks per Scanline      113.6666        106.5625
 *   CPU Clks per One Frame     29780.66        33247.5
 *   CPU Clks per Other Frame   29780.33        33247.5
 *   CPU Clks per Two Frames    59561.0         66495.0
 *
 * Modulator vs PPU Timings
 *   Type                       NTSC            PAL             Dendy
 *   Dots per Color Clk         1.5 (6/4)       1.2 (6/5)       1.2 (6/5)
 *   Color Clks per Scanline    227.3333        284.1666
 *   Color Clks per One Frame   59561.33        88660.0
 *   Color Clks per Other Frame 59560.66        88660.0
 *   Color Clks per Two Frames  119122.0        177320.0
 *
 *
 * http://wiki.nesdev.com/w/index.php/CPU
 *
 *  CPU signals and frequencies
 *  The CPU generates its clock signal by dividing the master clock signal.
 *
 *                   Rate                     |        NTSC NES/Famicom         |             PAL NES              |    Dendy
 *   ------------------------------------------------------------------------------------------------------------------------------------
 *   Color subcarrier frequency fsc (exact)   | 21477272.72 Hz (39375000/11 Hz) | 4433618.75 Hz                    | 4433618.75 Hz
 *   Color subcarrier frequency fsc (approx.) | 3.579545 MHz                    | 4.433619 MHz                     | 4.433619 MHz
 *   Master clock frequency 6fsc              | 21.477272 MHz                   | 26.601712 MHz                    | 26.601712 MHz
 *   Clock divisor d                          | 12                              | 16                               | 15
 *   CPU clock frequency 6fsc/d               | 1.789773 MHz (~559 ns per cycle)| 1.662607 MHz (~601 ns per cycle) | 1.773448 MHz (~564 ns per cycle)
 *   ------------------------------------------------------------------------------------------------------------------------------------
 */

 // default model enum.
enum mmEmuVideoMode_t
{
    MM_EMU_NES_MODEL_NTSC  = 0,
    MM_EMU_NES_MODEL_PAL   = 1,
    MM_EMU_NES_MODEL_DENDY = 2,
};

MM_EXPORT_EMU extern const char* MM_EMU_VIDEO_MODE[3];

MM_EXPORT_EMU const char* mmEmuVideoMode_String(int type);

struct mmEmuClockConfig
{
    double MasterClock;       // Hz
    double ColorClockFactor;  // Hz Color Clock = MasterClock / ColorClockFactor
    double DotClockFactor;    // Hz   Dot Clock = MasterClock / DotClock_Factor
    double CPUClockFactor;    // Hz   CPU Clock = MasterClock / CPUClock_Factor
    double FrameRate;         // Frame number / 1 second

    mmInt_t NormalScanlines;  // PPU Normal Scanlines.
    mmInt_t TotalScanlines;   // Vertical Timings (in scanlines)
};
MM_EXPORT_EMU void mmEmuClockConfig_Init(struct mmEmuClockConfig* p);
MM_EXPORT_EMU void mmEmuClockConfig_Destroy(struct mmEmuClockConfig* p);

MM_EXPORT_EMU extern const struct mmEmuClockConfig MM_EMU_CLOCK_NTSC;
MM_EXPORT_EMU extern const struct mmEmuClockConfig MM_EMU_CLOCK_PAL;
MM_EXPORT_EMU extern const struct mmEmuClockConfig MM_EMU_CLOCK_DENDY;

MM_EXPORT_EMU const struct mmEmuClockConfig* mmEmuClockConfig_ModeConfig(mmInt_t nMode);

struct mmEmuVideoFormatConfig
{
    double BaseClock;           // Clock Speeds [Master Clock] = X1 MHz * 1000000.0
    double CpuClock;            // Clock Speeds [CPU Clock]

    mmInt_t FrameRate;          // Clock Speeds [Frame Rate]
    double FramePeriod;         // 1000.0 / FrameRate

    mmInt_t NormalScanlines;    // PPU Normal Scanlines.
    mmInt_t TotalScanlines;     // Vertical Timings (in scanlines)

    mmInt_t FrameIrqCycles;     // (int)(CpuClock / FrameRate)

    mmInt_t ScanlineCycles;     // (FrameIrqCycles * 12.0) / FrameRate

    mmInt_t FrameCycles;        // TotalScanlines * ScanlineCycles

    mmInt_t HDrawCycles;        // NTSC: 1024 PAL:  960 DENDY: 1024   = ScanlineCycles * 0.75
    mmInt_t HBlankCycles;       // NTSC:  340 PAL:  318 DENDY:  340   = ScanlineCycles - HDrawCycles
    mmInt_t ScanlineEndCycles;  // NTSC:    4 PAL:    2 DENDY:    4   = ScanlineCycles - 8 * (128 + 2 * 10 + 22)
};
MM_EXPORT_EMU void mmEmuVideoFormatConfig_Init(struct mmEmuVideoFormatConfig* p);
MM_EXPORT_EMU void mmEmuVideoFormatConfig_Destroy(struct mmEmuVideoFormatConfig* p);
MM_EXPORT_EMU void mmEmuVideoFormatConfig_Default(struct mmEmuVideoFormatConfig* p);
MM_EXPORT_EMU void mmEmuVideoFormatConfig_FromClockConfig(struct mmEmuVideoFormatConfig* p, const struct mmEmuClockConfig* clock_config);

MM_EXPORT_EMU extern const struct mmEmuVideoFormatConfig MM_EMU_VIDEO_FORMAT_NTSC;
MM_EXPORT_EMU extern const struct mmEmuVideoFormatConfig MM_EMU_VIDEO_FORMAT_PAL;
MM_EXPORT_EMU extern const struct mmEmuVideoFormatConfig MM_EMU_VIDEO_FORMAT_DENDY;

MM_EXPORT_EMU const struct mmEmuVideoFormatConfig* mmEmuVideoFormatConfig_ModeConfig(mmInt_t nMode);

struct mmEmuNesConfig
{
    struct mmEmuPathConfig path;
    struct mmEmuEmulatorConfig emulator;
    struct mmEmuGraphicsConfig graphics;
    struct mmEmuAudioConfig audio;
    struct mmEmuControllerConfig controller;
    struct mmEmuMovieConfig movie;
    struct mmEmuVideoFormatConfig nescfg;
};
MM_EXPORT_EMU void mmEmuNesConfig_Init(struct mmEmuNesConfig* p);
MM_EXPORT_EMU void mmEmuNesConfig_Destroy(struct mmEmuNesConfig* p);
MM_EXPORT_EMU void mmEmuNesConfig_Default(struct mmEmuNesConfig* p);
MM_EXPORT_EMU void mmEmuNesConfig_SetVideoMode(struct mmEmuNesConfig* p, mmInt_t nMode);

#include "core/mmSuffix.h"

#endif//__mmEmuNesConfig_h__
