/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuTape.h"
#include "mmEmuNes.h"
#include "mmEmuErrorCode.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuTape_Init(struct mmEmuTape* p)
{
    p->nes = NULL;

    p->m_bTapePlay = MM_FALSE;
    p->m_bTapeRec = MM_FALSE;
    p->m_fpTape = NULL;
    p->m_TapeCycles = 0.0;
    p->m_TapeIn = 0;
    p->m_TapeOut = 0;
}
MM_EXPORT_EMU void mmEmuTape_Destroy(struct mmEmuTape* p)
{
    p->nes = NULL;

    p->m_bTapePlay = MM_FALSE;
    p->m_bTapeRec = MM_FALSE;
    p->m_fpTape = NULL;
    p->m_TapeCycles = 0.0;
    p->m_TapeIn = 0;
    p->m_TapeOut = 0;
}

MM_EXPORT_EMU void mmEmuTape_SetParent(struct mmEmuTape* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU int mmEmuTape_TapePlay(struct mmEmuTape* p, const char* fname)
{
    struct mmEmuNes* nes = p->nes;

    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;

    do
    {
        if (mmEmuRom_IsNSF(&nes->rom))
        {
            code = MM_FAILURE;
            break;
        }

        if (mmEmuTape_IsTapePlay(p) || mmEmuTape_IsTapeRec(p))
        {
            mmEmuTape_TapeStop(p);
        }

        mmLogger_LogD(gLogger, "%s %d.", __FUNCTION__, __LINE__);

        if (!(p->m_fpTape = fopen(fname, "rb")))
        {
            mmLogger_LogE(gLogger, "%s %d Tape play error. File not found. %s.", __FUNCTION__, __LINE__, fname);
            // There is no file.
            code = MM_ERR_EMU_FILE_NOT_EXIST;
            break;
        }

        p->m_bTapePlay = MM_TRUE;
        p->m_TapeCycles = 0;
        p->m_TapeOut = 0;

        mmEmuCpu_SetClockProcess(&nes->cpu, MM_TRUE);

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_EMU int mmEmuTape_TapeRec(struct mmEmuTape* p, const char* fname)
{
    struct mmEmuNes* nes = p->nes;

    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;

    do
    {
        if (mmEmuRom_IsNSF(&nes->rom))
        {
            code = MM_FAILURE;
            break;
        }

        if (mmEmuTape_IsTapePlay(p) || mmEmuTape_IsTapeRec(p))
        {
            mmEmuTape_TapeStop(p);
        }

        mmLogger_LogD(gLogger, "%s %d.", __FUNCTION__, __LINE__);

        if (!(p->m_fpTape = fopen(fname, "wb")))
        {
            mmLogger_LogE(gLogger, "%s %d Tape rec error. File not found. %s.", __FUNCTION__, __LINE__, fname);
            // There is no file.
            code = MM_ERR_EMU_FILE_NOT_EXIST;
            break;
        }

        p->m_bTapeRec = MM_TRUE;
        p->m_TapeCycles = 0;
        p->m_TapeIn = 0;

        mmEmuCpu_SetClockProcess(&nes->cpu, MM_TRUE);

        code = MM_SUCCESS;
    } while (0);

    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuTape_TapeStop(struct mmEmuTape* p)
{
    struct mmEmuNes* nes = p->nes;

    if (!nes->barcode.m_bBarcode)
    {
        mmEmuCpu_SetClockProcess(&nes->cpu, MM_FALSE);
    }

    p->m_bTapePlay = p->m_bTapeRec = MM_FALSE;

    if (NULL != p->m_fpTape)
    {
        fclose(p->m_fpTape);
        p->m_fpTape = NULL;
    }
}
MM_EXPORT_EMU void mmEmuTape_Tape(struct mmEmuTape* p, mmInt_t cycles)
{
    struct mmEmuNes* nes = p->nes;

    struct mmEmuVideoFormatConfig* nescfg = &nes->config.nescfg;

    if (!(mmEmuTape_IsTapePlay(p) || mmEmuTape_IsTapeRec(p)))
    {
        return;
    }

    if ((p->m_TapeCycles -= (double)cycles) > 0)
    {
        return;
    }

    p->m_TapeCycles += (nescfg->CpuClock / 32000.0);
    // p->m_TapeCycles += (p->nescfg->CpuClock / 22050.0);  // It's too late to be useless.

    if (p->m_bTapePlay)
    {
        mmInt_t data = fgetc(p->m_fpTape);
        if (data != EOF)
        {
            if ((data & 0xFF) >= 0x8C)
            {
                p->m_TapeOut = 0x02;
            }
            else
            {
                if ((data & 0xFF) <= 0x74)
                {
                    p->m_TapeOut = 0x00;
                }
            }
        }
        else
        {
            mmEmuTape_TapeStop(p);
        }
    }
    if (p->m_bTapeRec)
    {
        fputc((int)((p->m_TapeIn & 7) == 7) ? 0x90 : 0x70, p->m_fpTape);
    }
}

