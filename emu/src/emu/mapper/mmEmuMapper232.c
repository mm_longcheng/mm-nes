/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper232.h"
#include "emu/mmEmuNes.h"


MM_EXPORT_EMU void mmEmuMapper232_Init(struct mmEmuMapper232* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[0] = 0x0C;
    p->reg[1] = 0x00;

    //
    p->super.Reset = &mmEmuMapper232_Reset;
    p->super.Write = &mmEmuMapper232_Write;
    //p->super.Read = &mmEmuMapper232_Read;
    //p->super.ReadLow = &mmEmuMapper232_ReadLow;
    p->super.WriteLow = &mmEmuMapper232_WriteLow;
    //p->super.ExRead = &mmEmuMapper232_ExRead;
    //p->super.ExWrite = &mmEmuMapper232_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper232_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper232_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper232_HSync;
    //p->super.VSync = &mmEmuMapper232_VSync;
    //p->super.Clock = &mmEmuMapper232_Clock;
    //p->super.PPULatch = &mmEmuMapper232_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper232_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper232_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper232_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper232_IsStateSave;
    p->super.SaveState = &mmEmuMapper232_SaveState;
    p->super.LoadState = &mmEmuMapper232_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper232_Destroy(struct mmEmuMapper232* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg[0] = 0x0C;
    p->reg[1] = 0x00;
}

MM_EXPORT_EMU void mmEmuMapper232_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper232* p = (struct mmEmuMapper232*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    p->reg[0] = 0x0C;
    p->reg[1] = 0x00;
}
MM_EXPORT_EMU void mmEmuMapper232_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    if (addr >= 0x6000)
    {
        mmEmuMapper232_Write(super, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper232_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper232* p = (struct mmEmuMapper232*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //if (addr == 0x9000)
    //{
    //  p->reg[0] = (data & 0x18) >> 1;
    //}
    //else if (addr >= 0xA000 && addr <= 0xFFFF)
    //{
    //  p->reg[1] = data & 0x03;
    //}
    if (addr <= 0x9FFF)
    {
        p->reg[0] = (data & 0x18) >> 1;
    }
    else
    {
        p->reg[1] = data & 0x03;
    }

    mmEmuMmu_SetPROM8KBank(mmu, 4, (p->reg[0] | p->reg[1]) * 2 + 0);
    mmEmuMmu_SetPROM8KBank(mmu, 5, (p->reg[0] | p->reg[1]) * 2 + 1);
    mmEmuMmu_SetPROM8KBank(mmu, 6, (p->reg[0] | 0x03) * 2 + 0);
    mmEmuMmu_SetPROM8KBank(mmu, 7, (p->reg[0] | 0x03) * 2 + 1);
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper232_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper232_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper232* p = (struct mmEmuMapper232*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
}
MM_EXPORT_EMU void mmEmuMapper232_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper232* p = (struct mmEmuMapper232*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper232_Produce(void)
{
    struct mmEmuMapper232* p = (struct mmEmuMapper232*)mmMalloc(sizeof(struct mmEmuMapper232));
    mmEmuMapper232_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper232_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper232* p = (struct mmEmuMapper232*)(m);
    mmEmuMapper232_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER232 =
{
    &__static_mmEmuMapper232_Produce,
    &__static_mmEmuMapper232_Recycle,
};
