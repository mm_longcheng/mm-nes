/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadSuporKeyboard.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuPadSuporKeyboard_Init(struct mmEmuPadSuporKeyboard* p)
{
    mmEmuExpad_Init(&p->super);

    p->bGraph = MM_FALSE;
    p->bOut = MM_FALSE;
    p->ScanNo = 0;

    //
    p->super.Reset = &mmEmuPadSuporKeyboard_Reset;
    //p->super.Strobe = &mmEmuPadSuporKeyboard_Strobe;
    p->super.Read4016 = &mmEmuPadSuporKeyboard_Read4016;
    p->super.Read4017 = &mmEmuPadSuporKeyboard_Read4017;
    p->super.Write4016 = &mmEmuPadSuporKeyboard_Write4016;
    //p->super.Write4017 = &mmEmuPadSuporKeyboard_Write4017;
    //p->super.Sync = &mmEmuPadSuporKeyboard_Sync;
    //p->super.SetSyncData = &mmEmuPadSuporKeyboard_SetSyncData;
    //p->super.GetSyncData = &mmEmuPadSuporKeyboard_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadSuporKeyboard_Destroy(struct mmEmuPadSuporKeyboard* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->bGraph = MM_FALSE;
    p->bOut = MM_FALSE;
    p->ScanNo = 0;
}

MM_EXPORT_EMU void mmEmuPadSuporKeyboard_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadSuporKeyboard* p = (struct mmEmuPadSuporKeyboard*)(super);

    p->bGraph = MM_FALSE;
    p->bOut = MM_FALSE;
    p->ScanNo = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadSuporKeyboard_Read4016(struct mmEmuExpad* super)
{
    // struct mmEmuPadSuporKeyboard* p = (struct mmEmuPadSuporKeyboard*)(super);

    mmByte_t data = 0;

    return data;
}
MM_EXPORT_EMU mmByte_t mmEmuPadSuporKeyboard_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadSuporKeyboard* p = (struct mmEmuPadSuporKeyboard*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmByte_t data = 0xFF;

    switch (p->ScanNo)
    {
    case 1:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_4)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_G)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_C)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F2)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_E)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_5)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_V)) data &= ~0x10;
        }
        break;
    case 2:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_2)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_D)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_S)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_END)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F1)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_W)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_3)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_X)) data &= ~0x10;
        }
        break;
    case 3:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_INSERT)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_BACK)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NEXT)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_RIGHT)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F8)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_PRIOR)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_DELETE)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_HOME)) data &= ~0x10;
        }
        break;
    case 4:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_9)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_I)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_L)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_COMMA)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F5)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_O)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_0)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_PERIOD)) data &= ~0x10;
        }
        break;
    case 5:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_RBRACKET)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_RETURN)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_UP)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LEFT)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F7)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LBRACKET)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_BACKSLASH)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_DOWN)) data &= ~0x10;
        }
        break;
    case 6:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_Q)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_CAPITAL)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_Z)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_TAB)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_ESCAPE)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_A)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_1)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LCONTROL)) data &= ~0x10;
        }
        break;
    case 7:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_7)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_Y)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_K)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_M)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F4)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_U)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_8)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_J)) data &= ~0x10;
        }
        break;
    case 8:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_MINUS)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_SEMICOLON)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_APOSTROPHE)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_SLASH)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F6)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_P)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_EQUALS)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LSHIFT) ||
                mmEmuController_KeyboardStatus(controller, MM_KC_RSHIFT)) data &= ~0x10;
        }
        break;
    case 9:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_T)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_H)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_N)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_SPACE)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F3)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_R)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_6)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_B)) data &= ~0x10;
        }
        break;
    case 10:
        if (p->bOut)
        {
            // do nothing.
        }
        else
        {
            data &= ~0x02;
        }
        break;
    case 11:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_LMENU)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD4)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD7)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F11)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F12)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD1)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD2)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD8)) data &= ~0x10;
        }
        break;
    case 12:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_SUBTRACT)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_ADD)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_MULTIPLY)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD9)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F10)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD5)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_DIVIDE)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMLOCK)) data &= ~0x10;
        }
        break;
    case 13:
        if (p->bOut)
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_GRAVE)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD6)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_PAUSE)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_SPACE)) data &= ~0x10;
        }
        else
        {
            if (mmEmuController_KeyboardStatus(controller, MM_KC_F9)) data &= ~0x02;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD3)) data &= ~0x04;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_DECIMAL)) data &= ~0x08;
            if (mmEmuController_KeyboardStatus(controller, MM_KC_NUMPAD0)) data &= ~0x10;
        }
        break;
    default:
        break;
    }

    return data;
}
MM_EXPORT_EMU void mmEmuPadSuporKeyboard_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadSuporKeyboard* p = (struct mmEmuPadSuporKeyboard*)(super);

    if (data == 0x05)
    {
        p->bOut = MM_FALSE;
        p->ScanNo = 0;
    }
    else if (data == 0x04)
    {
        if (++p->ScanNo > 13)
        {
            p->ScanNo = 0;
        }
        p->bOut = !p->bOut;
    }
    else if (data == 0x06)
    {
        p->bOut = !p->bOut;
    }
}

static struct mmEmuExpad* __static_mmEmuPadSuporKeyboard_Produce(void)
{
    struct mmEmuPadSuporKeyboard* p = (struct mmEmuPadSuporKeyboard*)mmMalloc(sizeof(struct mmEmuPadSuporKeyboard));
    mmEmuPadSuporKeyboard_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadSuporKeyboard_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadSuporKeyboard* p = (struct mmEmuPadSuporKeyboard*)(m);
    mmEmuPadSuporKeyboard_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_SUPORKEYBOARD =
{
    &__static_mmEmuPadSuporKeyboard_Produce,
    &__static_mmEmuPadSuporKeyboard_Recycle,
};
