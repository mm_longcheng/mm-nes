/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper051.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper051_Init(struct mmEmuMapper051* p)
{
    mmEmuMapper_Init(&p->super);

    p->mode = 0;
    p->bank = 0;

    //
    p->super.Reset = &mmEmuMapper051_Reset;
    p->super.Write = &mmEmuMapper051_Write;
    //p->super.Read = &mmEmuMapper051_Read;
    //p->super.ReadLow = &mmEmuMapper051_ReadLow;
    p->super.WriteLow = &mmEmuMapper051_WriteLow;
    //p->super.ExRead = &mmEmuMapper051_ExRead;
    //p->super.ExWrite = &mmEmuMapper051_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper051_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper051_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper051_HSync;
    //p->super.VSync = &mmEmuMapper051_VSync;
    //p->super.Clock = &mmEmuMapper051_Clock;
    //p->super.PPULatch = &mmEmuMapper051_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper051_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper051_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper051_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper051_IsStateSave;
    p->super.SaveState = &mmEmuMapper051_SaveState;
    p->super.LoadState = &mmEmuMapper051_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper051_Destroy(struct mmEmuMapper051* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->mode = 0;
    p->bank = 0;
}

MM_EXPORT_EMU void mmEmuMapper051_SetBankCPU(struct mmEmuMapper051* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (p->mode)
    {
    case 0:
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        mmEmuMmu_SetPROM8KBank(mmu, 3, (p->bank | 0x2c | 3));
        mmEmuMmu_SetPROM8KBank(mmu, 4, (p->bank | 0x00 | 0));
        mmEmuMmu_SetPROM8KBank(mmu, 5, (p->bank | 0x00 | 1));
        mmEmuMmu_SetPROM8KBank(mmu, 6, (p->bank | 0x0c | 2));
        mmEmuMmu_SetPROM8KBank(mmu, 7, (p->bank | 0x0c | 3));
        break;
    case 1:
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        mmEmuMmu_SetPROM8KBank(mmu, 3, (p->bank | 0x20 | 3));
        mmEmuMmu_SetPROM8KBank(mmu, 4, (p->bank | 0x00 | 0));
        mmEmuMmu_SetPROM8KBank(mmu, 5, (p->bank | 0x00 | 1));
        mmEmuMmu_SetPROM8KBank(mmu, 6, (p->bank | 0x00 | 2));
        mmEmuMmu_SetPROM8KBank(mmu, 7, (p->bank | 0x00 | 3));
        break;
    case 2:
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        mmEmuMmu_SetPROM8KBank(mmu, 3, (p->bank | 0x2e | 3));
        mmEmuMmu_SetPROM8KBank(mmu, 4, (p->bank | 0x02 | 0));
        mmEmuMmu_SetPROM8KBank(mmu, 5, (p->bank | 0x02 | 1));
        mmEmuMmu_SetPROM8KBank(mmu, 6, (p->bank | 0x0e | 2));
        mmEmuMmu_SetPROM8KBank(mmu, 7, (p->bank | 0x0e | 3));
        break;
    case 3:
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        mmEmuMmu_SetPROM8KBank(mmu, 3, (p->bank | 0x20 | 3));
        mmEmuMmu_SetPROM8KBank(mmu, 4, (p->bank | 0x00 | 0));
        mmEmuMmu_SetPROM8KBank(mmu, 5, (p->bank | 0x00 | 1));
        mmEmuMmu_SetPROM8KBank(mmu, 6, (p->bank | 0x00 | 2));
        mmEmuMmu_SetPROM8KBank(mmu, 7, (p->bank | 0x00 | 3));
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper051_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper051* p = (struct mmEmuMapper051*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->bank = 0;
    p->mode = 1;

    mmEmuMapper051_SetBankCPU(p);

    mmEmuMmu_SetCRAM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper051_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper051* p = (struct mmEmuMapper051*)(super);

    p->bank = (data & 0x0f) << 2;
    if (0xC000 <= addr && addr <= 0xDFFF)
    {
        p->mode = (p->mode & 0x01) | ((data & 0x10) >> 3);
    }
    mmEmuMapper051_SetBankCPU(p);
}
MM_EXPORT_EMU void mmEmuMapper051_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper051* p = (struct mmEmuMapper051*)(super);

    if (addr >= 0x6000)
    {
        p->mode = ((data & 0x10) >> 3) | ((data & 0x02) >> 1);
        mmEmuMapper051_SetBankCPU(p);
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper051_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper051_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper051* p = (struct mmEmuMapper051*)(super);

    buffer[0] = p->mode;
    buffer[1] = p->bank;
}
MM_EXPORT_EMU void mmEmuMapper051_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper051* p = (struct mmEmuMapper051*)(super);

    p->mode = buffer[0];
    p->bank = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper051_Produce(void)
{
    struct mmEmuMapper051* p = (struct mmEmuMapper051*)mmMalloc(sizeof(struct mmEmuMapper051));
    mmEmuMapper051_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper051_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper051* p = (struct mmEmuMapper051*)(m);
    mmEmuMapper051_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER051 =
{
    &__static_mmEmuMapper051_Produce,
    &__static_mmEmuMapper051_Recycle,
};
