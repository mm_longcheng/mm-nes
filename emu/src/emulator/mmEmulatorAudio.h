/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmulatorAudio_h__
#define __mmEmulatorAudio_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmRbtsetU32.h"
#include "container/mmRbtsetString.h"

//======================================================================//
// We use OpenAL-Soft
#include "AL/al.h"
#include "AL/alc.h"
//======================================================================//

#include "emu/mmEmuEmulator.h"

#include "emulator/mmEmulatorExport.h"

#include "core/mmPrefix.h"

#define MM_EMULATOR_AUDIO_RATE_TRIM_BORDER_DEFAULT 0.75
#define MM_EMULATOR_AUDIO_RATE_TRIM_NUMBER_DEFAULT 0.25

#define MM_EMULATOR_AUDIO_ACCUMULATION_LIMIT_DEFAULT 100

struct mmEmulatorAudio
{
    struct mmString device_name;

    struct mmRbtsetString devices;

    // strong ref.
    struct mmRbtsetU32 pool;
    // weak ref.
    struct mmRbtsetU32 used;
    // weak ref.
    struct mmRbtsetU32 idle;

    float rate_trim_border;
    float rate_trim_number;

    int accumulation_limit;

    // mute or not.
    mmBool_t mute;

    // background status.
    mmBool_t background;

    // audio Volume. default is 1.0f.
    float volume;

    int nRate;

    // OpenAL format
    ALenum nFormat;

    ALCdevice* device;
    ALCcontext* context;

    ALuint source;

    // 3D orientation
    ALfloat ListenerOrientation[6];
};
MM_EXPORT_EMULATOR void mmEmulatorAudio_Init(struct mmEmulatorAudio* p);
MM_EXPORT_EMULATOR void mmEmulatorAudio_Destroy(struct mmEmulatorAudio* p);

MM_EXPORT_EMULATOR void mmEmulatorAudio_SetDeviceName(struct mmEmulatorAudio* p, const char* device_name);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetRate(struct mmEmulatorAudio* p, int nRate);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetFormat(struct mmEmulatorAudio* p, ALenum nFormat);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetMute(struct mmEmulatorAudio* p, mmBool_t mute);

MM_EXPORT_EMULATOR ALuint mmEmulatorAudio_BufferAdd(struct mmEmulatorAudio* p);
MM_EXPORT_EMULATOR void mmEmulatorAudio_BufferRmv(struct mmEmulatorAudio* p, ALuint e);
MM_EXPORT_EMULATOR void mmEmulatorAudio_BufferClear(struct mmEmulatorAudio* p);

MM_EXPORT_EMULATOR ALuint mmEmulatorAudio_BufferProduce(struct mmEmulatorAudio* p);
MM_EXPORT_EMULATOR void mmEmulatorAudio_BufferRecycle(struct mmEmulatorAudio* p, ALuint v);

MM_EXPORT_EMULATOR void mmEmulatorAudio_DeviceList(struct mmEmulatorAudio* p);

MM_EXPORT_EMULATOR void mmEmulatorAudio_FinishLaunching(struct mmEmulatorAudio* p);
MM_EXPORT_EMULATOR void mmEmulatorAudio_BeforeTerminate(struct mmEmulatorAudio* p);

MM_EXPORT_EMULATOR void mmEmulatorAudio_EnterBackground(struct mmEmulatorAudio* p);
MM_EXPORT_EMULATOR void mmEmulatorAudio_EnterForeground(struct mmEmulatorAudio* p);

MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourcePosition(struct mmEmulatorAudio* p, float x, float y, float z);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceDirection(struct mmEmulatorAudio* p, float dirx, float diry, float dirz);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceVelocity(struct mmEmulatorAudio* p, float velx, float vely, float velz);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceVolume(struct mmEmulatorAudio* p, float gain);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceMaxVolume(struct mmEmulatorAudio* p, float maxGain);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceMinVolume(struct mmEmulatorAudio* p, float minGain);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceConeAngles(struct mmEmulatorAudio* p, float innerConeAngle, float outerConeAngle);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceOuterConeVolume(struct mmEmulatorAudio* p, float gain);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceMaxDistance(struct mmEmulatorAudio* p, float maxDistance);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetSourceRolloffFactor(struct mmEmulatorAudio* p, float rolloffFactor);

MM_EXPORT_EMULATOR void mmEmulatorAudio_SetListenerPosition(struct mmEmulatorAudio* p, float x, float y, float z);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetListenerVelocity(struct mmEmulatorAudio* p, float velx, float vely, float velz);
MM_EXPORT_EMULATOR void mmEmulatorAudio_SetListenerOrientation(struct mmEmulatorAudio* p, float x, float y, float z, float upx, float upy, float upz);

MM_EXPORT_EMULATOR void mmEmulatorAudio_Play(struct mmEmulatorAudio* p);
MM_EXPORT_EMULATOR void mmEmulatorAudio_Stop(struct mmEmulatorAudio* p);
MM_EXPORT_EMULATOR void mmEmulatorAudio_Pause(struct mmEmulatorAudio* p);

MM_EXPORT_EMULATOR int mmEmulatorAudio_ProcessBuffer(struct mmEmulatorAudio* p, mmByte_t* buffer, size_t length);

#include "core/mmSuffix.h"

#endif//__mmEmulatorAudio_h__
