/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper044.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper044_Init(struct mmEmuMapper044* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->patch = 0;
    p->bank = 0;
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    //
    p->super.Reset = &mmEmuMapper044_Reset;
    p->super.Write = &mmEmuMapper044_Write;
    //p->super.Read = &mmEmuMapper044_Read;
    //p->super.ReadLow = &mmEmuMapper044_ReadLow;
    p->super.WriteLow = &mmEmuMapper044_WriteLow;
    //p->super.ExRead = &mmEmuMapper044_ExRead;
    //p->super.ExWrite = &mmEmuMapper044_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper044_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper044_ExCmdWrite;
    p->super.HSync = &mmEmuMapper044_HSync;
    //p->super.VSync = &mmEmuMapper044_VSync;
    //p->super.Clock = &mmEmuMapper044_Clock;
    //p->super.PPULatch = &mmEmuMapper044_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper044_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper044_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper044_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper044_IsStateSave;
    p->super.SaveState = &mmEmuMapper044_SaveState;
    p->super.LoadState = &mmEmuMapper044_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper044_Destroy(struct mmEmuMapper044* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->patch = 0;
    p->bank = 0;
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}

MM_EXPORT_EMU void mmEmuMapper044_SetBankCPU(struct mmEmuMapper044* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, ((p->bank == 6) ? 0x1e : 0x0e) | (p->bank << 4));
        mmEmuMmu_SetPROM8KBank(mmu, 5, ((p->bank == 6) ? 0x1f & p->prg1 : 0x0f & p->prg1) | (p->bank << 4));
        mmEmuMmu_SetPROM8KBank(mmu, 6, ((p->bank == 6) ? 0x1f & p->prg0 : 0x0f & p->prg0) | (p->bank << 4));
        mmEmuMmu_SetPROM8KBank(mmu, 7, ((p->bank == 6) ? 0x1f : 0x0f) | (p->bank << 4));
    }
    else
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, ((p->bank == 6) ? 0x1f & p->prg0 : 0x0f & p->prg0) | (p->bank << 4));
        mmEmuMmu_SetPROM8KBank(mmu, 5, ((p->bank == 6) ? 0x1f & p->prg1 : 0x0f & p->prg1) | (p->bank << 4));
        mmEmuMmu_SetPROM8KBank(mmu, 6, ((p->bank == 6) ? 0x1e : 0x0e) | (p->bank << 4));
        mmEmuMmu_SetPROM8KBank(mmu, 7, ((p->bank == 6) ? 0x1f : 0x0f) | (p->bank << 4));
    }
}
MM_EXPORT_EMU void mmEmuMapper044_SetBankPPU(struct mmEmuMapper044* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, ((p->bank == 6) ? 0xff & p->chr4 : 0x7f & p->chr4) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 1, ((p->bank == 6) ? 0xff & p->chr5 : 0x7f & p->chr5) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 2, ((p->bank == 6) ? 0xff & p->chr6 : 0x7f & p->chr6) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 3, ((p->bank == 6) ? 0xff & p->chr7 : 0x7f & p->chr7) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 4, ((p->bank == 6) ? 0xff & p->chr01 : 0x7f & p->chr01) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 5, ((p->bank == 6) ? 0xff & (p->chr01 + 1) : 0x7f & (p->chr01 + 1)) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 6, ((p->bank == 6) ? 0xff & p->chr23 : 0x7f & p->chr23) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 7, ((p->bank == 6) ? 0xff & (p->chr23 + 1) : 0x7f & (p->chr23 + 1)) | (p->bank << 7));
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, ((p->bank == 6) ? 0xff & p->chr01 : 0x7f & p->chr01) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 1, ((p->bank == 6) ? 0xff & (p->chr01 + 1) : 0x7f & (p->chr01 + 1)) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 2, ((p->bank == 6) ? 0xff & p->chr23 : 0x7f & p->chr23) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 3, ((p->bank == 6) ? 0xff & (p->chr23 + 1) : 0x7f & (p->chr23 + 1)) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 4, ((p->bank == 6) ? 0xff & p->chr4 : 0x7f & p->chr4) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 5, ((p->bank == 6) ? 0xff & p->chr5 : 0x7f & p->chr5) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 6, ((p->bank == 6) ? 0xff & p->chr6 : 0x7f & p->chr6) | (p->bank << 7));
            mmEmuMmu_SetVROM1KBank(mmu, 7, ((p->bank == 6) ? 0xff & p->chr7 : 0x7f & p->chr7) | (p->bank << 7));
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper044_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper044* p = (struct mmEmuMapper044*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = mmEmuRom_GetPROMCRC(&nes->rom);

    p->patch = 0;

    if (crc == 0x7eef434c)
    {
        p->patch = 1;
    }

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->bank = 0;
    p->prg0 = 0;
    p->prg1 = 1;

    // set VROM banks
    if (mmu->VROM_1K_SIZE)
    {
        p->chr01 = 0;
        p->chr23 = 2;
        p->chr4 = 4;
        p->chr5 = 5;
        p->chr6 = 6;
        p->chr7 = 7;
    }
    else
    {
        p->chr01 = p->chr23 = p->chr4 = p->chr5 = p->chr6 = p->chr7 = 0;
    }

    mmEmuMapper044_SetBankCPU(p);
    mmEmuMapper044_SetBankPPU(p);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}
MM_EXPORT_EMU void mmEmuMapper044_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper044* p = (struct mmEmuMapper044*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper044_SetBankCPU(p);
        mmEmuMapper044_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;
        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr01 = data & 0xFE;
            mmEmuMapper044_SetBankPPU(p);
            break;
        case 0x01:
            p->chr23 = data & 0xFE;
            mmEmuMapper044_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper044_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper044_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper044_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper044_SetBankPPU(p);
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper044_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper044_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        p->bank = data & 0x07;
        if (p->bank == 7)
        {
            p->bank = 6;
        }
        mmEmuMapper044_SetBankCPU(p);
        mmEmuMapper044_SetBankPPU(p);
        break;
    case 0xC000:
        p->reg[4] = data;
        p->irq_counter = data;
        break;
    case 0xC001:
        p->reg[5] = data;
        p->irq_latch = data;
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 1;
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER );
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper044_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper044* p = (struct mmEmuMapper044*)(super);

    if (addr == 0x6000)
    {
        if (p->patch)
        {
            p->bank = (data & 0x06) >> 1;
        }
        else
        {
            p->bank = (data & 0x01) << 1;
        }
        mmEmuMapper044_SetBankCPU(p);
        mmEmuMapper044_SetBankPPU(p);
    }
}

MM_EXPORT_EMU void mmEmuMapper044_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper044* p = (struct mmEmuMapper044*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(--p->irq_counter))
                {
                    p->irq_counter = p->irq_latch;
                    //mm_emu_cpu_IRQ(&nes->cpu);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper044_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper044_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper044* p = (struct mmEmuMapper044*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
    buffer[16] = p->irq_enable;
    buffer[17] = p->irq_counter;
    buffer[18] = p->irq_latch;
    buffer[19] = p->bank;
}
MM_EXPORT_EMU void mmEmuMapper044_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper044* p = (struct mmEmuMapper044*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
    p->irq_enable = buffer[16];
    p->irq_counter = buffer[17];
    p->irq_latch = buffer[18];
    p->bank = buffer[19];
}

static struct mmEmuMapper* __static_mmEmuMapper044_Produce(void)
{
    struct mmEmuMapper044* p = (struct mmEmuMapper044*)mmMalloc(sizeof(struct mmEmuMapper044));
    mmEmuMapper044_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper044_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper044* p = (struct mmEmuMapper044*)(m);
    mmEmuMapper044_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER044 =
{
    &__static_mmEmuMapper044_Produce,
    &__static_mmEmuMapper044_Recycle,
};
