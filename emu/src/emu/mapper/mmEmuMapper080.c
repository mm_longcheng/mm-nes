/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper080.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper080_Init(struct mmEmuMapper080* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper080_Reset;
    //p->super.Write = &mmEmuMapper080_Write;
    //p->super.Read = &mmEmuMapper080_Read;
    //p->super.ReadLow = &mmEmuMapper080_ReadLow;
    p->super.WriteLow = &mmEmuMapper080_WriteLow;
    //p->super.ExRead = &mmEmuMapper080_ExRead;
    //p->super.ExWrite = &mmEmuMapper080_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper080_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper080_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper080_HSync;
    //p->super.VSync = &mmEmuMapper080_VSync;
    //p->super.Clock = &mmEmuMapper080_Clock;
    //p->super.PPULatch = &mmEmuMapper080_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper080_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper080_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper080_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper080_IsStateSave;
    //p->super.SaveState = &mmEmuMapper080_SaveState;
    //p->super.LoadState = &mmEmuMapper080_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper080_Destroy(struct mmEmuMapper080* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper080_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper080* p = (struct mmEmuMapper080*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper080_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper080* p = (struct mmEmuMapper080*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x7EF0:
        mmEmuMmu_SetVROM2KBank(mmu, 0, (data >> 1) & 0x3F);
        if (mmu->PROM_08K_SIZE == 32)
        {
            if (data & 0x80)
            {
                mmEmuMmu_SetVRAM1KBank(mmu, 8, 1);
                mmEmuMmu_SetVRAM1KBank(mmu, 9, 1);
            }
            else
            {
                mmEmuMmu_SetVRAM1KBank(mmu, 8, 0);
                mmEmuMmu_SetVRAM1KBank(mmu, 9, 0);
            }
        }
        break;

    case 0x7EF1:
        mmEmuMmu_SetVROM2KBank(mmu, 2, (data >> 1) & 0x3F);
        if (mmu->PROM_08K_SIZE == 32)
        {
            if (data & 0x80)
            {
                mmEmuMmu_SetVRAM1KBank(mmu, 10, 1);
                mmEmuMmu_SetVRAM1KBank(mmu, 11, 1);
            }
            else
            {
                mmEmuMmu_SetVRAM1KBank(mmu, 10, 0);
                mmEmuMmu_SetVRAM1KBank(mmu, 11, 0);
            }
        }
        break;

    case 0x7EF2:
        mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        break;
    case 0x7EF3:
        mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        break;
    case 0x7EF4:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        break;
    case 0x7EF5:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        break;

    case 0x7EF6:
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        break;

    case 0x7EFA:
    case 0x7EFB:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0x7EFC:
    case 0x7EFD:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0x7EFE:
    case 0x7EFF:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;
    default:
        mmEmuMapper_WriteLow(super, addr, data);
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper080_Produce(void)
{
    struct mmEmuMapper080* p = (struct mmEmuMapper080*)mmMalloc(sizeof(struct mmEmuMapper080));
    mmEmuMapper080_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper080_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper080* p = (struct mmEmuMapper080*)(m);
    mmEmuMapper080_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER080 =
{
    &__static_mmEmuMapper080_Produce,
    &__static_mmEmuMapper080_Recycle,
};
