/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper001.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper001_Init(struct mmEmuMapper001* p)
{
    mmEmuMapper_Init(&p->super);

    p->last_addr = 0;

    p->patch = 0;
    p->wram_patch = 0;
    p->wram_bank = 0;
    p->wram_count = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);

    p->shift = 0;
    p->regbuf = 0;

    //
    p->super.Reset = &mmEmuMapper001_Reset;
    p->super.Write = &mmEmuMapper001_Write;
    //p->super.Read = &mmEmuMapper001_Read;
    //p->super.ReadLow = &mmEmuMapper001_ReadLow;
    //p->super.WriteLow = &mmEmuMapper001_WriteLow;
    //p->super.ExRead = &mmEmuMapper001_ExRead;
    //p->super.ExWrite = &mmEmuMapper001_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper001_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper001_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper001_HSync;
    //p->super.VSync = &mmEmuMapper001_VSync;
    //p->super.Clock = &mmEmuMapper001_Clock;
    //p->super.PPULatch = &mmEmuMapper001_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper001_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper001_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper001_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper001_IsStateSave;
    p->super.SaveState = &mmEmuMapper001_SaveState;
    p->super.LoadState = &mmEmuMapper001_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper001_Destroy(struct mmEmuMapper001* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->last_addr = 0;

    p->patch = 0;
    p->wram_patch = 0;
    p->wram_bank = 0;
    p->wram_count = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);

    p->shift = 0;
    p->regbuf = 0;
}

MM_EXPORT_EMU void mmEmuMapper001_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper001* p = (struct mmEmuMapper001*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->reg[0] = 0x0C;       // D3=1,D2=1
    p->reg[1] = p->reg[2] = p->reg[3] = 0;
    p->shift = p->regbuf = 0;

    p->patch = 0;
    p->wram_patch = 0;

    if (mmu->PROM_16K_SIZE < 32)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        // For 512K/1M byte Cartridge
        mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
        mmEmuMmu_SetPROM16KBank(mmu, 6, 16 - 1);

        p->patch = 1;
    }

    //if (mmu->VROM_8K_SIZE) 
    //{
    //  mmEmuMmu_SetVROM8KBank(mmu, 0);
    //}

    crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);

    if (crc == 0xb8e16bd0)      // Snow Bros.(J)
    {
        p->patch = 2;
    }
    //if (crc == 0x9b565541)        // Triathron, The(J)
    //{
    //  mmEmuNes_SetFrameIRQmode(nes, MM_FALSE);
    //}
    if (crc == 0xc96c6f04)      // Venus Senki(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
    }
    //if (crc == 0x5e3f7004) // Softball Tengoku(J)
    //{
    //}

    if (crc == 0x4d2edf70)      // Night Rider(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0xcd2a73f0)      // Pirates!(U)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
        p->patch = 2;
    }

    //if (crc == 0x09efe54b) // Majaventure - Mahjong Senki(J)
    //{
    //  mmEmuNes_SetFrameIRQmode(nes, MM_FALSE);
    //}

    if (crc == 0x11469ce3)      // Viva! Las Vegas(J)
    {
    }
    if (crc == 0xd878ebf5)      // Ninja Ryukenden(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
    }

    //if (crc == 0x7bd7b849) // Nekketsu Koukou - Dodgeball Bu(J)
    //{
    //}

    if (crc == 0x466efdc2)      // Final Fantasy(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
        mmEmuPpu_SetExtMonoMode(&nes->ppu, MM_TRUE);
    }
    if (crc == 0xc9556b36)      // Final Fantasy I&II(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
        mmEmuPpu_SetExtMonoMode(&nes->ppu, MM_TRUE);
        mmEmuNes_SetSAVERAMSIZE(nes, 16 * 1024);
        p->wram_patch = 2;
    }

    if (crc == 0x717e1169)      // Cosmic Wars(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }

    if (crc == 0xC05D2034)      // Snake's Revenge(U)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }

    if (crc == 0xb8747abf ||        // Best Play - Pro Yakyuu Special(J)
        crc == 0x29449ba9 ||        // Nobunaga no Yabou - Zenkoku Ban(J)
        crc == 0x2b11e0b0 ||        // Nobunaga no Yabou - Zenkoku Ban(J)(alt)
        crc == 0x4642dda6 ||        // Nobunaga's Ambition(U)
        crc == 0xfb69743a ||        // Aoki Ookami to Shiroki Mejika - Genghis Khan(J)
        crc == 0x2225c20f ||        // Genghis Khan(U)
        crc == 0xabbf7217)// Sangokushi(J) 
    {
        mmEmuNes_SetSAVERAMSIZE(nes, 16 * 1024);
        p->wram_patch = 1;
        p->wram_bank = 0;
        p->wram_count = 0;
    }
}
MM_EXPORT_EMU void mmEmuMapper001_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper001* p = (struct mmEmuMapper001*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //  DEBUGOUT( "MMC1 %04X=%02X\n", addr&0xFFFF,data&0xFF );

    if (p->wram_patch == 1 && addr == 0xBFFF)
    {
        p->wram_count++;
        p->wram_bank += data & 0x01;
        if (p->wram_count == 5)
        {
            if (p->wram_bank)
            {
                mmEmuMmu_SetPROMBank(mmu, 3, &mmu->WRAM[0x2000], MM_EMU_MMU_BANKTYPE_RAM);
            }
            else
            {
                mmEmuMmu_SetPROMBank(mmu, 3, &mmu->WRAM[0x0000], MM_EMU_MMU_BANKTYPE_RAM);
            }
            p->wram_bank = p->wram_count = 0;
        }
    }

    if (p->patch != 1)
    {
        if ((addr & 0x6000) != (p->last_addr & 0x6000))
        {
            p->shift = p->regbuf = 0;
        }
        p->last_addr = addr;
    }

    if (data & 0x80)
    {
        p->shift = p->regbuf = 0;
        //p->reg[0] = 0x0C;     // D3=1,D2=1
        p->reg[0] |= 0x0C;      // D3=1,D2=1 The rest is not reset.
        return;
    }

    if (data & 0x01)
    {
        p->regbuf |= 1 << p->shift;
    }
    if (++p->shift < 5)
    {
        return;
    }
    addr = (addr & 0x7FFF) >> 13;
    p->reg[addr] = p->regbuf;

    //  DEBUGOUT( "MMC1 %d=%02X\n", addr&0xFFFF,regbuf&0xFF );

    p->regbuf = 0;
    p->shift = 0;

    if (p->patch != 1)
    {
        // For Normal Cartridge
        switch (addr)
        {
        case 0:
            if (p->reg[0] & 0x02)
            {
                if (p->reg[0] & 0x01)
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
                }
                else
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
                }
            }
            else
            {
                if (p->reg[0] & 0x01)
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
                }
                else
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
                }
            }
            break;
        case 1:
            // Register #1
            if (mmu->VROM_1K_SIZE)
            {
                if (p->reg[0] & 0x10)
                {
                    // CHR 4K bank lower($0000-$0FFF)
                    mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[1]);
                    // CHR 4K bank higher($1000-$1FFF)
                    mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[2]);
                }
                else
                {
                    // CHR 8K bank($0000-$1FFF)
                    mmEmuMmu_SetVROM8KBank(mmu, p->reg[1] >> 1);
                }
            }
            else
            {
                // For Romancia
                if (p->reg[0] & 0x10)
                {
                    mmEmuMmu_SetCRAM4KBank(mmu, 0, p->reg[1]);
                }
            }
            break;
        case 2:
            // Register #2
            if (mmu->VROM_1K_SIZE)
            {
                if (p->reg[0] & 0x10)
                {
                    // CHR 4K bank lower($0000-$0FFF)
                    mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[1]);
                    // CHR 4K bank higher($1000-$1FFF)
                    mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[2]);
                }
                else
                {
                    // CHR 8K bank($0000-$1FFF)
                    mmEmuMmu_SetVROM8KBank(mmu, p->reg[1] >> 1);
                }
            }
            else
            {
                // For Romancia
                if (p->reg[0] & 0x10)
                {
                    mmEmuMmu_SetCRAM4KBank(mmu, 4, p->reg[2]);
                }
            }
            break;
        case 3:
            if (!(p->reg[0] & 0x08))
            {
                // PRG 32K bank ($8000-$FFFF)
                mmEmuMmu_SetPROM32KBank(mmu, p->reg[3] >> 1);
            }
            else
            {
                if (p->reg[0] & 0x04)
                {
                    // PRG 16K bank ($8000-$BFFF)
                    mmEmuMmu_SetPROM16KBank(mmu, 4, p->reg[3]);
                    mmEmuMmu_SetPROM16KBank(mmu, 6, mmu->PROM_16K_SIZE - 1);
                }
                else
                {
                    // PRG 16K bank ($C000-$FFFF)
                    mmEmuMmu_SetPROM16KBank(mmu, 6, p->reg[3]);
                    mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
                }
            }
            break;
        default:
            break;
        }
    }
    else
    {
        // For 512K/1M byte Cartridge
        mmInt_t PROM_BASE = 0;
        if (mmu->PROM_16K_SIZE >= 32)
        {
            PROM_BASE = p->reg[1] & 0x10;
        }

        // For FinalFantasy I&II
        if (p->wram_patch == 2)
        {
            if (!(p->reg[1] & 0x18))
            {
                mmEmuMmu_SetPROMBank(mmu, 3, &mmu->WRAM[0x0000], MM_EMU_MMU_BANKTYPE_RAM);
            }
            else
            {
                mmEmuMmu_SetPROMBank(mmu, 3, &mmu->WRAM[0x2000], MM_EMU_MMU_BANKTYPE_RAM);
            }
        }

        // Register #0
        if (addr == 0)
        {
            if (p->reg[0] & 0x02)
            {
                if (p->reg[0] & 0x01)
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
                }
                else
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
                }
            }
            else
            {
                if (p->reg[0] & 0x01)
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
                }
                else
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
                }
            }
        }
        // Register #1
        if (mmu->VROM_1K_SIZE)
        {
            if (p->reg[0] & 0x10)
            {
                // CHR 4K bank lower($0000-$0FFF)
                mmEmuMmu_SetVROM4KBank(mmu, 0, p->reg[1]);
            }
            else
            {
                // CHR 8K bank($0000-$1FFF)
                mmEmuMmu_SetVROM8KBank(mmu, p->reg[1] >> 1);
            }
        }
        else
        {
            // For Romancia
            if (p->reg[0] & 0x10)
            {
                mmEmuMmu_SetCRAM4KBank(mmu, 0, p->reg[1]);
            }
        }
        // Register #2
        if (mmu->VROM_1K_SIZE)
        {
            if (p->reg[0] & 0x10)
            {
                // CHR 4K bank higher($1000-$1FFF)
                mmEmuMmu_SetVROM4KBank(mmu, 4, p->reg[2]);
            }
        }
        else
        {
            // For Romancia
            if (p->reg[0] & 0x10)
            {
                mmEmuMmu_SetCRAM4KBank(mmu, 4, p->reg[2]);
            }
        }
        // Register #3
        if (!(p->reg[0] & 0x08))
        {
            // PRG 32K bank ($8000-$FFFF)
            mmEmuMmu_SetPROM32KBank(mmu, (p->reg[3] & (0xF + PROM_BASE)) >> 1);
        }
        else
        {
            if (p->reg[0] & 0x04)
            {
                // PRG 16K bank ($8000-$BFFF)
                mmEmuMmu_SetPROM16KBank(mmu, 4, PROM_BASE + (p->reg[3] & 0x0F));
                if (mmu->PROM_16K_SIZE >= 32)
                {
                    mmEmuMmu_SetPROM16KBank(mmu, 6, PROM_BASE + 16 - 1);
                }
            }
            else
            {
                // PRG 16K bank ($C000-$FFFF)
                mmEmuMmu_SetPROM16KBank(mmu, 6, PROM_BASE + (p->reg[3] & 0x0F));
                if (mmu->PROM_16K_SIZE >= 32)
                {
                    mmEmuMmu_SetPROM16KBank(mmu, 4, PROM_BASE);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper001_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper001_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper001* p = (struct mmEmuMapper001*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->reg[3];
    buffer[4] = p->shift;
    buffer[5] = p->regbuf;

    buffer[6] = p->wram_bank;
    buffer[7] = p->wram_count;
}
MM_EXPORT_EMU void mmEmuMapper001_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper001* p = (struct mmEmuMapper001*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->reg[3] = buffer[3];
    p->shift = buffer[4];
    p->regbuf = buffer[5];

    p->wram_bank = buffer[6];
    p->wram_count = buffer[7];
}

static struct mmEmuMapper* __static_mmEmuMapper001_Produce(void)
{
    struct mmEmuMapper001* p = (struct mmEmuMapper001*)mmMalloc(sizeof(struct mmEmuMapper001));
    mmEmuMapper001_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper001_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper001* p = (struct mmEmuMapper001*)(m);
    mmEmuMapper001_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER001 =
{
    &__static_mmEmuMapper001_Produce,
    &__static_mmEmuMapper001_Recycle,
};
