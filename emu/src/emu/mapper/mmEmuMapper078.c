/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper078.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper078_Init(struct mmEmuMapper078* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper078_Reset;
    p->super.Write = &mmEmuMapper078_Write;
    //p->super.Read = &mmEmuMapper078_Read;
    //p->super.ReadLow = &mmEmuMapper078_ReadLow;
    //p->super.WriteLow = &mmEmuMapper078_WriteLow;
    //p->super.ExRead = &mmEmuMapper078_ExRead;
    //p->super.ExWrite = &mmEmuMapper078_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper078_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper078_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper078_HSync;
    //p->super.VSync = &mmEmuMapper078_VSync;
    //p->super.Clock = &mmEmuMapper078_Clock;
    //p->super.PPULatch = &mmEmuMapper078_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper078_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper078_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper078_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper078_IsStateSave;
    //p->super.SaveState = &mmEmuMapper078_SaveState;
    //p->super.LoadState = &mmEmuMapper078_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper078_Destroy(struct mmEmuMapper078* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper078_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper078* p = (struct mmEmuMapper078*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper078_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper078* p = (struct mmEmuMapper078*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "MAP78 WR $%04X=$%02X L=%d\n", addr, data, nes->GetScanline() );

    mmEmuMmu_SetPROM16KBank(mmu, 4, data & 0x0F);
    mmEmuMmu_SetVROM8KBank(mmu, (data & 0xF0) >> 4);

    if ((addr & 0xFE00) != 0xFE00)
    {
        if (data & 0x08)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper078_Produce(void)
{
    struct mmEmuMapper078* p = (struct mmEmuMapper078*)mmMalloc(sizeof(struct mmEmuMapper078));
    mmEmuMapper078_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper078_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper078* p = (struct mmEmuMapper078*)(m);
    mmEmuMapper078_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER078 =
{
    &__static_mmEmuMapper078_Produce,
    &__static_mmEmuMapper078_Recycle,
};
