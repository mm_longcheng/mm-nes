LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libmm_emulator_shared
LOCAL_MODULE_FILENAME := libmm_emulator_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-undefined-var-template

LOCAL_CFLAGS += -DMM_SHARED_EMULATOR

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -l$(MM_PLATFORM)
########################################################################
LOCAL_SHARED_LIBRARIES += libOgreMain_shared
LOCAL_SHARED_LIBRARIES += libRenderSystem_GLES2_shared

LOCAL_SHARED_LIBRARIES += libCEGUIMain_shared

LOCAL_SHARED_LIBRARIES += libtinyxml_shared

LOCAL_SHARED_LIBRARIES += libCEGUIOgreRenderer_shared

LOCAL_SHARED_LIBRARIES += libOpenAL_shared

LOCAL_SHARED_LIBRARIES += libmm_core_shared
LOCAL_SHARED_LIBRARIES += libmm_dish_shared
LOCAL_SHARED_LIBRARIES += libmm_nwsi_shared

LOCAL_SHARED_LIBRARIES += libmm_emu_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/os/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/mm/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/mm/src/os/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/dish/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/dish/src/os/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/nwsi/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/nwsi/src/os/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-nes/emu/src

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/lua/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/build/libiconv/include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/libiconv/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/ois/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/zziplib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/tinyxml

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/build/Ogre/include/$(MM_PLATFORM) 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/Ogre/OgreMain/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/Ogre/RenderSystems/GLSupport/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/Ogre/RenderSystems/GLSupport/include/EGL 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/Ogre/RenderSystems/GLES2/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/Ogre/RenderSystems/GLES2/include/EGL 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/Ogre/Components/RTShaderSystem/include 

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/build/cegui/include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/cegui/cegui/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/build/OpenAL/include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/OpenAL/include/AL
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-lib/src/OpenAL/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/emulator
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################