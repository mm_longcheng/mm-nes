/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuPad_h__
#define __mmEmuPad_h__

#include "core/mmCore.h"

#include "mmEmuExpad.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

enum
{
    MM_EMU_PAD_VS_TYPE0 = 0,    // SELECT1P=START1P/SELECT2P=START2P 1P/2P No reverse
    MM_EMU_PAD_VS_TYPE1,        // SELECT1P=START1P/SELECT2P=START2P 1P/2P Reverse
    MM_EMU_PAD_VS_TYPE2,        // SELECT1P=START1P/START1P =START2P 1P/2P No reverse
    MM_EMU_PAD_VS_TYPE3,        // SELECT1P=START1P/START1P =START2P 1P/2P Reverse
    MM_EMU_PAD_VS_TYPE4,        // SELECT1P=START1P/SELECT2P=START2P 1P/2P No reverse (Protection)
    MM_EMU_PAD_VS_TYPE5,        // SELECT1P=START1P/SELECT2P=START2P 1P/2P Reverse    (Protection)
    MM_EMU_PAD_VS_TYPE6,        // SELECT1P=START1P/SELECT2P=START2P 1P/2P Reverse  (For Golf)
    MM_EMU_PAD_VS_TYPEZ,        // ZAPPER
};

enum mmEmuPadExController_t
{
    MM_EMU_PAD_EXCONTROLLER_NONE = 0,

    MM_EMU_PAD_EXCONTROLLER_PADDLE,
    MM_EMU_PAD_EXCONTROLLER_HYPERSHOT,
    MM_EMU_PAD_EXCONTROLLER_ZAPPER,
    MM_EMU_PAD_EXCONTROLLER_KEYBOARD,
    MM_EMU_PAD_EXCONTROLLER_CRAZYCLIMBER,
    MM_EMU_PAD_EXCONTROLLER_TOPRIDER,
    MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN,

    MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERA,
    MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERB,
    MM_EMU_PAD_EXCONTROLLER_EXCITINGBOXING,
    MM_EMU_PAD_EXCONTROLLER_MAHJANG,
    MM_EMU_PAD_EXCONTROLLER_OEKAKIDSTABLET,
    MM_EMU_PAD_EXCONTROLLER_TURBOFILE,

    MM_EMU_PAD_EXCONTROLLER_VSUNISYSTEM,
    MM_EMU_PAD_EXCONTROLLER_VSZAPPER,

    MM_EMU_PAD_EXCONTROLLER_GYROMITE,
    MM_EMU_PAD_EXCONTROLLER_STACKUP,

    MM_EMU_PAD_EXCONTROLLER_SUPORKEYBOARD,
};

MM_EXPORT_EMU extern const char* MM_EMU_EXCONTROLLER_TYPE[19];

MM_EXPORT_EMU const char* mmEmuPad_ExControllerTypeToString(int type);

/*
 *   Controllers - Joypads
 *
 * Joypads (or Joysticks)
 * Each joypad includes an 8bit shift register, set Port 4016h/Bit0=1 to reload the button states into the
 * shift registers of both joypads, then reset Port 4016h/Bit0=0 to disable the shift reload (otherwise all
 * further reads would be stuck to the 1st bit, ie. Button A). Joypad data can be then read from bit 0 of 4016h
 * (joypad 1) and/or bit 0 of 4017h (joypad 2) as serial bitstream of 8bit length, ordered as follows:
 *
 *   1st         Button A         (0=High=Released, 1=Low=Pressed)
 *   2nd         Button B         (0=High=Released, 1=Low=Pressed)
 *   3rd         Select           (0=High=Released, 1=Low=Pressed)
 *   4th         Start            (0=High=Released, 1=Low=Pressed)
 *   5th         Direction Up     (0=High=Released, 1=Low=Moved)
 *   6th         Direction Down   (0=High=Released, 1=Low=Moved)
 *   7th         Direction Left   (0=High=Released, 1=Low=Moved)
 *   8th         Direction Right  (0=High=Released, 1=Low=Moved)
 *   9th and up  Unused (all 1=Low)  ;(or all 0=High when no joypad connected)
 *
 * The console automatically sends a clock pulse to the Joypad 1 shift register after each read from 4016h
 * (and to joypad 2 after read from 4017h). There are no timing restrictions, joypads can be handled as fast,
 * or as slow, as desired.
 *
 * Note that older Famicom controllers include Select & Start buttons only on joypad 1, whilst joypad 2 probably
 * returns unused dummy bits instead (the values of that dummy bits are unknown, probably 0=High=Released?).
 *
 * Joypad Layout
 *    ___________________________________
 *   |    _                              |
 *   |  _| |_               (|B|) (|A|)  |
 *   | |_   _| SELECT START              |
 *   |   |_|    (==)  (==)  ( B ) ( A )  |
 *   |___________________________________|
 *
 * Jump and Run Games conventionally use A=Jump, B=Fire.
 *
 * rapid A B need external simulation A B at a certain frequency pressed and release.
 *
 */

enum mmEmuPadJoypadButton_t
{
    MM_EMU_PAD_JOYPAD_A         = 0,
    MM_EMU_PAD_JOYPAD_B         = 1,
    MM_EMU_PAD_JOYPAD_CONTROL_L = 2, // select
    MM_EMU_PAD_JOYPAD_CONTROL_R = 3, // start
    MM_EMU_PAD_JOYPAD_ARROW_U   = 4, // arrow up
    MM_EMU_PAD_JOYPAD_ARROW_D   = 5, // arrow down
    MM_EMU_PAD_JOYPAD_ARROW_L   = 6, // arrow left
    MM_EMU_PAD_JOYPAD_ARROW_R   = 7, // arrow right
    MM_EMU_PAD_JOYPAD_A_RAPID   = 8,
    MM_EMU_PAD_JOYPAD_B_RAPID   = 9,
};

enum mmEmuPadNsfButton_t
{
    MM_EMU_PAD_NSF_PLAY          = 0,// Play
    MM_EMU_PAD_NSF_STOP          = 1,// Stop
    MM_EMU_PAD_NSF_NUMBER_DEC_1  = 2,// Number -1
    MM_EMU_PAD_NSF_NUMBER_INC_1  = 3,// Number +1
    MM_EMU_PAD_NSF_NUMBER_DEC_16 = 4,// Number -16
    MM_EMU_PAD_NSF_NUMBER_INC_16 = 5,// Number +16
};

enum mmEmuPadRapidSpeed_t
{
    MM_EMU_PAD_RAPID_SPEED_10FPS = 0,// 10 FPS
    MM_EMU_PAD_RAPID_SPEED_15FPS = 1,// 15 FPS
    MM_EMU_PAD_RAPID_SPEED_20FPS = 2,// 20 FPS
    MM_EMU_PAD_RAPID_SPEED_30FPS = 3,// 30 FPS
};

MM_EXPORT_EMU extern const char* MM_EMU_PAD_RAPID_SPEED[4];

MM_EXPORT_EMU const char* mmEmuPad_RapidSpeedToString(mmWord_t speed);

struct mmEmuNes;

struct mmEmuPad
{
    struct mmEmuNes* nes;

    mmUInt32_t  pad1bit, pad2bit;
    mmUInt32_t  pad3bit, pad4bit;

    // Frame Synchronized
    mmLong_t    zapperx, zappery;
    mmByte_t    zapperbit;
    mmByte_t    crazyclimberbit;

    /* status */

    // Extension Devices
    struct mmEmuExpadFactory expad_factory;
    struct mmEmuExpad* expad;

    // Frame Synchronized
    mmByte_t    padbit[4];
    mmByte_t    micbit;

    mmByte_t    padbitsync[4];
    mmByte_t    micbitsync;

    mmInt_t     excontroller_select;
    mmInt_t     padcnt[4][2];

    mmBool_t    bStrobe;
    mmBool_t    bZapperMode;

    // For VS-Unisystem
    mmInt_t     nVSSwapType;
    mmBool_t    bSwapPlayer;
    mmBool_t    bSwapButton;

    // For BarcodeWorld
    mmBool_t    bBarcodeWorld;

    mmByte_t    nsfbit;

    // Manual bit controller.
    mmWord_t    nManualPadbit[4];
    mmByte_t    nManualNsfbit;
};

MM_EXPORT_EMU void mmEmuPad_Init(struct mmEmuPad* p);
MM_EXPORT_EMU void mmEmuPad_Destroy(struct mmEmuPad* p);

MM_EXPORT_EMU void mmEmuPad_SetParent(struct mmEmuPad* p, struct mmEmuNes* parent);

// Member function.
MM_EXPORT_EMU void mmEmuPad_Reset(struct mmEmuPad* p);

// For movie
MM_EXPORT_EMU mmUInt32_t mmEmuPad_GetSyncData(const struct mmEmuPad* p);
MM_EXPORT_EMU void mmEmuPad_SetSyncData(struct mmEmuPad* p, mmUInt32_t data);

MM_EXPORT_EMU mmUInt32_t mmEmuPad_GetSyncExData(const struct mmEmuPad* p);
MM_EXPORT_EMU void mmEmuPad_SetSyncExData(struct mmEmuPad* p, mmUInt32_t data);

MM_EXPORT_EMU void mmEmuPad_VSync(struct mmEmuPad* p);

MM_EXPORT_EMU void mmEmuPad_Sync(struct mmEmuPad* p);
MM_EXPORT_EMU mmByte_t mmEmuPad_SyncSub(struct mmEmuPad* p, mmInt_t no);

static mmInline void mmEmuPad_SetStrobe(struct mmEmuPad* p, mmBool_t bStrb) { p->bStrobe = bStrb; }
static mmInline mmBool_t mmEmuPad_GetStrobe(const struct mmEmuPad* p) { return p->bStrobe; }

MM_EXPORT_EMU void mmEmuPad_Strobe(struct mmEmuPad* p);
MM_EXPORT_EMU mmWord_t mmEmuPad_StrobeSub(struct mmEmuPad* p, mmInt_t no);

MM_EXPORT_EMU mmByte_t mmEmuPad_Read(struct mmEmuPad* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuPad_Write(struct mmEmuPad* p, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuPad_SetExController(struct mmEmuPad* p, mmInt_t type);
static mmInline mmInt_t mmEmuPad_GetExController(const struct mmEmuPad* p) { return p->excontroller_select; }

static mmInline mmBool_t mmEmuPad_IsZapperMode(const struct mmEmuPad* p) { return p->bZapperMode; };

// For VS-Unisystem
static mmInline void mmEmuPad_SetSwapPlayer(struct mmEmuPad* p, mmBool_t bSwap) { p->bSwapPlayer = bSwap; }
static mmInline void mmEmuPad_SetSwapButton(struct mmEmuPad* p, mmBool_t bSwap) { p->bSwapButton = bSwap; }
static mmInline void mmEmuPad_SetVSType(struct mmEmuPad* p, mmInt_t nType) { p->nVSSwapType = nType; }

// For NSF Player
static mmInline mmByte_t mmEmuPad_GetNsfController(const struct mmEmuPad* p) { return p->nsfbit; }
// For NSF Player
MM_EXPORT_EMU void mmEmuPad_NsfSub(struct mmEmuPad* p);

// For Manual bit controller Joypad.
MM_EXPORT_EMU void mmEmuPad_JoypadBitSetData(struct mmEmuPad* p, int n, mmWord_t data);
MM_EXPORT_EMU mmWord_t mmEmuPad_JoypadBitGetData(const struct mmEmuPad* p, int n);
MM_EXPORT_EMU void mmEmuPad_JoypadBitPressed(struct mmEmuPad* p, int n, int hId);
MM_EXPORT_EMU void mmEmuPad_JoypadBitRelease(struct mmEmuPad* p, int n, int hId);
// For Manual bit controller Nsf.
MM_EXPORT_EMU void mmEmuPad_NsfBitSetData(struct mmEmuPad* p, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuPad_NsfBitGetData(const struct mmEmuPad* p);
MM_EXPORT_EMU void mmEmuPad_NsfBitPressed(struct mmEmuPad* p, int hId);
MM_EXPORT_EMU void mmEmuPad_NsfBitRelease(struct mmEmuPad* p, int hId);

#include "core/mmSuffix.h"

#endif//__mmEmuPad_h__
