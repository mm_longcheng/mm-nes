#include "core/mmConfig.h"
#include "core/mmCore.h"
#include "core/mmLoggerManager.h"

#include <stdio.h>
// #define USE_VLD_CHECK_MEMORY_LEAK
#if _DEBUG && MM_WIN32
#ifdef USE_VLD_CHECK_MEMORY_LEAK
#include <vld.h>
#endif
#endif // _DEBUG

#include <exception>

#include "mmApplication.h"

BOOL WINAPI __static_SignalDestroy(DWORD msgType)
{
	switch (msgType)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
		{
			struct mmApplication* p = mmApplication_Instance();
			struct mmLogger* gLogger = mmLogger_Instance();
            mmApplication_Shutdown(p);
			mmLogger_LogI(gLogger, "__static_SignalDestroy signal: %d.", msgType);
			return TRUE;
		}
		break;
	default:
		return FALSE;
	}
	return FALSE;
}
int main(int argc, char **argv)
{
	struct mmApplication* p = NULL;
#ifdef _DEBUG
	int _argc = gArgument.argc;
	const char** _argv = gArgument.argv;
#else
	int _argc = argc;
	const char** _argv = (const char**)argv;
#endif
	try
	{
		//
		SetConsoleCtrlHandler(__static_SignalDestroy, TRUE);
		//
		p = mmApplication_Instance();
        mmApplication_Init(p);
        mmApplication_Initialize(p,_argc,_argv);
        mmApplication_Start(p);
        mmApplication_Join(p);
        mmApplication_Destroy(p);
	}
	catch (std::exception& e)
	{
		mmPrintf("A untreated exception occur:%s\n", e.what());
		MessageBox(NULL, e.what(), "An exception has occurred!", MB_ICONERROR | MB_TASKMODAL);
	}
	return 0;
}