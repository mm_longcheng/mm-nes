/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApuFme7_h__
#define __mmEmuApuFme7_h__

#include "core/mmCore.h"

#include "emu/mmEmuApuInterface.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuApuFme7Envelope
{
    mmByte_t    reg[3];
    mmByte_t    volume;

    mmInt_t     freq;
    mmInt_t     phaseacc;
    mmInt_t     envadr;

    const mmByte_t*     envtbl;
    const mmSChar_t*    envstep;
};

struct mmEmuApuFme7Noise
{
    mmInt_t     freq;
    mmInt_t     phaseacc;
    mmInt_t     noiserange;
    mmByte_t    noiseout;
};

struct mmEmuApuFme7Channel
{
    mmByte_t    reg[3];
    mmByte_t    enable;
    mmByte_t    env_on;
    mmByte_t    noise_on;
    mmByte_t    adder;
    mmByte_t    volume;

    mmInt_t     freq;
    mmInt_t     phaseacc;

    mmInt_t     output_vol;
};

// Tables
MM_EXPORT_EMU extern const mmByte_t        MM_EMU_APU_FME7_ENVELOPE_PULSE0[];
MM_EXPORT_EMU extern const mmByte_t        MM_EMU_APU_FME7_ENVELOPE_PULSE1[];
MM_EXPORT_EMU extern const mmByte_t        MM_EMU_APU_FME7_ENVELOPE_PULSE2[];
MM_EXPORT_EMU extern const mmByte_t        MM_EMU_APU_FME7_ENVELOPE_PULSE3[];
MM_EXPORT_EMU extern const mmSChar_t       MM_EMU_APU_FME7_ENVSTEP_PULSE[];

MM_EXPORT_EMU extern const mmByte_t        MM_EMU_APU_FME7_ENVELOPE_SAWTOOTH0[];
MM_EXPORT_EMU extern const mmByte_t        MM_EMU_APU_FME7_ENVELOPE_SAWTOOTH1[];
MM_EXPORT_EMU extern const mmSChar_t       MM_EMU_APU_FME7_ENVSTEP_SAWTOOTH[];

MM_EXPORT_EMU extern const mmByte_t        MM_EMU_APU_FME7_ENVELOPE_TRIANGLE0[];
MM_EXPORT_EMU extern const mmByte_t        MM_EMU_APU_FME7_ENVELOPE_TRIANGLE1[];

MM_EXPORT_EMU extern const mmSChar_t       MM_EMU_APU_FME7_ENVSTEP_TRIANGLE[];

MM_EXPORT_EMU extern const mmByte_t*       MM_EMU_APU_FME7_ENVELOPE_TABLE[16];
MM_EXPORT_EMU extern const mmSChar_t*      MM_EMU_APU_FME7_ENVSTEP_TABLE[16];

struct mmEmuApuFme7
{
    struct mmEmuApuInterface super;

    struct mmEmuApuFme7Envelope envelope;
    struct mmEmuApuFme7Noise    noise;
    struct mmEmuApuFme7Channel  op[3];

    mmByte_t    address;

    mmInt_t     vol_table[0x20];
    mmInt_t     cycle_rate;

    double      cpu_clock;
};

MM_EXPORT_EMU void mmEmuApuFme7_Init(struct mmEmuApuFme7* p);
MM_EXPORT_EMU void mmEmuApuFme7_Destroy(struct mmEmuApuFme7* p);

MM_EXPORT_EMU void mmEmuApuFme7_EnvelopeRender(struct mmEmuApuFme7* p);
MM_EXPORT_EMU void mmEmuApuFme7_NoiseRender(struct mmEmuApuFme7* p);

MM_EXPORT_EMU mmInt_t mmEmuApuFme7_ChannelRender(struct mmEmuApuFme7* p, struct mmEmuApuFme7Channel* ch);

// virtual function for super.
MM_EXPORT_EMU void mmEmuApuFme7_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuFme7_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuFme7_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmInt_t mmEmuApuFme7_Process(struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuFme7_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuFme7_GetStateSize(const struct mmEmuApuInterface* super);
MM_EXPORT_EMU void mmEmuApuFme7_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuApuFme7_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer);

#include "core/mmSuffix.h"

#endif//__mmEmuApuFme7_h__
