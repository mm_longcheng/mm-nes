/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper175_h__
#define __mmEmuMapper175_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper175  15-in-1 (Kaiser)                                          //
//----------------------------------------------------------------------//
struct mmEmuMapper175
{
    struct mmEmuMapper super;

    mmByte_t reg_dat;
};

MM_EXPORT_EMU void mmEmuMapper175_Init(struct mmEmuMapper175* p);
MM_EXPORT_EMU void mmEmuMapper175_Destroy(struct mmEmuMapper175* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper175_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper175_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper175_Read(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper175_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper175_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper175_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER175;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper175_h__
