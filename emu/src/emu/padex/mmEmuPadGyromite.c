/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadGyromite.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuMmu.h"

MM_EXPORT_EMU void mmEmuPadGyromite_Init(struct mmEmuPadGyromite* p)
{
    mmEmuExpad_Init(&p->super);

    mmMemset(p->code, 0, sizeof(mmWord_t) * 8);
    p->bits = 0;
    p->arms = 0;
    p->spot = 0;

    //
    p->super.Reset = &mmEmuPadGyromite_Reset;
    p->super.Strobe = &mmEmuPadGyromite_Strobe;
    //p->super.Read4016 = &mmEmuPadGyromite_Read4016;
    //p->super.Read4017 = &mmEmuPadGyromite_Read4017;
    //p->super.Write4016 = &mmEmuPadGyromite_Write4016;
    //p->super.Write4017 = &mmEmuPadGyromite_Write4017;
    p->super.Sync = &mmEmuPadGyromite_Sync;
    p->super.SetSyncData = &mmEmuPadGyromite_SetSyncData;
    p->super.GetSyncData = &mmEmuPadGyromite_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadGyromite_Destroy(struct mmEmuPadGyromite* p)
{
    mmEmuExpad_Destroy(&p->super);

    mmMemset(p->code, 0, sizeof(mmWord_t) * 8);
    p->bits = 0;
    p->arms = 0;
    p->spot = 0;
}

MM_EXPORT_EMU mmInt_t mmEmuPadGyromite_CheckSignal(struct mmEmuPadGyromite* p)
{
    struct mmEmuRGBQuad palette[256];
    struct mmEmuRGBQuad pixel;

    mmByte_t* BGPAL = p->super.nes->mmu.BGPAL;

    mmInt_t lcv = 0;

    // throttling detection issues
    mmEmuScreen_GetPaletteData(&p->super.nes->screen, palette);
    pixel = palette[BGPAL[0]];

    for (lcv = 1; lcv < 16; lcv++)
    {
        // reject non-solid screens
        if (mmMemcmp(&(palette[BGPAL[lcv]]), &pixel, sizeof(struct mmEmuRGBQuad)) != 0)
        {
            return 0xF00;
        }
    }

    // check for a black or green screen
    return (pixel.rgbGreen > 0x10) ? 1 : 0;
}

MM_EXPORT_EMU void mmEmuPadGyromite_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadGyromite* p = (struct mmEmuPadGyromite*)(super);

    p->bits = 0;
    p->arms = 0;
    p->spot = 0;

    // Initialize light on/off sequences
    p->code[0] = 0x0EE8;    // Open arm
    p->code[1] = 0x0FA8;    // Close arm
    p->code[2] = 0x1AE8;    // Test robot
    p->code[3] = 0x0AAA;    // Unused
    p->code[4] = 0x1BA8;    // Raise arm
    p->code[5] = 0x1BE8;    // Lower arm
    p->code[6] = 0x0BA8;    // Turn left
    p->code[7] = 0x0AE8;    // Turn right
}
MM_EXPORT_EMU void mmEmuPadGyromite_Strobe(struct mmEmuExpad* super)
{
    struct mmEmuPadGyromite* p = (struct mmEmuPadGyromite*)(super);

    struct mmEmuPad* pad = &p->super.nes->pad;

    pad->pad2bit &= ~0xFF;
    pad->pad2bit |= p->arms;
}

MM_EXPORT_EMU void mmEmuPadGyromite_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadGyromite* p = (struct mmEmuPadGyromite*)(super);

    // mmInt_t signal = mmEmuPadGyromite_CheckSignal( 129 );
    mmInt_t signal = mmEmuPadGyromite_CheckSignal(p);

    // build our message
    if (signal > 0xFF)
    {
        p->bits = 0;
        p->spot = 0;
    }
    else
    {
        p->bits |= (signal << p->spot);
        p->spot++;

        // Check for a test message: BBBBBB[test]
        if (p->spot == 6 && p->bits == 0)
        {
            p->spot = 0;
        }
    }

    // decode message
    if (p->spot == 13)
    {
        mmInt_t i = 0;
        for (i = 0; i < 8; i++)
        {
            if (p->bits == p->code[i])
            {
                p->arms ^= (1 << i);
            }
        }
        p->spot = 0;
        p->bits = 0;
    }
}
MM_EXPORT_EMU void mmEmuPadGyromite_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadGyromite* p = (struct mmEmuPadGyromite*)(super);

    p->bits = ((mmWord_t)(data & 0x0000FFFF));
    p->arms = ((mmByte_t)((data >> 16) & 0x000000FF));
    p->spot = (mmByte_t)(data >> 24);
}
MM_EXPORT_EMU mmLong_t mmEmuPadGyromite_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadGyromite* p = (struct mmEmuPadGyromite*)(super);

    mmLong_t data = 0;

    data =
        (p->bits & 0xFFFF) |
        ((p->arms & 0xFF) << 16) |
        (p->spot << 24);

    return  data;
}

static struct mmEmuExpad* __static_mmEmuPadGyromite_Produce(void)
{
    struct mmEmuPadGyromite* p = (struct mmEmuPadGyromite*)mmMalloc(sizeof(struct mmEmuPadGyromite));
    mmEmuPadGyromite_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadGyromite_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadGyromite* p = (struct mmEmuPadGyromite*)(m);
    mmEmuPadGyromite_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_GYROMITE =
{
    &__static_mmEmuPadGyromite_Produce,
    &__static_mmEmuPadGyromite_Recycle,
};
