/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMmu_h__
#define __mmEmuMmu_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

/*
*
* http://wiki.nesdev.com/w/index.php/CPU_memory_map
*
* CPU memory map
*
*     Address range | Size  | Device
*     -----------------------------------------------------------------------------------------------
*     $0000-$07FF   | $0800 | 2KB internal RAM
*     $0800-$0FFF   | $0800 | Mirrors of $0000-$07FF
*     $1000-$17FF   | $0800 |
*     $1800-$1FFF   | $0800 |
*     $2000-$2007   | $0008 | NES PPU registers
*     $2008-$3FFF   | $1FF8 | Mirrors of $2000-2007 (repeats every 8 bytes)
*     $4000-$4017   | $0018 | NES APU and I/O registers
*     $4018-$401F   | $0008 | APU and I/O functionality that is normally disabled. See CPU Test Mode.
*     $4020-$FFFF   | $BFE0 | Cartridge space: PRG ROM, PRG RAM, and mapper registers (See Note)
*     -----------------------------------------------------------------------------------------------
*
* Some parts of the 2 KiB of internal RAM at $0000-$07FF have predefined purposes dictated by the 6502 architecture. 
*   The zero page is $0000-$00FF, and the stack always uses some part of the $0100-$01FF page. Games may divide up 
*   the rest however the programmer deems useful. See Sample RAM map for an example allocation strategy for this RAM.
*
* Note: Most common boards and iNES mappers address ROM and Save/Work RAM in this format:
*
*     $6000-$7FFF = Battery Backed Save or Work RAM
*     $8000-$FFFF = Usual ROM, commonly with Mapper Registers (see MMC1 and UxROM for example)
*
* The CPU expects interrupt vectors in a fixed place at the end of the cartridge space:
*
*     $FFFA-$FFFB = NMI vector
*     $FFFC-$FFFD = Reset vector
*     $FFFE-$FFFF = IRQ/BRK vector
*
* If a mapper doesn't fix $FFFA-$FFFF to some known bank (typically, along with the rest of the bank containing them, 
*   e.g. $C000-$FFFF for a 16KiB banking mapper) or use some sort of reset detection, the vectors need to be stored 
*   in all banks.
*/

// Memory type
// For PROM (CPU)
#define MM_EMU_MMU_BANKTYPE_ROM    0x00
#define MM_EMU_MMU_BANKTYPE_RAM    0xFF
#define MM_EMU_MMU_BANKTYPE_DRAM   0x01
#define MM_EMU_MMU_BANKTYPE_MAPPER 0x80
// For VROM/VRAM/CRAM (PPU)
#define MM_EMU_MMU_BANKTYPE_VROM   0x00
#define MM_EMU_MMU_BANKTYPE_CRAM   0x01
#define MM_EMU_MMU_BANKTYPE_VRAM   0x80

// Mirror type
#define MM_EMU_MMU_VRAM_HMIRROR    0x00 // Horizontal
#define MM_EMU_MMU_VRAM_VMIRROR    0x01 // Virtical
#define MM_EMU_MMU_VRAM_MIRROR4    0x02 // All screen
#define MM_EMU_MMU_VRAM_MIRROR4L   0x03 // The PA 10 L 2000 FF mirror 23 is fixed or not
#define MM_EMU_MMU_VRAM_MIRROR4H   0x04 // The PA 10 H 2400 FF mirror 27 is fixed or not

struct mmEmuMmu
{
    // CPU memory bank
    mmByte_t* CPU_MEM_BANK[8]; // 8K Unit
    mmByte_t  CPU_MEM_TYPE[8];
    mmInt_t   CPU_MEM_PAGE[8]; // For save state

    // PPU memory bank
    mmByte_t* PPU_MEM_BANK[12];// 1K Unit
    mmByte_t  PPU_MEM_TYPE[12];
    mmInt_t   PPU_MEM_PAGE[12];// For save state
    mmByte_t  CRAM_USED[16];   // For save state

    // NES memory
    mmByte_t RAM [  8 * 1024]; // NES visceral RAM
    mmByte_t WRAM[128 * 1024]; // Work RAM backup
    mmByte_t DRAM[ 40 * 1024]; // RAM disk system
    mmByte_t XRAM[  8 * 1024]; // The dummy bank
    mmByte_t ERAM[ 32 * 1024]; // Extension device for RAM

    mmByte_t CRAM[ 32 * 1024]; // RAM character pattern
    mmByte_t VRAM[  4 * 1024]; // RAM attribute and attribute name table

    mmByte_t SPRAM[0x100];     // RAM Sprite
    mmByte_t BGPAL[0x10];      // BG palette
    mmByte_t SPPAL[0x10];      // SP palette

    // Register
    mmByte_t CPUREG[0x18];     // Nes $4000-$4017
    mmByte_t PPUREG[0x04];     // Nes $2000-$2003

    // Search for Frame IRQ 4017 register.
    mmByte_t FrameIRQ;

    // PPU internal register
    mmByte_t PPU56Toggle;      // $2005-$2006 Toggle
    mmByte_t PPU7_Temp;        // $2007 read buffer
    mmWord_t loopy_t;          // same as $2005/$2006
    mmWord_t loopy_v;          // same as $2005/$2006
    mmWord_t loopy_x;          // tile x offset

    // ROM data pointer
    mmByte_t* PROM;            // PROM ptr
    mmByte_t* VROM;            // VROM ptr

    // For dis...
    mmByte_t* PROM_ACCESS;

    // ROM bank size
    mmInt_t PROM_08K_SIZE;
    mmInt_t PROM_16K_SIZE;
    mmInt_t PROM_32K_SIZE;

    mmInt_t VROM_1K_SIZE;
    mmInt_t VROM_2K_SIZE;
    mmInt_t VROM_4K_SIZE;
    mmInt_t VROM_8K_SIZE;
};

MM_EXPORT_EMU void mmEmuMmu_Init(struct mmEmuMmu* p);
MM_EXPORT_EMU void mmEmuMmu_Destroy(struct mmEmuMmu* p);

MM_EXPORT_EMU void mmEmuMmu_Reset(struct mmEmuMmu* p);

MM_EXPORT_EMU void mmEmuMmu_SetPROMBank(struct mmEmuMmu* p, mmByte_t page, mmByte_t* ptr, mmByte_t type);
MM_EXPORT_EMU void mmEmuMmu_SetPROM8KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetPROM16KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetPROM32KBank(struct mmEmuMmu* p, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetPROM32KBankArray(struct mmEmuMmu* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3);

MM_EXPORT_EMU void mmEmuMmu_SetVROMBank(struct mmEmuMmu* p, mmByte_t page, mmByte_t* ptr, mmByte_t type);
MM_EXPORT_EMU void mmEmuMmu_SetVROM1KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetVROM2KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetVROM4KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetVROM8KBank(struct mmEmuMmu* p, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetVROM8KBankArray(struct mmEmuMmu* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3, mmInt_t bank4, mmInt_t bank5, mmInt_t bank6, mmInt_t bank7);

MM_EXPORT_EMU void mmEmuMmu_SetCRAM1KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetCRAM2KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetCRAM4KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetCRAM8KBank(struct mmEmuMmu* p, mmInt_t bank);

MM_EXPORT_EMU void mmEmuMmu_SetVRAM1KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMmu_SetVRAMBank(struct mmEmuMmu* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3);
MM_EXPORT_EMU void mmEmuMmu_SetVRAMMirror(struct mmEmuMmu* p, mmInt_t type);
MM_EXPORT_EMU void mmEmuMmu_SetVRAMMirrorArray(struct mmEmuMmu* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3);

#include "core/mmSuffix.h"

#endif//__mmEmuMmu_h__
