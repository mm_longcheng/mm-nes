/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuBarcode.h"
#include "mmEmuNes.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuBarcode_Init(struct mmEmuBarcode* p)
{
    p->nes = NULL;

    p->m_bBarcode = MM_FALSE;
    p->m_BarcodeOut = 0;
    p->m_BarcodePtr = 0;
    p->m_BarcodeCycles = 0;
    mmMemset(p->m_BarcodeData, 0, sizeof(mmByte_t) * 256);

    p->m_bBarcode2 = MM_FALSE;
    p->m_Barcode2seq = 0;
    p->m_Barcode2ptr = 0;
    p->m_Barcode2cnt = 0;
    p->m_Barcode2bit = 0;
    mmMemset(p->m_Barcode2data, 0, sizeof(mmByte_t) * 32);
}
MM_EXPORT_EMU void mmEmuBarcode_Destroy(struct mmEmuBarcode* p)
{
    p->nes = NULL;

    p->m_bBarcode = MM_FALSE;
    p->m_BarcodeOut = 0;
    p->m_BarcodePtr = 0;
    p->m_BarcodeCycles = 0;
    mmMemset(p->m_BarcodeData, 0, sizeof(mmByte_t) * 256);

    p->m_bBarcode2 = MM_FALSE;
    p->m_Barcode2seq = 0;
    p->m_Barcode2ptr = 0;
    p->m_Barcode2cnt = 0;
    p->m_Barcode2bit = 0;
    mmMemset(p->m_Barcode2data, 0, sizeof(mmByte_t) * 32);
}

MM_EXPORT_EMU void mmEmuBarcode_SetParent(struct mmEmuBarcode* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU void mmEmuBarcode_SetBarcodeData(struct mmEmuBarcode* p, mmByte_t* code, mmInt_t len)
{
    struct mmEmuNes* nes = p->nes;

    struct mmLogger* gLogger = mmLogger_Instance();

    static const mmBool_t prefix_parity_type[10][6] =
    {
        { 0,0,0,0,0,0 },{ 0,0,1,0,1,1 },{ 0,0,1,1,0,1 },{ 0,0,1,1,1,0 },
        { 0,1,0,0,1,1 },{ 0,1,1,0,0,1 },{ 0,1,1,1,0,0 },{ 0,1,0,1,0,1 },
        { 0,1,0,1,1,0 },{ 0,1,1,0,1,0 },
    };
    static const mmBool_t data_left_odd[10][7] =
    {
        { 0,0,0,1,1,0,1 },{ 0,0,1,1,0,0,1 },{ 0,0,1,0,0,1,1 },{ 0,1,1,1,1,0,1 },
        { 0,1,0,0,0,1,1 },{ 0,1,1,0,0,0,1 },{ 0,1,0,1,1,1,1 },{ 0,1,1,1,0,1,1 },
        { 0,1,1,0,1,1,1 },{ 0,0,0,1,0,1,1 },
    };
    static const mmBool_t data_left_even[10][7] =
    {
        { 0,1,0,0,1,1,1 },{ 0,1,1,0,0,1,1 },{ 0,0,1,1,0,1,1 },{ 0,1,0,0,0,0,1 },
        { 0,0,1,1,1,0,1 },{ 0,1,1,1,0,0,1 },{ 0,0,0,0,1,0,1 },{ 0,0,1,0,0,0,1 },
        { 0,0,0,1,0,0,1 },{ 0,0,1,0,1,1,1 },
    };
    static const mmBool_t data_right[10][7] =
    {
        { 1,1,1,0,0,1,0 },{ 1,1,0,0,1,1,0 },{ 1,1,0,1,1,0,0 },{ 1,0,0,0,0,1,0 },
        { 1,0,1,1,1,0,0 },{ 1,0,0,1,1,1,0 },{ 1,0,1,0,0,0,0 },{ 1,0,0,0,1,0,0 },
        { 1,0,0,1,0,0,0 },{ 1,1,1,0,1,0,0 },
    };

    mmInt_t i, j, count = 0;

    mmUInt32_t crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x67898319) // Barcode World (J)
    {
        mmEmuBarcode_SetBarcode2Data(p, code, len);
        return;
    }

    mmLogger_LogD(gLogger, "%s %d code=%s len=%d.", __FUNCTION__, __LINE__, code, len);

    // Convert to number.
    for (i = 0; i < len; i++)
    {
        code[i] = code[i] - '0';
    }

    // Left margin.
    for (i = 0; i < 32; i++)
    {
        p->m_BarcodeData[count++] = 0x08;
    }
    // Left guard bar.
    p->m_BarcodeData[count++] = 0x00;
    p->m_BarcodeData[count++] = 0x08;
    p->m_BarcodeData[count++] = 0x00;

    if (len == 13)
    {
#if 0
        // Recalculating check digit.
        mmInt_t sum = 0;
        for (i = 0; i < 12; i++)
        {
            sum += (i & 1) ? (code[i] * 3) : code[i];
        }
        code[12] = (10 - (sum % 10)) % 10;
        //// test start
        //mmInt_t cs = (10 - (sum % 10)) % 10;
        //if (cs == 0)
        //{
        //  cs = 9;
        //}
        //else
        //{
        //  cs--;
        //}
        //code[12] = cs;
        //// test end
#endif
        // 6 characters on the left side.
        for (i = 0; i < 6; i++)
        {
            if (prefix_parity_type[code[0]][i])
            {
                // Even parity.
                for (j = 0; j < 7; j++)
                {
                    p->m_BarcodeData[count++] = data_left_even[code[i + 1]][j] ? 0x00 : 0x08;
                }
            }
            else
            {
                // Odd parity.
                for (j = 0; j < 7; j++)
                {
                    p->m_BarcodeData[count++] = data_left_odd[code[i + 1]][j] ? 0x00 : 0x08;
                }
            }
        }

        // Center bar.
        p->m_BarcodeData[count++] = 0x08;
        p->m_BarcodeData[count++] = 0x00;
        p->m_BarcodeData[count++] = 0x08;
        p->m_BarcodeData[count++] = 0x00;
        p->m_BarcodeData[count++] = 0x08;

        // 5 characters on the right and check digit.
        for (i = 7; i < 13; i++)
        {
            // Even parity.
            for (j = 0; j < 7; j++)
            {
                p->m_BarcodeData[count++] = data_right[code[i]][j] ? 0x00 : 0x08;
            }
        }
    }
    else
    {
        if (len == 8)
        {
            // Recalculating check digit.
            mmInt_t sum = 0;
            for (i = 0; i < 7; i++)
            {
                sum += (i & 1) ? code[i] : (code[i] * 3);
            }
            code[7] = (10 - (sum % 10)) % 10;

            // Left 4 Characters.
            for (i = 0; i < 4; i++)
            {
                // Odd parity.
                for (j = 0; j < 7; j++)
                {
                    p->m_BarcodeData[count++] = data_left_odd[code[i]][j] ? 0x00 : 0x08;
                }
            }

            // Center bar.
            p->m_BarcodeData[count++] = 0x08;
            p->m_BarcodeData[count++] = 0x00;
            p->m_BarcodeData[count++] = 0x08;
            p->m_BarcodeData[count++] = 0x00;
            p->m_BarcodeData[count++] = 0x08;

            // 3 characters on the right and check digit.
            for (i = 4; i < 8; i++)
            {
                // Even parity.
                for (j = 0; j < 7; j++)
                {
                    p->m_BarcodeData[count++] = data_right[code[i]][j] ? 0x00 : 0x08;
                }
            }
        }
    }

    // Light guard bar.
    p->m_BarcodeData[count++] = 0x00;
    p->m_BarcodeData[count++] = 0x08;
    p->m_BarcodeData[count++] = 0x00;
    // Light margin.
    for (i = 0; i < 32; i++)
    {
        p->m_BarcodeData[count++] = 0x08;
    }
    // End mark.
    p->m_BarcodeData[count++] = 0xFF;

    // Transfer start.
    p->m_bBarcode = MM_TRUE;
    p->m_BarcodeCycles = 0;
    p->m_BarcodePtr = 0;
    p->m_BarcodeOut = 0x08;

    mmEmuCpu_SetClockProcess(&nes->cpu, MM_TRUE);

    mmLogger_LogD(gLogger, "%s %d BARCODE DATA MAX:%d.", __FUNCTION__, __LINE__, count);
}
MM_EXPORT_EMU void mmEmuBarcode_Barcode(struct mmEmuBarcode* p, mmInt_t cycles)
{
    struct mmEmuNes* nes = p->nes;

    if (p->m_bBarcode)
    {
        if ((p->m_BarcodeCycles += cycles) > 1000)
        {
            p->m_BarcodeCycles = 0;
            // Stop?
            if (p->m_BarcodeData[p->m_BarcodePtr] != 0xFF)
            {
                p->m_BarcodeOut = p->m_BarcodeData[p->m_BarcodePtr++];
            }
            else
            {
                struct mmLogger* gLogger = mmLogger_Instance();

                p->m_bBarcode = MM_FALSE;
                p->m_BarcodeOut = 0;

                mmLogger_LogD(gLogger, "%s %d Barcode data trasnfer complete!!", __FUNCTION__, __LINE__);

                if (!(mmEmuNes_IsTapePlay(nes) || mmEmuNes_IsTapeRec(nes)))
                {
                    mmEmuCpu_SetClockProcess(&nes->cpu, MM_FALSE);
                }
            }
        }
    }
}

MM_EXPORT_EMU void mmEmuBarcode_SetBarcode2Data(struct mmEmuBarcode* p, mmByte_t* code, mmInt_t len)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmLogger_LogD(gLogger, "%s %d code=%s len=%d.", __FUNCTION__, __LINE__, code, len);

    if (len < 13)
    {
        return;
    }

    p->m_bBarcode2 = MM_TRUE;
    p->m_Barcode2seq = 0;
    p->m_Barcode2ptr = 0;

    mmStrcpy((char*)p->m_Barcode2data, (char*)code);

    p->m_Barcode2data[13] = 'S';
    p->m_Barcode2data[14] = 'U';
    p->m_Barcode2data[15] = 'N';
    p->m_Barcode2data[16] = 'S';
    p->m_Barcode2data[17] = 'O';
    p->m_Barcode2data[18] = 'F';
    p->m_Barcode2data[19] = 'T';
}
MM_EXPORT_EMU mmByte_t mmEmuBarcode_Barcode2(struct mmEmuBarcode* p)
{
    mmByte_t ret = 0x00;

    if (!p->m_bBarcode2 || p->m_Barcode2seq < 0)
    {
        return ret;
    }

    switch (p->m_Barcode2seq)
    {
    case 0:
        p->m_Barcode2seq++;
        p->m_Barcode2ptr = 0;
        ret = 0x04;     // d3
        break;

    case 1:
        p->m_Barcode2seq++;
        p->m_Barcode2bit = p->m_Barcode2data[p->m_Barcode2ptr];
        p->m_Barcode2cnt = 0;
        ret = 0x04;     // d3
        break;

    case 2:
        ret = (p->m_Barcode2bit & 0x01) ? 0x00 : 0x04;  // Bit rev.
        p->m_Barcode2bit >>= 1;
        if (++p->m_Barcode2cnt > 7)
        {
            p->m_Barcode2seq++;
        }
        break;
    case 3:
        if (++p->m_Barcode2ptr > 19)
        {
            p->m_bBarcode2 = MM_FALSE;
            p->m_Barcode2seq = -1;
        }
        else
        {
            p->m_Barcode2seq = 1;
        }
        break;
    default:
        break;
    }

    return ret;
}

