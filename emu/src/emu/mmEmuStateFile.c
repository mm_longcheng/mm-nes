/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuStateFile.h"
#include "mmEmuNes.h"
#include "mmEmuErrorCode.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU mmInt_t mmEmuStateFile_IsStateFile(const struct mmEmuNes* p, const char* fname)
{
    size_t sz = 0;
    FILE* fp = NULL;
    struct mmEmuFileHdr2 header;

    if (!(fp = fopen(fname, "rb")))
    {
        return -1;
    }
    sz = fread(&header, sizeof(struct mmEmuFileHdr2), 1, fp);
    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }
    if (sz != 1)
    {
        return -1;
    }

    if (mmMemcmp(header.ID, "VirtuaNES ST", sizeof(header.ID)) == 0)
    {
        if (header.BlockVersion < 0x0100)
        {
            return 0;
        }

        if (p->config.emulator.bCrcCheck)
        {
            if (header.BlockVersion >= 0x200)
            {
                if (mmEmuRom_GetMapperNo(&p->rom) != 20)
                {
                    // Other than FDS.
                    if (header.Ext0 != mmEmuRom_GetPROMCRC(&p->rom))
                    {
                        // It is different.
                        return MM_ERR_EMU_ILLEGAL_STATE_CRC;
                    }
                }
                else
                {
                    // FDS
                    if (header.Ext0 != mmEmuRom_GetGameID(&p->rom) ||
                        header.Ext1 != (mmWord_t)mmEmuRom_GetMakerID(&p->rom) ||
                        header.Ext2 != (mmWord_t)mmEmuRom_GetDiskNo(&p->rom))
                    {
                        // It is different.
                        return MM_ERR_EMU_ILLEGAL_STATE_CRC;
                    }
                }
            }
        }
        return 0;
    }

    return -1;
}
MM_EXPORT_EMU mmUInt32_t mmEmuStateFile_LoadState(struct mmEmuNes* p, const char* fname)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    FILE* fp = NULL;
    mmUInt32_t bRet = MM_UNKNOWN;

    if (mmEmuRom_IsNSF(&p->rom))
    {
        return MM_SUCCESS;
    }

    if (!(fp = fopen(fname, "rb")))
    {
        // Can not open xxx file.
        mmLogger_LogE(gLogger, "%s %d Can not open file: %s.", __FUNCTION__, __LINE__, fname);
        return MM_ERR_EMU_THERE_IS_NO_STATE_FILE;
    }
    bRet = mmEmuStateFile_ReadState(p, fp);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    if (MM_SUCCESS != bRet)
    {
        mmLogger_LogE(gLogger, "%s %d State load error.", __FUNCTION__, __LINE__);
        mmLogger_LogE(gLogger, "%s %d ReadState failure: (%u)%s.", __FUNCTION__, __LINE__, bRet, fname);
    }
    return bRet;
}
MM_EXPORT_EMU mmUInt32_t mmEmuStateFile_SaveState(struct mmEmuNes* p, const char* fname)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    FILE* fp = NULL;
    mmUInt32_t bRet = MM_UNKNOWN;

    if (mmEmuRom_IsNSF(&p->rom))
    {
        return MM_SUCCESS;
    }

    if (!(fp = fopen(fname, "wb")))
    {
        //Can not open xxx file.
        mmLogger_LogE(gLogger, "%s %d Can not open file: %s.", __FUNCTION__, __LINE__, fname);
        return MM_ERR_EMU_THERE_IS_NO_STATE_FILE;
    }

    bRet = mmEmuStateFile_WriteState(p, fp);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    if (MM_SUCCESS != bRet)
    {
        mmLogger_LogE(gLogger, "%s %d State safe error.", __FUNCTION__, __LINE__);
        mmLogger_LogE(gLogger, "%s %d WriteState failure: (%u)%s.", __FUNCTION__, __LINE__, bRet, fname);
    }
    return bRet;
}
//
MM_EXPORT_EMU int mmEmuStateFile_ReadState(struct mmEmuNes* p, FILE* fp)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;

    mmInt_t i;
    mmBool_t bHeader = MM_FALSE;
    mmWord_t Version = 0;

    struct mmEmuBlockHdr hdr;
    mmInt_t type;

    while (MM_TRUE)
    {
        // Read File
        if (fread(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
        {
            code = MM_SUCCESS;
            break;
        }

        // File Header check
        if (!bHeader)
        {
            struct mmEmuFileHdr* fh = (struct mmEmuFileHdr*)&hdr;
            if (mmMemcmp(fh->ID, "VirtuaNES ST", sizeof(fh->ID)) == 0)
            {
                Version = fh->BlockVersion;
                if (Version == 0x0100)
                {
                    // Ver 02.4.
                    bHeader = MM_TRUE;
                    // The old guy can not load during the movie.
                    if (mmEmuMovie_IsMoviePlay(&p->movie))
                    {
                        code = MM_FAILURE;
                        break;
                    }
                    // I can not load old FDS's.
                    if (mmEmuRom_GetMapperNo(&p->rom) == 20)
                    {
                        // Not supported format.
                        mmLogger_LogE(gLogger, "%s %d Not supported format.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_NOT_SUPPORTED_FORMAT;
                        break;
                    }
                }
                else
                {
                    if (Version == 0x0200 || Version == 0x0210)
                    {
                        // Ver 0.30 or later Ver 0.60 or later.
                        struct mmEmuFileHdr2 hdr2;
                        // Re-reading header section.
                        if (fseek(fp, (long)(-(long)sizeof(struct mmEmuBlockHdr)), SEEK_CUR))
                        {
                            // Failed to read file.
                            mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                            code = MM_ERR_EMU_FILE_READ;
                            break;
                        }
                        // Read File
                        if (fread(&hdr2, sizeof(struct mmEmuFileHdr2), 1, fp) != 1)
                        {
                            // Failed to read file.
                            mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                            code = MM_ERR_EMU_FILE_READ;
                            break;
                        }

#if 0
                        if (p->config.emulator.bCrcCheck)
                        {
                            // Check if it is different from the currently loaded title.                            
                            if (mmEmuRom_GetMapperNo(&p->rom) != 20)
                            {
                                // Other than FDS.                              
                                if (hdr2.Ext0 != mmEmuRom_GetPROMCRC(&p->rom))
                                {
                                    // It is different.
                                    code = MM_ERR_EMU_ILLEGAL_ROM_CRC;
                                    break;
                                }
                            }
                            else
                            {
                                // FDS
                                if (hdr2.Ext0 != mmEmuRom_GetGameID(&p->rom) ||
                                    hdr2.Ext1 != (mmWord_t)mmEmuRom_GetMakerID(&p->rom) ||
                                    hdr2.Ext2 != (mmWord_t)mmEmuRom_GetDiskNo(&p->rom))
                                {
                                    // It is different.
                                    code = MM_ERR_EMU_ILLEGAL_ROM_CRC;
                                    break;
                                }
                            }
                        }
#endif

                        // While in the movie file pointer and step number.
                        // Change and change movie to recording mode.
                        if (mmEmuMovie_IsMoviePlay(&p->movie) || mmEmuMovie_IsMovieRec(&p->movie))
                        {
                            // Can you shoot again?
                            if (p->movie.m_hedMovie.Control & 0x80)
                            {
                                if (hdr2.MovieOffset && hdr2.MovieStep)
                                {
                                    if (mmEmuMovie_IsMoviePlay(&p->movie))
                                    {
                                        // During playback
                                        // If the number of steps in the state is ahead of the record, it will not work.
                                        if (hdr2.MovieStep > p->movie.m_hedMovie.MovieStep)
                                        {
                                            code = MM_FAILURE;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        // Recording
                                        // If the number of steps in state is ahead of current.
                                        if (hdr2.MovieStep > p->movie.m_MovieStep)
                                        {
                                            code = MM_FAILURE;
                                            break;
                                        }
                                    }
                                    //DEBUGOUT( "LD STEP=%d POS=%d\n", hdr2.MovieStep, hdr2.MovieOffset );

                                    p->movie.m_bMoviePlay = MM_FALSE;
                                    p->movie.m_bMovieRec = MM_TRUE;
                                    p->movie.m_MovieStep = hdr2.MovieStep;
                                    p->movie.m_hedMovie.RecordTimes++;  // Number of Shots + 1
                                    if (fseek(p->movie.m_fpMovie, hdr2.MovieOffset, SEEK_SET))
                                    {
                                        mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                                        code = MM_ERR_EMU_FILE_WRITE;
                                        break;
                                    }
                                }
                                else
                                {
                                    code = MM_FAILURE;
                                    break;
                                }
                            }
                            else
                            {
                                code = MM_FAILURE;
                                break;
                            }
                        }
                    }
                }
                bHeader = MM_TRUE;
                continue;
            }
        }

        if (!bHeader)
        {
            // Not supported format.
            mmLogger_LogE(gLogger, "%s %d Not supported format.", __FUNCTION__, __LINE__);
            code = MM_ERR_EMU_NOT_SUPPORTED_FORMAT;
            break;
        }

        //DEBUGOUT( "HEADER ID=%8s\n", hdr.ID );

        type = -1;
        if (mmMemcmp(hdr.ID, "REG DATA", sizeof(hdr.ID)) == 0) type = 0;
        if (mmMemcmp(hdr.ID, "RAM DATA", sizeof(hdr.ID)) == 0)  type = 1;
        if (mmMemcmp(hdr.ID, "MMU DATA", sizeof(hdr.ID)) == 0)  type = 2;
        if (mmMemcmp(hdr.ID, "MMC DATA", sizeof(hdr.ID)) == 0)  type = 3;
        if (mmMemcmp(hdr.ID, "CTR DATA", sizeof(hdr.ID)) == 0)  type = 4;
        if (mmMemcmp(hdr.ID, "SND DATA", sizeof(hdr.ID)) == 0)  type = 5;

        if (mmEmuRom_GetMapperNo(&p->rom) == 20)
        {
            if (mmMemcmp(hdr.ID, "DISKDATA", sizeof(hdr.ID)) == 0)  type = 6;
        }

        if (mmMemcmp(hdr.ID, "EXCTRDAT", sizeof(hdr.ID)) == 0) type = 7;

        if (type == -1)
        {
            //DEBUGOUT( "UNKNOWN HEADER ID=%8s\n", hdr.ID );
            break;
        }

        switch (type)
        {
            // REGISTER STATE       
        case 0:
        {
            if (hdr.BlockVersion < 0x0200)
            {
                struct mmEmuX6502State  R;

                struct mmEmuRegStatO    reg;
                if (fread(&reg, sizeof(struct mmEmuRegStatO), 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_READ;
                    break;
                }

                // LOAD CPU STATE
                R.PC = reg.cpureg.cpu.PC;
                R.A = reg.cpureg.cpu.A;
                R.X = reg.cpureg.cpu.X;
                R.Y = reg.cpureg.cpu.Y;
                R.S = reg.cpureg.cpu.S;
                R.P = reg.cpureg.cpu.P;
                R.pending = reg.cpureg.cpu.I;
                mmEmuCpu_SetContext(&p->cpu, &R);
                // FrameIRQ = reg.cpureg.cpu.FrameIRQ;

                if (hdr.BlockVersion < 0x0110)
                {
                    p->emul_cycles = 0;
                    p->base_cycles = reg.cpureg.cpu.mod_cycles;
                }
                else if (hdr.BlockVersion == 0x0110)
                {
                    // FrameIRQ_cycles = reg.cpureg.cpu.mod_cycles;
                    p->emul_cycles = reg.cpureg.cpu.emul_cycles;
                    p->base_cycles = reg.cpureg.cpu.base_cycles;
                }

                // LOAD PPU STATE
                p->mmu.PPUREG[0] = reg.ppureg.ppu.reg0;
                p->mmu.PPUREG[1] = reg.ppureg.ppu.reg1;
                p->mmu.PPUREG[2] = reg.ppureg.ppu.reg2;
                p->mmu.PPUREG[3] = reg.ppureg.ppu.reg3;
                p->mmu.PPU7_Temp = reg.ppureg.ppu.reg7;
                p->mmu.loopy_t = reg.ppureg.ppu.loopy_t;
                p->mmu.loopy_v = reg.ppureg.ppu.loopy_v;
                p->mmu.loopy_x = reg.ppureg.ppu.loopy_x;
                p->mmu.PPU56Toggle = reg.ppureg.ppu.toggle56;
            }
            else
            {
                struct mmEmuX6502State R;

                struct mmEmuRegStat reg;
                if (fread(&reg, sizeof(struct mmEmuRegStat), 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_READ;
                    break;
                }

                // LOAD CPU STATE
                R.PC = reg.cpureg.cpu.PC;
                R.A = reg.cpureg.cpu.A;
                R.X = reg.cpureg.cpu.X;
                R.Y = reg.cpureg.cpu.Y;
                R.S = reg.cpureg.cpu.S;
                R.P = reg.cpureg.cpu.P;
                R.pending = reg.cpureg.cpu.I;
                mmEmuCpu_SetContext(&p->cpu, &R);

                if (hdr.BlockVersion == 0x0200)
                {
                    // FrameIRQ = reg.cpureg.cpu.FrameIRQ;
                    // bFrameIRQ_occur = (reg.cpureg.cpu.FrameIRQ_occur!=0)?TRUE:FALSE;
                    // FrameIRQ_cycles = reg.cpureg.cpu.FrameIRQ_cycles;
                }
                else
                {
                    mmEmuApu_SetFrameIRQ(
                        &p->apu,
                        (mmInt_t)reg.cpureg.cpu.FrameIRQ_cycles,
                        (mmByte_t)reg.cpureg.cpu.FrameIRQ_count,
                        (mmByte_t)reg.cpureg.cpu.FrameIRQ_type,
                        (mmByte_t)reg.cpureg.cpu.FrameIRQ,
                        (mmByte_t)reg.cpureg.cpu.FrameIRQ_occur);
                }

                p->emul_cycles = reg.cpureg.cpu.emul_cycles;
                p->base_cycles = reg.cpureg.cpu.base_cycles;

                mmEmuCpu_SetDmaCycles(&p->cpu, (mmInt_t)reg.cpureg.cpu.DMA_cycles);

                // LOAD PPU STATE
                p->mmu.PPUREG[0] = reg.ppureg.ppu.reg0;
                p->mmu.PPUREG[1] = reg.ppureg.ppu.reg1;
                p->mmu.PPUREG[2] = reg.ppureg.ppu.reg2;
                p->mmu.PPUREG[3] = reg.ppureg.ppu.reg3;
                p->mmu.PPU7_Temp = reg.ppureg.ppu.reg7;
                p->mmu.loopy_t = reg.ppureg.ppu.loopy_t;
                p->mmu.loopy_v = reg.ppureg.ppu.loopy_v;
                p->mmu.loopy_x = reg.ppureg.ppu.loopy_x;
                p->mmu.PPU56Toggle = reg.ppureg.ppu.toggle56;
            }

            // APU STATE
            // To delete the queue.
            mmEmuApu_QueueClear(&p->apu);

            // I made it to save the APU state.
            //// Bad things happen unless you stop DMC.
            //for( i = 0x4010; i <= 0x4013; i++ ) 
            //{
            //  mmEmuApu_Write(&p->apu, i, 0);
            //}
        }
        break;
        // RAM STATE 
        case 1:
        {
            struct mmEmuRamStat ram;
            if (fread(&ram, sizeof(struct mmEmuRamStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
            mmMemcpy(p->mmu.RAM, ram.RAM, sizeof(ram.RAM));
            mmMemcpy(p->mmu.BGPAL, ram.BGPAL, sizeof(ram.BGPAL));
            mmMemcpy(p->mmu.SPPAL, ram.SPPAL, sizeof(ram.SPPAL));
            mmMemcpy(p->mmu.SPRAM, ram.SPRAM, sizeof(ram.SPRAM));
            if (mmEmuRom_IsBATTERY(&p->rom))
            {
                if (fread(p->mmu.WRAM, p->SAVERAM_SIZE, 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_READ;
                    break;
                }
            }
        }
        break;
        // BANK STATE   
        case 2:
        {
            struct mmEmuMmuStat mmu;
            if (fread(&mmu, sizeof(struct mmEmuMmuStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
            if (hdr.BlockVersion == 0x100)
            {
                // A bit earlier version.
                if (mmu.CPU_MEM_TYPE[3] == MM_EMU_MMU_BANKTYPE_RAM ||
                    mmu.CPU_MEM_TYPE[3] == MM_EMU_MMU_BANKTYPE_DRAM)
                {
                    if (fread(p->mmu.CPU_MEM_BANK[3], 8 * 1024, 1, fp) != 1)
                    {
                        mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_FILE_READ;
                        break;
                    }
                }
                else if (!mmEmuRom_IsBATTERY(&p->rom))
                {
                    mmEmuMmu_SetPROM8KBank(&p->mmu, 3, mmu.CPU_MEM_PAGE[3]);
                }
                // Loading except bank 0 to 3.
                for (i = 4; i < 8; i++)
                {
                    p->mmu.CPU_MEM_TYPE[i] = mmu.CPU_MEM_TYPE[i];
                    p->mmu.CPU_MEM_PAGE[i] = mmu.CPU_MEM_PAGE[i];
                    if (p->mmu.CPU_MEM_TYPE[i] == MM_EMU_MMU_BANKTYPE_ROM)
                    {
                        mmEmuMmu_SetPROM8KBank(&p->mmu, i, p->mmu.CPU_MEM_PAGE[i]);
                    }
                    else
                    {
                        if (fread(p->mmu.CPU_MEM_BANK[i], 8 * 1024, 1, fp) != 1)
                        {
                            mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                            code = MM_ERR_EMU_FILE_READ;
                            break;
                        }
                    }
                }
                // break the for loop process.
                if (MM_UNKNOWN != code)
                {
                    break;
                }
            }
            else if (hdr.BlockVersion == 0x200)
            {
                // newest version
                // Even though there is SRAM, it's all loaded again.
                for (i = 3; i < 8; i++)
                {
                    p->mmu.CPU_MEM_TYPE[i] = mmu.CPU_MEM_TYPE[i];
                    p->mmu.CPU_MEM_PAGE[i] = mmu.CPU_MEM_PAGE[i];
                    if (p->mmu.CPU_MEM_TYPE[i] == MM_EMU_MMU_BANKTYPE_ROM)
                    {
                        mmEmuMmu_SetPROM8KBank(&p->mmu, i, p->mmu.CPU_MEM_PAGE[i]);
                    }
                    else
                    {
                        if (fread(p->mmu.CPU_MEM_BANK[i], 8 * 1024, 1, fp) != 1)
                        {
                            mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                            code = MM_ERR_EMU_FILE_READ;
                            break;
                        }
                    }
                }
                // break the for loop process.
                if (MM_UNKNOWN != code)
                {
                    break;
                }
            }
            // VRAM
            if (fread(p->mmu.VRAM, 4 * 1024, 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }

            // CRAM
            for (i = 0; i < 8; i++)
            {
                if (mmu.CRAM_USED[i] != 0)
                {
                    if (fread(&p->mmu.CRAM[0x1000 * i], 4 * 1024, 1, fp) != 1)
                    {
                        mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_FILE_READ;
                        break;
                    }
                }
            }
            // break the for loop process.
            if (MM_UNKNOWN != code)
            {
                break;
            }

            // BANK
            for (i = 0; i < 12; i++)
            {
                if (mmu.PPU_MEM_TYPE[i] == MM_EMU_MMU_BANKTYPE_VROM)
                {
                    mmEmuMmu_SetVROM1KBank(&p->mmu, i, mmu.PPU_MEM_PAGE[i]);
                }
                else if (mmu.PPU_MEM_TYPE[i] == MM_EMU_MMU_BANKTYPE_CRAM)
                {
                    mmEmuMmu_SetCRAM1KBank(&p->mmu, i, mmu.PPU_MEM_PAGE[i]);
                }
                else if (mmu.PPU_MEM_TYPE[i] == MM_EMU_MMU_BANKTYPE_VRAM)
                {
                    mmEmuMmu_SetVRAM1KBank(&p->mmu, i, mmu.PPU_MEM_PAGE[i]);
                }
                else
                {
                    mmLogger_LogE(gLogger, "%s %d Unknown bank types.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_NOT_SUPPORTED_BANK;
                    break;
                }
            }
            // break the for loop process.
            if (MM_UNKNOWN != code)
            {
                break;
            }
        }
        break;
        // MMC STATE 
        case 3:
        {
            struct mmEmuMmcStat mmc;
            if (fread(&mmc, sizeof(struct mmEmuMmcStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
            (*(p->mapper->LoadState))(p->mapper, mmc.mmcdata);
        }
        break;
        // CTR STATE            
        case 4:
        {
            struct mmEmuCtrStat ctr;
            if (fread(&ctr, sizeof(struct mmEmuCtrStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }

            p->pad.pad1bit = ctr.ctrreg.ctr.pad1bit;
            p->pad.pad2bit = ctr.ctrreg.ctr.pad2bit;
            p->pad.pad3bit = ctr.ctrreg.ctr.pad3bit;
            p->pad.pad4bit = ctr.ctrreg.ctr.pad4bit;
            mmEmuPad_SetStrobe(&p->pad, (ctr.ctrreg.ctr.strobe != 0) ? MM_TRUE : MM_FALSE);
        }
        break;

        // SND STATE    
        case 5:
        {
            struct mmEmuSndStat snd;
            if (fread(&snd, sizeof(struct mmEmuSndStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
            mmEmuApu_LoadState(&p->apu, snd.snddata);
        }
        break;

        // Disk Images
        // Ver 0.30 or later
        case 6:
        {
            struct mmEmuDiskData ddata;
            mmUInt32_t pos;
            mmByte_t data;
            mmLong_t DiskSize = 16 + 65500 * mmEmuRom_GetDiskNo(&p->rom);
            mmByte_t* lpDisk = mmEmuRom_GetPROM(&p->rom);
            mmByte_t* lpWrite = mmEmuRom_GetDISK(&p->rom);

            // Rewrite FLAG erase
            mmMemset(lpWrite, 0, 16 + 65500 * mmEmuRom_GetDiskNo(&p->rom));

            if (fread(&ddata, sizeof(struct mmEmuDiskData), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }

            for (i = 0; i < ddata.DifferentSize; i++)
            {
                if (fread(&pos, sizeof(mmUInt32_t), 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_READ;
                    break;
                }
                data = (mmByte_t)(pos >> 24);
                pos &= 0x00FFFFFF;
                if (pos >= 16 && (mmLong_t)pos < DiskSize)
                {
                    lpDisk[pos] = data;
                    lpWrite[pos] = 0xFF;
                }
            }
            // break the for loop process.
            if (MM_UNKNOWN != code)
            {
                break;
            }
        }
        break;

        // EXCTR STATE
        case  7:
        {
            struct mmEmu_ExCtrStat exctr;
            if (fread(&exctr, sizeof(struct mmEmu_ExCtrStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }

            mmEmuPad_SetSyncExData(&p->pad, exctr.data);
        }
        break;
        default:
            break;
        }
    }

    return code;
}
MM_EXPORT_EMU int mmEmuStateFile_WriteState(struct mmEmuNes* p, FILE* fp)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;

    mmInt_t i;
    struct mmEmuBlockHdr hdr;

    do
    {
        // HEADER
        {
            struct mmEmuFileHdr2 hdr;

            mmMemset(&hdr, 0, sizeof(struct mmEmuFileHdr2));
            mmMemcpy(hdr.ID, "VirtuaNES ST", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0200;

            if (mmEmuRom_GetMapperNo(&p->rom) != 20)
            {
                hdr.Ext0 = mmEmuRom_GetPROMCRC(&p->rom);
            }
            else
            {
                hdr.Ext0 = mmEmuRom_GetGameID(&p->rom);
                hdr.Ext1 = (mmWord_t)mmEmuRom_GetMakerID(&p->rom);
                hdr.Ext2 = (mmWord_t)mmEmuRom_GetDiskNo(&p->rom);
            }

            // If the movie is being played or recorded, its position is recorded.
            if (mmEmuMovie_IsMoviePlay(&p->movie) || mmEmuMovie_IsMovieRec(&p->movie))
            {
                hdr.MovieStep = p->movie.m_MovieStep;
                hdr.MovieOffset = (mmSInt32_t)ftell(p->movie.m_fpMovie);
                //DEBUGOUT( "\nSV STEP=%d POS=%d\n", m_MovieStep, hdr.MovieOffset );
            }

            // Write File
            if (fwrite(&hdr, sizeof(struct mmEmuFileHdr2), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
        }

        // REGISTER STATE
        {
            struct mmEmuRegStat reg;

            struct mmEmuX6502State  R;
            mmInt_t cycles;

            mmMemset(&hdr, 0, sizeof(struct mmEmuBlockHdr));
            mmMemset(&reg, 0, sizeof(struct mmEmuRegStat));

            // Create Header
            mmMemcpy(hdr.ID, "REG DATA", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0210;
            hdr.BlockSize = sizeof(struct mmEmuRegStat);

            // SAVE CPU STATE
            mmEmuCpu_GetContext(&p->cpu, &R);

            reg.cpureg.cpu.PC = R.PC;
            reg.cpureg.cpu.A = R.A;
            reg.cpureg.cpu.X = R.X;
            reg.cpureg.cpu.Y = R.Y;
            reg.cpureg.cpu.S = R.S;
            reg.cpureg.cpu.P = R.P;
            reg.cpureg.cpu.I = R.pending;

            mmEmuApu_GetFrameIRQ(
                &p->apu,
                &cycles,
                &reg.cpureg.cpu.FrameIRQ_count,
                &reg.cpureg.cpu.FrameIRQ_type,
                &reg.cpureg.cpu.FrameIRQ,
                &reg.cpureg.cpu.FrameIRQ_occur);

            // Because the reference is INT
            reg.cpureg.cpu.FrameIRQ_cycles = (mmSInt32_t)cycles;

            reg.cpureg.cpu.DMA_cycles = (mmSInt32_t)mmEmuCpu_GetDmaCycles(&p->cpu);
            reg.cpureg.cpu.emul_cycles = p->emul_cycles;
            reg.cpureg.cpu.base_cycles = p->base_cycles;

            // SAVE PPU STATE
            reg.ppureg.ppu.reg0 = p->mmu.PPUREG[0];
            reg.ppureg.ppu.reg1 = p->mmu.PPUREG[1];
            reg.ppureg.ppu.reg2 = p->mmu.PPUREG[2];
            reg.ppureg.ppu.reg3 = p->mmu.PPUREG[3];
            reg.ppureg.ppu.reg7 = p->mmu.PPU7_Temp;
            reg.ppureg.ppu.loopy_t = p->mmu.loopy_t;
            reg.ppureg.ppu.loopy_v = p->mmu.loopy_v;
            reg.ppureg.ppu.loopy_x = p->mmu.loopy_x;
            reg.ppureg.ppu.toggle56 = p->mmu.PPU56Toggle;

            // Write File
            if (fwrite(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
            if (fwrite(&reg, sizeof(struct mmEmuRegStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
        }

        // RAM STATE
        {
            struct mmEmuRamStat ram;
            mmUInt32_t size = 0;

            mmMemset(&hdr, 0, sizeof(struct mmEmuBlockHdr));
            mmMemset(&ram, 0, sizeof(struct mmEmuRamStat));

            // SAVE RAM STATE
            mmMemcpy(ram.RAM, p->mmu.RAM, sizeof(ram.RAM));
            mmMemcpy(ram.BGPAL, p->mmu.BGPAL, sizeof(ram.BGPAL));
            mmMemcpy(ram.SPPAL, p->mmu.SPPAL, sizeof(ram.SPPAL));
            mmMemcpy(ram.SPRAM, p->mmu.SPRAM, sizeof(ram.SPRAM));

            // S-RAM STATE (Save if it exists whether used or not used).        
            if (mmEmuRom_IsBATTERY(&p->rom))
            {
                size = p->SAVERAM_SIZE;
            }

            // Create Header
            mmMemcpy(hdr.ID, "RAM DATA", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0100;
            hdr.BlockSize = size + sizeof(struct mmEmuRamStat);

            // Write File
            if (fwrite(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
            if (fwrite(&ram, sizeof(struct mmEmuRamStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
            if (mmEmuRom_IsBATTERY(&p->rom))
            {
                if (fwrite(p->mmu.WRAM, p->SAVERAM_SIZE, 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_WRITE;
                    break;
                }
            }
        }

        // BANK STATE
        {
            struct mmEmuMmuStat mmu;
            mmUInt32_t size;

            mmMemset(&hdr, 0, sizeof(struct mmEmuBlockHdr));
            mmMemset(&mmu, 0, sizeof(struct mmEmuMmuStat));

            size = 0;
            // SAVE CPU MEMORY BANK DATA
            // BANK 0, 1, 2 are not related to bank save.
            // From Virtua NES 0.30
            // Bank 3 saves regardless of using SRAM.
            for (i = 3; i < 8; i++)
            {
                mmu.CPU_MEM_TYPE[i] = p->mmu.CPU_MEM_TYPE[i];
                mmu.CPU_MEM_PAGE[i] = p->mmu.CPU_MEM_PAGE[i];

                if (p->mmu.CPU_MEM_TYPE[i] == MM_EMU_MMU_BANKTYPE_RAM ||
                    p->mmu.CPU_MEM_TYPE[i] == MM_EMU_MMU_BANKTYPE_DRAM)
                {
                    size += 8 * 1024;   // 8K BANK
                }
            }

            // SAVE VRAM MEMORY DATA
            for (i = 0; i < 12; i++)
            {
                mmu.PPU_MEM_TYPE[i] = p->mmu.PPU_MEM_TYPE[i];
                mmu.PPU_MEM_PAGE[i] = p->mmu.PPU_MEM_PAGE[i];
            }
            // 1K BANK x 4 (VRAM)
            size += 4 * 1024;

            for (i = 0; i < 8; i++)
            {
                mmu.CRAM_USED[i] = p->mmu.CRAM_USED[i];
                if (p->mmu.CRAM_USED[i] != 0)
                {
                    // 4K BANK
                    size += 4 * 1024;
                }
            }

            // Create Header
            mmMemcpy(hdr.ID, "MMU DATA", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0200;
            hdr.BlockSize = size + sizeof(struct mmEmuMmuStat);

            // Write File
            if (fwrite(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
            if (fwrite(&mmu, sizeof(struct mmEmuMmuStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            // WRITE CPU RAM MEMORY BANK
            for (i = 3; i < 8; i++)
            {
                if (mmu.CPU_MEM_TYPE[i] != MM_EMU_MMU_BANKTYPE_ROM)
                {
                    if (fwrite(p->mmu.CPU_MEM_BANK[i], 8 * 1024, 1, fp) != 1)
                    {
                        mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_FILE_WRITE;
                        break;
                    }
                }
            }
            // break the for loop process.
            if (MM_UNKNOWN != code)
            {
                break;
            }
            // WRITE VRAM MEMORY (Always write every 4 K minutes).
            if (fwrite(p->mmu.VRAM, 4 * 1024, 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            // WRITE CRAM MEMORY
            for (i = 0; i < 8; i++)
            {
                if (p->mmu.CRAM_USED[i] != 0)
                {
                    if (fwrite(&p->mmu.CRAM[0x1000 * i], 4 * 1024, 1, fp) != 1)
                    {
                        mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_FILE_WRITE;
                        break;
                    }
                }
            }
            // break the for loop process.
            if (MM_UNKNOWN != code)
            {
                break;
            }
        }

        // MMC STATE
        {
            struct mmEmuMmcStat mmc;

            mmMemset(&hdr, 0, sizeof(struct mmEmuBlockHdr));
            mmMemset(&mmc, 0, sizeof(struct mmEmuMmcStat));

            // Create Header
            mmMemcpy(hdr.ID, "MMC DATA", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0100;
            hdr.BlockSize = sizeof(struct mmEmuMmcStat);

            if ((*(p->mapper->IsStateSave))(p->mapper))
            {
                (*(p->mapper->SaveState))(p->mapper, mmc.mmcdata);
                // Write File
                if (fwrite(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_WRITE;
                    break;
                }
                if (fwrite(&mmc, sizeof(struct mmEmuMmcStat), 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_WRITE;
                    break;
                }
            }
        }

        // CONTROLLER STATE
        {
            struct mmEmuCtrStat ctr;

            mmMemset(&hdr, 0, sizeof(struct mmEmuBlockHdr));
            mmMemset(&ctr, 0, sizeof(struct mmEmuCtrStat));

            // Create Header
            mmMemcpy(hdr.ID, "CTR DATA", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0100;
            hdr.BlockSize = sizeof(struct mmEmuCtrStat);

            ctr.ctrreg.ctr.pad1bit = p->pad.pad1bit;
            ctr.ctrreg.ctr.pad2bit = p->pad.pad2bit;
            ctr.ctrreg.ctr.pad3bit = p->pad.pad3bit;
            ctr.ctrreg.ctr.pad4bit = p->pad.pad4bit;
            ctr.ctrreg.ctr.strobe = mmEmuPad_GetStrobe(&p->pad) ? 0xFF : 0;
            //DEBUGOUT( "SV pad1bit=%08X Strobe=%d\n", pad->pad1bit, pad->GetStrobe()?1:0 );

            if (fwrite(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            if (fwrite(&ctr, sizeof(struct mmEmuCtrStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
        }

        // SND STATE
        {
            struct mmEmuSndStat snd;

            mmMemset(&hdr, 0, sizeof(struct mmEmuBlockHdr));
            mmMemset(&snd, 0, sizeof(struct mmEmuSndStat));

            // Create Header
            mmMemcpy(hdr.ID, "SND DATA", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0100;
            hdr.BlockSize = sizeof(struct mmEmuSndStat);

            mmEmuApu_SaveState(&p->apu, snd.snddata);

            if (fwrite(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            if (fwrite(&snd, sizeof(struct mmEmuSndStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
        }

        // DISKIMAGE STATE  
        if (mmEmuRom_GetMapperNo(&p->rom) == 20)
        {
            struct mmEmuDiskData dsk;
            mmByte_t* lpDisk = mmEmuRom_GetPROM(&p->rom);
            mmByte_t* lpWrite = mmEmuRom_GetDISK(&p->rom);
            mmLong_t DiskSize = 16 + 65500 * mmEmuRom_GetDiskNo(&p->rom);
            mmUInt32_t  data;

            mmMemset(&hdr, 0, sizeof(struct mmEmuBlockHdr));
            mmMemset(&dsk, 0, sizeof(struct mmEmuDiskData));

            // Count the number of differences.
            for (i = 16; i < DiskSize; i++)
            {
                if (lpWrite[i])
                {
                    dsk.DifferentSize++;
                }
            }

            mmMemcpy(hdr.ID, "DISKDATA", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0210;
            hdr.BlockSize = 0;

            // Write File
            if (fwrite(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
            // Write File
            if (fwrite(&dsk, sizeof(struct mmEmuDiskData), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            for (i = 16; i < DiskSize; i++)
            {
                if (lpWrite[i])
                {
                    data = i & 0x00FFFFFF;
                    data |= ((mmUInt32_t)lpDisk[i] & 0xFF) << 24;

                    // Write File
                    if (fwrite(&data, sizeof(mmUInt32_t), 1, fp) != 1)
                    {
                        mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_FILE_WRITE;
                        break;
                    }
                }
            }
            // break the for loop process.
            if (MM_UNKNOWN != code)
            {
                break;
            }
        }

        // EXCTR STATE  
        if (mmEmuPad_GetExController(&p->pad))
        {
            struct mmEmu_ExCtrStat exctr;

            mmMemset(&hdr, 0, sizeof(struct mmEmuBlockHdr));
            mmMemset(&exctr, 0, sizeof(struct mmEmu_ExCtrStat));

            // Create Header
            mmMemcpy(hdr.ID, "EXCTRDAT", sizeof(hdr.ID));
            hdr.BlockVersion = 0x0100;
            hdr.BlockSize = sizeof(struct mmEmu_ExCtrStat);

            // Some excontrollers will default 0
            exctr.data = mmEmuPad_GetSyncExData(&p->pad);

            if (fwrite(&hdr, sizeof(struct mmEmuBlockHdr), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            if (fwrite(&exctr, sizeof(struct mmEmu_ExCtrStat), 1, fp) != 1)
            {
                mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }
        }

        code = MM_SUCCESS;
    } while (0);

    return code;
}

