/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper013.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper013_Init(struct mmEmuMapper013* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper013_Reset;
    p->super.Write = &mmEmuMapper013_Write;
    //p->super.Read = &mmEmuMapper013_Read;
    //p->super.ReadLow = &mmEmuMapper013_ReadLow;
    //p->super.WriteLow = &mmEmuMapper013_WriteLow;
    //p->super.ExRead = &mmEmuMapper013_ExRead;
    //p->super.ExWrite = &mmEmuMapper013_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper013_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper013_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper013_HSync;
    //p->super.VSync = &mmEmuMapper013_VSync;
    //p->super.Clock = &mmEmuMapper013_Clock;
    //p->super.PPULatch = &mmEmuMapper013_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper013_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper013_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper013_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper013_IsStateSave;
    //p->super.SaveState = &mmEmuMapper013_SaveState;
    //p->super.LoadState = &mmEmuMapper013_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper013_Destroy(struct mmEmuMapper013* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper013_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper013* p = (struct mmEmuMapper013*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
    mmEmuMmu_SetCRAM4KBank(mmu, 0, 0);
    mmEmuMmu_SetCRAM4KBank(mmu, 4, 0);
}
MM_EXPORT_EMU void mmEmuMapper013_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper013* p = (struct mmEmuMapper013*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, (data & 0x30) >> 4);
    mmEmuMmu_SetCRAM4KBank(mmu, 4, data & 0x03);
}

static struct mmEmuMapper* __static_mmEmuMapper013_Produce(void)
{
    struct mmEmuMapper013* p = (struct mmEmuMapper013*)mmMalloc(sizeof(struct mmEmuMapper013));
    mmEmuMapper013_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper013_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper013* p = (struct mmEmuMapper013*)(m);
    mmEmuMapper013_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER013 =
{
    &__static_mmEmuMapper013_Produce,
    &__static_mmEmuMapper013_Recycle,
};
