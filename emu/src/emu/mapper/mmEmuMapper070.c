/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper070.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper070_Init(struct mmEmuMapper070* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;

    //
    p->super.Reset = &mmEmuMapper070_Reset;
    p->super.Write = &mmEmuMapper070_Write;
    //p->super.Read = &mmEmuMapper070_Read;
    //p->super.ReadLow = &mmEmuMapper070_ReadLow;
    //p->super.WriteLow = &mmEmuMapper070_WriteLow;
    //p->super.ExRead = &mmEmuMapper070_ExRead;
    //p->super.ExWrite = &mmEmuMapper070_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper070_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper070_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper070_HSync;
    //p->super.VSync = &mmEmuMapper070_VSync;
    //p->super.Clock = &mmEmuMapper070_Clock;
    //p->super.PPULatch = &mmEmuMapper070_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper070_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper070_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper070_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper070_IsStateSave;
    //p->super.SaveState = &mmEmuMapper070_SaveState;
    //p->super.LoadState = &mmEmuMapper070_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper070_Destroy(struct mmEmuMapper070* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;
}

MM_EXPORT_EMU void mmEmuMapper070_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper070* p = (struct mmEmuMapper070*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xa59ca2ef)          // Kamen Rider Club(J)
    {
        p->patch = 1;
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
    }
    if (crc == 0x10bb8f9a)          // Family Trainer - Manhattan Police(J)
    {
        p->patch = 1;
    }
    if (crc == 0x0cd00488)          // Space Shadow(J)
    {
        p->patch = 1;
    }

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper070_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper070* p = (struct mmEmuMapper070*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM16KBank(mmu, 4, (data & 0x70) >> 4);
    mmEmuMmu_SetVROM8KBank(mmu, data & 0x0F);

    if (p->patch)
    {
        if (data & 0x80)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
    }
    else
    {
        if (data & 0x80)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper070_Produce(void)
{
    struct mmEmuMapper070* p = (struct mmEmuMapper070*)mmMalloc(sizeof(struct mmEmuMapper070));
    mmEmuMapper070_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper070_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper070* p = (struct mmEmuMapper070*)(m);
    mmEmuMapper070_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER070 =
{
    &__static_mmEmuMapper070_Produce,
    &__static_mmEmuMapper070_Recycle,
};
