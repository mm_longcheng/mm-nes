/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuplayer.h"

#include "core/mmLogger.h"

void mmEmuplayer_Init(struct mmEmuplayer* p)
{
    mmEmuplayerProcessor_Init(&p->processor);
    mmEmuEmulator_Init(&p->emulator);

    mmEmuEmulator_SetRomPath(&p->emulator, "rom/Red Fortress.nes");
    mmEmuEmulator_SetWritablePath(&p->emulator, "data");
    mmEmuEmulator_SetResourceRoot(&p->emulator, MM_EMU_ASSETS_SOURCE, "./asset.zip", "asset");
    mmEmuEmulator_SetProcessor(&p->emulator, &p->processor.super);
}
void mmEmuplayer_Destroy(struct mmEmuplayer* p)
{
    mmEmuplayerProcessor_Destroy(&p->processor);
    mmEmuEmulator_Destroy(&p->emulator);
}

void mmEmuplayer_Start(struct mmEmuplayer* p)
{
    mmEmuEmulator_LoadRom(&p->emulator);
    mmEmuEmulator_Play(&p->emulator);

    mmEmuplayerProcessor_Start(&p->processor);
    mmEmuEmulator_Start(&p->emulator);
}
void mmEmuplayer_Interrupt(struct mmEmuplayer* p)
{
    mmEmuplayerProcessor_Interrupt(&p->processor);
    mmEmuEmulator_Interrupt(&p->emulator);
}
void mmEmuplayer_Shutdown(struct mmEmuplayer* p)
{
    mmEmuplayerProcessor_Shutdown(&p->processor);
    mmEmuEmulator_Shutdown(&p->emulator);
}
void mmEmuplayer_Join(struct mmEmuplayer* p)
{
    mmEmuplayerProcessor_Join(&p->processor);
    mmEmuEmulator_Join(&p->emulator);
}
