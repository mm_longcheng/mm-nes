/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper183.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper183_Init(struct mmEmuMapper183* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper183_Reset;
    p->super.Write = &mmEmuMapper183_Write;
    //p->super.Read = &mmEmuMapper183_Read;
    //p->super.ReadLow = &mmEmuMapper183_ReadLow;
    //p->super.WriteLow = &mmEmuMapper183_WriteLow;
    //p->super.ExRead = &mmEmuMapper183_ExRead;
    //p->super.ExWrite = &mmEmuMapper183_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper183_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper183_ExCmdWrite;
    p->super.HSync = &mmEmuMapper183_HSync;
    //p->super.VSync = &mmEmuMapper183_VSync;
    //p->super.Clock = &mmEmuMapper183_Clock;
    //p->super.PPULatch = &mmEmuMapper183_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper183_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper183_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper183_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper183_IsStateSave;
    p->super.SaveState = &mmEmuMapper183_SaveState;
    p->super.LoadState = &mmEmuMapper183_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper183_Destroy(struct mmEmuMapper183* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper183_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper183* p = (struct mmEmuMapper183*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    for (i = 0; i < 8; i++)
    {
        p->reg[i] = i;
    }
    p->irq_enable = 0;
    p->irq_counter = 0;
}
MM_EXPORT_EMU void mmEmuMapper183_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper183* p = (struct mmEmuMapper183*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8800:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0xA800:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;

    case 0xB000:
        p->reg[0] = (p->reg[0] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;
    case 0xB004:
        p->reg[0] = (p->reg[0] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;
    case 0xB008:
        p->reg[1] = (p->reg[1] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;
    case 0xB00C:
        p->reg[1] = (p->reg[1] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;

    case 0xC000:
        p->reg[2] = (p->reg[2] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;
    case 0xC004:
        p->reg[2] = (p->reg[2] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;
    case 0xC008:
        p->reg[3] = (p->reg[3] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;
    case 0xC00C:
        p->reg[3] = (p->reg[3] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;

    case 0xD000:
        p->reg[4] = (p->reg[4] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;
    case 0xD004:
        p->reg[4] = (p->reg[4] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;
    case 0xD008:
        p->reg[5] = (p->reg[5] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;
    case 0xD00C:
        p->reg[5] = (p->reg[5] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;

    case 0xE000:
        p->reg[6] = (p->reg[6] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;
    case 0xE004:
        p->reg[6] = (p->reg[6] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;
    case 0xE008:
        p->reg[7] = (p->reg[3] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;
    case 0xE00C:
        p->reg[7] = (p->reg[3] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;

    case 0x9008:
        if (data == 1)
        {
            mmInt_t i = 0;
            for (i = 0; i < 8; i++)
            {
                p->reg[i] = i;
            }
            mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
            mmEmuMmu_SetVROM8KBank(mmu, 0);
        }
        break;

    case 0x9800:
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else if (data == 3)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0xF000:
        p->irq_counter = (p->irq_counter & 0xFF00) | data;
        break;
    case 0xF004:
        p->irq_counter = (p->irq_counter & 0x00FF) | (data << 8);
        break;
    case 0xF008:
        p->irq_enable = data;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper183_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper183* p = (struct mmEmuMapper183*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable & 0x02)
    {
        if (p->irq_counter <= 113)
        {
            p->irq_counter = 0;
            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        else
        {
            p->irq_counter -= 113;
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper183_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper183_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper183* p = (struct mmEmuMapper183*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->irq_enable;
    *((mmInt_t*)&buffer[9]) = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper183_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper183* p = (struct mmEmuMapper183*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->irq_enable = buffer[8];
    p->irq_counter = *((mmInt_t*)&buffer[9]);
}

static struct mmEmuMapper* __static_mmEmuMapper183_Produce(void)
{
    struct mmEmuMapper183* p = (struct mmEmuMapper183*)mmMalloc(sizeof(struct mmEmuMapper183));
    mmEmuMapper183_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper183_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper183* p = (struct mmEmuMapper183*)(m);
    mmEmuMapper183_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER183 =
{
    &__static_mmEmuMapper183_Produce,
    &__static_mmEmuMapper183_Recycle,
};
