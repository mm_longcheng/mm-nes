/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper135.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper135_Init(struct mmEmuMapper135* p)
{
    mmEmuMapper_Init(&p->super);

    p->cmd = 0;

    p->chr0l = 0;
    p->chr1l = 0;
    p->chr0h = 0;
    p->chr1h = 0;
    p->chrch = 0;

    //
    p->super.Reset = &mmEmuMapper135_Reset;
    //p->super.Write = &mmEmuMapper135_Write;
    //p->super.Read = &mmEmuMapper135_Read;
    //p->super.ReadLow = &mmEmuMapper135_ReadLow;
    p->super.WriteLow = &mmEmuMapper135_WriteLow;
    //p->super.ExRead = &mmEmuMapper135_ExRead;
    //p->super.ExWrite = &mmEmuMapper135_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper135_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper135_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper135_HSync;
    //p->super.VSync = &mmEmuMapper135_VSync;
    //p->super.Clock = &mmEmuMapper135_Clock;
    //p->super.PPULatch = &mmEmuMapper135_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper135_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper135_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper135_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper135_IsStateSave;
    p->super.SaveState = &mmEmuMapper135_SaveState;
    p->super.LoadState = &mmEmuMapper135_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper135_Destroy(struct mmEmuMapper135* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->cmd = 0;

    p->chr0l = 0;
    p->chr1l = 0;
    p->chr0h = 0;
    p->chr1h = 0;
    p->chrch = 0;
}

MM_EXPORT_EMU void mmEmuMapper135_SetBankPPU(struct mmEmuMapper135* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetVROM2KBank(mmu, 0, 0 | (p->chr0l << 1) | (p->chrch << 4));
    mmEmuMmu_SetVROM2KBank(mmu, 2, 1 | (p->chr0h << 1) | (p->chrch << 4));
    mmEmuMmu_SetVROM2KBank(mmu, 4, 0 | (p->chr1l << 1) | (p->chrch << 4));
    mmEmuMmu_SetVROM2KBank(mmu, 6, 1 | (p->chr1h << 1) | (p->chrch << 4));
}

MM_EXPORT_EMU void mmEmuMapper135_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper135* p = (struct mmEmuMapper135*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->cmd = 0;
    p->chr0l = p->chr1l = p->chr0h = p->chr1h = p->chrch = 0;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMapper135_SetBankPPU(p);
}
MM_EXPORT_EMU void mmEmuMapper135_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper135* p = (struct mmEmuMapper135*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0x4101)
    {
    case 0x4100:
        p->cmd = data & 0x07;
        break;
    case 0x4101:
        switch (p->cmd)
        {
        case 0:
            p->chr0l = data & 0x07;
            mmEmuMapper135_SetBankPPU(p);
            break;
        case 1:
            p->chr0h = data & 0x07;
            mmEmuMapper135_SetBankPPU(p);
            break;
        case 2:
            p->chr1l = data & 0x07;
            mmEmuMapper135_SetBankPPU(p);
            break;
        case 3:
            p->chr1h = data & 0x07;
            mmEmuMapper135_SetBankPPU(p);
            break;
        case 4:
            p->chrch = data & 0x07;
            mmEmuMapper135_SetBankPPU(p);
            break;
        case 5:
            mmEmuMmu_SetPROM32KBank(mmu, data & 0x07);
            break;
        case 6:
            break;
        case 7:
            switch ((data >> 1) & 0x03)
            {
            case 0:
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
                break;
            case 1:
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
                break;
            case 2:
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
                break;
            case 3:
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }

    mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper135_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper135_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper135* p = (struct mmEmuMapper135*)(super);

    buffer[0] = p->cmd;
    buffer[1] = p->chr0l;
    buffer[2] = p->chr0h;
    buffer[3] = p->chr1l;
    buffer[4] = p->chr1h;
    buffer[5] = p->chrch;
}
MM_EXPORT_EMU void mmEmuMapper135_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper135* p = (struct mmEmuMapper135*)(super);

    p->cmd = buffer[0];
    p->chr0l = buffer[1];
    p->chr0h = buffer[2];
    p->chr0l = buffer[3];
    p->chr0h = buffer[4];
    p->chrch = buffer[5];
}

static struct mmEmuMapper* __static_mmEmuMapper135_Produce(void)
{
    struct mmEmuMapper135* p = (struct mmEmuMapper135*)mmMalloc(sizeof(struct mmEmuMapper135));
    mmEmuMapper135_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper135_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper135* p = (struct mmEmuMapper135*)(m);
    mmEmuMapper135_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER135 =
{
    &__static_mmEmuMapper135_Produce,
    &__static_mmEmuMapper135_Recycle,
};
