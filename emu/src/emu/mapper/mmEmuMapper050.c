/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper050.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper050_Init(struct mmEmuMapper050* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;

    //
    p->super.Reset = &mmEmuMapper050_Reset;
    //p->super.Write = &mmEmuMapper050_Write;
    //p->super.Read = &mmEmuMapper050_Read;
    //p->super.ReadLow = &mmEmuMapper050_ReadLow;
    p->super.WriteLow = &mmEmuMapper050_WriteLow;
    //p->super.ExRead = &mmEmuMapper050_ExRead;
    p->super.ExWrite = &mmEmuMapper050_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper050_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper050_ExCmdWrite;
    p->super.HSync = &mmEmuMapper050_HSync;
    //p->super.VSync = &mmEmuMapper050_VSync;
    //p->super.Clock = &mmEmuMapper050_Clock;
    //p->super.PPULatch = &mmEmuMapper050_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper050_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper050_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper050_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper050_IsStateSave;
    p->super.SaveState = &mmEmuMapper050_SaveState;
    p->super.LoadState = &mmEmuMapper050_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper050_Destroy(struct mmEmuMapper050* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
}

MM_EXPORT_EMU void mmEmuMapper050_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper050* p = (struct mmEmuMapper050*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->irq_enable = 0;

    mmEmuMmu_SetPROM8KBank(mmu, 3, 15);
    mmEmuMmu_SetPROM8KBank(mmu, 4, 8);
    mmEmuMmu_SetPROM8KBank(mmu, 5, 9);
    mmEmuMmu_SetPROM8KBank(mmu, 6, 0);
    mmEmuMmu_SetPROM8KBank(mmu, 7, 11);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper050_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper050* p = (struct mmEmuMapper050*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xE060) == 0x4020)
    {
        if (addr & 0x0100)
        {
            p->irq_enable = data & 0x01;
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x08) | ((data & 0x01) << 2) | ((data & 0x06) >> 1));
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper050_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper050* p = (struct mmEmuMapper050*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xE060) == 0x4020)
    {
        if (addr & 0x0100)
        {
            p->irq_enable = data & 0x01;
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x08) | ((data & 0x01) << 2) | ((data & 0x06) >> 1));
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper050_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper050* p = (struct mmEmuMapper050*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if (scanline == 21)
        {
            //mm_emu_cpu_IRQ(&nes->cpu);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper050_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper050_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper050* p = (struct mmEmuMapper050*)(super);

    buffer[0] = p->irq_enable;
}
MM_EXPORT_EMU void mmEmuMapper050_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper050* p = (struct mmEmuMapper050*)(super);

    p->irq_enable = buffer[0];
}

static struct mmEmuMapper* __static_mmEmuMapper050_Produce(void)
{
    struct mmEmuMapper050* p = (struct mmEmuMapper050*)mmMalloc(sizeof(struct mmEmuMapper050));
    mmEmuMapper050_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper050_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper050* p = (struct mmEmuMapper050*)(m);
    mmEmuMapper050_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER050 =
{
    &__static_mmEmuMapper050_Produce,
    &__static_mmEmuMapper050_Recycle,
};
