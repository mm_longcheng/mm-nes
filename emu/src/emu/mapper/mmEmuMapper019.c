/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper019.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper019_Init(struct mmEmuMapper019* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;
    p->exsound_enable = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);
    mmMemset(p->exram, 0, sizeof(mmByte_t) * 128);

    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper019_Reset;
    p->super.Write = &mmEmuMapper019_Write;
    //p->super.Read = &mmEmuMapper019_Read;
    p->super.ReadLow = &mmEmuMapper019_ReadLow;
    p->super.WriteLow = &mmEmuMapper019_WriteLow;
    //p->super.ExRead = &mmEmuMapper019_ExRead;
    //p->super.ExWrite = &mmEmuMapper019_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper019_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper019_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper019_HSync;
    //p->super.VSync = &mmEmuMapper019_VSync;
    p->super.Clock = &mmEmuMapper019_Clock;
    //p->super.PPULatch = &mmEmuMapper019_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper019_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper019_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper019_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper019_IsStateSave;
    p->super.SaveState = &mmEmuMapper019_SaveState;
    p->super.LoadState = &mmEmuMapper019_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper019_Destroy(struct mmEmuMapper019* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;
    p->exsound_enable = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);
    mmMemset(p->exram, 0, sizeof(mmByte_t) * 128);

    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper019_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;

    p->reg[0] = p->reg[1] = p->reg[2] = 0;

    mmMemset(p->exram, 0, sizeof(mmByte_t) * 128);

    p->irq_enable = 0;
    p->irq_counter = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE >= 8)
    {
        mmEmuMmu_SetVROM8KBank(mmu, mmu->VROM_8K_SIZE - 1);
    }

    p->exsound_enable = 0xFF;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xb62a7b71)      // Family Circuit '91(J)
    {
        p->patch = 1;
    }

    if (crc == 0x02738c68)      // Wagan Land 2(J)
    {
        p->patch = 3;
    }
    if (crc == 0x14942c06)      // Wagan Land 3(J)
    {
        p->patch = 2;
    }

    if (crc == 0x968dcf09)      // Final Lap(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
    if (crc == 0x3deac303)      // Rolling Thunder(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
    }

    if (crc == 0xb1b9e187)      // For Kaijuu Monogatari(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }

    if (crc == 0x6901346e)      // For Sangokushi 2 - Haou no Tairiku(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }

    //if (crc == 0xdd454208)        // Hydlide 3(J)
    //{ 
    //  mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    //}

    if (crc == 0xaf15338f ||        // For Mindseeker(J)
        crc == 0xb1b9e187 ||        // For Kaijuu Monogatari(J)
        crc == 0x96533999 ||        // Dokuganryuu Masamune(J)
        //crc == 0x2b825ce1 ||      // Namco Classic(J)
        //crc == 0x9a2b0641 ||      // Namco Classic 2(J)
        crc == 0x3296ff7a ||        // Battle Fleet(J)
        crc == 0xdd454208)          // Hydlide 3(J)
    {
        p->exsound_enable = 0;
    }

    if (crc == 0x429fd177)          // Famista '90(J)
    {
        p->exsound_enable = 0;
    }

    if (p->exsound_enable)
    {
        mmEmuApu_SelectExSound(&nes->apu, 0x10);
    }
}

MM_EXPORT_EMU mmByte_t mmEmuMapper019_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t data = 0;

    switch (addr & 0xF800)
    {
    case 0x4800:
        if (addr == 0x4800)
        {
            if (p->exsound_enable)
            {
                mmEmuApu_ExRead(&nes->apu, addr);
                data = p->exram[p->reg[2] & 0x7F];
            }
            else
            {
                data = mmu->WRAM[p->reg[2] & 0x7F];
            }
            if (p->reg[2] & 0x80)
            {
                p->reg[2] = (p->reg[2] + 1) | 0x80;
            }
            return data;
        }
        break;
    case 0x5000:
        return (mmByte_t)p->irq_counter & 0x00FF;
    case 0x5800:
        return (mmByte_t)((p->irq_counter >> 8) & 0x7F);
    case 0x6000:
    case 0x6800:
    case 0x7000:
    case 0x7800:
        return mmEmuMapper_ReadLow(super, addr);
    default:
        break;
    }

    return (mmByte_t)(addr >> 8);
}
MM_EXPORT_EMU void mmEmuMapper019_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF800)
    {
    case 0x4800:
        if (addr == 0x4800)
        {
            if (p->exsound_enable)
            {
                mmEmuApu_ExWrite(&nes->apu, addr, data);
                p->exram[p->reg[2] & 0x7F] = data;
            }
            else
            {
                mmu->WRAM[p->reg[2] & 0x7F] = data;
            }
            if (p->reg[2] & 0x80)
            {
                p->reg[2] = (p->reg[2] + 1) | 0x80;
            }
        }
        break;
    case 0x5000:
        p->irq_counter = (p->irq_counter & 0xFF00) | (mmWord_t)data;
        //if (p->irq_enable)
        //{
        //  p->irq_counter++;
        //}
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x5800:
        p->irq_counter = (p->irq_counter & 0x00FF) | ((mmWord_t)(data & 0x7F) << 8);
        p->irq_enable = data & 0x80;
        //if (p->irq_enable)
        //{
        //  p->irq_counter++;
        //}
        //if (!p->irq_enable)
        //{
        //  mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        //}
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x6000:
    case 0x6800:
    case 0x7000:
    case 0x7800:
        mmEmuMapper_WriteLow(super, addr, data);
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper019_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //if( addr >= 0xC000 ) 
    //{
    //  DEBUGOUT( "W %04X %02X L:%3d\n", addr, data, nes->GetScanline() );
    //}
    switch (addr & 0xF800)
    {
    case 0x8000:
        if ((data < 0xE0) || (p->reg[0] != 0))
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 0, data & 0x1F);
        }
        break;
    case 0x8800:
        if ((data < 0xE0) || (p->reg[0] != 0))
        {
            mmEmuMmu_SetVROM1KBank(mmu, 1, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 1, data & 0x1F);
        }
        break;
    case 0x9000:
        if ((data < 0xE0) || (p->reg[0] != 0))
        {
            mmEmuMmu_SetVROM1KBank(mmu, 2, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 2, data & 0x1F);
        }
        break;
    case 0x9800:
        if ((data < 0xE0) || (p->reg[0] != 0))
        {
            mmEmuMmu_SetVROM1KBank(mmu, 3, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 3, data & 0x1F);
        }
        break;
    case 0xA000:
        if ((data < 0xE0) || (p->reg[1] != 0))
        {
            mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 4, data & 0x1F);
        }
        break;
    case 0xA800:
        if ((data < 0xE0) || (p->reg[1] != 0))
        {
            mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 5, data & 0x1F);
        }
        break;
    case 0xB000:
        if ((data < 0xE0) || (p->reg[1] != 0))
        {
            mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 6, data & 0x1F);
        }
        break;
    case 0xB800:
        if ((data < 0xE0) || (p->reg[1] != 0))
        {
            mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 7, data & 0x1F);
        }
        break;
    case 0xC000:
        if (!p->patch)
        {
            if (data <= 0xDF)
            {
                mmEmuMmu_SetVROM1KBank(mmu, 8, data);
            }
            else
            {
                mmEmuMmu_SetVRAM1KBank(mmu, 8, data & 0x01);
            }
        }
        break;
    case 0xC800:
        if (!p->patch)
        {
            if (data <= 0xDF)
            {
                mmEmuMmu_SetVROM1KBank(mmu, 9, data);
            }
            else
            {
                mmEmuMmu_SetVRAM1KBank(mmu, 9, data & 0x01);
            }
        }
        break;
    case 0xD000:
        if (!p->patch)
        {
            if (data <= 0xDF)
            {
                mmEmuMmu_SetVROM1KBank(mmu, 10, data);
            }
            else
            {
                mmEmuMmu_SetVRAM1KBank(mmu, 10, data & 0x01);
            }
        }
        break;
    case 0xD800:
        if (!p->patch)
        {
            if (data <= 0xDF)
            {
                mmEmuMmu_SetVROM1KBank(mmu, 11, data);
            }
            else
            {
                mmEmuMmu_SetVRAM1KBank(mmu, 11, data & 0x01);
            }
        }
        break;
    case 0xE000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data & 0x3F);
        if (p->patch == 2)
        {
            if (data & 0x40)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
            }
        }
        if (p->patch == 3)
        {
            if (data & 0x80)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xE800:
        p->reg[0] = data & 0x40;
        p->reg[1] = data & 0x80;
        mmEmuMmu_SetPROM8KBank(mmu, 5, data & 0x3F);
        break;
    case 0xF000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data & 0x3F);
        break;
    case 0xF800:
        if (addr == 0xF800)
        {
            if (p->exsound_enable)
            {
                mmEmuApu_ExWrite(&nes->apu, addr, data);
            }
            p->reg[2] = data;
        }
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper019_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if ((p->irq_counter += cycles) >= 0x7FFF)
        {
            //p->irq_counter = 0x7FFF;
            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);

            p->irq_enable = 0;
            //p->irq_counter &= 0x7FFF;
            p->irq_counter = 0x7FFF;
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper019_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper019_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->irq_enable;
    *(mmWord_t*)&buffer[4] = p->irq_counter;

    mmMemcpy(&buffer[8], p->exram, sizeof(mmByte_t) * 128);
}
MM_EXPORT_EMU void mmEmuMapper019_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->irq_enable = buffer[3];
    p->irq_counter = *(mmWord_t*)&buffer[4];

    mmMemcpy(p->exram, &buffer[8], sizeof(mmByte_t) * 128);
}

static struct mmEmuMapper* __static_mmEmuMapper019_Produce(void)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)mmMalloc(sizeof(struct mmEmuMapper019));
    mmEmuMapper019_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper019_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper019* p = (struct mmEmuMapper019*)(m);
    mmEmuMapper019_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER019 =
{
    &__static_mmEmuMapper019_Produce,
    &__static_mmEmuMapper019_Recycle,
};
