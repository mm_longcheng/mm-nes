/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmApplication.h"
#include "core/mmAlloc.h"
#include "core/mmTimeCache.h"
#include "core/mmContext.h"
#include "core/mmLoggerManager.h"
#include "core/mmAtoi.h"

/////////////////////////////////////////////////////////////////
static const char* gArgumentImpl[] =
{
    "./mm_emuplayer",
    "./log",
    "7",
};
const struct mmArgument gArgument = { MM_ARRAY_SIZE(gArgumentImpl),(const char**)gArgumentImpl, };
/////////////////////////////////////////////////////////////////
static void __static_PrintHelp(void)
{
    mmPrintf("%s\n", "help:");
    mmPrintf("%s\n", "程序名       日志目录 日志等级");
    mmPrintf("%s\n", "program_name log_dir  log_lvl ");
    mmPrintf("%s\n", "./mm_emuplayer ./log    7       ");
}
/////////////////////////////////////////////////////////////////
static struct mmApplication gApplication;
struct mmApplication* mmApplication_Instance(void)
{
    return &gApplication;
}
/////////////////////////////////////////////////////////////////
void mmApplication_Init(struct mmApplication* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();

    mmContext_Init(gContext);
    mmTimeCache_Init(gTimeCache);

    mmArgument_Init(&p->argument);
    p->state = 0;
    mmEmuplayer_Init(&p->impl);
    //
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
void mmApplication_Destroy(struct mmApplication* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();

    mmArgument_Destroy(&p->argument);
    mmEmuplayer_Destroy(&p->impl);
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
    mmTimeCache_Destroy(gTimeCache);
    mmContext_Destroy(gContext);
}
void mmApplication_Initialize(struct mmApplication* p, int argc, const char **argv)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmString argument_string;
    //
    p->argument.argc = argc;
    p->argument.argv = argv;
    //
    mmString_Init(&argument_string);
    mmArgument_ToString(&p->argument, &argument_string);
    mmPrintf("%s\n", mmString_CStr(&argument_string));
    //
    if (p->argument.argc == gArgument.argc)
    {
        struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();

        mmLoggerManager_SetFileName(gLoggerManager, "mm_emuplayer");
        mmLoggerManager_SetLoggerPath(gLoggerManager, "./log");
        mmLoggerManager_SetFileSize(gLoggerManager, 300 * 1024 * 1024);
        mmLoggerManager_SetLoggerLevel(gLoggerManager, mmAtoi32(p->argument.argv[2]));
        mmLoggerManager_SetCheckFileSizeTime(gLoggerManager, 5000);
        mmLoggerManager_SetRollingIndex(gLoggerManager, 100);
        mmLoggerManager_SetIsRolling(gLoggerManager, 1);
        mmLoggerManager_SetIsConsole(gLoggerManager, 1);
        mmLoggerManager_SetIsImmediately(gLoggerManager, 1);

        //mm_logger_manager_open(gLogger_manager);
        ////////////////////////////////////////////////////////
        mmLogger_LogI(gLogger, "%s %d %s.", __FUNCTION__, __LINE__, mmString_CStr(&argument_string));
        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////
        p->state = 1;
    }
    else
    {
        __static_PrintHelp();
    }
    //
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
void mmApplication_Start(struct mmApplication* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == p->state)
    {
        struct mmContext* gContext = mmContext_Instance();
        struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
        mmLogger_LogI(gLogger, "%s begin %d.", __FUNCTION__, __LINE__);
        //
        mmContext_Start(gContext);
        mmTimeCache_Start(gTimeCache);
        //
        mmEmuplayer_Start(&p->impl);
        //
        mmLogger_LogI(gLogger, "%s end %d.", __FUNCTION__, __LINE__);
    }
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
void mmApplication_Interrupt(struct mmApplication* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
    mmEmuplayer_Interrupt(&p->impl);
    mmTimeCache_Interrupt(gTimeCache);
    mmContext_Interrupt(gContext);
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
void mmApplication_Shutdown(struct mmApplication* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
    mmEmuplayer_Shutdown(&p->impl);
    mmTimeCache_Shutdown(gTimeCache);
    mmContext_Shutdown(gContext);
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
void mmApplication_Join(struct mmApplication* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == p->state)
    {
        struct mmContext* gContext = mmContext_Instance();
        struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
        mmEmuplayer_Join(&p->impl);
        mmTimeCache_Join(gTimeCache);
        mmContext_Join(gContext);
    }
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
