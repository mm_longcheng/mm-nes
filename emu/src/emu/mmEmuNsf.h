/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuNsf_h__
#define __mmEmuNsf_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuNes;

struct mmEmuNsf
{
    struct mmEmuNes* nes;

    mmBool_t    m_bNsfPlaying;
    mmBool_t    m_bNsfInit;
    mmInt_t     m_nNsfSongNo;
    mmInt_t     m_nNsfSongMode;
};

MM_EXPORT_EMU void mmEmuNsf_Init(struct mmEmuNsf* p);
MM_EXPORT_EMU void mmEmuNsf_Destroy(struct mmEmuNsf* p);

MM_EXPORT_EMU void mmEmuNsf_SetParent(struct mmEmuNsf* p, struct mmEmuNes* parent);

MM_EXPORT_EMU void mmEmuNsf_EmulateNSF(struct mmEmuNsf* p);
MM_EXPORT_EMU void mmEmuNsf_SetNsfPlay(struct mmEmuNsf* p, mmInt_t songno, mmInt_t songmode);
MM_EXPORT_EMU void mmEmuNsf_SetNsfStop(struct mmEmuNsf* p);
static mmInline mmBool_t mmEmuNsf_IsNsfPlaying(const struct mmEmuNsf* p) { return p->m_bNsfPlaying; };

#include "core/mmSuffix.h"

#endif//__mmEmuNsf_h__
