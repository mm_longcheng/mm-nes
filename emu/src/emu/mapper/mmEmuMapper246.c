/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper246.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper246_Init(struct mmEmuMapper246* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper246_Reset;
    //p->super.Write = &mmEmuMapper246_Write;
    //p->super.Read = &mmEmuMapper246_Read;
    //p->super.ReadLow = &mmEmuMapper246_ReadLow;
    p->super.WriteLow = &mmEmuMapper246_WriteLow;
    //p->super.ExRead = &mmEmuMapper246_ExRead;
    //p->super.ExWrite = &mmEmuMapper246_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper246_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper246_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper246_HSync;
    //p->super.VSync = &mmEmuMapper246_VSync;
    //p->super.Clock = &mmEmuMapper246_Clock;
    //p->super.PPULatch = &mmEmuMapper246_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper246_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper246_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper246_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper246_IsStateSave;
    //p->super.SaveState = &mmEmuMapper246_SaveState;
    //p->super.LoadState = &mmEmuMapper246_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper246_Destroy(struct mmEmuMapper246* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper246_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper246* p = (struct mmEmuMapper246*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper246_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper246* p = (struct mmEmuMapper246*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x6000 && addr < 0x8000)
    {
        switch (addr)
        {
        case 0x6000:
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
            break;
        case 0x6001:
            mmEmuMmu_SetPROM8KBank(mmu, 5, data);
            break;
        case 0x6002:
            mmEmuMmu_SetPROM8KBank(mmu, 6, data);
            break;
        case 0x6003:
            mmEmuMmu_SetPROM8KBank(mmu, 7, data);
            break;
        case 0x6004:
            mmEmuMmu_SetVROM2KBank(mmu, 0, data);
            break;
        case 0x6005:
            mmEmuMmu_SetVROM2KBank(mmu, 2, data);
            break;
        case 0x6006:
            mmEmuMmu_SetVROM2KBank(mmu, 4, data);
            break;
        case 0x6007:
            mmEmuMmu_SetVROM2KBank(mmu, 6, data);
            break;
        default:
            mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
            break;
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper246_Produce(void)
{
    struct mmEmuMapper246* p = (struct mmEmuMapper246*)mmMalloc(sizeof(struct mmEmuMapper246));
    mmEmuMapper246_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper246_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper246* p = (struct mmEmuMapper246*)(m);
    mmEmuMapper246_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER246 =
{
    &__static_mmEmuMapper246_Produce,
    &__static_mmEmuMapper246_Recycle,
};
