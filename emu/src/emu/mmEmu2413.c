/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/***********************************************************************************

  emu2413.c -- YM2413 emulator written by Mitsutaka Okazaki 2001

  2001 01-08 : Version 0.10 -- 1st version.
  2001 01-15 : Version 0.20 -- semi-public version.
  2001 01-16 : Version 0.30 -- 1st public version.
  2001 01-17 : Version 0.31 -- Fixed bassdrum problem.
             : Version 0.32 -- LPF implemented.
  2001 01-18 : Version 0.33 -- Fixed the drum problem, refine the mix-down method.
                            -- Fixed the LFO bug.
  2001 01-24 : Version 0.35 -- Fixed the drum problem,
                               support undocumented EG behavior.
  2001 02-02 : Version 0.38 -- Improved the performance.
                               Fixed the hi-hat and cymbal model.
                               Fixed the default percussive datas.
                               Noise reduction.
                               Fixed the feedback problem.
  2001 03-03 : Version 0.39 -- Fixed some drum bugs.
                               Improved the performance.
  2001 03-04 : Version 0.40 -- Improved the feedback.
                               Change the default table size.
                               Clock and Rate can be changed during play.
  2001 06-24 : Version 0.50 -- Improved the hi-hat and the cymbal tone.
                               Added VRC7 patch (OPLL_reset_patch is changed).
                               Fix OPLL_reset() bug.
                               Added OPLL_setMask, OPLL_getMask and OPLL_toggleMask.
                               Added OPLL_writeIO.

  References:
    fmopl.c        -- 1999,2000 written by Tatsuyuki Satoh (MAME development).
    s_opl.c        -- 2001 written by mamiya (NEZplug development).
    fmgen.cpp      -- 1999,2000 written by cisc.
    fmpac.ill      -- 2000 created by NARUTO.
    MSX-Datapack
    YMU757 data sheet
    YM2143 data sheet

**************************************************************************************/
#include "mmEmu2413.h"

#include "core/mmAlloc.h"
#include "math/mmMathConst.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define OPLL_TONE_NUM 2

static const unsigned char default_inst[OPLL_TONE_NUM][(16 + 3) * 16] =
{
    {
        /* YM2413 VOICE */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x61, 0x61, 0x1e, 0x17, 0xf0, 0x7f, 0x07, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x13, 0x41, 0x0f, 0x0d, 0xce, 0xd2, 0x43, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x03, 0x01, 0x99, 0x04, 0xff, 0xc3, 0x03, 0x73, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x21, 0x61, 0x1b, 0x07, 0xaf, 0x63, 0x40, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x22, 0x21, 0x1e, 0x06, 0xf0, 0x76, 0x08, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x31, 0x22, 0x16, 0x05, 0x90, 0x71, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x21, 0x61, 0x1d, 0x07, 0x82, 0x81, 0x10, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x23, 0x21, 0x2d, 0x16, 0xc0, 0x70, 0x07, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x61, 0x21, 0x1b, 0x06, 0x64, 0x65, 0x18, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x61, 0x61, 0x0c, 0x18, 0x85, 0xa0, 0x79, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x23, 0x21, 0x87, 0x11, 0xf0, 0xa4, 0x00, 0xf7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x97, 0xe1, 0x28, 0x07, 0xff, 0xf3, 0x02, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x61, 0x10, 0x0c, 0x05, 0xf2, 0xc4, 0x40, 0xc8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x01, 0x01, 0x56, 0x03, 0xb4, 0xb2, 0x23, 0x58, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x61, 0x41, 0x89, 0x03, 0xf1, 0xf4, 0xf0, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x04, 0x21, 0x28, 0x00, 0xdf, 0xf8, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x23, 0x22, 0x00, 0x00, 0xd8, 0xf8, 0xf8, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x25, 0x18, 0x00, 0x00, 0xf8, 0xda, 0xf8, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    },
    {
        /* VRC7 VOICE */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x33, 0x01, 0x09, 0x0e, 0x94, 0x90, 0x40, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x13, 0x41, 0x0f, 0x0d, 0xce, 0xd3, 0x43, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x01, 0x12, 0x1b, 0x06, 0xff, 0xd2, 0x00, 0x32, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x61, 0x61, 0x1b, 0x07, 0xaf, 0x63, 0x20, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x22, 0x21, 0x1e, 0x06, 0xf0, 0x76, 0x08, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x66, 0x21, 0x15, 0x00, 0x93, 0x94, 0x20, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x21, 0x61, 0x1c, 0x07, 0x82, 0x81, 0x10, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x23, 0x21, 0x20, 0x1f, 0xc0, 0x71, 0x07, 0x47, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x25, 0x31, 0x26, 0x05, 0x64, 0x41, 0x18, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x17, 0x21, 0x28, 0x07, 0xff, 0x83, 0x02, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x97, 0x81, 0x25, 0x07, 0xcf, 0xc8, 0x02, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x21, 0x21, 0x54, 0x0f, 0x80, 0x7f, 0x07, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x01, 0x01, 0x56, 0x03, 0xd3, 0xb2, 0x43, 0x58, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x31, 0x21, 0x0c, 0x03, 0x82, 0xc0, 0x40, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x21, 0x01, 0x0c, 0x03, 0xd4, 0xd3, 0x40, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x04, 0x21, 0x28, 0x00, 0xdf, 0xf8, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x23, 0x22, 0x00, 0x00, 0xa8, 0xf8, 0xf8, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x25, 0x18, 0x00, 0x00, 0xf8, 0xa9, 0xf8, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    },
};

/* Size of Sintable ( 1 -- 18 can be used, but 7 -- 14 recommended.)*/
#define PG_BITS 9
#define PG_WIDTH (1<<PG_BITS)

/* Phase increment counter */
#define DP_BITS 18
#define DP_WIDTH (1<<DP_BITS)
#define DP_BASE_BITS (DP_BITS - PG_BITS)

/* Dynamic range */
#define DB_STEP 0.375
#define DB_BITS 7
#define DB_MUTE (1<<DB_BITS)

/* Dynamic range of envelope */
#define EG_STEP 0.375
#define EG_BITS 7
#define EG_MUTE (1<<EG_BITS)

/* Dynamic range of total level */
#define TL_STEP 0.75
#define TL_BITS 6
#define TL_MUTE (1<<TL_BITS)

/* Dynamic range of sustine level */
#define SL_STEP 3.0
#define SL_BITS 4
#define SL_MUTE (1<<SL_BITS)

#define EG2DB(d) ((d) * (int)(EG_STEP/DB_STEP))
#define TL2EG(d) ((d) * (int)(TL_STEP/EG_STEP))
#define SL2EG(d) ((d) * (int)(SL_STEP/EG_STEP))

/* Volume of Noise (dB) */
#define DB_NOISE (24.0)

#define DB_POS(x) (mmUInt32_t)((x)/DB_STEP)
#define DB_NEG(x) (mmUInt32_t)(DB_MUTE+DB_MUTE+(x)/DB_STEP)

/* Bits for liner value */
#define DB2LIN_AMP_BITS 10
#define SLOT_AMP_BITS (DB2LIN_AMP_BITS)

/* Bits for envelope phase incremental counter */
#define EG_DP_BITS 22
#define EG_DP_WIDTH (1<<EG_DP_BITS)

/* Bits for Pitch and Amp modulator */
#define PM_PG_BITS 8
#define PM_PG_WIDTH (1<<PM_PG_BITS)
#define PM_DP_BITS 16
#define PM_DP_WIDTH (1<<PM_DP_BITS)
#define AM_PG_BITS 8
#define AM_PG_WIDTH (1<<AM_PG_BITS)
#define AM_DP_BITS 16
#define AM_DP_WIDTH (1<<AM_DP_BITS)

/* PM table is calcurated by PM_AMP * pow(2,PM_DEPTH*sin(x)/1200) */
#define PM_AMP_BITS 8
#define PM_AMP (1<<PM_AMP_BITS)

/* PM speed(Hz) and depth(cent) */
#define PM_SPEED 6.4
#define PM_DEPTH 13.75

/* AM speed(Hz) and depth(dB) */
#define AM_SPEED 3.7
#define AM_DEPTH 4.8

/* Cut the lower b bit(s) off. */
#define HIGHBITS(c,b) ((c)>>(b))

/* Leave the lower b bit(s). */
#define LOWBITS(c,b) ((c)&((1<<(b))-1))

/* Expand x which is s bits to d bits. */
#define EXPAND_BITS(x,s,d) ((x)<<((d)-(s)))

/* Expand x which is s bits to d bits and fill expanded bits '1' */
#define EXPAND_BITS_X(x,s,d) (((x)<<((d)-(s)))|((1<<((d)-(s)))-1))

/* Adjust envelope speed which depends on sampling rate. */
#define rate_adjust(x) (mmUInt32_t)((double)(x) * p->clk / 72 / p->rate + 0.5) /* +0.5 to round */
/*************************************************************

OPLL internal interfaces

*************************************************************/
#define SLOT_BD1 12
#define SLOT_BD2 13
#define SLOT_HH 14
#define SLOT_SD 15
#define SLOT_TOM 16
#define SLOT_CYM 17

#define DPHASEARTABLE_L0 16     // 16
#define DPHASEARTABLE_SIZE 1024 // 16 * 16 * 4

#define DPHASEDRTABLE_L0 16     // 16
#define DPHASEDRTABLE_SIZE 1024 // 16 * 16 * 4

#define DPHASETABLE_L0 128      // 8 * 16 
#define DPHASETABLE_L1 16       // 16
#define DPHASETABLE_SIZE 262144 // 512 * 8 * 16 * 4

static MM_INLINE mmSInt32_t Min(mmSInt32_t i, mmSInt32_t j);

/* Table for AR to LogCurve. */
static void makeAdjustTable(void);

/* Table for dB(0 -- (1<<DB_BITS)) to Liner(0 -- DB2LIN_AMP_WIDTH) */
static void makeDB2LinTable(void);

/* Liner(+0.0 - +1.0) to dB((1<<DB_BITS) - 1 -- 0) */
static mmSInt32_t lin2db(double d);

/* Sin Table */
static void makeSinTable(void);

/* Table for Pitch Modulator */
static void makePmTable(void);

/* Table for Amp Modulator */
static void makeAmTable(void);

/* Phase increment counter table */
static void makeDphaseTable(struct mmEmuOpll* p);

static void makeTllTable(void);

/* Rate Table for Attack */
static void makeDphaseARTable(struct mmEmuOpll* p);

/* Rate Table for Decay */
static void makeDphaseDRTable(struct mmEmuOpll* p);

static void makeRksTable(void);

static void makeDefaultPatch(void);
/************************************************************

Calc Parameters

************************************************************/
static MM_INLINE mmUInt32_t calc_eg_dphase(struct mmEmuOpllSlot* slot);

/* Slot key on  */
static MM_INLINE void slotOn(struct mmEmuOpllSlot* slot);

/* Slot key off */
static MM_INLINE void slotOff(struct mmEmuOpllSlot* slot);

/* Channel key on */
static MM_INLINE void keyOn(struct mmEmuOpll *opll, int i);

/* Channel key off */
static MM_INLINE void keyOff(struct mmEmuOpll *opll, int i);

static MM_INLINE void keyOn_BD(struct mmEmuOpll *opll);
static MM_INLINE void keyOn_SD(struct mmEmuOpll *opll);
static MM_INLINE void keyOn_TOM(struct mmEmuOpll *opll);
static MM_INLINE void keyOn_HH(struct mmEmuOpll *opll);
static MM_INLINE void keyOn_CYM(struct mmEmuOpll *opll);

/* Drum key off */
static MM_INLINE void keyOff_BD(struct mmEmuOpll *opll);
static MM_INLINE void keyOff_SD(struct mmEmuOpll *opll);
static MM_INLINE void keyOff_TOM(struct mmEmuOpll *opll);
static MM_INLINE void keyOff_HH(struct mmEmuOpll *opll);
static MM_INLINE void keyOff_CYM(struct mmEmuOpll *opll);

/* Change a voice */
static MM_INLINE void setPatch(struct mmEmuOpll *opll, int i, int num);

/* Set sustine parameter */
static MM_INLINE void setSustine(struct mmEmuOpll *opll, int c, int sustine);

/* Volume : 6bit ( Volume register << 2 ) */
static MM_INLINE void setVolume(struct mmEmuOpll *opll, int c, int volume);

/* Set F-Number ( fnum : 9bit ) */
static MM_INLINE void setFnumber(struct mmEmuOpll *opll, int c, int fnum);

/* Set Block data (block : 3bit ) */
static MM_INLINE void setBlock(struct mmEmuOpll *opll, int c, int block);

/* Change Rythm Mode */
static MM_INLINE void setRythmMode(struct mmEmuOpll *opll, int mode);
/*********************************************************

Generate wave data

*********************************************************/
/* Convert Amp(0 to EG_HEIGHT) to Phase(0 to 2PI). */
#if ( SLOT_AMP_BITS - PG_BITS ) > 0
#define wave2_2pi(e)  ( (e) >> ( SLOT_AMP_BITS - PG_BITS ))
#else
#define wave2_2pi(e)  ( (e) << ( PG_BITS - SLOT_AMP_BITS ))
#endif

/* Convert Amp(0 to EG_HEIGHT) to Phase(0 to 4PI). */
#if ( SLOT_AMP_BITS - PG_BITS - 1 ) == 0
#define wave2_4pi(e)  (e)
#elif ( SLOT_AMP_BITS - PG_BITS - 1 ) > 0
#define wave2_4pi(e)  ( (e) >> ( SLOT_AMP_BITS - PG_BITS - 1 ))
#else
#define wave2_4pi(e)  ( (e) << ( 1 + PG_BITS - SLOT_AMP_BITS ))
#endif

/* Convert Amp(0 to EG_HEIGHT) to Phase(0 to 8PI). */
#if ( SLOT_AMP_BITS - PG_BITS - 2 ) == 0
#define wave2_8pi(e)  (e)
#elif ( SLOT_AMP_BITS - PG_BITS - 2 ) > 0
#define wave2_8pi(e)  ( (e) >> ( SLOT_AMP_BITS - PG_BITS - 2 ))
#else
#define wave2_8pi(e)  ( (e) << ( 2 + PG_BITS - SLOT_AMP_BITS ))
#endif

/* 16bit rand */
static MM_INLINE mmUInt32_t mrand(mmUInt32_t seed);

// static MM_INLINE mmUInt32_t DEC(mmUInt32_t db);

/* Update Noise unit */
static MM_INLINE void update_noise(struct mmEmuOpll *opll);

/* Update AM, PM unit */
static MM_INLINE void update_ampm(struct mmEmuOpll *opll);

/* PG */
static MM_INLINE mmUInt32_t calc_phase(struct mmEmuOpllSlot *slot);

/* EG */
static MM_INLINE mmUInt32_t calc_envelope(struct mmEmuOpllSlot *slot);

static MM_INLINE mmSInt32_t calc_slot_car(struct mmEmuOpllSlot *slot, mmSInt32_t fm);


static MM_INLINE mmSInt32_t calc_slot_mod(struct mmEmuOpllSlot *slot);

static MM_INLINE mmSInt32_t calc_slot_tom(struct mmEmuOpllSlot *slot);

/* calc SNARE slot */
static MM_INLINE mmSInt32_t calc_slot_snare(struct mmEmuOpllSlot *slot, mmUInt32_t whitenoise);

static MM_INLINE mmSInt32_t calc_slot_cym(struct mmEmuOpllSlot *slot, mmSInt32_t a, mmSInt32_t b, mmSInt32_t c);

static MM_INLINE mmSInt32_t calc_slot_hat(struct mmEmuOpllSlot *slot, mmSInt32_t a, mmSInt32_t b, mmSInt32_t c, mmUInt32_t whitenoise);

static mmInline void UPDATE_PG(struct mmEmuOpll* p, struct mmEmuOpllSlot* S);
static mmInline void UPDATE_TLL(struct mmEmuOpll* p, struct mmEmuOpllSlot* S);
static mmInline void UPDATE_RKS(struct mmEmuOpll* p, struct mmEmuOpllSlot* S);
static mmInline void UPDATE_WF(struct mmEmuOpll* p, struct mmEmuOpllSlot* S);
static mmInline void UPDATE_EG(struct mmEmuOpll* p, struct mmEmuOpllSlot* S);
static void UPDATE_ALL(struct mmEmuOpll* p, struct mmEmuOpllSlot* S);

/* static tables data */
static mmBool_t tables_is_make = MM_FALSE;

/* WaveTable for each envelope amp */
static mmUInt32_t fullsintable[PG_WIDTH];
static mmUInt32_t halfsintable[PG_WIDTH];
static mmUInt32_t snaretable[PG_WIDTH];

static mmSInt32_t noiseAtable[64] =
{
    -1, 1, 0,-1, 1, 0, 0,-1,
     1, 0, 0,-1, 1, 0, 0,-1,
     1, 0, 0,-1, 1, 0, 0,-1,
     1, 0, 0,-1, 1, 0, 0,-1,
     1, 0, 0, 0,-1, 1, 0, 0,
    -1, 1, 0, 0,-1, 1, 0, 0,
    -1, 1, 0, 0,-1, 1, 0, 0,
    -1, 1, 0, 0,-1, 1, 0, 0,
};

static mmSInt32_t noiseBtable[8] =
{
    -1, 1,-1, 1,
     0, 0, 0, 0,
};

static mmUInt32_t* waveform[5] = { fullsintable, halfsintable, snaretable, };

/* LFO Table */
static mmSInt32_t pmtable[PM_PG_WIDTH];
static mmSInt32_t amtable[AM_PG_WIDTH];

/* dB to Liner table */
static mmSInt32_t DB2LIN_TABLE[(DB_MUTE + DB_MUTE) * 2];

/* Liner to Log curve conversion table (for Attack rate). */
static mmUInt32_t AR_ADJUST_TABLE[1 << EG_BITS];

/* Basic voice Data */
static struct mmEmuOpllPatch default_patch[OPLL_TONE_NUM][(16 + 3) * 2];

/* KSL + TL Table */
static mmUInt32_t tllTable[16][8][1 << TL_BITS][4];
static mmSInt32_t rksTable[2][8][2];

/* Empty voice data */
static struct mmEmuOpllPatch null_patch = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };

/* Definition of envelope mode */
enum { SETTLE, ATTACK, DECAY, SUSHOLD, SUSTINE, RELEASE, FINISH, };

MM_EXPORT_EMU void mmEmuOpllPatch_FromDump(struct mmEmuOpllPatch* p, const unsigned char* dump)
{
    p[0].AM = (dump[0] >> 7) & 1;
    p[1].AM = (dump[1] >> 7) & 1;
    p[0].PM = (dump[0] >> 6) & 1;
    p[1].PM = (dump[1] >> 6) & 1;
    p[0].EG = (dump[0] >> 5) & 1;
    p[1].EG = (dump[1] >> 5) & 1;
    p[0].KR = (dump[0] >> 4) & 1;
    p[1].KR = (dump[1] >> 4) & 1;
    p[0].ML = (dump[0]) & 15;
    p[1].ML = (dump[1]) & 15;
    p[0].KL = (dump[2] >> 6) & 3;
    p[1].KL = (dump[3] >> 6) & 3;
    p[0].TL = (dump[2]) & 63;
    p[0].FB = (dump[3]) & 7;
    p[0].WF = (dump[3] >> 3) & 1;
    p[1].WF = (dump[3] >> 4) & 1;
    p[0].AR = (dump[4] >> 4) & 15;
    p[1].AR = (dump[5] >> 4) & 15;
    p[0].DR = (dump[4]) & 15;
    p[1].DR = (dump[5]) & 15;
    p[0].SL = (dump[6] >> 4) & 15;
    p[1].SL = (dump[7] >> 4) & 15;
    p[0].RR = (dump[6]) & 15;
    p[1].RR = (dump[7]) & 15;
}

MM_EXPORT_EMU void mmEmuOpllSlot_Init(struct mmEmuOpllSlot* p)
{
    p->type = 0;

    p->feedback = 0;
    mmMemset(p->output, 0, sizeof(mmSInt32_t) * 5);

    p->sintbl = NULL;
    p->phase = 0;
    p->dphase = 0;
    p->pgout = 0;

    p->fnum = 0;
    p->block = 0;
    p->volume = 0;
    p->sustine = 0;
    p->tll = 0;
    p->rks = 0;
    p->eg_mode = SETTLE;
    p->eg_phase = EG_DP_WIDTH;
    p->eg_dphase = 0;
    p->egout = 0;

    p->opll = NULL;
    p->patch = &null_patch;
}
MM_EXPORT_EMU void mmEmuOpllSlot_Destroy(struct mmEmuOpllSlot* p)
{
    p->type = 0;

    p->feedback = 0;
    mmMemset(p->output, 0, sizeof(mmSInt32_t) * 5);

    p->sintbl = NULL;
    p->phase = 0;
    p->dphase = 0;
    p->pgout = 0;

    p->fnum = 0;
    p->block = 0;
    p->volume = 0;
    p->sustine = 0;
    p->tll = 0;
    p->rks = 0;
    p->eg_mode = SETTLE;
    p->eg_phase = EG_DP_WIDTH;
    p->eg_dphase = 0;
    p->egout = 0;

    p->opll = NULL;
    p->patch = &null_patch;
}
MM_EXPORT_EMU void mmEmuOpllSlot_Reset(struct mmEmuOpllSlot* p)
{
    assert(NULL != p->opll && "NULL != p->opll is invalid.");
    // p->type = 0; // type not need reset.

    p->feedback = 0;
    mmMemset(p->output, 0, sizeof(mmSInt32_t) * 5);

    //p->sintbl = p->opll->waveform[0];
    p->sintbl = waveform[0];
    p->phase = 0;
    p->dphase = 0;
    p->pgout = 0;

    p->fnum = 0;
    p->block = 0;
    p->volume = 0;
    p->sustine = 0;
    p->tll = 0;
    p->rks = 0;
    p->eg_mode = SETTLE;
    p->eg_phase = EG_DP_WIDTH;
    p->eg_dphase = 0;
    p->egout = 0;

    // p->opll = NULL;// opll not need reset.
    p->patch = &null_patch;
}
MM_EXPORT_EMU void mmEmuOpllSlot_SetOpll(struct mmEmuOpllSlot* p, struct mmEmuOpll* opll)
{
    p->opll = opll;
    //
    //p->sintbl = p->opll->waveform[0];
    p->sintbl = waveform[0];
}
MM_EXPORT_EMU void mmEmuOpllSlot_SetPatch(struct mmEmuOpllSlot* p, struct mmEmuOpllPatch* patch)
{
    p->patch = patch;
}
MM_EXPORT_EMU void mmEmuOpllSlot_SetVolume(struct mmEmuOpllSlot* p, int volume)
{
    p->volume = volume;
}

MM_EXPORT_EMU void mmEmuOpllCh_Init(struct mmEmuOpllCh* p)
{
    mmEmuOpllSlot_Init(&p->mod);
    mmEmuOpllSlot_Init(&p->car);
    p->patch_number = 0;
    p->key_status = 0;
    //
    p->mod.type = 0;
    p->car.type = 1;
}
MM_EXPORT_EMU void mmEmuOpllCh_Destroy(struct mmEmuOpllCh* p)
{
    mmEmuOpllSlot_Destroy(&p->mod);
    mmEmuOpllSlot_Destroy(&p->car);
    p->patch_number = 0;
    p->key_status = 0;
    //
    p->mod.type = 0;
    p->car.type = 1;
}
MM_EXPORT_EMU void mmEmuOpllCh_Reset(struct mmEmuOpllCh* p)
{
    mmEmuOpllSlot_Reset(&p->mod);
    mmEmuOpllSlot_Reset(&p->car);
    p->patch_number = 0;
    p->key_status = 0;
}
MM_EXPORT_EMU void mmEmuOpllCh_SetOpll(struct mmEmuOpllCh* p, struct mmEmuOpll* opll)
{
    mmEmuOpllSlot_SetOpll(&p->mod, opll);
    mmEmuOpllSlot_SetOpll(&p->car, opll);
}

MM_EXPORT_EMU void mmEmuOpll_Init(struct mmEmuOpll* p)
{
    int i = 0;

    // try make tables.
    mmEmuOpll_MakeTables();

    mmMemset(p->ch, 0, sizeof(struct mmEmuOpllCh) * 9);
    mmMemset(p->slot, 0, sizeof(struct mmEmuOpllSlot*) * 18);

    mmMemset(p->patch, 0, sizeof(struct mmEmuOpllPatch) * 19 * 2);

    p->adr = 0;

    mmMemset(p->output, 0, sizeof(mmSInt32_t) * 2);

    mmMemset(p->reg, 0, sizeof(unsigned char) * 0x40);
    mmMemset(p->slot_on_flag, 0, sizeof(int) * 18);

    p->rythm_mode = 0;

    p->pm_phase = 0;
    p->lfo_pm = 0;

    p->am_phase = 0;
    p->lfo_am = 0;

    p->noise_seed = 0xffff;
    p->whitenoise = 0;
    p->noiseA = 0;
    p->noiseB = 0;
    p->noiseA_phase = 0;
    p->noiseB_phase = 0;
    p->noiseA_idx = 0;
    p->noiseB_idx = 0;
    p->noiseA_dphase = 0;
    p->noiseB_dphase = 0;

    mmMemset(p->patch_update, 0, sizeof(int) * 2);

    p->mask = 0;

    p->masterVolume = 32;

    /* Tables and config status. */

    p->rate = 22050;
    p->clk = 3579545;

    p->pm_dphase = 0;
    p->am_dphase = 0;

    p->dphaseARTable = (mmUInt32_t*)mmMalloc(DPHASEARTABLE_SIZE);
    p->dphaseDRTable = (mmUInt32_t*)mmMalloc(DPHASEDRTABLE_SIZE);

    p->dphaseTable = (mmUInt32_t*)mmMalloc(DPHASETABLE_SIZE);

    mmMemset(p->dphaseARTable, 0, DPHASEARTABLE_SIZE);
    mmMemset(p->dphaseDRTable, 0, DPHASEDRTABLE_SIZE);

    mmMemset(p->dphaseTable, 0, DPHASETABLE_SIZE);

    //mmMemset(p->dphaseARTable, 0, sizeof(mmUInt32_t) * 16 * 16);
    //mmMemset(p->dphaseDRTable, 0, sizeof(mmUInt32_t) * 16 * 16);

    //mmMemset(p->dphaseTable, 0, sizeof(mmUInt32_t) * 512 * 8 * 16);

    for (i = 0; i < 9; i++)
    {
        mmEmuOpllCh_Init(&p->ch[i]);
        mmEmuOpllCh_SetOpll(&p->ch[i], p);

        p->slot[i * 2 + 0] = &p->ch[i].mod;
        p->slot[i * 2 + 1] = &p->ch[i].car;
    }
}
MM_EXPORT_EMU void mmEmuOpll_Destroy(struct mmEmuOpll* p)
{
    int i = 0;

    mmFree(p->dphaseARTable);
    mmFree(p->dphaseDRTable);

    mmFree(p->dphaseTable);
    //
    mmMemset(p->ch, 0, sizeof(struct mmEmuOpllCh) * 9);
    mmMemset(p->slot, 0, sizeof(struct mmEmuOpllSlot*) * 18);

    mmMemset(p->patch, 0, sizeof(struct mmEmuOpllPatch) * 19 * 2);

    p->adr = 0;

    mmMemset(p->output, 0, sizeof(mmSInt32_t) * 2);

    mmMemset(p->reg, 0, sizeof(unsigned char) * 0x40);
    mmMemset(p->slot_on_flag, 0, sizeof(int) * 18);

    p->rythm_mode = 0;

    p->pm_phase = 0;
    p->lfo_pm = 0;

    p->am_phase = 0;
    p->lfo_am = 0;

    p->noise_seed = 0xffff;
    p->whitenoise = 0;
    p->noiseA = 0;
    p->noiseB = 0;
    p->noiseA_phase = 0;
    p->noiseB_phase = 0;
    p->noiseA_idx = 0;
    p->noiseB_idx = 0;
    p->noiseA_dphase = 0;
    p->noiseB_dphase = 0;

    mmMemset(p->patch_update, 0, sizeof(int) * 2);

    p->mask = 0;

    p->masterVolume = 32;

    /* Tables and config status. */

    p->rate = 22050;
    p->clk = 3579545;

    p->pm_dphase = 0;
    p->am_dphase = 0;

    p->dphaseARTable = NULL;
    p->dphaseDRTable = NULL;

    p->dphaseTable = NULL;

    for (i = 0; i < 9; i++)
    {
        mmEmuOpllCh_Destroy(&p->ch[i]);
    }
}
/* Reset whole of OPLL except patch datas. */
MM_EXPORT_EMU void mmEmuOpll_Reset(struct mmEmuOpll* p)
{
    int i;

    p->adr = 0;

    p->output[0] = 0;
    p->output[1] = 0;

    p->pm_phase = 0;
    p->am_phase = 0;

    p->noise_seed = 0xffff;
    p->noiseA = 0;
    p->noiseB = 0;
    p->noiseA_phase = 0;
    p->noiseB_phase = 0;
    p->noiseA_dphase = 0;
    p->noiseB_dphase = 0;
    p->noiseA_idx = 0;
    p->noiseB_idx = 0;

    for (i = 0; i < 9; i++)
    {
        mmEmuOpllCh_Reset(&p->ch[i]);
        setPatch(p, i, 0);
    }

    for (i = 0; i < 0x40; i++)
    {
        mmEmuOpll_WriteReg(p, i, 0);
    }
}
/* Reset patch datas by system default. */
MM_EXPORT_EMU void mmEmuOpll_ResetPatch(struct mmEmuOpll* p, int type)
{
    int i;

    for (i = 0; i < 19 * 2; i++)
    {
        mmEmuOpll_CopyPatch(p, i, &default_patch[type%OPLL_TONE_NUM][i]);
    }
}
MM_EXPORT_EMU void mmEmuOpll_SetClock(struct mmEmuOpll* p, mmUInt32_t c, mmUInt32_t r)
{
    p->clk = c;
    p->rate = r;
    makeDphaseTable(p);
    makeDphaseARTable(p);
    makeDphaseDRTable(p);
    p->pm_dphase = (mmUInt32_t)rate_adjust(PM_SPEED * PM_DP_WIDTH / (p->clk / 72));
    p->am_dphase = (mmUInt32_t)rate_adjust(AM_SPEED * AM_DP_WIDTH / (p->clk / 72));
}
MM_EXPORT_EMU void mmEmuOpll_SetMasterVolume(struct mmEmuOpll* p, int masterVolume)
{
    p->masterVolume = masterVolume;
}

MM_EXPORT_EMU void mmEmuOpll_WriteIO(struct mmEmuOpll* p, mmUInt32_t adr, mmUInt32_t val)
{
    adr &= 0xff;
    if (adr == 0x7C)
    {
        p->adr = val;
    }
    else if (adr == 0x7D)
    {
        mmEmuOpll_WriteReg(p, p->adr, val);
    }
}
MM_EXPORT_EMU void mmEmuOpll_WriteReg(struct mmEmuOpll* p, mmUInt32_t reg, mmUInt32_t data)
{
    int i, v, ch;

    data = data & 0xff;
    reg = reg & 0x3f;

    switch (reg)
    {
    case 0x00:
        p->patch[0].AM = (data >> 7) & 1;
        p->patch[0].PM = (data >> 6) & 1;
        p->patch[0].EG = (data >> 5) & 1;
        p->patch[0].KR = (data >> 4) & 1;
        p->patch[0].ML = (data) & 15;
        for (i = 0; i < 9; i++)
        {
            if (p->ch[i].patch_number == 0)
            {
                UPDATE_PG(p, &p->ch[i].mod);
                UPDATE_RKS(p, &p->ch[i].mod);
                UPDATE_EG(p, &p->ch[i].mod);
            }
        }
        break;

    case 0x01:
        p->patch[1].AM = (data >> 7) & 1;
        p->patch[1].PM = (data >> 6) & 1;
        p->patch[1].EG = (data >> 5) & 1;
        p->patch[1].KR = (data >> 4) & 1;
        p->patch[1].ML = (data) & 15;
        for (i = 0; i < 9; i++)
        {
            if (p->ch[i].patch_number == 0)
            {
                UPDATE_PG(p, &p->ch[i].car);
                UPDATE_RKS(p, &p->ch[i].car);
                UPDATE_EG(p, &p->ch[i].car);
            }
        }
        break;

    case 0x02:
        p->patch[0].KL = (data >> 6) & 3;
        p->patch[0].TL = (data) & 63;
        for (i = 0; i < 9; i++)
        {
            if (p->ch[i].patch_number == 0)
            {
                UPDATE_TLL(p, &p->ch[i].mod);
            }
        }
        break;

    case 0x03:
        p->patch[1].KL = (data >> 6) & 3;
        p->patch[1].WF = (data >> 4) & 1;
        p->patch[0].WF = (data >> 3) & 1;
        p->patch[0].FB = (data) & 7;
        for (i = 0; i < 9; i++)
        {
            if (p->ch[i].patch_number == 0)
            {
                UPDATE_WF(p, &p->ch[i].mod);
                UPDATE_WF(p, &p->ch[i].car);
            }
        }
        break;

    case 0x04:
        p->patch[0].AR = (data >> 4) & 15;
        p->patch[0].DR = (data) & 15;
        for (i = 0; i < 9; i++)
        {
            if (p->ch[i].patch_number == 0)
            {
                UPDATE_EG(p, &p->ch[i].mod);
            }
        }
        break;

    case 0x05:
        p->patch[1].AR = (data >> 4) & 15;
        p->patch[1].DR = (data) & 15;
        for (i = 0; i < 9; i++)
        {
            if (p->ch[i].patch_number == 0)
            {
                UPDATE_EG(p, &p->ch[i].car);
            }
        }
        break;

    case 0x06:
        p->patch[0].SL = (data >> 4) & 15;
        p->patch[0].RR = (data) & 15;
        for (i = 0; i < 9; i++)
        {
            if (p->ch[i].patch_number == 0)
            {
                UPDATE_EG(p, &p->ch[i].mod);
            }
        }
        break;

    case 0x07:
        p->patch[1].SL = (data >> 4) & 15;
        p->patch[1].RR = (data) & 15;
        for (i = 0; i < 9; i++)
        {
            if (p->ch[i].patch_number == 0)
            {
                UPDATE_EG(p, &p->ch[i].car);
            }
        }
        break;

    case 0x0e:

        if (p->rythm_mode)
        {
            p->slot_on_flag[SLOT_BD1] = (p->reg[0x0e] & 0x10) | (p->reg[0x26] & 0x10);
            p->slot_on_flag[SLOT_BD2] = (p->reg[0x0e] & 0x10) | (p->reg[0x26] & 0x10);
            p->slot_on_flag[SLOT_SD] = (p->reg[0x0e] & 0x08) | (p->reg[0x27] & 0x10);
            p->slot_on_flag[SLOT_HH] = (p->reg[0x0e] & 0x01) | (p->reg[0x27] & 0x10);
            p->slot_on_flag[SLOT_TOM] = (p->reg[0x0e] & 0x04) | (p->reg[0x28] & 0x10);
            p->slot_on_flag[SLOT_CYM] = (p->reg[0x0e] & 0x02) | (p->reg[0x28] & 0x10);
        }
        else
        {
            p->slot_on_flag[SLOT_BD1] = (p->reg[0x26] & 0x10);
            p->slot_on_flag[SLOT_BD2] = (p->reg[0x26] & 0x10);
            p->slot_on_flag[SLOT_SD] = (p->reg[0x27] & 0x10);
            p->slot_on_flag[SLOT_HH] = (p->reg[0x27] & 0x10);
            p->slot_on_flag[SLOT_TOM] = (p->reg[0x28] & 0x10);
            p->slot_on_flag[SLOT_CYM] = (p->reg[0x28] & 0x10);
        }

        if (((data >> 5) & 1) ^ (p->rythm_mode))
        {
            setRythmMode(p, (data & 32) >> 5);
        }

        if (p->rythm_mode)
        {
            if (data & 0x10) keyOn_BD(p); else keyOff_BD(p);
            if (data & 0x8) keyOn_SD(p); else keyOff_SD(p);
            if (data & 0x4) keyOn_TOM(p); else keyOff_TOM(p);
            if (data & 0x2) keyOn_CYM(p); else keyOff_CYM(p);
            if (data & 0x1) keyOn_HH(p); else keyOff_HH(p);
        }
        UPDATE_ALL(p, &p->ch[6].mod);
        UPDATE_ALL(p, &p->ch[6].car);
        UPDATE_ALL(p, &p->ch[7].mod);
        UPDATE_ALL(p, &p->ch[7].car);
        UPDATE_ALL(p, &p->ch[8].mod);
        UPDATE_ALL(p, &p->ch[8].car);
        break;

    case 0x0f:
        break;

    case 0x10:  case 0x11:  case 0x12:  case 0x13:
    case 0x14:  case 0x15:  case 0x16:  case 0x17:
    case 0x18:
        ch = reg - 0x10;
        setFnumber(p, ch, data + ((p->reg[0x20 + ch] & 1) << 8));
        UPDATE_ALL(p, &p->ch[ch].mod);
        UPDATE_ALL(p, &p->ch[ch].car);
        switch (reg)
        {
        case 0x17:
            p->noiseA_dphase = (data + ((p->reg[0x27] & 1) << 8)) << ((p->reg[0x27] >> 1) & 7);
            break;
        case 0x18:
            p->noiseB_dphase = (data + ((p->reg[0x28] & 1) << 8)) << ((p->reg[0x28] >> 1) & 7);
            break;
        default:
            break;
        }
        break;

    case 0x20:  case 0x21:  case 0x22:  case 0x23:
    case 0x24:  case 0x25:  case 0x26:  case 0x27:
    case 0x28:

        ch = reg - 0x20;
        setFnumber(p, ch, ((data & 1) << 8) + p->reg[0x10 + ch]);
        setBlock(p, ch, (data >> 1) & 7);
        p->slot_on_flag[ch * 2] = p->slot_on_flag[ch * 2 + 1] = (p->reg[reg]) & 0x10;

        if (p->rythm_mode)
        {
            switch (reg)
            {
            case 0x26:
                p->slot_on_flag[SLOT_BD1] |= (p->reg[0x0e]) & 0x10;
                p->slot_on_flag[SLOT_BD2] |= (p->reg[0x0e]) & 0x10;
                break;

            case 0x27:
                p->noiseA_dphase = (((data & 1) << 8) + p->reg[0x17]) << ((data >> 1) & 7);
                p->slot_on_flag[SLOT_SD] |= (p->reg[0x0e]) & 0x08;
                p->slot_on_flag[SLOT_HH] |= (p->reg[0x0e]) & 0x01;
                break;

            case 0x28:
                p->noiseB_dphase = (((data & 1) << 8) + p->reg[0x18]) << ((data >> 1) & 7);
                p->slot_on_flag[SLOT_TOM] |= (p->reg[0x0e]) & 0x04;
                p->slot_on_flag[SLOT_CYM] |= (p->reg[0x0e]) & 0x02;
                break;

            default:
                break;
            }
        }

        if ((p->reg[reg] ^ data) & 0x20)
        {
            setSustine(p, ch, (data >> 5) & 1);
        }
        if (data & 0x10)
        {
            keyOn(p, ch);
        }
        else
        {
            keyOff(p, ch);
        }
        UPDATE_ALL(p, &p->ch[ch].mod);
        UPDATE_ALL(p, &p->ch[ch].car);
        break;

    case 0x30: case 0x31: case 0x32: case 0x33: case 0x34:
    case 0x35: case 0x36: case 0x37: case 0x38:
        i = (data >> 4) & 15;
        v = data & 15;
        if ((p->rythm_mode) && (reg >= 0x36))
        {
            switch (reg)
            {
            case 0x37:
                mmEmuOpllSlot_SetVolume(&p->ch[7].mod, i << 2);
                break;
            case 0x38:
                mmEmuOpllSlot_SetVolume(&p->ch[8].mod, i << 2);
                break;
            default:
                break;
            }
        }
        else
        {
            setPatch(p, reg - 0x30, i);
        }

        setVolume(p, reg - 0x30, v << 2);
        UPDATE_ALL(p, &p->ch[reg - 0x30].mod);
        UPDATE_ALL(p, &p->ch[reg - 0x30].car);
        break;

    default:
        break;

    }

    p->reg[reg] = (unsigned char)data;
}
MM_EXPORT_EMU mmSInt16_t mmEmuOpll_Calc(struct mmEmuOpll* p)
{
    mmSInt32_t inst = 0, perc = 0, out = 0;
    mmSInt32_t rythmC = 0, rythmH = 0;
    int i;

    update_ampm(p);
    update_noise(p);

    for (i = 0; i < 6; i++)
    {
        if (!(p->mask&MM_EMU_OPLL_MASK_CH(i)) && (p->ch[i].car.eg_mode != FINISH))
        {
            inst += calc_slot_car(&p->ch[i].car, calc_slot_mod(&p->ch[i].mod));
        }
    }

    if (!p->rythm_mode)
    {
        for (i = 6; i < 9; i++)
        {
            if (!(p->mask&MM_EMU_OPLL_MASK_CH(i)) && (p->ch[i].car.eg_mode != FINISH))
            {
                inst += calc_slot_car(&p->ch[i].car, calc_slot_mod(&p->ch[i].mod));
            }
        }
    }
    else
    {
        p->ch[7].mod.pgout = calc_phase(&p->ch[7].mod);
        p->ch[8].car.pgout = calc_phase(&p->ch[8].car);
        if (p->ch[7].mod.phase < 256)
        {
            rythmH = DB_NEG(12.0);
        }
        else
        {
            rythmH = DB_MUTE - 1;
        }
        if (p->ch[8].car.phase < 256)
        {
            rythmC = DB_NEG(12.0);
        }
        else
        {
            rythmC = DB_MUTE - 1;
        }

        if (!(p->mask&MM_EMU_OPLL_MASK_BD) && (p->ch[6].car.eg_mode != FINISH))
        {
            perc += calc_slot_car(&p->ch[6].car, calc_slot_mod(&p->ch[6].mod));
        }

        if (!(p->mask&MM_EMU_OPLL_MASK_HH) && (p->ch[7].mod.eg_mode != FINISH))
        {
            perc += calc_slot_hat(&p->ch[7].mod, p->noiseA, p->noiseB, rythmH, p->whitenoise);
        }

        if (!(p->mask&MM_EMU_OPLL_MASK_SD) && (p->ch[7].car.eg_mode != FINISH))
        {
            perc += calc_slot_snare(&p->ch[7].car, p->whitenoise);
        }

        if (!(p->mask&MM_EMU_OPLL_MASK_TOM) && (p->ch[8].mod.eg_mode != FINISH))
        {
            perc += calc_slot_tom(&p->ch[8].mod);
        }

        if (!(p->mask&MM_EMU_OPLL_MASK_CYM) && (p->ch[8].car.eg_mode != FINISH))
        {
            perc += calc_slot_cym(&p->ch[8].car, p->noiseA, p->noiseB, rythmC);
        }
    }

#if SLOT_AMP_BITS > 8
    inst = (inst >> (SLOT_AMP_BITS - 8));
    perc = (perc >> (SLOT_AMP_BITS - 9));
#else
    inst = (inst << (8 - SLOT_AMP_BITS));
    perc = (perc << (9 - SLOT_AMP_BITS));
#endif

    out = ((inst + perc) * p->masterVolume) >> 2;

    if (out > 32767)
    {
        return 32767;
    }
    if (out < -32768)
    {
        return -32768;
    }

    return (mmSInt16_t)out;
}
MM_EXPORT_EMU void mmEmuOpll_CopyPatch(struct mmEmuOpll* p, int num, struct mmEmuOpllPatch *patch)
{
    mmMemcpy(&p->patch[num], patch, sizeof(struct mmEmuOpllPatch));
}
/* Force Refresh (When external program changes some parameters). */
MM_EXPORT_EMU void mmEmuOpll_ForceRefresh(struct mmEmuOpll* p)
{
    int i;

    for (i = 0; i < 18; i++)
    {
        UPDATE_PG(p, p->slot[i]);
        UPDATE_RKS(p, p->slot[i]);
        UPDATE_TLL(p, p->slot[i]);
        UPDATE_WF(p, p->slot[i]);
        UPDATE_EG(p, p->slot[i]);
    }
}

MM_EXPORT_EMU mmUInt32_t mmEmuOpll_SetMask(struct mmEmuOpll* p, mmUInt32_t mask)
{
    mmUInt32_t ret;

    assert(NULL != p && "NULL != p is invalid.");

    ret = p->mask;
    p->mask = mask;
    return ret;
}

MM_EXPORT_EMU mmUInt32_t mmEmuOpll_ToggleMask(struct mmEmuOpll* p, mmUInt32_t mask)
{
    mmUInt32_t ret;

    assert(NULL != p && "NULL != p is invalid.");

    ret = p->mask;
    p->mask ^= mask;
    return ret;
}

MM_EXPORT_EMU void mmEmuOpll_MakeTables(void)
{
    if (MM_FALSE == tables_is_make)
    {
        tables_is_make = MM_TRUE;

        makePmTable();
        makeAmTable();
        makeDB2LinTable();
        makeAdjustTable();
        makeTllTable();
        makeRksTable();
        makeSinTable();
        makeDefaultPatch();
    }
}

/***************************************************

                  Create tables

****************************************************/
static MM_INLINE mmSInt32_t Min(mmSInt32_t i, mmSInt32_t j)
{
    if (i < j) return i; else return j;
}

/* Table for AR to LogCurve. */
static void makeAdjustTable(void)
{
    int i;

    AR_ADJUST_TABLE[0] = (1 << EG_BITS);
    for (i = 1; i < 128; i++)
    {
        AR_ADJUST_TABLE[i] = (mmUInt32_t)((double)(1 << EG_BITS) - 1 - (1 << EG_BITS) * log(i) / log(128));
    }
}


/* Table for dB(0 -- (1<<DB_BITS)) to Liner(0 -- DB2LIN_AMP_WIDTH) */
static void makeDB2LinTable(void)
{
    int i;

    for (i = 0; i < DB_MUTE + DB_MUTE; i++)
    {
        DB2LIN_TABLE[i] = (mmSInt32_t)((double)((1 << DB2LIN_AMP_BITS) - 1) * pow(10, -(double)i*DB_STEP / 20));
        if (i >= DB_MUTE)
        {
            DB2LIN_TABLE[i] = 0;
        }
        DB2LIN_TABLE[i + DB_MUTE + DB_MUTE] = -DB2LIN_TABLE[i];
    }
}

/* Liner(+0.0 - +1.0) to dB((1<<DB_BITS) - 1 -- 0) */
static mmSInt32_t lin2db(double d)
{
    if (d == 0)
    {
        return (DB_MUTE - 1);
    }
    else
    {
        return Min(-(mmSInt32_t)(20.0*log10(d) / DB_STEP), DB_MUTE - 1); /* 0 -- 128 */
    }
}

/* Sin Table */
static void makeSinTable(void)
{
    int i;

    for (i = 0; i < PG_WIDTH / 4; i++)
    {
        fullsintable[i] = lin2db(sin(2.0*MM_PI*i / PG_WIDTH));
        snaretable[i] = (mmSInt32_t)((6.0) / DB_STEP);
    }

    for (i = 0; i < PG_WIDTH / 4; i++)
    {
        fullsintable[PG_WIDTH / 2 - 1 - i] = fullsintable[i];
        snaretable[PG_WIDTH / 2 - 1 - i] = snaretable[i];
    }

    for (i = 0; i < PG_WIDTH / 2; i++)
    {
        fullsintable[PG_WIDTH / 2 + i] = DB_MUTE + DB_MUTE + fullsintable[i];
        snaretable[PG_WIDTH / 2 + i] = DB_MUTE + DB_MUTE + snaretable[i];
    }

    for (i = 0; i < PG_WIDTH / 2; i++)
    {
        halfsintable[i] = fullsintable[i];
    }
    for (i = PG_WIDTH / 2; i < PG_WIDTH; i++)
    {
        halfsintable[i] = fullsintable[0];
    }

    for (i = 0; i < 64; i++)
    {
        if (noiseAtable[i] > 0)
        {
            noiseAtable[i] = DB_POS(0);
        }
        else if (noiseAtable[i] < 0)
        {
            noiseAtable[i] = DB_NEG(0);
        }
        else
        {
            noiseAtable[i] = DB_MUTE - 1;
        }
    }

    for (i = 0; i < 8; i++)
    {
        if (noiseBtable[i] > 0)
        {
            noiseBtable[i] = DB_POS(0);
        }
        else if (noiseBtable[i] < 0)
        {
            noiseBtable[i] = DB_NEG(0);
        }
        else
        {
            noiseBtable[i] = DB_MUTE - 1;
        }
    }
}

/* Table for Pitch Modulator */
static void makePmTable(void)
{
    int i;

    for (i = 0; i < PM_PG_WIDTH; i++)
    {
        pmtable[i] = (mmSInt32_t)((double)PM_AMP * pow(2, (double)PM_DEPTH*sin(2.0*MM_PI*i / PM_PG_WIDTH) / 1200));
    }
}

/* Table for Amp Modulator */
static void makeAmTable(void)
{
    int i;

    for (i = 0; i < AM_PG_WIDTH; i++)
    {
        amtable[i] = (mmSInt32_t)((double)AM_DEPTH / 2 / DB_STEP * (1.0 + sin(2.0*MM_PI*i / PM_PG_WIDTH)));
    }
}

/* Phase increment counter table */
static void makeDphaseTable(struct mmEmuOpll* p)
{
    mmUInt32_t fnum, block, ML;
    // index = DPHASETABLE_L0 * L0 + DPHASETABLE_L1 * L1 + DPHASETABLE_L2 * L2
    size_t index = 0;

    static const mmUInt32_t mltable[16] =
    {
             1,  1 * 2,  2 * 2,  3 * 2,
         4 * 2,  5 * 2,  6 * 2,  7 * 2,
         8 * 2,  9 * 2, 10 * 2, 10 * 2,
        12 * 2, 12 * 2, 15 * 2, 15 * 2,
    };

    for (fnum = 0; fnum < 512; fnum++)
    {
        for (block = 0; block < 8; block++)
        {
            for (ML = 0; ML < 16; ML++)
            {
                // p->dphaseTable[fnum][block][ML] = rate_adjust(((fnum * mltable[ML]) << block) >> (20 - DP_BITS));
                index = DPHASETABLE_L0 * fnum + DPHASETABLE_L1 * block + ML;
                p->dphaseTable[index] = rate_adjust(((fnum * mltable[ML]) << block) >> (20 - DP_BITS));
            }
        }
    }
}

static void makeTllTable(void)
{
#define dB2(x) (mmUInt32_t)((x)*2)

    static const mmUInt32_t kltable[16] =
    {
        dB2(0.000),dB2(9.000),dB2(12.000),dB2(13.875),dB2(15.000),dB2(16.125),dB2(16.875),dB2(17.625),
        dB2(18.000),dB2(18.750),dB2(19.125),dB2(19.500),dB2(19.875),dB2(20.250),dB2(20.625),dB2(21.000),
    };

    mmSInt32_t tmp;
    int fnum, block, TL, KL;

    for (fnum = 0; fnum < 16; fnum++)
    {
        for (block = 0; block < 8; block++)
        {
            for (TL = 0; TL < 64; TL++)
            {
                for (KL = 0; KL < 4; KL++)
                {
                    if (KL == 0)
                    {
                        tllTable[fnum][block][TL][KL] = TL2EG(TL);
                    }
                    else
                    {
                        tmp = kltable[fnum] - dB2(3.000) * (7 - block);
                        if (tmp <= 0)
                        {
                            tllTable[fnum][block][TL][KL] = TL2EG(TL);
                        }
                        else
                        {
                            tllTable[fnum][block][TL][KL] = (mmUInt32_t)((tmp >> (3 - KL)) / EG_STEP) + TL2EG(TL);
                        }
                    }
                }
            }
        }
    }

#undef dB2

}

/* Rate Table for Attack */
static void makeDphaseARTable(struct mmEmuOpll* p)
{
    int AR, Rks, RM, RL;
    // index = DPHASEARTABLE_L0 * L0 + L1;
    size_t index = 0;

    for (AR = 0; AR < 16; AR++)
    {
        for (Rks = 0; Rks < 16; Rks++)
        {
            RM = AR + (Rks >> 2);
            if (RM > 15) RM = 15;
            RL = Rks & 3;
            switch (AR)
            {
            case 0:
                // p->dphaseARTable[AR][Rks] = 0;
                index = DPHASEARTABLE_L0 * AR + Rks;
                p->dphaseARTable[index] = 0;
                break;
            case 15:
                // p->dphaseARTable[AR][Rks] = EG_DP_WIDTH;
                index = DPHASEARTABLE_L0 * AR + Rks;
                p->dphaseARTable[index] = EG_DP_WIDTH;
                break;
            default:
                // p->dphaseARTable[AR][Rks] = rate_adjust((3 * (RL + 4) << (RM + 1)));
                index = DPHASEARTABLE_L0 * AR + Rks;
                p->dphaseARTable[index] = rate_adjust((3 * (RL + 4) << (RM + 1)));
                break;
            }
        }
    }
}

/* Rate Table for Decay */
static void makeDphaseDRTable(struct mmEmuOpll* p)
{
    int DR, Rks, RM, RL;
    // index = DPHASEDRTABLE_L0 * L0 + L1;
    size_t index = 0;

    for (DR = 0; DR < 16; DR++)
    {
        for (Rks = 0; Rks < 16; Rks++)
        {
            RM = DR + (Rks >> 2);
            RL = Rks & 3;
            if (RM > 15) RM = 15;
            switch (DR)
            {
            case 0:
                // p->dphaseDRTable[DR][Rks] = 0;
                index = DPHASEDRTABLE_L0 * DR + Rks;
                p->dphaseDRTable[index] = 0;
                break;
            default:
                // p->dphaseDRTable[DR][Rks] = rate_adjust((RL + 4) << (RM - 1));
                index = DPHASEDRTABLE_L0 * DR + Rks;
                p->dphaseDRTable[index] = rate_adjust((RL + 4) << (RM - 1));
                break;
            }
        }
    }
}

static void makeRksTable(void)
{

    int fnum8, block, KR;

    for (fnum8 = 0; fnum8 < 2; fnum8++)
    {
        for (block = 0; block < 8; block++)
        {
            for (KR = 0; KR < 2; KR++)
            {
                if (KR != 0)
                {
                    rksTable[fnum8][block][KR] = (block << 1) + fnum8;
                }
                else
                {
                    rksTable[fnum8][block][KR] = block >> 1;
                }
            }
        }
    }
}

static void makeDefaultPatch(void)
{
    int i, j;

    for (i = 0; i < OPLL_TONE_NUM; i++)
    {
        for (j = 0; j < 19; j++)
        {
            mmEmuOpllPatch_FromDump(&default_patch[i][j * 2], default_inst[i] + j * 16);
        }
    }
}

/************************************************************

                      Calc Parameters

************************************************************/
static MM_INLINE mmUInt32_t calc_eg_dphase(struct mmEmuOpllSlot* slot)
{
    switch (slot->eg_mode)
    {
    case ATTACK:
        // return slot->opll->dphaseARTable[slot->patch->AR][slot->rks];
        return slot->opll->dphaseARTable[DPHASEARTABLE_L0 * slot->patch->AR + slot->rks];

    case DECAY:
        // return slot->opll->dphaseDRTable[slot->patch->DR][slot->rks];
        return slot->opll->dphaseDRTable[DPHASEDRTABLE_L0 * slot->patch->DR + slot->rks];

    case SUSHOLD:
        return 0;

    case SUSTINE:
        // return slot->opll->dphaseDRTable[slot->patch->RR][slot->rks];
        return slot->opll->dphaseDRTable[DPHASEDRTABLE_L0 * slot->patch->RR + slot->rks];

    case RELEASE:
        if (slot->sustine)
        {
            // return slot->opll->dphaseDRTable[5][slot->rks];
            return slot->opll->dphaseDRTable[DPHASEDRTABLE_L0 * 5 + slot->rks];
        }
        else if (slot->patch->EG)
        {
            // return slot->opll->dphaseDRTable[slot->patch->RR][slot->rks];
            return slot->opll->dphaseDRTable[DPHASEDRTABLE_L0 * slot->patch->RR + slot->rks];
        }
        else
        {
            // return slot->opll->dphaseDRTable[7][slot->rks];
            return slot->opll->dphaseDRTable[DPHASEDRTABLE_L0 * 7 + slot->rks];
        }

    case FINISH:
        return 0;

    default:
        return 0;
    }
}

/* Slot key on  */
static MM_INLINE void slotOn(struct mmEmuOpllSlot* slot)
{
    slot->eg_mode = ATTACK;
    slot->phase = 0;
    slot->eg_phase = 0;
}

/* Slot key off */
static MM_INLINE void slotOff(struct mmEmuOpllSlot* slot)
{
    if (slot->eg_mode == ATTACK)
    {
        slot->eg_phase = EXPAND_BITS(AR_ADJUST_TABLE[HIGHBITS(slot->eg_phase, EG_DP_BITS - EG_BITS)], EG_BITS, EG_DP_BITS);
    }
    slot->eg_mode = RELEASE;
}

/* Channel key on */
static MM_INLINE void keyOn(struct mmEmuOpll *opll, int i)
{
    if (!opll->slot_on_flag[i * 2])
    {
        slotOn(&opll->ch[i].mod);
    }
    if (!opll->slot_on_flag[i * 2 + 1])
    {
        slotOn(&opll->ch[i].car);
    }
    opll->ch[i].key_status = 1;
}

/* Channel key off */
static MM_INLINE void keyOff(struct mmEmuOpll *opll, int i)
{
    if (opll->slot_on_flag[i * 2 + 1])
    {
        slotOff(&opll->ch[i].car);
    }
    opll->ch[i].key_status = 0;
}

static MM_INLINE void keyOn_BD(struct mmEmuOpll *opll) { keyOn(opll, 6); }
static MM_INLINE void keyOn_SD(struct mmEmuOpll *opll) { if (!opll->slot_on_flag[SLOT_SD]) slotOn(&opll->ch[7].car); }
static MM_INLINE void keyOn_TOM(struct mmEmuOpll *opll) { if (!opll->slot_on_flag[SLOT_TOM]) slotOn(&opll->ch[8].mod); }
static MM_INLINE void keyOn_HH(struct mmEmuOpll *opll) { if (!opll->slot_on_flag[SLOT_HH]) slotOn(&opll->ch[7].mod); }
static MM_INLINE void keyOn_CYM(struct mmEmuOpll *opll) { if (!opll->slot_on_flag[SLOT_CYM]) slotOn(&opll->ch[8].car); }

/* Drum key off */
static MM_INLINE void keyOff_BD(struct mmEmuOpll *opll) { keyOff(opll, 6); }
static MM_INLINE void keyOff_SD(struct mmEmuOpll *opll) { if (opll->slot_on_flag[SLOT_SD]) slotOff(&opll->ch[7].car); }
static MM_INLINE void keyOff_TOM(struct mmEmuOpll *opll) { if (opll->slot_on_flag[SLOT_TOM]) slotOff(&opll->ch[8].mod); }
static MM_INLINE void keyOff_HH(struct mmEmuOpll *opll) { if (opll->slot_on_flag[SLOT_HH]) slotOff(&opll->ch[7].mod); }
static MM_INLINE void keyOff_CYM(struct mmEmuOpll *opll) { if (opll->slot_on_flag[SLOT_CYM]) slotOff(&opll->ch[8].car); }

/* Change a voice */
static MM_INLINE void setPatch(struct mmEmuOpll *opll, int i, int num)
{
    opll->ch[i].patch_number = num;

    opll->ch[i].mod.patch = &opll->patch[num * 2 + 0];
    opll->ch[i].car.patch = &opll->patch[num * 2 + 1];
}

/* Set sustine parameter */
static MM_INLINE void setSustine(struct mmEmuOpll *opll, int c, int sustine)
{
    opll->ch[c].car.sustine = sustine;
    if (opll->ch[c].mod.type)
    {
        opll->ch[c].mod.sustine = sustine;
    }
}

/* Volume : 6bit ( Volume register << 2 ) */
static MM_INLINE void setVolume(struct mmEmuOpll *opll, int c, int volume)
{
    opll->ch[c].car.volume = volume;
}

/* Set F-Number ( fnum : 9bit ) */
static MM_INLINE void setFnumber(struct mmEmuOpll *opll, int c, int fnum)
{
    opll->ch[c].car.fnum = fnum;
    opll->ch[c].mod.fnum = fnum;
}

/* Set Block data (block : 3bit ) */
static MM_INLINE void setBlock(struct mmEmuOpll *opll, int c, int block)
{
    opll->ch[c].car.block = block;
    opll->ch[c].mod.block = block;
}

/* Change Rythm Mode */
static MM_INLINE void setRythmMode(struct mmEmuOpll *opll, int mode)
{
    opll->rythm_mode = mode;

    if (mode)
    {
        opll->ch[6].patch_number = 16;
        opll->ch[7].patch_number = 17;
        opll->ch[8].patch_number = 18;
        mmEmuOpllSlot_SetPatch(opll->slot[SLOT_BD1], &opll->patch[16 * 2 + 0]);
        mmEmuOpllSlot_SetPatch(opll->slot[SLOT_BD2], &opll->patch[16 * 2 + 1]);
        mmEmuOpllSlot_SetPatch(opll->slot[SLOT_HH], &opll->patch[17 * 2 + 0]);
        mmEmuOpllSlot_SetPatch(opll->slot[SLOT_SD], &opll->patch[17 * 2 + 1]);
        opll->slot[SLOT_HH]->type = 1;
        mmEmuOpllSlot_SetPatch(opll->slot[SLOT_TOM], &opll->patch[18 * 2 + 0]);
        mmEmuOpllSlot_SetPatch(opll->slot[SLOT_CYM], &opll->patch[18 * 2 + 1]);
        opll->slot[SLOT_TOM]->type = 1;
    }
    else
    {
        setPatch(opll, 6, opll->reg[0x36] >> 4);
        setPatch(opll, 7, opll->reg[0x37] >> 4);
        opll->slot[SLOT_HH]->type = 0;
        setPatch(opll, 8, opll->reg[0x38] >> 4);
        opll->slot[SLOT_TOM]->type = 0;
    }

    if (!opll->slot_on_flag[SLOT_BD1]) opll->slot[SLOT_BD1]->eg_mode = FINISH;
    if (!opll->slot_on_flag[SLOT_BD2]) opll->slot[SLOT_BD2]->eg_mode = FINISH;
    if (!opll->slot_on_flag[SLOT_HH]) opll->slot[SLOT_HH]->eg_mode = FINISH;
    if (!opll->slot_on_flag[SLOT_SD]) opll->slot[SLOT_SD]->eg_mode = FINISH;
    if (!opll->slot_on_flag[SLOT_TOM]) opll->slot[SLOT_TOM]->eg_mode = FINISH;
    if (!opll->slot_on_flag[SLOT_CYM]) opll->slot[SLOT_CYM]->eg_mode = FINISH;
}
/*********************************************************

                 Generate wave data

*********************************************************/
/* Convert Amp(0 to EG_HEIGHT) to Phase(0 to 2PI). */
#if ( SLOT_AMP_BITS - PG_BITS ) > 0
#define wave2_2pi(e)  ( (e) >> ( SLOT_AMP_BITS - PG_BITS ))
#else
#define wave2_2pi(e)  ( (e) << ( PG_BITS - SLOT_AMP_BITS ))
#endif

/* Convert Amp(0 to EG_HEIGHT) to Phase(0 to 4PI). */
#if ( SLOT_AMP_BITS - PG_BITS - 1 ) == 0
#define wave2_4pi(e)  (e)
#elif ( SLOT_AMP_BITS - PG_BITS - 1 ) > 0
#define wave2_4pi(e)  ( (e) >> ( SLOT_AMP_BITS - PG_BITS - 1 ))
#else
#define wave2_4pi(e)  ( (e) << ( 1 + PG_BITS - SLOT_AMP_BITS ))
#endif

/* Convert Amp(0 to EG_HEIGHT) to Phase(0 to 8PI). */
#if ( SLOT_AMP_BITS - PG_BITS - 2 ) == 0
#define wave2_8pi(e)  (e)
#elif ( SLOT_AMP_BITS - PG_BITS - 2 ) > 0
#define wave2_8pi(e)  ( (e) >> ( SLOT_AMP_BITS - PG_BITS - 2 ))
#else
#define wave2_8pi(e)  ( (e) << ( 2 + PG_BITS - SLOT_AMP_BITS ))
#endif

/* 16bit rand */
static MM_INLINE mmUInt32_t mrand(mmUInt32_t seed)
{
    return ((seed >> 15) ^ ((seed >> 12) & 1)) | ((seed << 1) & 0xffff);
}

//static MM_INLINE mmUInt32_t DEC(mmUInt32_t db)
//{
//    if (db < DB_MUTE + DB_MUTE)
//    {
//        return Min(db + DB_POS(0.375 * 2), DB_MUTE - 1);
//    }
//    else
//    {
//        return Min(db + DB_POS(0.375 * 2), DB_MUTE + DB_MUTE + DB_MUTE - 1);
//    }
//}

/* Update Noise unit */
static MM_INLINE void update_noise(struct mmEmuOpll *opll)
{
    opll->noise_seed = mrand(opll->noise_seed);
    opll->whitenoise = opll->noise_seed & 1;

    opll->noiseA_phase = (opll->noiseA_phase + opll->noiseA_dphase);
    opll->noiseB_phase = (opll->noiseB_phase + opll->noiseB_dphase);

    if (opll->noiseA_phase < (1 << 11))
    {
        if (opll->noiseA_phase > 16) opll->noiseA = DB_MUTE - 1;
    }
    else
    {
        opll->noiseA_phase &= (1 << 11) - 1;
        opll->noiseA_idx = (opll->noiseA_idx + 1) & 63;
        opll->noiseA = noiseAtable[opll->noiseA_idx];
    }

    if (opll->noiseB_phase < (1 << 12))
    {
        if (opll->noiseB_phase > 16) opll->noiseB = DB_MUTE - 1;
    }
    else
    {
        opll->noiseB_phase &= (1 << 12) - 1;
        opll->noiseB_idx = (opll->noiseB_idx + 1) & 7;
        opll->noiseB = noiseBtable[opll->noiseB_idx];
    }
}

/* Update AM, PM unit */
static MM_INLINE void update_ampm(struct mmEmuOpll *opll)
{
    opll->pm_phase = (opll->pm_phase + opll->pm_dphase)&(PM_DP_WIDTH - 1);
    opll->am_phase = (opll->am_phase + opll->am_dphase)&(AM_DP_WIDTH - 1);
    opll->lfo_am = amtable[HIGHBITS(opll->am_phase, AM_DP_BITS - AM_PG_BITS)];
    opll->lfo_pm = pmtable[HIGHBITS(opll->pm_phase, PM_DP_BITS - PM_PG_BITS)];
}

/* PG */
static MM_INLINE mmUInt32_t calc_phase(struct mmEmuOpllSlot *slot)
{
    if (slot->patch->PM)
    {
        slot->phase += (slot->dphase * (slot->opll->lfo_pm)) >> PM_AMP_BITS;
    }
    else
    {
        slot->phase += slot->dphase;
    }

    slot->phase &= (DP_WIDTH - 1);

    return HIGHBITS(slot->phase, DP_BASE_BITS);
}

/* EG */
static MM_INLINE mmUInt32_t calc_envelope(struct mmEmuOpllSlot *slot)
{
#define S2E(x) (SL2EG((int)(x/SL_STEP))<<(EG_DP_BITS-EG_BITS)) 

    static const mmUInt32_t SL[16] =
    {
        S2E(0), S2E(3), S2E(6), S2E(9), S2E(12), S2E(15), S2E(18), S2E(21),
        S2E(24), S2E(27), S2E(30), S2E(33), S2E(36), S2E(39), S2E(42), S2E(48),
    };

#undef S2E

    struct mmEmuOpll* p = slot->opll;

    mmUInt32_t egout;

    switch (slot->eg_mode)
    {
    case ATTACK:
        slot->eg_phase += slot->eg_dphase;
        if (EG_DP_WIDTH & slot->eg_phase)
        {
            egout = 0;
            slot->eg_phase = 0;
            slot->eg_mode = DECAY;
            UPDATE_EG(p, slot);
        }
        else
        {
            egout = AR_ADJUST_TABLE[HIGHBITS(slot->eg_phase, EG_DP_BITS - EG_BITS)];
        }
        break;

    case DECAY:
        slot->eg_phase += slot->eg_dphase;
        egout = HIGHBITS(slot->eg_phase, EG_DP_BITS - EG_BITS);
        if (slot->eg_phase >= SL[slot->patch->SL])
        {
            if (slot->patch->EG)
            {
                slot->eg_phase = SL[slot->patch->SL];
                slot->eg_mode = SUSHOLD;
                UPDATE_EG(p, slot);
            }
            else
            {
                slot->eg_phase = SL[slot->patch->SL];
                slot->eg_mode = SUSTINE;
                UPDATE_EG(p, slot);
            }
            egout = HIGHBITS(slot->eg_phase, EG_DP_BITS - EG_BITS);
        }
        break;

    case SUSHOLD:
        egout = HIGHBITS(slot->eg_phase, EG_DP_BITS - EG_BITS);
        if (slot->patch->EG == 0)
        {
            slot->eg_mode = SUSTINE;
            UPDATE_EG(p, slot);
        }
        break;

    case SUSTINE:
    case RELEASE:
        slot->eg_phase += slot->eg_dphase;
        egout = HIGHBITS(slot->eg_phase, EG_DP_BITS - EG_BITS);
        if (egout >= (1 << EG_BITS))
        {
            slot->eg_mode = FINISH;
            egout = (1 << EG_BITS) - 1;
        }
        break;

    case FINISH:
        egout = (1 << EG_BITS) - 1;
        break;

    default:
        egout = (1 << EG_BITS) - 1;
        break;
    }

    if (slot->patch->AM)
    {
        egout = EG2DB(egout + slot->tll) + slot->opll->lfo_am;
    }
    else
    {
        egout = EG2DB(egout + slot->tll);
    }

    if (egout >= DB_MUTE)
    {
        egout = DB_MUTE - 1;
    }
    return egout;
}

static MM_INLINE mmSInt32_t calc_slot_car(struct mmEmuOpllSlot *slot, mmSInt32_t fm)
{
    slot->egout = calc_envelope(slot);
    slot->pgout = calc_phase(slot);
    if (slot->egout >= (DB_MUTE - 1))
    {
        return 0;
    }

    return DB2LIN_TABLE[slot->sintbl[(slot->pgout + wave2_8pi(fm))&(PG_WIDTH - 1)] + slot->egout];
}


static MM_INLINE mmSInt32_t calc_slot_mod(struct mmEmuOpllSlot *slot)
{
    mmSInt32_t fm;

    slot->output[1] = slot->output[0];
    slot->egout = calc_envelope(slot);
    slot->pgout = calc_phase(slot);

    if (slot->egout >= (DB_MUTE - 1))
    {
        slot->output[0] = 0;
    }
    else if (slot->patch->FB != 0)
    {
        fm = wave2_4pi(slot->feedback) >> (7 - slot->patch->FB);
        slot->output[0] = DB2LIN_TABLE[slot->sintbl[(slot->pgout + fm)&(PG_WIDTH - 1)] + slot->egout];
    }
    else
    {
        slot->output[0] = DB2LIN_TABLE[slot->sintbl[slot->pgout] + slot->egout];
    }

    slot->feedback = (slot->output[1] + slot->output[0]) >> 1;

    return slot->feedback;
}

static MM_INLINE mmSInt32_t calc_slot_tom(struct mmEmuOpllSlot *slot)
{
    slot->egout = calc_envelope(slot);
    slot->pgout = calc_phase(slot);
    if (slot->egout >= (DB_MUTE - 1))
    {
        return 0;
    }

    return DB2LIN_TABLE[slot->sintbl[slot->pgout] + slot->egout];
}

/* calc SNARE slot */
static MM_INLINE mmSInt32_t calc_slot_snare(struct mmEmuOpllSlot *slot, mmUInt32_t whitenoise)
{
    slot->egout = calc_envelope(slot);
    slot->pgout = calc_phase(slot);
    if (slot->egout >= (DB_MUTE - 1))
    {
        return 0;
    }

    if (whitenoise)
    {
        return DB2LIN_TABLE[snaretable[slot->pgout] + slot->egout] + DB2LIN_TABLE[slot->egout + 6];
    }
    else
    {
        return DB2LIN_TABLE[snaretable[slot->pgout] + slot->egout];
    }
}

static MM_INLINE mmSInt32_t calc_slot_cym(struct mmEmuOpllSlot *slot, mmSInt32_t a, mmSInt32_t b, mmSInt32_t c)
{
    slot->egout = calc_envelope(slot);
    if (slot->egout >= (DB_MUTE - 1))
    {
        return 0;
    }

    return DB2LIN_TABLE[slot->egout + a] + ((DB2LIN_TABLE[slot->egout + b] + DB2LIN_TABLE[slot->egout + c]) >> 2);
}

static MM_INLINE mmSInt32_t calc_slot_hat(struct mmEmuOpllSlot *slot, mmSInt32_t a, mmSInt32_t b, mmSInt32_t c, mmUInt32_t whitenoise)
{
    slot->egout = calc_envelope(slot);
    if (slot->egout >= (DB_MUTE - 1))
    {
        return 0;
    }

    if (whitenoise)
    {
        return DB2LIN_TABLE[slot->egout + a] + ((DB2LIN_TABLE[slot->egout + b] + DB2LIN_TABLE[slot->egout + c]) >> 2);
    }
    else
    {
        return 0;
    }
}

static mmInline void UPDATE_PG(struct mmEmuOpll* p, struct mmEmuOpllSlot* S)
{
    // S->dphase = p->dphaseTable[S->fnum][S->block][S->patch->ML];
    S->dphase = p->dphaseTable[DPHASETABLE_L0 * S->fnum + DPHASETABLE_L1 * S->block + S->patch->ML];
}
static mmInline void UPDATE_TLL(struct mmEmuOpll* p, struct mmEmuOpllSlot* S)
{
    ((S->type == 0) ?
        (S->tll = tllTable[(S->fnum) >> 5][S->block][S->patch->TL][S->patch->KL]) :
        (S->tll = tllTable[(S->fnum) >> 5][S->block][S->volume][S->patch->KL]));
}
static mmInline void UPDATE_RKS(struct mmEmuOpll* p, struct mmEmuOpllSlot* S)
{
    S->rks = rksTable[(S->fnum) >> 8][S->block][S->patch->KR];
}
static mmInline void UPDATE_WF(struct mmEmuOpll* p, struct mmEmuOpllSlot* S)
{
    S->sintbl = waveform[S->patch->WF];
}
static mmInline void UPDATE_EG(struct mmEmuOpll* p, struct mmEmuOpllSlot* S)
{
    S->eg_dphase = calc_eg_dphase(S);
}
static void UPDATE_ALL(struct mmEmuOpll* p, struct mmEmuOpllSlot* S)
{
    UPDATE_PG(p, S);
    UPDATE_TLL(p, S);
    UPDATE_RKS(p, S);
    UPDATE_WF(p, S);
    UPDATE_EG(p, S);
}
