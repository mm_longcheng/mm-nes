/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper133.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper133_Init(struct mmEmuMapper133* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper133_Reset;
    //p->super.Write = &mmEmuMapper133_Write;
    //p->super.Read = &mmEmuMapper133_Read;
    //p->super.ReadLow = &mmEmuMapper133_ReadLow;
    p->super.WriteLow = &mmEmuMapper133_WriteLow;
    //p->super.ExRead = &mmEmuMapper133_ExRead;
    //p->super.ExWrite = &mmEmuMapper133_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper133_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper133_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper133_HSync;
    //p->super.VSync = &mmEmuMapper133_VSync;
    //p->super.Clock = &mmEmuMapper133_Clock;
    //p->super.PPULatch = &mmEmuMapper133_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper133_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper133_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper133_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper133_IsStateSave;
    //p->super.SaveState = &mmEmuMapper133_SaveState;
    //p->super.LoadState = &mmEmuMapper133_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper133_Destroy(struct mmEmuMapper133* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper133_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper133* p = (struct mmEmuMapper133*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper133_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper133* p = (struct mmEmuMapper133*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0x4120)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (data & 0x04) >> 2);
        mmEmuMmu_SetVROM8KBank(mmu, data & 0x03);
    }
    mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
}

static struct mmEmuMapper* __static_mmEmuMapper133_Produce(void)
{
    struct mmEmuMapper133* p = (struct mmEmuMapper133*)mmMalloc(sizeof(struct mmEmuMapper133));
    mmEmuMapper133_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper133_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper133* p = (struct mmEmuMapper133*)(m);
    mmEmuMapper133_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER133 =
{
    &__static_mmEmuMapper133_Produce,
    &__static_mmEmuMapper133_Recycle,
};
