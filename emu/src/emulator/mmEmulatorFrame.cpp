/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmulatorFrame.h"

#include "core/mmAlloc.h"
#include "core/mmBit.h"
#include "core/mmSpinlock.h"
#include "core/mmInteger.h"

#include "emu/mmEmuScreen.h"

#include "OgreTextureManager.h"
#include "OgreRenderTarget.h"
#include "OgreRenderTexture.h"

#include "CEGUIOgreRenderer/Texture.h"
#include "CEGUIOgreRenderer/GeometryBuffer.h"

#include "CEGUI/ImageManager.h"

MM_EXPORT_EMULATOR const char* MM_EMULATOR_FRAME_OGRE_TEXTURE = "mmEmulatorFrame_OGRETexture_";
MM_EXPORT_EMULATOR const char* MM_EMULATOR_FRAME_CEGUI_TEXTURE = "mmEmulatorFrame_CEGUITexture_";
MM_EXPORT_EMULATOR const char* MM_EMULATOR_FRAME_CEGUI_IMAGERY = "mmEmulatorFrame_CEGUIImagery_";

MM_EXPORT_EMULATOR void mmEmulatorFrame_Init(struct mmEmulatorFrame* p)
{
    p->hScreenBuffSize = sizeof(mmByte_t) * MM_EMU_SCREEN_W * MM_EMU_SCREEN_H;
    p->hScreenRealSize = p->hScreenBuffSize;
    mmRoundup32(p->hScreenRealSize);
    p->pScreenBufferPtr = (mmByte_t*)mmMalloc(p->hScreenRealSize * 2);
    p->pScreenPtr[0] = p->pScreenBufferPtr;
    p->pScreenPtr[1] = p->pScreenBufferPtr + p->hScreenRealSize;
    //
    p->pContextMaster = NULL;

    // c++ struct can not internal structure life cycle manual, do it external.
    // new (&p->hName) std::string();
    // new (&p->hMaterialPathPrefix) std::string();
    // new (&p->hMaterialName) std::string();
    // new (&p->hNameOgreTextureEmu0) std::string();
    // new (&p->hNameOgreTextureEmu1) std::string();
    // new (&p->hNameCEGUITextureEmu) std::string();
    // new (&p->hNameCEGUIImageryEmu) std::string();

    p->hOgreTexture0.reset();
    p->hOgreTexture1.reset();

    p->pCEGUITexture = NULL;
    p->pCEGUIImagery = NULL;

    p->hHardwarePixelBuffer0.reset();
    p->hHardwarePixelBuffer1.reset();

    p->pRenderTarget0 = NULL;
    p->pRenderTarget1 = NULL;

    p->hMaterialPtr.reset();

    p->hSrcBox = Ogre::PixelBox();
    p->hDstBox = Ogre::Box();

    p->pPaletteRGB = NULL;

    p->hTextureW = MM_EMU_SCREEN_W;
    p->hTextureH = MM_EMU_SCREEN_H;

    mmSpinlock_Init(&p->hLockerScreenPtr0, NULL);
    mmSpinlock_Init(&p->hLockerScreenPtr1, NULL);
    // 0x3F is black color index.
    mmMemset(p->pScreenBufferPtr, 0x3F, p->hScreenRealSize * 2);

    p->hName = "";
    p->hMaterialPathPrefix = "emulator";
    p->hMaterialName = "EMU/GeometryBuffer/emulator_" + p->hName;

    p->hNameOgreTextureEmu0 = "";
    p->hNameOgreTextureEmu1 = "";
    p->hNameCEGUITextureEmu = "";
    p->hNameCEGUIImageryEmu = "";

    //
    mmEmulatorFrame_SetName(p, "mm");
    //
    mmRoundup32(p->hTextureW);
    mmRoundup32(p->hTextureH);
    //
    p->hSrcBox = Ogre::PixelBox(MM_EMU_SCREEN_W / 4, MM_EMU_SCREEN_H, 1, Ogre::PF_A8B8G8R8, NULL);
    p->hDstBox = Ogre::Box(0, 0, MM_EMU_SCREEN_W / 4, MM_EMU_SCREEN_H);
}
MM_EXPORT_EMULATOR void mmEmulatorFrame_Destroy(struct mmEmulatorFrame* p)
{
    p->pContextMaster = NULL;

    // c++ struct can not internal structure life cycle manual, do it external.
    // p->hName.~basic_string();
    // p->hMaterialPathPrefix.~basic_string();
    // p->hMaterialName.~basic_string();
    // p->hNameOgreTextureEmu0.~basic_string();
    // p->hNameOgreTextureEmu1.~basic_string();
    // p->hNameCEGUITextureEmu.~basic_string();
    // p->hNameCEGUIImageryEmu.~basic_string();

    p->hOgreTexture0.reset();
    p->hOgreTexture1.reset();

    p->pCEGUITexture = NULL;
    p->pCEGUIImagery = NULL;

    p->hHardwarePixelBuffer0.reset();
    p->hHardwarePixelBuffer1.reset();

    p->pRenderTarget0 = NULL;
    p->pRenderTarget1 = NULL;

    p->hMaterialPtr.reset();

    p->hSrcBox = Ogre::PixelBox();
    p->hDstBox = Ogre::Box();

    p->pPaletteRGB = NULL;

    p->hTextureW = MM_EMU_SCREEN_W;
    p->hTextureH = MM_EMU_SCREEN_H;

    mmSpinlock_Destroy(&p->hLockerScreenPtr0);
    mmSpinlock_Destroy(&p->hLockerScreenPtr1);
    // 0x3F is black color index.
    mmMemset(p->pScreenBufferPtr, 0x3F, p->hScreenRealSize * 2);
    //
    mmRoundup32(p->hTextureW);
    mmRoundup32(p->hTextureH);
    //
    p->hSrcBox = Ogre::PixelBox(MM_EMU_SCREEN_W / 4, MM_EMU_SCREEN_H, 1, Ogre::PF_A8B8G8R8, NULL);
    p->hDstBox = Ogre::Box(0, 0, MM_EMU_SCREEN_W / 4, MM_EMU_SCREEN_H);
    //
    mmFree(p->pScreenBufferPtr);
}

MM_EXPORT_EMULATOR void mmEmulatorFrame_SetContextMaster(struct mmEmulatorFrame* p, struct mmContextMaster* pContextMaster)
{
    p->pContextMaster = pContextMaster;
}
MM_EXPORT_EMULATOR void mmEmulatorFrame_SetName(struct mmEmulatorFrame* p, const std::string& name)
{
    p->hName = name;
    p->hMaterialName = "EMU/GeometryBuffer/emulator_" + p->hName;

    p->hNameOgreTextureEmu0 = MM_EMULATOR_FRAME_OGRE_TEXTURE + p->hName + "_0";
    p->hNameOgreTextureEmu1 = MM_EMULATOR_FRAME_OGRE_TEXTURE + p->hName + "_1";
    p->hNameCEGUITextureEmu = MM_EMULATOR_FRAME_CEGUI_TEXTURE + p->hName;
    p->hNameCEGUIImageryEmu = MM_EMULATOR_FRAME_CEGUI_IMAGERY + p->hName;
}
MM_EXPORT_EMULATOR void mmEmulatorFrame_SetMaterialPathPrefix(struct mmEmulatorFrame* p, const std::string& path)
{
    p->hMaterialPathPrefix = path;
}
MM_EXPORT_EMULATOR void mmEmulatorFrame_SetPaletteRGB(struct mmEmulatorFrame* p, void* data)
{
    p->pPaletteRGB = data;
}

MM_EXPORT_EMULATOR void mmEmulatorFrame_FinishLaunching(struct mmEmulatorFrame* p)
{
    Ogre::TextureManager* _TextureManager = Ogre::TextureManager::getSingletonPtr();
    CEGUI::ImageManager* _ImageManager = CEGUI::ImageManager::getSingletonPtr();
    struct mmOgreSystem* _OgreSystem = &p->pContextMaster->hOgreSystem;
    struct mmCEGUISystem* _CEGUISystem = &p->pContextMaster->hCEGUISystem;
    CEGUI::CEGUIOgreRenderer* _OgreRenderer = _CEGUISystem->pRenderer;
    Ogre::ResourceGroupManager* _ResourceGroupManager = Ogre::ResourceGroupManager::getSingletonPtr();
    Ogre::MaterialManager* _MaterialManager = Ogre::MaterialManager::getSingletonPtr();
    CEGUI::CEGUIOgreTexture* _OgreTexture = NULL;

    Ogre::MaterialPtr _GeometryBufferMaterial;
    Ogre::AliasTextureNamePairList _AliasTextureName;
    //
    CEGUI::Rectf _ImageArea(0, 0, (float)p->hTextureW, (float)p->hTextureH);
    CEGUI::Rectf _TextureArea;
    
    std::string _MaterialGroupName = p->hMaterialPathPrefix + "/materials";

    p->hOgreTexture0 = _TextureManager->createManual(
        p->hNameOgreTextureEmu0,
        Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        Ogre::TEX_TYPE_2D,
        (Ogre::uint)16,
        (Ogre::uint)16,
        1,
        0,
        Ogre::PF_A8R8G8B8,
        Ogre::TU_RENDERTARGET,
        NULL);

    // Ogre::PF_A8B8G8R8 always support. Ogre::PF_R8 Ogre::PF_L8 some driver not support.
    p->hOgreTexture1 = _TextureManager->createManual(
        p->hNameOgreTextureEmu1,
        Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        Ogre::TEX_TYPE_2D,
        (Ogre::uint)p->hTextureW / 4,
        (Ogre::uint)p->hTextureH,
        1,
        0,
        Ogre::PF_A8B8G8R8,
        Ogre::TU_RENDERTARGET,
        NULL);

    p->hHardwarePixelBuffer0 = p->hOgreTexture0->getBuffer(0, 0);
    p->hHardwarePixelBuffer1 = p->hOgreTexture1->getBuffer(0, 0);

    p->pRenderTarget0 = p->hHardwarePixelBuffer0->getRenderTarget();
    p->pRenderTarget1 = p->hHardwarePixelBuffer1->getRenderTarget();

    p->pCEGUITexture = &_OgreRenderer->createTexture(p->hNameCEGUITextureEmu);
    _OgreTexture = static_cast<CEGUI::CEGUIOgreTexture*>(p->pCEGUITexture);

    _TextureArea.setPosition(CEGUI::Vector2f(0, 0));
    _TextureArea.setWidth((float)p->hTextureW);
    _TextureArea.setHeight((float)p->hTextureH);

    // weak reference mode.
    _OgreTexture->setOgreTexture(p->hOgreTexture1, _TextureArea);
    // note: the ogre texture size width is logic width / 4. we need reset the logic size.
    _OgreTexture->setOgreTextureSize(_TextureArea.getSize());
    _OgreTexture->updateCachedScaleValues((float)1.0f);

    p->pCEGUIImagery = static_cast<CEGUI::BasicImage*>(&_ImageManager->create("BasicImage", p->hNameCEGUIImageryEmu));
    p->pCEGUIImagery->setTexture(p->pCEGUITexture);

    p->pCEGUIImagery->setArea(_ImageArea);
    p->pCEGUIImagery->setAutoScaled(CEGUI::ASM_Disabled);

    // note: we update render target Manual.
    p->pRenderTarget0->setAutoUpdated(false);
    p->pRenderTarget1->setAutoUpdated(false);

    // update palette.
    mmEmulatorFrame_UpdatePalette(p);

    // materials.
    mmOgreSystem_AcquireMaterialsResources(_OgreSystem, p->hMaterialPathPrefix.c_str(), _MaterialGroupName.c_str());
    _ResourceGroupManager->initialiseResourceGroup(p->hMaterialPathPrefix + "/materials");
    _GeometryBufferMaterial = _MaterialManager->getByName("EMU/GeometryBuffer", p->hMaterialPathPrefix + "/materials");
    p->hMaterialPtr = _GeometryBufferMaterial->clone(p->hMaterialName);

    _AliasTextureName["EMU/GeometryBuffer0"] = p->hNameOgreTextureEmu0;
    _AliasTextureName["EMU/GeometryBuffer1"] = p->hNameOgreTextureEmu1;

    p->hMaterialPtr->applyTextureAliases(_AliasTextureName);
}
MM_EXPORT_EMULATOR void mmEmulatorFrame_BeforeTerminate(struct mmEmulatorFrame* p)
{
    Ogre::TextureManager* _TextureManager = Ogre::TextureManager::getSingletonPtr();
    CEGUI::ImageManager* _ImageManager = CEGUI::ImageManager::getSingletonPtr();
    struct mmOgreSystem* _OgreSystem = &p->pContextMaster->hOgreSystem;
    struct mmCEGUISystem* _CEGUISystem = &p->pContextMaster->hCEGUISystem;
    CEGUI::CEGUIOgreRenderer* _OgreRenderer = _CEGUISystem->pRenderer;
    Ogre::MaterialManager* _MaterialManager = Ogre::MaterialManager::getSingletonPtr();

    std::string _MaterialGroupName = p->hMaterialPathPrefix + "/materials";
    
    _MaterialManager->remove(p->hMaterialPtr->getHandle());
    p->hMaterialPtr.reset();
    
    mmOgreSystem_ReleaseMaterialsResources(_OgreSystem, p->hMaterialPathPrefix.c_str(), _MaterialGroupName.c_str());

    _ImageManager->destroy(*p->pCEGUIImagery);
    _OgreRenderer->destroyTexture(*p->pCEGUITexture);
    _TextureManager->remove(p->hOgreTexture0);
    p->hOgreTexture0.reset();
    _TextureManager->remove(p->hOgreTexture1);
    p->hOgreTexture1.reset();
    //
    p->hHardwarePixelBuffer0.reset();
    p->hHardwarePixelBuffer1.reset();
}

MM_EXPORT_EMULATOR void mmEmulatorFrame_EnterBackground(struct mmEmulatorFrame* p)
{

}
MM_EXPORT_EMULATOR void mmEmulatorFrame_EnterForeground(struct mmEmulatorFrame* p)
{
    if (NULL != p->hOgreTexture0.get())
    {
        // some time the HardwarePixelBuffer will change when EnterBackground and EnterForeground.
        p->hHardwarePixelBuffer0 = p->hOgreTexture0->getBuffer(0, 0);

        p->pRenderTarget0 = p->hHardwarePixelBuffer0->getRenderTarget();
        // note: we update render target Manual.
        p->pRenderTarget0->setAutoUpdated(false);
        //
        mmEmulatorFrame_UpdatePalette(p);
    }
    if (NULL != p->hOgreTexture1.get())
    {
        // some time the HardwarePixelBuffer will change when EnterBackground and EnterForeground.
        p->hHardwarePixelBuffer1 = p->hOgreTexture1->getBuffer(0, 0);

        p->pRenderTarget1 = p->hHardwarePixelBuffer1->getRenderTarget();
        // note: we update render target Manual.
        p->pRenderTarget1->setAutoUpdated(false);
        //
        mmEmulatorFrame_UpdateFrameBitmap(p);
    }
}

MM_EXPORT_EMULATOR void mmEmulatorFrame_UpdatePalette(struct mmEmulatorFrame* p)
{
    Ogre::PixelBox srcPixelBox(16, 16, 1, Ogre::PF_A8R8G8B8, (void*)p->pPaletteRGB);
    Ogre::Box dstBox(0, 0, 16, 16);

    p->pRenderTarget0->update(false);

    p->hHardwarePixelBuffer0->blitFromMemory(srcPixelBox, dstBox);

    p->pRenderTarget0->swapBuffers();
}
MM_EXPORT_EMULATOR void mmEmulatorFrame_UpdateFrameBitmap(struct mmEmulatorFrame* p)
{
    p->pRenderTarget1->update(false);

    if (mmSpinlock_Trylock(&p->hLockerScreenPtr1))
    {
        p->hSrcBox.data = (void*)p->pScreenPtr[1];
        p->hHardwarePixelBuffer1->blitFromMemory(p->hSrcBox, p->hDstBox);

        mmSpinlock_Unlock(&p->hLockerScreenPtr1);
    }
    else
    {
        mmSpinlock_Lock(&p->hLockerScreenPtr0);

        p->hSrcBox.data = (void*)p->pScreenPtr[0];
        p->hHardwarePixelBuffer1->blitFromMemory(p->hSrcBox, p->hDstBox);

        mmSpinlock_Unlock(&p->hLockerScreenPtr0);
    }

    p->pRenderTarget1->swapBuffers();
}

MM_EXPORT_EMULATOR int mmEmulatorFrame_ProcessBuffer(struct mmEmulatorFrame* p, mmByte_t* buffer, size_t length)
{
    if (mmSpinlock_Trylock(&p->hLockerScreenPtr0))
    {
        mmMemcpy(p->pScreenPtr[0], buffer, length);

        mmSpinlock_Lock(&p->hLockerScreenPtr1);
        mmInteger_UIntptrSwap((uintptr_t*)&p->pScreenPtr[0], (uintptr_t*)&p->pScreenPtr[1]);
        mmSpinlock_Unlock(&p->hLockerScreenPtr1);

        mmSpinlock_Unlock(&p->hLockerScreenPtr0);
		
		return 0;
    }
    else
    {
        mmSpinlock_Lock(&p->hLockerScreenPtr1);
        mmMemcpy(p->pScreenPtr[1], buffer, length);
        mmSpinlock_Unlock(&p->hLockerScreenPtr1);
		
		return 1;
    }
}

