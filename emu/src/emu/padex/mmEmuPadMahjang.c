/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadMahjang.h"
#include "emu/mmEmuNes.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuPadMahjang_Init(struct mmEmuPadMahjang* p)
{
    mmEmuExpad_Init(&p->super);

    p->outbits = 0;
    p->padbits = 0;

    //
    p->super.Reset = &mmEmuPadMahjang_Reset;
    //p->super.Strobe = &mmEmuPadMahjang_Strobe;
    //p->super.Read4016 = &mmEmuPadMahjang_Read4016;
    p->super.Read4017 = &mmEmuPadMahjang_Read4017;
    p->super.Write4016 = &mmEmuPadMahjang_Write4016;
    //p->super.Write4017 = &mmEmuPadMahjang_Write4017;
    p->super.Sync = &mmEmuPadMahjang_Sync;
    p->super.SetSyncData = &mmEmuPadMahjang_SetSyncData;
    p->super.GetSyncData = &mmEmuPadMahjang_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadMahjang_Destroy(struct mmEmuPadMahjang* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->outbits = 0;
    p->padbits = 0;
}

MM_EXPORT_EMU void mmEmuPadMahjang_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadMahjang* p = (struct mmEmuPadMahjang*)(super);

    p->outbits = 0;
    p->padbits = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadMahjang_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadMahjang* p = (struct mmEmuPadMahjang*)(super);

    mmByte_t out = (p->outbits & 1) << 1;

    p->outbits >>= 1;
    return  out;
}
MM_EXPORT_EMU void mmEmuPadMahjang_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadMahjang* p = (struct mmEmuPadMahjang*)(super);

    p->outbits = 0;

    if ((data & 0x06) == 0x02)
    {
        // H-N
        p->outbits = (mmByte_t)p->padbits;
    }
    else
    {
        if ((data & 0x06) == 0x04)
        {
            // A-G
            p->outbits = (mmByte_t)(p->padbits >> 8);
        }
        else
        {
            if ((data & 0x06) == 0x06)
            {
                // Start,Select,Kan,Pon,Chii,Reach,Ron
                p->outbits = (mmByte_t)(p->padbits >> 16);
            }
        }
    }
}

MM_EXPORT_EMU void mmEmuPadMahjang_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadMahjang* p = (struct mmEmuPadMahjang*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    struct mmLogger* gLogger = mmLogger_Instance();

    p->padbits = 0;

    // H-N
    if (mmEmuController_ExButtonCheck(controller, 3, 8)) p->padbits |= 1 << 7;
    if (mmEmuController_ExButtonCheck(controller, 3, 9)) p->padbits |= 1 << 6;
    if (mmEmuController_ExButtonCheck(controller, 3, 10)) p->padbits |= 1 << 5;
    if (mmEmuController_ExButtonCheck(controller, 3, 11)) p->padbits |= 1 << 4;
    if (mmEmuController_ExButtonCheck(controller, 3, 12)) p->padbits |= 1 << 3;
    if (mmEmuController_ExButtonCheck(controller, 3, 13)) p->padbits |= 1 << 2;
    // A-G
    if (mmEmuController_ExButtonCheck(controller, 3, 0)) p->padbits |= 1 << (7 + 8);
    if (mmEmuController_ExButtonCheck(controller, 3, 1)) p->padbits |= 1 << (6 + 8);
    if (mmEmuController_ExButtonCheck(controller, 3, 2)) p->padbits |= 1 << (5 + 8);
    if (mmEmuController_ExButtonCheck(controller, 3, 3)) p->padbits |= 1 << (4 + 8);
    if (mmEmuController_ExButtonCheck(controller, 3, 4)) p->padbits |= 1 << (3 + 8);
    if (mmEmuController_ExButtonCheck(controller, 3, 5)) p->padbits |= 1 << (2 + 8);
    if (mmEmuController_ExButtonCheck(controller, 3, 6)) p->padbits |= 1 << (1 + 8);
    if (mmEmuController_ExButtonCheck(controller, 3, 7)) p->padbits |= 1 << (0 + 8);
    // Select,Start,Kan,Pon,Chii,Reach,Ron
    if (mmEmuController_ExButtonCheck(controller, 3, 14)) p->padbits |= 1 << (6 + 16);
    if (mmEmuController_ExButtonCheck(controller, 3, 15)) p->padbits |= 1 << (7 + 16);
    if (mmEmuController_ExButtonCheck(controller, 3, 16)) p->padbits |= 1 << (5 + 16);
    if (mmEmuController_ExButtonCheck(controller, 3, 17)) p->padbits |= 1 << (4 + 16);
    if (mmEmuController_ExButtonCheck(controller, 3, 18)) p->padbits |= 1 << (3 + 16);
    if (mmEmuController_ExButtonCheck(controller, 3, 19)) p->padbits |= 1 << (2 + 16);
    if (mmEmuController_ExButtonCheck(controller, 3, 20)) p->padbits |= 1 << (1 + 16);

    //DEBUGOUT("%08X\n", padbits);
    mmLogger_LogD(gLogger, "%s %d padbits=%08X.", __FUNCTION__, __LINE__, p->padbits);
}
MM_EXPORT_EMU void mmEmuPadMahjang_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadMahjang* p = (struct mmEmuPadMahjang*)(super);

    p->padbits = (mmUInt32_t)data;
}
MM_EXPORT_EMU mmLong_t mmEmuPadMahjang_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadMahjang* p = (struct mmEmuPadMahjang*)(super);

    return (mmLong_t)p->padbits;
}

static struct mmEmuExpad* __static_mmEmuPadMahjang_Produce(void)
{
    struct mmEmuPadMahjang* p = (struct mmEmuPadMahjang*)mmMalloc(sizeof(struct mmEmuPadMahjang));
    mmEmuPadMahjang_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadMahjang_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadMahjang* p = (struct mmEmuPadMahjang*)(m);
    mmEmuPadMahjang_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_MAHJANG =
{
    &__static_mmEmuPadMahjang_Produce,
    &__static_mmEmuPadMahjang_Recycle,
};
