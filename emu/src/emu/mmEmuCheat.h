/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuCheat_h__
#define __mmEmuCheat_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

// The following two are OR masks.
#define MM_EMU_CHEAT_ENABLE     (1<<0)
#define MM_EMU_CHEAT_KEYDISABLE (1<<1)

// Write type.
#define MM_EMU_CHEAT_TYPE_ALWAYS    0   // Always write
#define MM_EMU_CHEAT_TYPE_ONCE      1   // Write only once
#define MM_EMU_CHEAT_TYPE_GREATER   2   // When it is larger than data
#define MM_EMU_CHEAT_TYPE_LESS      3   // When it is smaller than data

// Data length.
#define MM_EMU_CHEAT_LENGTH_1BYTE   0
#define MM_EMU_CHEAT_LENGTH_2BYTE   1
#define MM_EMU_CHEAT_LENGTH_3BYTE   2
#define MM_EMU_CHEAT_LENGTH_4BYTE   3

struct mmEmuCheatcode
{
    mmByte_t    enable;
    mmByte_t    type;
    mmByte_t    length;
    mmWord_t    address;
    mmUInt32_t  data;

    struct mmString comment;
};
MM_EXPORT_EMU void mmEmuCheatcode_Init(struct mmEmuCheatcode* p);
MM_EXPORT_EMU void mmEmuCheatcode_Destroy(struct mmEmuCheatcode* p);

MM_EXPORT_EMU void mmEmuCheatcode_CopyForm(struct mmEmuCheatcode* p, const struct mmEmuCheatcode* q);

struct mmEmuGeniecode
{
    mmWord_t    address;
    mmByte_t    data;
    mmByte_t    cmp;
};
MM_EXPORT_EMU void mmEmuGeniecode_Init(struct mmEmuGeniecode* p);
MM_EXPORT_EMU void mmEmuGeniecode_Destroy(struct mmEmuGeniecode* p);

struct mmEmuNes;

struct mmEmuCheat
{
    struct mmEmuNes* nes;

    // For Cheat
    mmBool_t m_bCheatCodeAdd;
    struct mmVectorValue m_CheatCode;

    // For Genie
    struct mmVectorValue m_GenieCode;
};
MM_EXPORT_EMU void mmEmuCheat_Init(struct mmEmuCheat* p);
MM_EXPORT_EMU void mmEmuCheat_Destroy(struct mmEmuCheat* p);

MM_EXPORT_EMU void mmEmuCheat_SetParent(struct mmEmuCheat* p, struct mmEmuNes* parent);

// For Cheat
MM_EXPORT_EMU void mmEmuCheat_CheatInitial(struct mmEmuCheat* p);

MM_EXPORT_EMU mmBool_t mmEmuCheat_IsCheatCodeAdd(struct mmEmuCheat* p);

MM_EXPORT_EMU mmInt_t mmEmuCheat_GetCheatCodeNum(const struct mmEmuCheat* p);
MM_EXPORT_EMU mmBool_t mmEmuCheat_GetCheatCode(const struct mmEmuCheat* p, mmInt_t no, struct mmEmuCheatcode* code);
MM_EXPORT_EMU void mmEmuCheat_SetCheatCodeFlag(struct mmEmuCheat* p, mmInt_t no, mmBool_t bEnable);
MM_EXPORT_EMU void mmEmuCheat_SetCheatCodeAllFlag(struct mmEmuCheat* p, mmBool_t bEnable, mmBool_t bKey);

MM_EXPORT_EMU void mmEmuCheat_ReplaceCheatCode(struct mmEmuCheat* p, mmInt_t no, struct mmEmuCheatcode* code);
MM_EXPORT_EMU void mmEmuCheat_AddCheatCode(struct mmEmuCheat* p, struct mmEmuCheatcode* code);
MM_EXPORT_EMU void mmEmuCheat_DelCheatCode(struct mmEmuCheat* p, mmInt_t no);

MM_EXPORT_EMU mmUInt32_t mmEmuCheat_CheatRead(struct mmEmuCheat* p, mmInt_t length, mmWord_t addr);
MM_EXPORT_EMU void mmEmuCheat_CheatWrite(struct mmEmuCheat* p, mmInt_t length, mmWord_t addr, mmUInt32_t data);
MM_EXPORT_EMU void mmEmuCheat_CheatCodeProcess(struct mmEmuCheat* p);

// For Genie
MM_EXPORT_EMU void mmEmuCheat_GenieInitial(struct mmEmuCheat* p);
MM_EXPORT_EMU void mmEmuCheat_GenieLoad(struct mmEmuCheat* p, const char* fname);
MM_EXPORT_EMU void mmEmuCheat_GenieCodeProcess(struct mmEmuCheat* p);

#include "core/mmSuffix.h"

#endif//__mmEmuCheat_h__
