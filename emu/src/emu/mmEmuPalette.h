/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuPalette_h__
#define __mmEmuPalette_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

/*
 * palette
 *
 * 0xAARRGGBB[64]
 */

// make 0xAABBGGRR format R8G8B8A8
static mmInline void mmEmuPaletteABGRMake(mmUInt32_t* c, mmByte_t a, mmByte_t b, mmByte_t g, mmByte_t r)
{
    (*c) = (a << 24) | (b << 16) | (g << 8) | (r);
}

// make 0xAARRGGBB format B8G8R8A8
static mmInline void mmEmuPaletteARGBMake(mmUInt32_t* c, mmByte_t a, mmByte_t r, mmByte_t g, mmByte_t b)
{
    (*c) = (a << 24) | (r << 16) | (g << 8) | (b);
}

static mmInline mmByte_t mmEmuPaletteARGBGetA(mmUInt32_t c)
{
    return (c >> 24) & 0xFF;
}
static mmInline mmByte_t mmEmuPaletteARGBGetR(mmUInt32_t c)
{
    return (c >> 16) & 0xFF;
}
static mmInline mmByte_t mmEmuPaletteARGBGetG(mmUInt32_t c)
{
    return (c >>  8) & 0xFF;
}
static mmInline mmByte_t mmEmuPaletteARGBGetB(mmUInt32_t c)
{
    return (c      ) & 0xFF;
}

// Default.
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_DEFAULT[64];

/* 2C02 */
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_2C02[64];

/* 2C03 */
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_2C03[64];

/* 2C03B */
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_2C03B[64];

/* 2C04-0001 */
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_2C04_0001[64];

/* 2C04-0002 */
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_2C04_0002[64];

/* 2C04-0003 */
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_2C04_0003[64];

/* 2C04-0004 */
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_2C04_0004[64];

/* 2C05 */
MM_EXPORT_EMU extern const mmUInt32_t MM_EMU_PALETTE_2C05[64];

#include "core/mmSuffix.h"

#endif//__mmEmuPalette_h__
