/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper003.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper003_Init(struct mmEmuMapper003* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper003_Reset;
    p->super.Write = &mmEmuMapper003_Write;
    //p->super.Read = &mmEmuMapper003_Read;
    //p->super.ReadLow = &mmEmuMapper003_ReadLow;
    //p->super.WriteLow = &mmEmuMapper003_WriteLow;
    //p->super.ExRead = &mmEmuMapper003_ExRead;
    //p->super.ExWrite = &mmEmuMapper003_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper003_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper003_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper003_HSync;
    //p->super.VSync = &mmEmuMapper003_VSync;
    //p->super.Clock = &mmEmuMapper003_Clock;
    //p->super.PPULatch = &mmEmuMapper003_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper003_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper003_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper003_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper003_IsStateSave;
    //p->super.SaveState = &mmEmuMapper003_SaveState;
    //p->super.LoadState = &mmEmuMapper003_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper003_Destroy(struct mmEmuMapper003* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper003_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper003* p = (struct mmEmuMapper003*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    switch (mmu->PROM_16K_SIZE)
    {
    case 1: // 16K only
        mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
        mmEmuMmu_SetPROM16KBank(mmu, 6, 0);
        break;
    case 2: // 32K
        mmEmuMmu_SetPROM32KBank(mmu, 0);
        break;
    default:
        break;
    }
    //  nes->SetRenderMethod( NES::TILE_RENDER );
    crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);

    if (crc == 0x2b72fe7e)      // Ganso Saiyuuki - Super Monkey Dai Bouken(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
        mmEmuPpu_SetExtNameTableMode(&nes->ppu, MM_TRUE);
    }

    //if( crc == 0xE44D95B5 ) 
    //{
    //}
}
MM_EXPORT_EMU void mmEmuMapper003_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper003* p = (struct mmEmuMapper003*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetVROM8KBank(mmu, data);
}

static struct mmEmuMapper* __static_mmEmuMapper003_Produce(void)
{
    struct mmEmuMapper003* p = (struct mmEmuMapper003*)mmMalloc(sizeof(struct mmEmuMapper003));
    mmEmuMapper003_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper003_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper003* p = (struct mmEmuMapper003*)(m);
    mmEmuMapper003_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER003 =
{
    &__static_mmEmuMapper003_Produce,
    &__static_mmEmuMapper003_Recycle,
};
