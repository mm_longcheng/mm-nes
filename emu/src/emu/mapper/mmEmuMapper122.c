/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper122.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper122_Init(struct mmEmuMapper122* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper122_Reset;
    //p->super.Write = &mmEmuMapper122_Write;
    //p->super.Read = &mmEmuMapper122_Read;
    //p->super.ReadLow = &mmEmuMapper122_ReadLow;
    p->super.WriteLow = &mmEmuMapper122_WriteLow;
    //p->super.ExRead = &mmEmuMapper122_ExRead;
    //p->super.ExWrite = &mmEmuMapper122_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper122_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper122_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper122_HSync;
    //p->super.VSync = &mmEmuMapper122_VSync;
    //p->super.Clock = &mmEmuMapper122_Clock;
    //p->super.PPULatch = &mmEmuMapper122_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper122_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper122_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper122_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper122_IsStateSave;
    //p->super.SaveState = &mmEmuMapper122_SaveState;
    //p->super.LoadState = &mmEmuMapper122_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper122_Destroy(struct mmEmuMapper122* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper122_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper122* p = (struct mmEmuMapper122*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
}
MM_EXPORT_EMU void mmEmuMapper122_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper122* p = (struct mmEmuMapper122*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0x6000)
    {
        mmEmuMmu_SetVROM4KBank(mmu, 0, data & 0x07);
        mmEmuMmu_SetVROM4KBank(mmu, 4, (data & 0x70) >> 4);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper122_Produce(void)
{
    struct mmEmuMapper122* p = (struct mmEmuMapper122*)mmMalloc(sizeof(struct mmEmuMapper122));
    mmEmuMapper122_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper122_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper122* p = (struct mmEmuMapper122*)(m);
    mmEmuMapper122_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER122 =
{
    &__static_mmEmuMapper122_Produce,
    &__static_mmEmuMapper122_Recycle,
};
