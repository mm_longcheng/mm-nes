/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper248.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper248_Init(struct mmEmuMapper248* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    p->we_sram = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    //
    p->super.Reset = &mmEmuMapper248_Reset;
    p->super.Write = &mmEmuMapper248_Write;
    //p->super.Read = &mmEmuMapper248_Read;
    //p->super.ReadLow = &mmEmuMapper248_ReadLow;
    p->super.WriteLow = &mmEmuMapper248_WriteLow;
    //p->super.ExRead = &mmEmuMapper248_ExRead;
    //p->super.ExWrite = &mmEmuMapper248_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper248_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper248_ExCmdWrite;
    p->super.HSync = &mmEmuMapper248_HSync;
    //p->super.VSync = &mmEmuMapper248_VSync;
    //p->super.Clock = &mmEmuMapper248_Clock;
    //p->super.PPULatch = &mmEmuMapper248_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper248_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper248_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper248_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper248_IsStateSave;
    p->super.SaveState = &mmEmuMapper248_SaveState;
    p->super.LoadState = &mmEmuMapper248_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper248_Destroy(struct mmEmuMapper248* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    p->we_sram = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;
}

MM_EXPORT_EMU void mmEmuMapper248_SetBankCPU(struct mmEmuMapper248* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, p->prg1, p->prg0, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper248_SetBankPPU(struct mmEmuMapper248* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr4, p->chr5, p->chr6, p->chr7,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1);
        }
        else
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1,
                p->chr4, p->chr5, p->chr6, p->chr7);
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper248_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper248* p = (struct mmEmuMapper248*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper248_SetBankCPU(p);

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    mmEmuMapper248_SetBankPPU(p);

    p->we_sram = 0; // Disable
    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;
}
MM_EXPORT_EMU void mmEmuMapper248_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper248* p = (struct mmEmuMapper248*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper248_SetBankCPU(p);
        mmEmuMapper248_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;

        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr01 = data & 0xFE;
            mmEmuMapper248_SetBankPPU(p);
            break;
        case 0x01:
            p->chr23 = data & 0xFE;
            mmEmuMapper248_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper248_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper248_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper248_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper248_SetBankPPU(p);
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper248_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper248_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xC000:
        p->irq_enable = 0;
        p->irq_latch = 0xBE;
        p->irq_counter = 0xBE;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xC001:
        p->irq_enable = 1;
        p->irq_latch = 0xBE;
        p->irq_counter = 0xBE;
        break;
    default:
        break;
    }

}
MM_EXPORT_EMU void mmEmuMapper248_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper248* p = (struct mmEmuMapper248*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 2 * data, 2 * data + 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}

MM_EXPORT_EMU void mmEmuMapper248_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper248* p = (struct mmEmuMapper248*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(p->irq_counter--))
                {
                    p->irq_counter = p->irq_latch;
                    //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper248_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper248_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper248* p = (struct mmEmuMapper248*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
    buffer[16] = p->irq_enable;
    buffer[17] = (mmByte_t)p->irq_counter;
    buffer[18] = p->irq_latch;
    buffer[19] = p->irq_request;
}
MM_EXPORT_EMU void mmEmuMapper248_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper248* p = (struct mmEmuMapper248*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
    p->irq_enable = buffer[16];
    p->irq_counter = (mmInt_t)buffer[17];
    p->irq_latch = buffer[18];
    p->irq_request = buffer[19];
}

static struct mmEmuMapper* __static_mmEmuMapper248_Produce(void)
{
    struct mmEmuMapper248* p = (struct mmEmuMapper248*)mmMalloc(sizeof(struct mmEmuMapper248));
    mmEmuMapper248_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper248_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper248* p = (struct mmEmuMapper248*)(m);
    mmEmuMapper248_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER248 =
{
    &__static_mmEmuMapper248_Produce,
    &__static_mmEmuMapper248_Recycle,
};
