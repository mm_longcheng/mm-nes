/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuEmulator.h"

#include "mmEmuNes.h"

#include "core/mmLogger.h"
#include "core/mmByteOrder.h"

// Sound mute string table.
static const char* g_lpSoundMuteStringTable[] =
{
    "Master ",
    "Rectangle 1",
    "Rectangle 2",
    "Triangle ",
    "Noise ",
    "DPCM ",
    "Ex CH1 ",
    "Ex CH2 ",
    "Ex CH3 ",
    "Ex CH4 ",
    "Ex CH5 ",
    "Ex CH6 ",
    "Ex CH7 ",
    "Ex CH8 ",
    NULL,
    NULL,
};

static void __static_mmEmuEmulator_MovieFinish(struct mmEmuNes* obj);
static void __static_mmEmuEmulator_ScreenMessage(struct mmEmuNes* obj, const char* message);

static void* __static_mmEmuEmulator_UpdateThread(void* arg);
static void __static_mmEmuEmulator_FrameTimerUnitUpdateSynchronize(void* obj, double interval);
static void __static_mmEmuEmulator_DiskCommand(struct mmEmuEmulator* p, mmByte_t cmd);
static void __static_mmEmuEmulator_MessageQueueEvHandle(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_MessageQueueNtHandle(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_HandleForCommonEvent(struct mmEmuEmulator* p);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_EXIT(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_NONE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_INITIAL(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_PLAY(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_STOP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_PAUSE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_RESUME(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_VIDEOMODE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_LOADROM(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SCREENCLEAR(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SETMESSAGESTRING(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_FULLSCREEN_GDI(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_EMUPAUSE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_ONEFRAME(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_THROTTLE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_AUTO(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_UP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_DOWN(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_HWRESET(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SWRESET(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_NETPLAY_START(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_STATE_LOAD(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_STATE_SAVE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DISK_COMMAND(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_EXCONTROLLER(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SOUND_MUTE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SNAPSHOT(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_PLAY(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_REC(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_RECAPPEND(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_STOP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_WAVEREC_START(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_WAVEREC_STOP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_PLAY(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_REC(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_STOP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_BARCODE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_TURBOFILE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_RUN(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_BRAKE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_STEP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_COMMAND(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);

MM_EXPORT_EMU void mmEmuEmulatorEvent_Init(struct mmEmuEmulatorEvent* p)
{
    p->p0 = 0;
    p->p1 = 0;
    mmString_Init(&p->ps);
}
MM_EXPORT_EMU void mmEmuEmulatorEvent_Destroy(struct mmEmuEmulatorEvent* p)
{
    p->p0 = 0;
    p->p1 = 0;
    mmString_Destroy(&p->ps);
}

MM_EXPORT_EMU void mmEmuEmulatorEvent_Reset(struct mmEmuEmulatorEvent* p)
{
    p->p0 = 0;
    p->p1 = 0;
    mmString_Clear(&p->ps);
}

MM_EXPORT_EMU size_t mmEmuEmulatorEvent_BufferSize(const struct mmEmuEmulatorEvent* p)
{
    return 8 + mmString_Size(&p->ps);
}

MM_EXPORT_EMU void mmEmuEmulatorEvent_Encode(const struct mmEmuEmulatorEvent* p, struct mmByteBuffer* b, size_t* offset)
{
    mmUInt32_t sz = (mmUInt32_t)mmString_Size(&p->ps);

    assert(8 <= b->length && "8 <= b->length is invalid, buffer out of range.");

    mmByteOrder_ByteBiggerEncodeU32(b, offset, &p->p0);
    mmByteOrder_ByteBiggerEncodeU32(b, offset, &p->p1);

    assert(b->length >= mmEmuEmulatorEvent_BufferSize(p) && "b->length >= mmEmuEmulatorEvent_BufferSize(p) is invalid, buffer out of range.");

    /* text, not null-terminated, not package null */
    mmByteOrder_ByteBiggerEncodeU32(b, offset, &sz);
    mmMemcpy(b->buffer + b->offset + *offset, mmString_CStr(&p->ps), mmString_Size(&p->ps));
    *offset += mmString_Size(&p->ps);
}
MM_EXPORT_EMU void mmEmuEmulatorEvent_Decode(struct mmEmuEmulatorEvent* p, const struct mmByteBuffer* b, size_t* offset)
{
    mmUInt32_t sz = 0;

    assert(8 <= b->length && "8 <= b->length is invalid, buffer out of range.");

    mmByteOrder_ByteBiggerDecodeU32(b, offset, &p->p0);
    mmByteOrder_ByteBiggerDecodeU32(b, offset, &p->p1);

    assert(b->length >= mmEmuEmulatorEvent_BufferSize(p) && "b->length >= mmEmuEmulatorEvent_BufferSize(p) is invalid, buffer out of range.");

    /* text, not null-terminated, append null manual */
    mmByteOrder_ByteBiggerDecodeU32(b, offset, &sz);
    mmString_SetSizeValue(&p->ps, sz);
    mmString_Assignsn(&p->ps, (const char *)(b->buffer + b->offset + *offset), mmString_Size(&p->ps));
    *offset += mmString_Size(&p->ps);
}

MM_EXPORT_EMU void mmEmuEmulatorEvent_SetP0(struct mmEmuEmulatorEvent* p, mmUInt32_t p0)
{
    p->p0 = p0;
}
MM_EXPORT_EMU void mmEmuEmulatorEvent_SetP1(struct mmEmuEmulatorEvent* p, mmUInt32_t p1)
{
    p->p1 = p1;
}
MM_EXPORT_EMU void mmEmuEmulatorEvent_SetPs(struct mmEmuEmulatorEvent* p, const char* ps)
{
    mmString_Assigns(&p->ps, ps);
}

// MM_SUCCESS MM_FAILURE
MM_EXPORT_EMU int mmEmuEmulatorEvent_Validity(const struct mmEmuEmulatorEvent* p)
{
    return MM_SUCCESS;
}

static void __static_mmEmuEmulator_HandleDefault(void* obj, void* u, mmUInt32_t mid, struct mmEmuEmulatorEvent* pEvent)
{

}
MM_EXPORT_EMU void mmEmuEmulatorCallback_Init(struct mmEmuEmulatorCallback* p)
{
    p->Handle = &__static_mmEmuEmulator_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_EMU void mmEmuEmulatorCallback_Destroy(struct mmEmuEmulatorCallback* p)
{
    p->Handle = &__static_mmEmuEmulator_HandleDefault;
    p->obj = NULL;
}

static void __static_mmEmuEmulator_ProcessorThreadEnter(struct mmEmuEmulatorProcessor* obj)
{

}
static void __static_mmEmuEmulator_ProcessorThreadLeave(struct mmEmuEmulatorProcessor* obj)
{

}
static int __static_mmEmuEmulator_ProcessorProcessAudio(struct mmEmuEmulatorProcessor* obj, mmByte_t* buffer, size_t length)
{
    return 0;
}
static int __static_mmEmuEmulator_ProcessorProcessFrame(struct mmEmuEmulatorProcessor* obj, mmByte_t* buffer, size_t length)
{
    return 0;
}
MM_EXPORT_EMU void mmEmuEmulatorProcessor_Init(struct mmEmuEmulatorProcessor* p)
{
    p->emulator = NULL;

    p->ThreadEnter = &__static_mmEmuEmulator_ProcessorThreadEnter;
    p->ThreadLeave = &__static_mmEmuEmulator_ProcessorThreadLeave;
    p->ProcessAudio = &__static_mmEmuEmulator_ProcessorProcessAudio;
    p->ProcessFrame = &__static_mmEmuEmulator_ProcessorProcessFrame;
}
MM_EXPORT_EMU void mmEmuEmulatorProcessor_Destroy(struct mmEmuEmulatorProcessor* p)
{
    p->emulator = NULL;

    p->ThreadEnter = &__static_mmEmuEmulator_ProcessorThreadEnter;
    p->ThreadLeave = &__static_mmEmuEmulator_ProcessorThreadLeave;
    p->ProcessAudio = &__static_mmEmuEmulator_ProcessorProcessAudio;
    p->ProcessFrame = &__static_mmEmuEmulator_ProcessorProcessFrame;
}
MM_EXPORT_EMU void mmEmuEmulatorProcessor_SetParent(struct mmEmuEmulatorProcessor* p, struct mmEmuEmulator* parent)
{
    p->emulator = parent;
}

MM_EXPORT_EMU const struct mmEmuEmulatorProcessor MM_EMU_EMULATOR_PROCESSOR_DEFAULT =
{
    NULL,
    &__static_mmEmuEmulator_ProcessorThreadEnter,
    &__static_mmEmuEmulator_ProcessorThreadLeave,
    &__static_mmEmuEmulator_ProcessorProcessAudio,
    &__static_mmEmuEmulator_ProcessorProcessFrame,
};

MM_EXPORT_EMU void mmEmuEmulator_Init(struct mmEmuEmulator* p)
{
    struct mmFrameTimerCallback frame_timer_callback;
    struct mmEmuNesCallback nes_callback;
    struct mmMessageQueueCallback queue_callback;

    p->palette = (mmUInt32_t*)mmMalloc(sizeof(mmUInt32_t) * 256);

    p->emu = (struct mmEmuNes*)mmMalloc(sizeof(struct mmEmuNes));

    mmEmuNes_Init(p->emu);
    mmFrameTask_Init(&p->frame_task);
    mmEmuEmulatorCallback_Init(&p->callback_q);

    mmRbtreeU32Vpt_Init(&p->rbtree_q);

    mmSpinlock_Init(&p->locker_rbtree_q, NULL);

    p->frequency = MM_EMU_EMULATOR_FREQUENCY_DEFAULT;

    p->rom_load = MM_FAILURE;
    p->background = MM_FALSE;
    p->background_runing = MM_FALSE;

    p->audio_buffer_idx = 0;
    mmStreambuf_Init(&p->audio_block);
    p->audio_block_size = 0;
    p->audio_frame_size = 0;
    p->audio_block_k = 0;
    p->audio_frame_index = 0;
    p->audio_queue_balance = 0;

    p->processor = (struct mmEmuEmulatorProcessor*)(&MM_EMU_EMULATOR_PROCESSOR_DEFAULT);

    mmMessageQueue_Init(&p->mq_ev);
    mmMessageQueue_Init(&p->mq_nt);

    p->emulator_state = MM_EMULATOR_NONE;

    p->state = MM_TS_CLOSED;

    p->u = NULL;

    mmMemset(p->palette, 0, sizeof(mmUInt32_t) * 256);
    mmEmuNes_GetPaletteR8G8B8A8(p->emu, p->palette);

    frame_timer_callback.Update = &__static_mmEmuEmulator_FrameTimerUnitUpdateSynchronize;
    frame_timer_callback.obj = p;

    mmFrameTask_SetName(&p->frame_task, "emulator");
    mmFrameTask_SetFrequency(&p->frame_task, p->frequency);
    mmFrameTask_SetBackground(&p->frame_task, MM_FRAME_TIMER_BACKGROUND_RUNING);
    mmFrameTask_SetCallback(&p->frame_task, &frame_timer_callback);

    nes_callback.MovieFinish = &__static_mmEmuEmulator_MovieFinish;
    nes_callback.ScreenMessage = &__static_mmEmuEmulator_ScreenMessage;
    nes_callback.obj = p;
    mmEmuNes_SetCallback(p->emu, &nes_callback);

    queue_callback.Handle = &__static_mmEmuEmulator_MessageQueueEvHandle;
    queue_callback.obj = p;
    mmMessageQueue_SetQueueCallback(&p->mq_ev, &queue_callback);

    queue_callback.Handle = &__static_mmEmuEmulator_MessageQueueNtHandle;
    queue_callback.obj = p;
    mmMessageQueue_SetQueueCallback(&p->mq_nt, &queue_callback);

    __static_mmEmuEmulator_HandleForCommonEvent(p);
}
MM_EXPORT_EMU void mmEmuEmulator_Destroy(struct mmEmuEmulator* p)
{
    mmEmuNes_Destroy(p->emu);
    mmFrameTask_Destroy(&p->frame_task);
    mmEmuEmulatorCallback_Destroy(&p->callback_q);

    mmRbtreeU32Vpt_Destroy(&p->rbtree_q);

    mmSpinlock_Destroy(&p->locker_rbtree_q);

    p->frequency = MM_EMU_EMULATOR_FREQUENCY_DEFAULT;
    p->rom_load = MM_FAILURE;
    p->background = MM_FALSE;
    p->background_runing = MM_FALSE;

    p->audio_buffer_idx = 0;
    mmStreambuf_Destroy(&p->audio_block);
    p->audio_block_size = 0;
    p->audio_frame_size = 0;
    p->audio_block_k = 0;
    p->audio_frame_index = 0;
    p->audio_queue_balance = 0;

    p->processor = (struct mmEmuEmulatorProcessor*)(&MM_EMU_EMULATOR_PROCESSOR_DEFAULT);

    mmMessageQueue_Destroy(&p->mq_ev);
    mmMessageQueue_Destroy(&p->mq_nt);

    p->emulator_state = MM_EMULATOR_NONE;

    p->state = MM_TS_CLOSED;

    p->u = NULL;

    mmFree(p->emu);
    p->emu = NULL;

    mmFree(p->palette);
    p->palette = NULL;
}

MM_EXPORT_EMU void mmEmuEmulator_SetRomPath(struct mmEmuEmulator* p, const char* szRomPath)
{
    mmEmuNes_SetRomPath(p->emu, szRomPath);
}
MM_EXPORT_EMU void mmEmuEmulator_SetRomName(struct mmEmuEmulator* p, const char* szRomName)
{
    mmEmuNes_SetRomName(p->emu, szRomName);
}
MM_EXPORT_EMU void mmEmuEmulator_SetWritablePath(struct mmEmuEmulator* p, const char* szWritablePath)
{
    mmEmuNes_SetWritablePath(p->emu, szWritablePath);
}
MM_EXPORT_EMU void mmEmuEmulator_SetResourceRoot(struct mmEmuEmulator* p, mmUInt32_t type_assets, const char* root_path, const char* root_base)
{
    mmEmuNes_SetResourceRoot(p->emu, type_assets, root_path, root_base);
}
MM_EXPORT_EMU void mmEmuEmulator_SetPaletteTableByte(struct mmEmuEmulator* p, const mmUInt32_t pal[64])
{
    mmEmuNes_SetPaletteTableByte(p->emu, pal);
    mmMemset(p->palette, 0, sizeof(mmUInt32_t) * 256);
    mmEmuNes_GetPaletteR8G8B8A8(p->emu, p->palette);
}

MM_EXPORT_EMU void mmEmuEmulator_SetTaskBackground(struct mmEmuEmulator* p, mmUInt8_t background)
{
    mmFrameTask_SetBackground(&p->frame_task, background);
}

MM_EXPORT_EMU mmUInt32_t mmEmuEmulator_GetRomLoadCode(const struct mmEmuEmulator* p)
{
    return p->rom_load;
}

MM_EXPORT_EMU void mmEmuEmulator_SetProcessor(struct mmEmuEmulator* p, struct mmEmuEmulatorProcessor* processor)
{
    if (NULL != processor)
    {
        p->processor = processor;
        mmEmuEmulatorProcessor_SetParent(p->processor, p);
    }
    else
    {
        p->processor = (struct mmEmuEmulatorProcessor*)(&MM_EMU_EMULATOR_PROCESSOR_DEFAULT);
    }
}

MM_EXPORT_EMU void mmEmuEmulator_SetContext(struct mmEmuEmulator* p, void* u)
{
    p->u = u;
}
MM_EXPORT_EMU void mmEmuEmulator_SetCallbackDefault(struct mmEmuEmulator* p, struct mmEmuEmulatorCallback* callback)
{
    assert(NULL != callback && "NULL != callback is invalid.");
    p->callback_q = *callback;
}
MM_EXPORT_EMU void mmEmuEmulator_SetHandle(struct mmEmuEmulator* p, mmUInt32_t mid, mmEmuEmulatorHandleFunc callback)
{
    mmSpinlock_Lock(&p->locker_rbtree_q);
    mmRbtreeU32Vpt_Set(&p->rbtree_q, mid, (void*)callback);
    mmSpinlock_Unlock(&p->locker_rbtree_q);
}
MM_EXPORT_EMU void mmEmuEmulator_ClearHandleRbtree(struct mmEmuEmulator* p)
{
    mmSpinlock_Lock(&p->locker_rbtree_q);
    mmRbtreeU32Vpt_Clear(&p->rbtree_q);
    mmSpinlock_Unlock(&p->locker_rbtree_q);
}

MM_EXPORT_EMU void mmEmuEmulator_SetVideoModeImmediately(struct mmEmuEmulator* p, mmInt_t nMode)
{
    if (MM_SUCCESS == p->rom_load)
    {
        mmEmuNes_SetVideoMode(p->emu, nMode);

        p->frequency = p->emu->config.nescfg.FrameRate;
        mmFrameTask_SetFrequency(&p->frame_task, p->frequency);
    }
}
MM_EXPORT_EMU void mmEmuEmulator_LoadRomImmediately(struct mmEmuEmulator* p)
{
    p->rom_load = mmEmuNes_LoadRom(p->emu);

    if (MM_SUCCESS == p->rom_load)
    {
        struct mmEmuEmulatorEvent emulator_event;
        
        mmInt_t nmode = mmEmuEmulator_GetVideoMode(p);
        mmInt_t ntype = mmEmuEmulator_GetExControllerType(p);
        
        int FrameRate = p->emu->config.nescfg.FrameRate;
        int nBits = p->emu->config.audio.nBits;
        int nRate = p->emu->config.audio.nRate;
        int nBufferSize = p->emu->config.audio.nBufferSize;
        int nChannels = 1;

        p->frequency = p->emu->config.nescfg.FrameRate;
        mmFrameTask_SetFrequency(&p->frame_task, p->frequency);

        p->audio_block_k = (double)(nBufferSize * nChannels * nBits) / (double)(8 * FrameRate);
        p->audio_block_size = (mmUInt32_t)(p->audio_block_k * nRate);
        p->audio_frame_size = p->audio_block_size;
        // we want to keep 2 frames (r - 2 + 1)
        // (r - 1) can be -1, we need to reserve MM_EMU_EMULATOR_MAX_BALANCE bits for safety.
        mmStreambuf_AlignedMemory(&p->audio_block, p->audio_block_size + MM_EMU_EMULATOR_AUDIO_MAX_BALANCE);

        mmEmuEmulatorEvent_Init(&emulator_event);

        emulator_event.p0 = nmode;
        emulator_event.p1 = 0;
        mmString_Assigns(&emulator_event.ps, "");
        mmEmuEmulator_CommandNt(p, MM_EMU_NT_VIDEOMODE, &emulator_event);

        emulator_event.p0 = ntype;
        emulator_event.p1 = 0;
        mmString_Assigns(&emulator_event.ps, "");
        mmEmuEmulator_CommandNt(p, MM_EMU_NT_EXCONTROLLER, &emulator_event);

        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
    else
    {
        p->frequency = MM_EMU_EMULATOR_FREQUENCY_DEFAULT;
        mmFrameTask_SetFrequency(&p->frame_task, p->frequency);
    }
}
MM_EXPORT_EMU void mmEmuEmulator_ScreenClearImmediately(struct mmEmuEmulator* p)
{
    if (MM_SUCCESS == p->rom_load)
    {
        mmEmuNes_ScreenClear(p->emu);

        mmEmuEmulator_UpdateFrame(p, 0);
    }
}
MM_EXPORT_EMU mmInt_t mmEmuEmulator_GetVideoMode(const struct mmEmuEmulator* p)
{
    return mmEmuNes_GetVideoMode(p->emu);
}
MM_EXPORT_EMU mmInt_t mmEmuEmulator_GetExControllerType(const struct mmEmuEmulator* p)
{
    struct mmEmuPad* pad = &p->emu->pad;
    return mmEmuPad_GetExController(pad);
}
MM_EXPORT_EMU void mmEmuEmulator_SetRapidSpeed(struct mmEmuEmulator* p, mmInt_t no, const mmWord_t speed[2])
{
    struct mmEmuNesConfig* _nes_config = &p->emu->config;
    struct mmEmuControllerConfig* _controller_config = &_nes_config->controller;
    // this is thread safe api.
    _controller_config->nRapid[no][0] = speed[0];
    _controller_config->nRapid[no][1] = speed[1];
}
MM_EXPORT_EMU void mmEmuEmulator_GetRapidSpeed(const struct mmEmuEmulator* p, mmInt_t no, mmWord_t speed[2])
{
    struct mmEmuNesConfig* _nes_config = &p->emu->config;
    struct mmEmuControllerConfig* _controller_config = &_nes_config->controller;
    // this is thread safe api.
    speed[0] = _controller_config->nRapid[no][0];
    speed[1] = _controller_config->nRapid[no][1];
}

MM_EXPORT_EMU void mmEmuEmulator_Play(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);
    
    p->emulator_state = MM_EMULATOR_PLAYING;

    mmFrameTask_SetFrequency(&p->frame_task, p->frequency);
    
    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_PLAY, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_Stop(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);
    
    p->emulator_state = MM_EMULATOR_STOPPED;

    mmFrameTask_SetFrequency(&p->frame_task, MM_EMU_EMULATOR_FREQUENCY_DEFAULT);
    
    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_STOP, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_Pause(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);
    
    p->emulator_state = MM_EMULATOR_PAUSED;
    
    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_PAUSE, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_Resume(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);
    
    p->emulator_state = MM_EMULATOR_PLAYING;

    mmFrameTask_SetFrequency(&p->frame_task, p->frequency);
    
    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_RESUME, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU mmInt_t mmEmuEmulator_GetState(const struct mmEmuEmulator* p)
{
    return p->emulator_state;
}

MM_EXPORT_EMU void mmEmuEmulator_KeyboardPressed(struct mmEmuEmulator* p, int hId)
{
    mmEmuNes_KeyboardPressed(p->emu, hId);
}
MM_EXPORT_EMU void mmEmuEmulator_KeyboardRelease(struct mmEmuEmulator* p, int hId)
{
    mmEmuNes_KeyboardRelease(p->emu, hId);
}
MM_EXPORT_EMU void mmEmuEmulator_MouseBegan(struct mmEmuEmulator* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuNes_MouseBegan(p->emu, x, y, button_mask);
}
MM_EXPORT_EMU void mmEmuEmulator_MouseMoved(struct mmEmuEmulator* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuNes_MouseMoved(p->emu, x, y, button_mask);
}
MM_EXPORT_EMU void mmEmuEmulator_MouseEnded(struct mmEmuEmulator* p, mmLong_t x, mmLong_t y, int button_mask)
{
    mmEmuNes_MouseEnded(p->emu, x, y, button_mask);
}
MM_EXPORT_EMU void mmEmuEmulator_EnterBackground(struct mmEmuEmulator* p)
{
    p->background = MM_TRUE;

    mmFrameTask_EnterBackground(&p->frame_task);
}
MM_EXPORT_EMU void mmEmuEmulator_EnterForeground(struct mmEmuEmulator* p)
{
    p->background = MM_FALSE;

    mmFrameTask_EnterForeground(&p->frame_task);
}

// For Manual bit controller Joypad.
MM_EXPORT_EMU void mmEmuEmulator_JoypadBitSetData(struct mmEmuEmulator* p, int n, mmWord_t data)
{
    mmEmuNes_JoypadBitSetData(p->emu, n, data);
}
MM_EXPORT_EMU mmWord_t mmEmuEmulator_JoypadBitGetData(struct mmEmuEmulator* p, int n)
{
    return mmEmuNes_JoypadBitGetData(p->emu, n);
}
MM_EXPORT_EMU void mmEmuEmulator_JoypadBitPressed(struct mmEmuEmulator* p, int n, int hId)
{
    mmEmuNes_JoypadBitPressed(p->emu, n, hId);
}
MM_EXPORT_EMU void mmEmuEmulator_JoypadBitRelease(struct mmEmuEmulator* p, int n, int hId)
{
    mmEmuNes_JoypadBitRelease(p->emu, n, hId);
}
// For Manual bit controller Nsf.
MM_EXPORT_EMU void mmEmuEmulator_NsfBitSetData(struct mmEmuEmulator* p, mmByte_t data)
{
    mmEmuNes_NsfBitSetData(p->emu, data);
}
MM_EXPORT_EMU mmByte_t mmEmuEmulator_NsfBitGetData(struct mmEmuEmulator* p)
{
    return mmEmuNes_NsfBitGetData(p->emu);
}
MM_EXPORT_EMU void mmEmuEmulator_NsfBitPressed(struct mmEmuEmulator* p, int hId)
{
    mmEmuNes_NsfBitPressed(p->emu, hId);
}
MM_EXPORT_EMU void mmEmuEmulator_NsfBitRelease(struct mmEmuEmulator* p, int hId)
{
    mmEmuNes_NsfBitRelease(p->emu, hId);
}

MM_EXPORT_EMU void mmEmuEmulator_UpdateEmulator(struct mmEmuEmulator* p)
{
    mmFrameTask_Update(&p->frame_task);
    mmFrameTask_Timewait(&p->frame_task);
}
MM_EXPORT_EMU void mmEmuEmulator_UpdateRenderer(struct mmEmuEmulator* p)
{
    // handle message.
    mmMessageQueue_ThreadHandle(&p->mq_nt);
}

MM_EXPORT_EMU void mmEmuEmulator_UpdateSynchronize(struct mmEmuEmulator* p, double interval)
{
    // handle message.
    mmMessageQueue_ThreadHandle(&p->mq_ev);

    if (MM_EMULATOR_PLAYING == p->emulator_state)
    {
        // emulate.
        if (MM_SUCCESS == p->rom_load)
        {
            if (MM_TRUE == p->background)
            {
                if (MM_TRUE == p->background_runing)
                {
                    mmEmuNes_EmulateFrame(p->emu, MM_FALSE);

                    mmEmuNes_PadSync(p->emu);

                    mmEmuNes_Movie(p->emu);

                    mmEmuEmulator_UpdateAudio(p, interval);
                }
            }
            else
            {
                mmEmuNes_EmulateFrame(p->emu, MM_TRUE);

                mmEmuNes_PadSync(p->emu);

                mmEmuNes_Movie(p->emu);

                mmEmuEmulator_UpdateAudio(p, interval);
                mmEmuEmulator_UpdateFrame(p, interval);
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuEmulator_UpdateAudio(struct mmEmuEmulator* p, double interval)
{
    do
    {
        struct mmEmuNes* nes = p->emu;
        struct mmEmuNesConfig* config = &p->emu->config;
        int nBufferSize = config->audio.nBufferSize;
        int FrameRate, r, k;
        mmInt_t nRate;

        if ((++p->audio_buffer_idx) < nBufferSize)
        {
            break;
        }

        FrameRate = config->nescfg.FrameRate;
        nRate = (mmInt_t)(p->audio_frame_size / p->audio_block_k);
        mmEmuNes_ApuProcess(p->emu, p->audio_block.buff, p->audio_frame_size, nRate);
        r = (p->processor->ProcessAudio)(p->processor, (mmByte_t*)p->audio_block.buff, p->audio_frame_size);
        mmEmuNes_WaveRecordOutput(nes, (mmByte_t*)p->audio_block.buff, p->audio_frame_size);
        p->audio_buffer_idx = 0;

        // 0 == r will quick balance.
        // 5 seconds balance once.
        if (0 != r && (++p->audio_frame_index) * nBufferSize < 5 * FrameRate)
        {
            break;
        }

        // use the last frame balance value. we want to keep 2 frames (r - 2 + 1).
        k = ((r - 1) + p->audio_queue_balance);
        // queue remaining feedback balance.
        p->audio_queue_balance = (k > -MM_EMU_EMULATOR_AUDIO_MAX_BALANCE) ? (k - 1) : (-MM_EMU_EMULATOR_AUDIO_MAX_BALANCE);
        p->audio_frame_size = p->audio_block_size - k;
        p->audio_frame_index = 0;

        // printf("r: %d balance: %d k: %d\n", r, p->audio_queue_balance, k);
    } while (0);
}
MM_EXPORT_EMU void mmEmuEmulator_UpdateFrame(struct mmEmuEmulator* p, double interval)
{
    mmByte_t* framebuffer = mmEmuNes_GetFramebuffer(p->emu);
    size_t framelength = MM_EMU_SCREEN_RENDER_W * MM_EMU_SCREEN_RENDER_H * sizeof(mmByte_t);
    mmEmuNes_ScreenBitBlockTransfer(p->emu);
    (p->processor->ProcessFrame)(p->processor, framebuffer, framelength);
}

MM_EXPORT_EMU void mmEmuEmulator_CommandEv(struct mmEmuEmulator* p, mmUInt32_t mid, struct mmEmuEmulatorEvent* pEvent)
{
    size_t length = 0;
    size_t offset = 0;
    struct mmPoolMessageElem* v = NULL;

    length = mmEmuEmulatorEvent_BufferSize(pEvent);

    v = mmMessageQueue_Overdraft(&p->mq_ev, length);

    v->mid = mid;
    v->obj = p;
    mmEmuEmulatorEvent_Encode(pEvent, &v->byte_buffer, &offset);

    mmMessageQueue_Repayment(&p->mq_ev, v);
}
MM_EXPORT_EMU void mmEmuEmulator_CommandNt(struct mmEmuEmulator* p, mmUInt32_t mid, struct mmEmuEmulatorEvent* pEvent)
{
    size_t length = 0;
    size_t offset = 0;
    struct mmPoolMessageElem* v = NULL;

    length = mmEmuEmulatorEvent_BufferSize(pEvent);

    v = mmMessageQueue_Overdraft(&p->mq_nt, length);

    v->mid = mid;
    v->obj = p;
    mmEmuEmulatorEvent_Encode(pEvent, &v->byte_buffer, &offset);

    mmMessageQueue_Repayment(&p->mq_nt, v);
}

MM_EXPORT_EMU void mmEmuEmulator_SetVideoMode(struct mmEmuEmulator* p, mmInt_t nMode)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = nMode;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_VIDEOMODE, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_LoadRom(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_LOADROM, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_ScreenClear(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_SCREENCLEAR, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_HardwareReset(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_HWRESET, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_SoftwareReset(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_SWRESET, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_SetMessageString(struct mmEmuEmulator* p, const char* message)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == message ? "" : message);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_SETMESSAGESTRING, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_StateLoad(struct mmEmuEmulator* p, const char* szFName)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == szFName ? "" : szFName);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_STATE_LOAD, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_StateSave(struct mmEmuEmulator* p, const char* szFName)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == szFName ? "" : szFName);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_STATE_SAVE, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_DiskCommand(struct mmEmuEmulator* p, mmInt_t cmd)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = cmd;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_DISK_COMMAND, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_ExController(struct mmEmuEmulator* p, mmInt_t param)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = param;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_EXCONTROLLER, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_SoundMute(struct mmEmuEmulator* p, mmInt_t param)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = param;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_SOUND_MUTE, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_Snapshot(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_SNAPSHOT, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU mmBool_t mmEmuEmulator_IsMoviePlay(const struct mmEmuEmulator* p)
{
    // this api is thread safe.
    return mmEmuNes_IsMoviePlay(p->emu);
}
MM_EXPORT_EMU mmBool_t mmEmuEmulator_IsMovieRec(const struct mmEmuEmulator* p)
{
    // this api is thread safe.
    return mmEmuNes_IsMovieRec(p->emu);
}
MM_EXPORT_EMU void mmEmuEmulator_MoviePlay(struct mmEmuEmulator* p, const char* szFName)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == szFName ? "" : szFName);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_MOVIE_PLAY, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_MovieRec(struct mmEmuEmulator* p, const char* szFName)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == szFName ? "" : szFName);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_MOVIE_REC, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_MovieRecAppend(struct mmEmuEmulator* p, const char* szFName)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == szFName ? "" : szFName);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_MOVIE_RECAPPEND, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_MovieStop(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_MOVIE_STOP, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_WaveRecStart(struct mmEmuEmulator* p, const char* szFName)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == szFName ? "" : szFName);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_WAVEREC_START, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_WaveRecStop(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_WAVEREC_STOP, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_TapePlay(struct mmEmuEmulator* p, const char* szFName)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == szFName ? "" : szFName);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_TAPE_PLAY, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_TapeRec(struct mmEmuEmulator* p, const char* szFName)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == szFName ? "" : szFName);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_TAPE_REC, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_TapeStop(struct mmEmuEmulator* p)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_TAPE_STOP, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_SetBarcodeData(struct mmEmuEmulator* p, mmByte_t* code, mmInt_t len)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = len;
    emulator_event.p1 = 0;
    mmString_Assignsn(&emulator_event.ps, (const char*)code, len);

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_BARCODE, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
MM_EXPORT_EMU void mmEmuEmulator_SetTurboFileBank(struct mmEmuEmulator* p, mmInt_t bank)
{
    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = bank;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandEv(p, MM_EMU_RQ_TURBOFILE, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}

MM_EXPORT_EMU void mmEmuEmulator_PollWait(struct mmEmuEmulator* p)
{
    (p->processor->ThreadEnter)(p->processor);

    while (MM_TS_MOTION == p->state)
    {
        mmEmuEmulator_UpdateEmulator(p);
    }

    (p->processor->ThreadLeave)(p->processor);
}

MM_EXPORT_EMU void mmEmuEmulator_Start(struct mmEmuEmulator* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->timer_thread, NULL, &__static_mmEmuEmulator_UpdateThread, p);
}
MM_EXPORT_EMU void mmEmuEmulator_Interrupt(struct mmEmuEmulator* p)
{
    p->state = MM_TS_CLOSED;
    mmFrameTask_Interrupt(&p->frame_task);
}
MM_EXPORT_EMU void mmEmuEmulator_Shutdown(struct mmEmuEmulator* p)
{
    p->state = MM_TS_FINISH;
    mmFrameTask_Shutdown(&p->frame_task);
}
MM_EXPORT_EMU void mmEmuEmulator_Join(struct mmEmuEmulator* p)
{
    pthread_join(p->timer_thread, NULL);
}

static void __static_mmEmuEmulator_MovieFinish(struct mmEmuNes* obj)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj->callback.obj);

    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, "");

    mmEmuEmulator_CommandNt(emulator, MM_EMU_NT_MOVIE_FINISH, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
static void __static_mmEmuEmulator_ScreenMessage(struct mmEmuNes* obj, const char* message)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj->callback.obj);

    struct mmEmuEmulatorEvent emulator_event;

    mmEmuEmulatorEvent_Init(&emulator_event);

    emulator_event.p0 = 0;
    emulator_event.p1 = 0;
    mmString_Assigns(&emulator_event.ps, NULL == message ? "" : message);

    mmEmuEmulator_CommandNt(emulator, MM_EMU_NT_SCREENMESSAGE, &emulator_event);

    mmEmuEmulatorEvent_Destroy(&emulator_event);
}

static void* __static_mmEmuEmulator_UpdateThread(void* arg)
{
    struct mmEmuEmulator* p = (struct mmEmuEmulator*)(arg);
    mmEmuEmulator_PollWait(p);
    return NULL;
}
static void __static_mmEmuEmulator_FrameTimerUnitUpdateSynchronize(void* obj, double interval)
{
    struct mmFrameTimer* unit = (struct mmFrameTimer*)(obj);
    struct mmEmuEmulator* p = (struct mmEmuEmulator*)(unit->callback.obj);
    mmEmuEmulator_UpdateSynchronize(p, interval);
    //mm_frame_stats_logger_average(&unit->status, unit->name.s);
}
static void __static_mmEmuEmulator_DiskCommand(struct mmEmuEmulator* p, mmByte_t cmd)
{
    struct mmEmuNes* nes = p->emu;

    mmInt_t DiskNo = mmEmuRom_GetDiskNo(&nes->rom);

    switch (cmd)
    {
    case 0: // Eject
        if (DiskNo > 0)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_EJECT);
            mmEmuNes_SetMessageString(nes, "Disk Eject.");
        }
        break;
    case 1: // Disk0 SideA
        if (DiskNo > 0)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_0A);
            mmEmuNes_SetMessageString(nes, "Change Disk1 SideA.");
        }
        break;
    case 2: // Disk0 SideB
        if (DiskNo > 1)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_0B);
            mmEmuNes_SetMessageString(nes, "Change Disk1 SideB.");
        }
        break;
    case 3: // Disk1 SideA
        if (DiskNo > 2)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_1A);
            mmEmuNes_SetMessageString(nes, "Change Disk2 SideA.");
        }
        break;
    case 4: // Disk1 SideB
        if (DiskNo > 3)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_1B);
            mmEmuNes_SetMessageString(nes, "Change Disk2 SideB.");
        }
    case 5: // Disk1 SideA
        if (DiskNo > 4)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_2A);
            mmEmuNes_SetMessageString(nes, "Change Disk3 SideA.");
        }
        break;
    case 6: // Disk1 SideB
        if (DiskNo > 5)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_2B);
            mmEmuNes_SetMessageString(nes, "Change Disk3 SideB.");
        }
    case 7: // Disk1 SideA
        if (DiskNo > 6)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_3A);
            mmEmuNes_SetMessageString(nes, "Change Disk4 SideA.");
        }
        break;
    case 8: // Disk1 SideB
        if (DiskNo > 7)
        {
            mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_3B);
            mmEmuNes_SetMessageString(nes, "Change Disk4 SideB.");
        }
        break;
    default:
        break;
    }
}
static void __static_mmEmuEmulator_MessageQueueEvHandle(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X size:%" PRIuPTR " not handle.", __FUNCTION__, __LINE__, mid, byte_buffer->length);
}
static void __static_mmEmuEmulator_MessageQueueNtHandle(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    
    size_t offset = 0;
    struct mmEmuEmulatorEvent emulator_event;
    //
    mmEmuEmulatorHandleFunc handle = NULL;
    
    mmEmuEmulatorEvent_Init(&emulator_event);
    mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
    
    mmSpinlock_Lock(&emulator->locker_rbtree_q);
    handle = (mmEmuEmulatorHandleFunc)mmRbtreeU32Vpt_Get(&emulator->rbtree_q, mid);
    mmSpinlock_Unlock(&emulator->locker_rbtree_q);
    
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(obj, u, mid, &emulator_event);
    }
    else
    {
        assert(NULL != emulator->callback_q.Handle && "emulator->callback_q.Handle is a null.");
        (*(emulator->callback_q.Handle))(obj, u, mid, &emulator_event);
    }
    
    mmEmuEmulatorEvent_Destroy(&emulator_event);
}
static void __static_mmEmuEmulator_HandleForCommonEvent(struct mmEmuEmulator* p)
{
    // mq_ev
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_EXIT, &__static_mmEmuEmulator_Message_MM_EMU_RQ_EXIT);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_NONE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_NONE);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_INITIAL, &__static_mmEmuEmulator_Message_MM_EMU_RQ_INITIAL);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_PLAY, &__static_mmEmuEmulator_Message_MM_EMU_RQ_PLAY);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_STOP, &__static_mmEmuEmulator_Message_MM_EMU_RQ_STOP);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_PAUSE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_PAUSE);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_RESUME, &__static_mmEmuEmulator_Message_MM_EMU_RQ_RESUME);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_VIDEOMODE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_VIDEOMODE);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_LOADROM, &__static_mmEmuEmulator_Message_MM_EMU_RQ_LOADROM);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_SCREENCLEAR, &__static_mmEmuEmulator_Message_MM_EMU_RQ_SCREENCLEAR);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_SETMESSAGESTRING, &__static_mmEmuEmulator_Message_MM_EMU_RQ_SETMESSAGESTRING);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_FULLSCREEN_GDI, &__static_mmEmuEmulator_Message_MM_EMU_RQ_FULLSCREEN_GDI);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_EMUPAUSE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_EMUPAUSE);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_ONEFRAME, &__static_mmEmuEmulator_Message_MM_EMU_RQ_ONEFRAME);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_THROTTLE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_THROTTLE);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_FRAMESKIP_AUTO, &__static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_AUTO);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_FRAMESKIP_UP, &__static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_UP);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_FRAMESKIP_DOWN, &__static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_DOWN);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_HWRESET, &__static_mmEmuEmulator_Message_MM_EMU_RQ_HWRESET);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_SWRESET, &__static_mmEmuEmulator_Message_MM_EMU_RQ_SWRESET);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_NETPLAY_START, &__static_mmEmuEmulator_Message_MM_EMU_RQ_NETPLAY_START);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_STATE_LOAD, &__static_mmEmuEmulator_Message_MM_EMU_RQ_STATE_LOAD);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_STATE_SAVE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_STATE_SAVE);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_DISK_COMMAND, &__static_mmEmuEmulator_Message_MM_EMU_RQ_DISK_COMMAND);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_EXCONTROLLER, &__static_mmEmuEmulator_Message_MM_EMU_RQ_EXCONTROLLER);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_SOUND_MUTE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_SOUND_MUTE);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_SNAPSHOT, &__static_mmEmuEmulator_Message_MM_EMU_RQ_SNAPSHOT);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_MOVIE_PLAY, &__static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_PLAY);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_MOVIE_REC, &__static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_REC);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_MOVIE_RECAPPEND, &__static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_RECAPPEND);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_MOVIE_STOP, &__static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_STOP);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_WAVEREC_START, &__static_mmEmuEmulator_Message_MM_EMU_RQ_WAVEREC_START);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_WAVEREC_STOP, &__static_mmEmuEmulator_Message_MM_EMU_RQ_WAVEREC_STOP);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_TAPE_PLAY, &__static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_PLAY);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_TAPE_REC, &__static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_REC);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_TAPE_STOP, &__static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_STOP);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_BARCODE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_BARCODE);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_TURBOFILE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_TURBOFILE);

    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_DEBUG_RUN, &__static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_RUN);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_DEBUG_BRAKE, &__static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_BRAKE);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_DEBUG_STEP, &__static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_STEP);
    mmMessageQueue_SetHandle(&p->mq_ev, MM_EMU_RQ_DEBUG_COMMAND, &__static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_COMMAND);
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_EXIT(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_EXIT, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_NONE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_NONE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_INITIAL(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_INITIAL, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_PLAY(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_PLAY, &emulator_event);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_NT_PLAY, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_STOP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_STOP, &emulator_event);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_NT_STOP, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_PAUSE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_PAUSE, &emulator_event);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_NT_STOP, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_RESUME(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_RESUME, &emulator_event);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_NT_PLAY, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_VIDEOMODE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_SetVideoModeImmediately(emulator, emulator_event.p0);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_VIDEOMODE, &emulator_event);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_NT_VIDEOMODE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_LOADROM(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    // if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_LoadRomImmediately(emulator);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_LOADROM, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SCREENCLEAR(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    // if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_ScreenClearImmediately(emulator);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_SCREENCLEAR, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SETMESSAGESTRING(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_SetMessageString(nes, mmString_CStr(&emulator_event.ps));
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_SETMESSAGESTRING, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_FULLSCREEN_GDI(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_FULLSCREEN_GDI, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_EMUPAUSE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_EMUPAUSE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_ONEFRAME(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_ONEFRAME, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_THROTTLE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_THROTTLE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_AUTO(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_FRAMESKIP_AUTO, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_UP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_FRAMESKIP_UP, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_FRAMESKIP_DOWN(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_FRAMESKIP_DOWN, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_HWRESET(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_HWRESET);
        mmEmuNes_SetMessageString(nes, "Hardware reset.");
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_HWRESET, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SWRESET(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_SWRESET);
        mmEmuNes_SetMessageString(nes, "Software reset.");
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_SWRESET, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_NETPLAY_START(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_NETPLAY_START, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_STATE_LOAD(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmEmuAssets* assets = &nes->assets;
        struct mmString szStateFile;

        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmString_Init(&szStateFile);

        mmEmuAssets_GetStateFile(assets, &szStateFile, mmString_CStr(&emulator_event.ps));

        mmLogger_LogD(gLogger, "%s %d StateFile: %s", __FUNCTION__, __LINE__, mmString_CStr(&szStateFile));

        if (MM_SUCCESS == mmEmuNes_LoadState(nes, mmString_CStr(&szStateFile)))
        {
            char szString[32] = { 0 };
            if ((mmUInt32_t)(-1) == emulator_event.p0)
            {
                mmSprintf(szString, "State load success.");
            }
            else
            {
                mmSprintf(szString, "State load #%d success.", emulator_event.p0);
            }
            mmEmuNes_SetMessageString(nes, szString);
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "State load failure.");
        }
        
        mmString_Destroy(&szStateFile);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_STATE_LOAD, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_STATE_SAVE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmEmuAssets* assets = &nes->assets;
        struct mmString szStateFile;

        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmString_Init(&szStateFile);

        mmEmuAssets_GetStateFile(assets, &szStateFile, mmString_CStr(&emulator_event.ps));

        mmLogger_LogD(gLogger, "%s %d StateFile: %s", __FUNCTION__, __LINE__, mmString_CStr(&szStateFile));

        if (MM_SUCCESS == mmEmuNes_SaveState(nes, mmString_CStr(&szStateFile)))
        {
            char szString[32] = { 0 };
            if ((mmUInt32_t)(-1) == emulator_event.p0)
            {
                mmSprintf(szString, "State save success.");
            }
            else
            {
                mmSprintf(szString, "State save #%d success.", emulator_event.p0);
            }
            mmEmuNes_SetMessageString(nes, szString);
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "State save failure.");
        }
        
        mmString_Destroy(&szStateFile);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_STATE_SAVE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DISK_COMMAND(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        __static_mmEmuEmulator_DiskCommand(emulator, emulator_event.p0);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_DISK_COMMAND, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_EXCONTROLLER(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_CommandParam(nes, MM_EMU_NES_NESCMD_EXCONTROLLER, emulator_event.p0);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_EXCONTROLLER, &emulator_event);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_NT_EXCONTROLLER, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SOUND_MUTE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        char szString[32] = { 0 };
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        if (mmEmuNes_CommandParam(nes, MM_EMU_NES_NESCMD_SOUND_MUTE, emulator_event.p0))
        {
            mmSprintf(szString, "%s Enable.", g_lpSoundMuteStringTable[emulator_event.p0]);
        }
        else
        {
            mmSprintf(szString, "%s Mute.", g_lpSoundMuteStringTable[emulator_event.p0]);
        }
        mmEmuNes_SetMessageString(nes, szString);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_SOUND_MUTE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_SNAPSHOT(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        if (MM_SUCCESS == mmEmuNes_Snapshot(nes))
        {
            mmEmuNes_SetMessageString(nes, "Snap shot.");
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "Snap shot failure.");
        }
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_SNAPSHOT, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_PLAY(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmEmuAssets* assets = &nes->assets;
        struct mmString szMovieFile;

        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmString_Init(&szMovieFile);

        mmEmuAssets_GetMovieFile(assets, &szMovieFile, mmString_CStr(&emulator_event.ps));

        mmLogger_LogD(gLogger, "%s %d MovieFile: %s", __FUNCTION__, __LINE__, mmString_CStr(&szMovieFile));

        if (MM_SUCCESS == mmEmuNes_MoviePlay(nes, mmString_CStr(&szMovieFile)))
        {
            mmEmuNes_SetMessageString(nes, "Movie replay.");
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "Movie replay failure.");
        }
        mmString_Destroy(&szMovieFile);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_MOVIE_PLAY, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_REC(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmEmuAssets* assets = &nes->assets;
        struct mmString szMovieFile;

        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmString_Init(&szMovieFile);

        mmEmuAssets_GetMovieFile(assets, &szMovieFile, mmString_CStr(&emulator_event.ps));

        mmLogger_LogD(gLogger, "%s %d MovieFile: %s", __FUNCTION__, __LINE__, mmString_CStr(&szMovieFile));

        if (MM_SUCCESS == mmEmuNes_MovieRec(nes, mmString_CStr(&szMovieFile)))
        {
            mmEmuNes_SetMessageString(nes, "Movie record.");
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "Movie record failure.");
        }
        mmString_Destroy(&szMovieFile);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_MOVIE_REC, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_RECAPPEND(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmEmuAssets* assets = &nes->assets;
        struct mmString szMovieFile;

        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmString_Init(&szMovieFile);

        mmEmuAssets_GetMovieFile(assets, &szMovieFile, mmString_CStr(&emulator_event.ps));

        mmLogger_LogD(gLogger, "%s %d MovieFile: %s", __FUNCTION__, __LINE__, mmString_CStr(&szMovieFile));

        if (MM_SUCCESS == mmEmuNes_MovieRecAppend(nes, mmString_CStr(&szMovieFile)))
        {
            mmEmuNes_SetMessageString(nes, "Movie record append.");
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "Movie record append failure.");
        }
        mmString_Destroy(&szMovieFile);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_MOVIE_RECAPPEND, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_MOVIE_STOP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        if (MM_SUCCESS == mmEmuNes_MovieStop(nes))
        {
            mmEmuNes_SetMessageString(nes, "Movie stop.");
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "Movie stop failure.");
        }
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_MOVIE_STOP, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_WAVEREC_START(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_WaveRecordStart(nes, mmString_CStr(&emulator_event.ps));
        mmEmuNes_SetMessageString(nes, "Wave recording start.");
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_WAVEREC_START, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_WAVEREC_STOP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_WaveRecordStop(nes);
        mmEmuNes_SetMessageString(nes, "Wave recording stop.");
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_WAVEREC_STOP, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_PLAY(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmEmuAssets* assets = &nes->assets;
        struct mmString szTapeFile;

        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmString_Init(&szTapeFile);

        mmEmuAssets_GetTapeFile(assets, &szTapeFile);

        mmLogger_LogD(gLogger, "%s %d TapeFile: %s", __FUNCTION__, __LINE__, mmString_CStr(&szTapeFile));

        if (MM_SUCCESS == mmEmuNes_TapePlay(nes, mmString_CStr(&szTapeFile)))
        {
            mmEmuNes_SetMessageString(nes, "Tape play.");
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "Tape play failure.");
        }
        mmString_Destroy(&szTapeFile);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_TAPE_PLAY, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_REC(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmEmuAssets* assets = &nes->assets;
        struct mmString szTapeFile;

        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmString_Init(&szTapeFile);

        mmEmuAssets_GetTapeFile(assets, &szTapeFile);

        mmLogger_LogD(gLogger, "%s %d TapeFile: %s", __FUNCTION__, __LINE__, mmString_CStr(&szTapeFile));

        if (MM_SUCCESS == mmEmuNes_TapeRec(nes, mmString_CStr(&emulator_event.ps)))
        {
            mmEmuNes_SetMessageString(nes, "Tape record.");
        }
        else
        {
            mmEmuNes_SetMessageString(nes, "Tape record failure.");
        }
        mmString_Destroy(&szTapeFile);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_TAPE_REC, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_TAPE_STOP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_TapeStop(nes);
        mmEmuNes_SetMessageString(nes, "Tape stop.");
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_TAPE_STOP, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_BARCODE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_SetBarcodeData(nes, (mmByte_t*)mmString_CStr(&emulator_event.ps), (mmInt_t)emulator_event.p0);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_BARCODE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_TURBOFILE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuNes_SetTurboFileBank(nes, (mmInt_t)emulator_event.p0);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_TURBOFILE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_RUN(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_DEBUG_RUN, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_BRAKE(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_DEBUG_BRAKE, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_STEP(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_DEBUG_STEP, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}
static void __static_mmEmuEmulator_Message_MM_EMU_RQ_DEBUG_COMMAND(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmEmuEmulator* emulator = (struct mmEmuEmulator*)(obj);
    // struct mmEmuNes* nes = emulator->emu;
    if (MM_SUCCESS == emulator->rom_load)
    {
        size_t offset = 0;
        struct mmEmuEmulatorEvent emulator_event;
        mmEmuEmulatorEvent_Init(&emulator_event);
        mmEmuEmulatorEvent_Decode(&emulator_event, byte_buffer, &offset);
        
        mmEmuEmulator_CommandNt(emulator, MM_EMU_RS_DEBUG_COMMAND, &emulator_event);
        
        mmEmuEmulatorEvent_Destroy(&emulator_event);
    }
}

