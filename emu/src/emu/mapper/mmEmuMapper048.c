/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper048.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper048_Init(struct mmEmuMapper048* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper048_Reset;
    p->super.Write = &mmEmuMapper048_Write;
    //p->super.Read = &mmEmuMapper048_Read;
    //p->super.ReadLow = &mmEmuMapper048_ReadLow;
    //p->super.WriteLow = &mmEmuMapper048_WriteLow;
    //p->super.ExRead = &mmEmuMapper048_ExRead;
    //p->super.ExWrite = &mmEmuMapper048_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper048_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper048_ExCmdWrite;
    p->super.HSync = &mmEmuMapper048_HSync;
    //p->super.VSync = &mmEmuMapper048_VSync;
    //p->super.Clock = &mmEmuMapper048_Clock;
    //p->super.PPULatch = &mmEmuMapper048_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper048_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper048_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper048_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper048_IsStateSave;
    p->super.SaveState = &mmEmuMapper048_SaveState;
    p->super.LoadState = &mmEmuMapper048_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper048_Destroy(struct mmEmuMapper048* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper048_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper048* p = (struct mmEmuMapper048*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmUInt32_t  crc = 0;

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    //crc = mmEmuRom_GetPROMCRC(&nes->rom);
    //if (crc == 0x547e6cc1)                // Flintstones - The Rescue of Dino & Hoppy(J)
    //{
    //  mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    //}
}
MM_EXPORT_EMU void mmEmuMapper048_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper048* p = (struct mmEmuMapper048*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        if (!p->reg)
        {
            if (data & 0x40)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0x8001:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;

    case 0x8002:
        mmEmuMmu_SetVROM2KBank(mmu, 0, data);
        break;
    case 0x8003:
        mmEmuMmu_SetVROM2KBank(mmu, 2, data);
        break;
    case 0xA000:
        mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        break;
    case 0xA001:
        mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        break;
    case 0xA002:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        break;
    case 0xA003:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        break;

    case 0xC000:
        p->irq_counter = data;
        p->irq_enable = 0;
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xC001:
        p->irq_counter = data;
        p->irq_enable = 1;
        //p->irq_enable = data & 0x01;
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xC002:
        break;
    case 0xC003:
        break;

    case 0xE000:
        if (data & 0x40)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        p->reg = 1;
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper048_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper048* p = (struct mmEmuMapper048*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {

        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (p->irq_counter == 0xFF)
                {
                    //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                    //mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_TRIGGER2);
                }
                p->irq_counter++;
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper048_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper048_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper048* p = (struct mmEmuMapper048*)(super);

    buffer[0] = p->reg;
    buffer[1] = p->irq_enable;
    buffer[2] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper048_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper048* p = (struct mmEmuMapper048*)(super);

    p->reg = buffer[0];
    p->irq_enable = buffer[1];
    p->irq_counter = buffer[2];
}

static struct mmEmuMapper* __static_mmEmuMapper048_Produce(void)
{
    struct mmEmuMapper048* p = (struct mmEmuMapper048*)mmMalloc(sizeof(struct mmEmuMapper048));
    mmEmuMapper048_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper048_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper048* p = (struct mmEmuMapper048*)(m);
    mmEmuMapper048_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER048 =
{
    &__static_mmEmuMapper048_Produce,
    &__static_mmEmuMapper048_Recycle,
};
