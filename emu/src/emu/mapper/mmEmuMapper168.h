/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper168_h__
#define __mmEmuMapper168_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper168                                                            //
//----------------------------------------------------------------------//
struct mmEmuMapper168
{
    struct mmEmuMapper super;

    mmByte_t    reg5000, reg5200, reg5300;
    mmByte_t    PPU_SW;
    mmByte_t    Rom_Type;
};

MM_EXPORT_EMU void mmEmuMapper168_Init(struct mmEmuMapper168* p);
MM_EXPORT_EMU void mmEmuMapper168_Destroy(struct mmEmuMapper168* p);

MM_EXPORT_EMU void mmEmuMapper168_SetBankCPU(struct mmEmuMapper168* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper168_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU mmByte_t mmEmuMapper168_ReadLow(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper168_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper168_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper168_PPUExtLatch(struct mmEmuMapper* super, mmWord_t ntbladr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper168_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper168_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper168_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER168;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper168_h__
