/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApu.h"
#include "mmEmuNes.h"
#include "mmEmuCpu.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

// Volume adjust
// Internal sounds
#define RECTANGLE_VOL   (0x0F0)
#define TRIANGLE_VOL    (0x130)
#define NOISE_VOL       (0x0C0)
#define DPCM_VOL        (0x0F0)
// Extra sounds
#define VRC6_VOL        (0x0F0)
#define VRC7_VOL        (0x130)
#define FDS_VOL         (0x0F0)
#define MMC5_VOL        (0x0F0)
#define N106_VOL        (0x088)
#define FME7_VOL        (0x130)

MM_EXPORT_EMU void mmEmuApuQueue_Init(struct mmEmuApuQueue* p)
{
    p->rdptr = 0;
    p->wrptr = 0;
    p->data = NULL;
    p->data = (struct mmEmuApuQueueData*)mmMalloc(sizeof(struct mmEmuApuQueueData) * MM_EMU_APU_QUEUE_LENGTH);
    //
    mmMemset(p->data, 0, sizeof(struct mmEmuApuQueueData) * MM_EMU_APU_QUEUE_LENGTH);
}
MM_EXPORT_EMU void mmEmuApuQueue_Destroy(struct mmEmuApuQueue* p)
{
    mmFree(p->data);

    p->rdptr = 0;
    p->wrptr = 0;
    p->data = NULL;
}

MM_EXPORT_EMU void mmEmuApuQueue_Clear(struct mmEmuApuQueue* p)
{
    p->rdptr = 0;
    p->wrptr = 0;
    mmMemset(p->data, 0, sizeof(struct mmEmuApuQueueData) * MM_EMU_APU_QUEUE_LENGTH);
}
MM_EXPORT_EMU void mmEmuApuQueue_Reset(struct mmEmuApuQueue* p)
{
    p->rdptr = 0;
    p->wrptr = 0;
    mmMemset(p->data, 0, sizeof(struct mmEmuApuQueueData) * MM_EMU_APU_QUEUE_LENGTH);
}

MM_EXPORT_EMU void mmEmuApuQueue_Set(struct mmEmuApuQueue* p, mmUInt64_t writetime, mmWord_t addr, mmByte_t data)
{
    p->data[p->wrptr].time = writetime;
    p->data[p->wrptr].addr = addr;
    p->data[p->wrptr].data = data;
    p->wrptr++;
    p->wrptr &= MM_EMU_APU_QUEUE_LENGTH - 1;
    if (p->wrptr == p->rdptr)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogD(gLogger, "%s %d queue overflow.", __FUNCTION__, __LINE__);
    }
}
MM_EXPORT_EMU mmBool_t mmEmuApuQueue_Get(struct mmEmuApuQueue* p, mmUInt64_t writetime, struct mmEmuApuQueueData* ret)
{
    if (p->wrptr == p->rdptr)
    {
        return MM_FALSE;
    }
    if (p->data[p->rdptr].time <= writetime)
    {
        *ret = p->data[p->rdptr];
        p->rdptr++;
        p->rdptr &= MM_EMU_APU_QUEUE_LENGTH - 1;
        return MM_TRUE;
    }
    return MM_FALSE;
}

MM_EXPORT_EMU void mmEmuApu_Init(struct mmEmuApu* p)
{
    p->nes = NULL;

    mmEmuApuInternal_Init(&p->internal);
    mmEmuApuVrc6_Init(&p->vrc6);
    mmEmuApuVrc7_Init(&p->vrc7);
    mmEmuApuMmc5_Init(&p->mmc5);
    mmEmuApuFds_Init(&p->fds);
    mmEmuApuN106_Init(&p->n106);
    mmEmuApuFme7_Init(&p->fme7);

    mmEmuApuQueue_Init(&p->queue);
    mmEmuApuQueue_Init(&p->exqueue);

    p->exsound_select = 0;

    p->elapsed_time = 0;

    p->last_data = 0;
    p->last_diff = 0;
    mmMemset(p->lowpass_filter, 0, sizeof(mmInt_t) * 4);

    mmMemset(p->m_bMute, 0, sizeof(mmBool_t) * 16);

    mmMemset(p->m_SoundBuffer, 0, sizeof(mmShort_t) * 0x100);
    //
    mmEmuApu_SetMute(p, MM_TRUE);
}
MM_EXPORT_EMU void mmEmuApu_Destroy(struct mmEmuApu* p)
{
    p->nes = NULL;

    mmEmuApuInternal_Destroy(&p->internal);
    mmEmuApuVrc6_Destroy(&p->vrc6);
    mmEmuApuVrc7_Destroy(&p->vrc7);
    mmEmuApuMmc5_Destroy(&p->mmc5);
    mmEmuApuFds_Destroy(&p->fds);
    mmEmuApuN106_Destroy(&p->n106);
    mmEmuApuFme7_Destroy(&p->fme7);

    mmEmuApuQueue_Destroy(&p->queue);
    mmEmuApuQueue_Destroy(&p->exqueue);

    p->exsound_select = 0;

    p->elapsed_time = 0;

    p->last_data = 0;
    p->last_diff = 0;
    mmMemset(p->lowpass_filter, 0, sizeof(mmInt_t) * 4);

    mmMemset(p->m_bMute, 0, sizeof(mmBool_t) * 16);

    mmMemset(p->m_SoundBuffer, 0, sizeof(mmShort_t) * 0x100);
}

MM_EXPORT_EMU void mmEmuApu_SetParent(struct mmEmuApu* p, struct mmEmuNes* parent)
{
    p->nes = parent;

    mmEmuApuInterface_SetParent(&p->internal.super, p->nes);
    mmEmuApuInterface_SetParent(&p->vrc6.super, p->nes);
    mmEmuApuInterface_SetParent(&p->vrc7.super, p->nes);
    mmEmuApuInterface_SetParent(&p->mmc5.super, p->nes);
    mmEmuApuInterface_SetParent(&p->fds.super, p->nes);
    mmEmuApuInterface_SetParent(&p->n106.super, p->nes);
    mmEmuApuInterface_SetParent(&p->fme7.super, p->nes);
}

MM_EXPORT_EMU void mmEmuApu_SoundSetup(struct mmEmuApu* p)
{
    double fClock = (double)p->nes->config.nescfg.CpuClock;
    mmInt_t nRate = (mmInt_t)p->nes->config.audio.nRate;

    ((p->internal.super.Setup))(&p->internal.super, fClock, nRate);
    ((p->vrc6.super.Setup))(&p->vrc6.super, fClock, nRate);
    ((p->vrc7.super.Setup))(&p->vrc7.super, fClock, nRate);
    ((p->mmc5.super.Setup))(&p->mmc5.super, fClock, nRate);
    ((p->fds.super.Setup))(&p->fds.super, fClock, nRate);
    ((p->n106.super.Setup))(&p->n106.super, fClock, nRate);
    ((p->fme7.super.Setup))(&p->fme7.super, fClock, nRate);
}
MM_EXPORT_EMU void mmEmuApu_SelectExSound(struct mmEmuApu* p, mmByte_t data)
{
    p->exsound_select = data;
}
MM_EXPORT_EMU mmBool_t mmEmuApu_SetChannelMute(struct mmEmuApu* p, mmInt_t nCH)
{
    p->m_bMute[nCH] = !p->m_bMute[nCH];
    return p->m_bMute[nCH];
}
MM_EXPORT_EMU void mmEmuApu_SetMute(struct mmEmuApu* p, mmBool_t v)
{
    mmInt_t i = 0;

    for (i = 0; i < 16; i++)
    {
        p->m_bMute[i] = v;
    }
}
MM_EXPORT_EMU void mmEmuApu_QueueClear(struct mmEmuApu* p)
{
    mmEmuApuQueue_Clear(&p->queue);
    mmEmuApuQueue_Clear(&p->exqueue);
}
MM_EXPORT_EMU void mmEmuApu_Reset(struct mmEmuApu* p)
{
    double fClock = (double)p->nes->config.nescfg.CpuClock;
    mmInt_t nRate = (mmInt_t)p->nes->config.audio.nRate;

    mmEmuApuQueue_Reset(&p->queue);
    mmEmuApuQueue_Reset(&p->exqueue);

    p->elapsed_time = 0;

    ((p->internal.super.Reset))(&p->internal.super, fClock, nRate);
    ((p->vrc6.super.Reset))(&p->vrc6.super, fClock, nRate);
    ((p->vrc7.super.Reset))(&p->vrc7.super, fClock, nRate);
    ((p->mmc5.super.Reset))(&p->mmc5.super, fClock, nRate);
    ((p->fds.super.Reset))(&p->fds.super, fClock, nRate);
    ((p->n106.super.Reset))(&p->n106.super, fClock, nRate);
    ((p->fme7.super.Reset))(&p->fme7.super, fClock, nRate);

    mmEmuApu_SoundSetup(p);
}
MM_EXPORT_EMU mmByte_t mmEmuApu_Read(struct mmEmuApu* p, mmWord_t addr)
{
    return ((p->internal.super.SyncRead))(&p->internal.super, addr);
}
MM_EXPORT_EMU void mmEmuApu_Write(struct mmEmuApu* p, mmWord_t addr, mmByte_t data)
{
    // $ 4018 is the VirtuaNES specific port.
    if (addr >= 0x4000 && addr <= 0x401F)
    {
        ((p->internal.super.SyncWrite))(&p->internal.super, addr, data);

        mmEmuApu_SetQueue(p, mmEmuCpu_GetTotalCycles(&p->nes->cpu), addr, data);
    }
}
MM_EXPORT_EMU mmByte_t mmEmuApu_ExRead(struct mmEmuApu* p, mmWord_t addr)
{
    mmByte_t data = 0;

    if (p->exsound_select & 0x10)
    {
        if (addr == 0x4800)
        {
            mmEmuApu_SetExQueue(p, mmEmuCpu_GetTotalCycles(&p->nes->cpu), 0, 0);
        }
    }
    if (p->exsound_select & 0x04)
    {
        if (addr >= 0x4040 && addr < 0x4100)
        {
            data = ((p->fds.super.SyncRead))(&p->fds.super, addr);
        }
    }
    if (p->exsound_select & 0x08)
    {
        if (addr >= 0x5000 && addr <= 0x5015)
        {
            data = ((p->mmc5.super.SyncRead))(&p->mmc5.super, addr);
        }
    }

    return  data;
}
MM_EXPORT_EMU void mmEmuApu_ExWrite(struct mmEmuApu* p, mmWord_t addr, mmByte_t data)
{
    mmEmuApu_SetExQueue(p, mmEmuCpu_GetTotalCycles(&p->nes->cpu), addr, data);

    if (p->exsound_select & 0x04)
    {
        if (addr >= 0x4040 && addr < 0x4100)
        {
            ((p->fds.super.SyncWrite))(&p->fds.super, addr, data);
        }
    }

    if (p->exsound_select & 0x08)
    {
        if (addr >= 0x5000 && addr <= 0x5015)
        {
            ((p->mmc5.super.SyncWrite))(&p->mmc5.super, addr, data);
        }
    }
}
MM_EXPORT_EMU void mmEmuApu_Sync(struct mmEmuApu* p)
{
    // do nothing.
}
MM_EXPORT_EMU void mmEmuApu_SyncDPCM(struct mmEmuApu* p, mmInt_t cycles)
{
    ((p->internal.super.Sync))(&p->internal.super, cycles);

    if (p->exsound_select & 0x04)
    {
        ((p->fds.super.Sync))(&p->fds.super, cycles);
    }
    if (p->exsound_select & 0x08)
    {
        ((p->mmc5.super.Sync))(&p->mmc5.super, cycles);
    }
}
MM_EXPORT_EMU void mmEmuApu_Process(struct mmEmuApu* p, mmByte_t* lpBuffer, mmUInt32_t dwSize, mmInt_t nRate)
{
    mmInt_t nBits = p->nes->config.audio.nBits;
    mmUInt32_t  dwLength = dwSize / (nBits / 8);
    mmInt_t output;
    struct mmEmuApuQueueData q;
    mmUInt64_t  writetime;

    mmShort_t*  pSoundBuf = p->m_SoundBuffer;
    mmInt_t nCcount = 0;

    mmInt_t nFilterType = p->nes->config.audio.nFilterType;

    //
    // Volume setup
    //  0:Master
    //  1:Rectangle 1
    //  2:Rectangle 2
    //  3:Triangle
    //  4:Noise
    //  5:DPCM
    //  6:VRC6
    //  7:VRC7
    //  8:FDS
    //  9:MMC5
    // 10:N106
    // 11:FME7
    mmInt_t vol[24];
    mmBool_t*   bMute = p->m_bMute;
    mmShort_t*  nVolume = p->nes->config.audio.nVolume;
    mmInt_t FrameCycles = p->nes->config.nescfg.FrameCycles;

    mmInt_t nMasterVolume = bMute[0] ? nVolume[0] : 0;
    //
    double cycle_rate = 0;
    //
    if (!p->nes->config.audio.bEnable)
    {
        mmMemset(lpBuffer, (mmByte_t)(nRate == 8 ? 128 : 0), dwSize);
        return;
    }

    // Internal
    vol[0] = bMute[1] ? (RECTANGLE_VOL*nVolume[1] * nMasterVolume) / (100 * 100) : 0;
    vol[1] = bMute[2] ? (RECTANGLE_VOL*nVolume[2] * nMasterVolume) / (100 * 100) : 0;
    vol[2] = bMute[3] ? (TRIANGLE_VOL *nVolume[3] * nMasterVolume) / (100 * 100) : 0;
    vol[3] = bMute[4] ? (NOISE_VOL    *nVolume[4] * nMasterVolume) / (100 * 100) : 0;
    vol[4] = bMute[5] ? (DPCM_VOL     *nVolume[5] * nMasterVolume) / (100 * 100) : 0;

    // VRC6
    vol[5] = bMute[6] ? (VRC6_VOL*nVolume[6] * nMasterVolume) / (100 * 100) : 0;
    vol[6] = bMute[7] ? (VRC6_VOL*nVolume[6] * nMasterVolume) / (100 * 100) : 0;
    vol[7] = bMute[8] ? (VRC6_VOL*nVolume[6] * nMasterVolume) / (100 * 100) : 0;

    // VRC7
    vol[8] = bMute[6] ? (VRC7_VOL*nVolume[7] * nMasterVolume) / (100 * 100) : 0;

    // FDS
    vol[9] = bMute[6] ? (FDS_VOL*nVolume[8] * nMasterVolume) / (100 * 100) : 0;

    // MMC5
    vol[10] = bMute[6] ? (MMC5_VOL*nVolume[9] * nMasterVolume) / (100 * 100) : 0;
    vol[11] = bMute[7] ? (MMC5_VOL*nVolume[9] * nMasterVolume) / (100 * 100) : 0;
    vol[12] = bMute[8] ? (MMC5_VOL*nVolume[9] * nMasterVolume) / (100 * 100) : 0;

    // N106
    vol[13] = bMute[6] ? (N106_VOL*nVolume[10] * nMasterVolume) / (100 * 100) : 0;
    vol[14] = bMute[7] ? (N106_VOL*nVolume[10] * nMasterVolume) / (100 * 100) : 0;
    vol[15] = bMute[8] ? (N106_VOL*nVolume[10] * nMasterVolume) / (100 * 100) : 0;
    vol[16] = bMute[9] ? (N106_VOL*nVolume[10] * nMasterVolume) / (100 * 100) : 0;
    vol[17] = bMute[10] ? (N106_VOL*nVolume[10] * nMasterVolume) / (100 * 100) : 0;
    vol[18] = bMute[11] ? (N106_VOL*nVolume[10] * nMasterVolume) / (100 * 100) : 0;
    vol[19] = bMute[12] ? (N106_VOL*nVolume[10] * nMasterVolume) / (100 * 100) : 0;
    vol[20] = bMute[13] ? (N106_VOL*nVolume[10] * nMasterVolume) / (100 * 100) : 0;

    // FME7
    vol[21] = bMute[6] ? (FME7_VOL*nVolume[11] * nMasterVolume) / (100 * 100) : 0;
    vol[22] = bMute[7] ? (FME7_VOL*nVolume[11] * nMasterVolume) / (100 * 100) : 0;
    vol[23] = bMute[8] ? (FME7_VOL*nVolume[11] * nMasterVolume) / (100 * 100) : 0;

    // cycle_rate = ((double)FRAME_CYCLES*60.0/12.0)/(double)Config.sound.nRate;
    cycle_rate = ((double)FrameCycles * 60.0 / 12.0) / (double)nRate;

    // Measures to be taken when the number of CPU cycles loops.
    if (p->elapsed_time > (double)mmEmuCpu_GetTotalCycles(&p->nes->cpu))
    {
        mmEmuApu_QueueFlush(p);
    }

    while (dwLength--)
    {
        writetime = (mmUInt64_t)p->elapsed_time;

        while (mmEmuApu_GetQueue(p, writetime, &q))
        {
            mmEmuApu_WriteProcess(p, q.addr, q.data);
        }

        while (mmEmuApu_GetExQueue(p, writetime, &q))
        {
            mmEmuApu_WriteExProcess(p, q.addr, q.data);
        }

        // 0-4:internal 5-7:VRC6 8:VRC7 9:FDS 10-12:MMC5 13-20:N106 21-23:FME7
        output = 0;
        output += ((p->internal.super.Process))(&p->internal.super, 0) * vol[0];
        output += ((p->internal.super.Process))(&p->internal.super, 1) * vol[1];
        output += ((p->internal.super.Process))(&p->internal.super, 2) * vol[2];
        output += ((p->internal.super.Process))(&p->internal.super, 3) * vol[3];
        output += ((p->internal.super.Process))(&p->internal.super, 4) * vol[4];

        if (p->exsound_select & 0x01)
        {
            output += ((p->vrc6.super.Process))(&p->vrc6.super, 0) * vol[5];
            output += ((p->vrc6.super.Process))(&p->vrc6.super, 1) * vol[6];
            output += ((p->vrc6.super.Process))(&p->vrc6.super, 2) * vol[7];
        }
        if (p->exsound_select & 0x02)
        {
            output += ((p->vrc7.super.Process))(&p->vrc7.super, 0) * vol[8];
        }
        if (p->exsound_select & 0x04)
        {
            output += ((p->fds.super.Process))(&p->fds.super, 0) * vol[9];
        }
        if (p->exsound_select & 0x08)
        {
            output += ((p->mmc5.super.Process))(&p->mmc5.super, 0) * vol[10];
            output += ((p->mmc5.super.Process))(&p->mmc5.super, 1) * vol[11];
            output += ((p->mmc5.super.Process))(&p->mmc5.super, 2) * vol[12];
        }
        if (p->exsound_select & 0x10)
        {
            output += ((p->n106.super.Process))(&p->n106.super, 0) * vol[13];
            output += ((p->n106.super.Process))(&p->n106.super, 1) * vol[14];
            output += ((p->n106.super.Process))(&p->n106.super, 2) * vol[15];
            output += ((p->n106.super.Process))(&p->n106.super, 3) * vol[16];
            output += ((p->n106.super.Process))(&p->n106.super, 4) * vol[17];
            output += ((p->n106.super.Process))(&p->n106.super, 5) * vol[18];
            output += ((p->n106.super.Process))(&p->n106.super, 6) * vol[19];
            output += ((p->n106.super.Process))(&p->n106.super, 7) * vol[20];
        }
        if (p->exsound_select & 0x20)
        {
            // Envelope & Noise
            ((p->fme7.super.Process))(&p->fme7.super, 3);
            output += ((p->fme7.super.Process))(&p->fme7.super, 0) * vol[21];
            output += ((p->fme7.super.Process))(&p->fme7.super, 1) * vol[22];
            output += ((p->fme7.super.Process))(&p->fme7.super, 2) * vol[23];
        }

        output >>= 8;

        if (nFilterType == 1)
        {
            // Low pass filter TYPE 1 (Simple).
            output = (p->lowpass_filter[0] + output) / 2;
            p->lowpass_filter[0] = output;
        }
        else if (nFilterType == 2)
        {
            // Low pass filter TYPE 2 (Weighted type 1).
            output = (p->lowpass_filter[1] + p->lowpass_filter[0] + output) / 3;
            p->lowpass_filter[1] = p->lowpass_filter[0];
            p->lowpass_filter[0] = output;
        }
        else if (nFilterType == 3)
        {
            // Low pass filter TYPE 3 (Weighted type 2).
            output = (p->lowpass_filter[2] + p->lowpass_filter[1] + p->lowpass_filter[0] + output) / 4;
            p->lowpass_filter[2] = p->lowpass_filter[1];
            p->lowpass_filter[1] = p->lowpass_filter[0];
            p->lowpass_filter[0] = output;
        }
        else if (nFilterType == 4)
        {
            // Low pass filter TYPE 4 (Weighted type 3)
            output = (p->lowpass_filter[1] + p->lowpass_filter[0] * 2 + output) / 4;
            p->lowpass_filter[1] = p->lowpass_filter[0];
            p->lowpass_filter[0] = output;
        }

#if 0
        // Cut DC component.
        {
            static double ave = 0.0, max = 0.0, min = 0.0;
            double delta;
            delta = (max - min) / 32768.0;
            max -= delta;
            min += delta;
            if (output > max) max = output;
            if (output < min) min = output;
            ave -= ave / 1024.0;
            ave += (max + min) / 2048.0;
            output -= (mmInt_t)ave;
        }
#endif
#if 1
        // DC component cut (HPF TEST).
        {
            // static   double  cutoff = (2.0*3.141592653579*40.0/44100.0);
            static  double  cutofftemp = (2.0*3.141592653579*40.0);
            double  cutoff = cutofftemp / (double)nRate;
            static  double  tmp = 0.0;
            double  in, out;

            in = (double)output;
            out = (in - tmp);
            tmp = tmp + cutoff * out;

            output = (mmInt_t)out;
        }
#endif
#if 0
        // Removal of spike noise (AGC TEST).
        {
            mmInt_t diff = abs(output - p->last_data);
            if (diff > 0x4000)
            {
                output /= 4;
            }
            else
            {
                if (diff > 0x3000)
                {
                    output /= 3;
                }
                else
                {
                    if (diff > 0x2000)
                    {
                        output /= 2;
                    }
                }
            }
            p->last_data = output;
        }
#endif
        // Limit
        if (output > 0x7FFF)
        {
            output = 0x7FFF;
        }
        else if (output < -0x8000)
        {
            output = -0x8000;
        }

        if (nBits != 8)
        {
            *(mmShort_t*)lpBuffer = (mmShort_t)output;
            lpBuffer += sizeof(mmShort_t);
        }
        else
        {
            *lpBuffer++ = (output >> 8) ^ 0x80;
        }

        if (nCcount < 0x0100)
        {
            pSoundBuf[nCcount++] = (mmShort_t)output;
        }

        // elapsedtime += cycle_rate;
        p->elapsed_time += cycle_rate;
    }

#if 1
    if (p->elapsed_time > ((FrameCycles / 24) + mmEmuCpu_GetTotalCycles(&p->nes->cpu)))
    {
        p->elapsed_time = (double)mmEmuCpu_GetTotalCycles(&p->nes->cpu);
    }
    if ((p->elapsed_time + (FrameCycles / 6)) < mmEmuCpu_GetTotalCycles(&p->nes->cpu))
    {
        p->elapsed_time = (double)mmEmuCpu_GetTotalCycles(&p->nes->cpu);
    }
#else
    p->elapsed_time = (double)mmEmuCpu_GetTotalCycles(&p->nes->cpu);
#endif
}
// Channel frequency acquisition subroutine (for NSF).
MM_EXPORT_EMU mmInt_t mmEmuApu_GetChannelFrequency(struct mmEmuApu* p, mmInt_t channel)
{
    if (!p->m_bMute[0])
    {
        return 0;
    }

    // Internal
    if (channel < 5)
    {
        return p->m_bMute[channel + 1] ? ((p->internal.super.GetFreq))(&p->internal.super, channel) : 0;
    }
    // VRC6
    if ((p->exsound_select & 0x01) && channel >= 0x0100 && channel < 0x0103)
    {
        return p->m_bMute[6 + (channel & 0x03)] ? ((p->vrc6.super.GetFreq))(&p->vrc6.super, channel & 0x03) : 0;
    }
    // FDS
    if ((p->exsound_select & 0x04) && channel == 0x300)
    {
        return p->m_bMute[6] ? ((p->fds.super.GetFreq))(&p->fds.super, 0) : 0;
    }
    // MMC5
    if ((p->exsound_select & 0x08) && channel >= 0x0400 && channel < 0x0402)
    {
        return p->m_bMute[6 + (channel & 0x03)] ? ((p->mmc5.super.GetFreq))(&p->mmc5.super, channel & 0x03) : 0;
    }
    // N106
    if ((p->exsound_select & 0x10) && channel >= 0x0500 && channel < 0x0508)
    {
        return p->m_bMute[6 + (channel & 0x07)] ? ((p->n106.super.GetFreq))(&p->n106.super, channel & 0x07) : 0;
    }
    // FME7
    if ((p->exsound_select & 0x20) && channel >= 0x0600 && channel < 0x0603)
    {
        return p->m_bMute[6 + (channel & 0x03)] ? ((p->fme7.super.GetFreq))(&p->fme7.super, channel & 0x03) : 0;
    }
    // VRC7
    if ((p->exsound_select & 0x02) && channel >= 0x0700 && channel < 0x0709)
    {

        return p->m_bMute[6] ? ((p->vrc7.super.GetFreq))(&p->vrc7.super, channel & 0x0F) : 0;
    }
    return 0;
}
MM_EXPORT_EMU mmShort_t* mmEmuApu_GetSoundBuffer(struct mmEmuApu* p)
{
    return p->m_SoundBuffer;
}
MM_EXPORT_EMU void mmEmuApu_GetFrameIRQ(struct mmEmuApu* p, mmInt_t* Cycle, mmByte_t* Count, mmByte_t* Type, mmByte_t* IRQ, mmByte_t* Occur)
{
    mmEmuApuInternal_GetFrameIRQ(&p->internal, Cycle, Count, Type, IRQ, Occur);
}

MM_EXPORT_EMU void mmEmuApu_SetFrameIRQ(struct mmEmuApu* p, mmInt_t Cycle, mmByte_t Count, mmByte_t Type, mmByte_t IRQ, mmByte_t Occur)
{
    mmEmuApuInternal_SetFrameIRQ(&p->internal, Cycle, Count, Type, IRQ, Occur);
}

MM_EXPORT_EMU void mmEmuApu_SaveState(struct mmEmuApu* p, mmByte_t* buffer)
{
#ifdef  _DEBUG
    struct mmLogger* gLogger = mmLogger_Instance();
    mmByte_t*   pold = buffer;
#endif

    // Flush to synchronize the time axis.
    mmEmuApu_QueueFlush(p);

    ((p->internal.super.SaveState))(&p->internal.super, buffer);
    buffer += (((p->internal.super.GetStateSize))(&p->internal.super) + 15)&(~0x0F);    // Padding

    // VRC6
    if (p->exsound_select & 0x01)
    {
        ((p->vrc6.super.SaveState))(&p->vrc6.super, buffer);
        buffer += (((p->vrc6.super.GetStateSize))(&p->vrc6.super) + 15) &(~0x0F); // Padding
    }
    // VRC7 (not support)
    if (p->exsound_select & 0x02)
    {
        ((p->vrc7.super.SaveState))(&p->vrc7.super, buffer);
        buffer += (((p->vrc7.super.GetStateSize))(&p->vrc7.super) + 15) &(~0x0F); // Padding
    }
    // FDS
    if (p->exsound_select & 0x04)
    {
        ((p->fds.super.SaveState))(&p->fds.super, buffer);
        buffer += (((p->fds.super.GetStateSize))(&p->fds.super) + 15) &(~0x0F); // Padding
    }
    // MMC5
    if (p->exsound_select & 0x08)
    {
        ((p->mmc5.super.SaveState))(&p->mmc5.super, buffer);
        buffer += (((p->mmc5.super.GetStateSize))(&p->mmc5.super) + 15) &(~0x0F); // Padding
    }
    // N106
    if (p->exsound_select & 0x10)
    {
        ((p->n106.super.SaveState))(&p->n106.super, buffer);
        buffer += (((p->n106.super.GetStateSize))(&p->n106.super) + 15) &(~0x0F); // Padding
    }
    // FME7
    if (p->exsound_select & 0x20)
    {
        ((p->fme7.super.SaveState))(&p->fme7.super, buffer);
        buffer += (((p->fme7.super.GetStateSize))(&p->fme7.super) + 15) &(~0x0F); // Padding
    }

#ifdef  _DEBUG
    mmLogger_LogD(gLogger, "%s %d SAVE APU SIZE: %d.", __FUNCTION__, __LINE__, (mmInt_t)((uintptr_t)buffer - (uintptr_t)pold));
#endif
}
MM_EXPORT_EMU void mmEmuApu_LoadState(struct mmEmuApu* p, mmByte_t* buffer)
{
    // Erase to synchronize the time axis.
    mmEmuApu_QueueClear(p);

    ((p->internal.super.LoadState))(&p->internal.super, buffer);
    buffer += (((p->internal.super.GetStateSize))(&p->internal.super) + 15)&(~0x0F);    // Padding

    // VRC6
    if (p->exsound_select & 0x01)
    {
        ((p->vrc6.super.LoadState))(&p->vrc6.super, buffer);
        buffer += (((p->vrc6.super.GetStateSize))(&p->vrc6.super) + 15) &(~0x0F); // Padding
    }
    // VRC7 (not support)
    if (p->exsound_select & 0x02)
    {
        ((p->vrc7.super.LoadState))(&p->vrc7.super, buffer);
        buffer += (((p->vrc7.super.GetStateSize))(&p->vrc7.super) + 15) &(~0x0F); // Padding
    }
    // FDS
    if (p->exsound_select & 0x04)
    {
        ((p->fds.super.LoadState))(&p->fds.super, buffer);
        buffer += (((p->fds.super.GetStateSize))(&p->fds.super) + 15) &(~0x0F); // Padding
    }
    // MMC5
    if (p->exsound_select & 0x08)
    {
        ((p->mmc5.super.LoadState))(&p->mmc5.super, buffer);
        buffer += (((p->mmc5.super.GetStateSize))(&p->mmc5.super) + 15) &(~0x0F); // Padding
    }
    // N106
    if (p->exsound_select & 0x10)
    {
        ((p->n106.super.LoadState))(&p->n106.super, buffer);
        buffer += (((p->n106.super.GetStateSize))(&p->n106.super) + 15) &(~0x0F); // Padding
    }
    // FME7
    if (p->exsound_select & 0x20)
    {
        ((p->fme7.super.LoadState))(&p->fme7.super, buffer);
        buffer += (((p->fme7.super.GetStateSize))(&p->fme7.super) + 15) &(~0x0F); // Padding
    }
}

MM_EXPORT_EMU void mmEmuApu_SetQueue(struct mmEmuApu* p, mmUInt64_t writetime, mmWord_t addr, mmByte_t data)
{
    mmEmuApuQueue_Set(&p->queue, writetime, addr, data);
}
MM_EXPORT_EMU mmBool_t mmEmuApu_GetQueue(struct mmEmuApu* p, mmUInt64_t writetime, struct mmEmuApuQueueData* ret)
{
    return mmEmuApuQueue_Get(&p->queue, writetime, ret);
}

MM_EXPORT_EMU void mmEmuApu_SetExQueue(struct mmEmuApu* p, mmUInt64_t writetime, mmWord_t addr, mmByte_t data)
{
    mmEmuApuQueue_Set(&p->exqueue, writetime, addr, data);
}
MM_EXPORT_EMU mmBool_t mmEmuApu_GetExQueue(struct mmEmuApu* p, mmUInt64_t writetime, struct mmEmuApuQueueData* ret)
{
    return mmEmuApuQueue_Get(&p->exqueue, writetime, ret);
}

MM_EXPORT_EMU void mmEmuApu_QueueFlush(struct mmEmuApu* p)
{
    while (p->queue.wrptr != p->queue.rdptr)
    {
        mmEmuApu_WriteProcess(p, p->queue.data[p->queue.rdptr].addr, p->queue.data[p->queue.rdptr].data);
        p->queue.rdptr++;
        p->queue.rdptr &= MM_EMU_APU_QUEUE_LENGTH - 1;
    }

    while (p->exqueue.wrptr != p->exqueue.rdptr)
    {
        mmEmuApu_WriteExProcess(p, p->exqueue.data[p->exqueue.rdptr].addr, p->exqueue.data[p->exqueue.rdptr].data);
        p->exqueue.rdptr++;
        p->exqueue.rdptr &= MM_EMU_APU_QUEUE_LENGTH - 1;
    }
}

MM_EXPORT_EMU void mmEmuApu_WriteProcess(struct mmEmuApu* p, mmWord_t addr, mmByte_t data)
{
    // $ 4018 is the VirtuaNES specific port.
    if (addr >= 0x4000 && addr <= 0x401F)
    {
        ((p->internal.super.Write))(&p->internal.super, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuApu_WriteExProcess(struct mmEmuApu* p, mmWord_t addr, mmByte_t data)
{
    if (p->exsound_select & 0x01)
    {
        ((p->vrc6.super.Write))(&p->vrc6.super, addr, data);
    }
    if (p->exsound_select & 0x02)
    {
        ((p->vrc7.super.Write))(&p->vrc7.super, addr, data);
    }
    if (p->exsound_select & 0x04)
    {
        ((p->fds.super.Write))(&p->fds.super, addr, data);
    }
    if (p->exsound_select & 0x08)
    {
        ((p->mmc5.super.Write))(&p->mmc5.super, addr, data);
    }
    if (p->exsound_select & 0x10)
    {
        if (addr == 0x0000)
        {
            // mmByte_t dummy = ((p->n106.super.Read))(&p->n106.super, addr);
            ((p->n106.super.Read))(&p->n106.super, addr);
        }
        else
        {
            ((p->n106.super.Write))(&p->n106.super, addr, data);
        }
    }
    if (p->exsound_select & 0x20)
    {
        ((p->fme7.super.Write))(&p->fme7.super, addr, data);
    }
}

