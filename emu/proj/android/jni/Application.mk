APP_ABI := arm64-v8a armeabi-v7a

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libmm_emu_shared
APP_MODULES += libmm_emu_static

# APP_MODULES += libmm_emulator_shared
# APP_MODULES += libmm_emulator_static
