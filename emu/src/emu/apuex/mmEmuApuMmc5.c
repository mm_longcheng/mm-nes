/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApuMmc5.h"

#include "emu/mmEmuState.h"

#include "core/mmString.h"

#define RECTANGLE_VOL_SHIFT 8
#define DAOUT_VOL_SHIFT     6

MM_EXPORT_EMU const mmInt_t MM_EMU_APU_MMC5_VBL_LENGTH[32] =
{
     5, 127,   10,   1,   19,   2,   40,   3,
    80,   4,   30,   5,    7,   6,   13,   7,
     6,   8,   12,   9,   24,  10,   48,  11,
    96,  12,   36,  13,    8,  14,   16,  15,
};

MM_EXPORT_EMU const mmInt_t MM_EMU_APU_MMC5_DUTY_LUT[4] =
{
    2,  4,  8, 12,
};

MM_EXPORT_EMU void mmEmuApuMmc5_Init(struct mmEmuApuMmc5* p)
{
    mmEmuApuInterface_Init(&p->super);

    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuMmc5Rectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuMmc5Rectangle));

    mmMemset(&p->sch0, 0, sizeof(struct mmEmuApuMmc5SyncRectangle));
    mmMemset(&p->sch1, 0, sizeof(struct mmEmuApuMmc5SyncRectangle));

    p->reg5010 = 0;
    p->reg5011 = 0;
    p->reg5015 = 0;

    p->cycle_rate = 0;

    p->FrameCycle = 0;
    p->sync_reg5015 = 0;

    p->cpu_clock = 0;

    mmMemset(&p->decay_lut, 0, sizeof(mmInt_t) * 16);
    mmMemset(&p->vbl_lut, 0, sizeof(mmInt_t) * 32);
    //
    p->super.Reset = &mmEmuApuMmc5_Reset;
    p->super.Setup = &mmEmuApuMmc5_Setup;
    p->super.Process = &mmEmuApuMmc5_Process;
    p->super.Write = &mmEmuApuMmc5_Write;
    //p->super.Read = &mm_emu_apu_mmc5_Read;
    p->super.SyncWrite = &mmEmuApuMmc5_SyncWrite;
    p->super.SyncRead = &mmEmuApuMmc5_SyncRead;
    p->super.Sync = &mmEmuApuMmc5_Sync;
    p->super.GetFreq = &mmEmuApuMmc5_GetFreq;
    p->super.GetStateSize = &mmEmuApuMmc5_GetStateSize;
    p->super.SaveState = &mmEmuApuMmc5_SaveState;
    p->super.LoadState = &mmEmuApuMmc5_LoadState;

    // Provisional setting.
    (*(p->super.Reset))(&p->super, MM_EMU_APU_CLOCK, 22050);
}
MM_EXPORT_EMU void mmEmuApuMmc5_Destroy(struct mmEmuApuMmc5* p)
{
    mmEmuApuInterface_Destroy(&p->super);

    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuMmc5Rectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuMmc5Rectangle));

    mmMemset(&p->sch0, 0, sizeof(struct mmEmuApuMmc5SyncRectangle));
    mmMemset(&p->sch1, 0, sizeof(struct mmEmuApuMmc5SyncRectangle));

    p->reg5010 = 0;
    p->reg5011 = 0;
    p->reg5015 = 0;

    p->cycle_rate = 0;

    p->FrameCycle = 0;
    p->sync_reg5015 = 0;

    p->cpu_clock = 0;

    mmMemset(&p->decay_lut, 0, sizeof(mmInt_t) * 16);
    mmMemset(&p->vbl_lut, 0, sizeof(mmInt_t) * 32);
}

MM_EXPORT_EMU mmInt_t mmEmuApuMmc5_RectangleRender(struct mmEmuApuMmc5* p, struct mmEmuApuMmc5Rectangle* ch)
{
    mmInt_t volume = 0;
    mmInt_t output = 0;

    if (!ch->enable || ch->vbl_length <= 0)
    {
        return 0;
    }

    // vbl length counter
    if (!ch->holdnote)
    {
        ch->vbl_length -= 5;
    }

    // envelope unit
    ch->env_phase -= 5 * 4;
    while (ch->env_phase < 0)
    {
        ch->env_phase += ch->env_decay;
        if (ch->holdnote)
        {
            ch->env_vol = (ch->env_vol + 1) & 0x0F;
        }
        else if (ch->env_vol < 0x0F)
        {
            ch->env_vol++;
        }
    }

    if (ch->freq < MM_EMU_INT2FIX(8))
    {
        return 0;
    }

    volume = 0;
    if (ch->fixed_envelope)
    {
        volume = (mmInt_t)ch->volume;
    }
    else
    {
        volume = (mmInt_t)(0x0F - ch->env_vol);
    }

    output = volume << RECTANGLE_VOL_SHIFT;

    ch->phaseacc -= p->cycle_rate;
    if (ch->phaseacc >= 0)
    {
        if (ch->adder < ch->duty_flip)
        {
            ch->output_vol = output;
        }
        else
        {
            ch->output_vol = -output;
        }
        return ch->output_vol;
    }

    if (ch->freq > p->cycle_rate)
    {
        ch->phaseacc += ch->freq;
        ch->adder = (ch->adder + 1) & 0x0F;
        if (ch->adder < ch->duty_flip)
        {
            ch->output_vol = output;
        }
        else
        {
            ch->output_vol = -output;
        }
    }
    else
    {
        // weighted average.
        mmInt_t num_times, total;
        num_times = total = 0;
        while (ch->phaseacc < 0)
        {
            ch->phaseacc += ch->freq;
            ch->adder = (ch->adder + 1) & 0x0F;
            if (ch->adder < ch->duty_flip)
            {
                total += output;
            }
            else
            {
                total -= output;
            }
            num_times++;
        }
        ch->output_vol = total / num_times;
    }

    return ch->output_vol;
}

MM_EXPORT_EMU void mmEmuApuMmc5_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    mmWord_t addr = 0;

    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuMmc5Rectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuMmc5Rectangle));

    p->reg5010 = p->reg5011 = p->reg5015 = 0;

    p->sync_reg5015 = 0;
    p->FrameCycle = 0;

    (*(p->super.Setup))(&p->super, fClock, nRate);

    for (addr = 0x5000; addr <= 0x5015; addr++)
    {
        (*(p->super.Write))(&p->super, addr, 0);
    }
}
MM_EXPORT_EMU void mmEmuApuMmc5_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    mmInt_t i = 0;
    mmInt_t samples = 0;

    p->cpu_clock = fClock;
    p->cycle_rate = (mmInt_t)(fClock*65536.0f / (float)nRate);

    // Create Tables
    samples = (mmInt_t)((float)nRate / 60.0f);
    for (i = 0; i < 16; i++)
    {
        p->decay_lut[i] = (i + 1) * samples * 5;
    }
    for (i = 0; i < 32; i++)
    {
        p->vbl_lut[i] = MM_EMU_APU_MMC5_VBL_LENGTH[i] * samples * 5;
    }
}
MM_EXPORT_EMU void mmEmuApuMmc5_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    //DEBUGOUT( "$%04X:%02X\n", addr, data );
    switch (addr)
    {
        // MMC5 CH0 rectangle
    case 0x5000:
        p->ch0.reg[0] = data;
        p->ch0.volume = data & 0x0F;
        p->ch0.holdnote = data & 0x20;
        p->ch0.fixed_envelope = data & 0x10;
        p->ch0.env_decay = p->decay_lut[data & 0x0F];
        p->ch0.duty_flip = MM_EMU_APU_MMC5_DUTY_LUT[data >> 6];
        break;
    case 0x5001:
        p->ch0.reg[1] = data;
        break;
    case 0x5002:
        p->ch0.reg[2] = data;
        p->ch0.freq = MM_EMU_INT2FIX(((p->ch0.reg[3] & 0x07) << 8) + data + 1);
        break;
    case 0x5003:
        p->ch0.reg[3] = data;
        p->ch0.vbl_length = p->vbl_lut[data >> 3];
        p->ch0.env_vol = 0;
        p->ch0.freq = MM_EMU_INT2FIX(((data & 0x07) << 8) + p->ch0.reg[2] + 1);
        if (p->reg5015 & 0x01)
        {
            p->ch0.enable = 0xFF;
        }
        break;
        // MMC5 CH1 rectangle
    case 0x5004:
        p->ch1.reg[0] = data;
        p->ch1.volume = data & 0x0F;
        p->ch1.holdnote = data & 0x20;
        p->ch1.fixed_envelope = data & 0x10;
        p->ch1.env_decay = p->decay_lut[data & 0x0F];
        p->ch1.duty_flip = MM_EMU_APU_MMC5_DUTY_LUT[data >> 6];
        break;
    case 0x5005:
        p->ch1.reg[1] = data;
        break;
    case 0x5006:
        p->ch1.reg[2] = data;
        p->ch1.freq = MM_EMU_INT2FIX(((p->ch1.reg[3] & 0x07) << 8) + data + 1);
        break;
    case 0x5007:
        p->ch1.reg[3] = data;
        p->ch1.vbl_length = p->vbl_lut[data >> 3];
        p->ch1.env_vol = 0;
        p->ch1.freq = MM_EMU_INT2FIX(((data & 0x07) << 8) + p->ch1.reg[2] + 1);
        if (p->reg5015 & 0x02)
        {
            p->ch1.enable = 0xFF;
        }
        break;
    case 0x5010:
        p->reg5010 = data;
        break;
    case 0x5011:
        p->reg5011 = data;
        break;
    case 0x5012:
    case 0x5013:
    case 0x5014:
        break;
    case 0x5015:
        p->reg5015 = data;
        if (p->reg5015 & 0x01)
        {
            p->ch0.enable = 0xFF;
        }
        else
        {
            p->ch0.enable = 0;
            p->ch0.vbl_length = 0;
        }
        if (p->reg5015 & 0x02)
        {
            p->ch1.enable = 0xFF;
        }
        else
        {
            p->ch1.enable = 0;
            p->ch1.vbl_length = 0;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU mmInt_t mmEmuApuMmc5_Process(struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    switch (channel)
    {
    case 0:
        return mmEmuApuMmc5_RectangleRender(p, &p->ch0);
        break;
    case 1:
        return mmEmuApuMmc5_RectangleRender(p, &p->ch1);
        break;
    case 2:
        return (mmInt_t)p->reg5011 << DAOUT_VOL_SHIFT;
        break;
    default:
        break;
    }

    return 0;
}

MM_EXPORT_EMU void mmEmuApuMmc5_SyncWrite(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    switch (addr)
    {
        // MMC5 CH0 rectangle
    case 0x5000:
        p->sch0.reg[0] = data;
        p->sch0.holdnote = data & 0x20;
        break;
    case 0x5001:
    case 0x5002:
        p->sch0.reg[addr & 3] = data;
        break;
    case 0x5003:
        p->sch0.reg[3] = data;
        p->sch0.vbl_length = MM_EMU_APU_MMC5_VBL_LENGTH[data >> 3];
        if (p->sync_reg5015 & 0x01)
        {
            p->sch0.enable = 0xFF;
        }
        break;
        // MMC5 CH1 rectangle
    case 0x5004:
        p->sch1.reg[0] = data;
        p->sch1.holdnote = data & 0x20;
        break;
    case 0x5005:
    case 0x5006:
        p->sch1.reg[addr & 3] = data;
        break;
    case 0x5007:
        p->sch1.reg[3] = data;
        p->sch1.vbl_length = MM_EMU_APU_MMC5_VBL_LENGTH[data >> 3];
        if (p->sync_reg5015 & 0x02)
        {
            p->sch1.enable = 0xFF;
        }
        break;
    case 0x5010:
    case 0x5011:
    case 0x5012:
    case 0x5013:
    case 0x5014:
        break;
    case 0x5015:
        p->sync_reg5015 = data;
        if (p->sync_reg5015 & 0x01)
        {
            p->sch0.enable = 0xFF;
        }
        else
        {
            p->sch0.enable = 0;
            p->sch0.vbl_length = 0;
        }
        if (p->sync_reg5015 & 0x02)
        {
            p->sch1.enable = 0xFF;
        }
        else
        {
            p->sch1.enable = 0;
            p->sch1.vbl_length = 0;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU mmByte_t mmEmuApuMmc5_SyncRead(struct mmEmuApuInterface* super, mmWord_t addr)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    mmByte_t data = 0;

    if (addr == 0x5015)
    {
        if (p->sch0.enable && p->sch0.vbl_length > 0) data |= (1 << 0);
        if (p->sch1.enable && p->sch1.vbl_length > 0) data |= (1 << 1);
    }

    return data;
}
MM_EXPORT_EMU mmBool_t mmEmuApuMmc5_Sync(struct mmEmuApuInterface* super, mmInt_t cycles)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    p->FrameCycle += cycles;
    if (p->FrameCycle >= 7457 * 5 / 2)
    {
        p->FrameCycle -= 7457 * 5 / 2;

        if (p->sch0.enable && !p->sch0.holdnote)
        {
            if (p->sch0.vbl_length)
            {
                p->sch0.vbl_length--;
            }
        }
        if (p->sch1.enable && !p->sch1.holdnote)
        {
            if (p->sch1.vbl_length)
            {
                p->sch1.vbl_length--;
            }
        }
    }

    return MM_FALSE;
}

MM_EXPORT_EMU mmInt_t mmEmuApuMmc5_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    if (channel == 0 || channel == 1)
    {
        struct mmEmuApuMmc5Rectangle* ch;
        if (channel == 0)
        {
            ch = &p->ch0;
        }
        else
        {
            ch = &p->ch1;
        }

        if (!ch->enable || ch->vbl_length <= 0)
        {
            return 0;
        }
        if (ch->freq < MM_EMU_INT2FIX(8))
        {
            return 0;
        }
        if (ch->fixed_envelope)
        {
            if (!ch->volume)
            {
                return 0;
            }
        }
        else
        {
            if (!(0x0F - ch->env_vol))
            {
                return 0;
            }
        }

        return (mmInt_t)(256.0f * p->cpu_clock / ((double)MM_EMU_FIX2INT(ch->freq) * 16.0f));
    }

    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuMmc5_GetStateSize(const struct mmEmuApuInterface* super)
{
    // struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    return
        sizeof(mmByte_t) * 3 +
        sizeof(struct mmEmuApuMmc5Rectangle) +
        sizeof(struct mmEmuApuMmc5Rectangle) +
        sizeof(struct mmEmuApuMmc5SyncRectangle) +
        sizeof(struct mmEmuApuMmc5SyncRectangle);
}
MM_EXPORT_EMU void mmEmuApuMmc5_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    MM_EMU_SETBYTE(buffer, p->reg5010);
    MM_EMU_SETBYTE(buffer, p->reg5011);
    MM_EMU_SETBYTE(buffer, p->reg5015);

    MM_EMU_SETBLOCK(buffer, &p->ch0, sizeof(struct mmEmuApuMmc5Rectangle));
    MM_EMU_SETBLOCK(buffer, &p->ch1, sizeof(struct mmEmuApuMmc5Rectangle));

    MM_EMU_SETBYTE(buffer, p->sync_reg5015);
    MM_EMU_SETBLOCK(buffer, &p->sch0, sizeof(struct mmEmuApuMmc5SyncRectangle));
    MM_EMU_SETBLOCK(buffer, &p->sch1, sizeof(struct mmEmuApuMmc5SyncRectangle));
}
MM_EXPORT_EMU void mmEmuApuMmc5_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuMmc5* p = (struct mmEmuApuMmc5*)(super);

    MM_EMU_GETBYTE(buffer, p->reg5010);
    MM_EMU_GETBYTE(buffer, p->reg5011);
    MM_EMU_GETBYTE(buffer, p->reg5015);

    MM_EMU_GETBLOCK(buffer, &p->ch0, sizeof(struct mmEmuApuMmc5Rectangle));
    MM_EMU_GETBLOCK(buffer, &p->ch1, sizeof(struct mmEmuApuMmc5Rectangle));

    MM_EMU_GETBYTE(buffer, p->sync_reg5015);
    MM_EMU_GETBLOCK(buffer, &p->sch0, sizeof(struct mmEmuApuMmc5SyncRectangle));
    MM_EMU_GETBLOCK(buffer, &p->sch1, sizeof(struct mmEmuApuMmc5SyncRectangle));
}
