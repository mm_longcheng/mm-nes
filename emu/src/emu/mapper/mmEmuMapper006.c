/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper006.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper006_Init(struct mmEmuMapper006* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper006_Reset;
    p->super.Write = &mmEmuMapper006_Write;
    //p->super.Read = &mmEmuMapper006_Read;
    //p->super.ReadLow = &mmEmuMapper006_ReadLow;
    p->super.WriteLow = &mmEmuMapper006_WriteLow;
    //p->super.ExRead = &mmEmuMapper006_ExRead;
    //p->super.ExWrite = &mmEmuMapper006_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper006_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper006_ExCmdWrite;
    p->super.HSync = &mmEmuMapper006_HSync;
    //p->super.VSync = &mmEmuMapper006_VSync;
    //p->super.Clock = &mmEmuMapper006_Clock;
    //p->super.PPULatch = &mmEmuMapper006_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper006_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper006_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper006_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper006_IsStateSave;
    p->super.SaveState = &mmEmuMapper006_SaveState;
    p->super.LoadState = &mmEmuMapper006_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper006_Destroy(struct mmEmuMapper006* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper006_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper006* p = (struct mmEmuMapper006*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 14, 15);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
    else
    {
        mmEmuMmu_SetCRAM8KBank(mmu, 0);
    }

    p->irq_enable = 0;
    p->irq_counter = 0;
}
MM_EXPORT_EMU void mmEmuMapper006_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper006* p = (struct mmEmuMapper006*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x42FE:
        if (data & 0x10)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        break;
    case 0x42FF:
        if (data & 0x10)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;

    case 0x4501:
        p->irq_enable = 0;

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x4502:
        p->irq_counter = (p->irq_counter & 0xFF00) | data;
        break;
    case 0x4503:
        p->irq_counter = (p->irq_counter & 0x00FF) | ((mmInt_t)data << 8);
        p->irq_enable = 0xFF;

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        mmEmuMapper_WriteLow(super, addr, data);
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper006_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper006* p = (struct mmEmuMapper006*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM16KBank(mmu, 4, (data & 0x3C) >> 2);
    mmEmuMmu_SetCRAM8KBank(mmu, data & 0x03);
}
MM_EXPORT_EMU void mmEmuMapper006_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper006* p = (struct mmEmuMapper006*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        p->irq_counter += 133;
        if (p->irq_counter >= 0xFFFF)
        {
            // nes->cpu->IRQ();
            p->irq_counter = 0;

            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper006_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper006_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper006* p = (struct mmEmuMapper006*)(super);

    buffer[0] = p->irq_enable;
    *(mmInt_t*)&buffer[1] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper006_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper006* p = (struct mmEmuMapper006*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = *(mmInt_t*)&buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper006_Produce(void)
{
    struct mmEmuMapper006* p = (struct mmEmuMapper006*)mmMalloc(sizeof(struct mmEmuMapper006));
    mmEmuMapper006_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper006_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper006* p = (struct mmEmuMapper006*)(m);
    mmEmuMapper006_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER006 =
{
    &__static_mmEmuMapper006_Produce,
    &__static_mmEmuMapper006_Recycle,
};
