/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuNesProfiler.h"

#include "core/mmString.h"

MM_EXPORT_EMU void mmEmuNesProfiler_Init(struct mmEmuNesProfiler* p)
{
    p->m_ProfileEnable = 0;

    p->m_dwTotalCycle = 0;
    p->m_dwTotalTempCycle = 0;
    p->m_dwProfileTotalCycle = 0;
    p->m_dwProfileTotalCount = 0;
    p->m_dwProfileCycle = 0;
    p->m_dwProfileTempCycle = 0;
    p->m_dwProfileAveCycle = 0;
    p->m_dwProfileMaxCycle = 0;
}
MM_EXPORT_EMU void mmEmuNesProfiler_Destroy(struct mmEmuNesProfiler* p)
{
    p->m_ProfileEnable = 0;

    p->m_dwTotalCycle = 0;
    p->m_dwTotalTempCycle = 0;
    p->m_dwProfileTotalCycle = 0;
    p->m_dwProfileTotalCount = 0;
    p->m_dwProfileCycle = 0;
    p->m_dwProfileTempCycle = 0;
    p->m_dwProfileAveCycle = 0;
    p->m_dwProfileMaxCycle = 0;
}

MM_EXPORT_EMU void mmEmuNesProfiler_Reset(struct mmEmuNesProfiler* p)
{
    p->m_ProfileEnable = 0;

    p->m_dwTotalCycle = 0;
    p->m_dwTotalTempCycle = 0;
    p->m_dwProfileTotalCycle = 0;
    p->m_dwProfileTotalCount = 0;
    p->m_dwProfileCycle = 0;
    p->m_dwProfileTempCycle = 0;
    p->m_dwProfileAveCycle = 0;
    p->m_dwProfileMaxCycle = 0;
}

