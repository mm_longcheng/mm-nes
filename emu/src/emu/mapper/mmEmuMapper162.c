/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper162.h"
#include "emu/mmEmuNes.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuMapper162_Init(struct mmEmuMapper162* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg5000 = 3;
    p->reg5100 = 0;
    p->reg5200 = 0;
    p->reg5300 = 7;

    //
    p->super.Reset = &mmEmuMapper162_Reset;
    p->super.Write = &mmEmuMapper162_Write;
    //p->super.Read = &mmEmuMapper162_Read;
    //p->super.ReadLow = &mmEmuMapper162_ReadLow;
    p->super.WriteLow = &mmEmuMapper162_WriteLow;
    //p->super.ExRead = &mmEmuMapper162_ExRead;
    //p->super.ExWrite = &mmEmuMapper162_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper162_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper162_ExCmdWrite;
    p->super.HSync = &mmEmuMapper162_HSync;
    //p->super.VSync = &mmEmuMapper162_VSync;
    //p->super.Clock = &mmEmuMapper162_Clock;
    //p->super.PPULatch = &mmEmuMapper162_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper162_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper162_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper162_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper162_IsStateSave;
    p->super.SaveState = &mmEmuMapper162_SaveState;
    p->super.LoadState = &mmEmuMapper162_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper162_Destroy(struct mmEmuMapper162* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg5000 = 3;
    p->reg5100 = 0;
    p->reg5200 = 0;
    p->reg5300 = 7;
}

MM_EXPORT_EMU void mmEmuMapper162_SetBankCPU(struct mmEmuMapper162* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t bank = 0;

    switch (p->reg5300)
    {
    case 4:
        bank = (((p->reg5000 & 0xF) + ((p->reg5100 & 3) >> 1)) | ((p->reg5200 & 3) << 4));
        break;
    case 7:
        bank = (((p->reg5000 & 0xF) + ((p->reg5100 & 1) << 4)) | ((p->reg5200 & 3) << 4));
        //bank = ((p->reg5000 & 0xF) | ((p->reg5200 & 1) << 4));
        break;
    default:
        break;
    }
    mmEmuMmu_SetPROM32KBank(mmu, bank);
}
MM_EXPORT_EMU void mmEmuMapper162_SetBankPPU(struct mmEmuMapper162* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetCRAM8KBank(mmu, 0);
}

MM_EXPORT_EMU void mmEmuMapper162_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper162* p = (struct mmEmuMapper162*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    p->reg5000 = 3;
    p->reg5100 = 0;
    p->reg5200 = 0;
    p->reg5300 = 7;
    mmEmuMapper162_SetBankCPU(p);
    mmEmuMapper162_SetBankPPU(p);
}
MM_EXPORT_EMU void mmEmuMapper162_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    // struct mmLogger* gLogger = mmLogger_Instance();
    //
    // mmLogger_LogD(gLogger, "%s %d Address=%04X Data=%02X", __FUNCTION__, __LINE__, addr & 0xFFFF, data & 0xFF);
}

MM_EXPORT_EMU void mmEmuMapper162_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper162* p = (struct mmEmuMapper162*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // struct mmLogger* gLogger = mmLogger_Instance();

    //  DEBUGOUT("Address=%04X Data=%02X\n", addr&0xFFFF, data&0xFF );

    if (addr == 0x5000)
    {
        p->reg5000 = data;
        mmEmuMapper162_SetBankCPU(p);
        mmEmuMapper162_SetBankPPU(p);
    }
    else if (addr == 0x5100)
    {
        p->reg5100 = data;
        mmEmuMapper162_SetBankCPU(p);
        mmEmuMapper162_SetBankPPU(p);
    }
    else if (addr == 0x5200)
    {
        p->reg5200 = data;
        mmEmuMapper162_SetBankCPU(p);
        mmEmuMapper162_SetBankPPU(p);
    }
    else if (addr == 0x5300)
    {
        p->reg5300 = data;
    }
    else if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }
    else
    {
        // mmLogger_LogD(gLogger, "%s %d write to %04x:%02x", __FUNCTION__, __LINE__, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper162_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper162* p = (struct mmEmuMapper162*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((p->reg5000 & 0x80) && mmEmuPpu_IsDispON(&nes->ppu))
    {
        if (scanline < 127)
        {
            mmEmuMmu_SetCRAM4KBank(mmu, 4, 0);
        }
        else if (scanline < 240)
        {
            mmEmuMmu_SetCRAM4KBank(mmu, 4, 1);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper162_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper162_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper162* p = (struct mmEmuMapper162*)(super);

    buffer[0] = p->reg5000;
    buffer[1] = p->reg5100;
    buffer[2] = p->reg5200;
    buffer[3] = p->reg5300;
}
MM_EXPORT_EMU void mmEmuMapper162_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper162* p = (struct mmEmuMapper162*)(super);

    p->reg5000 = buffer[0];
    p->reg5100 = buffer[1];
    p->reg5200 = buffer[2];
    p->reg5300 = buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper162_Produce(void)
{
    struct mmEmuMapper162* p = (struct mmEmuMapper162*)mmMalloc(sizeof(struct mmEmuMapper162));
    mmEmuMapper162_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper162_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper162* p = (struct mmEmuMapper162*)(m);
    mmEmuMapper162_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER162 =
{
    &__static_mmEmuMapper162_Produce,
    &__static_mmEmuMapper162_Recycle,
};
