// Direct3D11 HLSL 4.0

Texture2D texture0 : register(t0);
Texture2D texture1 : register(t1);
SamplerState textureSamplerState0 : register(s0);
SamplerState textureSamplerState1 : register(s1);

struct VertOut
{
	float4 pos : SV_Position;
	float2 texcoord0 : TEXCOORD;
};

float4 main(VertOut input) : SV_Target
{
	float2 s_uv;

	// x_scale = 128 / 256
	// y_scale = 256 / 240
	s_uv.x = input.texcoord0.x / 0.5;
	s_uv.y = input.texcoord0.y / 1.0666667;

	// single channel --> rgba index.
	float2 i_idx;
	s_uv.x = s_uv.x * 128.0;
	i_idx.x = s_uv.x / 4.0;
	i_idx.y = s_uv.x - 4.0 * floor(i_idx.x);

	int c_idx = int(i_idx.y);
	s_uv.x = i_idx.x / 128.0;

	float s_idx = texture1.Sample(textureSamplerState1, s_uv)[c_idx];
	// index [0.0, 255.0]
	float index = floor(s_idx * 256.0);

	float2 v_idx;
	v_idx.y = index / 16.0;
	v_idx.x = index - 16.0 * floor(v_idx.y);
	v_idx.y /= 16.0;
	v_idx.x /= 16.0;

	float4 tc = texture0.Sample(textureSamplerState0, v_idx);

	return tc;
}

