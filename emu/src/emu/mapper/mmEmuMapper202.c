/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper202.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper202_Init(struct mmEmuMapper202* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper202_Reset;
    p->super.Write = &mmEmuMapper202_Write;
    //p->super.Read = &mmEmuMapper202_Read;
    //p->super.ReadLow = &mmEmuMapper202_ReadLow;
    p->super.WriteLow = &mmEmuMapper202_WriteLow;
    //p->super.ExRead = &mmEmuMapper202_ExRead;
    p->super.ExWrite = &mmEmuMapper202_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper202_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper202_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper202_HSync;
    //p->super.VSync = &mmEmuMapper202_VSync;
    //p->super.Clock = &mmEmuMapper202_Clock;
    //p->super.PPULatch = &mmEmuMapper202_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper202_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper202_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper202_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper202_IsStateSave;
    //p->super.SaveState = &mmEmuMapper202_SaveState;
    //p->super.LoadState = &mmEmuMapper202_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper202_Destroy(struct mmEmuMapper202* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper202_WriteSub(struct mmEmuMapper202* p, mmWord_t addr, mmByte_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t bank = (addr >> 1) & 0x07;

    mmEmuMmu_SetPROM16KBank(mmu, 4, bank);
    if ((addr & 0x0C) == 0x0C)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 6, bank + 1);
    }
    else
    {
        mmEmuMmu_SetPROM16KBank(mmu, 6, bank);
    }
    mmEmuMmu_SetVROM8KBank(mmu, bank);

    if (addr & 0x01)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
}

MM_EXPORT_EMU void mmEmuMapper202_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper202* p = (struct mmEmuMapper202*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM16KBank(mmu, 4, 6);
    mmEmuMmu_SetPROM16KBank(mmu, 6, 7);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper202_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper202* p = (struct mmEmuMapper202*)(super);

    if (addr >= 0x4020)
    {
        mmEmuMapper202_WriteSub(p, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper202_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper202* p = (struct mmEmuMapper202*)(super);

    mmEmuMapper202_WriteSub(p, addr, data);
}
MM_EXPORT_EMU void mmEmuMapper202_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper202* p = (struct mmEmuMapper202*)(super);

    mmEmuMapper202_WriteSub(p, addr, data);
}

static struct mmEmuMapper* __static_mmEmuMapper202_Produce(void)
{
    struct mmEmuMapper202* p = (struct mmEmuMapper202*)mmMalloc(sizeof(struct mmEmuMapper202));
    mmEmuMapper202_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper202_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper202* p = (struct mmEmuMapper202*)(m);
    mmEmuMapper202_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER202 =
{
    &__static_mmEmuMapper202_Produce,
    &__static_mmEmuMapper202_Recycle,
};
