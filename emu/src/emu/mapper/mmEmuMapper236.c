/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper236.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper236_Init(struct mmEmuMapper236* p)
{
    mmEmuMapper_Init(&p->super);

    p->bank = 0;
    p->mode = 0;

    //
    p->super.Reset = &mmEmuMapper236_Reset;
    p->super.Write = &mmEmuMapper236_Write;
    //p->super.Read = &mmEmuMapper236_Read;
    //p->super.ReadLow = &mmEmuMapper236_ReadLow;
    //p->super.WriteLow = &mmEmuMapper236_WriteLow;
    //p->super.ExRead = &mmEmuMapper236_ExRead;
    //p->super.ExWrite = &mmEmuMapper236_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper236_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper236_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper236_HSync;
    //p->super.VSync = &mmEmuMapper236_VSync;
    //p->super.Clock = &mmEmuMapper236_Clock;
    //p->super.PPULatch = &mmEmuMapper236_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper236_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper236_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper236_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper236_IsStateSave;
    p->super.SaveState = &mmEmuMapper236_SaveState;
    p->super.LoadState = &mmEmuMapper236_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper236_Destroy(struct mmEmuMapper236* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->bank = 0;
    p->mode = 0;
}

MM_EXPORT_EMU void mmEmuMapper236_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper236* p = (struct mmEmuMapper236*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    p->bank = p->mode = 0;
}
MM_EXPORT_EMU void mmEmuMapper236_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper236* p = (struct mmEmuMapper236*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x8000 && addr <= 0xBFFF)
    {
        p->bank = ((addr & 0x03) << 4) | (p->bank & 0x07);
    }
    else
    {
        p->bank = (addr & 0x07) | (p->bank & 0x30);
        p->mode = addr & 0x30;
    }

    if (addr & 0x20)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }

    switch (p->mode)
    {
    case 0x00:
        p->bank |= 0x08;
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->bank * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->bank * 2 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, (p->bank | 0x07) * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 7, (p->bank | 0x07) * 2 + 1);
        break;
    case 0x10:
        p->bank |= 0x37;
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->bank * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->bank * 2 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, (p->bank | 0x07) * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 7, (p->bank | 0x07) * 2 + 1);
        break;
    case 0x20:
        p->bank |= 0x08;
        mmEmuMmu_SetPROM8KBank(mmu, 4, (p->bank & 0xFE) * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, (p->bank & 0xFE) * 2 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, (p->bank & 0xFE) * 2 + 2);
        mmEmuMmu_SetPROM8KBank(mmu, 7, (p->bank & 0xFE) * 2 + 3);
        break;
    case 0x30:
        p->bank |= 0x08;
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->bank * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->bank * 2 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, p->bank * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 7, p->bank * 2 + 1);
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper236_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper236_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper236* p = (struct mmEmuMapper236*)(super);

    buffer[0] = p->bank;
    buffer[1] = p->mode;
}
MM_EXPORT_EMU void mmEmuMapper236_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper236* p = (struct mmEmuMapper236*)(super);

    p->bank = buffer[0];
    p->mode = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper236_Produce(void)
{
    struct mmEmuMapper236* p = (struct mmEmuMapper236*)mmMalloc(sizeof(struct mmEmuMapper236));
    mmEmuMapper236_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper236_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper236* p = (struct mmEmuMapper236*)(m);
    mmEmuMapper236_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER236 =
{
    &__static_mmEmuMapper236_Produce,
    &__static_mmEmuMapper236_Recycle,
};
