/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapperRegister.h"

// 000 - 015
#include "emu/mapper/mmEmuMapper000.h"
#include "emu/mapper/mmEmuMapper001.h"
#include "emu/mapper/mmEmuMapper002.h"
#include "emu/mapper/mmEmuMapper003.h"
#include "emu/mapper/mmEmuMapper004.h"
#include "emu/mapper/mmEmuMapper005.h"
#include "emu/mapper/mmEmuMapper006.h"
#include "emu/mapper/mmEmuMapper007.h"
#include "emu/mapper/mmEmuMapper008.h"
#include "emu/mapper/mmEmuMapper009.h"
#include "emu/mapper/mmEmuMapper010.h"
#include "emu/mapper/mmEmuMapper011.h"
#include "emu/mapper/mmEmuMapper012.h"
#include "emu/mapper/mmEmuMapper013.h"
//#include "emu/mapper/mmEmuMapper014.h"
#include "emu/mapper/mmEmuMapper015.h"

// 016 - 031
#include "emu/mapper/mmEmuMapper016.h"
#include "emu/mapper/mmEmuMapper017.h"
#include "emu/mapper/mmEmuMapper018.h"
#include "emu/mapper/mmEmuMapper019.h"
#include "emu/mapper/mmEmuMapper020.h"
#include "emu/mapper/mmEmuMapper021.h"
#include "emu/mapper/mmEmuMapper022.h"
#include "emu/mapper/mmEmuMapper023.h"
#include "emu/mapper/mmEmuMapper024.h"
#include "emu/mapper/mmEmuMapper025.h"
#include "emu/mapper/mmEmuMapper026.h"
#include "emu/mapper/mmEmuMapper027.h"
//#include "emu/mapper/mmEmuMapper028.h"
//#include "emu/mapper/mmEmuMapper029.h"
//#include "emu/mapper/mmEmuMapper030.h"
//#include "emu/mapper/mmEmuMapper031.h"

// 032 - 047
#include "emu/mapper/mmEmuMapper032.h"
#include "emu/mapper/mmEmuMapper033.h"
#include "emu/mapper/mmEmuMapper034.h"
//#include "emu/mapper/mmEmuMapper035.h"
//#include "emu/mapper/mmEmuMapper036.h"
//#include "emu/mapper/mmEmuMapper037.h"
//#include "emu/mapper/mmEmuMapper038.h"
//#include "emu/mapper/mmEmuMapper039.h"
#include "emu/mapper/mmEmuMapper040.h"
#include "emu/mapper/mmEmuMapper041.h"
#include "emu/mapper/mmEmuMapper042.h"
#include "emu/mapper/mmEmuMapper043.h"
#include "emu/mapper/mmEmuMapper044.h"
#include "emu/mapper/mmEmuMapper045.h"
#include "emu/mapper/mmEmuMapper046.h"
#include "emu/mapper/mmEmuMapper047.h"

// 048 - 063
#include "emu/mapper/mmEmuMapper048.h"
//#include "emu/mapper/mmEmuMapper049.h"
#include "emu/mapper/mmEmuMapper050.h"
#include "emu/mapper/mmEmuMapper051.h"
//#include "emu/mapper/mmEmuMapper052.h"
//#include "emu/mapper/mmEmuMapper053.h"
//#include "emu/mapper/mmEmuMapper054.h"
//#include "emu/mapper/mmEmuMapper055.h"
//#include "emu/mapper/mmEmuMapper056.h"
#include "emu/mapper/mmEmuMapper057.h"
#include "emu/mapper/mmEmuMapper058.h"
//#include "emu/mapper/mmEmuMapper059.h"
#include "emu/mapper/mmEmuMapper060.h"
#include "emu/mapper/mmEmuMapper061.h"
#include "emu/mapper/mmEmuMapper062.h"
//#include "emu/mapper/mmEmuMapper063.h"

// 064 - 079
#include "emu/mapper/mmEmuMapper064.h"
#include "emu/mapper/mmEmuMapper065.h"
#include "emu/mapper/mmEmuMapper066.h"
#include "emu/mapper/mmEmuMapper067.h"
#include "emu/mapper/mmEmuMapper068.h"
#include "emu/mapper/mmEmuMapper069.h"
#include "emu/mapper/mmEmuMapper070.h"
#include "emu/mapper/mmEmuMapper071.h"
#include "emu/mapper/mmEmuMapper072.h"
#include "emu/mapper/mmEmuMapper073.h"
#include "emu/mapper/mmEmuMapper074.h"
#include "emu/mapper/mmEmuMapper075.h"
#include "emu/mapper/mmEmuMapper076.h"
#include "emu/mapper/mmEmuMapper077.h"
#include "emu/mapper/mmEmuMapper078.h"
#include "emu/mapper/mmEmuMapper079.h"

// 080 - 095
#include "emu/mapper/mmEmuMapper080.h"
//#include "emu/mapper/mmEmuMapper081.h"
#include "emu/mapper/mmEmuMapper082.h"
#include "emu/mapper/mmEmuMapper083.h"
//#include "emu/mapper/mmEmuMapper084.h"
#include "emu/mapper/mmEmuMapper085.h"
#include "emu/mapper/mmEmuMapper086.h"
#include "emu/mapper/mmEmuMapper087.h"
#include "emu/mapper/mmEmuMapper088.h"
#include "emu/mapper/mmEmuMapper089.h"
#include "emu/mapper/mmEmuMapper090.h"
#include "emu/mapper/mmEmuMapper091.h"
#include "emu/mapper/mmEmuMapper092.h"
#include "emu/mapper/mmEmuMapper093.h"
#include "emu/mapper/mmEmuMapper094.h"
#include "emu/mapper/mmEmuMapper095.h"

// 096 - 111
#include "emu/mapper/mmEmuMapper096.h"
#include "emu/mapper/mmEmuMapper097.h"
//#include "emu/mapper/mmEmuMapper098.h"
#include "emu/mapper/mmEmuMapper099.h"
#include "emu/mapper/mmEmuMapper100.h"
#include "emu/mapper/mmEmuMapper101.h"
//#include "emu/mapper/mmEmuMapper102.h"
//#include "emu/mapper/mmEmuMapper103.h"
//#include "emu/mapper/mmEmuMapper104.h"
#include "emu/mapper/mmEmuMapper105.h"
//#include "emu/mapper/mmEmuMapper106.h"
#include "emu/mapper/mmEmuMapper107.h"
#include "emu/mapper/mmEmuMapper108.h"
#include "emu/mapper/mmEmuMapper109.h"
#include "emu/mapper/mmEmuMapper110.h"
//#include "emu/mapper/mmEmuMapper111.h"

// 112 - 127
#include "emu/mapper/mmEmuMapper112.h"
#include "emu/mapper/mmEmuMapper113.h"
#include "emu/mapper/mmEmuMapper114.h"
#include "emu/mapper/mmEmuMapper115.h"
#include "emu/mapper/mmEmuMapper116.h"
#include "emu/mapper/mmEmuMapper117.h"
#include "emu/mapper/mmEmuMapper118.h"
#include "emu/mapper/mmEmuMapper119.h"
//#include "emu/mapper/mmEmuMapper120.h"
//#include "emu/mapper/mmEmuMapper121.h"
#include "emu/mapper/mmEmuMapper122.h"
//#include "emu/mapper/mmEmuMapper123.h"
//#include "emu/mapper/mmEmuMapper124.h"
//#include "emu/mapper/mmEmuMapper125.h"
//#include "emu/mapper/mmEmuMapper126.h"
//#include "emu/mapper/mmEmuMapper127.h"

// 128 - 143
//#include "emu/mapper/mmEmuMapper128.h"
//#include "emu/mapper/mmEmuMapper129.h"
//#include "emu/mapper/mmEmuMapper130.h"
//#include "emu/mapper/mmEmuMapper131.h"
//#include "emu/mapper/mmEmuMapper132.h"
#include "emu/mapper/mmEmuMapper133.h"
#include "emu/mapper/mmEmuMapper134.h"
#include "emu/mapper/mmEmuMapper135.h"
//#include "emu/mapper/mmEmuMapper136.h"
//#include "emu/mapper/mmEmuMapper137.h"
//#include "emu/mapper/mmEmuMapper138.h"
//#include "emu/mapper/mmEmuMapper139.h"
#include "emu/mapper/mmEmuMapper140.h"
//#include "emu/mapper/mmEmuMapper141.h"
#include "emu/mapper/mmEmuMapper142.h"
//#include "emu/mapper/mmEmuMapper143.h"

// 144 - 159
//#include "emu/mapper/mmEmuMapper144.h"
//#include "emu/mapper/mmEmuMapper145.h"
//#include "emu/mapper/mmEmuMapper146.h"
//#include "emu/mapper/mmEmuMapper147.h"
//#include "emu/mapper/mmEmuMapper148.h"
//#include "emu/mapper/mmEmuMapper149.h"
//#include "emu/mapper/mmEmuMapper150.h"
#include "emu/mapper/mmEmuMapper151.h"
//#include "emu/mapper/mmEmuMapper152.h"
//#include "emu/mapper/mmEmuMapper153.h"
//#include "emu/mapper/mmEmuMapper154.h"
//#include "emu/mapper/mmEmuMapper155.h"
//#include "emu/mapper/mmEmuMapper156.h"
//#include "emu/mapper/mmEmuMapper157.h"
//#include "emu/mapper/mmEmuMapper158.h"
//#include "emu/mapper/mmEmuMapper159.h"

// 160 - 175
#include "emu/mapper/mmEmuMapper160.h"
//#include "emu/mapper/mmEmuMapper161.h"
#include "emu/mapper/mmEmuMapper162.h"
#include "emu/mapper/mmEmuMapper163.h"
#include "emu/mapper/mmEmuMapper164.h"
#include "emu/mapper/mmEmuMapper165.h"
//#include "emu/mapper/mmEmuMapper166.h"
#include "emu/mapper/mmEmuMapper167.h"
#include "emu/mapper/mmEmuMapper168.h"
#include "emu/mapper/mmEmuMapper169.h"
#include "emu/mapper/mmEmuMapper170.h"
//#include "emu/mapper/mmEmuMapper171.h"
//#include "emu/mapper/mmEmuMapper172.h"
//#include "emu/mapper/mmEmuMapper173.h"
//#include "emu/mapper/mmEmuMapper174.h"
#include "emu/mapper/mmEmuMapper175.h"

// 176 - 191
#include "emu/mapper/mmEmuMapper176.h"
#include "emu/mapper/mmEmuMapper177.h"
#include "emu/mapper/mmEmuMapper178.h"
//#include "emu/mapper/mmEmuMapper179.h"
#include "emu/mapper/mmEmuMapper180.h"
#include "emu/mapper/mmEmuMapper181.h"
#include "emu/mapper/mmEmuMapper182.h"
#include "emu/mapper/mmEmuMapper183.h"
//#include "emu/mapper/mmEmuMapper184.h"
#include "emu/mapper/mmEmuMapper185.h"
//#include "emu/mapper/mmEmuMapper186.h"
#include "emu/mapper/mmEmuMapper187.h"
#include "emu/mapper/mmEmuMapper188.h"
#include "emu/mapper/mmEmuMapper189.h"
#include "emu/mapper/mmEmuMapper190.h"
#include "emu/mapper/mmEmuMapper191.h"

// 192 - 207
#include "emu/mapper/mmEmuMapper192.h"
#include "emu/mapper/mmEmuMapper193.h"
#include "emu/mapper/mmEmuMapper194.h"
#include "emu/mapper/mmEmuMapper195.h"
//#include "emu/mapper/mmEmuMapper196.h"
//#include "emu/mapper/mmEmuMapper197.h"
#include "emu/mapper/mmEmuMapper198.h"
//#include "emu/mapper/mmEmuMapper199.h"
#include "emu/mapper/mmEmuMapper200.h"
#include "emu/mapper/mmEmuMapper201.h"
#include "emu/mapper/mmEmuMapper202.h"
//#include "emu/mapper/mmEmuMapper203.h"
//#include "emu/mapper/mmEmuMapper204.h"
//#include "emu/mapper/mmEmuMapper205.h"
//#include "emu/mapper/mmEmuMapper206.h"
//#include "emu/mapper/mmEmuMapper207.h"

// 208 - 223
//#include "emu/mapper/mmEmuMapper208.h"
//#include "emu/mapper/mmEmuMapper209.h"
//#include "emu/mapper/mmEmuMapper210.h"
//#include "emu/mapper/mmEmuMapper211.h"
//#include "emu/mapper/mmEmuMapper212.h"
//#include "emu/mapper/mmEmuMapper213.h"
//#include "emu/mapper/mmEmuMapper214.h"
//#include "emu/mapper/mmEmuMapper215.h"
//#include "emu/mapper/mmEmuMapper216.h"
//#include "emu/mapper/mmEmuMapper217.h"
//#include "emu/mapper/mmEmuMapper218.h"
//#include "emu/mapper/mmEmuMapper219.h"
//#include "emu/mapper/mmEmuMapper220.h"
//#include "emu/mapper/mmEmuMapper221.h"
#include "emu/mapper/mmEmuMapper222.h"
//#include "emu/mapper/mmEmuMapper223.h"

// 224 - 239
//#include "emu/mapper/mmEmuMapper224.h"
#include "emu/mapper/mmEmuMapper225.h"
#include "emu/mapper/mmEmuMapper226.h"
#include "emu/mapper/mmEmuMapper227.h"
#include "emu/mapper/mmEmuMapper228.h"
#include "emu/mapper/mmEmuMapper229.h"
#include "emu/mapper/mmEmuMapper230.h"
#include "emu/mapper/mmEmuMapper231.h"
#include "emu/mapper/mmEmuMapper232.h"
#include "emu/mapper/mmEmuMapper233.h"
#include "emu/mapper/mmEmuMapper234.h"
#include "emu/mapper/mmEmuMapper235.h"
#include "emu/mapper/mmEmuMapper236.h"
//#include "emu/mapper/mmEmuMapper237.h"
//#include "emu/mapper/mmEmuMapper238.h"
//#include "emu/mapper/mmEmuMapper239.h"

// 240 - 255
#include "emu/mapper/mmEmuMapper240.h"
#include "emu/mapper/mmEmuMapper241.h"
#include "emu/mapper/mmEmuMapper242.h"
#include "emu/mapper/mmEmuMapper243.h"
#include "emu/mapper/mmEmuMapper244.h"
#include "emu/mapper/mmEmuMapper245.h"
#include "emu/mapper/mmEmuMapper246.h"
//#include "emu/mapper/mmEmuMapper247.h"
#include "emu/mapper/mmEmuMapper248.h"
#include "emu/mapper/mmEmuMapper249.h"
//#include "emu/mapper/mmEmuMapper250.h"
#include "emu/mapper/mmEmuMapper251.h"
#include "emu/mapper/mmEmuMapper252.h"
#include "emu/mapper/mmEmuMapper253.h"
#include "emu/mapper/mmEmuMapper254.h"
#include "emu/mapper/mmEmuMapper255.h"

// 256 - 256
#include "emu/mapper/mmEmuMapper256.h"

MM_EXPORT_EMU const struct mmEmuMapperCreator* MM_EMU_MAPPER_BASE_CREATOR[256 + 1] =
{
    // 000 - 015
    &MM_EMU_MAPPER_CREATOR_MAPPER000,
    &MM_EMU_MAPPER_CREATOR_MAPPER001,
    &MM_EMU_MAPPER_CREATOR_MAPPER002,
    &MM_EMU_MAPPER_CREATOR_MAPPER003,
    &MM_EMU_MAPPER_CREATOR_MAPPER004,
    &MM_EMU_MAPPER_CREATOR_MAPPER005,
    &MM_EMU_MAPPER_CREATOR_MAPPER006,
    &MM_EMU_MAPPER_CREATOR_MAPPER007,
    &MM_EMU_MAPPER_CREATOR_MAPPER008,
    &MM_EMU_MAPPER_CREATOR_MAPPER009,
    &MM_EMU_MAPPER_CREATOR_MAPPER010,
    &MM_EMU_MAPPER_CREATOR_MAPPER011,
    &MM_EMU_MAPPER_CREATOR_MAPPER012,
    &MM_EMU_MAPPER_CREATOR_MAPPER013,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER014,
    &MM_EMU_MAPPER_CREATOR_MAPPER015,

    // 016 - 031
    &MM_EMU_MAPPER_CREATOR_MAPPER016,
    &MM_EMU_MAPPER_CREATOR_MAPPER017,
    &MM_EMU_MAPPER_CREATOR_MAPPER018,
    &MM_EMU_MAPPER_CREATOR_MAPPER019,
    &MM_EMU_MAPPER_CREATOR_MAPPER020,
    &MM_EMU_MAPPER_CREATOR_MAPPER021,
    &MM_EMU_MAPPER_CREATOR_MAPPER022,
    &MM_EMU_MAPPER_CREATOR_MAPPER023,
    &MM_EMU_MAPPER_CREATOR_MAPPER024,
    &MM_EMU_MAPPER_CREATOR_MAPPER025,
    &MM_EMU_MAPPER_CREATOR_MAPPER026,
    &MM_EMU_MAPPER_CREATOR_MAPPER027,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER028,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER029,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER030,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER031,

    // 032 - 047
    &MM_EMU_MAPPER_CREATOR_MAPPER032,
    &MM_EMU_MAPPER_CREATOR_MAPPER033,
    &MM_EMU_MAPPER_CREATOR_MAPPER034,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER035,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER036,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER037,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER038,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER039,
    &MM_EMU_MAPPER_CREATOR_MAPPER040,
    &MM_EMU_MAPPER_CREATOR_MAPPER041,
    &MM_EMU_MAPPER_CREATOR_MAPPER042,
    &MM_EMU_MAPPER_CREATOR_MAPPER043,
    &MM_EMU_MAPPER_CREATOR_MAPPER044,
    &MM_EMU_MAPPER_CREATOR_MAPPER045,
    &MM_EMU_MAPPER_CREATOR_MAPPER046,
    &MM_EMU_MAPPER_CREATOR_MAPPER047,

    // 048 - 063
    &MM_EMU_MAPPER_CREATOR_MAPPER048,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER049,
    &MM_EMU_MAPPER_CREATOR_MAPPER050,
    &MM_EMU_MAPPER_CREATOR_MAPPER051,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER052,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER053,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER054,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER055,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER056,
    &MM_EMU_MAPPER_CREATOR_MAPPER057,
    &MM_EMU_MAPPER_CREATOR_MAPPER058,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER059,
    &MM_EMU_MAPPER_CREATOR_MAPPER060,
    &MM_EMU_MAPPER_CREATOR_MAPPER061,
    &MM_EMU_MAPPER_CREATOR_MAPPER062,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER063,

    // 064 - 079
    &MM_EMU_MAPPER_CREATOR_MAPPER064,
    &MM_EMU_MAPPER_CREATOR_MAPPER065,
    &MM_EMU_MAPPER_CREATOR_MAPPER066,
    &MM_EMU_MAPPER_CREATOR_MAPPER067,
    &MM_EMU_MAPPER_CREATOR_MAPPER068,
    &MM_EMU_MAPPER_CREATOR_MAPPER069,
    &MM_EMU_MAPPER_CREATOR_MAPPER070,
    &MM_EMU_MAPPER_CREATOR_MAPPER071,
    &MM_EMU_MAPPER_CREATOR_MAPPER072,
    &MM_EMU_MAPPER_CREATOR_MAPPER073,
    &MM_EMU_MAPPER_CREATOR_MAPPER074,
    &MM_EMU_MAPPER_CREATOR_MAPPER075,
    &MM_EMU_MAPPER_CREATOR_MAPPER076,
    &MM_EMU_MAPPER_CREATOR_MAPPER077,
    &MM_EMU_MAPPER_CREATOR_MAPPER078,
    &MM_EMU_MAPPER_CREATOR_MAPPER079,

    // 080 - 095
    &MM_EMU_MAPPER_CREATOR_MAPPER080,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER081,
    &MM_EMU_MAPPER_CREATOR_MAPPER082,
    &MM_EMU_MAPPER_CREATOR_MAPPER083,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER084,
    &MM_EMU_MAPPER_CREATOR_MAPPER085,
    &MM_EMU_MAPPER_CREATOR_MAPPER086,
    &MM_EMU_MAPPER_CREATOR_MAPPER087,
    &MM_EMU_MAPPER_CREATOR_MAPPER088,
    &MM_EMU_MAPPER_CREATOR_MAPPER089,
    &MM_EMU_MAPPER_CREATOR_MAPPER090,
    &MM_EMU_MAPPER_CREATOR_MAPPER091,
    &MM_EMU_MAPPER_CREATOR_MAPPER092,
    &MM_EMU_MAPPER_CREATOR_MAPPER093,
    &MM_EMU_MAPPER_CREATOR_MAPPER094,
    &MM_EMU_MAPPER_CREATOR_MAPPER095,

    // 096 - 111
    &MM_EMU_MAPPER_CREATOR_MAPPER096,
    &MM_EMU_MAPPER_CREATOR_MAPPER097,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER098,
    &MM_EMU_MAPPER_CREATOR_MAPPER099,
    &MM_EMU_MAPPER_CREATOR_MAPPER100,
    &MM_EMU_MAPPER_CREATOR_MAPPER101,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER102,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER103,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER104,
    &MM_EMU_MAPPER_CREATOR_MAPPER105,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER106,
    &MM_EMU_MAPPER_CREATOR_MAPPER107,
    &MM_EMU_MAPPER_CREATOR_MAPPER108,
    &MM_EMU_MAPPER_CREATOR_MAPPER109,
    &MM_EMU_MAPPER_CREATOR_MAPPER110,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER111,

    // 112 - 127
    &MM_EMU_MAPPER_CREATOR_MAPPER112,
    &MM_EMU_MAPPER_CREATOR_MAPPER113,
    &MM_EMU_MAPPER_CREATOR_MAPPER114,
    &MM_EMU_MAPPER_CREATOR_MAPPER115,
    &MM_EMU_MAPPER_CREATOR_MAPPER116,
    &MM_EMU_MAPPER_CREATOR_MAPPER117,
    &MM_EMU_MAPPER_CREATOR_MAPPER118,
    &MM_EMU_MAPPER_CREATOR_MAPPER119,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER120,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER121,
    &MM_EMU_MAPPER_CREATOR_MAPPER122,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER123,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER124,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER125,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER126,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER127,

    // 128 - 143
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER128,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER129,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER130,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER131,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER132,
    &MM_EMU_MAPPER_CREATOR_MAPPER133,
    &MM_EMU_MAPPER_CREATOR_MAPPER134,
    &MM_EMU_MAPPER_CREATOR_MAPPER135,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER136,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER137,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER138,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER139,
    &MM_EMU_MAPPER_CREATOR_MAPPER140,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER141,
    &MM_EMU_MAPPER_CREATOR_MAPPER142,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER143,

    // 144 - 159
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER144,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER145,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER146,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER147,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER148,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER149,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER150,
    &MM_EMU_MAPPER_CREATOR_MAPPER151,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER152,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER153,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER154,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER155,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER156,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER157,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER158,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER159,

    // 160 - 175
    &MM_EMU_MAPPER_CREATOR_MAPPER160,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER161,
    &MM_EMU_MAPPER_CREATOR_MAPPER162,
    &MM_EMU_MAPPER_CREATOR_MAPPER163,
    &MM_EMU_MAPPER_CREATOR_MAPPER164,
    &MM_EMU_MAPPER_CREATOR_MAPPER165,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER166,
    &MM_EMU_MAPPER_CREATOR_MAPPER167,
    &MM_EMU_MAPPER_CREATOR_MAPPER168,
    &MM_EMU_MAPPER_CREATOR_MAPPER169,
    &MM_EMU_MAPPER_CREATOR_MAPPER170,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER171,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER172,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER173,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER174,
    &MM_EMU_MAPPER_CREATOR_MAPPER175,

    // 176 - 191
    &MM_EMU_MAPPER_CREATOR_MAPPER176,
    &MM_EMU_MAPPER_CREATOR_MAPPER177,
    &MM_EMU_MAPPER_CREATOR_MAPPER178,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER179,
    &MM_EMU_MAPPER_CREATOR_MAPPER180,
    &MM_EMU_MAPPER_CREATOR_MAPPER181,
    &MM_EMU_MAPPER_CREATOR_MAPPER182,
    &MM_EMU_MAPPER_CREATOR_MAPPER183,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER184,
    &MM_EMU_MAPPER_CREATOR_MAPPER185,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER186,
    &MM_EMU_MAPPER_CREATOR_MAPPER187,
    &MM_EMU_MAPPER_CREATOR_MAPPER188,
    &MM_EMU_MAPPER_CREATOR_MAPPER189,
    &MM_EMU_MAPPER_CREATOR_MAPPER190,
    &MM_EMU_MAPPER_CREATOR_MAPPER191,

    // 192 - 207
    &MM_EMU_MAPPER_CREATOR_MAPPER192,
    &MM_EMU_MAPPER_CREATOR_MAPPER193,
    &MM_EMU_MAPPER_CREATOR_MAPPER194,
    &MM_EMU_MAPPER_CREATOR_MAPPER195,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER196,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER197,
    &MM_EMU_MAPPER_CREATOR_MAPPER198,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER199,
    &MM_EMU_MAPPER_CREATOR_MAPPER200,
    &MM_EMU_MAPPER_CREATOR_MAPPER201,
    &MM_EMU_MAPPER_CREATOR_MAPPER202,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER203,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER204,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER205,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER206,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER207,

    // 208 - 223
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER208,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER209,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER210,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER211,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER212,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER213,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER214,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER215,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER216,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER217,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER218,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER219,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER220,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER221,
    &MM_EMU_MAPPER_CREATOR_MAPPER222,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER223,

    // 224 - 239
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER224,
    &MM_EMU_MAPPER_CREATOR_MAPPER225,
    &MM_EMU_MAPPER_CREATOR_MAPPER226,
    &MM_EMU_MAPPER_CREATOR_MAPPER227,
    &MM_EMU_MAPPER_CREATOR_MAPPER228,
    &MM_EMU_MAPPER_CREATOR_MAPPER229,
    &MM_EMU_MAPPER_CREATOR_MAPPER230,
    &MM_EMU_MAPPER_CREATOR_MAPPER231,
    &MM_EMU_MAPPER_CREATOR_MAPPER232,
    &MM_EMU_MAPPER_CREATOR_MAPPER233,
    &MM_EMU_MAPPER_CREATOR_MAPPER234,
    &MM_EMU_MAPPER_CREATOR_MAPPER235,
    &MM_EMU_MAPPER_CREATOR_MAPPER236,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER237,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER238,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER239,

    // 240 - 255
    &MM_EMU_MAPPER_CREATOR_MAPPER240,
    &MM_EMU_MAPPER_CREATOR_MAPPER241,
    &MM_EMU_MAPPER_CREATOR_MAPPER242,
    &MM_EMU_MAPPER_CREATOR_MAPPER243,
    &MM_EMU_MAPPER_CREATOR_MAPPER244,
    &MM_EMU_MAPPER_CREATOR_MAPPER245,
    &MM_EMU_MAPPER_CREATOR_MAPPER246,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER247,
    &MM_EMU_MAPPER_CREATOR_MAPPER248,
    &MM_EMU_MAPPER_CREATOR_MAPPER249,
    NULL,//&MM_EMU_MAPPER_CREATOR_MAPPER250,
    &MM_EMU_MAPPER_CREATOR_MAPPER251,
    &MM_EMU_MAPPER_CREATOR_MAPPER252,
    &MM_EMU_MAPPER_CREATOR_MAPPER253,
    &MM_EMU_MAPPER_CREATOR_MAPPER254,
    &MM_EMU_MAPPER_CREATOR_MAPPER255,

    // 256 - 256
    &MM_EMU_MAPPER_CREATOR_MAPPER256,
};

MM_EXPORT_EMU struct mmEmuMapper* mmEmuMapper_BaseProduce(mmUInt32_t id)
{
    struct mmEmuMapper* m = NULL;
    const struct mmEmuMapperCreator* creator = NULL;
    assert(0 <= id && id <= 256 && "mapper id is invalid.");
    creator = MM_EMU_MAPPER_BASE_CREATOR[id];
    if (NULL != creator)
    {
        m = (*(creator->Produce))();
    }
    return m;
}
MM_EXPORT_EMU void mmEmuMapper_BaseRecycle(mmUInt32_t id, struct mmEmuMapper* m)
{
    const struct mmEmuMapperCreator* creator = NULL;
    assert(0 <= id && id <= 256 && "mapper id is invalid.");
    creator = MM_EMU_MAPPER_BASE_CREATOR[id];
    if (NULL != creator)
    {
        (*(creator->Recycle))(m);
    }
}

MM_EXPORT_EMU void mmEmuMapper_RegisterInternal(struct mmEmuMapperFactory* p)
{
    // 000 - 015
    mmEmuMapperFactory_Register(p, 0, &MM_EMU_MAPPER_CREATOR_MAPPER000);
    mmEmuMapperFactory_Register(p, 1, &MM_EMU_MAPPER_CREATOR_MAPPER001);
    mmEmuMapperFactory_Register(p, 2, &MM_EMU_MAPPER_CREATOR_MAPPER002);
    mmEmuMapperFactory_Register(p, 3, &MM_EMU_MAPPER_CREATOR_MAPPER003);
    mmEmuMapperFactory_Register(p, 4, &MM_EMU_MAPPER_CREATOR_MAPPER004);
    mmEmuMapperFactory_Register(p, 5, &MM_EMU_MAPPER_CREATOR_MAPPER005);
    mmEmuMapperFactory_Register(p, 6, &MM_EMU_MAPPER_CREATOR_MAPPER006);
    mmEmuMapperFactory_Register(p, 7, &MM_EMU_MAPPER_CREATOR_MAPPER007);
    mmEmuMapperFactory_Register(p, 8, &MM_EMU_MAPPER_CREATOR_MAPPER008);
    mmEmuMapperFactory_Register(p, 9, &MM_EMU_MAPPER_CREATOR_MAPPER009);
    mmEmuMapperFactory_Register(p, 10, &MM_EMU_MAPPER_CREATOR_MAPPER010);
    mmEmuMapperFactory_Register(p, 11, &MM_EMU_MAPPER_CREATOR_MAPPER011);
    mmEmuMapperFactory_Register(p, 12, &MM_EMU_MAPPER_CREATOR_MAPPER012);
    mmEmuMapperFactory_Register(p, 13, &MM_EMU_MAPPER_CREATOR_MAPPER013);
    //mmEmuMapperFactory_Register(p,  14, &MM_EMU_MAPPER_CREATOR_MAPPER014);
    mmEmuMapperFactory_Register(p, 15, &MM_EMU_MAPPER_CREATOR_MAPPER015);

    // 016 - 031
    mmEmuMapperFactory_Register(p, 16, &MM_EMU_MAPPER_CREATOR_MAPPER016);
    mmEmuMapperFactory_Register(p, 17, &MM_EMU_MAPPER_CREATOR_MAPPER017);
    mmEmuMapperFactory_Register(p, 18, &MM_EMU_MAPPER_CREATOR_MAPPER018);
    mmEmuMapperFactory_Register(p, 19, &MM_EMU_MAPPER_CREATOR_MAPPER019);
    mmEmuMapperFactory_Register(p, 20, &MM_EMU_MAPPER_CREATOR_MAPPER020);
    mmEmuMapperFactory_Register(p, 21, &MM_EMU_MAPPER_CREATOR_MAPPER021);
    mmEmuMapperFactory_Register(p, 22, &MM_EMU_MAPPER_CREATOR_MAPPER022);
    mmEmuMapperFactory_Register(p, 23, &MM_EMU_MAPPER_CREATOR_MAPPER023);
    mmEmuMapperFactory_Register(p, 24, &MM_EMU_MAPPER_CREATOR_MAPPER024);
    mmEmuMapperFactory_Register(p, 25, &MM_EMU_MAPPER_CREATOR_MAPPER025);
    mmEmuMapperFactory_Register(p, 26, &MM_EMU_MAPPER_CREATOR_MAPPER026);
    mmEmuMapperFactory_Register(p, 27, &MM_EMU_MAPPER_CREATOR_MAPPER027);
    //mmEmuMapperFactory_Register(p,  28, &MM_EMU_MAPPER_CREATOR_MAPPER028);
    //mmEmuMapperFactory_Register(p,  29, &MM_EMU_MAPPER_CREATOR_MAPPER029);
    //mmEmuMapperFactory_Register(p,  30, &MM_EMU_MAPPER_CREATOR_MAPPER030);
    //mmEmuMapperFactory_Register(p,  31, &MM_EMU_MAPPER_CREATOR_MAPPER031);

    // 032 - 047
    mmEmuMapperFactory_Register(p, 32, &MM_EMU_MAPPER_CREATOR_MAPPER032);
    mmEmuMapperFactory_Register(p, 33, &MM_EMU_MAPPER_CREATOR_MAPPER033);
    mmEmuMapperFactory_Register(p, 34, &MM_EMU_MAPPER_CREATOR_MAPPER034);
    //mmEmuMapperFactory_Register(p,  35, &MM_EMU_MAPPER_CREATOR_MAPPER035);
    //mmEmuMapperFactory_Register(p,  36, &MM_EMU_MAPPER_CREATOR_MAPPER036);
    //mmEmuMapperFactory_Register(p,  37, &MM_EMU_MAPPER_CREATOR_MAPPER037);
    //mmEmuMapperFactory_Register(p,  38, &MM_EMU_MAPPER_CREATOR_MAPPER038);
    //mmEmuMapperFactory_Register(p,  39, &MM_EMU_MAPPER_CREATOR_MAPPER039);
    mmEmuMapperFactory_Register(p, 40, &MM_EMU_MAPPER_CREATOR_MAPPER040);
    mmEmuMapperFactory_Register(p, 41, &MM_EMU_MAPPER_CREATOR_MAPPER041);
    mmEmuMapperFactory_Register(p, 42, &MM_EMU_MAPPER_CREATOR_MAPPER042);
    mmEmuMapperFactory_Register(p, 43, &MM_EMU_MAPPER_CREATOR_MAPPER043);
    mmEmuMapperFactory_Register(p, 44, &MM_EMU_MAPPER_CREATOR_MAPPER044);
    mmEmuMapperFactory_Register(p, 45, &MM_EMU_MAPPER_CREATOR_MAPPER045);
    mmEmuMapperFactory_Register(p, 46, &MM_EMU_MAPPER_CREATOR_MAPPER046);
    mmEmuMapperFactory_Register(p, 47, &MM_EMU_MAPPER_CREATOR_MAPPER047);

    // 048 - 063
    mmEmuMapperFactory_Register(p, 48, &MM_EMU_MAPPER_CREATOR_MAPPER048);
    //mmEmuMapperFactory_Register(p,  49, &MM_EMU_MAPPER_CREATOR_MAPPER049);
    mmEmuMapperFactory_Register(p, 50, &MM_EMU_MAPPER_CREATOR_MAPPER050);
    mmEmuMapperFactory_Register(p, 51, &MM_EMU_MAPPER_CREATOR_MAPPER051);
    //mmEmuMapperFactory_Register(p,  52, &MM_EMU_MAPPER_CREATOR_MAPPER052);
    //mmEmuMapperFactory_Register(p,  53, &MM_EMU_MAPPER_CREATOR_MAPPER053);
    //mmEmuMapperFactory_Register(p,  54, &MM_EMU_MAPPER_CREATOR_MAPPER054);
    //mmEmuMapperFactory_Register(p,  55, &MM_EMU_MAPPER_CREATOR_MAPPER055);
    //mmEmuMapperFactory_Register(p,  56, &MM_EMU_MAPPER_CREATOR_MAPPER056);
    mmEmuMapperFactory_Register(p, 57, &MM_EMU_MAPPER_CREATOR_MAPPER057);
    mmEmuMapperFactory_Register(p, 58, &MM_EMU_MAPPER_CREATOR_MAPPER058);
    //mmEmuMapperFactory_Register(p,  59, &MM_EMU_MAPPER_CREATOR_MAPPER059);
    mmEmuMapperFactory_Register(p, 60, &MM_EMU_MAPPER_CREATOR_MAPPER060);
    mmEmuMapperFactory_Register(p, 61, &MM_EMU_MAPPER_CREATOR_MAPPER061);
    mmEmuMapperFactory_Register(p, 62, &MM_EMU_MAPPER_CREATOR_MAPPER062);
    //mmEmuMapperFactory_Register(p,  63, &MM_EMU_MAPPER_CREATOR_MAPPER063);

    // 064 - 079
    mmEmuMapperFactory_Register(p, 64, &MM_EMU_MAPPER_CREATOR_MAPPER064);
    mmEmuMapperFactory_Register(p, 65, &MM_EMU_MAPPER_CREATOR_MAPPER065);
    mmEmuMapperFactory_Register(p, 66, &MM_EMU_MAPPER_CREATOR_MAPPER066);
    mmEmuMapperFactory_Register(p, 67, &MM_EMU_MAPPER_CREATOR_MAPPER067);
    mmEmuMapperFactory_Register(p, 68, &MM_EMU_MAPPER_CREATOR_MAPPER068);
    mmEmuMapperFactory_Register(p, 69, &MM_EMU_MAPPER_CREATOR_MAPPER069);
    mmEmuMapperFactory_Register(p, 70, &MM_EMU_MAPPER_CREATOR_MAPPER070);
    mmEmuMapperFactory_Register(p, 71, &MM_EMU_MAPPER_CREATOR_MAPPER071);
    mmEmuMapperFactory_Register(p, 72, &MM_EMU_MAPPER_CREATOR_MAPPER072);
    mmEmuMapperFactory_Register(p, 73, &MM_EMU_MAPPER_CREATOR_MAPPER073);
    mmEmuMapperFactory_Register(p, 74, &MM_EMU_MAPPER_CREATOR_MAPPER074);
    mmEmuMapperFactory_Register(p, 75, &MM_EMU_MAPPER_CREATOR_MAPPER075);
    mmEmuMapperFactory_Register(p, 76, &MM_EMU_MAPPER_CREATOR_MAPPER076);
    mmEmuMapperFactory_Register(p, 77, &MM_EMU_MAPPER_CREATOR_MAPPER077);
    mmEmuMapperFactory_Register(p, 78, &MM_EMU_MAPPER_CREATOR_MAPPER078);
    mmEmuMapperFactory_Register(p, 79, &MM_EMU_MAPPER_CREATOR_MAPPER079);

    // 080 - 095
    mmEmuMapperFactory_Register(p, 80, &MM_EMU_MAPPER_CREATOR_MAPPER080);
    //mmEmuMapperFactory_Register(p,  81, &MM_EMU_MAPPER_CREATOR_MAPPER081);
    mmEmuMapperFactory_Register(p, 82, &MM_EMU_MAPPER_CREATOR_MAPPER082);
    mmEmuMapperFactory_Register(p, 83, &MM_EMU_MAPPER_CREATOR_MAPPER083);
    //mmEmuMapperFactory_Register(p,  84, &MM_EMU_MAPPER_CREATOR_MAPPER084);
    mmEmuMapperFactory_Register(p, 85, &MM_EMU_MAPPER_CREATOR_MAPPER085);
    mmEmuMapperFactory_Register(p, 86, &MM_EMU_MAPPER_CREATOR_MAPPER086);
    mmEmuMapperFactory_Register(p, 87, &MM_EMU_MAPPER_CREATOR_MAPPER087);
    mmEmuMapperFactory_Register(p, 88, &MM_EMU_MAPPER_CREATOR_MAPPER088);
    mmEmuMapperFactory_Register(p, 89, &MM_EMU_MAPPER_CREATOR_MAPPER089);
    mmEmuMapperFactory_Register(p, 90, &MM_EMU_MAPPER_CREATOR_MAPPER090);
    mmEmuMapperFactory_Register(p, 91, &MM_EMU_MAPPER_CREATOR_MAPPER091);
    mmEmuMapperFactory_Register(p, 92, &MM_EMU_MAPPER_CREATOR_MAPPER092);
    mmEmuMapperFactory_Register(p, 93, &MM_EMU_MAPPER_CREATOR_MAPPER093);
    mmEmuMapperFactory_Register(p, 94, &MM_EMU_MAPPER_CREATOR_MAPPER094);
    mmEmuMapperFactory_Register(p, 95, &MM_EMU_MAPPER_CREATOR_MAPPER095);

    // 096 - 111
    mmEmuMapperFactory_Register(p, 96, &MM_EMU_MAPPER_CREATOR_MAPPER096);
    mmEmuMapperFactory_Register(p, 97, &MM_EMU_MAPPER_CREATOR_MAPPER097);
    //mmEmuMapperFactory_Register(p,  98, &MM_EMU_MAPPER_CREATOR_MAPPER098);
    mmEmuMapperFactory_Register(p, 99, &MM_EMU_MAPPER_CREATOR_MAPPER099);
    mmEmuMapperFactory_Register(p, 100, &MM_EMU_MAPPER_CREATOR_MAPPER100);
    mmEmuMapperFactory_Register(p, 101, &MM_EMU_MAPPER_CREATOR_MAPPER101);
    //mmEmuMapperFactory_Register(p, 102, &MM_EMU_MAPPER_CREATOR_MAPPER102);
    //mmEmuMapperFactory_Register(p, 103, &MM_EMU_MAPPER_CREATOR_MAPPER103);
    //mmEmuMapperFactory_Register(p, 104, &MM_EMU_MAPPER_CREATOR_MAPPER104);
    mmEmuMapperFactory_Register(p, 105, &MM_EMU_MAPPER_CREATOR_MAPPER105);
    //mmEmuMapperFactory_Register(p, 106, &MM_EMU_MAPPER_CREATOR_MAPPER106);
    mmEmuMapperFactory_Register(p, 107, &MM_EMU_MAPPER_CREATOR_MAPPER107);
    mmEmuMapperFactory_Register(p, 108, &MM_EMU_MAPPER_CREATOR_MAPPER108);
    mmEmuMapperFactory_Register(p, 109, &MM_EMU_MAPPER_CREATOR_MAPPER109);
    mmEmuMapperFactory_Register(p, 110, &MM_EMU_MAPPER_CREATOR_MAPPER110);
    //mmEmuMapperFactory_Register(p, 111, &MM_EMU_MAPPER_CREATOR_MAPPER111);

    // 112 - 127
    mmEmuMapperFactory_Register(p, 112, &MM_EMU_MAPPER_CREATOR_MAPPER112);
    mmEmuMapperFactory_Register(p, 113, &MM_EMU_MAPPER_CREATOR_MAPPER113);
    mmEmuMapperFactory_Register(p, 114, &MM_EMU_MAPPER_CREATOR_MAPPER114);
    mmEmuMapperFactory_Register(p, 115, &MM_EMU_MAPPER_CREATOR_MAPPER115);
    mmEmuMapperFactory_Register(p, 116, &MM_EMU_MAPPER_CREATOR_MAPPER116);
    mmEmuMapperFactory_Register(p, 117, &MM_EMU_MAPPER_CREATOR_MAPPER117);
    mmEmuMapperFactory_Register(p, 118, &MM_EMU_MAPPER_CREATOR_MAPPER118);
    mmEmuMapperFactory_Register(p, 119, &MM_EMU_MAPPER_CREATOR_MAPPER119);
    //mmEmuMapperFactory_Register(p, 120, &MM_EMU_MAPPER_CREATOR_MAPPER120);
    //mmEmuMapperFactory_Register(p, 121, &MM_EMU_MAPPER_CREATOR_MAPPER121);
    mmEmuMapperFactory_Register(p, 122, &MM_EMU_MAPPER_CREATOR_MAPPER122);
    //mmEmuMapperFactory_Register(p, 123, &MM_EMU_MAPPER_CREATOR_MAPPER123);
    //mmEmuMapperFactory_Register(p, 124, &MM_EMU_MAPPER_CREATOR_MAPPER124);
    //mmEmuMapperFactory_Register(p, 125, &MM_EMU_MAPPER_CREATOR_MAPPER125);
    //mmEmuMapperFactory_Register(p, 126, &MM_EMU_MAPPER_CREATOR_MAPPER126);
    //mmEmuMapperFactory_Register(p, 127, &MM_EMU_MAPPER_CREATOR_MAPPER127);

    // 128 - 143
    //mmEmuMapperFactory_Register(p, 128, &MM_EMU_MAPPER_CREATOR_MAPPER128);
    //mmEmuMapperFactory_Register(p, 129, &MM_EMU_MAPPER_CREATOR_MAPPER129);
    //mmEmuMapperFactory_Register(p, 130, &MM_EMU_MAPPER_CREATOR_MAPPER130);
    //mmEmuMapperFactory_Register(p, 131, &MM_EMU_MAPPER_CREATOR_MAPPER131);
    //mmEmuMapperFactory_Register(p, 132, &MM_EMU_MAPPER_CREATOR_MAPPER132);
    mmEmuMapperFactory_Register(p, 133, &MM_EMU_MAPPER_CREATOR_MAPPER133);
    mmEmuMapperFactory_Register(p, 134, &MM_EMU_MAPPER_CREATOR_MAPPER134);
    mmEmuMapperFactory_Register(p, 135, &MM_EMU_MAPPER_CREATOR_MAPPER135);
    //mmEmuMapperFactory_Register(p, 136, &MM_EMU_MAPPER_CREATOR_MAPPER136);
    //mmEmuMapperFactory_Register(p, 137, &MM_EMU_MAPPER_CREATOR_MAPPER137);
    //mmEmuMapperFactory_Register(p, 138, &MM_EMU_MAPPER_CREATOR_MAPPER138);
    //mmEmuMapperFactory_Register(p, 139, &MM_EMU_MAPPER_CREATOR_MAPPER139);
    mmEmuMapperFactory_Register(p, 140, &MM_EMU_MAPPER_CREATOR_MAPPER140);
    //mmEmuMapperFactory_Register(p, 141, &MM_EMU_MAPPER_CREATOR_MAPPER141);
    mmEmuMapperFactory_Register(p, 142, &MM_EMU_MAPPER_CREATOR_MAPPER142);
    //mmEmuMapperFactory_Register(p, 143, &MM_EMU_MAPPER_CREATOR_MAPPER143);

    // 144 - 159
    //mmEmuMapperFactory_Register(p, 144, &MM_EMU_MAPPER_CREATOR_MAPPER144);
    //mmEmuMapperFactory_Register(p, 145, &MM_EMU_MAPPER_CREATOR_MAPPER145);
    //mmEmuMapperFactory_Register(p, 146, &MM_EMU_MAPPER_CREATOR_MAPPER146);
    //mmEmuMapperFactory_Register(p, 147, &MM_EMU_MAPPER_CREATOR_MAPPER147);
    //mmEmuMapperFactory_Register(p, 148, &MM_EMU_MAPPER_CREATOR_MAPPER148);
    //mmEmuMapperFactory_Register(p, 149, &MM_EMU_MAPPER_CREATOR_MAPPER149);
    //mmEmuMapperFactory_Register(p, 150, &MM_EMU_MAPPER_CREATOR_MAPPER150);
    mmEmuMapperFactory_Register(p, 151, &MM_EMU_MAPPER_CREATOR_MAPPER151);
    //mmEmuMapperFactory_Register(p, 152, &MM_EMU_MAPPER_CREATOR_MAPPER152);
    //mmEmuMapperFactory_Register(p, 153, &MM_EMU_MAPPER_CREATOR_MAPPER153);
    //mmEmuMapperFactory_Register(p, 154, &MM_EMU_MAPPER_CREATOR_MAPPER154);
    //mmEmuMapperFactory_Register(p, 155, &MM_EMU_MAPPER_CREATOR_MAPPER155);
    //mmEmuMapperFactory_Register(p, 156, &MM_EMU_MAPPER_CREATOR_MAPPER156);
    //mmEmuMapperFactory_Register(p, 157, &MM_EMU_MAPPER_CREATOR_MAPPER157);
    //mmEmuMapperFactory_Register(p, 158, &MM_EMU_MAPPER_CREATOR_MAPPER158);
    //mmEmuMapperFactory_Register(p, 159, &MM_EMU_MAPPER_CREATOR_MAPPER159);

    // 160 - 175
    mmEmuMapperFactory_Register(p, 160, &MM_EMU_MAPPER_CREATOR_MAPPER160);
    //mmEmuMapperFactory_Register(p, 161, &MM_EMU_MAPPER_CREATOR_MAPPER161);
    mmEmuMapperFactory_Register(p, 162, &MM_EMU_MAPPER_CREATOR_MAPPER162);
    mmEmuMapperFactory_Register(p, 163, &MM_EMU_MAPPER_CREATOR_MAPPER163);
    mmEmuMapperFactory_Register(p, 164, &MM_EMU_MAPPER_CREATOR_MAPPER164);
    mmEmuMapperFactory_Register(p, 165, &MM_EMU_MAPPER_CREATOR_MAPPER165);
    //mmEmuMapperFactory_Register(p, 166, &MM_EMU_MAPPER_CREATOR_MAPPER166);
    mmEmuMapperFactory_Register(p, 167, &MM_EMU_MAPPER_CREATOR_MAPPER167);
    mmEmuMapperFactory_Register(p, 168, &MM_EMU_MAPPER_CREATOR_MAPPER168);
    mmEmuMapperFactory_Register(p, 169, &MM_EMU_MAPPER_CREATOR_MAPPER169);
    mmEmuMapperFactory_Register(p, 170, &MM_EMU_MAPPER_CREATOR_MAPPER170);
    //mmEmuMapperFactory_Register(p, 171, &MM_EMU_MAPPER_CREATOR_MAPPER171);
    //mmEmuMapperFactory_Register(p, 172, &MM_EMU_MAPPER_CREATOR_MAPPER172);
    //mmEmuMapperFactory_Register(p, 173, &MM_EMU_MAPPER_CREATOR_MAPPER173);
    //mmEmuMapperFactory_Register(p, 174, &MM_EMU_MAPPER_CREATOR_MAPPER174);
    mmEmuMapperFactory_Register(p, 175, &MM_EMU_MAPPER_CREATOR_MAPPER175);

    // 176 - 191
    mmEmuMapperFactory_Register(p, 176, &MM_EMU_MAPPER_CREATOR_MAPPER176);
    mmEmuMapperFactory_Register(p, 177, &MM_EMU_MAPPER_CREATOR_MAPPER177);
    mmEmuMapperFactory_Register(p, 178, &MM_EMU_MAPPER_CREATOR_MAPPER178);
    //mmEmuMapperFactory_Register(p, 179, &MM_EMU_MAPPER_CREATOR_MAPPER179);
    mmEmuMapperFactory_Register(p, 180, &MM_EMU_MAPPER_CREATOR_MAPPER180);
    mmEmuMapperFactory_Register(p, 181, &MM_EMU_MAPPER_CREATOR_MAPPER181);
    mmEmuMapperFactory_Register(p, 182, &MM_EMU_MAPPER_CREATOR_MAPPER182);
    mmEmuMapperFactory_Register(p, 183, &MM_EMU_MAPPER_CREATOR_MAPPER183);
    //mmEmuMapperFactory_Register(p, 184, &MM_EMU_MAPPER_CREATOR_MAPPER184);
    mmEmuMapperFactory_Register(p, 185, &MM_EMU_MAPPER_CREATOR_MAPPER185);
    //mmEmuMapperFactory_Register(p, 186, &MM_EMU_MAPPER_CREATOR_MAPPER186);
    mmEmuMapperFactory_Register(p, 187, &MM_EMU_MAPPER_CREATOR_MAPPER187);
    mmEmuMapperFactory_Register(p, 188, &MM_EMU_MAPPER_CREATOR_MAPPER188);
    mmEmuMapperFactory_Register(p, 189, &MM_EMU_MAPPER_CREATOR_MAPPER189);
    mmEmuMapperFactory_Register(p, 190, &MM_EMU_MAPPER_CREATOR_MAPPER190);
    mmEmuMapperFactory_Register(p, 191, &MM_EMU_MAPPER_CREATOR_MAPPER191);

    // 192 - 207
    mmEmuMapperFactory_Register(p, 192, &MM_EMU_MAPPER_CREATOR_MAPPER192);
    mmEmuMapperFactory_Register(p, 193, &MM_EMU_MAPPER_CREATOR_MAPPER193);
    mmEmuMapperFactory_Register(p, 194, &MM_EMU_MAPPER_CREATOR_MAPPER194);
    mmEmuMapperFactory_Register(p, 195, &MM_EMU_MAPPER_CREATOR_MAPPER195);
    //mmEmuMapperFactory_Register(p, 196, &MM_EMU_MAPPER_CREATOR_MAPPER196);
    //mmEmuMapperFactory_Register(p, 197, &MM_EMU_MAPPER_CREATOR_MAPPER197);
    mmEmuMapperFactory_Register(p, 198, &MM_EMU_MAPPER_CREATOR_MAPPER198);
    //mmEmuMapperFactory_Register(p, 199, &MM_EMU_MAPPER_CREATOR_MAPPER199);
    mmEmuMapperFactory_Register(p, 200, &MM_EMU_MAPPER_CREATOR_MAPPER200);
    mmEmuMapperFactory_Register(p, 201, &MM_EMU_MAPPER_CREATOR_MAPPER201);
    mmEmuMapperFactory_Register(p, 202, &MM_EMU_MAPPER_CREATOR_MAPPER202);
    //mmEmuMapperFactory_Register(p, 203, &MM_EMU_MAPPER_CREATOR_MAPPER203);
    //mmEmuMapperFactory_Register(p, 204, &MM_EMU_MAPPER_CREATOR_MAPPER204);
    //mmEmuMapperFactory_Register(p, 205, &MM_EMU_MAPPER_CREATOR_MAPPER205);
    //mmEmuMapperFactory_Register(p, 206, &MM_EMU_MAPPER_CREATOR_MAPPER206);
    //mmEmuMapperFactory_Register(p, 207, &MM_EMU_MAPPER_CREATOR_MAPPER207);

    // 208 - 223
    //mmEmuMapperFactory_Register(p, 208, &MM_EMU_MAPPER_CREATOR_MAPPER208);
    //mmEmuMapperFactory_Register(p, 209, &MM_EMU_MAPPER_CREATOR_MAPPER209);
    //mmEmuMapperFactory_Register(p, 210, &MM_EMU_MAPPER_CREATOR_MAPPER210);
    //mmEmuMapperFactory_Register(p, 211, &MM_EMU_MAPPER_CREATOR_MAPPER211);
    //mmEmuMapperFactory_Register(p, 212, &MM_EMU_MAPPER_CREATOR_MAPPER212);
    //mmEmuMapperFactory_Register(p, 213, &MM_EMU_MAPPER_CREATOR_MAPPER213);
    //mmEmuMapperFactory_Register(p, 214, &MM_EMU_MAPPER_CREATOR_MAPPER214);
    //mmEmuMapperFactory_Register(p, 215, &MM_EMU_MAPPER_CREATOR_MAPPER215);
    //mmEmuMapperFactory_Register(p, 216, &MM_EMU_MAPPER_CREATOR_MAPPER216);
    //mmEmuMapperFactory_Register(p, 217, &MM_EMU_MAPPER_CREATOR_MAPPER217);
    //mmEmuMapperFactory_Register(p, 218, &MM_EMU_MAPPER_CREATOR_MAPPER218);
    //mmEmuMapperFactory_Register(p, 219, &MM_EMU_MAPPER_CREATOR_MAPPER219);
    //mmEmuMapperFactory_Register(p, 220, &MM_EMU_MAPPER_CREATOR_MAPPER220);
    //mmEmuMapperFactory_Register(p, 221, &MM_EMU_MAPPER_CREATOR_MAPPER221);
    mmEmuMapperFactory_Register(p, 222, &MM_EMU_MAPPER_CREATOR_MAPPER222);
    //mmEmuMapperFactory_Register(p, 223, &MM_EMU_MAPPER_CREATOR_MAPPER223);

    // 224 - 239
    //mmEmuMapperFactory_Register(p, 224, &MM_EMU_MAPPER_CREATOR_MAPPER224);
    mmEmuMapperFactory_Register(p, 225, &MM_EMU_MAPPER_CREATOR_MAPPER225);
    mmEmuMapperFactory_Register(p, 226, &MM_EMU_MAPPER_CREATOR_MAPPER226);
    mmEmuMapperFactory_Register(p, 227, &MM_EMU_MAPPER_CREATOR_MAPPER227);
    mmEmuMapperFactory_Register(p, 228, &MM_EMU_MAPPER_CREATOR_MAPPER228);
    mmEmuMapperFactory_Register(p, 229, &MM_EMU_MAPPER_CREATOR_MAPPER229);
    mmEmuMapperFactory_Register(p, 230, &MM_EMU_MAPPER_CREATOR_MAPPER230);
    mmEmuMapperFactory_Register(p, 231, &MM_EMU_MAPPER_CREATOR_MAPPER231);
    mmEmuMapperFactory_Register(p, 232, &MM_EMU_MAPPER_CREATOR_MAPPER232);
    mmEmuMapperFactory_Register(p, 233, &MM_EMU_MAPPER_CREATOR_MAPPER233);
    mmEmuMapperFactory_Register(p, 234, &MM_EMU_MAPPER_CREATOR_MAPPER234);
    mmEmuMapperFactory_Register(p, 235, &MM_EMU_MAPPER_CREATOR_MAPPER235);
    mmEmuMapperFactory_Register(p, 236, &MM_EMU_MAPPER_CREATOR_MAPPER236);
    //mmEmuMapperFactory_Register(p, 237, &MM_EMU_MAPPER_CREATOR_MAPPER237);
    //mmEmuMapperFactory_Register(p, 238, &MM_EMU_MAPPER_CREATOR_MAPPER238);
    //mmEmuMapperFactory_Register(p, 239, &MM_EMU_MAPPER_CREATOR_MAPPER239);

    // 240 - 255
    mmEmuMapperFactory_Register(p, 240, &MM_EMU_MAPPER_CREATOR_MAPPER240);
    mmEmuMapperFactory_Register(p, 241, &MM_EMU_MAPPER_CREATOR_MAPPER241);
    mmEmuMapperFactory_Register(p, 242, &MM_EMU_MAPPER_CREATOR_MAPPER242);
    mmEmuMapperFactory_Register(p, 243, &MM_EMU_MAPPER_CREATOR_MAPPER243);
    mmEmuMapperFactory_Register(p, 244, &MM_EMU_MAPPER_CREATOR_MAPPER244);
    mmEmuMapperFactory_Register(p, 245, &MM_EMU_MAPPER_CREATOR_MAPPER245);
    mmEmuMapperFactory_Register(p, 246, &MM_EMU_MAPPER_CREATOR_MAPPER246);
    //mmEmuMapperFactory_Register(p, 247, &MM_EMU_MAPPER_CREATOR_MAPPER247);
    mmEmuMapperFactory_Register(p, 248, &MM_EMU_MAPPER_CREATOR_MAPPER248);
    mmEmuMapperFactory_Register(p, 249, &MM_EMU_MAPPER_CREATOR_MAPPER249);
    //mmEmuMapperFactory_Register(p, 250, &MM_EMU_MAPPER_CREATOR_MAPPER250);
    mmEmuMapperFactory_Register(p, 251, &MM_EMU_MAPPER_CREATOR_MAPPER251);
    mmEmuMapperFactory_Register(p, 252, &MM_EMU_MAPPER_CREATOR_MAPPER252);
    mmEmuMapperFactory_Register(p, 253, &MM_EMU_MAPPER_CREATOR_MAPPER253);
    mmEmuMapperFactory_Register(p, 254, &MM_EMU_MAPPER_CREATOR_MAPPER254);
    mmEmuMapperFactory_Register(p, 255, &MM_EMU_MAPPER_CREATOR_MAPPER255);

    // 256 - 256
    mmEmuMapperFactory_Register(p, 256, &MM_EMU_MAPPER_CREATOR_MAPPER256);
}


