/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper018.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper018_Init(struct mmEmuMapper018* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 11);

    p->irq_enable = 0;
    p->irq_mode = 0;
    p->irq_latch = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper018_Reset;
    p->super.Write = &mmEmuMapper018_Write;
    //p->super.Read = &mmEmuMapper018_Read;
    //p->super.ReadLow = &mmEmuMapper018_ReadLow;
    //p->super.WriteLow = &mmEmuMapper018_WriteLow;
    //p->super.ExRead = &mmEmuMapper018_ExRead;
    //p->super.ExWrite = &mmEmuMapper018_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper018_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper018_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper018_HSync;
    //p->super.VSync = &mmEmuMapper018_VSync;
    p->super.Clock = &mmEmuMapper018_Clock;
    //p->super.PPULatch = &mmEmuMapper018_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper018_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper018_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper018_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper018_IsStateSave;
    p->super.SaveState = &mmEmuMapper018_SaveState;
    p->super.LoadState = &mmEmuMapper018_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper018_Destroy(struct mmEmuMapper018* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 11);

    p->irq_enable = 0;
    p->irq_mode = 0;
    p->irq_latch = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper018_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper018* p = (struct mmEmuMapper018*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 11);

    p->reg[2] = mmu->PROM_08K_SIZE - 2;
    p->reg[3] = mmu->PROM_08K_SIZE - 1;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    p->irq_enable = 0;
    p->irq_mode = 0;
    p->irq_counter = 0xFFFF;
    p->irq_latch = 0xFFFF;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xefb1df9e)          // The Lord of King(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
    if (crc == 0x3746f951)          // Pizza Pop!(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }

    //mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    //mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
}
MM_EXPORT_EMU void mmEmuMapper018_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper018* p = (struct mmEmuMapper018*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        p->reg[0] = (p->reg[0] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->reg[0]);
        break;
    case 0x8001:
        p->reg[0] = (p->reg[0] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->reg[0]);
        break;
    case 0x8002:
        p->reg[1] = (p->reg[1] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->reg[1]);
        break;
    case 0x8003:
        p->reg[1] = (p->reg[1] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->reg[1]);
        break;
    case 0x9000:
        p->reg[2] = (p->reg[2] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetPROM8KBank(mmu, 6, p->reg[2]);
        break;
    case 0x9001:
        p->reg[2] = (p->reg[2] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetPROM8KBank(mmu, 6, p->reg[2]);
        break;

    case 0xA000:
        p->reg[3] = (p->reg[3] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[3]);
        break;
    case 0xA001:
        p->reg[3] = (p->reg[3] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[3]);
        break;
    case 0xA002:
        p->reg[4] = (p->reg[4] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[4]);
        break;
    case 0xA003:
        p->reg[4] = (p->reg[4] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[4]);
        break;

    case 0xB000:
        p->reg[5] = (p->reg[5] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[5]);
        break;
    case 0xB001:
        p->reg[5] = (p->reg[5] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[5]);
        break;
    case 0xB002:
        p->reg[6] = (p->reg[6] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[6]);
        break;
    case 0xB003:
        p->reg[6] = (p->reg[6] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[6]);
        break;

    case 0xC000:
        p->reg[7] = (p->reg[7] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[7]);
        break;
    case 0xC001:
        p->reg[7] = (p->reg[7] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[7]);
        break;
    case 0xC002:
        p->reg[8] = (p->reg[8] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[8]);
        break;
    case 0xC003:
        p->reg[8] = (p->reg[8] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[8]);
        break;

    case 0xD000:
        p->reg[9] = (p->reg[9] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[9]);
        break;
    case 0xD001:
        p->reg[9] = (p->reg[9] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[9]);
        break;
    case 0xD002:
        p->reg[10] = (p->reg[10] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[10]);
        break;
    case 0xD003:
        p->reg[10] = (p->reg[10] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[10]);
        break;

    case 0xE000:
        p->irq_latch = (p->irq_latch & 0xFFF0) | (data & 0x0F);
        break;
    case 0xE001:
        p->irq_latch = (p->irq_latch & 0xFF0F) | ((data & 0x0F) << 4);
        break;
    case 0xE002:
        p->irq_latch = (p->irq_latch & 0xF0FF) | ((data & 0x0F) << 8);
        break;
    case 0xE003:
        p->irq_latch = (p->irq_latch & 0x0FFF) | ((data & 0x0F) << 12);
        break;

    case 0xF000:
        p->irq_counter = p->irq_latch;

        //if (data & 0x01)
        //{
        //  p->irq_counter = p->irq_latch;
        //}
        //else
        //{
        //  p->irq_counter = 0;
        //}
        break;
    case 0xF001:
        p->irq_mode = (data >> 1) & 0x07;
        p->irq_enable = (data & 0x01);

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);

        //if( !p->irq_enable )
        //{
        //  mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        //}
        break;

    case 0xF002:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper018_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper018* p = (struct mmEmuMapper018*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmBool_t bIRQ = MM_FALSE;
    mmInt_t irq_counter_old = p->irq_counter;

    if (p->irq_enable && p->irq_counter)
    {
        p->irq_counter -= cycles;

        switch (p->irq_mode)
        {
        case 0:
            if (p->irq_counter <= 0)
            {
                bIRQ = MM_TRUE;
            }
            break;
        case 1:
            if ((p->irq_counter & 0xF000) != (irq_counter_old & 0xF000))
            {
                bIRQ = MM_TRUE;
            }
            break;
        case 2:
        case 3:
            if ((p->irq_counter & 0xFF00) != (irq_counter_old & 0xFF00))
            {
                bIRQ = MM_TRUE;
            }
            break;
        case 4:
        case 5:
        case 6:
        case 7:
            if ((p->irq_counter & 0xFFF0) != (irq_counter_old & 0xFFF0))
            {
                bIRQ = MM_TRUE;
            }
            break;
        }

        if (bIRQ)
        {
            //p->irq_enable = 0;
            //p->irq_counter = p->irq_latch;
            p->irq_counter = 0;
            p->irq_enable = 0;
            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper018_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper018_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper018* p = (struct mmEmuMapper018*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 11);
    buffer[11] = p->irq_enable;
    buffer[12] = p->irq_mode;
    *(mmInt_t*)&buffer[13] = p->irq_counter;
    *(mmInt_t*)&buffer[17] = p->irq_latch;
}
MM_EXPORT_EMU void mmEmuMapper018_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper018* p = (struct mmEmuMapper018*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 11);
    p->irq_enable = buffer[11];
    p->irq_mode = buffer[12];
    p->irq_counter = *(mmInt_t*)&buffer[13];
    p->irq_latch = *(mmInt_t*)&buffer[17];
}

static struct mmEmuMapper* __static_mmEmuMapper018_Produce(void)
{
    struct mmEmuMapper018* p = (struct mmEmuMapper018*)mmMalloc(sizeof(struct mmEmuMapper018));
    mmEmuMapper018_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper018_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper018* p = (struct mmEmuMapper018*)(m);
    mmEmuMapper018_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER018 =
{
    &__static_mmEmuMapper018_Produce,
    &__static_mmEmuMapper018_Recycle,
};
