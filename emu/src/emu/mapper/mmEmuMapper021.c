/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper021.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper021_Init(struct mmEmuMapper021* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    //
    p->super.Reset = &mmEmuMapper021_Reset;
    p->super.Write = &mmEmuMapper021_Write;
    //p->super.Read = &mmEmuMapper021_Read;
    //p->super.ReadLow = &mmEmuMapper021_ReadLow;
    //p->super.WriteLow = &mmEmuMapper021_WriteLow;
    //p->super.ExRead = &mmEmuMapper021_ExRead;
    //p->super.ExWrite = &mmEmuMapper021_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper021_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper021_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper021_HSync;
    //p->super.VSync = &mmEmuMapper021_VSync;
    p->super.Clock = &mmEmuMapper021_Clock;
    //p->super.PPULatch = &mmEmuMapper021_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper021_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper021_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper021_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper021_IsStateSave;
    p->super.SaveState = &mmEmuMapper021_SaveState;
    p->super.LoadState = &mmEmuMapper021_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper021_Destroy(struct mmEmuMapper021* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;
}

MM_EXPORT_EMU void mmEmuMapper021_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper021* p = (struct mmEmuMapper021*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i = 0;

    for (i = 0; i < 8; i++)
    {
        p->reg[i] = i;
    }
    p->reg[8] = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper021_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper021* p = (struct mmEmuMapper021*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF0CF)
    {
    case 0x8000:
        if (p->reg[8] & 0x02)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        }
        break;
    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;

    case 0x9000:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0x9002:
    case 0x9080:
        p->reg[8] = data;
        break;

    case 0xB000:
        p->reg[0] = (p->reg[0] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;
    case 0xB002:
    case 0xB040:
        p->reg[0] = (p->reg[0] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;

    case 0xB001:
    case 0xB004:
    case 0xB080:
        p->reg[1] = (p->reg[1] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;
    case 0xB003:
    case 0xB006:
    case 0xB0C0:
        p->reg[1] = (p->reg[1] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;

    case 0xC000:
        p->reg[2] = (p->reg[2] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;
    case 0xC002:
    case 0xC040:
        p->reg[2] = (p->reg[2] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;

    case 0xC001:
    case 0xC004:
    case 0xC080:
        p->reg[3] = (p->reg[3] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;
    case 0xC003:
    case 0xC006:
    case 0xC0C0:
        p->reg[3] = (p->reg[3] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;

    case 0xD000:
        p->reg[4] = (p->reg[4] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;
    case 0xD002:
    case 0xD040:
        p->reg[4] = (p->reg[4] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;

    case 0xD001:
    case 0xD004:
    case 0xD080:
        p->reg[5] = (p->reg[5] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;
    case 0xD003:
    case 0xD006:
    case 0xD0C0:
        p->reg[5] = (p->reg[5] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;

    case 0xE000:
        p->reg[6] = (p->reg[6] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;
    case 0xE002:
    case 0xE040:
        p->reg[6] = (p->reg[6] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;

    case 0xE001:
    case 0xE004:
    case 0xE080:
        p->reg[7] = (p->reg[7] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;
    case 0xE003:
    case 0xE006:
    case 0xE0C0:
        p->reg[7] = (p->reg[7] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;

    case 0xF000:
        p->irq_latch = (p->irq_latch & 0xF0) | (data & 0x0F);
        break;
    case 0xF002:
    case 0xF040:
        p->irq_latch = (p->irq_latch & 0x0F) | ((data & 0x0F) << 4);
        break;

    case 0xF003:
    case 0xF0C0:
    case 0xF006:
        p->irq_enable = (p->irq_enable & 0x01) * 3;
        p->irq_clock = 0;

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF004:
    case 0xF080:
        p->irq_enable = data & 0x03;
        if (p->irq_enable & 0x02)
        {
            p->irq_counter = p->irq_latch;
            p->irq_clock = 0;
        }

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

        //case 0xF006:
        //  mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        //  break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper021_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper021* p = (struct mmEmuMapper021*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (p->irq_enable & 0x02)
    {
        if ((p->irq_clock -= cycles) < 0)
        {
            p->irq_clock += 0x72;
            if (p->irq_counter == 0xFF)
            {
                p->irq_counter = p->irq_latch;

                //p->irq_enable = (p->irq_enable & 0x01) * 3;
                //mm_emu_cpu_IRQ_NotPending(&nes->cpu);

                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
            else
            {
                p->irq_counter++;
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper021_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper021_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper021* p = (struct mmEmuMapper021*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 9);
    buffer[9] = p->irq_enable;
    buffer[10] = p->irq_counter;
    buffer[11] = p->irq_latch;
    *(mmInt_t*)&buffer[12] = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper021_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper021* p = (struct mmEmuMapper021*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 9);
    p->irq_enable = buffer[9];
    p->irq_counter = buffer[10];
    p->irq_latch = buffer[11];
    p->irq_clock = *(mmInt_t*)&buffer[12];
}

static struct mmEmuMapper* __static_mmEmuMapper021_Produce(void)
{
    struct mmEmuMapper021* p = (struct mmEmuMapper021*)mmMalloc(sizeof(struct mmEmuMapper021));
    mmEmuMapper021_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper021_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper021* p = (struct mmEmuMapper021*)(m);
    mmEmuMapper021_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER021 =
{
    &__static_mmEmuMapper021_Produce,
    &__static_mmEmuMapper021_Recycle,
};
