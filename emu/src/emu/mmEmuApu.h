/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApu_h__
#define __mmEmuApu_h__

#include "core/mmCore.h"

#include "apuex/mmEmuApuInternal.h"
#include "apuex/mmEmuApuVrc6.h"
#include "apuex/mmEmuApuVrc7.h"
#include "apuex/mmEmuApuMmc5.h"
#include "apuex/mmEmuApuFds.h"
#include "apuex/mmEmuApuN106.h"
#include "apuex/mmEmuApuFme7.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

// #define MM_EMU_APU_QUEUE_LENGTH   4096
#define MM_EMU_APU_QUEUE_LENGTH   8192

struct mmEmuApuQueueData
{
    mmUInt64_t  time;
    mmWord_t    addr;
    mmByte_t    data;
    mmByte_t    reserved;
};

struct mmEmuApuQueue
{
    mmInt_t     rdptr;
    mmInt_t     wrptr;
    // we use malloc for less stack memory.
    // struct mmEmuApuQueueData data[MM_EMU_APU_QUEUE_LENGTH];
    struct mmEmuApuQueueData* data;
};
MM_EXPORT_EMU void mmEmuApuQueue_Init(struct mmEmuApuQueue* p);
MM_EXPORT_EMU void mmEmuApuQueue_Destroy(struct mmEmuApuQueue* p);

MM_EXPORT_EMU void mmEmuApuQueue_Clear(struct mmEmuApuQueue* p);
MM_EXPORT_EMU void mmEmuApuQueue_Reset(struct mmEmuApuQueue* p);

MM_EXPORT_EMU void mmEmuApuQueue_Set(struct mmEmuApuQueue* p, mmUInt64_t writetime, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmBool_t mmEmuApuQueue_Get(struct mmEmuApuQueue* p, mmUInt64_t writetime, struct mmEmuApuQueueData* ret);

struct mmEmuNes;

struct mmEmuApu
{
    struct mmEmuNes* nes;

    // Sound core
    struct mmEmuApuInternal  internal;
    struct mmEmuApuVrc6      vrc6;
    struct mmEmuApuVrc7      vrc7;
    struct mmEmuApuMmc5      mmc5;
    struct mmEmuApuFds       fds;
    struct mmEmuApuN106      n106;
    struct mmEmuApuFme7      fme7;

    struct mmEmuApuQueue    queue;
    struct mmEmuApuQueue    exqueue;

    mmByte_t exsound_select;

    double   elapsed_time;
    // INT    elapsed_time;

    // Filter
    mmInt_t last_data;
    mmInt_t last_diff;
    mmInt_t lowpass_filter[4];

    // Channel mute
    mmBool_t    m_bMute[16];

    // Sound Buffer.
    mmShort_t   m_SoundBuffer[0x100];
};

MM_EXPORT_EMU void mmEmuApu_Init(struct mmEmuApu* p);
MM_EXPORT_EMU void mmEmuApu_Destroy(struct mmEmuApu* p);

MM_EXPORT_EMU void mmEmuApu_SetParent(struct mmEmuApu* p, struct mmEmuNes* parent);

MM_EXPORT_EMU void mmEmuApu_SoundSetup(struct mmEmuApu* p);
MM_EXPORT_EMU void mmEmuApu_SelectExSound(struct mmEmuApu* p, mmByte_t data);

MM_EXPORT_EMU mmBool_t mmEmuApu_SetChannelMute(struct mmEmuApu* p, mmInt_t nCH);
// set all Channel Mute.
MM_EXPORT_EMU void mmEmuApu_SetMute(struct mmEmuApu* p, mmBool_t v);

MM_EXPORT_EMU void mmEmuApu_QueueClear(struct mmEmuApu* p);

MM_EXPORT_EMU void mmEmuApu_Reset(struct mmEmuApu* p);
MM_EXPORT_EMU mmByte_t mmEmuApu_Read(struct mmEmuApu* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuApu_Write(struct mmEmuApu* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuApu_ExRead(struct mmEmuApu* p, mmWord_t addr);
MM_EXPORT_EMU void mmEmuApu_ExWrite(struct mmEmuApu* p, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuApu_Sync(struct mmEmuApu* p);
MM_EXPORT_EMU void mmEmuApu_SyncDPCM(struct mmEmuApu* p, mmInt_t cycles);

MM_EXPORT_EMU void mmEmuApu_Process(struct mmEmuApu* p, mmByte_t* lpBuffer, mmUInt32_t dwSize, mmInt_t nRate);

// For NSF player
// Channel frequency acquisition subroutine (for NSF).
MM_EXPORT_EMU mmInt_t mmEmuApu_GetChannelFrequency(struct mmEmuApu* p, mmInt_t channel);
MM_EXPORT_EMU mmShort_t* mmEmuApu_GetSoundBuffer(struct mmEmuApu* p);

// For State
MM_EXPORT_EMU void mmEmuApu_GetFrameIRQ(struct mmEmuApu* p, mmInt_t* Cycle, mmByte_t* Count, mmByte_t* Type, mmByte_t* IRQ, mmByte_t* Occur);
MM_EXPORT_EMU void mmEmuApu_SetFrameIRQ(struct mmEmuApu* p, mmInt_t Cycle, mmByte_t Count, mmByte_t Type, mmByte_t IRQ, mmByte_t Occur);

MM_EXPORT_EMU void mmEmuApu_SaveState(struct mmEmuApu* p, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuApu_LoadState(struct mmEmuApu* p, mmByte_t* buffer);

MM_EXPORT_EMU void mmEmuApu_SetQueue(struct mmEmuApu* p, mmUInt64_t writetime, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmBool_t mmEmuApu_GetQueue(struct mmEmuApu* p, mmUInt64_t writetime, struct mmEmuApuQueueData* ret);

MM_EXPORT_EMU void mmEmuApu_SetExQueue(struct mmEmuApu* p, mmUInt64_t writetime, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmBool_t mmEmuApu_GetExQueue(struct mmEmuApu* p, mmUInt64_t writetime, struct mmEmuApuQueueData* ret);

MM_EXPORT_EMU void mmEmuApu_QueueFlush(struct mmEmuApu* p);

MM_EXPORT_EMU void mmEmuApu_WriteProcess(struct mmEmuApu* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuApu_WriteExProcess(struct mmEmuApu* p, mmWord_t addr, mmByte_t data);

#include "core/mmSuffix.h"

#endif//__mmEmuApu_h__
