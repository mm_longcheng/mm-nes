/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper242.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper242_Init(struct mmEmuMapper242* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper242_Reset;
    p->super.Write = &mmEmuMapper242_Write;
    //p->super.Read = &mmEmuMapper242_Read;
    //p->super.ReadLow = &mmEmuMapper242_ReadLow;
    //p->super.WriteLow = &mmEmuMapper242_WriteLow;
    //p->super.ExRead = &mmEmuMapper242_ExRead;
    //p->super.ExWrite = &mmEmuMapper242_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper242_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper242_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper242_HSync;
    //p->super.VSync = &mmEmuMapper242_VSync;
    //p->super.Clock = &mmEmuMapper242_Clock;
    //p->super.PPULatch = &mmEmuMapper242_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper242_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper242_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper242_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper242_IsStateSave;
    //p->super.SaveState = &mmEmuMapper242_SaveState;
    //p->super.LoadState = &mmEmuMapper242_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper242_Destroy(struct mmEmuMapper242* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper242_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper242* p = (struct mmEmuMapper242*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper242_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper242* p = (struct mmEmuMapper242*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr & 0x01)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (addr & 0xF8) >> 3);
    }
    if ((addr & 0x03) >> 1)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper242_Produce(void)
{
    struct mmEmuMapper242* p = (struct mmEmuMapper242*)mmMalloc(sizeof(struct mmEmuMapper242));
    mmEmuMapper242_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper242_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper242* p = (struct mmEmuMapper242*)(m);
    mmEmuMapper242_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER242 =
{
    &__static_mmEmuMapper242_Produce,
    &__static_mmEmuMapper242_Recycle,
};
