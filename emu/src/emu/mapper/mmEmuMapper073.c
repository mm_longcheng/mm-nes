/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper073.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper073_Init(struct mmEmuMapper073* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper073_Reset;
    p->super.Write = &mmEmuMapper073_Write;
    //p->super.Read = &mmEmuMapper073_Read;
    //p->super.ReadLow = &mmEmuMapper073_ReadLow;
    //p->super.WriteLow = &mmEmuMapper073_WriteLow;
    //p->super.ExRead = &mmEmuMapper073_ExRead;
    //p->super.ExWrite = &mmEmuMapper073_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper073_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper073_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper073_HSync;
    //p->super.VSync = &mmEmuMapper073_VSync;
    p->super.Clock = &mmEmuMapper073_Clock;
    //p->super.PPULatch = &mmEmuMapper073_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper073_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper073_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper073_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper073_IsStateSave;
    p->super.SaveState = &mmEmuMapper073_SaveState;
    p->super.LoadState = &mmEmuMapper073_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper073_Destroy(struct mmEmuMapper073* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper073_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper073* p = (struct mmEmuMapper073*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->irq_enable = 0;
    p->irq_counter = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper073_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper073* p = (struct mmEmuMapper073*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0xF000:
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        break;

    case 0x8000:
        p->irq_counter = (p->irq_counter & 0xFFF0) | (data & 0x0F);
        break;
    case 0x9000:
        p->irq_counter = (p->irq_counter & 0xFF0F) | ((data & 0x0F) << 4);
        break;
    case 0xA000:
        p->irq_counter = (p->irq_counter & 0xF0FF) | ((data & 0x0F) << 8);
        break;
    case 0xB000:
        p->irq_counter = (p->irq_counter & 0x0FFF) | ((data & 0x0F) << 12);
        break;
    case 0xC000:
        p->irq_enable = data & 0x02;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xD000:
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper073_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper073* p = (struct mmEmuMapper073*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if ((p->irq_counter += cycles) >= 0xFFFF)
        {
            p->irq_enable = 0;
            p->irq_counter &= 0xFFFF;
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper073_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper073_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper073* p = (struct mmEmuMapper073*)(super);

    buffer[0] = p->irq_enable;
    *(mmInt_t*)&buffer[1] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper073_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper073* p = (struct mmEmuMapper073*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = *(mmInt_t*)&buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper073_Produce(void)
{
    struct mmEmuMapper073* p = (struct mmEmuMapper073*)mmMalloc(sizeof(struct mmEmuMapper073));
    mmEmuMapper073_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper073_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper073* p = (struct mmEmuMapper073*)(m);
    mmEmuMapper073_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER073 =
{
    &__static_mmEmuMapper073_Produce,
    &__static_mmEmuMapper073_Recycle,
};
