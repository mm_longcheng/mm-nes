/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper169_h__
#define __mmEmuMapper169_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

#define MM_EMU_MAPPER169_BANKTYPE_YCRAM 0x02

//----------------------------------------------------------------------//
// Mapper169                                                            //
//----------------------------------------------------------------------//
struct mmEmuMapper169
{
    struct mmEmuMapper super;

    mmByte_t    YX_type, reg[8];

    mmByte_t    a3;
    mmInt_t     key_map_row;
    mmByte_t    cmd_4800_6, cmd_4800_7, cmd_4800_8;
    mmByte_t    cmd_5500_3, cmd_5500_8;
    mmByte_t    cmd_5501_8;

    mmByte_t    MMC3_mode, MMC3_reg, MMC3_prg0, MMC3_prg1;
    mmByte_t    MMC3_chr4, MMC3_chr5, MMC3_chr6, MMC3_chr7;

    // mmEmuNes size little big.
    // we use malloc for less stack memory.
    mmByte_t*   YWRAM;//[1024 * 1024];  // for YuXing 98/F 1024K PRam
    mmByte_t*   YSRAM;//[32 * 1024];    // for YuXing 98/F 32K SRam
    mmByte_t*   YCRAM;//[128 * 1024];   // for YuXing 98/F 128K CRam
};

MM_EXPORT_EMU void mmEmuMapper169_Init(struct mmEmuMapper169* p);
MM_EXPORT_EMU void mmEmuMapper169_Destroy(struct mmEmuMapper169* p);

MM_EXPORT_EMU void mmEmuMapper169_SetBankCPU(struct mmEmuMapper169* p);
MM_EXPORT_EMU void mmEmuMapper169_WriteCPUPRAM(struct mmEmuMapper169* p, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper169_MMC3WriteH(struct mmEmuMapper169* p, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper169_MMC3SetBankCPU(struct mmEmuMapper169* p);
MM_EXPORT_EMU void mmEmuMapper169_MMC3SetBankPPU(struct mmEmuMapper169* p);
//
// for YuXing 98/F 1024K PRam
MM_EXPORT_EMU void mmEmuMapper169_SetYWRAM8KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMapper169_SetYWRAM16KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMapper169_SetYWRAM32KBank(struct mmEmuMapper169* p, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMapper169_SetYWRAM32KBankArray(struct mmEmuMapper169* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3);
// for YuXing 98/F 128K CRam
MM_EXPORT_EMU void mmEmuMapper169_SetYCRAM1KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMapper169_SetYCRAM2KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMapper169_SetYCRAM4KBank(struct mmEmuMapper169* p, mmByte_t page, mmInt_t bank);
MM_EXPORT_EMU void mmEmuMapper169_SetYCRAM8KBank(struct mmEmuMapper169* p, mmInt_t bank);
//
MM_EXPORT_EMU mmByte_t mmEmuMapper169_PPUExtLatchSP(struct mmEmuMapper169* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper169_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU mmByte_t mmEmuMapper169_ReadLow(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper169_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper169_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper169_PPULatch(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper169_PPUExtLatchX(struct mmEmuMapper* super, mmInt_t x);
MM_EXPORT_EMU void mmEmuMapper169_PPUExtLatch(struct mmEmuMapper* super, mmWord_t ntbladr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper169_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper169_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper169_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER169;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper169_h__
