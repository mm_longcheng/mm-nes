/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper256_h__
#define __mmEmuMapper256_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"
#include "emu/mmEmuRom.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

#define MM_EMU_MAPPER256_NSF_PROFILE 0

// Star max number.
enum { MM_EMU_MAPPER256_STAR_MAX = 128 };

struct mmEmuMapper256Star
{
    mmInt_t x, y, z;
};

//----------------------------------------------------------------------//
// Mapper256  MapperNSF  NSF:NES Sound Format                           //
//----------------------------------------------------------------------//
struct mmEmuMapper256
{
    struct mmEmuMapper super;

    struct mmEmuRomNsfHeader    nsfheader;
    mmByte_t    exchip;
    mmByte_t    bankswitch;
    mmInt_t     banksize;
    mmByte_t    multipul[2];

    mmByte_t    exaddr;
    mmByte_t    exram[128];

    // Song number
    mmInt_t     songno;

    // Key buffer
    mmByte_t    pad, padold;
    mmByte_t    repcnt;

    mmInt_t     Freq2KeyTbl[96 + 1];

    struct mmEmuMapper256Star StarBuf[MM_EMU_MAPPER256_STAR_MAX];

#if MM_EMU_MAPPER256_NSF_PROFILE
    mmUInt32_t  avecount;
    float       tave;
    float       pave;
    float       ptave;
    mmUInt32_t  maxtotalclk;
    mmUInt32_t  maxprofclk;
    mmUInt32_t  maxproftotalclk;
    mmUInt32_t  maxproftotalcnt;
#endif
};

MM_EXPORT_EMU void mmEmuMapper256_Init(struct mmEmuMapper256* p);
MM_EXPORT_EMU void mmEmuMapper256_Destroy(struct mmEmuMapper256* p);

MM_EXPORT_EMU void mmEmuMapper256_BankSwitch(struct mmEmuMapper256* p, mmInt_t no, mmByte_t bank);

// For keyboard
MM_EXPORT_EMU mmInt_t mmEmuMapper256_GetKeyTable(struct mmEmuMapper256* p, mmInt_t freq);
MM_EXPORT_EMU void mmEmuMapper256_DrawKey(struct mmEmuMapper256* p, mmInt_t key);

// For Drawing
MM_EXPORT_EMU void mmEmuMapper256_DrawRect(struct mmEmuMapper256* p, mmInt_t x, mmInt_t y, mmInt_t w, mmInt_t h, mmByte_t col);
MM_EXPORT_EMU void mmEmuMapper256_DrawFont(struct mmEmuMapper256* p, mmInt_t x, mmInt_t y, mmByte_t chr, mmByte_t col);
MM_EXPORT_EMU void mmEmuMapper256_DrawString(struct mmEmuMapper256* p, mmInt_t x, mmInt_t y, const char* str, mmByte_t col);
MM_EXPORT_EMU void mmEmuMapper256_DrawWave(struct mmEmuMapper256* p, mmInt_t x, mmInt_t y, mmInt_t col);

MM_EXPORT_EMU void mmEmuMapper256_DrawBitmap(struct mmEmuMapper256* p, mmInt_t x, mmInt_t y, mmByte_t* lpBitmap);

MM_EXPORT_EMU void mmEmuMapper256_StarInitial(struct mmEmuMapper256* p);
MM_EXPORT_EMU void mmEmuMapper256_Star(struct mmEmuMapper256* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper256_Reset(struct mmEmuMapper* super);

MM_EXPORT_EMU mmByte_t mmEmuMapper256_ExRead(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper256_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU mmByte_t mmEmuMapper256_ReadLow(struct mmEmuMapper* super, mmWord_t addr);
MM_EXPORT_EMU void mmEmuMapper256_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper256_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper256_VSync(struct mmEmuMapper* super);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER256;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper256_h__
