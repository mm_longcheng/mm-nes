/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper175.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper175_Init(struct mmEmuMapper175* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg_dat = 0;

    //
    p->super.Reset = &mmEmuMapper175_Reset;
    p->super.Write = &mmEmuMapper175_Write;
    p->super.Read = &mmEmuMapper175_Read;
    //p->super.ReadLow = &mmEmuMapper175_ReadLow;
    //p->super.WriteLow = &mmEmuMapper175_WriteLow;
    //p->super.ExRead = &mmEmuMapper175_ExRead;
    //p->super.ExWrite = &mmEmuMapper175_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper175_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper175_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper175_HSync;
    //p->super.VSync = &mmEmuMapper175_VSync;
    //p->super.Clock = &mmEmuMapper175_Clock;
    //p->super.PPULatch = &mmEmuMapper175_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper175_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper175_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper175_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper175_IsStateSave;
    p->super.SaveState = &mmEmuMapper175_SaveState;
    p->super.LoadState = &mmEmuMapper175_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper175_Destroy(struct mmEmuMapper175* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg_dat = 0;
}

MM_EXPORT_EMU void mmEmuMapper175_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper175* p = (struct mmEmuMapper175*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
    mmEmuMmu_SetPROM16KBank(mmu, 6, 0);
    p->reg_dat = 0;

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper175_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper175* p = (struct mmEmuMapper175*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        if (data & 0x04)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    case 0xA000:
        p->reg_dat = data;
        mmEmuMmu_SetPROM8KBank(mmu, 7, (p->reg_dat & 0x0F) * 2 + 1);
        mmEmuMmu_SetVROM8KBank(mmu, p->reg_dat & 0x0F);
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper175_Read(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper175* p = (struct mmEmuMapper175*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0xFFFC)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, p->reg_dat & 0x0F);
        mmEmuMmu_SetPROM8KBank(mmu, 6, (p->reg_dat & 0x0F) * 2);
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper175_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper175_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper175* p = (struct mmEmuMapper175*)(super);

    buffer[0] = p->reg_dat;
}
MM_EXPORT_EMU void mmEmuMapper175_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper175* p = (struct mmEmuMapper175*)(super);

    p->reg_dat = buffer[0];
}

static struct mmEmuMapper* __static_mmEmuMapper175_Produce(void)
{
    struct mmEmuMapper175* p = (struct mmEmuMapper175*)mmMalloc(sizeof(struct mmEmuMapper175));
    mmEmuMapper175_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper175_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper175* p = (struct mmEmuMapper175*)(m);
    mmEmuMapper175_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER175 =
{
    &__static_mmEmuMapper175_Produce,
    &__static_mmEmuMapper175_Recycle,
};
