/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper016.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuX24C01_Init(struct mmEmuX24C01* p)
{
    p->now_state = MM_EMU_X24C01_IDLE;
    p->next_state = MM_EMU_X24C01_IDLE;
    p->bitcnt = 0;
    p->addr = 0;
    p->data = 0;
    p->sda = 0xFF;
    p->scl_old = 0;
    p->sda_old = 0;
    p->pEEPDATA = NULL;
}
MM_EXPORT_EMU void mmEmuX24C01_Destroy(struct mmEmuX24C01* p)
{
    p->now_state = MM_EMU_X24C01_IDLE;
    p->next_state = MM_EMU_X24C01_IDLE;
    p->bitcnt = 0;
    p->addr = 0;
    p->data = 0;
    p->sda = 0xFF;
    p->scl_old = 0;
    p->sda_old = 0;
    p->pEEPDATA = NULL;
}
MM_EXPORT_EMU void mmEmuX24C01_Reset(struct mmEmuX24C01* p, mmByte_t* ptr)
{
    p->now_state = MM_EMU_X24C01_IDLE;
    p->next_state = MM_EMU_X24C01_IDLE;
    p->addr = 0;
    p->data = 0;
    p->sda = 0xFF;
    p->scl_old = 0;
    p->sda_old = 0;

    p->pEEPDATA = ptr;
}
MM_EXPORT_EMU void mmEmuX24C01_Write(struct mmEmuX24C01* p, mmByte_t scl_in, mmByte_t sda_in)
{
    // Clock line
    mmByte_t scl_rise = ~p->scl_old & scl_in;
    mmByte_t scl_fall = p->scl_old & ~scl_in;
    // Data line
    mmByte_t sda_rise = ~p->sda_old & sda_in;
    mmByte_t sda_fall = p->sda_old & ~sda_in;

    mmByte_t scl_old_temp = p->scl_old;
    // mmByte_t sda_old_temp = p->sda_old;

    p->scl_old = scl_in;
    p->sda_old = sda_in;

    // Start condition?
    if (scl_old_temp && sda_fall)
    {
        p->now_state = MM_EMU_X24C01_ADDRESS;
        p->bitcnt = 0;
        p->addr = 0;
        p->sda = 0xFF;
        return;
    }

    // Stop condition?
    if (scl_old_temp && sda_rise)
    {
        p->now_state = MM_EMU_X24C01_IDLE;
        p->sda = 0xFF;
        return;
    }

    // SCL ____---- RISE
    if (scl_rise)
    {
        switch (p->now_state)
        {
        case MM_EMU_X24C01_ADDRESS:
            if (p->bitcnt < 7)
            {
                // Originally MSB -> LSB
                p->addr &= ~(1 << p->bitcnt);
                p->addr |= (sda_in ? 1 : 0) << p->bitcnt;
            }
            else
            {
                if (sda_in)
                {
                    p->next_state = MM_EMU_X24C01_READ;
                    p->data = p->pEEPDATA[p->addr & 0x7F];
                }
                else
                {
                    p->next_state = MM_EMU_X24C01_WRITE;
                }
            }
            p->bitcnt++;
            break;
        case MM_EMU_X24C01_ACK:
            p->sda = 0; // ACK
            break;
        case MM_EMU_X24C01_READ:
            if (p->bitcnt < 8)
            {
                // Originally MSB -> LSB
                p->sda = (p->data&(1 << p->bitcnt)) ? 1 : 0;
            }
            p->bitcnt++;
            break;
        case MM_EMU_X24C01_WRITE:
            if (p->bitcnt < 8)
            {
                // Originally MSB -> LSB
                p->data &= ~(1 << p->bitcnt);
                p->data |= (sda_in ? 1 : 0) << p->bitcnt;
            }
            p->bitcnt++;
            break;

        case MM_EMU_X24C01_ACK_WAIT:
            if (!sda_in)
            {
                p->next_state = MM_EMU_X24C01_IDLE;
            }
            break;
        default:
            break;
        }
    }

    // SCL ----____ FALL
    if (scl_fall)
    {
        switch (p->now_state)
        {
        case MM_EMU_X24C01_ADDRESS:
            if (p->bitcnt >= 8)
            {
                p->now_state = MM_EMU_X24C01_ACK;
                p->sda = 0xFF;
            }
            break;
        case MM_EMU_X24C01_ACK:
            p->now_state = p->next_state;
            p->bitcnt = 0;
            p->sda = 0xFF;
            break;
        case MM_EMU_X24C01_READ:
            if (p->bitcnt >= 8)
            {
                p->now_state = MM_EMU_X24C01_ACK_WAIT;
                p->addr = (p->addr + 1) & 0x7F;
            }
            break;
        case MM_EMU_X24C01_WRITE:
            if (p->bitcnt >= 8)
            {
                p->now_state = MM_EMU_X24C01_ACK;
                p->next_state = MM_EMU_X24C01_IDLE;
                p->pEEPDATA[p->addr & 0x7F] = p->data;
                p->addr = (p->addr + 1) & 0x7F;
            }
            break;
        default:
            break;
        }
    }
}
MM_EXPORT_EMU mmByte_t mmEmuX24C01_Read(struct mmEmuX24C01* p)
{
    return p->sda;
}
MM_EXPORT_EMU void mmEmuX24C01_Load(struct mmEmuX24C01* p, const mmByte_t* buffer)
{
    p->now_state = *((mmInt_t*)&buffer[0]);
    p->next_state = *((mmInt_t*)&buffer[4]);
    p->bitcnt = *((mmInt_t*)&buffer[8]);
    p->addr = buffer[12];
    p->data = buffer[13];
    p->sda = buffer[14];
    p->scl_old = buffer[15];
    p->sda_old = buffer[16];
}
MM_EXPORT_EMU void mmEmuX24C01_Save(const struct mmEmuX24C01* p, mmByte_t* buffer)
{
    *((mmInt_t*)&buffer[0]) = p->now_state;
    *((mmInt_t*)&buffer[4]) = p->next_state;
    *((mmInt_t*)&buffer[8]) = p->bitcnt;
    buffer[12] = p->addr;
    buffer[13] = p->data;
    buffer[14] = p->sda;
    buffer[15] = p->scl_old;
    buffer[16] = p->sda_old;
}

MM_EXPORT_EMU void mmEmuX24C02_Init(struct mmEmuX24C02* p)
{
    p->now_state = MM_EMU_X24C02_IDLE;
    p->next_state = MM_EMU_X24C02_IDLE;
    p->bitcnt = 0;
    p->addr = 0;
    p->data = 0;
    p->rw = 0;
    p->sda = 0xFF;
    p->scl_old = 0;
    p->sda_old = 0;

    p->pEEPDATA = NULL;
}
MM_EXPORT_EMU void mmEmuX24C02_Destroy(struct mmEmuX24C02* p)
{
    p->now_state = MM_EMU_X24C02_IDLE;
    p->next_state = MM_EMU_X24C02_IDLE;
    p->bitcnt = 0;
    p->addr = 0;
    p->data = 0;
    p->rw = 0;
    p->sda = 0xFF;
    p->scl_old = 0;
    p->sda_old = 0;

    p->pEEPDATA = NULL;
}
MM_EXPORT_EMU void mmEmuX24C02_Reset(struct mmEmuX24C02* p, mmByte_t* ptr)
{
    p->now_state = MM_EMU_X24C02_IDLE;
    p->next_state = MM_EMU_X24C02_IDLE;
    p->addr = 0;
    p->data = 0;
    p->rw = 0;
    p->sda = 0xFF;
    p->scl_old = 0;
    p->sda_old = 0;

    p->pEEPDATA = ptr;
}
MM_EXPORT_EMU void mmEmuX24C02_Write(struct mmEmuX24C02* p, mmByte_t scl_in, mmByte_t sda_in)
{
    // Clock line
    mmByte_t scl_rise = ~p->scl_old & scl_in;
    mmByte_t scl_fall = p->scl_old & ~scl_in;
    // Data line
    mmByte_t sda_rise = ~p->sda_old & sda_in;
    mmByte_t sda_fall = p->sda_old & ~sda_in;

    mmByte_t scl_old_temp = p->scl_old;
    // mmByte_t sda_old_temp = p->sda_old;

    p->scl_old = scl_in;
    p->sda_old = sda_in;

    // Start condition?
    if (scl_old_temp && sda_fall)
    {
        p->now_state = MM_EMU_X24C02_DEVADDR;
        p->bitcnt = 0;
        p->sda = 0xFF;
        return;
    }

    // Stop condition?
    if (scl_old_temp && sda_rise)
    {
        p->now_state = MM_EMU_X24C02_IDLE;
        p->sda = 0xFF;
        return;
    }

    // SCL ____---- RISE
    if (scl_rise)
    {
        switch (p->now_state)
        {
        case MM_EMU_X24C02_DEVADDR:
            if (p->bitcnt < 8)
            {
                p->data &= ~(1 << (7 - p->bitcnt));
                p->data |= (sda_in ? 1 : 0) << (7 - p->bitcnt);
            }
            p->bitcnt++;
            break;
        case MM_EMU_X24C02_ADDRESS:
            if (p->bitcnt < 8)
            {
                p->addr &= ~(1 << (7 - p->bitcnt));
                p->addr |= (sda_in ? 1 : 0) << (7 - p->bitcnt);
            }
            p->bitcnt++;
            break;
        case MM_EMU_X24C02_READ:
            if (p->bitcnt < 8)
            {
                p->sda = (p->data&(1 << (7 - p->bitcnt))) ? 1 : 0;
            }
            p->bitcnt++;
            break;
        case MM_EMU_X24C02_WRITE:
            if (p->bitcnt < 8)
            {
                p->data &= ~(1 << (7 - p->bitcnt));
                p->data |= (sda_in ? 1 : 0) << (7 - p->bitcnt);
            }
            p->bitcnt++;
            break;
        case MM_EMU_X24C02_NAK:
            p->sda = 0xFF;  // NAK
            break;
        case MM_EMU_X24C02_ACK:
            p->sda = 0; // ACK
            break;
        case MM_EMU_X24C02_ACK_WAIT:
            if (!sda_in)
            {
                p->next_state = MM_EMU_X24C02_READ;
                p->data = p->pEEPDATA[p->addr];
            }
            break;
        default:
            break;
        }
    }

    // SCL ----____ FALL
    if (scl_fall)
    {
        switch (p->now_state)
        {
        case MM_EMU_X24C02_DEVADDR:
            if (p->bitcnt >= 8)
            {
                if ((p->data & 0xA0) == 0xA0)
                {
                    p->now_state = MM_EMU_X24C02_ACK;
                    p->rw = p->data & 0x01;
                    p->sda = 0xFF;
                    if (p->rw)
                    {
                        // Now address read
                        p->next_state = MM_EMU_X24C02_READ;
                        p->data = p->pEEPDATA[p->addr];
                    }
                    else
                    {
                        p->next_state = MM_EMU_X24C02_ADDRESS;
                    }
                    p->bitcnt = 0;
                }
                else
                {
                    p->now_state = MM_EMU_X24C02_NAK;
                    p->next_state = MM_EMU_X24C02_IDLE;
                    p->sda = 0xFF;
                }
            }
            break;
        case MM_EMU_X24C02_ADDRESS:
            if (p->bitcnt >= 8)
            {
                p->now_state = MM_EMU_X24C02_ACK;
                p->sda = 0xFF;
                if (p->rw)
                {
                    // I definitely will not come but for just in case.
                    p->next_state = MM_EMU_X24C02_IDLE;
                }
                else
                {
                    // to Data Write
                    p->next_state = MM_EMU_X24C02_WRITE;
                }
                p->bitcnt = 0;
            }
            break;
        case MM_EMU_X24C02_READ:
            if (p->bitcnt >= 8)
            {
                p->now_state = MM_EMU_X24C02_ACK_WAIT;
                p->addr = (p->addr + 1) & 0xFF;
            }
            break;
        case MM_EMU_X24C02_WRITE:
            if (p->bitcnt >= 8)
            {
                p->pEEPDATA[p->addr] = p->data;
                p->now_state = MM_EMU_X24C02_ACK;
                p->next_state = MM_EMU_X24C02_WRITE;
                p->addr = (p->addr + 1) & 0xFF;
                p->bitcnt = 0;
            }
            break;
        case MM_EMU_X24C02_NAK:
            p->now_state = MM_EMU_X24C02_IDLE;
            p->bitcnt = 0;
            p->sda = 0xFF;
            break;
        case MM_EMU_X24C02_ACK:
            p->now_state = p->next_state;
            p->bitcnt = 0;
            p->sda = 0xFF;
            break;
        case MM_EMU_X24C02_ACK_WAIT:
            p->now_state = p->next_state;
            p->bitcnt = 0;
            p->sda = 0xFF;
            break;
        default:
            break;
        }
    }
}
MM_EXPORT_EMU mmByte_t mmEmuX24C02_Read(struct mmEmuX24C02* p)
{
    return p->sda;
}
MM_EXPORT_EMU void mmEmuX24C02_Load(struct mmEmuX24C02* p, const mmByte_t* buffer)
{
    p->now_state = *((mmInt_t*)&buffer[0]);
    p->next_state = *((mmInt_t*)&buffer[4]);
    p->bitcnt = *((mmInt_t*)&buffer[8]);
    p->addr = buffer[12];
    p->data = buffer[13];
    p->rw = buffer[14];
    p->sda = buffer[15];
    p->scl_old = buffer[16];
    p->sda_old = buffer[17];
}
MM_EXPORT_EMU void mmEmuX24C02_Save(const struct mmEmuX24C02* p, mmByte_t* buffer)
{
    *((mmInt_t*)&buffer[0]) = p->now_state;
    *((mmInt_t*)&buffer[4]) = p->next_state;
    *((mmInt_t*)&buffer[8]) = p->bitcnt;
    buffer[12] = p->addr;
    buffer[13] = p->data;
    buffer[14] = p->rw;
    buffer[15] = p->sda;
    buffer[16] = p->scl_old;
    buffer[17] = p->sda_old;
}

MM_EXPORT_EMU void mmEmuMapper016_Init(struct mmEmuMapper016* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;
    p->eeprom_type = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_type = 0;

    mmEmuX24C01_Init(&p->x24c01);
    mmEmuX24C02_Init(&p->x24c02);
    //
    p->super.Reset = &mmEmuMapper016_Reset;
    p->super.Write = &mmEmuMapper016_Write;
    //p->super.Read = &mmEmuMapper016_Read;
    p->super.ReadLow = &mmEmuMapper016_ReadLow;
    p->super.WriteLow = &mmEmuMapper016_WriteLow;
    //p->super.ExRead = &mmEmuMapper016_ExRead;
    //p->super.ExWrite = &mmEmuMapper016_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper016_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper016_ExCmdWrite;
    p->super.HSync = &mmEmuMapper016_HSync;
    //p->super.VSync = &mmEmuMapper016_VSync;
    p->super.Clock = &mmEmuMapper016_Clock;
    //p->super.PPULatch = &mmEmuMapper016_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper016_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper016_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper016_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper016_IsStateSave;
    p->super.SaveState = &mmEmuMapper016_SaveState;
    p->super.LoadState = &mmEmuMapper016_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper016_Destroy(struct mmEmuMapper016* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;
    p->eeprom_type = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_type = 0;

    mmEmuX24C01_Destroy(&p->x24c01);
    mmEmuX24C02_Destroy(&p->x24c02);
}

// For Datach Barcode Battler
MM_EXPORT_EMU void mmEmuMapper016_SetBarcodeData(struct mmEmuMapper016* p, mmByte_t* code, mmInt_t len)
{
    // do nothing.
}
MM_EXPORT_EMU void mmEmuMapper016_WriteSubA(struct mmEmuMapper016* p, mmWord_t addr, mmByte_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0x000F)
    {
    case 0x0000:
    case 0x0001:
    case 0x0002:
    case 0x0003:
    case 0x0004:
    case 0x0005:
    case 0x0006:
    case 0x0007:
        if (mmu->VROM_1K_SIZE)
        {
            mmEmuMmu_SetVROM1KBank(mmu, addr & 0x0007, data);
        }
        if (p->eeprom_type == 2)
        {
            p->reg[0] = data;
            mmEmuX24C01_Write(&p->x24c01, (data & 0x08) ? 0xFF : 0, (p->reg[1] & 0x40) ? 0xFF : 0);
        }
        break;

    case 0x0008:
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        break;

    case 0x0009:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0x000A:
        p->irq_enable = data & 0x01;
        p->irq_counter = p->irq_latch;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x000B:
        p->irq_latch = (p->irq_latch & 0xFF00) | data;
        p->irq_counter = (p->irq_counter & 0xFF00) | data;
        break;
    case 0x000C:
        p->irq_latch = ((mmInt_t)data << 8) | (p->irq_latch & 0x00FF);
        p->irq_counter = ((mmInt_t)data << 8) | (p->irq_counter & 0x00FF);
        break;

    case 0x000D:
        // EEPTYPE0(DragonBallZ)
        if (p->eeprom_type == 0)
        {
            mmEmuX24C01_Write(&p->x24c01, (data & 0x20) ? 0xFF : 0, (data & 0x40) ? 0xFF : 0);
        }
        // EEPTYPE1(DragonBallZ2,Z3,Z Gaiden)
        if (p->eeprom_type == 1)
        {
            mmEmuX24C02_Write(&p->x24c02, (data & 0x20) ? 0xFF : 0, (data & 0x40) ? 0xFF : 0);
        }
        // EEPTYPE2(DATACH)
        if (p->eeprom_type == 2)
        {
            p->reg[1] = data;
            mmEmuX24C02_Write(&p->x24c02, (data & 0x20) ? 0xFF : 0, (data & 0x40) ? 0xFF : 0);
            mmEmuX24C01_Write(&p->x24c01, (p->reg[0] & 0x08) ? 0xFF : 0, (data & 0x40) ? 0xFF : 0);
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper016_WriteSubB(struct mmEmuMapper016* p, mmWord_t addr, mmByte_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
    case 0x8001:
    case 0x8002:
    case 0x8003:
        p->reg[0] = data & 0x01;
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->reg[0] * 0x20 + p->reg[2] * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->reg[0] * 0x20 + p->reg[2] * 2 + 1);
        break;
    case 0x8004:
    case 0x8005:
    case 0x8006:
    case 0x8007:
        p->reg[1] = data & 0x01;
        mmEmuMmu_SetPROM8KBank(mmu, 6, p->reg[1] * 0x20 + 0x1E);
        mmEmuMmu_SetPROM8KBank(mmu, 7, p->reg[1] * 0x20 + 0x1F);
        break;
    case 0x8008:
        p->reg[2] = data;
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->reg[0] * 0x20 + p->reg[2] * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->reg[0] * 0x20 + p->reg[2] * 2 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, p->reg[1] * 0x20 + 0x1E);
        mmEmuMmu_SetPROM8KBank(mmu, 7, p->reg[1] * 0x20 + 0x1F);
        break;

    case 0x8009:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0x800A:
        p->irq_enable = data & 0x01;
        p->irq_counter = p->irq_latch;

        //if (!p->irq_enable)
        //{
        //  mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        //}
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x800B:
        p->irq_latch = (p->irq_latch & 0xFF00) | data;
        break;
    case 0x800C:
        p->irq_latch = ((mmInt_t)data << 8) | (p->irq_latch & 0x00FF);
        break;

    case 0x800D:
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper016_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;

    p->reg[0] = p->reg[1] = p->reg[2] = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    p->irq_type = 0;
    mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_CLOCK);

    p->eeprom_type = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x3f15d20d ||        // Famicom Jump 2(J)
        crc == 0xf76aa523)          // Famicom Jump 2(J)(alt)
    {
        p->patch = 1;
        p->eeprom_type = 0xFF;

        // SRAM
        mmu->WRAM[0x0BBC] = 0xFF;
    }

    if (crc == 0x1d6f27f7)          // Dragon Ball Z 2(Korean Hack)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        p->eeprom_type = 1;
    }
    if (crc == 0x6f7247c8)          // Dragon Ball Z 3(Korean Hack)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_CLOCK);
        p->eeprom_type = 1;
    }

    if (crc == 0x7fb799fd)          // Dragon Ball 2 - Dai Maou Fukkatsu(J)
    {

    }
    if (crc == 0x6c6c2feb ||        // Dragon Ball 3 - Gokuu Den(J)
        crc == 0x8edeb257)          // Dragon Ball 3 - Gokuu Den(J)(Alt)
    {

    }
    if (crc == 0x31cd9903) // Dragon Ball Z - Kyoushuu! Saiya Jin(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0xe49fc53e ||        // Dragon Ball Z 2 - Gekishin Freeza!!(J)
        crc == 0x1582fee0)          // Dragon Ball Z 2 - Gekishin Freeza!!(J) [alt]
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        p->eeprom_type = 1;
    }
    if (crc == 0x09499f4d)          // Dragon Ball Z 3 - Ressen Jinzou Ningen(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        p->eeprom_type = 1;
    }
    if (crc == 0x2e991109)          // Dragon Ball Z Gaiden - Saiya Jin Zetsumetsu Keikaku (J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        p->eeprom_type = 1;
    }
    if (crc == 0x146fb9c3)          // SD Gundam Gaiden - Knight Gundam Monogatari(J)
    {

    }

    if (crc == 0x73ac76db ||        // SD Gundam Gaiden - Knight Gundam Monogatari 2 - Hikari no Kishi(J)
        crc == 0x81a15eb8)          // SD Gundam Gaiden - Knight Gundam Monogatari 3 - Densetsu no Kishi Dan(J)
    {
        p->eeprom_type = 1;
    }
    if (crc == 0x170250de) // Rokudenashi Blues(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
        p->eeprom_type = 1;
    }

    // DATACH
    if (crc == 0x0be0a328 ||        // Datach - SD Gundam - Gundam Wars(J)
        crc == 0x19e81461 ||        // Datach - Dragon Ball Z - Gekitou Tenkaichi Budou Kai(J)
        crc == 0x5b457641 ||        // Datach - Ultraman Club - Supokon Fight!(J)
        crc == 0x894efdbc ||        // Datach - Crayon Shin Chan - Ora to Poi Poi(J)
        crc == 0x983d8175 ||        // Datach - Battle Rush - Build Up Robot Tournament(J)
        crc == 0xbe06853f)          // Datach - J League Super Top Players(J)
    {
        p->eeprom_type = 2;
    }
    if (crc == 0xf51a7f46)          // Datach - Yuu Yuu Hakusho - Bakutou Ankoku Bujutsu Kai(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        p->eeprom_type = 2;
    }

    if (p->eeprom_type == 0)
    {
        mmEmuNes_SetSAVERAMSIZE(nes, 128);
        mmEmuX24C01_Reset(&p->x24c01, mmu->WRAM);
    }
    else
    {
        if (p->eeprom_type == 1)
        {
            mmEmuNes_SetSAVERAMSIZE(nes, 256);
            mmEmuX24C02_Reset(&p->x24c02, mmu->WRAM);
        }
        else
        {
            if (p->eeprom_type == 2)
            {
                mmEmuNes_SetSAVERAMSIZE(nes, 384);
                mmEmuX24C02_Reset(&p->x24c02, mmu->WRAM);
                mmEmuX24C01_Reset(&p->x24c01, mmu->WRAM + 256);
            }
        }
    }
}
MM_EXPORT_EMU mmByte_t mmEmuMapper016_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->patch)
    {
        return mmEmuMapper_ReadLow(super, addr);
    }
    else
    {
        if ((addr & 0x00FF) == 0x0000)
        {
            mmByte_t ret = 0;
            if (p->eeprom_type == 0)
            {
                ret = mmEmuX24C01_Read(&p->x24c01);
            }
            else
            {
                if (p->eeprom_type == 1)
                {
                    ret = mmEmuX24C02_Read(&p->x24c02);
                }
                else
                {
                    if (p->eeprom_type == 2)
                    {
                        ret = mmEmuX24C02_Read(&p->x24c02) & mmEmuX24C01_Read(&p->x24c01);
                    }
                }
            }
            return (ret ? 0x10 : 0) | (mmEmuNes_GetBarcodeStatus(nes));
        }
    }
    return 0x00;
}
MM_EXPORT_EMU void mmEmuMapper016_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(super);

    if (!p->patch)
    {
        mmEmuMapper016_WriteSubA(p, addr, data);
    }
    else
    {
        mmEmuMapper_WriteLow(super, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper016_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(super);

    if (!p->patch)
    {
        mmEmuMapper016_WriteSubA(p, addr, data);
    }
    else
    {
        mmEmuMapper016_WriteSubB(p, addr, data);
    }
}

MM_EXPORT_EMU void mmEmuMapper016_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable && (mmEmuNes_GetIrqType(nes) == MM_EMU_NES_IRQ_HSYNC))
    {
        if (p->irq_counter <= 113)
        {
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);

            //mm_emu_cpu_IRQ(&nes->cpu);
            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
            //p->irq_enable = 0;
            //p->irq_counter = 0;

            p->irq_counter &= 0xFFFF;
        }
        else
        {
            p->irq_counter -= 113;
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper016_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable && (mmEmuNes_GetIrqType(nes) == MM_EMU_NES_IRQ_CLOCK))
    {
        if ((p->irq_counter -= cycles) <= 0)
        {
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);

            //mm_emu_cpu_IRQ(&nes->cpu);
            //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
            //p->irq_enable = 0;
            //p->irq_counter = 0;

            p->irq_counter &= 0xFFFF;
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper016_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper016_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->irq_enable;
    *(mmInt_t*)&buffer[4] = p->irq_counter;
    *(mmInt_t*)&buffer[8] = p->irq_latch;

    if (p->eeprom_type == 0)
    {
        mmEmuX24C01_Save(&p->x24c01, &buffer[16]);
    }
    else
    {
        if (p->eeprom_type == 1)
        {
            mmEmuX24C02_Save(&p->x24c02, &buffer[16]);
        }
        else
        {
            if (p->eeprom_type == 2)
            {
                mmEmuX24C02_Save(&p->x24c02, &buffer[16]);
                mmEmuX24C01_Save(&p->x24c01, &buffer[48]);
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper016_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->irq_enable = buffer[3];
    p->irq_counter = *(mmInt_t*)&buffer[4];
    p->irq_latch = *(mmInt_t*)&buffer[8];
    if (p->eeprom_type == 0)
    {
        mmEmuX24C01_Load(&p->x24c01, &buffer[16]);
    }
    else
    {
        if (p->eeprom_type == 1)
        {
            mmEmuX24C02_Load(&p->x24c02, &buffer[16]);
        }
        else
        {
            if (p->eeprom_type == 2)
            {
                mmEmuX24C02_Load(&p->x24c02, &buffer[16]);
                mmEmuX24C01_Load(&p->x24c01, &buffer[48]);
            }
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper016_Produce(void)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)mmMalloc(sizeof(struct mmEmuMapper016));
    mmEmuMapper016_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper016_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper016* p = (struct mmEmuMapper016*)(m);
    mmEmuMapper016_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER016 =
{
    &__static_mmEmuMapper016_Produce,
    &__static_mmEmuMapper016_Recycle,
};
