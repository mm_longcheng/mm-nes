/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper114.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper114_Init(struct mmEmuMapper114* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg_m = 0;
    p->reg_a = 0;
    mmMemset(p->reg_b, 0, sizeof(mmByte_t) * 8);
    p->reg_c = 0;

    p->irq_counter = 0;
    p->irq_occur = 0;

    //
    p->super.Reset = &mmEmuMapper114_Reset;
    p->super.Write = &mmEmuMapper114_Write;
    //p->super.Read = &mmEmuMapper114_Read;
    //p->super.ReadLow = &mmEmuMapper114_ReadLow;
    p->super.WriteLow = &mmEmuMapper114_WriteLow;
    //p->super.ExRead = &mmEmuMapper114_ExRead;
    //p->super.ExWrite = &mmEmuMapper114_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper114_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper114_ExCmdWrite;
    p->super.HSync = &mmEmuMapper114_HSync;
    //p->super.VSync = &mmEmuMapper114_VSync;
    p->super.Clock = &mmEmuMapper114_Clock;
    //p->super.PPULatch = &mmEmuMapper114_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper114_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper114_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper114_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper114_IsStateSave;
    p->super.SaveState = &mmEmuMapper114_SaveState;
    p->super.LoadState = &mmEmuMapper114_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper114_Destroy(struct mmEmuMapper114* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg_m = 0;
    p->reg_a = 0;
    mmMemset(p->reg_b, 0, sizeof(mmByte_t) * 8);
    p->reg_c = 0;

    p->irq_counter = 0;
    p->irq_occur = 0;
}

MM_EXPORT_EMU void mmEmuMapper114_SetBankCPU(struct mmEmuMapper114* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg_m & 0x80)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, p->reg_m & 0x1F);
    }
    else
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->reg_b[4]);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->reg_b[5]);
    }
}
MM_EXPORT_EMU void mmEmuMapper114_SetBankPPU(struct mmEmuMapper114* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetVROM2KBank(mmu, 0, p->reg_b[0] >> 1);
    mmEmuMmu_SetVROM2KBank(mmu, 2, p->reg_b[2] >> 1);
    mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg_b[6]);
    mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg_b[1]);
    mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg_b[7]);
    mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg_b[3]);
}

MM_EXPORT_EMU void mmEmuMapper114_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper114* p = (struct mmEmuMapper114*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->reg_a = p->reg_c = p->reg_m = 0;
    mmMemset(p->reg_b, 0, sizeof(mmByte_t) * 8);

    p->irq_counter = 0;
    p->irq_occur = 0;

    mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
}
MM_EXPORT_EMU void mmEmuMapper114_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper114* p = (struct mmEmuMapper114*)(super);

    p->reg_m = data;
    mmEmuMapper114_SetBankCPU(p);
}
MM_EXPORT_EMU void mmEmuMapper114_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper114* p = (struct mmEmuMapper114*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0xE003)
    {
        p->irq_counter = data;
    }
    else
    {
        if (addr == 0xE002)
        {
            p->irq_occur = 0;
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        else
        {
            switch (addr & 0xE000)
            {
            case 0x8000:
                if (data & 0x01)
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
                }
                else
                {
                    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
                }
                break;
            case 0xA000:
                p->reg_c = 1;
                p->reg_a = data;
                break;
            case 0xC000:
                if (!p->reg_c)
                {
                    break;
                }
                p->reg_b[p->reg_a & 0x07] = data;
                switch (p->reg_a & 0x07)
                {
                case 0:
                case 1:
                case 2:
                case 3:
                case 6:
                case 7:
                    mmEmuMapper114_SetBankPPU(p);
                    break;
                case 4:
                case 5:
                    mmEmuMapper114_SetBankCPU(p);
                    break;
                default:
                    break;
                }
                p->reg_c = 0;
                break;
            default:
                break;
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper114_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    //struct mmEmuMapper114* p = (struct mmEmuMapper114*)(super);

    //struct mmEmuNes* nes = p->super.nes;

    //if (p->irq_occur)
    //{
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    //}
}
MM_EXPORT_EMU void mmEmuMapper114_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper114* p = (struct mmEmuMapper114*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239) && mmEmuPpu_IsDispON(&nes->ppu))
    {
        if (p->irq_counter)
        {
            p->irq_counter--;
            if (!p->irq_counter)
            {
                p->irq_occur = 0xFF;
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper114_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper114_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper114* p = (struct mmEmuMapper114*)(super);

    mmMemcpy(&buffer[0], &p->reg_b[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->reg_m;
    buffer[9] = p->reg_a;
    buffer[10] = p->reg_c;
    buffer[11] = p->irq_counter;
    buffer[12] = p->irq_occur;
}
MM_EXPORT_EMU void mmEmuMapper114_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper114* p = (struct mmEmuMapper114*)(super);

    mmMemcpy(&p->reg_b[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->reg_m = buffer[8];
    p->reg_a = buffer[9];
    p->reg_c = buffer[10];
    p->irq_counter = buffer[11];
    p->irq_occur = buffer[12];
}

static struct mmEmuMapper* __static_mmEmuMapper114_Produce(void)
{
    struct mmEmuMapper114* p = (struct mmEmuMapper114*)mmMalloc(sizeof(struct mmEmuMapper114));
    mmEmuMapper114_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper114_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper114* p = (struct mmEmuMapper114*)(m);
    mmEmuMapper114_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER114 =
{
    &__static_mmEmuMapper114_Produce,
    &__static_mmEmuMapper114_Recycle,
};
