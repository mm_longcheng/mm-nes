@echo off 

set VSVARS="%VS141COMNTOOLS%vcvars64.bat"
set VC_VER=141

call %VSVARS%

devenv %~dp0/virtuanes.sln /build "Debug|x86"
devenv %~dp0/virtuanes.sln /build "Release|x86"

pause