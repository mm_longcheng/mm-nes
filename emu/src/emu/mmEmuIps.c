/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuIps.h"

#include "core/mmString.h"

MM_EXPORT_EMU mmBool_t mmEmuIps_PatchIPS(mmByte_t* pIPS, mmByte_t* pROM, mmLong_t romsize, mmLong_t ipssize)
{
    mmInt_t index = 0;

    mmInt_t offset, length;

    mmByte_t data = 0;

    // IPS patch?
    if (mmStrncmp((const char *)pIPS, "PATCH", 5) != 0)
    {
        return  MM_FALSE;
    }

    index += 5;

    while (MM_TRUE)
    {
        // read patch address offset
        if (index + 3 > ipssize)
        {
            return MM_FALSE;
        }
        offset = ((mmInt_t)pIPS[index] << 16) | ((mmInt_t)pIPS[index + 1] << 8) | ((mmInt_t)pIPS[index + 2]);
        index += 3;

        // END?
        if (offset == 0x00454f46)
        {
            break;
        }

        // read patch length
        if (index + 2 > ipssize)
        {
            return MM_FALSE;
        }
        length = ((mmInt_t)pIPS[index] << 8) | ((mmInt_t)pIPS[index + 1]);
        index += 2;

        if (length)
        {
            while (length--)
            {
                if (offset < romsize)
                {
                    pROM[offset++] = pIPS[index];
                }
                if (++index > ipssize)
                {
                    return MM_FALSE;
                }
            }
        }
        else
        {
            if (index + 2 > ipssize)
            {
                return MM_FALSE;
            }
            length = ((mmInt_t)pIPS[index] << 8) | ((mmInt_t)pIPS[index + 1]);
            index += 2;

            data = pIPS[index];
            if (++index > ipssize)
            {
                return MM_FALSE;
            }

            while (length--)
            {
                if (offset < romsize)
                {
                    pROM[offset++] = data;
                }
            }
        }
    }

    return  MM_TRUE;
}

