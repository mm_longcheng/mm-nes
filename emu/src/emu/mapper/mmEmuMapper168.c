/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper168.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper168_Init(struct mmEmuMapper168* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg5000 = 0;
    p->reg5200 = 0;
    p->reg5300 = 0;

    p->PPU_SW = 0;
    p->Rom_Type = 0;

    //
    p->super.Reset = &mmEmuMapper168_Reset;
    p->super.Write = &mmEmuMapper168_Write;
    //p->super.Read = &mmEmuMapper168_Read;
    p->super.ReadLow = &mmEmuMapper168_ReadLow;
    p->super.WriteLow = &mmEmuMapper168_WriteLow;
    //p->super.ExRead = &mmEmuMapper168_ExRead;
    //p->super.ExWrite = &mmEmuMapper168_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper168_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper168_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper168_HSync;
    //p->super.VSync = &mmEmuMapper168_VSync;
    //p->super.Clock = &mmEmuMapper168_Clock;
    //p->super.PPULatch = &mmEmuMapper168_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper168_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper168_PPUExtLatchX;
    p->super.PPUExtLatch = &mmEmuMapper168_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper168_IsStateSave;
    p->super.SaveState = &mmEmuMapper168_SaveState;
    p->super.LoadState = &mmEmuMapper168_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper168_Destroy(struct mmEmuMapper168* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg5000 = 0;
    p->reg5200 = 0;
    p->reg5300 = 0;

    p->PPU_SW = 0;
    p->Rom_Type = 0;
}

MM_EXPORT_EMU void mmEmuMapper168_SetBankCPU(struct mmEmuMapper168* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg5200 < 4)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, p->reg5000);
    }
    else
    {
        mmEmuMmu_SetPROM32KBank(mmu, p->reg5000);
    }

    switch (p->reg5200)
    {
    case 0:
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        p->PPU_SW = 0;
        break;
    case 2:
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        p->PPU_SW = 1;
        break;
    case 1:
    case 3:
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        p->PPU_SW = 0;
        break;
    case 5:
        if (p->reg5000 == 4 && p->Rom_Type == 2)    //Special for [Subor] Subor V11.0 (C) - Tank (Tank battle)
        {
            mmEmuPpu_SetExtLatchMode(&nes->ppu, MM_FALSE);
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper168_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper168* p = (struct mmEmuMapper168*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->reg5000 = 0;
    p->reg5200 = 0;
    p->reg5300 = 0;
    p->PPU_SW = 0;

    mmEmuPpu_SetExtLatchMode(&nes->ppu, MM_TRUE);

    mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
    mmEmuMmu_SetPROM16KBank(mmu, 6, 0);

    p->Rom_Type = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x0A9808AE)      //[Subor] Karaoke (C)
    {
        p->Rom_Type = 1;
        mmEmuMmu_SetPROM32KBank(mmu, 0);
    }
    if (crc == 0x12D61CE8)      //[Subor] Subor V11.0 (C)
    {
        p->Rom_Type = 2;
    }
}
MM_EXPORT_EMU mmByte_t mmEmuMapper168_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    //  if(addr<0x6000) DEBUGOUT( "ReadLow - addr= %04x ; dat= %03x\n", addr, Mapper::ReadLow( addr ) );

    if (addr == 0x5300)
    {
        //Return 0x8F, skip the block related to vocal voice
        return 0x8F;
    }
    else
    {
        return mmEmuMapper_ReadLow(super, addr);
    }
}
MM_EXPORT_EMU void mmEmuMapper168_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper168* p = (struct mmEmuMapper168*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //  if(addr<0x6000) DEBUGOUT("Address=%04X Data=%02X\n", addr&0xFFFF, data&0xFF );

    if (addr == 0x5000)
    {
        p->reg5000 = data;
        mmEmuMapper168_SetBankCPU(p);
    }
    else if (addr == 0x5200)
    {
        p->reg5200 = data & 0x7;
        mmEmuMapper168_SetBankCPU(p);
    }
    else if (addr == 0x5300)
    {
        //Real voice voice related port.
        p->reg5300 = data;
    }
    else if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }
}
MM_EXPORT_EMU void mmEmuMapper168_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper168* p = (struct mmEmuMapper168*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //  DEBUGOUT("Address=%04X Data=%02X\n", addr&0xFFFF, data&0xFF );

    if (p->Rom_Type == 1)           //[Subor] Karaoke (C)
    {
        mmEmuMmu_SetPROM32KBank(mmu, data & 0x1F);

        if (data & 0x40)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        if (data & 0xC0)
        {
            p->PPU_SW = 1;
        }
        else
        {
            p->PPU_SW = 0;
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper168_PPUExtLatch(struct mmEmuMapper* super, mmWord_t ntbladr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr)
{
    struct mmEmuMapper168* p = (struct mmEmuMapper168*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t loopy_v = mmEmuPpu_GetPPUADDR(&nes->ppu);
    mmInt_t loopy_y = mmEmuPpu_GetTILEY(&nes->ppu);
    mmInt_t tileofs = (mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) << 8;
    mmInt_t attradr = 0x23C0 + (loopy_v & 0x0C00) + ((loopy_v & 0x0380) >> 4);
    mmInt_t attrsft = (ntbladr & 0x0040) >> 4;
    mmByte_t* pNTBL = mmu->PPU_MEM_BANK[ntbladr >> 10];
    mmInt_t ntbl_x = ntbladr & 0x001F;
    mmInt_t tileadr, ntb;

    ntb = (ntbladr >> 10) & 3;

    if (ntb == 2)
    {
        tileofs |= 0x1000;
    }
    else if (ntb && p->PPU_SW)
    {
        tileofs |= 0x1000;
    }
    else
    {
        tileofs |= 0x0000;
    }

    //  DEBUGOUT("ntb=%04X tileofs=%02X\n", ntb, tileofs );

    attradr &= 0x3FF;
    (*attr) = ((pNTBL[attradr + (ntbl_x >> 2)] >> ((ntbl_x & 2) + attrsft)) & 3) << 2;
    tileadr = tileofs + pNTBL[ntbladr & 0x03FF] * 0x10 + loopy_y;

    (*chr_l) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
    (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper168_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper168_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper168* p = (struct mmEmuMapper168*)(super);

    buffer[0] = p->reg5000;
    buffer[1] = p->reg5200;
    buffer[1] = p->reg5300;
    buffer[1] = p->PPU_SW;
    buffer[1] = p->Rom_Type;
}
MM_EXPORT_EMU void mmEmuMapper168_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper168* p = (struct mmEmuMapper168*)(super);

    p->reg5000 = buffer[0];
    p->reg5200 = buffer[1];
    p->reg5300 = buffer[1];
    p->PPU_SW = buffer[1];
    p->Rom_Type = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper168_Produce(void)
{
    struct mmEmuMapper168* p = (struct mmEmuMapper168*)mmMalloc(sizeof(struct mmEmuMapper168));
    mmEmuMapper168_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper168_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper168* p = (struct mmEmuMapper168*)(m);
    mmEmuMapper168_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER168 =
{
    &__static_mmEmuMapper168_Produce,
    &__static_mmEmuMapper168_Recycle,
};
