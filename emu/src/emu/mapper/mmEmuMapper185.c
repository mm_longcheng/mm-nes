/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper185.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper185_Init(struct mmEmuMapper185* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;

    //
    p->super.Reset = &mmEmuMapper185_Reset;
    p->super.Write = &mmEmuMapper185_Write;
    //p->super.Read = &mmEmuMapper185_Read;
    //p->super.ReadLow = &mmEmuMapper185_ReadLow;
    //p->super.WriteLow = &mmEmuMapper185_WriteLow;
    //p->super.ExRead = &mmEmuMapper185_ExRead;
    //p->super.ExWrite = &mmEmuMapper185_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper185_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper185_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper185_HSync;
    //p->super.VSync = &mmEmuMapper185_VSync;
    //p->super.Clock = &mmEmuMapper185_Clock;
    //p->super.PPULatch = &mmEmuMapper185_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper185_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper185_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper185_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper185_IsStateSave;
    //p->super.SaveState = &mmEmuMapper185_SaveState;
    //p->super.LoadState = &mmEmuMapper185_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper185_Destroy(struct mmEmuMapper185* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;
}

MM_EXPORT_EMU void mmEmuMapper185_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper185* p = (struct mmEmuMapper185*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    switch (mmu->PROM_16K_SIZE)
    {
    case 1: // 16K only
        mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
        mmEmuMmu_SetPROM16KBank(mmu, 6, 0);
        break;
    case 2: // 32K
        mmEmuMmu_SetPROM32KBank(mmu, 0);
        break;
    default:
        break;
    }

    //for (INT i = 0; i < 0x400; i++) 
    //{
    //  mmu->VRAM[0x800 + i] = 0xFF;
    //}
    mmMemset(&mmu->VRAM[0x800], 0xFF, 0x400);

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xb36457c7) // Spy vs Spy(J)
    {
        p->patch = 1;
    }
}
MM_EXPORT_EMU void mmEmuMapper185_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper185* p = (struct mmEmuMapper185*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((!p->patch && (data & 0x03)) || (p->patch && data == 0x21))
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
    else
    {
        mmEmuMmu_SetVRAM1KBank(mmu, 0, 2);  // use vram bank 2
        mmEmuMmu_SetVRAM1KBank(mmu, 1, 2);
        mmEmuMmu_SetVRAM1KBank(mmu, 2, 2);
        mmEmuMmu_SetVRAM1KBank(mmu, 3, 2);
        mmEmuMmu_SetVRAM1KBank(mmu, 4, 2);
        mmEmuMmu_SetVRAM1KBank(mmu, 5, 2);
        mmEmuMmu_SetVRAM1KBank(mmu, 6, 2);
        mmEmuMmu_SetVRAM1KBank(mmu, 7, 2);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper185_Produce(void)
{
    struct mmEmuMapper185* p = (struct mmEmuMapper185*)mmMalloc(sizeof(struct mmEmuMapper185));
    mmEmuMapper185_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper185_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper185* p = (struct mmEmuMapper185*)(m);
    mmEmuMapper185_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER185 =
{
    &__static_mmEmuMapper185_Produce,
    &__static_mmEmuMapper185_Recycle,
};
