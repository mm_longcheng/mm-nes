/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuExtSoundFile_h__
#define __mmEmuExtSoundFile_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

enum mmEmuExtSoundFile_t
{
    MM_EMU_ESF_MOEPRO_STRIKE = 0,   // Strike!
    MM_EMU_ESF_MOEPRO_BALL,         // ball
    MM_EMU_ESF_MOEPRO_TIME,         // Taime
    MM_EMU_ESF_MOEPRO_OUT,          // Ah!
    MM_EMU_ESF_MOEPRO_SAFE,         // safe
    MM_EMU_ESF_MOEPRO_FAIRBALL,     // Fall
    MM_EMU_ESF_MOEPRO_FOULBALL,     // Fair!
    MM_EMU_ESF_MOEPRO_BATTEROUT,    // Battery Au!
    MM_EMU_ESF_MOEPRO_PLAYBALL,     // Playball
    MM_EMU_ESF_MOEPRO_FOURBALL,     // Forebow
    MM_EMU_ESF_MOEPRO_HOMERUN,      // Homanck!
    MM_EMU_ESF_MOEPRO_PITCHER,      // Picha change
    MM_EMU_ESF_MOEPRO_OUCH,         // It
    MM_EMU_ESF_MOEPRO_AHO,          // Ah ~ Hott
    MM_EMU_ESF_MOEPRO_KNOCK,        // Bang sound
    MM_EMU_ESF_MOEPRO_WA,           // Wow! (Are you OK?

    MM_EMU_ESF_MOETENNIS_00,        // ?
    MM_EMU_ESF_MOETENNIS_01,        // ?
    MM_EMU_ESF_MOETENNIS_02,        // love
    MM_EMU_ESF_MOETENNIS_03,        // Fifteen
    MM_EMU_ESF_MOETENNIS_04,        // Thirty
    MM_EMU_ESF_MOETENNIS_05,        // Forty
    MM_EMU_ESF_MOETENNIS_06,        // advantage
    MM_EMU_ESF_MOETENNIS_07,        // server
    MM_EMU_ESF_MOETENNIS_08,        // receiver
    MM_EMU_ESF_MOETENNIS_09,        // All
    MM_EMU_ESF_MOETENNIS_0A,        // Deuce
    MM_EMU_ESF_MOETENNIS_0B,        // ?
    MM_EMU_ESF_MOETENNIS_0C,        // shot
    MM_EMU_ESF_MOETENNIS_0D,        // Cheers (applause)
    MM_EMU_ESF_MOETENNIS_0E,        // Fault
    MM_EMU_ESF_MOETENNIS_0F,        // A let
    MM_EMU_ESF_MOETENNIS_10,        // out!
    MM_EMU_ESF_MOETENNIS_11,        // ?
    MM_EMU_ESF_MOETENNIS_12,        // Sound hitting your body?

    MM_EMU_ESF_DISKSYSTEM_BOOT,     // Cam sound
    MM_EMU_ESF_DISKSYSTEM_MOTOR,    // Motor sound (loop)
    MM_EMU_ESF_DISKSYSTEM_SEEKEND,  // Stop sound

    MM_EMU_ESF_FILE_MAX,
};

#include "core/mmSuffix.h"

#endif//__mmEmuExtSoundFile_h__
