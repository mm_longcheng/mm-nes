/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmulatorAssets_h__
#define __mmEmulatorAssets_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "emulator/mmEmulatorExport.h"

#include "core/mmPrefix.h"

struct mmFileContext;

struct mmEmulatorAssets
{
    struct mmString fullname;
    struct mmString pathname;
    struct mmString basename;
    struct mmString suffixname;

    struct mmString rootpath;

    struct mmString assets_name;
    struct mmString source_name;

    struct mmString writable_directory;
    struct mmString writable_path;
    struct mmString writable_base;

    struct mmString emulator_directory;

    // MM_EMU_ASSETS_FOLDER
    // MM_EMU_ASSETS_SOURCE
    mmUInt32_t assets_type;

    struct mmFileContext* file_context;
};
MM_EXPORT_EMULATOR void mmEmulatorAssets_Init(struct mmEmulatorAssets* p);
MM_EXPORT_EMULATOR void mmEmulatorAssets_Destroy(struct mmEmulatorAssets* p);

MM_EXPORT_EMULATOR void mmEmulatorAssets_Reset(struct mmEmulatorAssets* p, struct mmEmulatorAssets* q);
// copy q -> p
MM_EXPORT_EMULATOR void mmEmulatorAssets_Copy(struct mmEmulatorAssets* p, struct mmEmulatorAssets* q);

MM_EXPORT_EMULATOR void mmEmulatorAssets_SetPath(struct mmEmulatorAssets* p, const char* path, const char* base);
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetAssetsType(struct mmEmulatorAssets* p, mmUInt32_t assets_type);
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetRootPath(struct mmEmulatorAssets* p, const char* rootpath);
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetAssetsName(struct mmEmulatorAssets* p, const char* assets_name);
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetSourceName(struct mmEmulatorAssets* p, const char* source_name);
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetWritable(struct mmEmulatorAssets* p, const char* writable_path, const char* writable_base);
MM_EXPORT_EMULATOR void mmEmulatorAssets_SetFileContext(struct mmEmulatorAssets* p, struct mmFileContext* file_context);

#include "core/mmSuffix.h"

#endif//__mmEmulatorAssets_h__