/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper007.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper007_Init(struct mmEmuMapper007* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;

    //
    p->super.Reset = &mmEmuMapper007_Reset;
    p->super.Write = &mmEmuMapper007_Write;
    //p->super.Read = &mmEmuMapper007_Read;
    //p->super.ReadLow = &mmEmuMapper007_ReadLow;
    //p->super.WriteLow = &mmEmuMapper007_WriteLow;
    //p->super.ExRead = &mmEmuMapper007_ExRead;
    //p->super.ExWrite = &mmEmuMapper007_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper007_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper007_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper007_HSync;
    //p->super.VSync = &mmEmuMapper007_VSync;
    //p->super.Clock = &mmEmuMapper007_Clock;
    //p->super.PPULatch = &mmEmuMapper007_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper007_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper007_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper007_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper007_IsStateSave;
    //p->super.SaveState = &mmEmuMapper007_SaveState;
    //p->super.LoadState = &mmEmuMapper007_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper007_Destroy(struct mmEmuMapper007* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;
}

MM_EXPORT_EMU void mmEmuMapper007_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper007* p = (struct mmEmuMapper007*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);

    crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);

    if (crc == 0x3c9fe649)          // WWF Wrestlemania Challenge(U)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        p->patch = 1;
    }
    if (crc == 0x09874777)          // Marble Madness(U)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }

    if (crc == 0x279710DC ||        // Battletoads (U)
        crc == 0xCEB65B06)          // Battletoads Double Dragon (U)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
        mmMemset(mmu->WRAM, 0, sizeof(mmu->WRAM));
    }
}
MM_EXPORT_EMU void mmEmuMapper007_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper007* p = (struct mmEmuMapper007*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, data & 0x07);

    if (!p->patch)
    {
        if (data & 0x10)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper007_Produce(void)
{
    struct mmEmuMapper007* p = (struct mmEmuMapper007*)mmMalloc(sizeof(struct mmEmuMapper007));
    mmEmuMapper007_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper007_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper007* p = (struct mmEmuMapper007*)(m);
    mmEmuMapper007_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER007 =
{
    &__static_mmEmuMapper007_Produce,
    &__static_mmEmuMapper007_Recycle,
};
