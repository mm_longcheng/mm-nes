/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper025.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper025_Init(struct mmEmuMapper025* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 11);
    p->irq_enable = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_counter = 0;
    p->irq_clock = 0;

    //
    p->super.Reset = &mmEmuMapper025_Reset;
    p->super.Write = &mmEmuMapper025_Write;
    //p->super.Read = &mmEmuMapper025_Read;
    //p->super.ReadLow = &mmEmuMapper025_ReadLow;
    //p->super.WriteLow = &mmEmuMapper025_WriteLow;
    //p->super.ExRead = &mmEmuMapper025_ExRead;
    //p->super.ExWrite = &mmEmuMapper025_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper025_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper025_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper025_HSync;
    //p->super.VSync = &mmEmuMapper025_VSync;
    p->super.Clock = &mmEmuMapper025_Clock;
    //p->super.PPULatch = &mmEmuMapper025_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper025_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper025_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper025_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper025_IsStateSave;
    p->super.SaveState = &mmEmuMapper025_SaveState;
    p->super.LoadState = &mmEmuMapper025_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper025_Destroy(struct mmEmuMapper025* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 11);
    p->irq_enable = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_counter = 0;
    p->irq_clock = 0;
}

MM_EXPORT_EMU void mmEmuMapper025_SetBankCPU(struct mmEmuMapper025* p)
{
    // need do nothing.
}
MM_EXPORT_EMU void mmEmuMapper025_SetBankPPU(struct mmEmuMapper025* p)
{
    // need do nothing.
}

MM_EXPORT_EMU void mmEmuMapper025_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper025* p = (struct mmEmuMapper025*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i = 0;

    mmUInt32_t crc = 0;

    for (i = 0; i < 11; i++)
    {
        p->reg[i] = 0;
    }
    p->reg[9] = mmu->PROM_08K_SIZE - 2;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_clock = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xc71d4ce7)      // Gradius II(J)
    {
        //mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
    if (crc == 0xa2e68da8)      // For Racer Mini Yonku - Japan Cup(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0xea74c587)      // For Teenage Mutant Ninja Turtles(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x5f82cb7d)      // For Teenage Mutant Ninja Turtles 2(J)
    {

    }
    if (crc == 0x0bbd85ff)      // For Bio Miracle Bokutte Upa(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
}
MM_EXPORT_EMU void mmEmuMapper025_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper025* p = (struct mmEmuMapper025*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //if( addr >= 0xF000 )
    //DEBUGOUT( "M25 WR $%04X=$%02X L=%3d\n", addr, data, nes->GetScanline() );

    switch (addr & 0xF000)
    {
    case 0x8000:
        if (p->reg[10] & 0x02)
        {
            p->reg[9] = data;
            mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        }
        else
        {
            p->reg[8] = data;
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        }
        break;
    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    default:
        break;
    }

    switch (addr & 0xF00F)
    {
    case 0x9000:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0x9001:
    case 0x9004:
        if ((p->reg[10] & 0x02) != (data & 0x02))
        {
            mmByte_t swap = p->reg[8];
            p->reg[8] = p->reg[9];
            p->reg[9] = swap;

            mmEmuMmu_SetPROM8KBank(mmu, 4, p->reg[8]);
            mmEmuMmu_SetPROM8KBank(mmu, 6, p->reg[9]);
        }
        p->reg[10] = data;
        break;

    case 0xB000:
        p->reg[0] = (p->reg[0] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;
    case 0xB002:
    case 0xB008:
        p->reg[0] = (p->reg[0] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;

    case 0xB001:
    case 0xB004:
        p->reg[1] = (p->reg[1] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;
    case 0xB003:
    case 0xB00C:
        p->reg[1] = (p->reg[1] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;

    case 0xC000:
        p->reg[2] = (p->reg[2] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;
    case 0xC002:
    case 0xC008:
        p->reg[2] = (p->reg[2] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;

    case 0xC001:
    case 0xC004:
        p->reg[3] = (p->reg[3] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;
    case 0xC003:
    case 0xC00C:
        p->reg[3] = (p->reg[3] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;

    case 0xD000:
        p->reg[4] = (p->reg[4] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;
    case 0xD002:
    case 0xD008:
        p->reg[4] = (p->reg[4] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;

    case 0xD001:
    case 0xD004:
        p->reg[5] = (p->reg[5] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;
    case 0xD003:
    case 0xD00C:
        p->reg[5] = (p->reg[5] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;

    case 0xE000:
        p->reg[6] = (p->reg[6] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;
    case 0xE002:
    case 0xE008:
        p->reg[6] = (p->reg[6] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;

    case 0xE001:
    case 0xE004:
        p->reg[7] = (p->reg[7] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;
    case 0xE003:
    case 0xE00C:
        p->reg[7] = (p->reg[7] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;

    case 0xF000:
        p->irq_latch = (p->irq_latch & 0xF0) | (data & 0x0F);
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF002:
    case 0xF008:
        p->irq_latch = (p->irq_latch & 0x0F) | ((data & 0x0F) << 4);
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF001:
    case 0xF004:
        p->irq_enable = data & 0x03;
        //          irq_counter = 0x100 - irq_latch;
        p->irq_counter = p->irq_latch;
        p->irq_clock = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF003:
    case 0xF00C:
        p->irq_enable = (p->irq_enable & 0x01) * 3;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper025_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper025* p = (struct mmEmuMapper025*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable & 0x02)
    {
        p->irq_clock += cycles * 3;
        while (p->irq_clock >= 341)
        {
            p->irq_clock -= 341;
            p->irq_counter++;
            if (p->irq_counter == 0)
            {
                p->irq_counter = p->irq_latch;
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper025_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper025_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper025* p = (struct mmEmuMapper025*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 11);
    buffer[11] = p->irq_enable;
    buffer[12] = p->irq_occur;
    buffer[13] = p->irq_latch;
    buffer[14] = p->irq_counter;
    *((mmInt_t*)&buffer[15]) = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper025_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper025* p = (struct mmEmuMapper025*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 11);
    p->irq_enable = buffer[11];
    p->irq_occur = buffer[12];
    p->irq_latch = buffer[13];
    p->irq_counter = buffer[14];
    p->irq_clock = *((mmInt_t*)&buffer[15]);
}

static struct mmEmuMapper* __static_mmEmuMapper025_Produce(void)
{
    struct mmEmuMapper025* p = (struct mmEmuMapper025*)mmMalloc(sizeof(struct mmEmuMapper025));
    mmEmuMapper025_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper025_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper025* p = (struct mmEmuMapper025*)(m);
    mmEmuMapper025_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER025 =
{
    &__static_mmEmuMapper025_Produce,
    &__static_mmEmuMapper025_Recycle,
};
