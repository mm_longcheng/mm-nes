/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmulatorFrame_h__
#define __mmEmulatorFrame_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmAtomic.h"

#include "nwsi/mmContextMaster.h"

#include "CEGUI/BasicImage.h"

#include "OgreHardwarePixelBuffer.h"

#include "emulator/mmEmulatorExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_EMULATOR extern const char* MM_EMULATOR_FRAME_OGRE_TEXTURE;
MM_EXPORT_EMULATOR extern const char* MM_EMULATOR_FRAME_CEGUI_TEXTURE;
MM_EXPORT_EMULATOR extern const char* MM_EMULATOR_FRAME_CEGUI_IMAGERY;

struct mmEmulatorFrame
{
    // weak ref.
    struct mmContextMaster* pContextMaster;

    // strong ref. name pattern resource. default is "mm".
    std::string hName;
    // default is "emulator"
    std::string hMaterialPathPrefix;
    // default is "EMU/GeometryBuffer/emulator_(%s d_Name)"
    std::string hMaterialName;

    std::string hNameOgreTextureEmu0;
    std::string hNameOgreTextureEmu1;
    std::string hNameCEGUITextureEmu;
    std::string hNameCEGUIImageryEmu;

    // strong ref.
    Ogre::TexturePtr hOgreTexture0;
    // strong ref.
    Ogre::TexturePtr hOgreTexture1;

    // strong ref.
    CEGUI::Texture* pCEGUITexture;
    // strong ref.
    CEGUI::BasicImage* pCEGUIImagery;

    // weak ref.
    Ogre::HardwarePixelBufferSharedPtr hHardwarePixelBuffer0;
    // weak ref.
    Ogre::HardwarePixelBufferSharedPtr hHardwarePixelBuffer1;

    // weak ref.is target to d_HardwarePixelBuffer0.
    Ogre::RenderTarget* pRenderTarget0;
    // weak ref.is target to d_HardwarePixelBuffer1.
    Ogre::RenderTarget* pRenderTarget1;

    Ogre::MaterialPtr hMaterialPtr;

    // cache value.
    Ogre::PixelBox hSrcBox;
    Ogre::Box hDstBox;

    // weak ref.
    void* pPaletteRGB;

    // cache texture w h.
    mmUInt32_t hTextureW;
    mmUInt32_t hTextureH;

    mmAtomic_t hLockerScreenPtr0;
    mmAtomic_t hLockerScreenPtr1;
    mmUInt32_t hScreenBuffSize;
    mmUInt32_t hScreenRealSize;
    mmByte_t* pScreenBufferPtr;
    mmByte_t* pScreenPtr[2];
};
MM_EXPORT_EMULATOR void mmEmulatorFrame_Init(struct mmEmulatorFrame* p);
MM_EXPORT_EMULATOR void mmEmulatorFrame_Destroy(struct mmEmulatorFrame* p);

MM_EXPORT_EMULATOR void mmEmulatorFrame_SetContextMaster(struct mmEmulatorFrame* p, struct mmContextMaster* pContextMaster);
MM_EXPORT_EMULATOR void mmEmulatorFrame_SetName(struct mmEmulatorFrame* p, const std::string& name);
MM_EXPORT_EMULATOR void mmEmulatorFrame_SetMaterialPathPrefix(struct mmEmulatorFrame* p, const std::string& path);
MM_EXPORT_EMULATOR void mmEmulatorFrame_SetPaletteRGB(struct mmEmulatorFrame* p, void* data);

MM_EXPORT_EMULATOR void mmEmulatorFrame_FinishLaunching(struct mmEmulatorFrame* p);
MM_EXPORT_EMULATOR void mmEmulatorFrame_BeforeTerminate(struct mmEmulatorFrame* p);

MM_EXPORT_EMULATOR void mmEmulatorFrame_EnterBackground(struct mmEmulatorFrame* p);
MM_EXPORT_EMULATOR void mmEmulatorFrame_EnterForeground(struct mmEmulatorFrame* p);

// render thread.
// palette_rgb x 256
MM_EXPORT_EMULATOR void mmEmulatorFrame_UpdatePalette(struct mmEmulatorFrame* p);
MM_EXPORT_EMULATOR void mmEmulatorFrame_UpdateFrameBitmap(struct mmEmulatorFrame* p);

// emulator thread.
MM_EXPORT_EMULATOR int mmEmulatorFrame_ProcessBuffer(struct mmEmulatorFrame* p, mmByte_t* buffer, size_t length);

#include "core/mmSuffix.h"

#endif//__mmEmulatorFrame_h__
