/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper067.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper067_Init(struct mmEmuMapper067* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_occur = 0;
    p->irq_toggle = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper067_Reset;
    p->super.Write = &mmEmuMapper067_Write;
    //p->super.Read = &mmEmuMapper067_Read;
    //p->super.ReadLow = &mmEmuMapper067_ReadLow;
    //p->super.WriteLow = &mmEmuMapper067_WriteLow;
    //p->super.ExRead = &mmEmuMapper067_ExRead;
    //p->super.ExWrite = &mmEmuMapper067_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper067_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper067_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper067_HSync;
    //p->super.VSync = &mmEmuMapper067_VSync;
    p->super.Clock = &mmEmuMapper067_Clock;
    //p->super.PPULatch = &mmEmuMapper067_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper067_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper067_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper067_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper067_IsStateSave;
    p->super.SaveState = &mmEmuMapper067_SaveState;
    p->super.LoadState = &mmEmuMapper067_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper067_Destroy(struct mmEmuMapper067* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_occur = 0;
    p->irq_toggle = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper067_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper067* p = (struct mmEmuMapper067*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->irq_enable = 0;
    p->irq_toggle = 0;
    p->irq_counter = 0;
    p->irq_occur = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    mmEmuMmu_SetVROM4KBank(mmu, 0, 0);
    mmEmuMmu_SetVROM4KBank(mmu, 4, mmu->VROM_4K_SIZE - 1);

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x7f2a04bf)          // For Fantasy Zone 2(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
}
MM_EXPORT_EMU void mmEmuMapper067_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper067* p = (struct mmEmuMapper067*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF800)
    {
    case 0x8800:
        mmEmuMmu_SetVROM2KBank(mmu, 0, data);
        break;
    case 0x9800:
        mmEmuMmu_SetVROM2KBank(mmu, 2, data);
        break;
    case 0xA800:
        mmEmuMmu_SetVROM2KBank(mmu, 4, data);
        break;
    case 0xB800:
        mmEmuMmu_SetVROM2KBank(mmu, 6, data);
        break;

    case 0xC800:
        if (!p->irq_toggle)
        {
            p->irq_counter = (p->irq_counter & 0x00FF) | ((mmInt_t)data << 8);
        }
        else
        {
            p->irq_counter = (p->irq_counter & 0xFF00) | ((mmInt_t)data & 0xFF);
        }
        p->irq_toggle ^= 1;
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xD800:
        p->irq_enable = data & 0x10;
        p->irq_toggle = 0;
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xE800:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0xF800:
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper067_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper067* p = (struct mmEmuMapper067*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if ((p->irq_counter -= cycles) <= 0)
        {
            p->irq_enable = 0;
            p->irq_occur = 0xFF;
            p->irq_counter = 0xFFFF;
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }

    //if (p->irq_occur) 
    //{
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    //}
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper067_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper067_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper067* p = (struct mmEmuMapper067*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_occur;
    buffer[2] = p->irq_toggle;
    *((mmInt_t*)&buffer[3]) = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper067_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper067* p = (struct mmEmuMapper067*)(super);

    p->irq_enable = buffer[0];
    p->irq_occur = buffer[1];
    p->irq_toggle = buffer[2];
    p->irq_counter = *((mmInt_t*)&buffer[3]);
}

static struct mmEmuMapper* __static_mmEmuMapper067_Produce(void)
{
    struct mmEmuMapper067* p = (struct mmEmuMapper067*)mmMalloc(sizeof(struct mmEmuMapper067));
    mmEmuMapper067_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper067_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper067* p = (struct mmEmuMapper067*)(m);
    mmEmuMapper067_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER067 =
{
    &__static_mmEmuMapper067_Produce,
    &__static_mmEmuMapper067_Recycle,
};
