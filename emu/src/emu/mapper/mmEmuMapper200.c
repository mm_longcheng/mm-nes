/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper200.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper200_Init(struct mmEmuMapper200* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper200_Reset;
    p->super.Write = &mmEmuMapper200_Write;
    //p->super.Read = &mmEmuMapper200_Read;
    //p->super.ReadLow = &mmEmuMapper200_ReadLow;
    //p->super.WriteLow = &mmEmuMapper200_WriteLow;
    //p->super.ExRead = &mmEmuMapper200_ExRead;
    //p->super.ExWrite = &mmEmuMapper200_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper200_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper200_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper200_HSync;
    //p->super.VSync = &mmEmuMapper200_VSync;
    //p->super.Clock = &mmEmuMapper200_Clock;
    //p->super.PPULatch = &mmEmuMapper200_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper200_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper200_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper200_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper200_IsStateSave;
    //p->super.SaveState = &mmEmuMapper200_SaveState;
    //p->super.LoadState = &mmEmuMapper200_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper200_Destroy(struct mmEmuMapper200* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper200_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper200* p = (struct mmEmuMapper200*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //mmEmuMmu_SetPROM32KBank(mmu, 0, 1, mmu->PROM_08K_SIZE-2, mmu->PROM_08K_SIZE-1);
    mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
    mmEmuMmu_SetPROM16KBank(mmu, 6, 0);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper200_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper200* p = (struct mmEmuMapper200*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM16KBank(mmu, 4, addr & 0x07);
    mmEmuMmu_SetPROM16KBank(mmu, 6, addr & 0x07);
    mmEmuMmu_SetVROM8KBank(mmu, addr & 0x07);

    if (addr & 0x01)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper200_Produce(void)
{
    struct mmEmuMapper200* p = (struct mmEmuMapper200*)mmMalloc(sizeof(struct mmEmuMapper200));
    mmEmuMapper200_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper200_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper200* p = (struct mmEmuMapper200*)(m);
    mmEmuMapper200_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER200 =
{
    &__static_mmEmuMapper200_Produce,
    &__static_mmEmuMapper200_Recycle,
};
