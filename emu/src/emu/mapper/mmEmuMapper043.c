/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper043.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper043_Init(struct mmEmuMapper043* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0xFF;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper043_Reset;
    p->super.Write = &mmEmuMapper043_Write;
    //p->super.Read = &mmEmuMapper043_Read;
    p->super.ReadLow = &mmEmuMapper043_ReadLow;
    p->super.WriteLow = &mmEmuMapper043_WriteLow;
    //p->super.ExRead = &mmEmuMapper043_ExRead;
    p->super.ExWrite = &mmEmuMapper043_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper043_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper043_ExCmdWrite;
    p->super.HSync = &mmEmuMapper043_HSync;
    //p->super.VSync = &mmEmuMapper043_VSync;
    //p->super.Clock = &mmEmuMapper043_Clock;
    //p->super.PPULatch = &mmEmuMapper043_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper043_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper043_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper043_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper043_IsStateSave;
    p->super.SaveState = &mmEmuMapper043_SaveState;
    p->super.LoadState = &mmEmuMapper043_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper043_Destroy(struct mmEmuMapper043* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0xFF;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper043_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->irq_enable = 0xFF;
    p->irq_counter = 0;

    mmEmuMmu_SetPROM8KBank(mmu, 3, 2);
    mmEmuMmu_SetPROM32KBankArray(mmu, 1, 0, 4, 9);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU mmByte_t mmEmuMapper043_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (0x5000 <= addr && addr < 0x6000)
    {
        mmByte_t* pPtr = mmEmuRom_GetPROM(&nes->rom);
        return pPtr[0x2000 * 8 + 0x1000 + (addr - 0x5000)];
    }

    return (mmByte_t)(addr >> 8);
}
MM_EXPORT_EMU void mmEmuMapper043_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xF0FF) == 0x4022)
    {
        switch (data & 0x07)
        {
        case 0x00:
        case 0x02:
        case 0x03:
        case 0x04:
            mmEmuMmu_SetPROM8KBank(mmu, 6, 4);
            break;
        case 0x01:
            mmEmuMmu_SetPROM8KBank(mmu, 6, 3);
            break;
        case 0x05:
            mmEmuMmu_SetPROM8KBank(mmu, 6, 7);
            break;
        case 0x06:
            mmEmuMmu_SetPROM8KBank(mmu, 6, 5);
            break;
        case 0x07:
            mmEmuMmu_SetPROM8KBank(mmu, 6, 6);
            break;
        default:
            break;
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper043_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    if ((addr & 0xF0FF) == 0x4022)
    {
        mmEmuMapper043_ExWrite(super, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper043_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (addr == 0x8122)
    {
        if (data & 0x03)
        {
            p->irq_enable = 1;
        }
        else
        {
            p->irq_counter = 0;
            p->irq_enable = 0;
        }
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
    }
}

MM_EXPORT_EMU void mmEmuMapper043_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)(super);

    struct mmEmuNes* nes = p->super.nes;

    mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);

    if (p->irq_enable)
    {
        p->irq_counter += 341;
        if (p->irq_counter >= 12288)
        {
            p->irq_counter = 0;
            //mm_emu_cpu_IRQ(&nes->cpu);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper043_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper043_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)(super);

    buffer[0] = p->irq_enable;
    *(mmInt_t*)&buffer[1] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper043_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = *(mmInt_t*)&buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper043_Produce(void)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)mmMalloc(sizeof(struct mmEmuMapper043));
    mmEmuMapper043_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper043_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper043* p = (struct mmEmuMapper043*)(m);
    mmEmuMapper043_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER043 =
{
    &__static_mmEmuMapper043_Produce,
    &__static_mmEmuMapper043_Recycle,
};
