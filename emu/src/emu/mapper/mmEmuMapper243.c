/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper243.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper243_Init(struct mmEmuMapper243* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);

    //
    p->super.Reset = &mmEmuMapper243_Reset;
    //p->super.Write = &mmEmuMapper243_Write;
    //p->super.Read = &mmEmuMapper243_Read;
    //p->super.ReadLow = &mmEmuMapper243_ReadLow;
    p->super.WriteLow = &mmEmuMapper243_WriteLow;
    //p->super.ExRead = &mmEmuMapper243_ExRead;
    //p->super.ExWrite = &mmEmuMapper243_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper243_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper243_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper243_HSync;
    //p->super.VSync = &mmEmuMapper243_VSync;
    //p->super.Clock = &mmEmuMapper243_Clock;
    //p->super.PPULatch = &mmEmuMapper243_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper243_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper243_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper243_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper243_IsStateSave;
    p->super.SaveState = &mmEmuMapper243_SaveState;
    p->super.LoadState = &mmEmuMapper243_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper243_Destroy(struct mmEmuMapper243* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);
}

MM_EXPORT_EMU void mmEmuMapper243_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper243* p = (struct mmEmuMapper243*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    if (mmu->VROM_8K_SIZE > 4)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 4);
    }
    else if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);

    p->reg[0] = 0;
    p->reg[1] = 0;
    p->reg[2] = 3;
    p->reg[3] = 0;
}
MM_EXPORT_EMU void mmEmuMapper243_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper243* p = (struct mmEmuMapper243*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0x4101) == 0x4100)
    {
        p->reg[0] = data;
    }
    else if ((addr & 0x4101) == 0x4101)
    {
        switch (p->reg[0] & 0x07)
        {
        case 0:
            p->reg[1] = 0;
            p->reg[2] = 3;
            break;
        case 4:
            p->reg[2] = (p->reg[2] & 0x06) | (data & 0x01);
            break;
        case 5:
            p->reg[1] = data & 0x01;
            break;
        case 6:
            p->reg[2] = (p->reg[2] & 0x01) | ((data & 0x03) << 1);
            break;
        case 7:
            p->reg[3] = data & 0x01;
            break;
        default:
            break;
        }

        mmEmuMmu_SetPROM32KBank(mmu, p->reg[1]);
        mmEmuMmu_SetVROM8KBankArray(
            mmu,
            p->reg[2] * 8 + 0, p->reg[2] * 8 + 1, p->reg[2] * 8 + 2, p->reg[2] * 8 + 3,
            p->reg[2] * 8 + 4, p->reg[2] * 8 + 5, p->reg[2] * 8 + 6, p->reg[2] * 8 + 7);

        if (p->reg[3])
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper243_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper243_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper243* p = (struct mmEmuMapper243*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->reg[3];
}
MM_EXPORT_EMU void mmEmuMapper243_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper243* p = (struct mmEmuMapper243*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->reg[3] = buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper243_Produce(void)
{
    struct mmEmuMapper243* p = (struct mmEmuMapper243*)mmMalloc(sizeof(struct mmEmuMapper243));
    mmEmuMapper243_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper243_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper243* p = (struct mmEmuMapper243*)(m);
    mmEmuMapper243_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER243 =
{
    &__static_mmEmuMapper243_Produce,
    &__static_mmEmuMapper243_Recycle,
};
