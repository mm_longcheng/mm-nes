/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuAssets_h__
#define __mmEmuAssets_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "dish/mmFileContext.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuNes;

// RomFile = Rom.nes
// RomPath = RomDirectory/RomFile
//
//----------------------------------------------------------------------------------------------------
// file name                                   | write/read | model
//----------------------------------------------------------------------------------------------------
// RomDirectory/DISKSYS.ROM                    |      /r    | DISKSYS.ROM                      
// RomDirectory/Rom.nes                        |      /r    | rom                      
// RomDirectory/Rom.ips                        |      /r    | rom pitch                
// RomDirectory/Rom.vtd                        |      /r    | apu internal ToneTable
//----------------------------------------------------------------------------------------------------
// WritablePath/ips/RomName.ips                |      /r    | rom pitch
// WritablePath/tone/RomName.vtd               |      /r    | apu internal ToneTable
// WritablePath/genie/RomName.gen              |      /r    | Genie file
//----------------------------------------------------------------------------------------------------
// WritablePath/snapshot/RomName_time_idx.png  | w    /     | snapshot file RomName_19920126_091314_520.png
//----------------------------------------------------------------------------------------------------
// WritablePath/cheatcode/RomName.vct          | w    /r    | cheatcode file
// WritablePath/turbo/RomName.vtf              | w    /r    | Turbo file
// WritablePath/movie/RomName.vmv              | w    /r    | movie file
// WritablePath/state/RomName.sta              | w    /r    | state file
// WritablePath/tape/RomName.vtp               | w    /r    | tape file
// WritablePath/save/RomName.sav               | w    /r    | save SRAM file
// WritablePath/disk/RomName.dsv               | w    /r    | disk file
// WritablePath/network/RomName.stn            | w    /r    | network state file
//----------------------------------------------------------------------------------------------------
struct mmEmuAssets
{
    struct mmEmuNes* nes;

    struct mmFileContext hFileContext;

    struct mmString szRomPath;
    struct mmString szRomName;
    struct mmString szWritablePath;
    //
    struct mmString szRomDirectory;
    struct mmString szRom;
    struct mmString szRomSuffix;
    //
    struct mmString szDISKSYSFile;
    struct mmString szRomFile;
    struct mmString szIpsFile;
    struct mmString szToneFile;

    struct mmString szGenieFile;
};

MM_EXPORT_EMU void mmEmuAssets_Init(struct mmEmuAssets* p);
MM_EXPORT_EMU void mmEmuAssets_Destroy(struct mmEmuAssets* p);

MM_EXPORT_EMU void mmEmuAssets_SetParent(struct mmEmuAssets* p, struct mmEmuNes* parent);

// will auto update the szRomName to be szRom.
MM_EXPORT_EMU void mmEmuAssets_SetRomPath(struct mmEmuAssets* p, const char* szRomPath);
MM_EXPORT_EMU void mmEmuAssets_SetRomName(struct mmEmuAssets* p, const char* szRomName);
MM_EXPORT_EMU void mmEmuAssets_SetWritablePath(struct mmEmuAssets* p, const char* szWritablePath);
MM_EXPORT_EMU void mmEmuAssets_SetResourceRoot(struct mmEmuAssets* p, mmUInt32_t type_assets, const char* root_path, const char* root_base);

MM_EXPORT_EMU void mmEmuAssets_GetGenieFile(struct mmEmuAssets* p, struct mmString* szFileName);

MM_EXPORT_EMU void mmEmuAssets_GetSnapshotFile(struct mmEmuAssets* p, struct mmString* szFileName, mmBool_t png, struct timeval* tv);

MM_EXPORT_EMU void mmEmuAssets_GetCheatCodeFile(struct mmEmuAssets* p, struct mmString* szFileName, const char* vct);
MM_EXPORT_EMU void mmEmuAssets_GetTurboFile(struct mmEmuAssets* p, struct mmString* szFileName);
MM_EXPORT_EMU void mmEmuAssets_GetMovieFile(struct mmEmuAssets* p, struct mmString* szFileName, const char* vmv);
MM_EXPORT_EMU void mmEmuAssets_GetStateFile(struct mmEmuAssets* p, struct mmString* szFileName, const char* stn);
MM_EXPORT_EMU void mmEmuAssets_GetTapeFile(struct mmEmuAssets* p, struct mmString* szFileName);
MM_EXPORT_EMU void mmEmuAssets_GetSaveFile(struct mmEmuAssets* p, struct mmString* szFileName);
MM_EXPORT_EMU void mmEmuAssets_GetDiskFile(struct mmEmuAssets* p, struct mmString* szFileName);
MM_EXPORT_EMU void mmEmuAssets_GetNetworkFile(struct mmEmuAssets* p, struct mmString* szFileName);
// point to Directory.
MM_EXPORT_EMU void mmEmuAssets_AddResourceFolder(struct mmEmuAssets* p, const char* name, const char* path, const char* base);
MM_EXPORT_EMU void mmEmuAssets_RmvResourceFolder(struct mmEmuAssets* p, const char* name);
// point to zip file.
MM_EXPORT_EMU void mmEmuAssets_AddResourceSource(struct mmEmuAssets* p, const char* name, const char* path, const char* base);
MM_EXPORT_EMU void mmEmuAssets_RmvResourceSource(struct mmEmuAssets* p, const char* name);

MM_EXPORT_EMU void mmEmuAssets_AcquireFileByteBuffer(struct mmEmuAssets* p, const char* path_name, struct mmByteBuffer* byte_buffer);
MM_EXPORT_EMU void mmEmuAssets_ReleaseFileByteBuffer(struct mmEmuAssets* p, struct mmByteBuffer* byte_buffer);

MM_EXPORT_EMU void mmEmuAssets_MakeDirectory(struct mmEmuAssets* p);

#include "core/mmSuffix.h"

#endif//__mmEmuAssets_h__
