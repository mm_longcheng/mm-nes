/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper240.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper240_Init(struct mmEmuMapper240* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper240_Reset;
    //p->super.Write = &mmEmuMapper240_Write;
    //p->super.Read = &mmEmuMapper240_Read;
    //p->super.ReadLow = &mmEmuMapper240_ReadLow;
    p->super.WriteLow = &mmEmuMapper240_WriteLow;
    //p->super.ExRead = &mmEmuMapper240_ExRead;
    //p->super.ExWrite = &mmEmuMapper240_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper240_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper240_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper240_HSync;
    //p->super.VSync = &mmEmuMapper240_VSync;
    //p->super.Clock = &mmEmuMapper240_Clock;
    //p->super.PPULatch = &mmEmuMapper240_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper240_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper240_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper240_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper240_IsStateSave;
    //p->super.SaveState = &mmEmuMapper240_SaveState;
    //p->super.LoadState = &mmEmuMapper240_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper240_Destroy(struct mmEmuMapper240* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper240_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper240* p = (struct mmEmuMapper240*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper240_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper240* p = (struct mmEmuMapper240*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x4020 && addr < 0x6000)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (data & 0xF0) >> 4);
        mmEmuMmu_SetVROM8KBank(mmu, data & 0xF);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper240_Produce(void)
{
    struct mmEmuMapper240* p = (struct mmEmuMapper240*)mmMalloc(sizeof(struct mmEmuMapper240));
    mmEmuMapper240_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper240_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper240* p = (struct mmEmuMapper240*)(m);
    mmEmuMapper240_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER240 =
{
    &__static_mmEmuMapper240_Produce,
    &__static_mmEmuMapper240_Recycle,
};
