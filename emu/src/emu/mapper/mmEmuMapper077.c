/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper077.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper077_Init(struct mmEmuMapper077* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper077_Reset;
    p->super.Write = &mmEmuMapper077_Write;
    //p->super.Read = &mmEmuMapper077_Read;
    //p->super.ReadLow = &mmEmuMapper077_ReadLow;
    //p->super.WriteLow = &mmEmuMapper077_WriteLow;
    //p->super.ExRead = &mmEmuMapper077_ExRead;
    //p->super.ExWrite = &mmEmuMapper077_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper077_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper077_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper077_HSync;
    //p->super.VSync = &mmEmuMapper077_VSync;
    //p->super.Clock = &mmEmuMapper077_Clock;
    //p->super.PPULatch = &mmEmuMapper077_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper077_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper077_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper077_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper077_IsStateSave;
    //p->super.SaveState = &mmEmuMapper077_SaveState;
    //p->super.LoadState = &mmEmuMapper077_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper077_Destroy(struct mmEmuMapper077* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper077_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper077* p = (struct mmEmuMapper077*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);

    mmEmuMmu_SetVROM2KBank(mmu, 0, 0);
    mmEmuMmu_SetCRAM2KBank(mmu, 2, 1);
    mmEmuMmu_SetCRAM2KBank(mmu, 4, 2);
    mmEmuMmu_SetCRAM2KBank(mmu, 6, 3);
}
MM_EXPORT_EMU void mmEmuMapper077_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper077* p = (struct mmEmuMapper077*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, data & 0x07);

    mmEmuMmu_SetVROM2KBank(mmu, 0, (data & 0xF0) >> 4);
}

static struct mmEmuMapper* __static_mmEmuMapper077_Produce(void)
{
    struct mmEmuMapper077* p = (struct mmEmuMapper077*)mmMalloc(sizeof(struct mmEmuMapper077));
    mmEmuMapper077_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper077_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper077* p = (struct mmEmuMapper077*)(m);
    mmEmuMapper077_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER077 =
{
    &__static_mmEmuMapper077_Produce,
    &__static_mmEmuMapper077_Recycle,
};
