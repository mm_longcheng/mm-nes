/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper188.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper188_Init(struct mmEmuMapper188* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper188_Reset;
    p->super.Write = &mmEmuMapper188_Write;
    //p->super.Read = &mmEmuMapper188_Read;
    //p->super.ReadLow = &mmEmuMapper188_ReadLow;
    //p->super.WriteLow = &mmEmuMapper188_WriteLow;
    //p->super.ExRead = &mmEmuMapper188_ExRead;
    //p->super.ExWrite = &mmEmuMapper188_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper188_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper188_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper188_HSync;
    //p->super.VSync = &mmEmuMapper188_VSync;
    //p->super.Clock = &mmEmuMapper188_Clock;
    //p->super.PPULatch = &mmEmuMapper188_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper188_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper188_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper188_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper188_IsStateSave;
    //p->super.SaveState = &mmEmuMapper188_SaveState;
    //p->super.LoadState = &mmEmuMapper188_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper188_Destroy(struct mmEmuMapper188* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper188_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper188* p = (struct mmEmuMapper188*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->PROM_08K_SIZE > 16)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 14, 15);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper188_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper188* p = (struct mmEmuMapper188*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (data)
    {
        if (data & 0x10)
        {
            data &= 0x07;
            mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        }
        else
        {
            mmEmuMmu_SetPROM16KBank(mmu, 4, data + 8);
        }
    }
    else
    {
        if (mmu->PROM_08K_SIZE == 0x10)
        {
            mmEmuMmu_SetPROM16KBank(mmu, 4, 7);
        }
        else
        {
            mmEmuMmu_SetPROM16KBank(mmu, 4, 8);
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper188_Produce(void)
{
    struct mmEmuMapper188* p = (struct mmEmuMapper188*)mmMalloc(sizeof(struct mmEmuMapper188));
    mmEmuMapper188_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper188_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper188* p = (struct mmEmuMapper188*)(m);
    mmEmuMapper188_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER188 =
{
    &__static_mmEmuMapper188_Produce,
    &__static_mmEmuMapper188_Recycle,
};
