/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadHyperShot.h"

#include "emu/mmEmuNes.h"
#include "emu/mmEmuPad.h"

MM_EXPORT_EMU void mmEmuPadHyperShot_Init(struct mmEmuPadHyperShot* p)
{
    mmEmuExpad_Init(&p->super);

    p->shotbits = 0;

    //
    p->super.Reset = &mmEmuPadHyperShot_Reset;
    p->super.Strobe = &mmEmuPadHyperShot_Strobe;
    //p->super.Read4016 = &mmEmuPadHyperShot_Read4016;
    p->super.Read4017 = &mmEmuPadHyperShot_Read4017;
    //p->super.Write4016 = &mmEmuPadHyperShot_Write4016;
    //p->super.Write4017 = &mmEmuPadHyperShot_Write4017;
    //p->super.Sync = &mmEmuPadHyperShot_Sync;
    //p->super.SetSyncData = &mmEmuPadHyperShot_SetSyncData;
    //p->super.GetSyncData = &mmEmuPadHyperShot_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadHyperShot_Destroy(struct mmEmuPadHyperShot* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->shotbits = 0;
}

MM_EXPORT_EMU void mmEmuPadHyperShot_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadHyperShot* p = (struct mmEmuPadHyperShot*)(super);

    p->shotbits = 0;
}

MM_EXPORT_EMU void mmEmuPadHyperShot_Strobe(struct mmEmuExpad* super)
{
    struct mmEmuPadHyperShot* p = (struct mmEmuPadHyperShot*)(super);

    struct mmEmuPad* pad = &p->super.nes->pad;

    p->shotbits = 0;
    p->shotbits |= (pad->pad1bit & 0x03) << 1;
    p->shotbits |= (pad->pad2bit & 0x03) << 3;
}
MM_EXPORT_EMU mmByte_t mmEmuPadHyperShot_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadHyperShot* p = (struct mmEmuPadHyperShot*)(super);

    return p->shotbits;
}

static struct mmEmuExpad* __static_mmEmuPadHyperShot_Produce(void)
{
    struct mmEmuPadHyperShot* p = (struct mmEmuPadHyperShot*)mmMalloc(sizeof(struct mmEmuPadHyperShot));
    mmEmuPadHyperShot_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadHyperShot_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadHyperShot* p = (struct mmEmuPadHyperShot*)(m);
    mmEmuPadHyperShot_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_HYPERSHOT =
{
    &__static_mmEmuPadHyperShot_Produce,
    &__static_mmEmuPadHyperShot_Recycle,
};
