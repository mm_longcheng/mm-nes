/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmu6502_h__
#define __mmEmu6502_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

// Status register tag index
#define MM_EMU_CPU_INDEX_C 0
#define MM_EMU_CPU_INDEX_Z 1
#define MM_EMU_CPU_INDEX_I 2
#define MM_EMU_CPU_INDEX_D 3
#define MM_EMU_CPU_INDEX_B 4
#define MM_EMU_CPU_INDEX_R 5
#define MM_EMU_CPU_INDEX_V 6
#define MM_EMU_CPU_INDEX_N 7

// 6502 status flags
#define MM_EMU_CPU_C_FLAG 0x01 // 1: Carry
#define MM_EMU_CPU_Z_FLAG 0x02 // 1: Zero
#define MM_EMU_CPU_I_FLAG 0x04 // 1: Irq disabled
#define MM_EMU_CPU_D_FLAG 0x08 // 1: Decimal mode flag (NES unused)
#define MM_EMU_CPU_B_FLAG 0x10 // 1: Break
#define MM_EMU_CPU_R_FLAG 0x20 // 1: Reserved (Always 1)
#define MM_EMU_CPU_V_FLAG 0x40 // 1: Overflow
#define MM_EMU_CPU_N_FLAG 0x80 // 1: Negative

// Vector
#define MM_EMU_CPU_NMI_VECTOR   0xFFFA
#define MM_EMU_CPU_RES_VECTOR   0xFFFC
#define MM_EMU_CPU_IRQ_VECTOR   0xFFFE


// Interrupt
#define MM_EMU_CPU_NMI_FLAG 0x01
#define MM_EMU_CPU_IRQ_FLAG 0x02

#define MM_EMU_CPU_IRQ_FRAMEIRQ 0x04
#define MM_EMU_CPU_IRQ_DPCM     0x08
#define MM_EMU_CPU_IRQ_MAPPER   0x10
#define MM_EMU_CPU_IRQ_MAPPER2  0x20
#define MM_EMU_CPU_IRQ_TRIGGER  0x40 // one shot(old IRQ())
#define MM_EMU_CPU_IRQ_TRIGGER2 0x80 // one shot(old IRQ_NotPending())

#define MM_EMU_CPU_IRQ_MASK (~(MM_EMU_CPU_NMI_FLAG | MM_EMU_CPU_IRQ_FLAG))

// 6502 context
struct mmEmuX6502State
{
    /**
     * Program Counter
     *  The 2 - byte program counter PC supports 65536 direct(unbanked) memory locations, however
     *  not all values are sent to the cartridge.It can be accessed either by allowing CPU's
     *  internal fetch logic increment the address bus, an interrupt (NMI, Reset, IRQ/BRQ),
     *  and using the RTS/JMP/JSR/Branch instructions.
     */
    mmWord_t   PC;

    /**
     * Accumulator
     *  A is byte - wide and along with the arithmetic logic unit(ALU), supports using the status
     *  register for carrying, overflow detection, and so on.
     */
    mmByte_t   A;

    /**
     * Status Register
     *  P has 6 bits used by the ALU but is byte - wide.PHP, PLP, arithmetic, testing, and branch
     *  instructions can access this register.
     */
    mmByte_t   P;

    /*
     * Indexes
     *  X and Y are byte - wide and used for several addressing modes.They can be used as loop counters
     *  easily, using INC / DEC and branch instructions.Not being the accumulator, they have limited
     *  addressing modes themselves when loading and saving.
     */
    mmByte_t   X;
    mmByte_t   Y;

    /*
     * Stack Pointer
     *  S is byte - wide and can be accessed using interrupts, pulls, pushes, and transfers.
     */
    mmByte_t   S;

    /*
     * int pending
     */
    mmByte_t   pending;
};
MM_EXPORT_EMU void mmEmuX6502State_Init(struct mmEmuX6502State* p);
MM_EXPORT_EMU void mmEmuX6502State_Destroy(struct mmEmuX6502State* p);

struct mmEmuX6502;

// Addressing mode
enum mmX6502AddressingMode_t
{
    MM_EMU_AM_UNK = 0,     // 
    MM_EMU_AM_ACC,         // : Op Accumulator
    MM_EMU_AM_IMP,         // : Implied    Addressing
    MM_EMU_AM_IMM,         // : Immediate  Addressing
    MM_EMU_AM_ABS,         // : Absolute   Addressing
    MM_EMU_AM_ABX,         // : Absolute X Addressing
    MM_EMU_AM_ABY,         // : Absolute Y Addressing
    MM_EMU_AM_ZPG,         // : Zero-Page  Addressing
    MM_EMU_AM_ZPX,         // : Zero-PageX Addressing
    MM_EMU_AM_ZPY,         // : Zero-PageY Addressing
    MM_EMU_AM_INX,         // : Pre-indexed Indirect Addressing
    MM_EMU_AM_INY,         // : Post-indexed Indirect Addressing
    MM_EMU_AM_IND,         // : Indirect   Addressing
    MM_EMU_AM_REL,         // : Relative   Addressing
};

struct mmX6502Code_t
{
    union
    {
        // Save data with 32 bits
        mmUInt32_t    data;
        // 4 8-bit data
        struct
        {
            // Opcode 
            mmUInt8_t op;
            // Address code 1
            mmUInt8_t a1;
            // Address code 2
            mmUInt8_t a2;
            // Display control
            mmUInt8_t ctrl;
        };
    };
};

// The disassembly buf length
enum { MM_DISASSEMBLY_OPCODE_BUF_LEN = 32 };

// Disassembly
void mmX6502_Disassembly(struct mmX6502Code_t* code, char buf[MM_DISASSEMBLY_OPCODE_BUF_LEN]);

// Specified address disassembly
enum { MM_DISASSEMBLY_ADDRESS_BUF_LEN = 64 };

void mmX6502_DisassemblyAddress(struct mmEmuX6502* p, mmWord_t address, char buf[MM_DISASSEMBLY_ADDRESS_BUF_LEN]);

struct mmEmuX6502Callback
{
    void(*BeforeExecute)(void* cpu);

    // mmByte_t(*OpcodeRdByte)(void* cpu, mmWord_t addr);

    mmByte_t(*MemoryRdByte)(void* cpu, mmWord_t addr);
    void(*MemoryWrByte)(void* cpu, mmWord_t addr, mmByte_t data);

    void(*StackPush)(void* cpu, mmByte_t data);
    mmByte_t(*StackPop)(void* cpu);
};
MM_EXPORT_EMU void mmEmuX6502Callback_Init(struct mmEmuX6502Callback* p);
MM_EXPORT_EMU void mmEmuX6502Callback_Destroy(struct mmEmuX6502Callback* p);

struct mmEmuX6502
{
    struct mmEmuX6502State R;
    struct mmEmuX6502Callback callback;
    
    void* cpu;
    // PTR for quick access.
    mmByte_t** MEM;

    mmUInt8_t  illegal_opcode;
    mmByte_t   opcode;
    mmUInt32_t cycles;
};
MM_EXPORT_EMU void mmEmuX6502_Init(struct mmEmuX6502* p);
MM_EXPORT_EMU void mmEmuX6502_Destroy(struct mmEmuX6502* p);

MM_EXPORT_EMU void mmEmuX6502_SetCallback(struct mmEmuX6502* p, struct mmEmuX6502Callback* m);
MM_EXPORT_EMU void mmEmuX6502_SetCPU(struct mmEmuX6502* p, void* cpu);
MM_EXPORT_EMU void mmEmuX6502_SetMEM(struct mmEmuX6502* p, mmByte_t** MEM);

MM_EXPORT_EMU void mmEmuX6502_EXEC(struct mmEmuX6502* p);

MM_EXPORT_EMU void mmEmuX6502_HardwareReset(struct mmEmuX6502* p);
MM_EXPORT_EMU void mmEmuX6502_SoftwareReset(struct mmEmuX6502* p);

MM_EXPORT_EMU void mmEmuX6502_NMI(struct mmEmuX6502* p, mmUInt32_t* cycle);
MM_EXPORT_EMU void mmEmuX6502_IRQ(struct mmEmuX6502* p, mmUInt32_t* cycle);

MM_EXPORT_EMU void mmEmuX6502_LoggerEXEC(struct mmEmuX6502* p, mmUInt32_t* line, char buffer[128]);

#include "core/mmSuffix.h"

#endif//__mmEmu6502_h__
