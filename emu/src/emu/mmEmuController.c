/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuController.h"
#include "mmEmuNes.h"

#include "core/mmKeyCode.h"

MM_EXPORT_EMU void mmEmuControllerCursor_Init(struct mmEmuControllerCursor* p)
{
    p->x = 0;
    p->y = 0;
    p->button_mask = 0;
}
MM_EXPORT_EMU void mmEmuControllerCursor_Destroy(struct mmEmuControllerCursor* p)
{
    p->x = 0;
    p->y = 0;
    p->button_mask = 0;
}
MM_EXPORT_EMU void mmEmuControllerCursor_Reset(struct mmEmuControllerCursor* p)
{
    p->x = 0;
    p->y = 0;
    p->button_mask = 0;
}

MM_EXPORT_EMU void mmEmuController_Init(struct mmEmuController* p)
{
    p->nes = NULL;

    mmBitset_Init(&p->keyboard);
    mmEmuControllerCursor_Init(&p->cursor);

    // key scan codes common is MM_EMU_CONTROLLER_KEYBOARD_NUMBER.
    mmBitset_Resize(&p->keyboard, MM_EMU_CONTROLLER_KEYBOARD_NUMBER);
}
MM_EXPORT_EMU void mmEmuController_Destroy(struct mmEmuController* p)
{
    p->nes = NULL;

    mmBitset_Destroy(&p->keyboard);
    mmEmuControllerCursor_Destroy(&p->cursor);
}

MM_EXPORT_EMU void mmEmuController_Reset(struct mmEmuController* p)
{
    mmBitset_BitReset(&p->keyboard);
    mmEmuControllerCursor_Reset(&p->cursor);
}

MM_EXPORT_EMU void mmEmuController_SetParent(struct mmEmuController* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU void mmEmuController_KeyboardPressed(struct mmEmuController* p, int hId)
{
    mmBitset_Set(&p->keyboard, hId, 1);
}
MM_EXPORT_EMU void mmEmuController_KeyboardRelease(struct mmEmuController* p, int hId)
{
    mmBitset_Set(&p->keyboard, hId, 0);
}

MM_EXPORT_EMU void mmEmuController_MouseBegan(struct mmEmuController* p, mmLong_t x, mmLong_t y, int button_mask)
{
    if (0 <= x && x <= MM_EMU_SCREEN_W && 0 <= y && y <= MM_EMU_SCREEN_H)
    {
        struct mmEmuControllerCursor* pCursor = &p->cursor;

        int button_id_mask = pCursor->button_mask ^ button_mask;

        pCursor->x = x;
        pCursor->y = y;
        pCursor->button_mask |= (button_id_mask);
    }
    else
    {
        x = x > 0 ? x : 0;
        y = y > 0 ? y : 0;
        x = x < MM_EMU_SCREEN_W ? x : MM_EMU_SCREEN_W;
        y = y < MM_EMU_SCREEN_H ? y : MM_EMU_SCREEN_H;

        p->cursor.x = x;
        p->cursor.y = y;
    }
}
MM_EXPORT_EMU void mmEmuController_MouseMoved(struct mmEmuController* p, mmLong_t x, mmLong_t y, int button_mask)
{
    struct mmEmuControllerCursor* pCursor = &p->cursor;

    int button_id_mask = pCursor->button_mask ^ button_mask;

    x = x > 0 ? x : 0;
    y = y > 0 ? y : 0;
    x = x < MM_EMU_SCREEN_W ? x : MM_EMU_SCREEN_W;
    y = y < MM_EMU_SCREEN_H ? y : MM_EMU_SCREEN_H;

    pCursor->x = x;
    pCursor->y = y;
    pCursor->button_mask |= (button_id_mask);
}
MM_EXPORT_EMU void mmEmuController_MouseEnded(struct mmEmuController* p, mmLong_t x, mmLong_t y, int button_mask)
{
    struct mmEmuControllerCursor* pCursor = &p->cursor;

    int button_id_mask = pCursor->button_mask ^ button_mask;

    x = x > 0 ? x : 0;
    y = y > 0 ? y : 0;
    x = x < MM_EMU_SCREEN_W ? x : MM_EMU_SCREEN_W;
    y = y < MM_EMU_SCREEN_H ? y : MM_EMU_SCREEN_H;

    pCursor->x = x;
    pCursor->y = y;
    pCursor->button_mask &= ~(button_id_mask);
}

MM_EXPORT_EMU void mmEmuController_KeyboardReset(struct mmEmuController* p)
{
    mmBitset_BitReset(&p->keyboard);
}
MM_EXPORT_EMU mmBool_t mmEmuController_KeyboardStatus(struct mmEmuController* p, int hId)
{
    return mmBitset_Get(&p->keyboard, hId) == 1;
}

MM_EXPORT_EMU mmBool_t mmEmuController_ButtonCheck(struct mmEmuController* p, mmInt_t nNo, mmInt_t nID)
{
    mmWord_t* button = p->nes->config.controller.nButton[nNo];

    mmWord_t id_l = button[nID + 0x00];
    mmWord_t id_r = button[nID + 0x20];

    return ((id_l && mmEmuController_KeyboardStatus(p, id_l)) || (id_r && mmEmuController_KeyboardStatus(p, id_r)));
}
MM_EXPORT_EMU mmBool_t mmEmuController_ExButtonCheck(struct mmEmuController* p, mmInt_t nNo, mmInt_t nID)
{
    mmWord_t* exbutton = p->nes->config.controller.nExButton[nNo];

    mmWord_t id_l = exbutton[nID + 0x00];
    mmWord_t id_r = exbutton[nID + 0x20];

    return ((id_l && mmEmuController_KeyboardStatus(p, id_l)) || (id_r && mmEmuController_KeyboardStatus(p, id_r)));
}
MM_EXPORT_EMU mmBool_t mmEmuController_NsfButtonCheck(struct mmEmuController* p, mmInt_t nID)
{
    mmWord_t* nsfbutton = p->nes->config.controller.nNsfButton;

    mmWord_t id_l = nsfbutton[nID + 0x00];
    mmWord_t id_r = nsfbutton[nID + 0x20];

    return ((id_l && mmEmuController_KeyboardStatus(p, id_l)) || (id_r && mmEmuController_KeyboardStatus(p, id_r)));
}
MM_EXPORT_EMU mmBool_t mmEmuController_ButtonCheckKeys(struct mmEmuController* p, mmInt_t nID, mmWord_t* pKey)
{
    mmWord_t id_l = pKey[nID + 0x00];
    mmWord_t id_r = pKey[nID + 0x20];

    return ((id_l && mmEmuController_KeyboardStatus(p, id_l)) || (id_r && mmEmuController_KeyboardStatus(p, id_r)));
}
MM_EXPORT_EMU mmBool_t mmEmuController_MouseButtonStatus(struct mmEmuController* p, int button_id)
{
    return p->cursor.button_mask & (1 << button_id);
}

