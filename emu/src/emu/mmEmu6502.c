/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmu6502.h"
#include "core/mmString.h"
#include "core/mmAlloc.h"

// instruction baisc cycle.
static const mmByte_t __baisc_cycle[256] =
{
    /* cycle   0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F */
    /* 0x00 */ 7, 6, 2, 8, 3, 3, 5, 5, 3, 2, 2, 2, 4, 4, 6, 6,
    /* 0x10 */ 2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    /* 0x20 */ 6, 6, 2, 8, 3, 3, 5, 5, 4, 2, 2, 2, 4, 4, 6, 6,
    /* 0x30 */ 2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    /* 0x40 */ 6, 6, 2, 8, 3, 3, 5, 5, 3, 2, 2, 2, 3, 4, 6, 6,
    /* 0x50 */ 2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    /* 0x60 */ 6, 6, 2, 8, 3, 3, 5, 5, 4, 2, 2, 2, 5, 4, 6, 6,
    /* 0x70 */ 2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    /* 0x80 */ 2, 6, 2, 6, 3, 3, 3, 3, 2, 2, 2, 2, 4, 4, 4, 4,
    /* 0x90 */ 2, 6, 2, 6, 4, 4, 4, 4, 2, 5, 2, 5, 5, 5, 5, 5,
    /* 0xA0 */ 2, 6, 2, 6, 3, 3, 3, 3, 2, 2, 2, 2, 4, 4, 4, 4,
    /* 0xB0 */ 2, 5, 2, 5, 4, 4, 4, 4, 2, 4, 2, 4, 4, 4, 4, 4,
    /* 0xC0 */ 2, 6, 2, 8, 3, 3, 5, 5, 2, 2, 2, 2, 4, 4, 6, 6,
    /* 0xD0 */ 2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    /* 0xE0 */ 2, 6, 2, 8, 3, 3, 5, 5, 2, 2, 2, 2, 4, 4, 6, 6,
    /* 0xF0 */ 2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
};

MM_EXPORT_EMU void mmEmuX6502State_Init(struct mmEmuX6502State* p)
{
    p->A = 0x00;
    p->X = 0x00;
    p->Y = 0x00;
    p->S = 0xFD;
    p->P = MM_EMU_CPU_I_FLAG | MM_EMU_CPU_R_FLAG;
    p->PC = 0;

    p->pending = 0;
}
MM_EXPORT_EMU void mmEmuX6502State_Destroy(struct mmEmuX6502State* p)
{
    p->A = 0x00;
    p->X = 0x00;
    p->Y = 0x00;
    p->S = 0xFD;
    p->P = MM_EMU_CPU_I_FLAG | MM_EMU_CPU_R_FLAG;
    p->PC = 0;

    p->pending = 0;
}

static FORCEINLINE void __static_6502_F_SE(struct mmEmuX6502State* R, mmByte_t f)
{
    R->P |= (mmByte_t)f;
}
static FORCEINLINE void __static_6502_F_CL(struct mmEmuX6502State* R, mmByte_t f)
{
    R->P &= ~(mmByte_t)f;
}
static FORCEINLINE void __static_6502_F_IF(struct mmEmuX6502State* R, mmByte_t f, mmWord_t x)
{
    x ? __static_6502_F_SE(R, f) : __static_6502_F_CL(R, f);
}

static FORCEINLINE void __static_stack_push(struct mmEmuX6502* p, mmByte_t a);
static FORCEINLINE mmByte_t __static_stack_pop(struct mmEmuX6502* p);
static FORCEINLINE void __static_check_zsflag(struct mmEmuX6502* p, mmByte_t x);

static FORCEINLINE mmByte_t __static_opcode_rd_byte(struct mmEmuX6502* p, mmWord_t addr);
// static mmInline mmWord_t __static_opcode_rd_word(struct mmEmuX6502* p, mmWord_t addr);
static FORCEINLINE mmByte_t __static_memory_rd_byte(struct mmEmuX6502* p, mmWord_t addr);
static FORCEINLINE mmWord_t __static_memory_rd_word(struct mmEmuX6502* p, mmWord_t addr);
static FORCEINLINE void __static_memory_wr_byte(struct mmEmuX6502* p, mmWord_t addr, mmByte_t data);

/* Addressing mode */

// Addressing mode: Unknown
static mmInline mmWord_t __static_addressing_UNK(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    // Unimplemented/Incorrect Unofficial addressing mode.
    return 0;
}
// Addressing mode: Accumulator
//static mmInline mmWord_t __static_addressing_ACC(struct mmEmuX6502* p, mmUInt32_t* cycle)
//{
//    return 0;
//}
// Addressing mode: Implicit
static mmInline mmWord_t __static_addressing_IMP(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    return 0;
}
// Addressing mode: Immediate
static mmInline mmWord_t __static_addressing_IMM(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmWord_t address = p->R.PC;
    p->R.PC++;
    return address;
}
// Addressing mode: Absolute
static mmInline mmWord_t __static_addressing_ABS(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmByte_t address0 = __static_opcode_rd_byte(p, p->R.PC++);
    mmByte_t address1 = __static_opcode_rd_byte(p, p->R.PC++);
    return (mmWord_t)address0 | (mmWord_t)((mmWord_t)address1 << 8);
}
// Addressing mode: Absolute X
static mmInline mmWord_t __static_addressing_ABX(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmWord_t base = __static_addressing_ABS(p, cycle);
    mmWord_t rvar = base + p->R.X;
    *cycle += ((base ^ rvar) >> 8) & 1;
    return rvar;
}
// Addressing mode: Absolute X (No additional cycle detection)
static mmInline mmWord_t __static_addressing_abx(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmWord_t base = __static_addressing_ABS(p, cycle);
    mmWord_t rvar = base + p->R.X;
    return rvar;
}
// Addressing mode: Absolute Y
static mmInline mmWord_t __static_addressing_ABY(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmWord_t base = __static_addressing_ABS(p, cycle);
    mmWord_t rvar = base + p->R.Y;
    *cycle += ((base ^ rvar) >> 8) & 1;
    return rvar;
}
// Addressing mode: Absolute Y (No additional cycle detection)
static mmInline mmWord_t __static_addressing_aby(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmWord_t base = __static_addressing_ABS(p, cycle);
    mmWord_t rvar = base + p->R.Y;
    return rvar;
}
// Addressing mode: Zero page
static mmInline mmWord_t __static_addressing_ZPG(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    return (mmWord_t)__static_opcode_rd_byte(p, p->R.PC++);
}
// Addressing mode: Zero page X index
static mmInline mmWord_t __static_addressing_ZPX(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmWord_t base = __static_addressing_ZPG(p, cycle);
    mmWord_t index = base + p->R.X;
    return index & (mmWord_t)0x00FF;
}
// Addressing mode: Zero page Y index
static mmInline mmWord_t __static_addressing_ZPY(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmWord_t base = __static_addressing_ZPG(p, cycle);
    mmWord_t index = base + p->R.Y;
    return index & (mmWord_t)0x00FF;
}
// Addressing mode: Indirect X index
static mmInline mmWord_t __static_addressing_INX(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmByte_t base = __static_opcode_rd_byte(p, p->R.PC++) + p->R.X;
    mmByte_t address0 = __static_memory_rd_byte(p, base++);
    mmByte_t address1 = __static_memory_rd_byte(p, base++);
    return (mmWord_t)address0 | (mmWord_t)((mmWord_t)address1 << 8);
}
// Addressing mode: Indirect Y index
static mmInline mmWord_t __static_addressing_INY(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmByte_t base = __static_opcode_rd_byte(p, p->R.PC++);
    mmByte_t address0 = __static_memory_rd_byte(p, base++);
    mmByte_t address1 = __static_memory_rd_byte(p, base++);
    mmWord_t addr = (mmWord_t)address0 | (mmWord_t)((mmWord_t)address1 << 8);
    mmWord_t rvar = addr + p->R.Y;
    *cycle += ((addr ^ rvar) >> 8) & 1;
    return rvar;
}
// Addressing mode: Indirect X index (No additional cycle detection)
static mmInline mmWord_t __static_addressing_iny(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmByte_t base = __static_opcode_rd_byte(p, p->R.PC++);
    mmByte_t address0 = __static_memory_rd_byte(p, base++);
    mmByte_t address1 = __static_memory_rd_byte(p, base++);
    mmWord_t addr = (mmWord_t)address0 | (mmWord_t)((mmWord_t)address1 << 8);
    mmWord_t rvar = addr + p->R.Y;
    return rvar;
}
// Addressing mode: Indirect
static mmInline mmWord_t __static_addressing_IND(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    /**
     * 6502 bugs:
     *   Zeropage index will not leave zeropage when page boundary is crossed.
     */
    mmWord_t base1 = __static_addressing_ABS(p, cycle);
    mmWord_t base2 = ((base1) & (mmWord_t)0xFF00) | ((base1 + 1) & (mmWord_t)0x00FF);
    mmWord_t addr1 = (mmWord_t)__static_memory_rd_byte(p, base1);
    mmWord_t addr2 = (mmWord_t)__static_memory_rd_byte(p, base2);
    return addr1 | addr2 << 8;
}
// Addressing mode: Relative
static mmInline mmWord_t __static_addressing_REL(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmByte_t data = __static_opcode_rd_byte(p, p->R.PC++);
    mmWord_t addr = p->R.PC + (mmSInt8_t)data;
    return addr;
}

/*-- Helper function     ----------------------------------------------*/
// operation : Executive branch jump
static mmInline void __static_branch(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmWord_t saved = p->R.PC;
    p->R.PC = address;
    ++(*cycle);
    *cycle += (address ^ saved) >> 8 & 1;
}
/*-- Unofficial OP codes ----------------------------------------------*/
// operation : UNK: Unknown
static mmInline void __static_operation_UNK(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // Unimplemented/Incorrect Unofficial OP codes.
    // do nothing here.
    p->illegal_opcode = 1;
}
// operation : SHY: [SYA] Store (Y & (ADDR_HI + 1))
static mmInline void __static_operation_SHY(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t addr_high = (mmByte_t)(address >> 8);
    mmByte_t addr_low = (mmByte_t)(address & 0xFF);
    mmByte_t value = p->R.Y & (addr_high + 1);

    // From here: http://forums.nesdev.com/viewtopic.php?f=3&t=3831&start=30
    // Unsure if this is accurate or not
    // "the target address for e.g. SYA becomes 
    //    ((y & (addr_high + 1)) << 8) | addr_low 
    //   instead of the normal 
    //    ((addr_high + 1) << 8) | addr_low"
    mmWord_t addr_mem = ((p->R.Y & (addr_high + 1)) << 8) | addr_low;

    __static_memory_wr_byte(p, addr_mem, value);
}
// operation : SHX: [SXA] Store (X & (ADDR_HI + 1))
static mmInline void __static_operation_SHX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t addr_high = (mmByte_t)(address >> 8);
    mmByte_t addr_low = (mmByte_t)(address & 0xFF);
    mmByte_t value = p->R.X & (addr_high + 1);

    // From here: http://forums.nesdev.com/viewtopic.php?f=3&t=3831&start=30
    // Unsure if this is accurate or not
    // "the target address for e.g. SYA becomes 
    //    ((x & (addr_high + 1)) << 8) | addr_low 
    //   instead of the normal 
    //    ((addr_high + 1) << 8) | addr_low"
    mmWord_t addr_mem = ((p->R.X & (addr_high + 1)) << 8) | addr_low;

    __static_memory_wr_byte(p, addr_mem, value);
}
// operation : TAS
static mmInline void __static_operation_TAS(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // "AND X register with accumulator and store result in stack
    // pointer, then AND stack pointer with the high byte of the
    // target address of the argument + 1. Store result in memory."
    p->R.S = p->R.X & p->R.A;
    __static_memory_wr_byte(p, address, p->R.S & ((address >> 8) + 1));
}
// operation : AHX: [AXA]
static mmInline void __static_operation_AHX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // "This opcode stores the result of A AND X AND the high byte of the target address of the operand +1 in memory."  
    // This may not be the actual behavior, but the read/write operations are needed for proper cycle counting
    mmByte_t value = ((address >> 8) + 1) & p->R.A & p->R.X;

    __static_memory_wr_byte(p, address, value);
}
// operation : XAA
static mmInline void __static_operation_XAA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // XAA Unimplemented/Incorrect Unofficial OP codes.
    // do nothing here.
    // XAA is not a illegal opcode.
}
// operation : LAS
static mmInline void __static_operation_LAS(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // "AND memory with stack pointer, transfer result to accumulator, X register and stack pointer."
    mmByte_t value = __static_memory_rd_byte(p, address);
    p->R.A = value & p->R.S;
    p->R.X = p->R.A;
    p->R.S = p->R.A;
}
/*----------------------------------------------------------------*/
// operation : SRE: Shift Right then "Exclusive-Or" - LSR + EOR
static mmInline void __static_operation_SRE(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // LSR
    mmByte_t data = __static_memory_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, data & 1);
    data >>= 1;
    __static_memory_wr_byte(p, address, data);
    // EOR
    p->R.A ^= data;
    __static_check_zsflag(p, p->R.A);
}
// operation : SLO - Shift Left then 'Or' - ASL + ORA
static mmInline void __static_operation_SLO(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // ASL
    mmByte_t data = __static_memory_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, data & (mmByte_t)0x80);
    data <<= 1;
    __static_memory_wr_byte(p, address, data);
    // ORA
    p->R.A |= data;
    __static_check_zsflag(p, p->R.A);
}
// operation : RRA: Rotate Right then Add with Carry - ROR + ADC
static mmInline void __static_operation_RRA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmWord_t result16_cf = 0;
    mmByte_t result8_ror = 0;
    mmByte_t src = 0;
    mmWord_t result16 = 0;
    mmByte_t result8 = 0;
    // ROR
    mmWord_t result16_ror = __static_memory_rd_byte(p, address);
    result16_ror |= ((mmWord_t)(p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG)) << (8 - MM_EMU_CPU_INDEX_C);
    result16_cf = result16_ror & 1;
    result16_ror >>= 1;
    result8_ror = (mmByte_t)result16_ror;
    __static_memory_wr_byte(p, address, result8_ror);
    // ADC
    src = result8_ror;
    result16 = (mmWord_t)p->R.A + (mmWord_t)src + result16_cf;
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, result16 >> 8);
    result8 = (mmByte_t)result16;
    __static_6502_F_IF(&p->R, MM_EMU_CPU_V_FLAG, !((p->R.A ^ src) & 0x80) && ((p->R.A ^ result8) & 0x80));
    p->R.A = result8;
    __static_check_zsflag(p, p->R.A);
}
// operation : RLA: Rotate Left then 'And' - ROL + AND
static mmInline void __static_operation_RLA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t result8 = 0;
    // ROL
    mmWord_t result16 = __static_memory_rd_byte(p, address);
    result16 <<= 1;
    result16 |= ((mmWord_t)(p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG)) >> (MM_EMU_CPU_INDEX_C);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, result16 & (mmWord_t)0x100);
    result8 = (mmByte_t)result16;
    __static_memory_wr_byte(p, address, result8);
    // AND
    p->R.A &= result8;
    __static_check_zsflag(p, p->R.A);
}
// operation : ISB: [ISC] Increment memory then Subtract with Carry - INC + SBC
static mmInline void __static_operation_ISB(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t src = 0;
    mmWord_t result16 = 0;
    mmByte_t result8 = 0;
    // INC
    mmByte_t data = __static_memory_rd_byte(p, address);
    ++data;
    __static_memory_wr_byte(p, address, data);
    // SBC
    src = data;
    result16 = (mmWord_t)p->R.A - (mmWord_t)src - ((p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG) ? 0 : 1);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, !(result16 >> 8));
    result8 = (mmByte_t)result16;
    __static_6502_F_IF(&p->R, MM_EMU_CPU_V_FLAG, ((p->R.A ^ src) & 0x80) && ((p->R.A ^ result8) & 0x80));
    p->R.A = result8;
    __static_check_zsflag(p, p->R.A);
}
// operation : ISC: [ISB]
//static mmInline void __static_operation_ISC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
//{
//    mmByte_t src = 0;
//    mmWord_t result16 = 0;
//    mmByte_t result8 = 0;
//    // INC
//    mmByte_t data = __static_memory_rd_byte(p, address);
//    ++data;
//    __static_memory_wr_byte(p, address, data);
//    // SBC
//    src = data;
//    result16 = (mmWord_t)p->R.A - (mmWord_t)src - ((p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG) ? 0 : 1);
//    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, !(result16 >> 8));
//    result8 = (mmByte_t)result16;
//    __static_6502_F_IF(&p->R, MM_EMU_CPU_V_FLAG, ((p->R.A ^ src) & 0x80) && ((p->R.A ^ result8) & 0x80));
//    p->R.A = result8;
//    __static_check_zsflag(p, p->R.A);
//}
// operation : DCP: Decrement memory then Compare with A - DEC + CMP
static mmInline void __static_operation_DCP(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmWord_t result16 = 0;
    // DEC
    mmByte_t data = __static_memory_rd_byte(p, address);
    --data;
    __static_memory_wr_byte(p, address, data);
    // CMP
    result16 = (mmWord_t)p->R.A - (mmWord_t)data;
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, !(result16 & (mmWord_t)0x8000));
    __static_check_zsflag(p, (mmByte_t)result16);
}
// operation : SAX: Store A 'And' X - 
static mmInline void __static_operation_SAX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_memory_wr_byte(p, address, p->R.A & p->R.X);
}
// operation : LAX: Load 'A' then Transfer X - LDA  + TAX
static mmInline void __static_operation_LAX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.A = __static_memory_rd_byte(p, address);
    p->R.X = p->R.A;
    __static_check_zsflag(p, p->R.X);
}
// operation : SBX
//static mmInline void __static_operation_SBX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
//{
//    // Unimplemented/Incorrect Unofficial OP codes.
//    // do nothing here.
//    p->illegal_opcode = 1;
//}
// operation : AXS
static mmInline void __static_operation_AXS(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // AXS Operation address is immediate
    mmWord_t tmp = (p->R.A & p->R.X) - __static_opcode_rd_byte(p, address);
    p->R.X = (mmByte_t)tmp;
    __static_check_zsflag(p, p->R.X);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, (tmp & 0x8000) == 0);
}
// operation : ARR
static mmInline void __static_operation_ARR(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // ARR Instruction is immediate
    p->R.A &= __static_opcode_rd_byte(p, address);
    p->R.A = (p->R.A >> 1) | ((p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG) << 7);
    __static_check_zsflag(p, p->R.A);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, (p->R.A >> 6) & 1);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_V_FLAG, ((p->R.A >> 5) ^ (p->R.A >> 6)) & 1);
}
// operation : AAC: [ANC]
//static mmInline void __static_operation_AAC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
//{
//    // AAC Both instructions are immediate
//    p->R.A &= __static_opcode_rd_byte(p, address);
//    __static_check_zsflag(p, p->R.A);
//    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, (p->R.P & (mmByte_t)MM_EMU_CPU_N_FLAG));
//}
// operation : ANC: [AAC]
static mmInline void __static_operation_ANC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // ANC Both instructions are immediate
    p->R.A &= __static_opcode_rd_byte(p, address);
    __static_check_zsflag(p, p->R.A);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, (p->R.P & (mmByte_t)MM_EMU_CPU_N_FLAG));
}
// operation : ASR: [ALR]
static mmInline void __static_operation_ASR(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // ASR instructions are immediate
    p->R.A &= __static_opcode_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, p->R.A & 1);
    p->R.A >>= 1;
    __static_check_zsflag(p, p->R.A);
}
// operation : ALR: [ASR]
//static mmInline void __static_operation_ALR(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
//{
//    // ALR instructions are immediate
//    p->R.A &= __static_opcode_rd_byte(p, address);
//    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, p->R.A & 1);
//    p->R.A >>= 1;
//    __static_check_zsflag(p, p->R.A);
//}
// operation : RTI: Return from I
static mmInline void __static_operation_RTI(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t pcl = 0;
    mmByte_t pch = 0;
    // P
    p->R.P = __static_stack_pop(p);
    __static_6502_F_SE(&p->R, MM_EMU_CPU_R_FLAG);
    __static_6502_F_CL(&p->R, MM_EMU_CPU_B_FLAG);
    // PC
    pcl = __static_stack_pop(p);
    pch = __static_stack_pop(p);
    p->R.PC = (mmWord_t)pcl | (mmWord_t)pch << 8;
}
// operation : BRK
static mmInline void __static_operation_BRK(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t pcl2 = 0;
    mmByte_t pch2 = 0;
    mmWord_t brk = 0;
    //
    mmWord_t pcp1 = p->R.PC + 1;
    mmByte_t pch = (mmByte_t)((pcp1) >> 8);
    mmByte_t pcl = (mmByte_t)(pcp1 & 0xFF);
    __static_stack_push(p, pch);
    __static_stack_push(p, pcl);
    __static_stack_push(p, p->R.P | (mmByte_t)(MM_EMU_CPU_R_FLAG) | (mmByte_t)(MM_EMU_CPU_B_FLAG));
    __static_6502_F_SE(&p->R, MM_EMU_CPU_I_FLAG);
    brk = (p->R.pending &= MM_EMU_CPU_NMI_FLAG) ? MM_EMU_CPU_NMI_VECTOR : MM_EMU_CPU_IRQ_VECTOR;
    pcl2 = __static_opcode_rd_byte(p, brk + 0);
    pch2 = __static_opcode_rd_byte(p, brk + 1);
    p->R.PC = (mmWord_t)pcl2 | (mmWord_t)pch2 << 8;
}
// operation : NOP: No Operation
static mmInline void __static_operation_NOP(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    // do nothing here.
}
// operation : RTS: Return from Subroutine
static mmInline void __static_operation_RTS(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t pcl = __static_stack_pop(p);
    mmByte_t pch = __static_stack_pop(p);
    p->R.PC = (mmWord_t)pcl | (mmWord_t)pch << 8;
    p->R.PC++;
}
// operation : JSR: Jump to Subroutine
static mmInline void __static_operation_JSR(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    const mmWord_t pc1 = p->R.PC - 1;
    __static_stack_push(p, (mmByte_t)(pc1 >> 8));
    __static_stack_push(p, (mmByte_t)(pc1 & 0xFF));
    p->R.PC = address;
}
// operation : BVC: Branch if Overflow Clear
static mmInline void __static_operation_BVC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    if (!(p->R.P & (mmByte_t)MM_EMU_CPU_V_FLAG))
    {
        __static_branch(p, address, cycle);
    }
}
// operation : BVS: Branch if Overflow Set
static mmInline void __static_operation_BVS(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    if ((p->R.P & (mmByte_t)MM_EMU_CPU_V_FLAG))
    {
        __static_branch(p, address, cycle);
    }
}
// operation : BPL: Branch if Plus
static mmInline void __static_operation_BPL(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    if (!(p->R.P & (mmByte_t)MM_EMU_CPU_N_FLAG))
    {
        __static_branch(p, address, cycle);
    }
}
// operation : BMI: Branch if Minus
static mmInline void __static_operation_BMI(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    if ((p->R.P & (mmByte_t)MM_EMU_CPU_N_FLAG))
    {
        __static_branch(p, address, cycle);
    }
}
// operation : BCC: Branch if Carry Clear
static mmInline void __static_operation_BCC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    if (!(p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG))
    {
        __static_branch(p, address, cycle);
    }
}
// operation : BCS: Branch if Carry Set
static mmInline void __static_operation_BCS(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    if ((p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG))
    {
        __static_branch(p, address, cycle);
    }
}
// operation : BNE: Branch if Not Equal
static mmInline void __static_operation_BNE(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    if (!(p->R.P & (mmByte_t)MM_EMU_CPU_Z_FLAG))
    {
        __static_branch(p, address, cycle);
    }
}
// operation : BEQ: Branch if Equal
static mmInline void __static_operation_BEQ(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    if ((p->R.P & (mmByte_t)MM_EMU_CPU_Z_FLAG))
    {
        __static_branch(p, address, cycle);
    }
}
// operation : JMP
static mmInline void __static_operation_JMP(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.PC = address;
}
// operation : PLP: Pull P
static mmInline void __static_operation_PLP(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.P = __static_stack_pop(p);
    __static_6502_F_SE(&p->R, MM_EMU_CPU_R_FLAG);
    __static_6502_F_CL(&p->R, MM_EMU_CPU_B_FLAG);
}
// operation : PHP: Push P
static mmInline void __static_operation_PHP(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_stack_push(p, p->R.P | (mmByte_t)(MM_EMU_CPU_R_FLAG | MM_EMU_CPU_B_FLAG));
}
// operation : PLA: Pull A
static mmInline void __static_operation_PLA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.A = __static_stack_pop(p);
    __static_check_zsflag(p, p->R.A);
}
// operation : PHA: Push A
static mmInline void __static_operation_PHA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_stack_push(p, p->R.A);
}
// operation : ROR A : Rotate Right for A
static mmInline void __static_operation_RORA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmWord_t result16 = p->R.A;
    result16 |= ((mmWord_t)(p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG)) << (8 - MM_EMU_CPU_INDEX_C);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, result16 & 1);
    result16 >>= 1;
    p->R.A = (mmByte_t)result16;
    __static_check_zsflag(p, p->R.A);
}
// operation : ROR: Rotate Right
static mmInline void __static_operation_ROR(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t result8 = 0;
    //
    mmWord_t result16 = (mmWord_t)__static_memory_rd_byte(p, address);
    result16 |= ((mmWord_t)(p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG)) << (8 - MM_EMU_CPU_INDEX_C);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, result16 & 1);
    result16 >>= 1;
    result8 = (mmByte_t)result16;
    __static_memory_wr_byte(p, address, result8);
    __static_check_zsflag(p, result8);
}
// operation : ROL: Rotate Left
static mmInline void __static_operation_ROL(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t result8 = 0;
    //
    mmWord_t result16 = (mmWord_t)__static_memory_rd_byte(p, address);
    result16 <<= 1;
    result16 |= ((mmWord_t)(p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG)) >> (MM_EMU_CPU_INDEX_C);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, result16 & (mmWord_t)0x100);
    result8 = (mmByte_t)result16;
    __static_memory_wr_byte(p, address, result8);
    __static_check_zsflag(p, result8);
}
// operation : ROL A : Rotate Left for A
static mmInline void __static_operation_ROLA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmWord_t result16 = p->R.A;
    result16 <<= 1;
    result16 |= ((mmWord_t)(p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG)) >> (MM_EMU_CPU_INDEX_C);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, result16 & (mmWord_t)0x100);
    p->R.A = (mmByte_t)result16;
    __static_check_zsflag(p, p->R.A);
}
// operation : LSR: Logical Shift Right
static mmInline void __static_operation_LSR(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t data = __static_memory_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, data & 1);
    data >>= 1;
    __static_memory_wr_byte(p, address, data);
    __static_check_zsflag(p, data);
}
// operation : LSR A : Logical Shift Right for A
static mmInline void __static_operation_LSRA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, p->R.A & 1);
    p->R.A >>= 1;
    __static_check_zsflag(p, p->R.A);
}
// operation : ASL: Arithmetic Shift Left
static mmInline void __static_operation_ASL(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t data = __static_memory_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, data & (mmByte_t)0x80);
    data <<= 1;
    __static_memory_wr_byte(p, address, data);
    __static_check_zsflag(p, data);
}
// operation : ASL A : Arithmetic Shift Left for A
static mmInline void __static_operation_ASLA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, p->R.A & (uint8_t)0x80);
    p->R.A <<= 1;
    __static_check_zsflag(p, p->R.A);
}
// operation : BIT: Bit Test
static mmInline void __static_operation_BIT(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t value = __static_memory_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_V_FLAG, value & (mmByte_t)(1 << 6));
    __static_6502_F_IF(&p->R, MM_EMU_CPU_N_FLAG, value & (mmByte_t)(1 << 7));
    __static_6502_F_IF(&p->R, MM_EMU_CPU_Z_FLAG, !(p->R.A & value));
}
// operation : CPY: Compare memory with Y
static mmInline void __static_operation_CPY(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmWord_t result16 = (mmWord_t)p->R.Y - (mmWord_t)__static_memory_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, !(result16 & (mmWord_t)0x8000));
    __static_check_zsflag(p, (mmByte_t)result16);
}
// operation : CPX
static mmInline void __static_operation_CPX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmWord_t result16 = (mmWord_t)p->R.X - (mmWord_t)__static_memory_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, !(result16 & (mmWord_t)0x8000));
    __static_check_zsflag(p, (mmByte_t)result16);
}
// operation : CMP: Compare memory with A
static mmInline void __static_operation_CMP(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmWord_t result16 = (mmWord_t)p->R.A - (mmWord_t)__static_memory_rd_byte(p, address);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, !(result16 & (mmWord_t)0x8000));
    __static_check_zsflag(p, (mmByte_t)result16);
}
// operation : SEI: Set I
static mmInline void __static_operation_SEI(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_SE(&p->R, MM_EMU_CPU_I_FLAG);
}
// operation : CLI - Clear I
static mmInline void __static_operation_CLI(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_CL(&p->R, MM_EMU_CPU_I_FLAG);
}
// operation : CLV: Clear V
static mmInline void __static_operation_CLV(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_CL(&p->R, MM_EMU_CPU_V_FLAG);
}
// operation : SED: Set D
static mmInline void __static_operation_SED(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_SE(&p->R, MM_EMU_CPU_D_FLAG);
}
// operation : CLD: Clear D
static mmInline void __static_operation_CLD(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_CL(&p->R, MM_EMU_CPU_D_FLAG);
}
// operation : SEC: Set Carry
static mmInline void __static_operation_SEC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_SE(&p->R, MM_EMU_CPU_C_FLAG);
}
// operation : CLC: Clear Carry
static mmInline void __static_operation_CLC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_6502_F_CL(&p->R, MM_EMU_CPU_C_FLAG);
}
// operation : EOR: "Exclusive-Or" memory with A
static mmInline void __static_operation_EOR(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.A ^= __static_memory_rd_byte(p, address);
    __static_check_zsflag(p, p->R.A);
}
// operation : ORA: 'Or' memory with A
static mmInline void __static_operation_ORA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.A |= __static_memory_rd_byte(p, address);
    __static_check_zsflag(p, p->R.A);
}
// operation : AND: 'And' memory with A
static mmInline void __static_operation_AND(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.A &= __static_memory_rd_byte(p, address);
    __static_check_zsflag(p, p->R.A);
}
// operation : DEY: Decrement Y
static mmInline void __static_operation_DEY(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.Y--;
    __static_check_zsflag(p, p->R.Y);
}
// operation : INY:  Increment Y
static mmInline void __static_operation_INY(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.Y++;
    __static_check_zsflag(p, p->R.Y);
}
// operation : DEX: Decrement X
static mmInline void __static_operation_DEX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.X--;
    __static_check_zsflag(p, p->R.X);
}
// operation : INX:  Increment X
static mmInline void __static_operation_INX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.X++;
    __static_check_zsflag(p, p->R.X);
}
// operation : DEC: Decrement memory
static mmInline void __static_operation_DEC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t data = __static_memory_rd_byte(p, address);
    --data;
    __static_memory_wr_byte(p, address, data);
    __static_check_zsflag(p, data);
}
// operation : INC: Increment memory
static mmInline void __static_operation_INC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t data = __static_memory_rd_byte(p, address);
    ++data;
    __static_memory_wr_byte(p, address, data);
    __static_check_zsflag(p, data);
}
// operation : SBC: Subtract with Carry
static mmInline void __static_operation_SBC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t result8 = 0;
    //
    mmByte_t src = __static_memory_rd_byte(p, address);
    mmWord_t result16 = (mmWord_t)p->R.A - (mmWord_t)src - ((p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG) ? 0 : 1);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, !(result16 >> 8));
    result8 = (mmByte_t)result16;
    __static_6502_F_IF(&p->R, MM_EMU_CPU_V_FLAG, ((p->R.A ^ src) & 0x80) && ((p->R.A ^ result8) & 0x80));
    p->R.A = result8;
    __static_check_zsflag(p, p->R.A);
}
// operation : ADC: Add with Carry
static mmInline void __static_operation_ADC(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    mmByte_t result8 = 0;
    //
    mmByte_t src = __static_memory_rd_byte(p, address);
    mmWord_t result16 = (mmWord_t)p->R.A + (mmWord_t)src + ((p->R.P & (mmByte_t)MM_EMU_CPU_C_FLAG) ? 1 : 0);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_C_FLAG, result16 >> 8);
    result8 = (mmByte_t)result16;
    __static_6502_F_IF(&p->R, MM_EMU_CPU_V_FLAG, !((p->R.A ^ src) & 0x80) && ((p->R.A ^ result8) & 0x80));
    p->R.A = result8;
    __static_check_zsflag(p, p->R.A);
}
// operation : TXS: Transfer X to SP
static mmInline void __static_operation_TXS(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.S = p->R.X;
}
// operation : TSX: Transfer SP to X
static mmInline void __static_operation_TSX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.X = p->R.S;
    __static_check_zsflag(p, p->R.X);
}
// operation : TYA: Transfer Y to A
static mmInline void __static_operation_TYA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.A = p->R.Y;
    __static_check_zsflag(p, p->R.A);
}
// operation : TAY: Transfer A to Y
static mmInline void __static_operation_TAY(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.Y = p->R.A;
    __static_check_zsflag(p, p->R.Y);
}
// operation : TXA: Transfer X to A
static mmInline void __static_operation_TXA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.A = p->R.X;
    __static_check_zsflag(p, p->R.A);
}
// operation : TAX: Transfer A to X
static mmInline void __static_operation_TAX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.X = p->R.A;
    __static_check_zsflag(p, p->R.X);
}
// operation : STY: Store 'Y'
static mmInline void __static_operation_STY(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_memory_wr_byte(p, address, p->R.Y);
}
// operation : STX: Store X
static mmInline void __static_operation_STX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_memory_wr_byte(p, address, p->R.X);
}
// operation : STA: Store 'A'
static mmInline void __static_operation_STA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    __static_memory_wr_byte(p, address, p->R.A);
}
// operation : LDY: Load 'Y'
static mmInline void __static_operation_LDY(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.Y = __static_memory_rd_byte(p, address);
    __static_check_zsflag(p, p->R.Y);
}
// operation : LDX: Load X
static mmInline void __static_operation_LDX(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.X = __static_memory_rd_byte(p, address);
    __static_check_zsflag(p, p->R.X);
}
// operation : LDA: Load A
static mmInline void __static_operation_LDA(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle)
{
    p->R.A = __static_memory_rd_byte(p, address);
    __static_check_zsflag(p, p->R.A);
}

typedef mmWord_t(*addressing_func)(struct mmEmuX6502* p, mmUInt32_t* cycle);
typedef void(*operation_func)(struct mmEmuX6502* p, mmWord_t address, mmUInt32_t* cycle);
struct mmEmuInstruction
{
    mmByte_t       opcode;
    addressing_func addressing;
    operation_func  operation;
};
#define I(n, a, o) { 0x##n, __static_addressing_##a, __static_operation_##o, }

/*
 *   http://www.oxyron.de/html/opcodes02.html
 *
 *   Special value
 *
 *      0x8B XAA imm 2
 *      0x93 AHX izy 6
 *      0x9B TAS aby 5
 *      0x9C SHY abx 5
 *      0x9E SHX aby 5
 *      0x9F AHX aby 5
 *      0xAB LAX imm 2
 *
 */

static const struct mmEmuInstruction __instruction[256] =
{
    /*        0               1               2               3               4               5               6               7               8               9               A               B               C               D               E               F            */
    /* 0x00 */I(00,IMP, BRK), I(01,INX, ORA), I(02,IMP, UNK), I(03,INX, SLO), I(04,ZPG, NOP), I(05,ZPG, ORA), I(06,ZPG, ASL), I(07,ZPG, SLO), I(08,IMP, PHP), I(09,IMM, ORA), I(0A,IMP,ASLA), I(0B,IMM, ANC), I(0C,ABS, NOP), I(0D,ABS, ORA), I(0E,ABS, ASL), I(0F,ABS, SLO),
    /* 0x10 */I(10,REL, BPL), I(11,INY, ORA), I(12,UNK, UNK), I(13,iny, SLO), I(14,ZPX, NOP), I(15,ZPX, ORA), I(16,ZPX, ASL), I(17,ZPX, SLO), I(18,IMP, CLC), I(19,ABY, ORA), I(1A,IMP, NOP), I(1B,aby, SLO), I(1C,ABX, NOP), I(1D,ABX, ORA), I(1E,abx, ASL), I(1F,abx, SLO),
    /* 0x20 */I(20,ABS, JSR), I(21,INX, AND), I(22,UNK, UNK), I(23,INX, RLA), I(24,ZPG, BIT), I(25,ZPG, AND), I(26,ZPG, ROL), I(27,ZPG, RLA), I(28,IMP, PLP), I(29,IMM, AND), I(2A,IMP,ROLA), I(2B,IMM, ANC), I(2C,ABS, BIT), I(2D,ABS, AND), I(2E,ABS, ROL), I(2F,ABS, RLA),
    /* 0x30 */I(30,REL, BMI), I(31,INY, AND), I(32,UNK, UNK), I(33,iny, RLA), I(34,ZPX, NOP), I(35,ZPX, AND), I(36,ZPX, ROL), I(37,ZPX, RLA), I(38,IMP, SEC), I(39,ABY, AND), I(3A,IMP, NOP), I(3B,aby, RLA), I(3C,ABX, NOP), I(3D,ABX, AND), I(3E,abx, ROL), I(3F,abx, RLA),
    /* 0x40 */I(40,IMP, RTI), I(41,INX, EOR), I(42,UNK, UNK), I(43,INX, SRE), I(44,ZPG, NOP), I(45,ZPG, EOR), I(46,ZPG, LSR), I(47,ZPG, SRE), I(48,IMP, PHA), I(49,IMM, EOR), I(4A,IMP,LSRA), I(4B,IMM, ASR), I(4C,ABS, JMP), I(4D,ABS, EOR), I(4E,ABS, LSR), I(4F,ABS, SRE),
    /* 0x50 */I(50,REL, BVC), I(51,INY, EOR), I(52,UNK, UNK), I(53,iny, SRE), I(54,ZPX, NOP), I(55,ZPX, EOR), I(56,ZPX, LSR), I(57,ZPX, SRE), I(58,IMP, CLI), I(59,ABY, EOR), I(5A,IMP, NOP), I(5B,aby, SRE), I(5C,ABX, NOP), I(5D,ABX, EOR), I(5E,abx, LSR), I(5F,abx, SRE),
    /* 0x60 */I(60,IMP, RTS), I(61,INX, ADC), I(62,UNK, UNK), I(63,INX, RRA), I(64,ZPG, NOP), I(65,ZPG, ADC), I(66,ZPG, ROR), I(67,ZPG, RRA), I(68,IMP, PLA), I(69,IMM, ADC), I(6A,IMP,RORA), I(6B,IMM, ARR), I(6C,IND, JMP), I(6D,ABS, ADC), I(6E,ABS, ROR), I(6F,ABS, RRA),
    /* 0x70 */I(70,REL, BVS), I(71,INY, ADC), I(72,UNK, UNK), I(73,iny, RRA), I(74,ZPX, NOP), I(75,ZPX, ADC), I(76,ZPX, ROR), I(77,ZPX, RRA), I(78,IMP, SEI), I(79,ABY, ADC), I(7A,IMP, NOP), I(7B,aby, RRA), I(7C,ABX, NOP), I(7D,ABX, ADC), I(7E,abx, ROR), I(7F,abx, RRA),
    /* 0x80 */I(80,IMM, NOP), I(81,INX, STA), I(82,IMM, NOP), I(83,INX, SAX), I(84,ZPG, STY), I(85,ZPG, STA), I(86,ZPG, STX), I(87,ZPG, SAX), I(88,IMP, DEY), I(89,IMM, NOP), I(8A,IMP, TXA), I(8B,IMM, XAA), I(8C,ABS, STY), I(8D,ABS, STA), I(8E,ABS, STX), I(8F,ABS, SAX),
    /* 0x90 */I(90,REL, BCC), I(91,iny, STA), I(92,UNK, UNK), I(93,iny, AHX), I(94,ZPX, STY), I(95,ZPX, STA), I(96,ZPY, STX), I(97,ZPY, SAX), I(98,IMP, TYA), I(99,aby, STA), I(9A,IMP, TXS), I(9B,aby, TAS), I(9C,abx, SHY), I(9D,abx, STA), I(9E,aby, SHX), I(9F,aby, AHX),
    /* 0xA0 */I(A0,IMM, LDY), I(A1,INX, LDA), I(A2,IMM, LDX), I(A3,INX, LAX), I(A4,ZPG, LDY), I(A5,ZPG, LDA), I(A6,ZPG, LDX), I(A7,ZPG, LAX), I(A8,IMP, TAY), I(A9,IMM, LDA), I(AA,IMP, TAX), I(AB,IMM, LAX), I(AC,ABS, LDY), I(AD,ABS, LDA), I(AE,ABS, LDX), I(AF,ABS, LAX),
    /* 0xB0 */I(B0,REL, BCS), I(B1,INY, LDA), I(B2,UNK, UNK), I(B3,INY, LAX), I(B4,ZPX, LDY), I(B5,ZPX, LDA), I(B6,ZPY, LDX), I(B7,ZPY, LAX), I(B8,IMP, CLV), I(B9,ABY, LDA), I(BA,IMP, TSX), I(BB,ABY, LAS), I(BC,ABX, LDY), I(BD,ABX, LDA), I(BE,ABY, LDX), I(BF,ABY, LAX),
    /* 0xC0 */I(C0,IMM, CPY), I(C1,INX, CMP), I(C2,IMM, NOP), I(C3,INX, DCP), I(C4,ZPG, CPY), I(C5,ZPG, CMP), I(C6,ZPG, DEC), I(C7,ZPG, DCP), I(C8,IMP, INY), I(C9,IMM, CMP), I(CA,IMP, DEX), I(CB,IMM, AXS), I(CC,ABS, CPY), I(CD,ABS, CMP), I(CE,ABS, DEC), I(CF,ABS, DCP),
    /* 0xD0 */I(D0,REL, BNE), I(D1,INY, CMP), I(D2,UNK, UNK), I(D3,iny, DCP), I(D4,ZPX, NOP), I(D5,ZPX, CMP), I(D6,ZPX, DEC), I(D7,ZPX, DCP), I(D8,IMP, CLD), I(D9,ABY, CMP), I(DA,IMP, NOP), I(DB,aby, DCP), I(DC,ABX, NOP), I(DD,ABX, CMP), I(DE,abx, DEC), I(DF,abx, DCP),
    /* 0xE0 */I(E0,IMM, CPX), I(E1,INX, SBC), I(E2,IMM, NOP), I(E3,INX, ISB), I(E4,ZPG, CPX), I(E5,ZPG, SBC), I(E6,ZPG, INC), I(E7,ZPG, ISB), I(E8,IMP, INX), I(E9,IMM, SBC), I(EA,IMP, NOP), I(EB,IMM, SBC), I(EC,ABS, CPX), I(ED,ABS, SBC), I(EE,ABS, INC), I(EF,ABS, ISB),
    /* 0xF0 */I(F0,REL, BEQ), I(F1,INY, SBC), I(F2,UNK, UNK), I(F3,iny, ISB), I(F4,ZPX, NOP), I(F5,ZPX, SBC), I(F6,ZPX, INC), I(F7,ZPX, ISB), I(F8,IMP, SED), I(F9,ABY, SBC), I(FA,IMP, NOP), I(FB,aby, ISB), I(FC,ABX, NOP), I(FD,ABX, SBC), I(FE,abx, INC), I(FF,abx, ISB),
};
#undef I

struct mmEmuOpcodeName
{
    // 3 word name
    char           name[3];
    // Addressing mode [low 4 bits] | instruction length [high 4 bits]
    mmUInt8_t     mode_inslen;
};

static const struct mmEmuOpcodeName __opcode_name_data[256] =
{
    // 0
    { { 'B', 'R', 'K' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'O', 'R', 'A' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'S', 'L', 'O' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'O', 'R', 'A' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'A', 'S', 'L' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'S', 'L', 'O' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'P', 'H', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'O', 'R', 'A' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'A', 'S', 'L' }, MM_EMU_AM_ACC | 1 << 4 },
    { { 'A', 'N', 'C' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'O', 'R', 'A' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'A', 'S', 'L' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'S', 'L', 'O' }, MM_EMU_AM_ABS | 3 << 4 },
    // 1
    { { 'B', 'P', 'L' }, MM_EMU_AM_REL | 2 << 4 },
    { { 'O', 'R', 'A' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'S', 'L', 'O' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'O', 'R', 'A' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'A', 'S', 'L' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'S', 'L', 'O' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'C', 'L', 'C' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'O', 'R', 'A' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'S', 'L', 'O' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'O', 'R', 'A' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'A', 'S', 'L' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'S', 'L', 'O' }, MM_EMU_AM_ABX | 3 << 4 },
    // 2
    { { 'J', 'S', 'R' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'A', 'N', 'D' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'R', 'L', 'A' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'B', 'I', 'T' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'A', 'N', 'D' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'R', 'O', 'L' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'R', 'L', 'A' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'P', 'L', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'A', 'N', 'D' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'R', 'O', 'L' }, MM_EMU_AM_ACC | 1 << 4 },
    { { 'A', 'N', 'C' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'B', 'I', 'T' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'A', 'N', 'D' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'R', 'O', 'L' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'R', 'L', 'A' }, MM_EMU_AM_ABS | 3 << 4 },
    // 3
    { { 'B', 'M', 'I' }, MM_EMU_AM_REL | 2 << 4 },
    { { 'A', 'N', 'D' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'R', 'L', 'A' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'A', 'N', 'D' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'R', 'O', 'L' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'R', 'L', 'A' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'S', 'E', 'C' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'A', 'N', 'D' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'R', 'L', 'A' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'A', 'N', 'D' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'R', 'O', 'L' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'R', 'L', 'A' }, MM_EMU_AM_ABX | 3 << 4 },
    // 4
    { { 'R', 'T', 'I' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'E', 'O', 'R' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'S', 'R', 'E' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'E', 'O', 'R' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'L', 'S', 'R' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'S', 'R', 'E' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'P', 'H', 'A' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'E', 'O', 'R' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'L', 'S', 'R' }, MM_EMU_AM_ACC | 1 << 4 },
    { { 'A', 'L', 'R' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'J', 'M', 'P' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'E', 'O', 'R' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'L', 'S', 'R' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'S', 'R', 'E' }, MM_EMU_AM_ABS | 3 << 4 },
    // 5
    { { 'B', 'V', 'C' }, MM_EMU_AM_REL | 2 << 4 },
    { { 'E', 'O', 'R' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'S', 'R', 'E' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'E', 'O', 'R' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'L', 'S', 'R' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'S', 'R', 'E' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'C', 'L', 'I' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'E', 'O', 'R' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'S', 'R', 'E' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'E', 'O', 'R' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'L', 'S', 'R' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'S', 'R', 'E' }, MM_EMU_AM_ABX | 3 << 4 },
    // 6
    { { 'R', 'T', 'S' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'A', 'D', 'C' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'R', 'R', 'A' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'A', 'D', 'C' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'R', 'O', 'R' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'R', 'R', 'A' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'P', 'L', 'A' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'A', 'D', 'C' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'R', 'O', 'R' }, MM_EMU_AM_ACC | 1 << 4 },
    { { 'A', 'R', 'R' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'J', 'M', 'P' }, MM_EMU_AM_IND | 3 << 4 },
    { { 'A', 'D', 'C' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'R', 'O', 'R' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'R', 'R', 'A' }, MM_EMU_AM_ABS | 3 << 4 },
    // 7
    { { 'B', 'V', 'S' }, MM_EMU_AM_REL | 2 << 4 },
    { { 'A', 'D', 'C' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'R', 'R', 'A' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'A', 'D', 'C' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'R', 'O', 'R' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'R', 'R', 'A' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'S', 'E', 'I' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'A', 'D', 'C' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'R', 'R', 'A' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'A', 'D', 'C' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'R', 'O', 'R' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'R', 'R', 'A' }, MM_EMU_AM_ABX | 3 << 4 },
    // 8
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'S', 'T', 'A' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'S', 'A', 'X' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'S', 'T', 'Y' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'S', 'T', 'A' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'S', 'T', 'X' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'S', 'A', 'X' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'D', 'E', 'Y' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'T', 'X', 'A' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'X', 'A', 'A' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'S', 'T', 'Y' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'S', 'T', 'A' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'S', 'T', 'X' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'S', 'A', 'X' }, MM_EMU_AM_ABS | 3 << 4 },
    // 9
    { { 'B', 'C', 'C' }, MM_EMU_AM_REL | 2 << 4 },
    { { 'S', 'T', 'A' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'A', 'H', 'X' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'Y' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'S', 'T', 'A' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'S', 'T', 'X' }, MM_EMU_AM_ZPY | 2 << 4 },
    { { 'S', 'A', 'X' }, MM_EMU_AM_ZPY | 2 << 4 },
    { { 'T', 'Y', 'A' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'S', 'T', 'A' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'T', 'X', 'S' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'T', 'A', 'S' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'S', 'H', 'Y' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'S', 'T', 'A' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'S', 'H', 'X' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'A', 'H', 'X' }, MM_EMU_AM_ABY | 3 << 4 },
    // A
    { { 'L', 'D', 'Y' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'L', 'D', 'A' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'L', 'D', 'X' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'L', 'A', 'X' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'L', 'D', 'Y' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'L', 'D', 'A' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'L', 'D', 'X' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'L', 'A', 'X' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'T', 'A', 'Y' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'L', 'D', 'A' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'T', 'A', 'X' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'L', 'A', 'X' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'L', 'D', 'Y' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'L', 'D', 'A' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'L', 'D', 'X' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'L', 'A', 'X' }, MM_EMU_AM_ABS | 3 << 4 },
    // B
    { { 'B', 'C', 'S' }, MM_EMU_AM_REL | 2 << 4 },
    { { 'L', 'D', 'A' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'L', 'A', 'X' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'L', 'D', 'Y' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'L', 'D', 'A' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'L', 'D', 'X' }, MM_EMU_AM_ZPY | 2 << 4 },
    { { 'L', 'A', 'X' }, MM_EMU_AM_ZPY | 2 << 4 },
    { { 'C', 'L', 'V' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'L', 'D', 'A' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'T', 'S', 'X' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'L', 'A', 'S' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'L', 'D', 'Y' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'L', 'D', 'A' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'L', 'D', 'X' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'L', 'A', 'X' }, MM_EMU_AM_ABY | 3 << 4 },
    // C
    { { 'C', 'P', 'Y' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'C', 'M', 'P' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'D', 'C', 'P' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'C', 'P', 'Y' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'C', 'M', 'P' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'D', 'E', 'C' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'D', 'C', 'P' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'I', 'N', 'Y' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'C', 'M', 'P' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'D', 'E', 'X' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'A', 'X', 'S' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'C', 'P', 'Y' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'C', 'M', 'P' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'D', 'E', 'C' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'D', 'C', 'P' }, MM_EMU_AM_ABS | 3 << 4 },
    // D
    { { 'B', 'N', 'E' }, MM_EMU_AM_REL | 2 << 4 },
    { { 'C', 'M', 'P' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'D', 'C', 'P' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'C', 'M', 'P' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'D', 'E', 'C' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'D', 'C', 'P' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'C', 'L', 'D' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'C', 'M', 'P' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'D', 'C', 'P' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'C', 'M', 'P' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'D', 'E', 'C' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'D', 'C', 'P' }, MM_EMU_AM_ABX | 3 << 4 },
    // E
    { { 'C', 'P', 'X' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'I', 'S', 'B' }, MM_EMU_AM_INX | 2 << 4 },
    { { 'C', 'P', 'X' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'I', 'N', 'C' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'I', 'S', 'B' }, MM_EMU_AM_ZPG | 2 << 4 },
    { { 'I', 'N', 'X' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_IMM | 2 << 4 },
    { { 'C', 'P', 'X' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'I', 'N', 'C' }, MM_EMU_AM_ABS | 3 << 4 },
    { { 'I', 'S', 'B' }, MM_EMU_AM_ABS | 3 << 4 },
    // F
    { { 'B', 'E', 'Q' }, MM_EMU_AM_REL | 2 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'S', 'T', 'P' }, MM_EMU_AM_UNK | 1 << 4 },
    { { 'I', 'S', 'B' }, MM_EMU_AM_INY | 2 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'I', 'N', 'C' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'I', 'S', 'B' }, MM_EMU_AM_ZPX | 2 << 4 },
    { { 'S', 'E', 'D' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_IMP | 1 << 4 },
    { { 'I', 'S', 'B' }, MM_EMU_AM_ABY | 3 << 4 },
    { { 'N', 'O', 'P' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'S', 'B', 'C' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'I', 'N', 'C' }, MM_EMU_AM_ABX | 3 << 4 },
    { { 'I', 'S', 'B' }, MM_EMU_AM_ABX | 3 << 4 },
};
// Get instruction length
static mmInline mmByte_t __static_get_inslen(mmByte_t opcode)
{
    return __opcode_name_data[opcode].mode_inslen >> 4;
}
// Hexadecimal character data
static const char __HEXDATA[] = "0123456789ABCDEF";

// Convert to hexadecimal
static mmInline void __static_btoh(char o[], mmByte_t b)
{
    // High nibble
    o[0] = __HEXDATA[b >> 4];
    // Low nibble
    o[1] = __HEXDATA[b & (mmByte_t)0x0F];
}

// Convert to signed decimal
static mmInline void __static_btod(char o[], mmByte_t b)
{
    const mmByte_t sb = (mmByte_t)b;
    if (sb < 0)
    {
        o[0] = '-';
        b = -b;
    }
    else
    {
        o[0] = '+';
    }
    o[1] = __HEXDATA[(mmByte_t)b / 100];
    o[2] = __HEXDATA[(mmByte_t)b / 10 % 10];
    o[3] = __HEXDATA[(mmByte_t)b % 10];
}
#define __mm_fallthrough

// Disassembly
void mmX6502_Disassembly(struct mmX6502Code_t* code, char buf[MM_DISASSEMBLY_OPCODE_BUF_LEN])
{
    enum
    {
        NAME_FIRSH = 0,
        ADDR_FIRSH = NAME_FIRSH + 4,
        LEN = ADDR_FIRSH + 9
    };

    const struct mmEmuOpcodeName* opname = &__opcode_name_data[code->op];
    enum mmX6502AddressingMode_t addrmode = opname->mode_inslen & 0x0f;

    mmMemset(buf, ' ', LEN); buf[LEN] = ';'; buf[LEN + 1] = 0;
    assert(LEN + 1 < MM_DISASSEMBLY_OPCODE_BUF_LEN && "disassembly opcode buffer length not enough.");
    // Set opcode
    buf[NAME_FIRSH + 0] = opname->name[0];
    buf[NAME_FIRSH + 1] = opname->name[1];
    buf[NAME_FIRSH + 2] = opname->name[2];
    // View addressing mode
    switch (addrmode)
    {
    case MM_EMU_AM_UNK:
        __mm_fallthrough;
    case MM_EMU_AM_IMP:
        // XXX     ;
        break;
    case MM_EMU_AM_ACC:
        // XXX A   ;
        buf[ADDR_FIRSH + 0] = 'A';
        break;
    case MM_EMU_AM_IMM:
        // XXX #$AB
        buf[ADDR_FIRSH + 0] = '#';
        buf[ADDR_FIRSH + 1] = '$';
        __static_btoh(buf + ADDR_FIRSH + 2, code->a1);
        break;
    case MM_EMU_AM_ABS:
        // XXX $ABCD
        __mm_fallthrough;
    case MM_EMU_AM_ABX:
        // XXX $ABCD, X
        __mm_fallthrough;
    case MM_EMU_AM_ABY:
        // XXX $ABCD, Y
        // REAL
        buf[ADDR_FIRSH] = '$';
        __static_btoh(buf + ADDR_FIRSH + 1, code->a2);
        __static_btoh(buf + ADDR_FIRSH + 3, code->a1);
        if (addrmode == MM_EMU_AM_ABS) break;
        buf[ADDR_FIRSH + 5] = ',';
        buf[ADDR_FIRSH + 7] = addrmode == MM_EMU_AM_ABX ? 'X' : 'Y';
        break;
    case MM_EMU_AM_ZPG:
        // XXX $AB
        __mm_fallthrough;
    case MM_EMU_AM_ZPX:
        // XXX $AB, X
        __mm_fallthrough;
    case MM_EMU_AM_ZPY:
        // XXX $AB, Y
        // REAL
        buf[ADDR_FIRSH] = '$';
        __static_btoh(buf + ADDR_FIRSH + 1, code->a1);
        if (addrmode == MM_EMU_AM_ZPG) break;
        buf[ADDR_FIRSH + 3] = ',';
        buf[ADDR_FIRSH + 5] = addrmode == MM_EMU_AM_ABX ? 'X' : 'Y';
        break;
    case MM_EMU_AM_INX:
        // XXX ($AB, X)
        buf[ADDR_FIRSH + 0] = '(';
        buf[ADDR_FIRSH + 1] = '$';
        __static_btoh(buf + ADDR_FIRSH + 2, code->a1);
        buf[ADDR_FIRSH + 4] = ',';
        buf[ADDR_FIRSH + 6] = 'X';
        buf[ADDR_FIRSH + 7] = ')';
        break;
    case MM_EMU_AM_INY:
        // XXX ($AB), Y
        buf[ADDR_FIRSH + 0] = '(';
        buf[ADDR_FIRSH + 1] = '$';
        __static_btoh(buf + ADDR_FIRSH + 2, code->a1);
        buf[ADDR_FIRSH + 4] = ')';
        buf[ADDR_FIRSH + 5] = ',';
        buf[ADDR_FIRSH + 7] = 'Y';
        break;
    case MM_EMU_AM_IND:
        // XXX ($ABCD)
        buf[ADDR_FIRSH + 0] = '(';
        buf[ADDR_FIRSH + 1] = '$';
        __static_btoh(buf + ADDR_FIRSH + 2, code->a2);
        __static_btoh(buf + ADDR_FIRSH + 4, code->a1);
        buf[ADDR_FIRSH + 6] = ')';
        break;
    case MM_EMU_AM_REL:
        // XXX $AB(-085)
        // XXX $ABCD
        buf[ADDR_FIRSH + 0] = '$';

        __static_btoh(buf + ADDR_FIRSH + 1, code->a1);
        buf[ADDR_FIRSH + 3] = '(';
        __static_btod(buf + ADDR_FIRSH + 4, code->a1);
        buf[ADDR_FIRSH + 8] = ')';
        break;
    default:
        break;
    }
}

void mmX6502_DisassemblyAddress(struct mmEmuX6502* p, mmWord_t address, char buf[MM_DISASSEMBLY_ADDRESS_BUF_LEN])
{
    enum
    {
        OFFSET_M = MM_DISASSEMBLY_ADDRESS_BUF_LEN - MM_DISASSEMBLY_OPCODE_BUF_LEN,
        OFFSET = 17
    };
    struct mmX6502Code_t code;

    assert(OFFSET < OFFSET_M && "LESS!");
    mmMemset(buf, ' ', OFFSET);
    buf[0] = '$';
    __static_btoh(buf + 1, (mmUInt8_t)(address >> 8));
    __static_btoh(buf + 3, (mmUInt8_t)(address & 0xFF));

    code.data = 0;
    // Violence (NoMo) reads 3 bytes
    code.op = __static_memory_rd_byte(p, address + 0);

    // Get instruction length
    switch (__static_get_inslen(code.op))
    {
    case 3:
        code.a2 = __static_memory_rd_byte(p, address + 2);
        __static_btoh(buf + 12, (mmUInt8_t)(code.a2));
    case 2:
        code.a1 = __static_memory_rd_byte(p, address + 1);
        __static_btoh(buf + 9, (mmUInt8_t)(code.a1));
    case 1:
        __static_btoh(buf + 6, (mmUInt8_t)(code.op));
    default:
        break;
    }

    // disassembly
    mmX6502_Disassembly(&code, buf + OFFSET);
}

static void __static_x6502_BeforeExecute(void* cpu)
{

}
//static mmByte_t __static_x6502_OpcodeRdByte(void* cpu, mmWord_t addr)
//{
//    return 0;
//}
//
static mmByte_t __static_x6502_MemoryRdByte(void* cpu, mmWord_t addr)
{
    return 0;
}
static void __static_x6502_MemoryWrByte(void* cpu, mmWord_t addr, mmByte_t data)
{

}
//
static void __static_x6502_StackPush(void* cpu, mmByte_t data)
{

}
static mmByte_t __static_x6502_StackPop(void* cpu)
{
    return 0;
}

MM_EXPORT_EMU void mmEmuX6502Callback_Init(struct mmEmuX6502Callback* p)
{
    p->BeforeExecute = &__static_x6502_BeforeExecute;
    //
    // p->OpcodeRdByte = &__static_x6502_OpcodeRdByte;
    //
    p->MemoryRdByte = &__static_x6502_MemoryRdByte;
    p->MemoryWrByte = &__static_x6502_MemoryWrByte;
    //
    p->StackPush = &__static_x6502_StackPush;
    p->StackPop = &__static_x6502_StackPop;
}
MM_EXPORT_EMU void mmEmuX6502Callback_Destroy(struct mmEmuX6502Callback* p)
{
    p->BeforeExecute = &__static_x6502_BeforeExecute;
    //
    // p->OpcodeRdByte = &__static_x6502_OpcodeRdByte;
    //
    p->MemoryRdByte = &__static_x6502_MemoryRdByte;
    p->MemoryWrByte = &__static_x6502_MemoryWrByte;
    //
    p->StackPush = &__static_x6502_StackPush;
    p->StackPop = &__static_x6502_StackPop;
}

MM_EXPORT_EMU void mmEmuX6502_Init(struct mmEmuX6502* p)
{
    mmEmuX6502State_Init(&p->R);
    mmEmuX6502Callback_Init(&p->callback);
    p->cpu = NULL;
    p->MEM = NULL;
    p->illegal_opcode = 0;
    p->opcode = 0;
    p->cycles = 0;
}
MM_EXPORT_EMU void mmEmuX6502_Destroy(struct mmEmuX6502* p)
{
    mmEmuX6502State_Destroy(&p->R);
    mmEmuX6502Callback_Destroy(&p->callback);
    p->cpu = NULL;
    p->MEM = NULL;
    p->illegal_opcode = 0;
    p->opcode = 0;
    p->cycles = 0;
}

MM_EXPORT_EMU void mmEmuX6502_SetCallback(struct mmEmuX6502* p, struct mmEmuX6502Callback* m)
{
    p->callback = *m;
}
MM_EXPORT_EMU void mmEmuX6502_SetCPU(struct mmEmuX6502* p, void* cpu)
{
    p->cpu = cpu;
}
MM_EXPORT_EMU void mmEmuX6502_SetMEM(struct mmEmuX6502* p, mmByte_t** MEM)
{
    p->MEM = MEM;
}
MM_EXPORT_EMU void mmEmuX6502_EXEC(struct mmEmuX6502* p)
{
    const struct mmEmuInstruction* instr = NULL;

    mmByte_t nmi_request = 0;
    mmByte_t irq_request = 0;
    mmWord_t address = 0;

    p->illegal_opcode = 0;
    p->cycles = 0;

#ifdef MM_EMNU_DISASSEMBLY_LOGGER
    (*(p->callback.BeforeExecute))(p->cpu);
#endif// MM_EMNU_DISASSEMBLY_LOGGER
    
    p->opcode = __static_opcode_rd_byte(p, p->R.PC++);

    if (p->R.pending)
    {
        if (p->R.pending & MM_EMU_CPU_NMI_FLAG)
        {
            nmi_request = 0xFF;
            p->R.pending &= ~MM_EMU_CPU_NMI_FLAG;
        }
        else
        {
            if (p->R.pending & MM_EMU_CPU_IRQ_MASK)
            {
                p->R.pending &= ~MM_EMU_CPU_IRQ_TRIGGER2;
                if (!(p->R.P & MM_EMU_CPU_I_FLAG) && p->opcode != 0x40)
                {
                    irq_request = 0xFF;
                    p->R.pending &= ~MM_EMU_CPU_IRQ_TRIGGER;
                }
            }
        }
    }

    instr = &__instruction[p->opcode];
    address = (*(instr->addressing))(p, &p->cycles);
    p->cycles += (mmUInt32_t)__baisc_cycle[p->opcode];
    (*(instr->operation))(p, address, &p->cycles);

    if (0 == p->illegal_opcode)
    {
        if (nmi_request)
        {
            mmEmuX6502_NMI(p, &p->cycles);
        }
        else
        {
            if (irq_request)
            {
                mmEmuX6502_IRQ(p, &p->cycles);
            }
        }
    }
}

/*
* At power-up
*   P = $34[1] (IRQ disabled)[2]
*   A, X, Y = 0
*   S = $FD
*   $4017 = $00 (frame irq enabled)
*   $4015 = $00 (all channels disabled)
*   $4000-$400F = $00 (not sure about $4010-$4013)
*   All 15 bits of noise channel LFSR = $0000[3]. The first time the LFSR is clocked from the all-0s state, it will shift in a 1.
*
*   Internal memory ($0000-$07FF) has unreliable startup state. Some machines may have consistent RAM
*   contents at power-on, but others do not.
*     Emulators often implement a consistent RAM startup state (e.g. all $00 or $FF, or a particular pattern),
*     and flash carts like the PowerPak may partially or fully initialize RAM before starting a program, so an
*     NES programmer must be careful not to rely on the startup contents of RAM.
*/
MM_EXPORT_EMU void mmEmuX6502_HardwareReset(struct mmEmuX6502* p)
{
    p->R.A = 0x00;
    p->R.X = 0x00;
    p->R.Y = 0x00;
    p->R.S = 0xFD;
    p->R.P = MM_EMU_CPU_I_FLAG | MM_EMU_CPU_R_FLAG;
    p->R.PC = 0;

    p->R.pending = 0;

    p->R.PC = __static_memory_rd_word(p, MM_EMU_CPU_RES_VECTOR);
}
/*
* After reset
*   A, X, Y were not affected
*   S was decremented by 3 (but nothing was written to the stack)[3]
*   The I (IRQ disable) flag was set to true (status ORed with $04)
*   The internal memory was unchanged
*   APU mode in $4017 was unchanged
*   APU was silenced ($4015 = 0)
*   APU triangle phase is reset to 0 (i.e. outputs a value of 15, the first step of its waveform)
*   APU DPCM output ANDed with 1 (upper 6 bits cleared)
*   2A03G: APU Frame Counter reset. (but 2A03letterless: APU frame counter retains old value) [6]
*/
MM_EXPORT_EMU void mmEmuX6502_SoftwareReset(struct mmEmuX6502* p)
{
    p->R.PC = __static_memory_rd_word(p, MM_EMU_CPU_RES_VECTOR);
    p->R.P |= MM_EMU_CPU_I_FLAG;
    p->R.S -= 0x03;
}

MM_EXPORT_EMU void mmEmuX6502_NMI(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmByte_t pcl2 = 0;
    mmByte_t pch2 = 0;
    //
    mmByte_t pch = (mmByte_t)((p->R.PC) >> 8);
    mmByte_t pcl = (mmByte_t)(p->R.PC & 0xFF);
    __static_stack_push(p, pch);
    __static_stack_push(p, pcl);
    __static_stack_push(p, p->R.P | (mmByte_t)(MM_EMU_CPU_R_FLAG));
    __static_6502_F_SE(&p->R, MM_EMU_CPU_I_FLAG);
    pcl2 = __static_opcode_rd_byte(p, MM_EMU_CPU_NMI_VECTOR + 0);
    pch2 = __static_opcode_rd_byte(p, MM_EMU_CPU_NMI_VECTOR + 1);
    p->R.PC = (mmWord_t)pcl2 | (mmWord_t)pch2 << 8;
    *cycle += 7;
}
MM_EXPORT_EMU void mmEmuX6502_IRQ(struct mmEmuX6502* p, mmUInt32_t* cycle)
{
    mmByte_t pcl2 = 0;
    mmByte_t pch2 = 0;
    //
    mmByte_t pch = (mmByte_t)((p->R.PC) >> 8);
    mmByte_t pcl = (mmByte_t)(p->R.PC & 0xFF);
    __static_stack_push(p, pch);
    __static_stack_push(p, pcl);
    __static_stack_push(p, p->R.P | (mmByte_t)(MM_EMU_CPU_R_FLAG));
    __static_6502_F_SE(&p->R, MM_EMU_CPU_I_FLAG);
    pcl2 = __static_opcode_rd_byte(p, MM_EMU_CPU_IRQ_VECTOR + 0);
    pch2 = __static_opcode_rd_byte(p, MM_EMU_CPU_IRQ_VECTOR + 1);
    p->R.PC = (mmWord_t)pcl2 | (mmWord_t)pch2 << 8;
    *cycle += 7;
}

MM_EXPORT_EMU void mmEmuX6502_LoggerEXEC(struct mmEmuX6502* p, mmUInt32_t* line, char buffer[128])
{
    char buf[MM_DISASSEMBLY_ADDRESS_BUF_LEN] = { 0 };
    mmWord_t pc = p->R.PC;
    (*line)++;
    mmX6502_DisassemblyAddress(p, pc, buf);

    mmSprintf(buffer,
        "%4d - %s   A:%02X X:%02X Y:%02X P:%02X SP:%02X",
        (*line),
        buf,
        (int)p->R.A,
        (int)p->R.X,
        (int)p->R.Y,
        (int)p->R.P,
        (int)p->R.S);
}

static FORCEINLINE void __static_stack_push(struct mmEmuX6502* p, mmByte_t a)
{
    (*(p->callback.StackPush))(p->cpu, a);
}
static FORCEINLINE mmByte_t __static_stack_pop(struct mmEmuX6502* p)
{
    return (*(p->callback.StackPop))(p->cpu);
}
static FORCEINLINE void __static_check_zsflag(struct mmEmuX6502* p, mmByte_t x)
{
    __static_6502_F_IF(&p->R, MM_EMU_CPU_N_FLAG, x & (mmByte_t)0x80);
    __static_6502_F_IF(&p->R, MM_EMU_CPU_Z_FLAG, x == 0);
}

static FORCEINLINE mmByte_t __static_opcode_rd_byte(struct mmEmuX6502* p, mmWord_t addr)
{
    // return (*(p->callback.OpcodeRdByte))(p->cpu, addr);
    return p->MEM[addr >> 13][addr & 0x1FFF];
}
//static mmInline mmWord_t __static_opcode_rd_word(struct mmEmuX6502* p, mmWord_t addr)
//{
//    mmByte_t address0 = __static_opcode_rd_byte(p, addr);
//    mmByte_t address1 = __static_opcode_rd_byte(p, addr + 1);
//    return (mmWord_t)address0 | (mmWord_t)((mmWord_t)address1 << 8);
//}

static FORCEINLINE mmByte_t __static_memory_rd_byte(struct mmEmuX6502* p, mmWord_t addr)
{
    return (*(p->callback.MemoryRdByte))(p->cpu, addr);
}
static FORCEINLINE mmWord_t __static_memory_rd_word(struct mmEmuX6502* p, mmWord_t addr)
{
    mmByte_t address0 = __static_memory_rd_byte(p, addr);
    mmByte_t address1 = __static_memory_rd_byte(p, addr + 1);
    return (mmWord_t)address0 | (mmWord_t)((mmWord_t)address1 << 8);
}
static FORCEINLINE void __static_memory_wr_byte(struct mmEmuX6502* p, mmWord_t addr, mmByte_t data)
{
    (*(p->callback.MemoryWrByte))(p->cpu, addr, data);
}

