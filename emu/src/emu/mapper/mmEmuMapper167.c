/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper167.h"
#include "emu/mmEmuNes.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuMapper167_Init(struct mmEmuMapper167* p)
{
    mmEmuMapper_Init(&p->super);

    p->regs[0] = 0;
    p->regs[1] = 0;
    p->regs[2] = 0;
    p->regs[3] = 0;

    p->rom_type = 0;

    //
    p->super.Reset = &mmEmuMapper167_Reset;
    p->super.Write = &mmEmuMapper167_Write;
    //p->super.Read = &mmEmuMapper167_Read;
    //p->super.ReadLow = &mmEmuMapper167_ReadLow;
    //p->super.WriteLow = &mmEmuMapper167_WriteLow;
    //p->super.ExRead = &mmEmuMapper167_ExRead;
    //p->super.ExWrite = &mmEmuMapper167_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper167_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper167_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper167_HSync;
    //p->super.VSync = &mmEmuMapper167_VSync;
    //p->super.Clock = &mmEmuMapper167_Clock;
    //p->super.PPULatch = &mmEmuMapper167_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper167_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper167_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper167_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper167_IsStateSave;
    p->super.SaveState = &mmEmuMapper167_SaveState;
    p->super.LoadState = &mmEmuMapper167_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper167_Destroy(struct mmEmuMapper167* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->regs[0] = 0;
    p->regs[1] = 0;
    p->regs[2] = 0;
    p->regs[3] = 0;

    p->rom_type = 0;
}

MM_EXPORT_EMU void mmEmuMapper167_SetBankCPU(struct mmEmuMapper167* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //struct mmLogger* gLogger = mmLogger_Instance();

    int base, bank;

    base = ((p->regs[0] ^ p->regs[1]) & 0x10) << 1;
    bank = (p->regs[2] ^ p->regs[3]) & 0x1f;

    if (p->regs[1] & 0x08)
    {
        bank &= 0xfe;
        if (p->rom_type == 0)
        {
            mmEmuMmu_SetPROM16KBank(mmu, 4, base + bank + 1);
            mmEmuMmu_SetPROM16KBank(mmu, 6, base + bank + 0);
        }
        else
        {
            mmEmuMmu_SetPROM16KBank(mmu, 6, base + bank + 1);
            mmEmuMmu_SetPROM16KBank(mmu, 4, base + bank + 0);
        }
        //mmLogger_LogD(gLogger, "%s %d 32K MODE!", __FUNCTION__, __LINE__);
    }
    else
    {
        if (p->regs[1] & 0x04)
        {
            mmEmuMmu_SetPROM16KBank(mmu, 4, 0x1f);
            mmEmuMmu_SetPROM16KBank(mmu, 6, base + bank);
            //mmLogger_LogD(gLogger, "%s %d HIGH 16K MODE!", __FUNCTION__, __LINE__);
        }
        else
        {
            mmEmuMmu_SetPROM16KBank(mmu, 4, base + bank);
            if (p->rom_type == 0)
            {
                mmEmuMmu_SetPROM16KBank(mmu, 6, 0x20);
            }
            else
            {
                mmEmuMmu_SetPROM16KBank(mmu, 6, 0x07);
            }
            //mmLogger_LogD(gLogger, "%s %d LOW  16K MODE!", __FUNCTION__, __LINE__);
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper167_SetBankPPU(struct mmEmuMapper167* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetCRAM8KBank(mmu, 0);
}

MM_EXPORT_EMU void mmEmuMapper167_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper167* p = (struct mmEmuMapper167*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->regs[0] = 0;
    p->regs[1] = 0;
    p->regs[2] = 0;
    p->regs[3] = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x82F1Fb96)
    {
        // Subor Computer(Russia)
        p->rom_type = 1;
    }
    else
    {
        // Subor Computer(Chinese)
        p->rom_type = 0;
    }

    mmEmuMapper167_SetBankCPU(p);
    mmEmuMapper167_SetBankPPU(p);
}
MM_EXPORT_EMU void mmEmuMapper167_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper167* p = (struct mmEmuMapper167*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    //struct mmLogger* gLogger = mmLogger_Instance();

    int idx;

    idx = (addr >> 13) & 0x03;
    p->regs[idx] = data;
    mmEmuMapper167_SetBankCPU(p);
    mmEmuMapper167_SetBankPPU(p);

    //mmLogger_LogD(gLogger, "%s %d write to %04x:%02x", __FUNCTION__, __LINE__, addr, data);
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper167_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper167_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper167* p = (struct mmEmuMapper167*)(super);

    buffer[0] = p->regs[0];
    buffer[1] = p->regs[1];
    buffer[2] = p->regs[2];
    buffer[3] = p->regs[3];
    buffer[4] = p->rom_type;
}
MM_EXPORT_EMU void mmEmuMapper167_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper167* p = (struct mmEmuMapper167*)(super);

    p->regs[0] = buffer[0];
    p->regs[1] = buffer[1];
    p->regs[2] = buffer[2];
    p->regs[3] = buffer[3];
    p->rom_type = buffer[4];
}

static struct mmEmuMapper* __static_mmEmuMapper167_Produce_mapper167(void)
{
    struct mmEmuMapper167* p = (struct mmEmuMapper167*)mmMalloc(sizeof(struct mmEmuMapper167));
    mmEmuMapper167_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper167_Recycle_mapper167(struct mmEmuMapper* m)
{
    struct mmEmuMapper167* p = (struct mmEmuMapper167*)(m);
    mmEmuMapper167_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER167 =
{
    &__static_mmEmuMapper167_Produce_mapper167,
    &__static_mmEmuMapper167_Recycle_mapper167,
};
