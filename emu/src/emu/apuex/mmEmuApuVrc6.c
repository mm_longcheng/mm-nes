/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApuVrc6.h"

#include "emu/mmEmuState.h"

#include "core/mmString.h"

#define RECTANGLE_VOL_SHIFT 8
#define SAWTOOTH_VOL_SHIFT  6

MM_EXPORT_EMU void mmEmuApuVrc6_Init(struct mmEmuApuVrc6* p)
{
    mmEmuApuInterface_Init(&p->super);

    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuVrc6Rectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuVrc6Rectangle));
    mmMemset(&p->ch2, 0, sizeof(struct mmEmuApuVrc6Sawtooth));

    p->cycle_rate = 0;

    p->cpu_clock = 0;

    //
    p->super.Reset = &mmEmuApuVrc6_Reset;
    p->super.Setup = &mmEmuApuVrc6_Setup;
    p->super.Process = &mmEmuApuVrc6_Process;
    p->super.Write = &mmEmuApuVrc6_Write;
    //p->super.Read = &mm_emu_apu_vrc6_Read;
    //p->super.SyncWrite = &mm_emu_apu_vrc6_SyncWrite;
    //p->super.SyncRead = &mm_emu_apu_vrc6_SyncRead;
    //p->super.Sync = &mm_emu_apu_vrc6_Sync;
    p->super.GetFreq = &mmEmuApuVrc6_GetFreq;
    p->super.GetStateSize = &mmEmuApuVrc6_GetStateSize;
    p->super.SaveState = &mmEmuApuVrc6_SaveState;
    p->super.LoadState = &mmEmuApuVrc6_LoadState;

    // Provisional setting.
    (*(p->super.Reset))(&p->super, MM_EMU_APU_CLOCK, 22050);
}
MM_EXPORT_EMU void mmEmuApuVrc6_Destroy(struct mmEmuApuVrc6* p)
{
    mmEmuApuInterface_Destroy(&p->super);

    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuVrc6Rectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuVrc6Rectangle));
    mmMemset(&p->ch2, 0, sizeof(struct mmEmuApuVrc6Sawtooth));

    p->cycle_rate = 0;

    p->cpu_clock = 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_RectangleRender(struct mmEmuApuVrc6* p, struct mmEmuApuVrc6Rectangle* ch)
{
    mmInt_t output = 0;
    // Enable?
    if (!ch->enable)
    {
        ch->output_vol = 0;
        ch->adder = 0;
        return ch->output_vol;
    }

    // Digitized output
    if (ch->gate)
    {
        ch->output_vol = ch->volume << RECTANGLE_VOL_SHIFT;
        return ch->output_vol;
    }

    // Do not process frequencies above a certain level (wasted).
    if (ch->freq < MM_EMU_INT2FIX(8))
    {
        ch->output_vol = 0;
        return ch->output_vol;
    }

    ch->phaseacc -= p->cycle_rate;
    if (ch->phaseacc >= 0)
    {
        return ch->output_vol;
    }

    output = ch->volume << RECTANGLE_VOL_SHIFT;

    if (ch->freq > p->cycle_rate)
    {
        // add 1 step
        ch->phaseacc += ch->freq;
        ch->adder = (ch->adder + 1) & 0x0F;
        if (ch->adder <= ch->duty_pos)
        {
            ch->output_vol = output;
        }
        else
        {
            ch->output_vol = -output;
        }
    }
    else
    {
        // average calculate
        mmInt_t num_times, total;
        num_times = total = 0;
        while (ch->phaseacc < 0)
        {
            ch->phaseacc += ch->freq;
            ch->adder = (ch->adder + 1) & 0x0F;
            if (ch->adder <= ch->duty_pos)
            {
                total += output;
            }
            else
            {
                total += -output;
            }
            num_times++;
        }
        ch->output_vol = total / num_times;
    }

    return ch->output_vol;
}
MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_SawtoothRender(struct mmEmuApuVrc6* p, struct mmEmuApuVrc6Sawtooth* ch)
{
    // Digitized output
    if (!ch->enable)
    {
        ch->output_vol = 0;
        return ch->output_vol;
    }

    // Do not process frequencies above a certain level (wasted).
    if (ch->freq < MM_EMU_INT2FIX(9))
    {
        return ch->output_vol;
    }

    ch->phaseacc -= p->cycle_rate / 2;
    if (ch->phaseacc >= 0)
    {
        return ch->output_vol;
    }

    if (ch->freq > p->cycle_rate / 2)
    {
        // add 1 step
        ch->phaseacc += ch->freq;
        if (++ch->adder >= 7)
        {
            ch->adder = 0;
            ch->accum = 0;
        }
        ch->accum += ch->phaseaccum;
        ch->output_vol = ch->accum << SAWTOOTH_VOL_SHIFT;
    }
    else
    {
        // average calculate
        mmInt_t num_times, total;
        num_times = total = 0;
        while (ch->phaseacc < 0)
        {
            ch->phaseacc += ch->freq;
            if (++ch->adder >= 7)
            {
                ch->adder = 0;
                ch->accum = 0;
            }
            ch->accum += ch->phaseaccum;
            total += ch->accum << SAWTOOTH_VOL_SHIFT;
            num_times++;
        }
        ch->output_vol = (total / num_times);
    }

    return ch->output_vol;
}

MM_EXPORT_EMU void mmEmuApuVrc6_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuVrc6* p = (struct mmEmuApuVrc6*)(super);

    mmMemset(&p->ch0, 0, sizeof(struct mmEmuApuVrc6Rectangle));
    mmMemset(&p->ch1, 0, sizeof(struct mmEmuApuVrc6Rectangle));
    mmMemset(&p->ch2, 0, sizeof(struct mmEmuApuVrc6Sawtooth));

    (*(p->super.Setup))(&p->super, fClock, nRate);
}
MM_EXPORT_EMU void mmEmuApuVrc6_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuVrc6* p = (struct mmEmuApuVrc6*)(super);

    p->cpu_clock = fClock;
    p->cycle_rate = (mmInt_t)(fClock * 65536.0f / (float)nRate);
}
MM_EXPORT_EMU void mmEmuApuVrc6_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuVrc6* p = (struct mmEmuApuVrc6*)(super);

    switch (addr)
    {
        // VRC6 CH0 rectangle
    case 0x9000:
        p->ch0.reg[0] = data;
        p->ch0.gate = data & 0x80;
        p->ch0.volume = data & 0x0F;
        p->ch0.duty_pos = (data >> 4) & 0x07;
        break;
    case 0x9001:
        p->ch0.reg[1] = data;
        p->ch0.freq = MM_EMU_INT2FIX((((p->ch0.reg[2] & 0x0F) << 8) | data) + 1);
        break;
    case 0x9002:
        p->ch0.reg[2] = data;
        p->ch0.enable = data & 0x80;
        p->ch0.freq = MM_EMU_INT2FIX((((data & 0x0F) << 8) | p->ch0.reg[1]) + 1);
        break;
        // VRC6 CH1 rectangle
    case 0xA000:
        p->ch1.reg[0] = data;
        p->ch1.gate = data & 0x80;
        p->ch1.volume = data & 0x0F;
        p->ch1.duty_pos = (data >> 4) & 0x07;
        break;
    case 0xA001:
        p->ch1.reg[1] = data;
        p->ch1.freq = MM_EMU_INT2FIX((((p->ch1.reg[2] & 0x0F) << 8) | data) + 1);
        break;
    case 0xA002:
        p->ch1.reg[2] = data;
        p->ch1.enable = data & 0x80;
        p->ch1.freq = MM_EMU_INT2FIX((((data & 0x0F) << 8) | p->ch1.reg[1]) + 1);
        break;
        // VRC6 CH2 sawtooth
    case 0xB000:
        p->ch2.reg[1] = data;
        p->ch2.phaseaccum = data & 0x3F;
        break;
    case 0xB001:
        p->ch2.reg[1] = data;
        p->ch2.freq = MM_EMU_INT2FIX((((p->ch2.reg[2] & 0x0F) << 8) | data) + 1);
        break;
    case 0xB002:
        p->ch2.reg[2] = data;
        p->ch2.enable = data & 0x80;
        p->ch2.freq = MM_EMU_INT2FIX((((data & 0x0F) << 8) | p->ch2.reg[1]) + 1);
        // p->ch2.adder = 0;    // Clearing it will cause noise.
        // p->ch2.accum = 0;    // Clearing it will cause noise.
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_Process(struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuVrc6* p = (struct mmEmuApuVrc6*)(super);

    switch (channel)
    {
    case 0:
        return mmEmuApuVrc6_RectangleRender(p, &p->ch0);
        break;
    case 1:
        return mmEmuApuVrc6_RectangleRender(p, &p->ch1);
        break;
    case 2:
        return mmEmuApuVrc6_SawtoothRender(p, &p->ch2);
        break;
    default:
        break;
    }

    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuVrc6* p = (struct mmEmuApuVrc6*)(super);

    if (channel == 0 || channel == 1)
    {
        struct mmEmuApuVrc6Rectangle* ch = NULL;
        if (channel == 0)
        {
            ch = &p->ch0;
        }
        else
        {
            ch = &p->ch1;
        }
        if (!ch->enable || ch->gate || !ch->volume)
        {
            return 0;
        }
        if (ch->freq < MM_EMU_INT2FIX(8))
        {
            return 0;
        }
        return (mmInt_t)(256.0f * p->cpu_clock / ((double)MM_EMU_FIX2INT(ch->freq) * 16.0f));
    }
    if (channel == 2)
    {
        struct mmEmuApuVrc6Sawtooth* ch = &p->ch2;
        if (!ch->enable || !ch->phaseaccum)
        {
            return 0;
        }
        if (ch->freq < MM_EMU_INT2FIX(8))
        {
            return 0;
        }
        return (mmInt_t)(256.0f * p->cpu_clock / ((double)MM_EMU_FIX2INT(ch->freq) * 14.0f));
    }

    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_GetStateSize(const struct mmEmuApuInterface* super)
{
    // struct mmEmuApuVrc6* p = (struct mmEmuApuVrc6*)(super);

    return
        sizeof(struct mmEmuApuVrc6Rectangle) +
        sizeof(struct mmEmuApuVrc6Rectangle) +
        sizeof(struct mmEmuApuVrc6Sawtooth);
}
MM_EXPORT_EMU void mmEmuApuVrc6_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuVrc6* p = (struct mmEmuApuVrc6*)(super);

    MM_EMU_SETBLOCK(buffer, &p->ch0, sizeof(struct mmEmuApuVrc6Rectangle));
    MM_EMU_SETBLOCK(buffer, &p->ch1, sizeof(struct mmEmuApuVrc6Rectangle));
    MM_EMU_SETBLOCK(buffer, &p->ch2, sizeof(struct mmEmuApuVrc6Sawtooth));
}
MM_EXPORT_EMU void mmEmuApuVrc6_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuVrc6* p = (struct mmEmuApuVrc6*)(super);

    MM_EMU_GETBLOCK(buffer, &p->ch0, sizeof(struct mmEmuApuVrc6Rectangle));
    MM_EMU_GETBLOCK(buffer, &p->ch1, sizeof(struct mmEmuApuVrc6Rectangle));
    MM_EMU_GETBLOCK(buffer, &p->ch2, sizeof(struct mmEmuApuVrc6Sawtooth));
}

