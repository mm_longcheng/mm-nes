/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuErrorCode.h"
#include "core/mmString.h"

MM_EXPORT_EMU void mmErrorDesc_SetEmu(struct mmErrorDesc* p)
{
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_OUT_OF_MEMORY         , "Out of memory.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_FILE_READ             , "File read error.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_FILE_WRITE            , "File write error.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_FILE_NOT_EXIST        , "File not exist.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_FILE_COMPRESS         , "File compress error.");

    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_THERE_IS_NO_ROM       , "There is no rom.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_THERE_IS_NO_DISK_BIOS , "There is no disk bios.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_THERE_IS_NO_STATE_FILE, "There is no state file.");

    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_NOT_SUPPORTED_FORMAT  , "Not supported format.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_NOT_SUPPORTED_DISK    , "Not supported disk.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_NOT_SUPPORTED_MAPPER  , "Not supported mapper.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_NOT_SUPPORTED_BANK    , "Not supported bank.");

    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_INVALID_NES_HEADER    , "Invalid nes header.");

    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_ILLEGAL_DISK_SIZE     , "Illegal disk size.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_ILLEGAL_OPCODE        , "Illegal opcode.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_ILLEGAL_STATE_CRC     , "Illegal state crc.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_ILLEGAL_MOVIE_OLD     , "Illegal movie old.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_ILLEGAL_MOVIE_CRC     , "Illegal movie crc.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_ILLEGAL_MOVIE_VER     , "Illegal movie version.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_ILLEGAL_ROM_CRC       , "Illegal rom crc.");

    mmErrorDesc_SetCodeDesc(p, MM_ERR_EMU_FILE_SIZE_IS_TOO_SMALL, "File size is too small.");
}
