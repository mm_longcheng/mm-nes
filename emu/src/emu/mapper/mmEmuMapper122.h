/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper122_h__
#define __mmEmuMapper122_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper122/184  SunSoft-1                                             //
//----------------------------------------------------------------------//
struct mmEmuMapper122
{
    struct mmEmuMapper super;
};

MM_EXPORT_EMU void mmEmuMapper122_Init(struct mmEmuMapper122* p);
MM_EXPORT_EMU void mmEmuMapper122_Destroy(struct mmEmuMapper122* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper122_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper122_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER122;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper122_h__
