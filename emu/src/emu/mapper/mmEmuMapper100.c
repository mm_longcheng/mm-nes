/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper100.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper100_Init(struct mmEmuMapper100* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;
    p->prg2 = 0;
    p->prg3 = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    //
    p->super.Reset = &mmEmuMapper100_Reset;
    p->super.Write = &mmEmuMapper100_Write;
    //p->super.Read = &mmEmuMapper100_Read;
    //p->super.ReadLow = &mmEmuMapper100_ReadLow;
    //p->super.WriteLow = &mmEmuMapper100_WriteLow;
    //p->super.ExRead = &mmEmuMapper100_ExRead;
    //p->super.ExWrite = &mmEmuMapper100_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper100_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper100_ExCmdWrite;
    p->super.HSync = &mmEmuMapper100_HSync;
    //p->super.VSync = &mmEmuMapper100_VSync;
    //p->super.Clock = &mmEmuMapper100_Clock;
    //p->super.PPULatch = &mmEmuMapper100_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper100_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper100_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper100_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper100_IsStateSave;
    p->super.SaveState = &mmEmuMapper100_SaveState;
    p->super.LoadState = &mmEmuMapper100_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper100_Destroy(struct mmEmuMapper100* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;
    p->prg2 = 0;
    p->prg3 = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}

MM_EXPORT_EMU void mmEmuMapper100_SetBankCPU(struct mmEmuMapper100* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, p->prg2, p->prg3);
}
MM_EXPORT_EMU void mmEmuMapper100_SetBankPPU(struct mmEmuMapper100* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBankArray(mmu, p->chr0, p->chr1, p->chr2, p->chr3, p->chr4, p->chr5, p->chr6, p->chr7);
    }
}

MM_EXPORT_EMU void mmEmuMapper100_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper100* p = (struct mmEmuMapper100*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    p->prg2 = mmu->PROM_08K_SIZE - 2;
    p->prg3 = mmu->PROM_08K_SIZE - 1;

    mmEmuMapper100_SetBankCPU(p);

    if (mmu->VROM_1K_SIZE)
    {
        p->chr0 = 0;
        p->chr1 = 1;
        p->chr2 = 2;
        p->chr3 = 3;
        p->chr4 = 4;
        p->chr5 = 5;
        p->chr6 = 6;
        p->chr7 = 7;

        mmEmuMapper100_SetBankPPU(p);
    }
    else
    {
        p->chr0 = p->chr2 = p->chr4 = p->chr5 = p->chr6 = p->chr7 = 0;
        p->chr1 = p->chr3 = 1;
    }

    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0;
}
MM_EXPORT_EMU void mmEmuMapper100_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper100* p = (struct mmEmuMapper100*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        break;
    case 0x8001:
        p->reg[1] = data;

        switch (p->reg[0] & 0xC7)
        {
        case 0x00:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr0 = data & 0xFE;
                p->chr1 = p->chr0 + 1;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x01:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr2 = data & 0xFE;
                p->chr3 = p->chr2 + 1;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x02:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr4 = data;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x03:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr5 = data;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x04:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr6 = data;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x05:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr7 = data;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;

        case 0x06:
            p->prg0 = data;
            mmEmuMapper100_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper100_SetBankCPU(p);
            break;
        case 0x46:
            p->prg2 = data;
            mmEmuMapper100_SetBankCPU(p);
            break;
        case 0x47:
            p->prg3 = data;
            mmEmuMapper100_SetBankCPU(p);
            break;

        case 0x80:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr4 = data & 0xFE;
                p->chr5 = p->chr4 + 1;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x81:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr6 = data & 0xFE;
                p->chr7 = p->chr6 + 1;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x82:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr0 = data;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x83:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr1 = data;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x84:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr2 = data;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        case 0x85:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr3 = data;
                mmEmuMapper100_SetBankPPU(p);
            }
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        break;
    case 0xC000:
        p->reg[4] = data;
        p->irq_counter = data;
        break;
    case 0xC001:
        p->reg[5] = data;
        p->irq_latch = data;
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 0xFF;
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper100_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper100* p = (struct mmEmuMapper100*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(p->irq_counter--))
                {
                    p->irq_counter = p->irq_latch;
                    //mm_emu_cpu_IRQ(&nes->cpu);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper100_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper100_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper100* p = (struct mmEmuMapper100*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->prg2;
    buffer[11] = p->prg3;
    buffer[12] = p->chr0;
    buffer[13] = p->chr1;
    buffer[14] = p->chr2;
    buffer[15] = p->chr3;
    buffer[16] = p->chr4;
    buffer[17] = p->chr5;
    buffer[18] = p->chr6;
    buffer[19] = p->chr7;
    buffer[20] = p->irq_enable;
    buffer[21] = p->irq_counter;
    buffer[22] = p->irq_latch;
}
MM_EXPORT_EMU void mmEmuMapper100_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper100* p = (struct mmEmuMapper100*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->prg2 = buffer[10];
    p->prg3 = buffer[11];
    p->chr0 = buffer[12];
    p->chr1 = buffer[13];
    p->chr2 = buffer[14];
    p->chr3 = buffer[15];
    p->chr4 = buffer[16];
    p->chr5 = buffer[17];
    p->chr6 = buffer[18];
    p->chr7 = buffer[19];
    p->irq_enable = buffer[20];
    p->irq_counter = buffer[21];
    p->irq_latch = buffer[22];
}

static struct mmEmuMapper* __static_mmEmuMapper100_Produce(void)
{
    struct mmEmuMapper100* p = (struct mmEmuMapper100*)mmMalloc(sizeof(struct mmEmuMapper100));
    mmEmuMapper100_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper100_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper100* p = (struct mmEmuMapper100*)(m);
    mmEmuMapper100_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER100 =
{
    &__static_mmEmuMapper100_Produce,
    &__static_mmEmuMapper100_Recycle,
};
