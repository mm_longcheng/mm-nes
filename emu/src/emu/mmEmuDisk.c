/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuDisk.h"
#include "mmEmuNes.h"
#include "mmEmuErrorCode.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU int mmEmuDisk_LoadDISK(struct mmEmuNes* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEmuAssets* assets = &p->assets;

    int code = MM_UNKNOWN;

    mmInt_t i, j, diskno;
    FILE*   fp = NULL;
    struct mmEmuDiskImgFileHdr ifh;
    struct mmEmuDiskImgHdr hdr;
    mmByte_t* disk;

    mmWord_t Version;

    struct mmString szDiskFile;

    mmString_Init(&szDiskFile);

    do
    {
        if (mmEmuRom_GetMapperNo(&p->rom) != 20)
        {
            code = MM_SUCCESS;
            break;
        }

        mmEmuAssets_GetDiskFile(assets, &szDiskFile);

        mmLogger_LogD(gLogger, "%s %d Path: %s", __FUNCTION__, __LINE__, mmString_CStr(&szDiskFile));

        if (!(fp = fopen(mmString_CStr(&szDiskFile), "rb")))
        {
            // Can not open xxx file.
            // this error is not serious.
            mmLogger_LogD(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, mmString_CStr(&szDiskFile));
            code = MM_SUCCESS;
            break;
        }

        if (fread(&ifh, sizeof(struct mmEmuDiskImgFileHdr), 1, fp) != 1)
        {
            mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
            code = MM_ERR_EMU_FILE_READ;
            break;
        }

        if (mmMemcmp(ifh.ID, "VirtuaNES DI", sizeof(ifh.ID)) == 0)
        {
            if (ifh.BlockVersion < 0x0100 || ifh.BlockVersion > 0x0210)
            {
                // Not supported format.
                mmLogger_LogE(gLogger, "%s %d Not supported format.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_NOT_SUPPORTED_FORMAT;
                break;
            }
            Version = ifh.BlockVersion;
        }
        else
        {
            // Not supported format.
            mmLogger_LogE(gLogger, "%s %d Not supported format.", __FUNCTION__, __LINE__);
            code = MM_ERR_EMU_NOT_SUPPORTED_FORMAT;
            break;
        }

        if (Version == 0x0100)
        {
            // Ver 0.24 or earlier
            if (ifh.DiskNumber > 4)
            {
                // Not supported format.
                mmLogger_LogE(gLogger, "%s %d Not supported format.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_NOT_SUPPORTED_FORMAT;
                break;
            }

            for (i = 0; i < (mmInt_t)ifh.DiskNumber; i++)
            {
                if (fread(&hdr, sizeof(struct mmEmuDiskImgHdr), 1, fp) != 1)
                {
                    if (i == 0)
                    {
                        mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_FILE_READ;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }

                if (mmMemcmp(hdr.ID, "SIDE0A", sizeof(hdr.ID)) == 0)
                {
                    diskno = 0;
                }
                else if (mmMemcmp(hdr.ID, "SIDE0B", sizeof(hdr.ID)) == 0)
                {
                    diskno = 1;
                }
                else if (mmMemcmp(hdr.ID, "SIDE1A", sizeof(hdr.ID)) == 0)
                {
                    diskno = 2;
                }
                else if (mmMemcmp(hdr.ID, "SIDE1B", sizeof(hdr.ID)) == 0)
                {
                    diskno = 3;
                }
                else
                {
                    // Not supported format.
                    mmLogger_LogE(gLogger, "%s %d Not supported format.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_NOT_SUPPORTED_FORMAT;
                    break;
                }

                for (j = 0; j < 16; j++)
                {
                    if (hdr.DiskTouch[j])
                    {
                        disk = mmEmuRom_GetPROM(&p->rom) + 16 + 65500 * diskno + (4 * 1024)*j;
                        if (j < 15)
                        {
                            if (fread(disk, 4 * 1024, 1, fp) != 1)
                            {
                                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                                code = MM_ERR_EMU_FILE_READ;
                                break;
                            }
                        }
                        else
                        {
                            if (fread(disk, 4 * 1024 - 36, 1, fp) != 1)
                            {
                                mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                                code = MM_ERR_EMU_FILE_READ;
                                break;
                            }
                        }
                    }
                }
            }
            code = MM_SUCCESS;
        }
        else
        {
            if (Version == 0x0200 || Version == 0x0210)
            {
                // Ver 0.30 or later
                struct mmEmuDiskFileHdr dfh;
                mmByte_t* lpDisk = mmEmuRom_GetPROM(&p->rom);
                mmByte_t* lpWrite = mmEmuRom_GetDISK(&p->rom);
                mmLong_t DiskSize = 16 + 65500 * mmEmuRom_GetDiskNo(&p->rom);
                mmUInt32_t  pos;
                mmByte_t data;

                // Rewrite FLAG erase.
                mmMemset(lpWrite, 0, 16 + 65500 * mmEmuRom_GetDiskNo(&p->rom));

                // Header rereading
                if (fseek(fp, 0, SEEK_SET))
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_READ;
                    break;
                }
                if (fread(&dfh, sizeof(struct mmEmuDiskFileHdr), 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_READ;
                    break;
                }

                if (p->config.emulator.bCrcCheck)
                {
                    // Check if it is different from the currently loaded title.
                    if (dfh.ProgID != mmEmuRom_GetGameID(&p->rom) ||
                        dfh.MakerID != (mmWord_t)mmEmuRom_GetMakerID(&p->rom) ||
                        dfh.DiskNo != (mmWord_t)mmEmuRom_GetDiskNo(&p->rom))
                    {
                        mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_FILE_READ;
                        break;
                    }
                }

                for (i = 0; i < (mmInt_t)dfh.DifferentSize; i++)
                {
                    if (fread(&pos, sizeof(mmWord_t), 1, fp) != 1)
                    {
                        // Failed to read file.
                        mmLogger_LogE(gLogger, "%s %d Failed to read file.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_FILE_READ;
                        break;
                    }
                    data = (mmByte_t)(pos >> 24);
                    pos &= 0x00FFFFFF;
                    if (pos >= 16 && (mmLong_t)pos < DiskSize)
                    {
                        lpDisk[pos] = data;
                        lpWrite[pos] = 0xFF;
                    }
                }
                code = MM_SUCCESS;
            }
        }
    } while (0);

    mmString_Destroy(&szDiskFile);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    return code;
}
MM_EXPORT_EMU int mmEmuDisk_SaveDISK(struct mmEmuNes* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEmuAssets* assets = &p->assets;

    int code = MM_UNKNOWN;

    mmInt_t i;
    FILE* fp = NULL;
    struct mmEmuDiskFileHdr ifh;
    mmByte_t* lpDisk = mmEmuRom_GetPROM(&p->rom);
    mmByte_t* lpWrite = mmEmuRom_GetDISK(&p->rom);
    mmLong_t DiskSize = 16 + 65500 * mmEmuRom_GetDiskNo(&p->rom);
    mmUInt32_t data;

    struct mmString szDiskFile;

    mmString_Init(&szDiskFile);

    do
    {
        if (mmEmuRom_GetMapperNo(&p->rom) != 20)
        {
            code = MM_SUCCESS;
            break;
        }

        mmMemset(&ifh, 0, sizeof(ifh));

        mmMemcpy(ifh.ID, "VirtuaNES DI", sizeof(ifh.ID));
        ifh.BlockVersion = 0x0210;
        ifh.ProgID = mmEmuRom_GetGameID(&p->rom);
        ifh.MakerID = (mmWord_t)mmEmuRom_GetMakerID(&p->rom);
        ifh.DiskNo = (mmWord_t)mmEmuRom_GetDiskNo(&p->rom);

        // Count the number of differences.
        for (i = 16; i < DiskSize; i++)
        {
            if (lpWrite[i])
            {
                ifh.DifferentSize++;
            }
        }

        if (!ifh.DifferentSize)
        {
            code = MM_SUCCESS;
            break;
        }

        mmEmuAssets_GetDiskFile(assets, &szDiskFile);

        mmLogger_LogD(gLogger, "%s %d Path: %s", __FUNCTION__, __LINE__, mmString_CStr(&szDiskFile));

        if (!(fp = fopen(mmString_CStr(&szDiskFile), "wb")))
        {
            // Can not open xxx file.
            mmLogger_LogE(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, mmString_CStr(&szDiskFile));
            code = MM_ERR_EMU_FILE_NOT_EXIST;
            break;
        }

        if (fwrite(&ifh, sizeof(struct mmEmuDiskFileHdr), 1, fp) != 1)
        {
            mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        for (i = 16; i < DiskSize; i++)
        {
            if (lpWrite[i])
            {
                data = i & 0x00FFFFFF;
                data |= ((mmUInt32_t)lpDisk[i] & 0xFF) << 24;

                // Write File
                if (fwrite(&data, sizeof(mmUInt32_t), 1, fp) != 1)
                {
                    mmLogger_LogE(gLogger, "%s %d Failed to write file.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_WRITE;
                    break;
                }
            }
        }
        code = MM_SUCCESS;
    } while (0);

    mmString_Destroy(&szDiskFile);

    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }
    return code;
}

