/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper241.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper241_Init(struct mmEmuMapper241* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper241_Reset;
    p->super.Write = &mmEmuMapper241_Write;
    //p->super.Read = &mmEmuMapper241_Read;
    //p->super.ReadLow = &mmEmuMapper241_ReadLow;
    //p->super.WriteLow = &mmEmuMapper241_WriteLow;
    //p->super.ExRead = &mmEmuMapper241_ExRead;
    //p->super.ExWrite = &mmEmuMapper241_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper241_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper241_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper241_HSync;
    //p->super.VSync = &mmEmuMapper241_VSync;
    //p->super.Clock = &mmEmuMapper241_Clock;
    //p->super.PPULatch = &mmEmuMapper241_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper241_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper241_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper241_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper241_IsStateSave;
    //p->super.SaveState = &mmEmuMapper241_SaveState;
    //p->super.LoadState = &mmEmuMapper241_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper241_Destroy(struct mmEmuMapper241* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper241_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper241* p = (struct mmEmuMapper241*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper241_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper241* p = (struct mmEmuMapper241*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0x8000)
    {
        mmEmuMmu_SetPROM32KBank(mmu, data);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper241_Produce(void)
{
    struct mmEmuMapper241* p = (struct mmEmuMapper241*)mmMalloc(sizeof(struct mmEmuMapper241));
    mmEmuMapper241_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper241_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper241* p = (struct mmEmuMapper241*)(m);
    mmEmuMapper241_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER241 =
{
    &__static_mmEmuMapper241_Produce,
    &__static_mmEmuMapper241_Recycle,
};
