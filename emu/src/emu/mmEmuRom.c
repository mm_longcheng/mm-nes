/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuRom.h"
#include "mmEmuNes.h"
#include "mmEmuIps.h"
#include "mmEmuRomPatch.h"
#include "mmEmuErrorCode.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "algorithm/mmCRC32.h"

MM_EXPORT_EMU void mmEmuRom_Init(struct mmEmuRom* p)
{
    p->nes = NULL;

    mmEmuRomNesHeader_Init(&p->nesheader);
    mmEmuRomNsfHeader_Init(&p->nsfheader);

    mmStreambuf_Init(&p->buffer_PRG);
    mmStreambuf_Init(&p->buffer_CHR);
    mmStreambuf_Init(&p->buffer_Trainer);
    mmStreambuf_Init(&p->buffer_DiskBios);
    mmStreambuf_Init(&p->buffer_Disk);

    // PROM CRC
    p->crcprom = 0;
    p->crcarom = 0;
    p->crcvrom = 0;

    p->fdsmakerID = 0;
    p->fdsgameID = 0;

    p->mapper = 0;
    p->diskno = 0;

    // For NSF
    p->bNSF = MM_FALSE;
    p->NSF_PAGE_SIZE = 0;
}
MM_EXPORT_EMU void mmEmuRom_Destroy(struct mmEmuRom* p)
{
    p->nes = NULL;

    mmEmuRomNesHeader_Destroy(&p->nesheader);
    mmEmuRomNsfHeader_Destroy(&p->nsfheader);

    mmStreambuf_Destroy(&p->buffer_PRG);
    mmStreambuf_Destroy(&p->buffer_CHR);
    mmStreambuf_Destroy(&p->buffer_Trainer);
    mmStreambuf_Destroy(&p->buffer_DiskBios);
    mmStreambuf_Destroy(&p->buffer_Disk);

    // PROM CRC
    p->crcprom = 0;
    p->crcarom = 0;
    p->crcvrom = 0;

    p->fdsmakerID = 0;
    p->fdsgameID = 0;

    p->mapper = 0;
    p->diskno = 0;

    // For NSF
    p->bNSF = MM_FALSE;
    p->NSF_PAGE_SIZE = 0;
}

MM_EXPORT_EMU void mmEmuRom_SetParent(struct mmEmuRom* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU int mmEmuRom_LoadBuffer(struct mmEmuRom* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    struct mmEmuAssets* assets = &p->nes->assets;
    //
    int code = MM_UNKNOWN;
    //
    struct mmByteBuffer rom;
    struct mmByteBuffer ips;
    struct mmByteBuffer sys;
    //
    mmByteBuffer_Init(&rom);
    mmByteBuffer_Init(&ips);
    mmByteBuffer_Init(&sys);
    //
    mmEmuAssets_AcquireFileByteBuffer(assets, mmString_CStr(&assets->szRomFile), &rom);
    mmEmuAssets_AcquireFileByteBuffer(assets, mmString_CStr(&assets->szIpsFile), &ips);
    mmEmuAssets_AcquireFileByteBuffer(assets, mmString_CStr(&assets->szDISKSYSFile), &sys);
    //
    do
    {
        mmByte_t* pROM = NULL;
        mmLong_t romsize = 0;
        mmUInt16_t rom_type = MM_EMU_ROM_FORMAT_NONE;

        mmUInt32_t PRGoffset = 0;
        mmUInt32_t CHRoffset = 0;
        mmUInt32_t PRGsize = 0;
        mmUInt32_t CHRsize = 0;

        if (0 == rom.length)
        {
            // There is no rom.
            mmLogger_LogE(gLogger, "%s %d There is no rom.", __FUNCTION__, __LINE__);
            code = MM_ERR_EMU_THERE_IS_NO_ROM;
            break;
        }

        pROM = (mmByte_t*)(rom.buffer + rom.offset);
        romsize = (mmLong_t)rom.length;
        //
        if (romsize < 17)
        {
            // File size is too small
            mmLogger_LogE(gLogger, "%s %d File size is too small.", __FUNCTION__, __LINE__);
            code = MM_ERR_EMU_FILE_SIZE_IS_TOO_SMALL;
            break;
        }
        
        mmMemcpy(&p->nesheader, pROM, sizeof(struct mmEmuRomNesHeader));

        rom_type = mmEmuRom_GetRomFormatType(rom.buffer, rom.offset, rom.length);

        if (MM_EMU_ROM_FORMAT_NONE == rom_type)
        {
            // Not supported format.
            mmLogger_LogE(gLogger, "%s %d Not supported format.", __FUNCTION__, __LINE__);
            code = MM_ERR_EMU_NOT_SUPPORTED_FORMAT;
            break;
        }

        // Since the zip/fds/nes is defrosted and raw, now apply the patch.
        if (0 != ips.length && p->nes->config.emulator.bAutoIPS)
        {
            mmByte_t* pIPS = (mmByte_t*)(ips.buffer + ips.offset);
            mmLong_t ipssize = (mmLong_t)(ips.length);

            mmByte_t* ipstemp = NULL;
            if (!(ipstemp = (mmByte_t*)mmMalloc(romsize)))
            {
                // Out of memory.
                mmLogger_LogE(gLogger, "%s %d out of memory.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_OUT_OF_MEMORY;
                break;
            }
            mmMemcpy(ipstemp, pROM, romsize);

            if (mmEmuIps_PatchIPS(pIPS, ipstemp, romsize, ipssize))
            {
                mmMemcpy(&p->nesheader, ipstemp, sizeof(struct mmEmuRomNesHeader));
                mmMemcpy(pROM, ipstemp, romsize);
            }

            if (NULL != ipstemp)
            {
                mmFree(ipstemp);
                ipstemp = NULL;
            }
        }

        {
            rom_type = mmEmuRom_GetRomFormatType(rom.buffer, rom.offset, rom.length);

            if (MM_EMU_ROM_FORMAT_NESA == rom_type)
            {
                p->bNSF = MM_FALSE;

                // common nes file.
                PRGsize = mmEmuRomNesHeader_GetPrgSize(&p->nesheader);
                CHRsize = mmEmuRomNesHeader_GetChrSize(&p->nesheader);
                PRGoffset = sizeof(struct mmEmuRomNesHeader);
                CHRoffset = PRGoffset + PRGsize;

                if (mmEmuRom_IsTRAINER(p))
                {
                    PRGoffset += 512;
                    CHRoffset += 512;
                }

                if (PRGsize <= 0 || (PRGsize + CHRsize) > (mmUInt32_t)romsize)
                {
                    // NES file invalid.
                    mmLogger_LogE(gLogger, "%s %d Invalid nes header.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_INVALID_NES_HEADER;
                    break;
                }

                // PRG BANK
                mmStreambuf_Reset(&p->buffer_PRG);
                mmStreambuf_AlignedMemory(&p->buffer_PRG, PRGsize);
                if (NULL == p->buffer_PRG.buff)
                {
                    // Out of memory.
                    mmLogger_LogE(gLogger, "%s %d out of memory.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_OUT_OF_MEMORY;
                    break;
                }

                mmStreambuf_Sputn(&p->buffer_PRG, (mmUInt8_t*)(pROM), (size_t)PRGoffset, (size_t)PRGsize);

                // CHR BANK
                if (CHRsize > 0)
                {
                    mmStreambuf_Reset(&p->buffer_CHR);
                    mmStreambuf_AlignedMemory(&p->buffer_CHR, CHRsize);
                    if (NULL == p->buffer_CHR.buff)
                    {
                        // Out of memory.
                        mmLogger_LogE(gLogger, "%s %d out of memory.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_OUT_OF_MEMORY;
                        break;
                    }

                    if ((mmUInt32_t)romsize >= CHRoffset + CHRsize)
                    {
                        mmStreambuf_Sputn(&p->buffer_CHR, (mmUInt8_t*)(pROM), (size_t)CHRoffset, (size_t)CHRsize);
                    }
                    else
                    {
                        // CHR bank less ...
                        CHRsize -= (CHRoffset + CHRsize - romsize);
                        mmStreambuf_Sputn(&p->buffer_CHR, (mmUInt8_t*)(pROM), (size_t)CHRoffset, (size_t)CHRsize);
                    }
                }
                else
                {
                    mmStreambuf_Reset(&p->buffer_CHR);
                }

                // Trainer
                if (mmEmuRom_IsTRAINER(p))
                {
                    mmStreambuf_Reset(&p->buffer_Trainer);
                    mmStreambuf_AlignedMemory(&p->buffer_Trainer, 512);
                    if (NULL == p->buffer_Trainer.buff)
                    {
                        // Out of memory.
                        mmLogger_LogE(gLogger, "%s %d out of memory.", __FUNCTION__, __LINE__);
                        code = MM_ERR_EMU_OUT_OF_MEMORY;
                        break;
                    }

                    mmStreambuf_Sputn(&p->buffer_Trainer, (mmUInt8_t*)(pROM), (size_t)sizeof(struct mmEmuRomNesHeader), (size_t)512);
                }
                else
                {
                    mmStreambuf_Reset(&p->buffer_Trainer);
                }

                mmEmuRomNesHeader_LoggerInformation(&p->nesheader, gLogger);
            }
            else if (MM_EMU_ROM_FORMAT_FDSA == rom_type)
            {
                mmByte_t* bios = NULL;

                p->bNSF = MM_FALSE;

                if (0 == sys.length)
                {
                    // There is no disk bios.
                    mmLogger_LogE(gLogger, "%s %d no disk bios.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_THERE_IS_NO_DISK_BIOS;
                    break;
                }

                bios = (mmByte_t*)(sys.buffer + sys.offset);

                // FDS(Nintendo Disk System)
                // Disk size.
                p->diskno = p->nesheader.PRG_PAGE_SIZE;

                if (romsize < (16 + 65500 * p->diskno))
                {
                    // illegal disk size.
                    mmLogger_LogE(gLogger, "%s %d illegal disk size.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_ILLEGAL_DISK_SIZE;
                    break;
                }
                if (p->diskno > 8)
                {
                    // Unsupport disk.
                    // Disks with more than 8 sides are not supported.
                    mmLogger_LogE(gLogger, "%s %d unsupport disk.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_NOT_SUPPORTED_DISK;
                    break;
                }

                mmMemset(&p->nesheader, 0, sizeof(struct mmEmuRomNesHeader));

                // Make a dummy header.
                p->nesheader.ID[0] = 'N';
                p->nesheader.ID[1] = 'E';
                p->nesheader.ID[2] = 'S';
                p->nesheader.ID[3] = 0x1A;
                p->nesheader.PRG_PAGE_SIZE = (mmByte_t)p->diskno * 4;
                p->nesheader.CHR_PAGE_SIZE = 0;
                p->nesheader.control_06 = 0x40;
                p->nesheader.control_07 = 0x10;

                PRGsize = (mmUInt32_t)(sizeof(struct mmEmuRomNesHeader) + 65500 * (mmLong_t)p->diskno);
                // PRG BANK
                mmStreambuf_Reset(&p->buffer_PRG);
                mmStreambuf_AlignedMemory(&p->buffer_PRG, PRGsize);
                if (NULL == p->buffer_PRG.buff)
                {
                    // Out of memory.
                    mmLogger_LogE(gLogger, "%s %d out of memory.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_OUT_OF_MEMORY;
                    break;
                }

                // For data backup.
                mmStreambuf_Reset(&p->buffer_Disk);
                mmStreambuf_AlignedMemory(&p->buffer_Disk, PRGsize);
                if (NULL == p->buffer_Disk.buff)
                {
                    // Out of memory.
                    mmLogger_LogE(gLogger, "%s %d out of memory.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_OUT_OF_MEMORY;
                    break;
                }

                // CHR BANK
                mmStreambuf_Reset(&p->buffer_CHR);

                mmStreambuf_Sputn(&p->buffer_PRG, (mmUInt8_t*)(&p->nesheader), (size_t)0, (size_t)sizeof(struct mmEmuRomNesHeader));
                mmStreambuf_Sputn(&p->buffer_PRG, (mmUInt8_t*)(pROM), (size_t)sizeof(struct mmEmuRomNesHeader), (size_t)(65500 * (mmLong_t)p->diskno));
                // For specifying data rewriting location.
                mmMemset((p->buffer_Disk.buff + p->buffer_Disk.gptr), 0, PRGsize);

                {
                    mmByte_t* lpPRG = NULL;

                    lpPRG = p->buffer_PRG.buff + p->buffer_PRG.gptr;

                    lpPRG[0] = 'F';
                    lpPRG[1] = 'D';
                    lpPRG[2] = 'S';
                    lpPRG[3] = 0x1A;
                    lpPRG[4] = (mmByte_t)p->diskno;
                }

                if (sys.length < 17)
                {
                    // File size is too small.
                    mmLogger_LogE(gLogger, "%s %d File size is too small.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_FILE_SIZE_IS_TOO_SMALL;
                    break;
                }

                // DiskBios
                mmStreambuf_Reset(&p->buffer_DiskBios);
                mmStreambuf_AlignedMemory(&p->buffer_DiskBios, 8 * 1024);
                if (NULL == p->buffer_DiskBios.buff)
                {
                    // Out of memory.
                    mmLogger_LogE(gLogger, "%s %d out of memory.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_OUT_OF_MEMORY;
                    break;
                }

                if (bios[0] == 'N' && bios[1] == 'E' && bios[2] == 'S' && bios[3] == 0x1A)
                {
                    // NES format BIOS
                    mmStreambuf_Sputn(&p->buffer_DiskBios, (mmUInt8_t*)(bios), (size_t)0x6010, (size_t)8 * 1024);
                }
                else
                {
                    // original BIOS
                    mmStreambuf_Sputn(&p->buffer_DiskBios, (mmUInt8_t*)(bios), (size_t)0, (size_t)8 * 1024);
                }
            }
            else if (MM_EMU_ROM_FORMAT_NSFM == rom_type)
            {
                // NSF
                p->bNSF = MM_TRUE;
                mmMemset(&p->nesheader, 0, sizeof(struct mmEmuRomNesHeader));

                // Header copy.
                mmMemcpy(&p->nsfheader, pROM, sizeof(struct mmEmuRomNsfHeader));

                PRGsize = (mmUInt32_t)(romsize - sizeof(struct mmEmuRomNsfHeader));
                mmLogger_LogD(gLogger, "%s %d PRGSIZE: %u.", __FUNCTION__, __LINE__, PRGsize);
                PRGsize = (PRGsize + 0x0FFF)&~0x0FFF;
                mmLogger_LogD(gLogger, "%s %d PRGSIZE: %u.", __FUNCTION__, __LINE__, PRGsize);

                mmStreambuf_Reset(&p->buffer_PRG);
                mmStreambuf_AlignedMemory(&p->buffer_PRG, PRGsize);
                if (NULL == p->buffer_PRG.buff)
                {
                    // Out of memory.
                    mmLogger_LogE(gLogger, "%s %d out of memory.", __FUNCTION__, __LINE__);
                    code = MM_ERR_EMU_OUT_OF_MEMORY;
                    break;
                }

                mmMemset((p->buffer_PRG.buff + p->buffer_PRG.gptr), 0, PRGsize);
                mmStreambuf_Sputn(&p->buffer_PRG, (mmUInt8_t*)(pROM), (size_t)sizeof(struct mmEmuRomNsfHeader), (size_t)romsize - sizeof(struct mmEmuRomNsfHeader));

                p->NSF_PAGE_SIZE = PRGsize >> 12;
                mmLogger_LogD(gLogger, "%s %d PAGESIZE: %d.", __FUNCTION__, __LINE__, p->NSF_PAGE_SIZE);

                mmEmuRomNsfHeader_LoggerInformation(&p->nsfheader, gLogger);
            }
            else
            {
                p->bNSF = MM_FALSE;

                // Not supported format.
                mmLogger_LogE(gLogger, "%s %d Not supported format.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_NOT_SUPPORTED_FORMAT;
                break;
            }
        }

        // Mapper setting.
        if (!p->bNSF)
        {
            p->mapper = mmEmuRomNesHeader_GetMapperID(&p->nesheader);
            p->crcprom = p->crcarom = p->crcvrom = 0;

            if (p->mapper != 20)
            {
                // Calculation of PRG crc (same as PRG CRC of NesToy)
                if (mmEmuRom_IsTRAINER(p))
                {
                    p->crcarom = mmCRC32C(pROM + sizeof(struct mmEmuRomNesHeader), (int)(512 + PRGsize + CHRsize), 0);
                    p->crcprom = mmCRC32C(pROM + sizeof(struct mmEmuRomNesHeader), (int)(512 + PRGsize), 0);
                    if (CHRsize)
                    {
                        p->crcvrom = mmCRC32C(pROM + PRGsize + 512 + sizeof(struct mmEmuRomNesHeader), (int)(CHRsize), 0);
                    }
                }
                else
                {
                    p->crcarom = mmCRC32C(pROM + sizeof(struct mmEmuRomNesHeader), (int)(PRGsize + CHRsize), 0);
                    p->crcprom = mmCRC32C(pROM + sizeof(struct mmEmuRomNesHeader), (int)(PRGsize), 0);
                    if (CHRsize)
                    {
                        p->crcvrom = mmCRC32C(pROM + PRGsize + sizeof(struct mmEmuRomNesHeader), (int)(CHRsize), 0);
                    }
                }

                //romdatabase.HeaderCorrect(p->nesheader, p->crcall, p->crc);

                // rom patch.
                mmEmuRomPatch_Apply(p);

                p->fdsmakerID = p->fdsgameID = 0;
            }
            else
            {
                mmByte_t* lpPRG = p->buffer_PRG.buff + p->buffer_PRG.gptr;

                p->crcprom = p->crcarom = p->crcvrom = 0;

                p->fdsmakerID = lpPRG[0x1F];
                p->fdsgameID = (lpPRG[0x20] << 24) | (lpPRG[0x21] << 16) | (lpPRG[0x22] << 8) | (lpPRG[0x23] << 0);
            }
        }
        else
        {
            // NSF
            p->mapper = 0x0100; // Private mapper
            p->crcprom = p->crcarom = p->crcvrom = 0;
            p->fdsmakerID = p->fdsgameID = 0;
        }

        // success.
        code = MM_SUCCESS;

    } while (0);
    //
    mmEmuAssets_ReleaseFileByteBuffer(assets, &rom);
    mmEmuAssets_ReleaseFileByteBuffer(assets, &ips);
    mmEmuAssets_ReleaseFileByteBuffer(assets, &sys);

    mmByteBuffer_Destroy(&rom);
    mmByteBuffer_Destroy(&ips);
    mmByteBuffer_Destroy(&sys);

    return code;
}

MM_EXPORT_EMU mmInt_t mmEmuRom_GetVideoMode(const struct mmEmuRom* p)
{
    mmInt_t nVideoMode = MM_EMU_NES_MODEL_NTSC;

    mmUInt16_t GameSystem = mmEmuRomNesHeader_GetGameSystem(&p->nesheader);

    switch (GameSystem)
    {
    case MM_EMU_GAMESYSTEM_NESNTSC:
        nVideoMode = MM_EMU_NES_MODEL_NTSC;
        break;
    case MM_EMU_GAMESYSTEM_NESPAL:
        nVideoMode = MM_EMU_NES_MODEL_PAL;
        break;
    case MM_EMU_GAMESYSTEM_DENDY:
        nVideoMode = MM_EMU_NES_MODEL_DENDY;
        break;
    default:
        nVideoMode = MM_EMU_NES_MODEL_NTSC;
        break;
    }
    return nVideoMode;
}

MM_EXPORT_EMU mmInt_t mmEmuRom_IsRomBuffer(const char* buffer, size_t offset, size_t length)
{
    mmInt_t code = MM_FAILURE;
    mmByte_t* ID = (mmByte_t*)(buffer + offset);
    do
    {
        if (length < 17)
        {
            code = MM_FAILURE;
            break;
        }
        if (ID[0] == 'N' && ID[1] == 'E' && ID[2] == 'S' && ID[3] == 0x1A)
        {
            code = MM_SUCCESS;
            break;
        }
        else if (ID[0] == 'F' && ID[1] == 'D' && ID[2] == 'S' && ID[3] == 0x1A)
        {
            code = MM_SUCCESS;
            break;
        }
        else if (ID[0] == 'N' && ID[1] == 'E' && ID[2] == 'S' && ID[3] == 'M')
        {
            code = MM_SUCCESS;
            break;
        }
        else
        {
            code = MM_FAILURE;
            break;
        }
    } while (0);
    return code;
}
MM_EXPORT_EMU mmUInt16_t mmEmuRom_GetRomFormatType(const mmUInt8_t* buffer, size_t offset, size_t length)
{
    mmByte_t* ID = (mmByte_t*)(buffer + offset);

    if (ID[0] == 'N' && ID[1] == 'E' && ID[2] == 'S' && ID[3] == 0x1A)
    {
        return MM_EMU_ROM_FORMAT_NESA;
    }
    else if (ID[0] == 'F' && ID[1] == 'D' && ID[2] == 'S' && ID[3] == 0x1A)
    {
        return MM_EMU_ROM_FORMAT_FDSA;
    }
    else if (ID[0] == 'N' && ID[1] == 'E' && ID[2] == 'S' && ID[3] == 'M')
    {
        return MM_EMU_ROM_FORMAT_NSFM;
    }
    else
    {
        return MM_EMU_ROM_FORMAT_NONE;
    }
}

