/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper000.h"
#include "emu/mmEmuNes.h"

/*
*
* http://wiki.nesdev.com/w/index.php/NROM
* 
* NROM
*
*     -------------------------------------------------------------------
*     Company               | Nintendo, others
*     Boards                | NROM, HROM*, RROM, RTROM, SROM, STROM
*     PRG ROM capacity      | 16K or 32K
*     PRG ROM window        | n/a
*     PRG RAM capacity      | 2K or 4K in Family Basic only
*     PRG RAM window        | n/a
*     CHR capacity          | 8K
*     CHR window            | n/a
*     Nametable mirroring   | Fixed H or V, controlled by solder pads (*V only)
*     Bus conflicts         | Yes
*     IRQ                   | No
*     Audio                 | No
*     iNES mappers          | 000
*     -------------------------------------------------------------------
*
* Overview
*
*     PRG ROM size: 16 KiB for NROM-128, 32 KiB for NROM-256 (DIP-28 standard pinout)
*     PRG ROM bank size: Not bankswitched
*     PRG RAM: 2 or 4 KiB, not bankswitched, only in Family Basic (but most emulators provide 8)
*     CHR capacity: 8 KiB ROM (DIP-28 standard pinout) but most emulators support RAM
*     CHR bank size: Not bankswitched, see CNROM
*     Nametable mirroring: Solder pads select vertical or horizontal mirroring
*     Subject to bus conflicts: Yes, but irrelevant
*
* Banks
*   All Banks are fixed,
*
*     CPU $6000-$7FFF: Family Basic only: PRG RAM, mirrored as necessary to fill entire 8 KiB window, write protectable with an external switch
*     CPU $8000-$BFFF: First 16 KB of ROM.
*     CPU $C000-$FFFF: Last 16 KB of ROM (NROM-256) or mirror of $8000-$BFFF (NROM-128).
*
*/

MM_EXPORT_EMU void mmEmuMapper000_Init(struct mmEmuMapper000* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper000_Reset;
    //p->super.Write = &mmEmuMapper000_Write;
    //p->super.Read = &mmEmuMapper000_Read;
    //p->super.ReadLow = &mmEmuMapper000_ReadLow;
    //p->super.WriteLow = &mmEmuMapper000_WriteLow;
    //p->super.ExRead = &mmEmuMapper000_ExRead;
    //p->super.ExWrite = &mmEmuMapper000_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper000_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper000_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper000_HSync;
    //p->super.VSync = &mmEmuMapper000_VSync;
    //p->super.Clock = &mmEmuMapper000_Clock;
    //p->super.PPULatch = &mmEmuMapper000_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper000_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper000_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper000_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper000_IsStateSave;
    //p->super.SaveState = &mmEmuMapper000_SaveState;
    //p->super.LoadState = &mmEmuMapper000_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper000_Destroy(struct mmEmuMapper000* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper000_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper000* p = (struct mmEmuMapper000*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    switch (mmu->PROM_16K_SIZE)
    {
    default:
    case 1: // 16K only
        mmEmuMmu_SetPROM16KBank(mmu, 4, 0);
        mmEmuMmu_SetPROM16KBank(mmu, 6, 0);
        break;
    case 2: // 32K
        mmEmuMmu_SetPROM32KBank(mmu, 0);
        break;
    }

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
    else
    {
        mmEmuMmu_SetCRAM8KBank(mmu, 0);
    }

    crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);
    if (crc == 0x4e7db5af)      // Circus Charlie(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
    if (crc == 0x57970078)      // F-1 Race(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
    if (crc == 0xaf2bbcbc ||    // Mach Rider(JU)
        crc == 0x3acd4bf1)      // Mach Rider(Alt)(JU)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper000_Produce(void)
{
    struct mmEmuMapper000* p = (struct mmEmuMapper000*)mmMalloc(sizeof(struct mmEmuMapper000));
    mmEmuMapper000_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper000_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper000* p = (struct mmEmuMapper000*)(m);
    mmEmuMapper000_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER000 =
{
    &__static_mmEmuMapper000_Produce,
    &__static_mmEmuMapper000_Recycle,
};
