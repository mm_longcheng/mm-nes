/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuPadOekakidsTablet_h__
#define __mmEmuPadOekakidsTablet_h__

#include "core/mmCore.h"

#include "emu/mmEmuExpad.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Oekakids Tablet                                                      //
//----------------------------------------------------------------------//
struct mmEmuPadOekakidsTablet
{
    struct mmEmuExpad super;

    mmByte_t    olddata;
    mmByte_t    outbits;
    mmUInt32_t  databits;

    mmLong_t    zapper_x, zapper_y;
    mmByte_t    zapper_button;
};

MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Init(struct mmEmuPadOekakidsTablet* p);
MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Destroy(struct mmEmuPadOekakidsTablet* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Reset(struct mmEmuExpad* super);

MM_EXPORT_EMU mmByte_t mmEmuPadOekakidsTablet_Read4017(struct mmEmuExpad* super);
MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Write4016(struct mmEmuExpad* super, mmByte_t data);

MM_EXPORT_EMU void mmEmuPadOekakidsTablet_Sync(struct mmEmuExpad* super);
MM_EXPORT_EMU void mmEmuPadOekakidsTablet_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data);
MM_EXPORT_EMU mmLong_t mmEmuPadOekakidsTablet_GetSyncData(const struct mmEmuExpad* super, mmInt_t type);

MM_EXPORT_EMU extern const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_OEKAKIDSTABLET;

#include "core/mmSuffix.h"

#endif//__mmEmuPadOekakidsTablet_h__
