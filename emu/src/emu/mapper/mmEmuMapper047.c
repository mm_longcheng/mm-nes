/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper047.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper047_Init(struct mmEmuMapper047* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->patch = 0;
    p->bank = 0;

    p->prg0 = 0;
    p->prg1 = 0;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    //
    p->super.Reset = &mmEmuMapper047_Reset;
    p->super.Write = &mmEmuMapper047_Write;
    //p->super.Read = &mmEmuMapper047_Read;
    //p->super.ReadLow = &mmEmuMapper047_ReadLow;
    p->super.WriteLow = &mmEmuMapper047_WriteLow;
    //p->super.ExRead = &mmEmuMapper047_ExRead;
    //p->super.ExWrite = &mmEmuMapper047_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper047_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper047_ExCmdWrite;
    p->super.HSync = &mmEmuMapper047_HSync;
    //p->super.VSync = &mmEmuMapper047_VSync;
    //p->super.Clock = &mmEmuMapper047_Clock;
    //p->super.PPULatch = &mmEmuMapper047_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper047_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper047_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper047_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper047_IsStateSave;
    p->super.SaveState = &mmEmuMapper047_SaveState;
    p->super.LoadState = &mmEmuMapper047_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper047_Destroy(struct mmEmuMapper047* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->patch = 0;
    p->bank = 0;

    p->prg0 = 0;
    p->prg1 = 0;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}

MM_EXPORT_EMU void mmEmuMapper047_SetBankCPU(struct mmEmuMapper047* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->bank * 8 + ((p->patch && p->bank != 2) ? 6 : 14));
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->bank * 8 + p->prg1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, p->bank * 8 + p->prg0);
        mmEmuMmu_SetPROM8KBank(mmu, 7, p->bank * 8 + ((p->patch && p->bank != 2) ? 7 : 15));
    }
    else
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, p->bank * 8 + p->prg0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, p->bank * 8 + p->prg1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, p->bank * 8 + ((p->patch && p->bank != 2) ? 6 : 14));
        mmEmuMmu_SetPROM8KBank(mmu, 7, p->bank * 8 + ((p->patch && p->bank != 2) ? 7 : 15));
    }
}
MM_EXPORT_EMU void mmEmuMapper047_SetBankPPU(struct mmEmuMapper047* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, (p->bank & 0x02) * 64 + p->chr4);
            mmEmuMmu_SetVROM1KBank(mmu, 1, (p->bank & 0x02) * 64 + p->chr5);
            mmEmuMmu_SetVROM1KBank(mmu, 2, (p->bank & 0x02) * 64 + p->chr6);
            mmEmuMmu_SetVROM1KBank(mmu, 3, (p->bank & 0x02) * 64 + p->chr7);
            mmEmuMmu_SetVROM1KBank(mmu, 4, (p->bank & 0x02) * 64 + p->chr01 + 0);
            mmEmuMmu_SetVROM1KBank(mmu, 5, (p->bank & 0x02) * 64 + p->chr01 + 1);
            mmEmuMmu_SetVROM1KBank(mmu, 6, (p->bank & 0x02) * 64 + p->chr23 + 0);
            mmEmuMmu_SetVROM1KBank(mmu, 7, (p->bank & 0x02) * 64 + p->chr23 + 1);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, (p->bank & 0x02) * 64 + p->chr01 + 0);
            mmEmuMmu_SetVROM1KBank(mmu, 1, (p->bank & 0x02) * 64 + p->chr01 + 1);
            mmEmuMmu_SetVROM1KBank(mmu, 2, (p->bank & 0x02) * 64 + p->chr23 + 0);
            mmEmuMmu_SetVROM1KBank(mmu, 3, (p->bank & 0x02) * 64 + p->chr23 + 1);
            mmEmuMmu_SetVROM1KBank(mmu, 4, (p->bank & 0x02) * 64 + p->chr4);
            mmEmuMmu_SetVROM1KBank(mmu, 5, (p->bank & 0x02) * 64 + p->chr5);
            mmEmuMmu_SetVROM1KBank(mmu, 6, (p->bank & 0x02) * 64 + p->chr6);
            mmEmuMmu_SetVROM1KBank(mmu, 7, (p->bank & 0x02) * 64 + p->chr7);
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper047_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper047* p = (struct mmEmuMapper047*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    p->patch = 0;

    if (crc == 0x7eef434c)
    {
        p->patch = 1;
    }

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->bank = 0;
    p->prg0 = 0;
    p->prg1 = 1;

    // set VROM banks
    if (mmu->VROM_1K_SIZE)
    {
        p->chr01 = 0;
        p->chr23 = 2;
        p->chr4 = 4;
        p->chr5 = 5;
        p->chr6 = 6;
        p->chr7 = 7;
    }
    else
    {
        p->chr01 = p->chr23 = p->chr4 = p->chr5 = p->chr6 = p->chr7 = 0;
    }

    mmEmuMapper047_SetBankCPU(p);
    mmEmuMapper047_SetBankPPU(p);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}
MM_EXPORT_EMU void mmEmuMapper047_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper047* p = (struct mmEmuMapper047*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper047_SetBankCPU(p);
        mmEmuMapper047_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;
        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr01 = data & 0xFE;
            mmEmuMapper047_SetBankPPU(p);
            break;
        case 0x01:
            p->chr23 = data & 0xFE;
            mmEmuMapper047_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper047_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper047_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper047_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper047_SetBankPPU(p);
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper047_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper047_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        break;
    case 0xC000:
        p->reg[4] = data;
        p->irq_counter = data;
        break;
    case 0xC001:
        p->reg[5] = data;
        p->irq_latch = data;
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 1;
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper047_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper047* p = (struct mmEmuMapper047*)(super);

    if (addr == 0x6000)
    {
        if (p->patch)
        {
            p->bank = (data & 0x06) >> 1;
        }
        else
        {
            p->bank = (data & 0x01) << 1;
        }
        mmEmuMapper047_SetBankCPU(p);
        mmEmuMapper047_SetBankPPU(p);
    }
}

MM_EXPORT_EMU void mmEmuMapper047_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper047* p = (struct mmEmuMapper047*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(--p->irq_counter))
                {
                    p->irq_counter = p->irq_latch;
                    //mm_emu_cpu_IRQ(&nes->cpu);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper047_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper047_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper047* p = (struct mmEmuMapper047*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
    buffer[16] = p->irq_enable;
    buffer[17] = p->irq_counter;
    buffer[18] = p->irq_latch;
    buffer[19] = p->bank;
}
MM_EXPORT_EMU void mmEmuMapper047_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper047* p = (struct mmEmuMapper047*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
    p->irq_enable = buffer[16];
    p->irq_counter = buffer[17];
    p->irq_latch = buffer[18];
    p->bank = buffer[19];
}

static struct mmEmuMapper* __static_mmEmuMapper047_Produce(void)
{
    struct mmEmuMapper047* p = (struct mmEmuMapper047*)mmMalloc(sizeof(struct mmEmuMapper047));
    mmEmuMapper047_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper047_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper047* p = (struct mmEmuMapper047*)(m);
    mmEmuMapper047_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER047 =
{
    &__static_mmEmuMapper047_Produce,
    &__static_mmEmuMapper047_Recycle,
};
