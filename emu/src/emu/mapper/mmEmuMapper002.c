/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper002.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper002_Init(struct mmEmuMapper002* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;
    //
    p->super.Reset = &mmEmuMapper002_Reset;
    p->super.Write = &mmEmuMapper002_Write;
    //p->super.Read = &mmEmuMapper002_Read;
    //p->super.ReadLow = &mmEmuMapper002_ReadLow;
    p->super.WriteLow = &mmEmuMapper002_WriteLow;
    //p->super.ExRead = &mmEmuMapper002_ExRead;
    //p->super.ExWrite = &mmEmuMapper002_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper002_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper002_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper002_HSync;
    //p->super.VSync = &mmEmuMapper002_VSync;
    //p->super.Clock = &mmEmuMapper002_Clock;
    //p->super.PPULatch = &mmEmuMapper002_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper002_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper002_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper002_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper002_IsStateSave;
    //p->super.SaveState = &mmEmuMapper002_SaveState;
    //p->super.LoadState = &mmEmuMapper002_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper002_Destroy(struct mmEmuMapper002* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;
}

MM_EXPORT_EMU void mmEmuMapper002_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper002* p = (struct mmEmuMapper002*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);

    //if (crc == 0x322c9b09)            // Metal Gear (Alt)(J)
    //{
    //  mmEmuNes_SetFrameIRQmode(nes, MM_FALSE);
    //}
    //if (crc == 0xe7a3867b)            // Dragon Quest 2(Alt)(J)
    //{
    //  mmEmuNes_SetFrameIRQmode(nes, MM_FALSE);
    //}
    //if (crc == 0x9622fbd9)            // Ballblazer(J)
    //{
    //  p->patch = 0;
    //}

    if (crc == 0x8c3d54e8 ||        // Ikari(J)
        crc == 0x655efeed ||        // Ikari Warriors(U)
        crc == 0x538218b2)          // Ikari Warriors(E)
    {
        p->patch = 1;
    }

    if (crc == 0xb20c1030)          // Shanghai(J)(original)
    {
        p->patch = 2;
    }
}
MM_EXPORT_EMU void mmEmuMapper002_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper002* p = (struct mmEmuMapper002*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (!mmEmuRom_IsBATTERY(&nes->rom))
    {
        if (addr >= 0x5000 && p->patch == 1)
        {
            mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        }
    }
    else
    {
        // Mapper::WriteLow(addr, data);
        mmEmuMapper_WriteLow(super, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper002_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper002* p = (struct mmEmuMapper002*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->patch != 2)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
    }
    else
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, data >> 4);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper002_Produce(void)
{
    struct mmEmuMapper002* p = (struct mmEmuMapper002*)mmMalloc(sizeof(struct mmEmuMapper002));
    mmEmuMapper002_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper002_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper002* p = (struct mmEmuMapper002*)(m);
    mmEmuMapper002_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER002 =
{
    &__static_mmEmuMapper002_Produce,
    &__static_mmEmuMapper002_Recycle,
};
