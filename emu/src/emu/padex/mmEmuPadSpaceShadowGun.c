/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadSpaceShadowGun.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuPad.h"

MM_EXPORT_EMU void mmEmuPadSpaceShadowGun_Init(struct mmEmuPadSpaceShadowGun* p)
{
    mmEmuExpad_Init(&p->super);

    p->zapper_x = 0;
    p->zapper_y = 0;
    p->bomb_bits = 0;
    p->zapper_button = 0;

    //
    p->super.Reset = &mmEmuPadSpaceShadowGun_Reset;
    p->super.Strobe = &mmEmuPadSpaceShadowGun_Strobe;
    p->super.Read4016 = &mmEmuPadSpaceShadowGun_Read4016;
    p->super.Read4017 = &mmEmuPadSpaceShadowGun_Read4017;
    //p->super.Write4016 = &mmEmuPadSpaceShadowGun_Write4016;
    //p->super.Write4017 = &mmEmuPadSpaceShadowGun_Write4017;
    p->super.Sync = &mmEmuPadSpaceShadowGun_Sync;
    p->super.SetSyncData = &mmEmuPadSpaceShadowGun_SetSyncData;
    p->super.GetSyncData = &mmEmuPadSpaceShadowGun_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadSpaceShadowGun_Destroy(struct mmEmuPadSpaceShadowGun* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->zapper_x = 0;
    p->zapper_y = 0;
    p->bomb_bits = 0;
    p->zapper_button = 0;
}

MM_EXPORT_EMU void mmEmuPadSpaceShadowGun_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)(super);

    p->bomb_bits = 0;
    p->zapper_button = 0;
}
MM_EXPORT_EMU void mmEmuPadSpaceShadowGun_Strobe(struct mmEmuExpad* super)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)(super);

    struct mmEmuPad* pad = &p->super.nes->pad;

    p->bomb_bits = pad->pad1bit & 0xFC;
    p->bomb_bits |= p->zapper_button & 0x02;
}
MM_EXPORT_EMU mmByte_t mmEmuPadSpaceShadowGun_Read4016(struct mmEmuExpad* super)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)(super);

    mmByte_t data = (p->bomb_bits & 0x01) << 1;
    p->bomb_bits >>= 1;
    return  data;
}
MM_EXPORT_EMU mmByte_t mmEmuPadSpaceShadowGun_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)(super);

    mmByte_t data = 0x08;

    if (p->zapper_button & 0x01)
    {
        data |= 0x10;
    }

    if (mmEmuNes_GetZapperHit(p->super.nes))
    {
        if (mmEmuScreen_GetZapperHit(&p->super.nes->screen) >= 0xFE)
        {
            data &= ~0x08;
        }
    }
    return  data;
}

MM_EXPORT_EMU void mmEmuPadSpaceShadowGun_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    mmEmuNes_GetZapperPos(p->super.nes, &p->zapper_x, &p->zapper_y);

    p->zapper_button = 0;
    if (mmEmuController_MouseButtonStatus(controller, MM_MB_LBUTTON))
    {
        p->zapper_button |= 0x01;
    }
    if (mmEmuController_MouseButtonStatus(controller, MM_MB_RBUTTON))
    {
        p->zapper_button |= 0x02;
    }
}
MM_EXPORT_EMU void mmEmuPadSpaceShadowGun_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)(super);

    if (type == 0)
    {
        p->zapper_x = data;
    }
    else if (type == 1)
    {
        p->zapper_y = data;
    }
    else if (type == 2)
    {
        p->zapper_button = (mmByte_t)data;
    }
}
MM_EXPORT_EMU mmLong_t mmEmuPadSpaceShadowGun_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)(super);

    mmLong_t data = 0;

    if (type == 0)
    {
        data = p->zapper_x;
    }
    else if (type == 1)
    {
        data = p->zapper_y;
    }
    else if (type == 2)
    {
        data = p->zapper_button;
    }
    return data;
}

static struct mmEmuExpad* __static_mmEmuPadSpaceShadowGun_Produce(void)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)mmMalloc(sizeof(struct mmEmuPadSpaceShadowGun));
    mmEmuPadSpaceShadowGun_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadSpaceShadowGun_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadSpaceShadowGun* p = (struct mmEmuPadSpaceShadowGun*)(m);
    mmEmuPadSpaceShadowGun_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_SPACESHADOWGUN =
{
    &__static_mmEmuPadSpaceShadowGun_Produce,
    &__static_mmEmuPadSpaceShadowGun_Recycle,
};
