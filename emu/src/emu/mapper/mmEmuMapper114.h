/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper114_h__
#define __mmEmuMapper114_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper114                                                            //
//----------------------------------------------------------------------//
struct mmEmuMapper114
{
    struct mmEmuMapper super;

    mmByte_t    reg_m, reg_a, reg_b[8];
    mmByte_t    reg_c;
    mmByte_t    irq_counter;
    mmByte_t    irq_occur;
};

MM_EXPORT_EMU void mmEmuMapper114_Init(struct mmEmuMapper114* p);
MM_EXPORT_EMU void mmEmuMapper114_Destroy(struct mmEmuMapper114* p);

MM_EXPORT_EMU void mmEmuMapper114_SetBankCPU(struct mmEmuMapper114* p);
MM_EXPORT_EMU void mmEmuMapper114_SetBankPPU(struct mmEmuMapper114* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper114_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper114_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper114_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper114_Clock(struct mmEmuMapper* super, mmInt_t cycles);
MM_EXPORT_EMU void mmEmuMapper114_HSync(struct mmEmuMapper* super, mmInt_t scanline);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper114_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper114_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper114_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER114;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper114_h__
