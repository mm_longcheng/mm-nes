/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper093.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper093_Init(struct mmEmuMapper093* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper093_Reset;
    //p->super.Write = &mmEmuMapper093_Write;
    //p->super.Read = &mmEmuMapper093_Read;
    //p->super.ReadLow = &mmEmuMapper093_ReadLow;
    p->super.WriteLow = &mmEmuMapper093_WriteLow;
    //p->super.ExRead = &mmEmuMapper093_ExRead;
    //p->super.ExWrite = &mmEmuMapper093_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper093_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper093_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper093_HSync;
    //p->super.VSync = &mmEmuMapper093_VSync;
    //p->super.Clock = &mmEmuMapper093_Clock;
    //p->super.PPULatch = &mmEmuMapper093_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper093_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper093_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper093_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper093_IsStateSave;
    //p->super.SaveState = &mmEmuMapper093_SaveState;
    //p->super.LoadState = &mmEmuMapper093_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper093_Destroy(struct mmEmuMapper093* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper093_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper093* p = (struct mmEmuMapper093*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper093_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper093* p = (struct mmEmuMapper093*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0x6000)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper093_Produce(void)
{
    struct mmEmuMapper093* p = (struct mmEmuMapper093*)mmMalloc(sizeof(struct mmEmuMapper093));
    mmEmuMapper093_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper093_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper093* p = (struct mmEmuMapper093*)(m);
    mmEmuMapper093_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER093 =
{
    &__static_mmEmuMapper093_Produce,
    &__static_mmEmuMapper093_Recycle,
};
