:: copylibs.bat 32 x86 Debug   "_d"
:: copylibs.bat 32 x86 Release ""
:: copylibs.bat 64 x64 Debug   "_d"
:: copylibs.bat 64 x64 Release ""

@echo off

@set OPTION=/s /e /h /d /y

call :cp-libs %3 %4 %1 %2
:: call datafiles-mslink.bat %3

GOTO:EOF

:: cp-libs Debug _d 32 x86
:cp-libs
call :cp-lib  mm-libx %~1   pthread        libpthread_shared%~2.dll
call :cp-lib  mm-libx %~1   zlib           libz_shared%~2.dll
call :cp-lib  mm-libx %~1   zlib           libminizip_shared%~2.dll
call :cp-lib  mm-libx %~1   openal-soft    libOpenAL_shared%~2.dll

call :cp-core mm-core %~1   mm             libmm_core_shared%~2.dll
call :cp-core mm-core %~1   dish           libmm_dish_shared%~2.dll

:: call :cp-file launch-binary.config %~1     launch.config

GOTO:EOF

:: cp-lib mm-lib Debug pthread libpthread_shared_d.dll
:cp-lib
@xcopy %MM_HOME%\\%~1\\build\\%~3\\proj_windows\\bin\\%~2\%~4 bin\\%~2\* %OPTION%   1>nul
GOTO:EOF

:: cp-core mm-core Debug mm libmm_core_shared_d.dll
:cp-core
@xcopy %MM_HOME%\\%~1\\%~3\\proj\\windows\\bin\\%~2\%~4       bin\\%~2\* %OPTION%   1>nul
GOTO:EOF

:: cp-sdk mm-libx Debug vulkan x64 vulkan-1.dll
:cp-sdk
@xcopy %MM_HOME%\\%~1\\sdk\\%~3\\bin\\windows\\%~4\%~5        bin\\%~2\* %OPTION%   1>nul
GOTO:EOF

:: cp-file %~1 Debug launch.config
:cp-file
@echo f | @xcopy %~1                                          bin\\%~2\%~3 /h /d /y 1>nul
GOTO:EOF

