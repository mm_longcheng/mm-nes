/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper094.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper094_Init(struct mmEmuMapper094* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper094_Reset;
    p->super.Write = &mmEmuMapper094_Write;
    //p->super.Read = &mmEmuMapper094_Read;
    //p->super.ReadLow = &mmEmuMapper094_ReadLow;
    //p->super.WriteLow = &mmEmuMapper094_WriteLow;
    //p->super.ExRead = &mmEmuMapper094_ExRead;
    //p->super.ExWrite = &mmEmuMapper094_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper094_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper094_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper094_HSync;
    //p->super.VSync = &mmEmuMapper094_VSync;
    //p->super.Clock = &mmEmuMapper094_Clock;
    //p->super.PPULatch = &mmEmuMapper094_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper094_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper094_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper094_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper094_IsStateSave;
    //p->super.SaveState = &mmEmuMapper094_SaveState;
    //p->super.LoadState = &mmEmuMapper094_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper094_Destroy(struct mmEmuMapper094* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper094_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper094* p = (struct mmEmuMapper094*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper094_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper094* p = (struct mmEmuMapper094*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xFFF0) == 0xFF00)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, (data >> 2) & 0x7);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper094_Produce(void)
{
    struct mmEmuMapper094* p = (struct mmEmuMapper094*)mmMalloc(sizeof(struct mmEmuMapper094));
    mmEmuMapper094_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper094_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper094* p = (struct mmEmuMapper094*)(m);
    mmEmuMapper094_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER094 =
{
    &__static_mmEmuMapper094_Produce,
    &__static_mmEmuMapper094_Recycle,
};
