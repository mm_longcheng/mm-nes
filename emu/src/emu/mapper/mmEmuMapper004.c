/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper004.h"
#include "emu/mmEmuNes.h"

#define MMC3_IRQ_KLAX           1
#define MMC3_IRQ_SHOUGIMEIKAN   2
#define MMC3_IRQ_DAI2JISUPER    3
#define MMC3_IRQ_DBZ2           4
#define MMC3_IRQ_ROCKMAN3       5

MM_EXPORT_EMU void mmEmuMapper004_Init(struct mmEmuMapper004* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    p->we_sram = 0;

    p->irq_type = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0xFF;
    p->irq_request = 0;
    p->irq_preset = 0;
    p->irq_preset_vbl = 0;

    p->vs_patch = 0;
    p->vs_index = 0;

    //
    p->super.Reset = &mmEmuMapper004_Reset;
    p->super.Write = &mmEmuMapper004_Write;
    //p->super.Read = &mmEmuMapper004_Read;
    p->super.ReadLow = &mmEmuMapper004_ReadLow;
    p->super.WriteLow = &mmEmuMapper004_WriteLow;
    //p->super.ExRead = &mmEmuMapper004_ExRead;
    //p->super.ExWrite = &mmEmuMapper004_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper004_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper004_ExCmdWrite;
    p->super.HSync = &mmEmuMapper004_HSync;
    //p->super.VSync = &mmEmuMapper004_VSync;
    p->super.Clock = &mmEmuMapper004_Clock;
    //p->super.PPULatch = &mmEmuMapper004_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper004_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper004_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper004_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper004_IsStateSave;
    p->super.SaveState = &mmEmuMapper004_SaveState;
    p->super.LoadState = &mmEmuMapper004_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper004_Destroy(struct mmEmuMapper004* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    p->we_sram = 0;

    p->irq_type = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0xFF;
    p->irq_request = 0;
    p->irq_preset = 0;
    p->irq_preset_vbl = 0;

    p->vs_patch = 0;
    p->vs_index = 0;
}

MM_EXPORT_EMU void mmEmuMapper004_SetBankCPU(struct mmEmuMapper004* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, p->prg1, p->prg0, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper004_SetBankPPU(struct mmEmuMapper004* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetVROM8KBankArray(mmu, p->chr4, p->chr5, p->chr6, p->chr7, p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1);
        }
        else
        {
            mmEmuMmu_SetVROM8KBankArray(mmu, p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1, p->chr4, p->chr5, p->chr6, p->chr7);
        }
    }
    else
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 4, (p->chr01 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 5, (p->chr01 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 6, (p->chr23 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 7, (p->chr23 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 0, p->chr4 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 1, p->chr5 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 2, p->chr6 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 3, p->chr7 & 0x07);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 0, (p->chr01 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 1, (p->chr01 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 2, (p->chr23 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 3, (p->chr23 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 4, p->chr4 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 5, p->chr5 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 6, p->chr6 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 7, p->chr7 & 0x07);
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper004_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper004_SetBankCPU(p);

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    mmEmuMapper004_SetBankPPU(p);

    p->we_sram = 0; // Disable
    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0xFF;
    p->irq_request = 0;
    p->irq_preset = 0;
    p->irq_preset_vbl = 0;

    // IRQ type setting.
    mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_CLOCK);
    p->irq_type = 0;

    crc = mmEmuRom_GetPROMCRC(&p->super.nes->rom);

    if (crc == 0x5c707ac4)      // Mother(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0xcb106f49)      // F-1 Sensation(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x1170392a)      // Karakuri Kengou Den - Musashi Road - Karakuri Nin Hashiru!(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0x14a01c70)      // Gun-Dec(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0xeffeea40)      // For Klax(J)
    {
        p->irq_type = MMC3_IRQ_KLAX;
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0xc17ae2dc)      // God Slayer - Haruka Tenkuu no Sonata(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x126ea4a0)      // Summer Carnival '92 - Recca(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0x1f2f4861)      // J League Fighting Soccer - The King of Ace Strikers(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0x5a6860f1)      // Shougi Meikan '92(J)
    {
        p->irq_type = MMC3_IRQ_SHOUGIMEIKAN;
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0xae280e20)      // Shougi Meikan '93(J)
    {
        p->irq_type = MMC3_IRQ_SHOUGIMEIKAN;
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0xe19a2473)      // Sugoro Quest - Dice no Senshi Tachi(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x702d9b33)      // Star Wars - The Empire Strikes Back(Victor)(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0xa9a0d729)      // Dai Kaijuu - Deburas(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0xc5fea9f2)      // Dai 2 Ji - Super Robot Taisen(J)
    {
        p->irq_type = MMC3_IRQ_DAI2JISUPER;
    }
    if (crc == 0xd852c2f7)      // Time Zone(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0xecfd3c69)      // Taito Chase H.Q.(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x7a748058)      // Tom & Jerry (and Tuffy)(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0xaafe699c)      // Ninja Ryukenden 3 - Yomi no Hakobune(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x6cc62c06)      // Hoshi no Kirby - Yume no Izumi no Monogatari(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x877dba77)      // My Life My Love - Boku no Yume - Watashi no Negai(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0x6f96ed15)      // Max Warrior - Wakusei Kaigenrei(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0x8685f366)      // Matendouji(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x8635fed1)      // Mickey Mouse 3 - Yume Fuusen(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x26ff3ea2)      // Yume Penguin Monogatari(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0x7671bc51)      // Red Ariimaa 2(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0xade11141)      // Wanpaku Kokkun no Gourmet World(J)
    {
        mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);
    }
    if (crc == 0x7c7ab58e)      // Walkuere no Bouken - Toki no Kagi Densetsu(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }
    if (crc == 0x26ff3ea2)      // Yume Penguin Monogatari(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x126ea4a0)      // Summer Carnival '92 Recca(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }

    if (crc == 0x1d2e5018 ||    // Rockman 3(J)
        crc == 0x6b999aaf)      // Megaman 3(U)
    {
        p->irq_type = MMC3_IRQ_ROCKMAN3;
    }

    if (crc == 0xd88d48d7)      // Kick Master(U)
    {
        p->irq_type = MMC3_IRQ_ROCKMAN3;
    }

    if (crc == 0xA67EA466)      // Alien 3(U)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }

    if (crc == 0xe763891b)      // DBZ2
    {
        p->irq_type = MMC3_IRQ_DBZ2;
    }

    // VS-Unisystem
    p->vs_patch = 0;
    p->vs_index = 0;

    if (crc == 0xeb2dba63 ||        // VS TKO Boxing
        crc == 0x98cfe016)  // VS TKO Boxing (Alt)
    {
        p->vs_patch = 1;
    }
    if (crc == 0x135adf7c)      // VS Atari RBI Baseball
    {
        p->vs_patch = 2;
    }
    if (crc == 0xf9d3b0a3 ||    // VS Super Xevious
        crc == 0x9924980a ||    // VS Super Xevious (b1)
        crc == 0x66bb838f)      // VS Super Xevious (b2)
    {
        p->vs_patch = 3;
    }

    if (crc == 0x06cae67f ||    // Monster in My Pocket.
        crc == 0x0a40846f)      // Monster in My Pocket(CN)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    
    if (crc == 0x9077a623)      // KIRBYADV.
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
}
MM_EXPORT_EMU mmByte_t mmEmuMapper004_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (!p->vs_patch)
    {
        if (addr >= 0x5000 && addr <= 0x5FFF)
        {
            return mmu->XRAM[addr - 0x4000];
        }
    }
    else if (p->vs_patch == 1)
    {
        // VS TKO Boxing Security
        if (addr == 0x5E00)
        {
            p->vs_index = 0;
            return 0x00;
        }
        else if (addr == 0x5E01)
        {
            static const mmByte_t VS_TKO_Security[32] =
            {
                0xff, 0xbf, 0xb7, 0x97, 0x97, 0x17, 0x57, 0x4f,
                0x6f, 0x6b, 0xeb, 0xa9, 0xb1, 0x90, 0x94, 0x14,
                0x56, 0x4e, 0x6f, 0x6b, 0xeb, 0xa9, 0xb1, 0x90,
                0xd4, 0x5c, 0x3e, 0x26, 0x87, 0x83, 0x13, 0x00,
            };
            return VS_TKO_Security[(p->vs_index++) & 0x1F];
        }
    }
    else if (p->vs_patch == 2)
    {
        // VS Atari RBI Baseball Security
        if (addr == 0x5E00)
        {
            p->vs_index = 0;
            return 0x00;
        }
        else if (addr == 0x5E01)
        {
            if (p->vs_index++ == 9)
            {
                return 0x6F;
            }
            else
            {
                return 0xB4;
            }
        }
    }
    else if (p->vs_patch == 3)
    {
        // VS Super Xevious
        switch (addr)
        {
        case 0x54FF:
            return 0x05;
        case 0x5678:
            if (p->vs_index)
            {
                return 0x00;
            }
            else
            {
                return 0x01;
            }
            break;
        case 0x578f:
            if (p->vs_index)
            {
                return 0xD1;
            }
            else
            {
                return 0x89;
            }
            break;
        case 0x5567:
            if (p->vs_index)
            {
                p->vs_index = 0;
                return 0x3E;
            }
            else
            {
                p->vs_index = 1;
                return 0x37;
            }
            break;
        default:
            break;
        }
    }

    return mmEmuMapper_ReadLow(super, addr);
}
MM_EXPORT_EMU void mmEmuMapper004_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x5000 && addr <= 0x5FFF)
    {
        mmu->XRAM[addr - 0x4000] = data;
    }
    else
    {
        mmEmuMapper_WriteLow(super, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper004_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper004_SetBankCPU(p);
        mmEmuMapper004_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;

        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr01 = data & 0xFE;
            mmEmuMapper004_SetBankPPU(p);
            break;
        case 0x01:
            p->chr23 = data & 0xFE;
            mmEmuMapper004_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper004_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper004_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper004_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper004_SetBankPPU(p);
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper004_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper004_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );
        break;
    case 0xC000:
        //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );
        p->reg[4] = data;
        if (p->irq_type == MMC3_IRQ_KLAX || p->irq_type == MMC3_IRQ_ROCKMAN3)
        {
            p->irq_counter = data;
        }
        else
        {
            p->irq_latch = data;
        }
        if (p->irq_type == MMC3_IRQ_DBZ2)
        {
            p->irq_latch = 0x07;
        }
        break;
    case 0xC001:
        p->reg[5] = data;
        if (p->irq_type == MMC3_IRQ_KLAX || p->irq_type == MMC3_IRQ_ROCKMAN3)
        {
            p->irq_latch = data;
        }
        else
        {
            if ((mmEmuNes_GetScanline(nes) < 240) || (p->irq_type == MMC3_IRQ_SHOUGIMEIKAN))
            {
                p->irq_counter |= 0x80;
                p->irq_preset = 0xFF;
            }
            else
            {
                p->irq_counter |= 0x80;
                p->irq_preset_vbl = 0xFF;
                p->irq_preset = 0;
            }
        }
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_enable = 0;
        p->irq_request = 0;

        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 1;
        p->irq_request = 0;

        // mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper004_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    // struct mmEmuMapper004* p = (struct mmEmuMapper004*)(super);

    // struct mmEmuNes* nes = p->super.nes;

    // if( p->irq_request && (mm_emu_cpu_GetIrqType(&nes->cpu) == MM_EMU_NES_IRQ_CLOCK) )
    // {
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    // }
}
MM_EXPORT_EMU void mmEmuMapper004_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (p->irq_type == MMC3_IRQ_KLAX)
    {
        if ((scanline >= 0 && scanline <= 239) && mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (p->irq_counter == 0)
                {
                    p->irq_counter = p->irq_latch;
                    p->irq_request = 0xFF;
                }
                if (p->irq_counter > 0)
                {
                    p->irq_counter--;
                }
            }
        }
        if (p->irq_request)
        {
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
    else if (p->irq_type == MMC3_IRQ_ROCKMAN3)
    {
        if ((scanline >= 0 && scanline <= 239) && mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(--p->irq_counter))
                {
                    p->irq_request = 0xFF;
                    p->irq_counter = p->irq_latch;
                }
            }
        }
        if (p->irq_request)
        {
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
    }
    else
    {
        if ((scanline >= 0 && scanline <= 239) && mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_preset_vbl)
            {
                p->irq_counter = p->irq_latch;
                p->irq_preset_vbl = 0;
            }
            if (p->irq_preset)
            {
                p->irq_counter = p->irq_latch;
                p->irq_preset = 0;
                if (p->irq_type == MMC3_IRQ_DAI2JISUPER && scanline == 0)
                {
                    p->irq_counter--;
                }
            }
            else if (p->irq_counter > 0)
            {
                p->irq_counter--;
            }

            if (p->irq_counter == 0)
            {
                if (p->irq_enable)
                {
                    p->irq_request = 0xFF;

                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);

#if 0
                    {
                        mmInt_t i = 0;
                        mmByte_t* lpScn = mmEmuPpu_GetScreenPtr(&nes->ppu);

                        lpScn = lpScn + (256 + 16) * mmEmuNes_GetScanline(nes);

                        for (i = 0; i < 16; i++)
                        {
                            lpScn[i] = 22;
                        }
                    }
#endif
                }
                p->irq_preset = 0xFF;
            }
        }
    }

    //if(p->irq_request && (mmEmuNes_GetIrqType(nes) == MM_EMU_NES_IRQ_HSYNC))
    //{
    //  mm_emu_nes_IRQ_NotPending(&nes->cpu);
    //}
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper004_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper004_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);

    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
    buffer[16] = p->irq_enable;
    buffer[17] = (mmByte_t)p->irq_counter;
    buffer[18] = p->irq_latch;
    buffer[19] = p->irq_request;
    buffer[20] = p->irq_preset;
    buffer[21] = p->irq_preset_vbl;
}
MM_EXPORT_EMU void mmEmuMapper004_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);

    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
    p->irq_enable = buffer[16];
    p->irq_counter = (mmInt_t)buffer[17];
    p->irq_latch = buffer[18];
    p->irq_request = buffer[19];
    p->irq_preset = buffer[20];
    p->irq_preset_vbl = buffer[21];
}

static struct mmEmuMapper* __static_mmEmuMapper004_Produce(void)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)mmMalloc(sizeof(struct mmEmuMapper004));
    mmEmuMapper004_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper004_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper004* p = (struct mmEmuMapper004*)(m);
    mmEmuMapper004_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER004 =
{
    &__static_mmEmuMapper004_Produce,
    &__static_mmEmuMapper004_Recycle,
};
