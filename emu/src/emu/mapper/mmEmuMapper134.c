/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper134.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper134_Init(struct mmEmuMapper134* p)
{
    mmEmuMapper_Init(&p->super);

    p->cmd = 0;
    p->prg = 0;
    p->chr = 0;

    //
    p->super.Reset = &mmEmuMapper134_Reset;
    //p->super.Write = &mmEmuMapper134_Write;
    //p->super.Read = &mmEmuMapper134_Read;
    //p->super.ReadLow = &mmEmuMapper134_ReadLow;
    p->super.WriteLow = &mmEmuMapper134_WriteLow;
    //p->super.ExRead = &mmEmuMapper134_ExRead;
    //p->super.ExWrite = &mmEmuMapper134_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper134_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper134_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper134_HSync;
    //p->super.VSync = &mmEmuMapper134_VSync;
    //p->super.Clock = &mmEmuMapper134_Clock;
    //p->super.PPULatch = &mmEmuMapper134_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper134_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper134_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper134_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper134_IsStateSave;
    p->super.SaveState = &mmEmuMapper134_SaveState;
    p->super.LoadState = &mmEmuMapper134_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper134_Destroy(struct mmEmuMapper134* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->cmd = 0;
    p->prg = 0;
    p->chr = 0;
}

MM_EXPORT_EMU void mmEmuMapper134_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper134* p = (struct mmEmuMapper134*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    //mmEmuMmu_SetPROM16KBank(mmu, 6, 0);
    //mmEmuMmu_SetPROM16KBank(mmu, 6, 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper134_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper134* p = (struct mmEmuMapper134*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0x4101)
    {
    case 0x4100:
        p->cmd = data & 0x07;
        break;
    case 0x4101:
        switch (p->cmd)
        {
        case 0:
            p->prg = 0;
            p->chr = 3;
            break;
        case 4:
            p->chr &= 0x3;
            p->chr |= (data & 0x07) << 2;
            break;
        case 5:
            p->prg = data & 0x07;
            break;
        case 6:
            p->chr &= 0x1C;
            p->chr |= data & 0x3;
            break;
        case 7:
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
    mmEmuMmu_SetPROM32KBank(mmu, p->prg);
    //mmEmuMmu_SetPROM16KBank(mmu, 4, (p->prg<<1)|0);
    //mmEmuMmu_SetPROM16KBank(mmu, 6, (p->prg<<1)|1); 
    mmEmuMmu_SetVROM8KBank(mmu, p->chr);
    mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper134_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper134_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper134* p = (struct mmEmuMapper134*)(super);

    buffer[0] = p->cmd;
    buffer[1] = p->prg;
    buffer[2] = p->chr;
}
MM_EXPORT_EMU void mmEmuMapper134_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper134* p = (struct mmEmuMapper134*)(super);

    p->cmd = buffer[0];
    p->prg = buffer[1];
    p->chr = buffer[2];
}

static struct mmEmuMapper* __static_mmEmuMapper134_Produce(void)
{
    struct mmEmuMapper134* p = (struct mmEmuMapper134*)mmMalloc(sizeof(struct mmEmuMapper134));
    mmEmuMapper134_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper134_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper134* p = (struct mmEmuMapper134*)(m);
    mmEmuMapper134_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER134 =
{
    &__static_mmEmuMapper134_Produce,
    &__static_mmEmuMapper134_Recycle,
};
