/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMmu.h"
#include "core/mmAlloc.h"

MM_EXPORT_EMU void mmEmuMmu_Init(struct mmEmuMmu* p)
{
    mmMemset(&p->CPU_MEM_BANK, 0, sizeof(mmByte_t*) * 8);
    mmMemset(&p->CPU_MEM_TYPE, 0, sizeof(mmByte_t) * 8);
    mmMemset(&p->CPU_MEM_PAGE, 0, sizeof(mmInt_t) * 8);

    mmMemset(&p->PPU_MEM_BANK, 0, sizeof(mmByte_t*) * 12);
    mmMemset(&p->PPU_MEM_TYPE, 0, sizeof(mmByte_t) * 12);
    mmMemset(&p->PPU_MEM_PAGE, 0, sizeof(mmInt_t) * 12);

    mmMemset(&p->CRAM_USED, 0, sizeof(mmByte_t) * 16);

    mmMemset(&p->RAM, 0, sizeof(mmByte_t) * 8 * 1024);
    mmMemset(&p->WRAM, 0, sizeof(mmByte_t) * 128 * 1024);
    mmMemset(&p->DRAM, 0, sizeof(mmByte_t) * 40 * 1024);
    mmMemset(&p->XRAM, 0, sizeof(mmByte_t) * 8 * 1024);
    mmMemset(&p->ERAM, 0, sizeof(mmByte_t) * 32 * 1024);

    mmMemset(&p->CRAM, 0, sizeof(mmByte_t) * 32 * 1024);
    mmMemset(&p->VRAM, 0, sizeof(mmByte_t) * 4 * 1024);

    mmMemset(&p->SPRAM, 0, sizeof(mmByte_t) * 0x100);
    mmMemset(&p->BGPAL, 0, sizeof(mmByte_t) * 0x10);
    mmMemset(&p->SPPAL, 0, sizeof(mmByte_t) * 0x10);

    mmMemset(&p->CPUREG, 0, sizeof(mmByte_t) * 0x18);
    mmMemset(&p->PPUREG, 0, sizeof(mmByte_t) * 0x04);

    p->FrameIRQ = 0xC0;

    p->PPU56Toggle = 0;
    p->PPU7_Temp = 0;
    p->loopy_t = 0;
    p->loopy_v = 0;
    p->loopy_x = 0;

    p->PROM = NULL;
    p->VROM = NULL;

    p->PROM_ACCESS = NULL;

    p->PROM_08K_SIZE = 1;
    p->PROM_16K_SIZE = 1;
    p->PROM_32K_SIZE = 1;

    p->VROM_1K_SIZE = 1;
    p->VROM_2K_SIZE = 1;
    p->VROM_4K_SIZE = 1;
    p->VROM_8K_SIZE = 1;

    // Set default
    mmMemset(&p->CPU_MEM_TYPE, MM_EMU_MMU_BANKTYPE_ROM, sizeof(mmByte_t) * 8);

    // Visceral RAM and WRAM
    mmEmuMmu_SetPROMBank(p, 0, p->RAM, MM_EMU_MMU_BANKTYPE_RAM);
    mmEmuMmu_SetPROMBank(p, 3, p->WRAM, MM_EMU_MMU_BANKTYPE_RAM);

    // Dummy
    mmEmuMmu_SetPROMBank(p, 1, p->XRAM, MM_EMU_MMU_BANKTYPE_ROM);
    mmEmuMmu_SetPROMBank(p, 2, p->XRAM, MM_EMU_MMU_BANKTYPE_ROM);
}
MM_EXPORT_EMU void mmEmuMmu_Destroy(struct mmEmuMmu* p)
{
    mmMemset(&p->CPU_MEM_BANK, 0, sizeof(mmByte_t*) * 8);
    mmMemset(&p->CPU_MEM_TYPE, 0, sizeof(mmByte_t) * 8);
    mmMemset(&p->CPU_MEM_PAGE, 0, sizeof(mmInt_t) * 8);

    mmMemset(&p->PPU_MEM_BANK, 0, sizeof(mmByte_t*) * 12);
    mmMemset(&p->PPU_MEM_TYPE, 0, sizeof(mmByte_t) * 12);
    mmMemset(&p->PPU_MEM_PAGE, 0, sizeof(mmInt_t) * 12);

    mmMemset(&p->CRAM_USED, 0, sizeof(mmByte_t) * 16);

    mmMemset(&p->RAM, 0, sizeof(mmByte_t) * 8 * 1024);
    mmMemset(&p->WRAM, 0, sizeof(mmByte_t) * 128 * 1024);
    mmMemset(&p->DRAM, 0, sizeof(mmByte_t) * 40 * 1024);
    mmMemset(&p->XRAM, 0, sizeof(mmByte_t) * 8 * 1024);
    mmMemset(&p->ERAM, 0, sizeof(mmByte_t) * 32 * 1024);

    mmMemset(&p->CRAM, 0, sizeof(mmByte_t) * 32 * 1024);
    mmMemset(&p->VRAM, 0, sizeof(mmByte_t) * 4 * 1024);

    mmMemset(&p->SPRAM, 0, sizeof(mmByte_t) * 0x100);
    mmMemset(&p->BGPAL, 0, sizeof(mmByte_t) * 0x10);
    mmMemset(&p->SPPAL, 0, sizeof(mmByte_t) * 0x10);

    mmMemset(&p->CPUREG, 0, sizeof(mmByte_t) * 0x18);
    mmMemset(&p->PPUREG, 0, sizeof(mmByte_t) * 0x04);

    p->FrameIRQ = 0xC0;

    p->PPU56Toggle = 0;
    p->PPU7_Temp = 0;
    p->loopy_t = 0;
    p->loopy_v = 0;
    p->loopy_x = 0;

    p->PROM = NULL;
    p->VROM = NULL;

    p->PROM_ACCESS = NULL;

    p->PROM_08K_SIZE = 1;
    p->PROM_16K_SIZE = 1;
    p->PROM_32K_SIZE = 1;

    p->VROM_1K_SIZE = 1;
    p->VROM_2K_SIZE = 1;
    p->VROM_4K_SIZE = 1;
    p->VROM_8K_SIZE = 1;
}

MM_EXPORT_EMU void mmEmuMmu_Reset(struct mmEmuMmu* p)
{
    mmMemset(&p->CPU_MEM_BANK, 0, sizeof(mmByte_t*) * 8);
    mmMemset(&p->CPU_MEM_TYPE, 0, sizeof(mmByte_t) * 8);
    mmMemset(&p->CPU_MEM_PAGE, 0, sizeof(mmInt_t) * 8);

    mmMemset(&p->PPU_MEM_BANK, 0, sizeof(mmByte_t*) * 12);
    mmMemset(&p->PPU_MEM_TYPE, 0, sizeof(mmByte_t) * 12);
    mmMemset(&p->PPU_MEM_PAGE, 0, sizeof(mmInt_t) * 12);

    mmMemset(&p->CRAM_USED, 0, sizeof(mmByte_t) * 16);

    mmMemset(&p->RAM, 0, sizeof(mmByte_t) * 8 * 1024);
    mmMemset(&p->WRAM, 0, sizeof(mmByte_t) * 128 * 1024);
    mmMemset(&p->DRAM, 0, sizeof(mmByte_t) * 40 * 1024);
    mmMemset(&p->XRAM, 0, sizeof(mmByte_t) * 8 * 1024);
    mmMemset(&p->ERAM, 0, sizeof(mmByte_t) * 32 * 1024);

    mmMemset(&p->CRAM, 0, sizeof(mmByte_t) * 32 * 1024);
    mmMemset(&p->VRAM, 0, sizeof(mmByte_t) * 4 * 1024);

    mmMemset(&p->SPRAM, 0, sizeof(mmByte_t) * 0x100);
    mmMemset(&p->BGPAL, 0, sizeof(mmByte_t) * 0x10);
    mmMemset(&p->SPPAL, 0, sizeof(mmByte_t) * 0x10);

    mmMemset(&p->CPUREG, 0, sizeof(mmByte_t) * 0x18);
    mmMemset(&p->PPUREG, 0, sizeof(mmByte_t) * 0x04);

    p->FrameIRQ = 0xC0;

    p->PPU56Toggle = 0;
    p->PPU7_Temp = 0;
    p->loopy_t = 0;
    p->loopy_v = 0;
    p->loopy_x = 0;

    p->PROM = NULL;
    p->VROM = NULL;

    p->PROM_ACCESS = NULL;

    p->PROM_08K_SIZE = 1;
    p->PROM_16K_SIZE = 1;
    p->PROM_32K_SIZE = 1;

    p->VROM_1K_SIZE = 1;
    p->VROM_2K_SIZE = 1;
    p->VROM_4K_SIZE = 1;
    p->VROM_8K_SIZE = 1;

    // Set default
    mmMemset(&p->CPU_MEM_TYPE, MM_EMU_MMU_BANKTYPE_ROM, sizeof(mmByte_t) * 8);

    // Visceral RAM and WRAM
    mmEmuMmu_SetPROMBank(p, 0, p->RAM, MM_EMU_MMU_BANKTYPE_RAM);
    mmEmuMmu_SetPROMBank(p, 3, p->WRAM, MM_EMU_MMU_BANKTYPE_RAM);

    // Dummy
    mmEmuMmu_SetPROMBank(p, 1, p->XRAM, MM_EMU_MMU_BANKTYPE_ROM);
    mmEmuMmu_SetPROMBank(p, 2, p->XRAM, MM_EMU_MMU_BANKTYPE_ROM);
}

MM_EXPORT_EMU void mmEmuMmu_SetPROMBank(struct mmEmuMmu* p, mmByte_t page, mmByte_t* ptr, mmByte_t type)
{
    p->CPU_MEM_BANK[page] = ptr;
    p->CPU_MEM_TYPE[page] = type;
    p->CPU_MEM_PAGE[page] = 0;
}
MM_EXPORT_EMU void mmEmuMmu_SetPROM8KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    bank %= p->PROM_08K_SIZE;
    p->CPU_MEM_BANK[page] = p->PROM + 0x2000 * bank;
    p->CPU_MEM_TYPE[page] = MM_EMU_MMU_BANKTYPE_ROM;
    p->CPU_MEM_PAGE[page] = bank;
}
MM_EXPORT_EMU void mmEmuMmu_SetPROM16KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    mmEmuMmu_SetPROM8KBank(p, page + 0, bank * 2 + 0);
    mmEmuMmu_SetPROM8KBank(p, page + 1, bank * 2 + 1);
}
MM_EXPORT_EMU void mmEmuMmu_SetPROM32KBank(struct mmEmuMmu* p, mmInt_t bank)
{
    mmEmuMmu_SetPROM8KBank(p, 4, bank * 4 + 0);
    mmEmuMmu_SetPROM8KBank(p, 5, bank * 4 + 1);
    mmEmuMmu_SetPROM8KBank(p, 6, bank * 4 + 2);
    mmEmuMmu_SetPROM8KBank(p, 7, bank * 4 + 3);
}
MM_EXPORT_EMU void mmEmuMmu_SetPROM32KBankArray(struct mmEmuMmu* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3)
{
    mmEmuMmu_SetPROM8KBank(p, 4, bank0);
    mmEmuMmu_SetPROM8KBank(p, 5, bank1);
    mmEmuMmu_SetPROM8KBank(p, 6, bank2);
    mmEmuMmu_SetPROM8KBank(p, 7, bank3);
}

MM_EXPORT_EMU void mmEmuMmu_SetVROMBank(struct mmEmuMmu* p, mmByte_t page, mmByte_t* ptr, mmByte_t type)
{
    p->PPU_MEM_BANK[page] = ptr;
    p->PPU_MEM_TYPE[page] = type;
    p->PPU_MEM_PAGE[page] = 0;
}
MM_EXPORT_EMU void mmEmuMmu_SetVROM1KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    bank %= p->VROM_1K_SIZE;
    p->PPU_MEM_BANK[page] = p->VROM + 0x0400 * bank;
    p->PPU_MEM_TYPE[page] = MM_EMU_MMU_BANKTYPE_VROM;
    p->PPU_MEM_PAGE[page] = bank;
}
MM_EXPORT_EMU void mmEmuMmu_SetVROM2KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    mmEmuMmu_SetVROM1KBank(p, page + 0, bank * 2 + 0);
    mmEmuMmu_SetVROM1KBank(p, page + 1, bank * 2 + 1);
}
MM_EXPORT_EMU void mmEmuMmu_SetVROM4KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    mmEmuMmu_SetVROM1KBank(p, page + 0, bank * 4 + 0);
    mmEmuMmu_SetVROM1KBank(p, page + 1, bank * 4 + 1);
    mmEmuMmu_SetVROM1KBank(p, page + 2, bank * 4 + 2);
    mmEmuMmu_SetVROM1KBank(p, page + 3, bank * 4 + 3);
}
MM_EXPORT_EMU void mmEmuMmu_SetVROM8KBank(struct mmEmuMmu* p, mmInt_t bank)
{
    mmEmuMmu_SetVROM1KBank(p, 0, bank * 8 + 0);
    mmEmuMmu_SetVROM1KBank(p, 1, bank * 8 + 1);
    mmEmuMmu_SetVROM1KBank(p, 2, bank * 8 + 2);
    mmEmuMmu_SetVROM1KBank(p, 3, bank * 8 + 3);
    mmEmuMmu_SetVROM1KBank(p, 4, bank * 8 + 4);
    mmEmuMmu_SetVROM1KBank(p, 5, bank * 8 + 5);
    mmEmuMmu_SetVROM1KBank(p, 6, bank * 8 + 6);
    mmEmuMmu_SetVROM1KBank(p, 7, bank * 8 + 7);
}
MM_EXPORT_EMU void mmEmuMmu_SetVROM8KBankArray(struct mmEmuMmu* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3, mmInt_t bank4, mmInt_t bank5, mmInt_t bank6, mmInt_t bank7)
{
    mmEmuMmu_SetVROM1KBank(p, 0, bank0);
    mmEmuMmu_SetVROM1KBank(p, 1, bank1);
    mmEmuMmu_SetVROM1KBank(p, 2, bank2);
    mmEmuMmu_SetVROM1KBank(p, 3, bank3);
    mmEmuMmu_SetVROM1KBank(p, 4, bank4);
    mmEmuMmu_SetVROM1KBank(p, 5, bank5);
    mmEmuMmu_SetVROM1KBank(p, 6, bank6);
    mmEmuMmu_SetVROM1KBank(p, 7, bank7);
}

MM_EXPORT_EMU void mmEmuMmu_SetCRAM1KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    bank &= 0x1F;
    p->PPU_MEM_BANK[page] = p->CRAM + 0x0400 * bank;
    p->PPU_MEM_TYPE[page] = MM_EMU_MMU_BANKTYPE_CRAM;
    p->PPU_MEM_PAGE[page] = bank;

    p->CRAM_USED[bank >> 2] = 0xFF;// CRAM use flag
}
MM_EXPORT_EMU void mmEmuMmu_SetCRAM2KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    mmEmuMmu_SetCRAM1KBank(p, page + 0, bank * 2 + 0);
    mmEmuMmu_SetCRAM1KBank(p, page + 1, bank * 2 + 1);
}
MM_EXPORT_EMU void mmEmuMmu_SetCRAM4KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    mmEmuMmu_SetCRAM1KBank(p, page + 0, bank * 4 + 0);
    mmEmuMmu_SetCRAM1KBank(p, page + 1, bank * 4 + 1);
    mmEmuMmu_SetCRAM1KBank(p, page + 2, bank * 4 + 2);
    mmEmuMmu_SetCRAM1KBank(p, page + 3, bank * 4 + 3);
}
MM_EXPORT_EMU void mmEmuMmu_SetCRAM8KBank(struct mmEmuMmu* p, mmInt_t bank)
{
    mmEmuMmu_SetCRAM1KBank(p, 0, bank * 8 + 0);
    mmEmuMmu_SetCRAM1KBank(p, 1, bank * 8 + 1);
    mmEmuMmu_SetCRAM1KBank(p, 2, bank * 8 + 2);
    mmEmuMmu_SetCRAM1KBank(p, 3, bank * 8 + 3);
    mmEmuMmu_SetCRAM1KBank(p, 4, bank * 8 + 4);
    mmEmuMmu_SetCRAM1KBank(p, 5, bank * 8 + 5);
    mmEmuMmu_SetCRAM1KBank(p, 6, bank * 8 + 6);
    mmEmuMmu_SetCRAM1KBank(p, 7, bank * 8 + 7);
}

MM_EXPORT_EMU void mmEmuMmu_SetVRAM1KBank(struct mmEmuMmu* p, mmByte_t page, mmInt_t bank)
{
    bank &= 3;
    p->PPU_MEM_BANK[page] = p->VRAM + 0x0400 * bank;
    p->PPU_MEM_TYPE[page] = MM_EMU_MMU_BANKTYPE_VRAM;
    p->PPU_MEM_PAGE[page] = bank;
}
MM_EXPORT_EMU void mmEmuMmu_SetVRAMBank(struct mmEmuMmu* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3)
{
    mmEmuMmu_SetVRAM1KBank(p,  8, bank0);
    mmEmuMmu_SetVRAM1KBank(p,  9, bank1);
    mmEmuMmu_SetVRAM1KBank(p, 10, bank2);
    mmEmuMmu_SetVRAM1KBank(p, 11, bank3);
}
MM_EXPORT_EMU void mmEmuMmu_SetVRAMMirror(struct mmEmuMmu* p, mmInt_t type)
{
    switch (type)
    {
    case MM_EMU_MMU_VRAM_HMIRROR:
        mmEmuMmu_SetVRAMBank(p, 0, 0, 1, 1);
        break;
    case MM_EMU_MMU_VRAM_VMIRROR:
        mmEmuMmu_SetVRAMBank(p, 0, 1, 0, 1);
        break;
    case MM_EMU_MMU_VRAM_MIRROR4L:
        mmEmuMmu_SetVRAMBank(p, 0, 0, 0, 0);
        break;
    case MM_EMU_MMU_VRAM_MIRROR4H:
        mmEmuMmu_SetVRAMBank(p, 1, 1, 1, 1);
        break;
    case MM_EMU_MMU_VRAM_MIRROR4:
        mmEmuMmu_SetVRAMBank(p, 0, 1, 2, 3);
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMmu_SetVRAMMirrorArray(struct mmEmuMmu* p, mmInt_t bank0, mmInt_t bank1, mmInt_t bank2, mmInt_t bank3)
{
    mmEmuMmu_SetVRAM1KBank(p,  8, bank0);
    mmEmuMmu_SetVRAM1KBank(p,  9, bank1);
    mmEmuMmu_SetVRAM1KBank(p, 10, bank2);
    mmEmuMmu_SetVRAM1KBank(p, 11, bank3);
}

