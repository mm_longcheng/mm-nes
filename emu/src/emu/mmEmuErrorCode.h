/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuErrorCode_h__
#define __mmEmuErrorCode_h__

#include "core/mmCore.h"

#include "dish/mmErrorDesc.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

enum mmErrEmu_t
{
    MM_ERR_EMU_OUT_OF_MEMORY          = 100001001, // (100001001)Out of memory
    MM_ERR_EMU_FILE_READ              = 100001002, // (100001002)File read error
    MM_ERR_EMU_FILE_WRITE             = 100001003, // (100001003)File write error
    MM_ERR_EMU_FILE_NOT_EXIST         = 100001004, // (100001004)File not exist
    MM_ERR_EMU_FILE_COMPRESS          = 100001005, // (100001005)File compress error

    MM_ERR_EMU_THERE_IS_NO_ROM        = 100001010, // (100001010)There is no rom
    MM_ERR_EMU_THERE_IS_NO_DISK_BIOS  = 100001011, // (100001011)There is no disk bios
    MM_ERR_EMU_THERE_IS_NO_STATE_FILE = 100001012, // (100001012)There is no state file

    MM_ERR_EMU_NOT_SUPPORTED_FORMAT   = 100001020, // (100001020)Not supported format
    MM_ERR_EMU_NOT_SUPPORTED_DISK     = 100001021, // (100001021)Not supported disk
    MM_ERR_EMU_NOT_SUPPORTED_MAPPER   = 100001022, // (100001022)Not supported mapper
    MM_ERR_EMU_NOT_SUPPORTED_BANK     = 100001023, // (100001023)Not supported bank

    MM_ERR_EMU_INVALID_NES_HEADER     = 100001030, // (100001030)Invalid nes header

    MM_ERR_EMU_ILLEGAL_DISK_SIZE      = 100001040, // (100001040)Illegal disk size
    MM_ERR_EMU_ILLEGAL_OPCODE         = 100001041, // (100001041)Illegal disk size
    MM_ERR_EMU_ILLEGAL_STATE_CRC      = 100001042, // (100001042)Illegal state crc
    MM_ERR_EMU_ILLEGAL_MOVIE_OLD      = 100001043, // (100001043)Illegal movie old
    MM_ERR_EMU_ILLEGAL_MOVIE_CRC      = 100001044, // (100001044)Illegal movie crc
    MM_ERR_EMU_ILLEGAL_MOVIE_VER      = 100001045, // (100001045)Illegal movie version
    MM_ERR_EMU_ILLEGAL_ROM_CRC        = 100001046, // (100001046)Illegal rom crc

    MM_ERR_EMU_FILE_SIZE_IS_TOO_SMALL = 100001050, // (100001050)File size is too small
};

MM_EXPORT_EMU void mmErrorDesc_SetEmu(struct mmErrorDesc* p);

#include "core/mmSuffix.h"

#endif//__mmEmuErrorCode_h__
