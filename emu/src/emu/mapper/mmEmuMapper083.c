/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper083.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper083_Init(struct mmEmuMapper083* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);
    p->chr_bank = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;

    p->patch = 0;

    //
    p->super.Reset = &mmEmuMapper083_Reset;
    p->super.Write = &mmEmuMapper083_Write;
    //p->super.Read = &mmEmuMapper083_Read;
    p->super.ReadLow = &mmEmuMapper083_ReadLow;
    p->super.WriteLow = &mmEmuMapper083_WriteLow;
    //p->super.ExRead = &mmEmuMapper083_ExRead;
    //p->super.ExWrite = &mmEmuMapper083_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper083_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper083_ExCmdWrite;
    p->super.HSync = &mmEmuMapper083_HSync;
    //p->super.VSync = &mmEmuMapper083_VSync;
    //p->super.Clock = &mmEmuMapper083_Clock;
    //p->super.PPULatch = &mmEmuMapper083_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper083_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper083_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper083_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper083_IsStateSave;
    p->super.SaveState = &mmEmuMapper083_SaveState;
    p->super.LoadState = &mmEmuMapper083_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper083_Destroy(struct mmEmuMapper083* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);
    p->chr_bank = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;

    p->patch = 0;
}

MM_EXPORT_EMU void mmEmuMapper083_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 3);

    if (mmu->PROM_08K_SIZE >= 32)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 30, 31);
        p->reg[1] = 0x30;
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    p->chr_bank = 0;

    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x1461D1F8)
    {
        p->patch = 1;
    }
}
MM_EXPORT_EMU mmByte_t mmEmuMapper083_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)(super);

    if ((addr & 0x5100) == 0x5100)
    {
        return p->reg[2];
    }
    else if (addr >= 0x6000)
    {
        return mmEmuMapper_ReadLow(super, addr);
    }

    return (mmByte_t)(addr >> 8);
}
MM_EXPORT_EMU void mmEmuMapper083_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)(super);

    //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );
    switch (addr)
    {
    case 0x5101:
    case 0x5102:
    case 0x5103:
        p->reg[2] = data;
        break;
    default:
        break;
    }

    if (addr >= 0x6000)
    {
        mmEmuMapper_WriteLow(super, addr, data);
    }
}
MM_EXPORT_EMU void mmEmuMapper083_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );

    switch (addr)
    {
    case 0x8000:
    case 0xB000:
    case 0xB0FF:
    case 0xB1FF:
        p->reg[0] = data;
        p->chr_bank = (data & 0x30) << 4;
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        mmEmuMmu_SetPROM16KBank(mmu, 6, (data & 0x30) | 0x0F);
        break;

    case 0x8100:
        p->reg[1] = data & 0x80;
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        break;

    case 0x8200:
        p->irq_counter = (p->irq_counter & 0xFF00) | (mmInt_t)data;
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0x8201:
        p->irq_counter = (p->irq_counter & 0x00FF) | ((mmInt_t)data << 8);
        p->irq_enable = p->reg[1];
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0x8300:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0x8301:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    case 0x8302:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;

    case 0x8310:
        if (p->patch)
        {
            mmEmuMmu_SetVROM2KBank(mmu, 0, p->chr_bank | data);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 0, p->chr_bank | data);
        }
        break;
    case 0x8311:
        if (p->patch)
        {
            mmEmuMmu_SetVROM2KBank(mmu, 2, p->chr_bank | data);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 1, p->chr_bank | data);
        }
        break;
    case 0x8312:
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->chr_bank | data);
        break;
    case 0x8313:
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->chr_bank | data);
        break;
    case 0x8314:
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->chr_bank | data);
        break;
    case 0x8315:
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->chr_bank | data);
        break;
    case 0x8316:
        if (p->patch)
        {
            mmEmuMmu_SetVROM2KBank(mmu, 4, p->chr_bank | data);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 6, p->chr_bank | data);
        }
        break;
    case 0x8317:
        if (p->patch)
        {
            mmEmuMmu_SetVROM2KBank(mmu, 6, p->chr_bank | data);
        }
        else
        {
            mmEmuMmu_SetVROM1KBank(mmu, 7, p->chr_bank | data);
        }
        break;

    case 0x8318:
        mmEmuMmu_SetPROM16KBank(mmu, 4, (p->reg[0] & 0x30) | data);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper083_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if (p->irq_counter <= 113)
        {
            //mm_emu_cpu_IRQ(&nes->cpu);
            p->irq_enable = 0;
            //mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_TRIGGER);
        }
        else
        {
            p->irq_counter -= 113;
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper083_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper083_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    *(mmInt_t*)&buffer[3] = p->chr_bank;
    buffer[7] = p->irq_enable;
    *(mmInt_t*)&buffer[8] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper083_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->chr_bank = *(mmInt_t*)&buffer[3];
    p->irq_enable = buffer[7];
    p->irq_counter = *(mmInt_t*)&buffer[8];
}

static struct mmEmuMapper* __static_mmEmuMapper083_Produce(void)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)mmMalloc(sizeof(struct mmEmuMapper083));
    mmEmuMapper083_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper083_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper083* p = (struct mmEmuMapper083*)(m);
    mmEmuMapper083_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER083 =
{
    &__static_mmEmuMapper083_Produce,
    &__static_mmEmuMapper083_Recycle,
};
