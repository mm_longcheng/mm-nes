/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApuVrc6_h__
#define __mmEmuApuVrc6_h__

#include "core/mmCore.h"

#include "emu/mmEmuApuInterface.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuApuVrc6Rectangle
{
    mmByte_t    reg[3];

    mmByte_t    enable;
    mmByte_t    gate;
    mmByte_t    volume;

    mmInt_t     phaseacc;
    mmInt_t     freq;
    mmInt_t     output_vol;

    mmByte_t    adder;
    mmByte_t    duty_pos;
};

struct mmEmuApuVrc6Sawtooth
{
    mmByte_t    reg[3];

    mmByte_t    enable;
    mmByte_t    volume;

    mmInt_t     phaseacc;
    mmInt_t     freq;
    mmInt_t     output_vol;

    mmByte_t    adder;
    mmByte_t    accum;
    mmByte_t    phaseaccum;
};

struct mmEmuApuVrc6
{
    struct mmEmuApuInterface super;

    struct mmEmuApuVrc6Rectangle    ch0, ch1;
    struct mmEmuApuVrc6Sawtooth     ch2;

    mmInt_t     cycle_rate;

    double      cpu_clock;
};

MM_EXPORT_EMU void mmEmuApuVrc6_Init(struct mmEmuApuVrc6* p);
MM_EXPORT_EMU void mmEmuApuVrc6_Destroy(struct mmEmuApuVrc6* p);

MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_RectangleRender(struct mmEmuApuVrc6* p, struct mmEmuApuVrc6Rectangle* ch);
MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_SawtoothRender(struct mmEmuApuVrc6* p, struct mmEmuApuVrc6Sawtooth* ch);

// virtual function for super.
MM_EXPORT_EMU void mmEmuApuVrc6_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuVrc6_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuVrc6_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_Process(struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuVrc6_GetStateSize(const struct mmEmuApuInterface* super);
MM_EXPORT_EMU void mmEmuApuVrc6_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuApuVrc6_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer);

#include "core/mmSuffix.h"

#endif//__mmEmuApuVrc6_h__
