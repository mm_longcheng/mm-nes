/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper253.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper253_Init(struct mmEmuMapper253* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    p->VRAM_switch = 0;

    //
    p->super.Reset = &mmEmuMapper253_Reset;
    p->super.Write = &mmEmuMapper253_Write;
    //p->super.Read = &mmEmuMapper253_Read;
    //p->super.ReadLow = &mmEmuMapper253_ReadLow;
    //p->super.WriteLow = &mmEmuMapper253_WriteLow;
    //p->super.ExRead = &mmEmuMapper253_ExRead;
    //p->super.ExWrite = &mmEmuMapper253_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper253_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper253_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper253_HSync;
    //p->super.VSync = &mmEmuMapper253_VSync;
    p->super.Clock = &mmEmuMapper253_Clock;
    //p->super.PPULatch = &mmEmuMapper253_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper253_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper253_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper253_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper253_IsStateSave;
    p->super.SaveState = &mmEmuMapper253_SaveState;
    p->super.LoadState = &mmEmuMapper253_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper253_Destroy(struct mmEmuMapper253* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    p->VRAM_switch = 0;
}

MM_EXPORT_EMU void mmEmuMapper253_SetBankPPUSUB(struct mmEmuMapper253* p, int bank, int page)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (page == 0x88)
    {
        p->VRAM_switch = 0;
        return;
    }
    else if (page == 0xc8)
    {
        p->VRAM_switch = 1;
        return;
    }
    if ((page == 4) || (page == 5))
    {
        if (p->VRAM_switch == 0)
        {
            mmEmuMmu_SetVROM1KBank(mmu, bank, page);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, bank, page);
        }
    }
    else
    {
        mmEmuMmu_SetVROM1KBank(mmu, bank, page);
    }
}

MM_EXPORT_EMU void mmEmuMapper253_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper253* p = (struct mmEmuMapper253*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i = 0;

    for (i = 0; i < 8; i++)
    {
        p->reg[i] = i;
    }
    p->reg[8] = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;
    p->VRAM_switch = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper253_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper253* p = (struct mmEmuMapper253*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr == 0x8010)
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        return;
    }
    if (addr == 0xA010)
    {
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        return;
    }
    if (addr == 0x9400)
    {
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
    }
    switch (addr & 0xF00C)
    {
    case 0xB000:
        p->reg[0] = (p->reg[0] & 0xF0) | (data & 0x0F);
        mmEmuMapper253_SetBankPPUSUB(p, 0, p->reg[0]);
        break;
    case 0xB004:
        p->reg[0] = (p->reg[0] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMapper253_SetBankPPUSUB(p, 0, p->reg[0] + ((data >> 4) * 0x100));
        break;
    case 0xB008:
        p->reg[1] = (p->reg[1] & 0xF0) | (data & 0x0F);
        mmEmuMapper253_SetBankPPUSUB(p, 1, p->reg[1]);
        break;
    case 0xB00C:
        p->reg[1] = (p->reg[1] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMapper253_SetBankPPUSUB(p, 1, p->reg[1] + ((data >> 4) * 0x100));
        break;
    case 0xC000:
        p->reg[2] = (p->reg[2] & 0xF0) | (data & 0x0F);
        mmEmuMapper253_SetBankPPUSUB(p, 2, p->reg[2]);
        break;
    case 0xC004:
        p->reg[2] = (p->reg[2] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMapper253_SetBankPPUSUB(p, 2, p->reg[2] + ((data >> 4) * 0x100));
        break;
    case 0xC008:
        p->reg[3] = (p->reg[3] & 0xF0) | (data & 0x0F);
        mmEmuMapper253_SetBankPPUSUB(p, 3, p->reg[3]);
        break;
    case 0xC00C:
        p->reg[3] = (p->reg[3] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMapper253_SetBankPPUSUB(p, 3, p->reg[3] + ((data >> 4) * 0x100));
        break;
    case 0xD000:
        p->reg[4] = (p->reg[4] & 0xF0) | (data & 0x0F);
        mmEmuMapper253_SetBankPPUSUB(p, 4, p->reg[4]);
        break;
    case 0xD004:
        p->reg[4] = (p->reg[4] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMapper253_SetBankPPUSUB(p, 4, p->reg[4] + ((data >> 4) * 0x100));
        break;
    case 0xD008:
        p->reg[5] = (p->reg[5] & 0xF0) | (data & 0x0F);
        mmEmuMapper253_SetBankPPUSUB(p, 5, p->reg[5]);
        break;
    case 0xD00C:
        p->reg[5] = (p->reg[5] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMapper253_SetBankPPUSUB(p, 5, p->reg[5] + ((data >> 4) * 0x100));
        break;
    case 0xE000:
        p->reg[6] = (p->reg[6] & 0xF0) | (data & 0x0F);
        mmEmuMapper253_SetBankPPUSUB(p, 6, p->reg[6]);
        break;
    case 0xE004:
        p->reg[6] = (p->reg[6] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMapper253_SetBankPPUSUB(p, 6, p->reg[6] + ((data >> 4) * 0x100));
        break;
    case 0xE008:
        p->reg[7] = (p->reg[7] & 0xF0) | (data & 0x0F);
        mmEmuMapper253_SetBankPPUSUB(p, 7, p->reg[7]);
        break;
    case 0xE00C:
        p->reg[7] = (p->reg[7] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMapper253_SetBankPPUSUB(p, 7, p->reg[7] + ((data >> 4) * 0x100));
        break;
    case 0xF000:
        p->irq_latch = (p->irq_latch & 0xF0) | (data & 0x0F);
        break;
    case 0xF004:
        p->irq_latch = (p->irq_latch & 0x0F) | ((data & 0x0F) << 4);
        break;
    case 0xF008:
        p->irq_enable = data & 0x03;
        if (p->irq_enable & 0x02)
        {
            p->irq_counter = p->irq_latch;
            p->irq_clock = 0;
        }
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper253_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper253* p = (struct mmEmuMapper253*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (p->irq_enable & 0x02)
    {
        if ((p->irq_clock += cycles) >= 0x72)
        {
            p->irq_clock -= 0x72;
            if (p->irq_counter == 0xFF)
            {
                p->irq_counter = p->irq_latch;
                p->irq_enable = (p->irq_enable & 0x01) * 3;
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
            else
            {
                p->irq_counter++;
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper253_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper253_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper253* p = (struct mmEmuMapper253*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 9);

    buffer[9] = p->irq_enable;
    buffer[10] = p->irq_counter;
    buffer[11] = p->irq_latch;
    *(mmInt_t*)&buffer[12] = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper253_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper253* p = (struct mmEmuMapper253*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 9);

    p->irq_enable = buffer[9];
    p->irq_counter = buffer[10];
    p->irq_latch = buffer[11];
    p->irq_clock = *(mmInt_t*)&buffer[12];
}

static struct mmEmuMapper* __static_mmEmuMapper253_Produce(void)
{
    struct mmEmuMapper253* p = (struct mmEmuMapper253*)mmMalloc(sizeof(struct mmEmuMapper253));
    mmEmuMapper253_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper253_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper253* p = (struct mmEmuMapper253*)(m);
    mmEmuMapper253_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER253 =
{
    &__static_mmEmuMapper253_Produce,
    &__static_mmEmuMapper253_Recycle,
};
