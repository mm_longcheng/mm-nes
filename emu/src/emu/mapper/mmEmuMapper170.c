/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper170.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper170_Init(struct mmEmuMapper170* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->Rom_Type = 0;
    p->dip_s = 0;

    mmMemset(p->WRAM00, 0, sizeof(mmByte_t) * 128 * 1024);
    p->a3 = 0;
    p->p_mode = 0;
    p->NT_data = 0;

    p->ex_slot2 = 0;
    p->ex_slot3 = 0;

    //
    p->super.Reset = &mmEmuMapper170_Reset;
    //p->super.Write = &mmEmuMapper170_Write;
    //p->super.Read = &mmEmuMapper170_Read;
    p->super.ReadLow = &mmEmuMapper170_ReadLow;
    p->super.WriteLow = &mmEmuMapper170_WriteLow;
    //p->super.ExRead = &mmEmuMapper170_ExRead;
    //p->super.ExWrite = &mmEmuMapper170_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper170_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper170_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper170_HSync;
    //p->super.VSync = &mmEmuMapper170_VSync;
    //p->super.Clock = &mmEmuMapper170_Clock;
    p->super.PPULatch = &mmEmuMapper170_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper170_PPUChrLatch;
    p->super.PPUExtLatchX = &mmEmuMapper170_PPUExtLatchX;
    p->super.PPUExtLatch = &mmEmuMapper170_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper170_IsStateSave;
    p->super.SaveState = &mmEmuMapper170_SaveState;
    p->super.LoadState = &mmEmuMapper170_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper170_Destroy(struct mmEmuMapper170* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->Rom_Type = 0;
    p->dip_s = 0;

    mmMemset(p->WRAM00, 0, sizeof(mmByte_t) * 128 * 1024);
    p->a3 = 0;
    p->p_mode = 0;
    p->NT_data = 0;

    p->ex_slot2 = 0;
    p->ex_slot3 = 0;
}

MM_EXPORT_EMU void mmEmuMapper170_SetPROMBank0(struct mmEmuMapper170* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROMBank(mmu, 4, &p->WRAM00[0x2000 * 0], 0);
    mmEmuMmu_SetPROMBank(mmu, 5, &p->WRAM00[0x2000 * 1], 0);
    mmEmuMmu_SetPROMBank(mmu, 6, &p->WRAM00[0x2000 * 2], 0);
    mmEmuMmu_SetPROMBank(mmu, 7, &p->WRAM00[0x2000 * 3], 0);
}
// Partially corrected sprite color problem, not completely.
MM_EXPORT_EMU mmByte_t mmEmuMapper170_PPUExtLatchSP(struct mmEmuMapper170* p)
{
    if (p->p_mode)
    {
        //For the time being, the specific reason is unknown!
        return 2;
    }
    else
    {
        return 0;
    }
}

MM_EXPORT_EMU void mmEmuMapper170_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmInt_t i = 0;

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    for (i = 0; i < 32; i += 2)
    {
        mmMemcpy(&p->WRAM00[0x400 * i + 0x000], mmu->PROM + 0x400 * ((mmu->PROM_08K_SIZE * 0x4 + 0x07) + i * 8), 0x400);
        mmMemcpy(&p->WRAM00[0x400 * i + 0x400], mmu->PROM + 0x400 * ((mmu->PROM_08K_SIZE * 0x4 + 0x0F) + i * 8), 0x400);
    }
    mmEmuMapper170_SetPROMBank0(p);
    p->p_mode = 0;
    p->NT_data = 0;
    p->ex_slot2 = 0;
    p->ex_slot3 = 0;
    mmEmuPpu_SetExtLatchMode(&nes->ppu, MM_TRUE);

    p->dip_s++;
    if (p->dip_s == 4)
    {
        p->dip_s = 0;
    }

    p->Rom_Type = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xFE31765B)          //[PYRAMID] PEC-9588 Computer & Game (C)
    {
        p->Rom_Type = 1;
        mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 0x3E, 0x3F);
    }
    if (crc == 0x428C1C1D ||        //[FengLi] Zhong Ying Wen Dian Nao Ka (C)
        crc == 0x836CDDEF)          //[BaTong] Zhong Ying Wen Dian Nao Ka (C)
    {
        p->Rom_Type = 2;
        mmEmuMmu_SetPROM32KBank(mmu, 0);
    }

}
MM_EXPORT_EMU mmByte_t mmEmuMapper170_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //  DEBUGOUT( "ReadLow  - addr= %04x ; dat= %03x\n", addr, Mapper::ReadLow( addr ) );
    if (addr == 0x5500)
    {
        return ((p->dip_s & 3) * 0x10); //[0x00]=XinKe; [0x10]=KingWang; [0x20]=?ABC; [0x30]=KingWangEX
    }

    if (addr >= 0x6000)
    {
        return mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF];
    }
    return mmEmuMapper_ReadLow(super, addr);
}
MM_EXPORT_EMU void mmEmuMapper170_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //  DEBUGOUT( "WriteLow - addr= %04x ; dat= %03x\n", addr, data );
    if (addr >= 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }

    if (addr < 0x6000)
    {
        p->reg[(addr & 0x700) >> 8] = data;
        p->p_mode = p->reg[0] & 0x80;
        if ((p->reg[0] & 0x18) == 0x18)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }

        if (p->reg[0] & 0x10)           // [PYRAMID] PEC-586 Computer & Game (C)
        {
            mmEmuMmu_SetPROM32KBank(mmu, p->reg[0] & 0x07);
        }
        else if ((p->reg[0] & 0x70) == 0x40)
        {
            mmEmuMapper170_SetPROMBank0(p);
            mmEmuMmu_SetPROM8KBank(mmu, 4, (p->reg[0] & 0x0F) | 0x20);
        }
        else if ((p->reg[0] & 0x70) == 0x60)
        {
            mmEmuMapper170_SetPROMBank0(p);
            mmEmuMmu_SetPROM8KBank(mmu, 4, (p->reg[0] & 0x0F) | 0x30);
        }
        else
        {
            mmEmuMapper170_SetPROMBank0(p);
        }

        if (p->Rom_Type == 1)           // [PYRAMID] PEC-9588 Computer & Game (C)
        {
            //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );
            switch (p->reg[0] & 0x70)
            {
            case 0x00:
                mmEmuMmu_SetPROM16KBank(mmu, 4, p->reg[0] & 0x0F);
                mmEmuMmu_SetPROM16KBank(mmu, 6, 0x1F);
                break;
            case 0x10:
                //
                break;
            case 0x20:
                mmEmuMmu_SetPROM16KBank(mmu, 4, (p->reg[0] & 0x0F) | 0x10);
                mmEmuMmu_SetPROM16KBank(mmu, 6, 0x1F);
                break;
            case 0x30:  //Small card expansion slot 2
                p->ex_slot2 = 1;
                //mmEmuMmu_SetPROM16KBank( 4, (reg[0]&0x0F) | 0x20 );
                //mmEmuMmu_SetPROM16KBank( 6, 0x2F );
                //mmEmuMmu_SetPROM32KBank((reg[0]&0x07)|0x20);
                break;
            case 0x40:
                mmEmuMmu_SetPROM16KBank(mmu, 4, p->reg[0] & 0x0F);
                mmEmuMmu_SetPROM16KBank(mmu, 6, 0x1E);
                break;
            case 0x50:  //Small card expansion slot 3
                p->ex_slot3 = 1;
                break;
            case 0x60:
                mmEmuMmu_SetPROM16KBank(mmu, 4, (p->reg[0] & 0x0F) | 0x10);
                mmEmuMmu_SetPROM16KBank(mmu, 6, 0x1E);
                if (p->reg[0] >= 0x6C)
                {
                    mmEmuMmu_SetPROM16KBank(mmu, 6, 0x1C);
                }
                break;
            default:
                break;
            }
        }
        if (p->Rom_Type == 2)
        {
            //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );
            mmEmuMmu_SetPROM32KBank(mmu, p->reg[0] & 0x0F);
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper170_PPULatch(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(super);

    if ((addr & 0xF000) == 0x2000)
    {
        p->NT_data = (addr >> 8) & 0x03;
    }
}
MM_EXPORT_EMU void mmEmuMapper170_PPUExtLatchX(struct mmEmuMapper* super, mmInt_t x)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(super);

    p->a3 = (x & 1) << 3;
}
MM_EXPORT_EMU void mmEmuMapper170_PPUExtLatch(struct mmEmuMapper* super, mmWord_t ntbladr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t loopy_v = mmEmuPpu_GetPPUADDR(&nes->ppu);
    mmInt_t loopy_y = mmEmuPpu_GetTILEY(&nes->ppu);
    mmInt_t tileofs = (mmu->PPUREG[0] & MM_EMU_PPU_BGTBL_BIT) << 8;
    mmInt_t attradr = 0x23C0 + (loopy_v & 0x0C00) + ((loopy_v & 0x0380) >> 4);
    mmInt_t attrsft = (ntbladr & 0x0040) >> 4;
    mmByte_t* pNTBL = mmu->PPU_MEM_BANK[ntbladr >> 10];
    mmInt_t ntbl_x = ntbladr & 0x001F;
    mmInt_t tileadr;

    attradr &= 0x3FF;
    (*attr) = ((pNTBL[attradr + (ntbl_x >> 2)] >> ((ntbl_x & 2) + attrsft)) & 3) << 2;
    tileadr = tileofs + pNTBL[ntbladr & 0x03FF] * 0x10 + loopy_y;

    if (p->p_mode)
    {
        if (p->NT_data & 0x02)
        {
            tileadr |= 0x1000;
        }
        else
        {
            tileadr &= ~0x1000;
        }
        tileadr = (tileadr & 0xfff7) | p->a3;
        (*chr_l) = (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
    }
    else
    {
        (*chr_l) = mmu->PPU_MEM_BANK[tileadr >> 10][tileadr & 0x03FF];
        (*chr_h) = mmu->PPU_MEM_BANK[tileadr >> 10][(tileadr & 0x03FF) + 8];
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper170_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper170_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->Rom_Type;
    buffer[9] = p->dip_s;

    buffer[10] = p->a3;
    buffer[11] = p->p_mode;
    buffer[12] = p->NT_data;
    buffer[13] = p->ex_slot2;
    buffer[14] = p->ex_slot3;
}
MM_EXPORT_EMU void mmEmuMapper170_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->Rom_Type = buffer[8];
    p->dip_s = buffer[9];

    p->a3 = buffer[10];
    p->p_mode = buffer[11];
    p->NT_data = buffer[12];
    p->ex_slot2 = buffer[13];
    p->ex_slot3 = buffer[14];
}

static struct mmEmuMapper* __static_mmEmuMapper170_Produce(void)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)mmMalloc(sizeof(struct mmEmuMapper170));
    mmEmuMapper170_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper170_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper170* p = (struct mmEmuMapper170*)(m);
    mmEmuMapper170_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER170 =
{
    &__static_mmEmuMapper170_Produce,
    &__static_mmEmuMapper170_Recycle,
};
