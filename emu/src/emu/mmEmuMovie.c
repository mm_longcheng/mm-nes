/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMovie.h"
#include "mmEmuNes.h"
#include "mmEmuVersion.h"
#include "mmEmuErrorCode.h"

#include "core/mmLogger.h"
#include "algorithm/mmCRC32.h"

static void __static_mmEmuMovie_MovieFinish(struct mmEmuMovie* obj)
{

}
MM_EXPORT_EMU void mmEmuMovieCallback_Init(struct mmEmuMovieCallback* p)
{
    p->MovieFinish = &__static_mmEmuMovie_MovieFinish;
    p->obj = NULL;
}
MM_EXPORT_EMU void mmEmuMovieCallback_Destroy(struct mmEmuMovieCallback* p)
{
    p->MovieFinish = &__static_mmEmuMovie_MovieFinish;
    p->obj = NULL;
}

MM_EXPORT_EMU void mmEmuMovie_Init(struct mmEmuMovie* p)
{
    p->nes = NULL;

    mmEmuMovieCallback_Init(&p->callback);

    p->m_bMoviePlay = MM_FALSE;
    p->m_bMovieRec = MM_FALSE;
    p->m_MovieVersion = 0;

    p->m_fpMovie = NULL;
    mmMemset(&p->m_hedMovie, 0, sizeof(struct mmEmuMovieFileHdr));
    p->m_MovieControl = 0;
    p->m_MovieStepTotal = 0;
    p->m_MovieStep = 0;
}
MM_EXPORT_EMU void mmEmuMovie_Destroy(struct mmEmuMovie* p)
{
    p->nes = NULL;

    mmEmuMovieCallback_Destroy(&p->callback);

    p->m_bMoviePlay = MM_FALSE;
    p->m_bMovieRec = MM_FALSE;
    p->m_MovieVersion = 0;

    p->m_fpMovie = NULL;
    mmMemset(&p->m_hedMovie, 0, sizeof(struct mmEmuMovieFileHdr));
    p->m_MovieControl = 0;
    p->m_MovieStepTotal = 0;
    p->m_MovieStep = 0;
}

MM_EXPORT_EMU void mmEmuMovie_SetParent(struct mmEmuMovie* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}
MM_EXPORT_EMU void mmEmuMovie_SetCallback(struct mmEmuMovie* p, struct mmEmuMovieCallback* callback)
{
    assert(NULL != callback && "NULL != callback is invalid.");
    p->callback = *callback;
}

MM_EXPORT_EMU mmInt_t mmEmuMovie_IsMovieFile(const struct mmEmuMovie* p, const char* fname)
{
    struct mmEmuNes* nes = p->nes;

    size_t sz = 0;
    FILE* fp = NULL;
    struct mmEmuMovieFileHdr header;

    if (!(fp = fopen(fname, "rb")))
    {
        return -1;
    }

    sz = fread(&header, sizeof(struct mmEmuMovieFileHdr), 1, fp);
    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }
    if (sz != 1)
    {
        return -1;
    }

    if (mmMemcmp(header.ID, "VirtuaNES MV", sizeof(header.ID)) == 0)
    {
        if (header.BlockVersion < 0x0300)
        {
            return MM_ERR_EMU_ILLEGAL_MOVIE_OLD;
        }
        else
        {
            if (header.BlockVersion >= 0x0300)
            {
                if (mmEmuRom_GetMapperNo(&nes->rom) != 20)
                {
                    // Other than FDS.                  
                    if (header.Ext0 != mmEmuRom_GetPROMCRC(&nes->rom))
                    {
                        // It is different.
                        return MM_ERR_EMU_ILLEGAL_MOVIE_CRC;
                    }
                }
                else
                {
                    // FDS
                    if (header.Ext0 != mmEmuRom_GetGameID(&nes->rom) ||
                        header.Ext1 != (mmWord_t)mmEmuRom_GetMakerID(&nes->rom) ||
                        header.Ext2 != (mmWord_t)mmEmuRom_GetDiskNo(&nes->rom))
                    {
                        // It is different.
                        return MM_ERR_EMU_ILLEGAL_MOVIE_CRC;
                    }
                }

                if (header.RecordVersion != MM_EMU_VERSION)
                {
                    return MM_ERR_EMU_ILLEGAL_MOVIE_VER;
                }

                return 0;
            }
        }
    }

    return -1;
}
MM_EXPORT_EMU int mmEmuMovie_MoviePlay(struct mmEmuMovie* p, const char* fname)
{
    struct mmEmuNes* nes = p->nes;

    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;

    mmSInt32_t MovieOffset;

    do
    {
        if (mmEmuRom_IsNSF(&nes->rom))
        {
            code = MM_FAILURE;
            break;
        }

        if (mmEmuMovie_IsMoviePlay(p) || mmEmuMovie_IsMovieRec(p))
        {
            mmEmuMovie_MovieStop(p);
        }

        mmLogger_LogD(gLogger, "%s %d.", __FUNCTION__, __LINE__);

        if (!(p->m_fpMovie = fopen(fname, "rb+")))
        {
            mmLogger_LogE(gLogger, "%s %d Movie play error. File not found: %s.", __FUNCTION__, __LINE__, fname);
            // Innocent.
            code = MM_ERR_EMU_FILE_NOT_EXIST;
            break;
        }

        // Read
        if (fread(&p->m_hedMovie, sizeof(p->m_hedMovie), 1, p->m_fpMovie) != 1)
        {
            // Failed to read file.
            mmLogger_LogE(gLogger, "%s %d Failed to read file: %s.", __FUNCTION__, __LINE__, fname);
            code = MM_ERR_EMU_FILE_READ;
            break;
        }

        if (mmMemcmp(p->m_hedMovie.ID, "VirtuaNES MV", sizeof(p->m_hedMovie.ID)) == 0)
        {
            p->m_MovieVersion = p->m_hedMovie.BlockVersion;

            // if( m_hedMovie.BlockVersion == 0x0300 ) {
            if (p->m_hedMovie.BlockVersion >= 0x0300)
            {
                if (p->m_hedMovie.CRC != 0)
                {
                    if (mmCRC32B(&p->m_hedMovie, (int)(sizeof(p->m_hedMovie) - sizeof(mmUInt32_t)), 0) != p->m_hedMovie.CRC)
                    {
                        if (NULL != p->m_fpMovie)
                        {
                            fclose(p->m_fpMovie);
                            p->m_fpMovie = NULL;
                        }
                        // It is different.
                        code = MM_FAILURE;
                        break;
                    }
                }
                // Ok ...
            }
            else
            {
                // teacher! Because it is old, it is not good.
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                code = MM_FAILURE;
                break;
            }
        }
        // Game specific option.
        nes->m_saveRenderMethod = (mmInt_t)mmEmuNes_GetRenderMethod(nes);
        nes->m_saveIrqType = mmEmuNes_GetIrqType(nes);
        nes->m_saveFrameIRQ = mmEmuNes_GetFrameIRQmode(nes);
        nes->m_saveVideoMode = mmEmuNes_GetVideoMode(nes);
        mmEmuNes_SetRenderMethod(nes, p->m_hedMovie.RenderMethod);
        mmEmuNes_SetIrqType(nes, (mmInt_t)p->m_hedMovie.IRQtype);
        mmEmuNes_SetFrameIRQmode(nes, (p->m_hedMovie.FrameIRQ != 0) ? MM_TRUE : MM_FALSE);
        mmEmuNes_SetVideoMode(nes, p->m_hedMovie.VideoMode);

        p->m_MovieControl = p->m_hedMovie.Control;
        p->m_MovieStepTotal = p->m_hedMovie.MovieStep;
        MovieOffset = p->m_hedMovie.MovieOffset;

        if (p->m_hedMovie.BlockVersion < 0x0400)
        {
            // State loading.
            mmEmuNes_ReadState(nes, p->m_fpMovie);
        }
        else if (!(p->m_MovieControl & 0x40))
        {
            // State loading.
            mmEmuNes_ReadState(nes, p->m_fpMovie);
        }
        else
        {
            mmEmuNes_HardwareReset(nes);
        }

        if (fseek(p->m_fpMovie, MovieOffset, SEEK_SET))
        {
            // Failed to read file.
            mmLogger_LogE(gLogger, "%s %d Failed to read file: %s.", __FUNCTION__, __LINE__, fname);
            code = MM_ERR_EMU_FILE_READ;
            break;
        }

        // Is not a movie recorded?
        if (p->m_MovieStepTotal == 0)
        {
            mmEmuMovie_MovieStop(p);
            code = MM_FAILURE;
            break;
        }

        p->m_bMoviePlay = MM_TRUE;
        p->m_MovieStep = 0;

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_EMU int mmEmuMovie_MovieRec(struct mmEmuMovie* p, const char* fname)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMovieConfig* movie_config = &nes->config.movie;

    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;

    do
    {
        if (mmEmuRom_IsNSF(&nes->rom))
        {
            code = MM_FAILURE;
            break;
        }

        if (mmEmuMovie_IsMoviePlay(p) || mmEmuMovie_IsMovieRec(p))
        {
            mmEmuMovie_MovieStop(p);
        }

        mmLogger_LogD(gLogger, "%s %d.", __FUNCTION__, __LINE__);

        if (!(p->m_fpMovie = fopen(fname, "wb")))
        {
            // Can not open xxx file.
            mmLogger_LogE(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, fname);
            code = MM_ERR_EMU_FILE_NOT_EXIST;
            break;
        }

        mmMemset(&p->m_hedMovie, 0, sizeof(p->m_hedMovie));
        mmMemcpy(p->m_hedMovie.ID, "VirtuaNES MV", sizeof(p->m_hedMovie.ID));

        // m_hedMovie.BlockVersion = 0x0300;
        p->m_hedMovie.BlockVersion = 0x0400;
        p->m_hedMovie.RecordVersion = MM_EMU_VERSION;

        p->m_hedMovie.StateStOffset = sizeof(p->m_hedMovie);

        p->m_hedMovie.Control |= movie_config->bUsePlayer[0] ? 0x01 : 0x00;
        p->m_hedMovie.Control |= movie_config->bUsePlayer[1] ? 0x02 : 0x00;
        p->m_hedMovie.Control |= movie_config->bUsePlayer[2] ? 0x04 : 0x00;
        p->m_hedMovie.Control |= movie_config->bUsePlayer[3] ? 0x08 : 0x00;
        p->m_hedMovie.Control |= movie_config->bResetRec ? 0x40 : 0x00;
        p->m_hedMovie.Control |= movie_config->bRerecord ? 0x80 : 0x00;
        p->m_MovieControl = p->m_hedMovie.Control;

        // Game specific option.
        p->m_hedMovie.RenderMethod = (mmByte_t)mmEmuNes_GetRenderMethod(nes);
        p->m_hedMovie.IRQtype = (mmByte_t)mmEmuNes_GetIrqType(nes);
        p->m_hedMovie.FrameIRQ = mmEmuNes_GetFrameIRQmode(nes) ? 0xFF : 0;
        p->m_hedMovie.VideoMode = mmEmuNes_GetVideoMode(nes);

        // CRC, write ID value (for preventing malfunction).
        if (mmEmuRom_GetMapperNo(&nes->rom) != 20)
        {
            // Other than FDS.      
            p->m_hedMovie.Ext0 = mmEmuRom_GetPROMCRC(&nes->rom);
        }
        else
        {
            // FDS
            p->m_hedMovie.Ext0 = mmEmuRom_GetGameID(&nes->rom);
            p->m_hedMovie.Ext1 = (mmWord_t)mmEmuRom_GetMakerID(&nes->rom);
            p->m_hedMovie.Ext2 = (mmWord_t)mmEmuRom_GetDiskNo(&nes->rom);
        }

        // Dummy write.
        if (fwrite(&p->m_hedMovie, sizeof(p->m_hedMovie), 1, p->m_fpMovie) != 1)
        {
            if (NULL != p->m_fpMovie)
            {
                fclose(p->m_fpMovie);
                p->m_fpMovie = NULL;
            }
            // Failed to write the file.
            mmLogger_LogE(gLogger, "%s %d Failed to write the file: %s.", __FUNCTION__, __LINE__, fname);
            code = MM_ERR_EMU_FILE_WRITE;
            break;
        }

        if (movie_config->bResetRec)
        {
            // Start recording from hardware reset.
            mmEmuNes_HardwareReset(nes);
        }
        else
        {
            // State Write.
            mmEmuNes_WriteState(nes, p->m_fpMovie);
        }

        p->m_hedMovie.MovieOffset = (mmSInt32_t)ftell(p->m_fpMovie);
        p->m_bMovieRec = MM_TRUE;
        p->m_MovieStep = p->m_MovieStepTotal = 0;
        // m_MovieVersion = 0x0300;
        p->m_MovieVersion = 0x0400;

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_EMU int mmEmuMovie_MovieRecAppend(struct mmEmuMovie* p, const char* fname)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMovieConfig* movie_config = &nes->config.movie;

    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;

    do
    {
        if (mmEmuRom_IsNSF(&nes->rom))
        {
            code = MM_FAILURE;
            break;
        }

        // There is no meaning during recording.
        if (mmEmuMovie_IsMovieRec(p))
        {
            code = MM_FAILURE;
            break;
        }

        if (mmEmuMovie_IsMoviePlay(p))
        {
            mmEmuMovie_MovieStop(p);
        }

        mmLogger_LogD(gLogger, "%s %d.", __FUNCTION__, __LINE__);

        if (!(p->m_fpMovie = fopen(fname, "rb")))
        {
            // When there is no file.
            if (!(p->m_fpMovie = fopen(fname, "wb")))
            {
                // Can not open xxx file.
                mmLogger_LogE(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, fname);
                code = MM_ERR_EMU_FILE_NOT_EXIST;
                break;
            }

            mmMemset(&p->m_hedMovie, 0, sizeof(p->m_hedMovie));
            mmMemcpy(p->m_hedMovie.ID, "VirtuaNES MV", sizeof(p->m_hedMovie.ID));
            // m_hedMovie.BlockVersion = 0x0300;
            p->m_hedMovie.BlockVersion = 0x0400;
            p->m_hedMovie.RecordVersion = MM_EMU_VERSION;
            p->m_hedMovie.StateStOffset = sizeof(p->m_hedMovie);

            p->m_hedMovie.Control |= movie_config->bUsePlayer[0] ? 0x01 : 0x00;
            p->m_hedMovie.Control |= movie_config->bUsePlayer[1] ? 0x02 : 0x00;
            p->m_hedMovie.Control |= movie_config->bUsePlayer[2] ? 0x04 : 0x00;
            p->m_hedMovie.Control |= movie_config->bUsePlayer[3] ? 0x08 : 0x00;
            p->m_hedMovie.Control |= movie_config->bRerecord ? 0x80 : 0x00;
            p->m_hedMovie.Control |= movie_config->bResetRec ? 0x40 : 0x00;
            p->m_MovieControl = p->m_hedMovie.Control;

            // Game specific option.
            p->m_hedMovie.RenderMethod = (mmByte_t)mmEmuNes_GetRenderMethod(nes);
            p->m_hedMovie.IRQtype = (mmByte_t)mmEmuNes_GetIrqType(nes);
            p->m_hedMovie.FrameIRQ = mmEmuNes_GetFrameIRQmode(nes) ? 0xFF : 0;
            p->m_hedMovie.VideoMode = mmEmuNes_GetVideoMode(nes);

            // Dummy write.
            if (fwrite(&p->m_hedMovie, sizeof(p->m_hedMovie), 1, p->m_fpMovie) != 1)
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                // Failed to write the file.
                mmLogger_LogE(gLogger, "%s %d Failed to write the file: %s.", __FUNCTION__, __LINE__, fname);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            if (movie_config->bResetRec)
            {
                // Start recording from hardware reset.
                mmEmuNes_HardwareReset(nes);
            }
            else
            {
                // State Write.
                mmEmuNes_WriteState(nes, p->m_fpMovie);
            }
            p->m_hedMovie.MovieOffset = (mmSInt32_t)ftell(p->m_fpMovie);
            p->m_MovieStep = p->m_MovieStepTotal = 0;
            // m_MovieVersion = 0x0300;
            p->m_MovieVersion = 0x0400;
        }
        else
        {
            if (!(p->m_fpMovie = fopen(fname, "rb+")))
            {
                // Can not open xxx file.
                mmLogger_LogE(gLogger, "%s %d File not found: %s.", __FUNCTION__, __LINE__, fname);
                code = MM_ERR_EMU_FILE_NOT_EXIST;
                break;
            }
            // Read.
            if (fseek(p->m_fpMovie, 0, SEEK_SET))
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                // Failed to read the file.
                mmLogger_LogE(gLogger, "%s %d Failed to read the file: %s.", __FUNCTION__, __LINE__, fname);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
            if (fread(&p->m_hedMovie, sizeof(p->m_hedMovie), 1, p->m_fpMovie) != 1)
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                // Failed to read the file.
                mmLogger_LogE(gLogger, "%s %d Failed to read the file: %s.", __FUNCTION__, __LINE__, fname);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }

            if (mmMemcmp(p->m_hedMovie.ID, "VirtuaNES MV", sizeof(p->m_hedMovie.ID)) != 0)
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                code = MM_FAILURE;
                break;
            }
            // Throw away the old version.
            if (p->m_hedMovie.BlockVersion < 0x0300)
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                code = MM_FAILURE;
                break;
            }

            p->m_MovieControl = p->m_hedMovie.Control;
            p->m_MovieStep = p->m_MovieStepTotal = p->m_hedMovie.MovieStep;
            // m_MovieVersion = 0x0300;
            p->m_MovieVersion = 0x0400;

            if (fseek(p->m_fpMovie, p->m_hedMovie.StateEdOffset, SEEK_SET))
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                // Failed to read the file.
                mmLogger_LogE(gLogger, "%s %d Failed to read the file: %s.", __FUNCTION__, __LINE__, fname);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
            if (MM_SUCCESS != mmEmuNes_ReadState(nes, p->m_fpMovie))
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                // Failed to read the file.
                mmLogger_LogE(gLogger, "%s %d Failed to read the file: %s.", __FUNCTION__, __LINE__, fname);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
            if (fseek(p->m_fpMovie, p->m_hedMovie.StateEdOffset, SEEK_SET))
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                // Failed to read the file.
                mmLogger_LogE(gLogger, "%s %d Failed to read the file: %s.", __FUNCTION__, __LINE__, fname);
                code = MM_ERR_EMU_FILE_READ;
                break;
            }
        }
        p->m_bMovieRec = MM_TRUE;

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_EMU mmBool_t mmEmuMovie_MovieStop(struct mmEmuMovie* p)
{
    struct mmEmuNes* nes = p->nes;

    struct mmLogger* gLogger = mmLogger_Instance();

    int code = MM_UNKNOWN;

    do
    {
        if (!p->m_fpMovie && !(p->m_bMoviePlay || p->m_bMovieRec))
        {
            code = MM_FAILURE;
            break;
        }

        mmLogger_LogD(gLogger, "%s %d.", __FUNCTION__, __LINE__);

        if (p->m_bMovieRec)
        {
            p->m_hedMovie.MovieStep = p->m_MovieStep;
            p->m_hedMovie.StateEdOffset = (mmSInt32_t)ftell(p->m_fpMovie);
            mmEmuNes_WriteState(nes, p->m_fpMovie);

            //// In case of prohibiting re-taking it is impossible to add.
            //if( p->m_MovieControl & 0x80 ) 
            //{
            //} 
            //else 
            //{
            //  p->m_hedMovie.StateEdOffset = 0;
            //}
            if (fseek(p->m_fpMovie, 0, SEEK_SET))
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                // Failed to write the file.
                mmLogger_LogE(gLogger, "%s %d Failed to write the file.", __FUNCTION__, __LINE__);
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            // CRC write.
            p->m_hedMovie.CRC = mmCRC32B(&p->m_hedMovie, (int)(sizeof(p->m_hedMovie) - sizeof(mmUInt32_t)), 0);

            // Final Header Write.
            if (fwrite(&p->m_hedMovie, sizeof(p->m_hedMovie), 1, p->m_fpMovie) != 1)
            {
                if (NULL != p->m_fpMovie)
                {
                    fclose(p->m_fpMovie);
                    p->m_fpMovie = NULL;
                }
                // Failed to write the file.
                code = MM_ERR_EMU_FILE_WRITE;
                break;
            }

            if (NULL != p->m_fpMovie)
            {
                fclose(p->m_fpMovie);
                p->m_fpMovie = NULL;
            }
            p->m_bMovieRec = MM_FALSE;
        }

        if (p->m_bMoviePlay)
        {
            if (NULL != p->m_fpMovie)
            {
                fclose(p->m_fpMovie);
                p->m_fpMovie = NULL;
            }
            p->m_bMoviePlay = MM_FALSE;

            // Restore game specific options.
            mmEmuNes_SetRenderMethod(nes, nes->m_saveRenderMethod);
            mmEmuNes_SetIrqType(nes, nes->m_saveIrqType);
            mmEmuNes_SetFrameIRQmode(nes, nes->m_saveFrameIRQ);
            mmEmuNes_SetVideoMode(nes, nes->m_saveVideoMode);
        }

        code = MM_SUCCESS;
    } while (0);

    return code;
}
// Call every frame.
MM_EXPORT_EMU void mmEmuMovie_Movie(struct mmEmuMovie* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMovieConfig* movie_config = &nes->config.movie;

    struct mmLogger* gLogger = mmLogger_Instance();

    mmInt_t exctr = 0;

    mmByte_t    Data;
    mmWord_t    wData;
    mmUInt32_t  dwData;
    mmInt_t i = 0;

    if (!p->m_fpMovie && !(p->m_bMoviePlay || p->m_bMovieRec))
    {
        // If you do not put it in, you will die.
        nes->m_CommandRequest = 0;
        return;
    }

    exctr = mmEmuPad_GetExController(&nes->pad);

    if (p->m_bMovieRec)
    {
        // When an extended controller has been set from the beginning.
        if (p->m_MovieStep == 0)
        {
            if (exctr == MM_EMU_PAD_EXCONTROLLER_ZAPPER ||
                exctr == MM_EMU_PAD_EXCONTROLLER_PADDLE ||
                exctr == MM_EMU_PAD_EXCONTROLLER_CRAZYCLIMBER ||
                exctr == MM_EMU_PAD_EXCONTROLLER_TOPRIDER ||
                exctr == MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN ||
                exctr == MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERA ||
                exctr == MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERB ||
                exctr == MM_EMU_PAD_EXCONTROLLER_MAHJANG ||
                exctr == MM_EMU_PAD_EXCONTROLLER_EXCITINGBOXING ||
                exctr == MM_EMU_PAD_EXCONTROLLER_OEKAKIDSTABLET)
            {
                // Command ID
                Data = 0xF0;
                // writing
                if (fwrite(&Data, sizeof(Data), 1, p->m_fpMovie) != 1)
                {
                    (*(p->callback.MovieFinish))(p);
                    mmEmuMovie_MovieStop(p);
                    // Failed to write the file.
                    mmLogger_LogE(gLogger, "%s %d Failed to write the file.", __FUNCTION__, __LINE__);
                    return;
                }
                // type
                wData = (mmWord_t)(0x0100 | (mmEmuPad_GetExController(&nes->pad) & 0x0FF));
                // writing
                if (fwrite(&wData, sizeof(wData), 1, p->m_fpMovie) != 1)
                {
                    (*(p->callback.MovieFinish))(p);
                    mmEmuMovie_MovieStop(p);
                    // Failed to write the file.
                    mmLogger_LogE(gLogger, "%s %d Failed to write the file.", __FUNCTION__, __LINE__);
                    return;
                }
            }
        }

        if (nes->m_CommandRequest)
        {
            // Command ID
            Data = 0xF0;
            // writing
            if (fwrite(&Data, sizeof(Data), 1, p->m_fpMovie) != 1)
            {
                (*(p->callback.MovieFinish))(p);
                mmEmuMovie_MovieStop(p);
                // Failed to write the file.
                mmLogger_LogE(gLogger, "%s %d Failed to write the file.", __FUNCTION__, __LINE__);
                return;
            }
            // command
            wData = (mmWord_t)nes->m_CommandRequest;
            // writing
            if (fwrite(&wData, sizeof(wData), 1, p->m_fpMovie) != 1)
            {
                (*(p->callback.MovieFinish))(p);
                mmEmuMovie_MovieStop(p);
                // Failed to write the file.
                mmLogger_LogE(gLogger, "%s %d Failed to write the file.", __FUNCTION__, __LINE__);
                return;
            }
        }
        nes->m_CommandRequest = 0;

        // Extended controller
        if (exctr == MM_EMU_PAD_EXCONTROLLER_ZAPPER ||
            exctr == MM_EMU_PAD_EXCONTROLLER_PADDLE ||
            exctr == MM_EMU_PAD_EXCONTROLLER_CRAZYCLIMBER ||
            exctr == MM_EMU_PAD_EXCONTROLLER_TOPRIDER ||
            exctr == MM_EMU_PAD_EXCONTROLLER_SPACESHADOWGUN ||
            exctr == MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERA ||
            exctr == MM_EMU_PAD_EXCONTROLLER_FAMILYTRAINERB ||
            exctr == MM_EMU_PAD_EXCONTROLLER_MAHJANG ||
            exctr == MM_EMU_PAD_EXCONTROLLER_EXCITINGBOXING ||
            exctr == MM_EMU_PAD_EXCONTROLLER_OEKAKIDSTABLET)
        {
            // Extended controller data ID
            Data = 0xF3;
            // writing
            if (fwrite(&Data, sizeof(Data), 1, p->m_fpMovie) != 1)
            {
                (*(p->callback.MovieFinish))(p);
                mmEmuMovie_MovieStop(p);
                // Failed to write the file
                mmLogger_LogE(gLogger, "%s %d Failed to write the file.", __FUNCTION__, __LINE__);
                return;
            }
            // data
            dwData = mmEmuPad_GetSyncExData(&nes->pad);

            // writing
            if (fwrite(&dwData, sizeof(dwData), 1, p->m_fpMovie) != 1)
            {
                (*(p->callback.MovieFinish))(p);
                mmEmuMovie_MovieStop(p);
                // Failed to write the file.
                mmLogger_LogE(gLogger, "%s %d Failed to write the file.", __FUNCTION__, __LINE__);
                return;
            }
        }

        dwData = mmEmuPad_GetSyncData(&nes->pad);
        for (i = 0; i < 4; i++)
        {
            Data = (mmByte_t)(dwData >> (i * 8));
            if (p->m_MovieControl & (1 << i))
            {
                // writing
                if (fwrite(&Data, sizeof(Data), 1, p->m_fpMovie) != 1)
                {
                    (*(p->callback.MovieFinish))(p);
                    mmEmuMovie_MovieStop(p);
                    // Failed to write the file.
                    mmLogger_LogE(gLogger, "%s %d Failed to write the file.", __FUNCTION__, __LINE__);
                    return;
                }
            }
        }

        p->m_MovieStep++;
    }

    if (p->m_bMoviePlay)
    {
        // mmUInt32_t   dwPadData = 0;
        mmInt_t num = 0;
        mmByte_t    PadBuf[4];

        PadBuf[0] = PadBuf[1] = PadBuf[2] = PadBuf[3] = 0;

        // End of movie playback?
        if (p->m_MovieStep >= p->m_MovieStepTotal)
        {
            if (!movie_config->bLoopPlay)
            {
                (*(p->callback.MovieFinish))(p);
                mmEmuMovie_MovieStop(p);
                return;
            }
            else
            {
                // I will say that it is not playing once.
                p->m_bMoviePlay = MM_FALSE;
                p->m_MovieStep = 0;
                fseek(p->m_fpMovie, p->m_hedMovie.StateStOffset, SEEK_SET);
                // State loading.
                mmEmuNes_ReadState(nes, p->m_fpMovie);
                fseek(p->m_fpMovie, p->m_hedMovie.MovieOffset, SEEK_SET);
                // I will say that it is playing.
                p->m_bMoviePlay = MM_TRUE;
            }
        }

        do
        {
            // Read
            if (fread(&Data, sizeof(Data), 1, p->m_fpMovie) != 1)
            {
                // The end?
                (*(p->callback.MovieFinish))(p);
                mmEmuMovie_MovieStop(p);
                return;
            }

            // command?
            if ((Data & 0xF0) == 0xF0)
            {
                if (Data == 0xF0)
                {
                    // Read
                    if (fread(&wData, sizeof(wData), 1, p->m_fpMovie) != 1)
                    {
                        // The end?
                        (*(p->callback.MovieFinish))(p);
                        mmEmuMovie_MovieStop(p);
                        return;
                    }
                    if (wData < 0x0100)
                    {
                        mmEmuNes_Command(nes, ((mmInt_t)wData));
                    }
                    else
                    {
                        // Extended controller
                        mmEmuNes_CommandParam(nes, MM_EMU_NES_NESCMD_EXCONTROLLER, ((mmInt_t)wData) & 0x00FF);
                    }
                }
                else
                {
                    if (Data == 0xF3)
                    {
                        // Read
                        if (fread(&dwData, sizeof(dwData), 1, p->m_fpMovie) != 1)
                        {
                            // The end?
                            (*(p->callback.MovieFinish))(p);
                            mmEmuMovie_MovieStop(p);
                            return;
                        }
                        mmEmuPad_SetSyncExData(&nes->pad, dwData);
                    }
                    else
                    {
                        // Data is broken? The end.
                        (*(p->callback.MovieFinish))(p);
                        mmEmuMovie_MovieStop(p);
                        return;
                    }
                }
            }
            else
            {
                // Unused player sinks away.
                while (!(p->m_MovieControl & (1 << num)) && (num < 4))
                {
                    PadBuf[num] = 0;
                    num++;
                }
                PadBuf[num] = Data;
                num++;
                // Unused player sinks away.
                while (!(p->m_MovieControl & (1 << num)) && (num < 4))
                {
                    PadBuf[num] = 0;
                    num++;
                }
            }
        } while (num < 4);

        dwData = (((mmUInt32_t)PadBuf[3]) << 24) | (((mmUInt32_t)PadBuf[2]) << 16) | (((mmUInt32_t)PadBuf[1]) << 8) | ((mmUInt32_t)PadBuf[0]);
        mmEmuPad_SetSyncData(&nes->pad, dwData);

        // Increase counter
        p->m_MovieStep++;
    }

    nes->m_CommandRequest = 0;
}
MM_EXPORT_EMU void mmEmuMovie_GetMovieInfo(const struct mmEmuMovie* p, struct mmEmuMovieInfo* info)
{
    info->wRecVersion = p->m_hedMovie.RecordVersion;
    info->wVersion = p->m_hedMovie.BlockVersion;
    info->dwRecordFrames = p->m_hedMovie.MovieStep;
    info->dwRecordTimes = p->m_hedMovie.RecordTimes;
}

