/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper092.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuExtSoundFile.h"

#include "core/mmLogger.h"


MM_EXPORT_EMU void mmEmuMapper092_Init(struct mmEmuMapper092* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper092_Reset;
    p->super.Write = &mmEmuMapper092_Write;
    //p->super.Read = &mmEmuMapper092_Read;
    //p->super.ReadLow = &mmEmuMapper092_ReadLow;
    //p->super.WriteLow = &mmEmuMapper092_WriteLow;
    //p->super.ExRead = &mmEmuMapper092_ExRead;
    //p->super.ExWrite = &mmEmuMapper092_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper092_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper092_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper092_HSync;
    //p->super.VSync = &mmEmuMapper092_VSync;
    //p->super.Clock = &mmEmuMapper092_Clock;
    //p->super.PPULatch = &mmEmuMapper092_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper092_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper092_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper092_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper092_IsStateSave;
    //p->super.SaveState = &mmEmuMapper092_SaveState;
    //p->super.LoadState = &mmEmuMapper092_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper092_Destroy(struct mmEmuMapper092* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper092_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper092* p = (struct mmEmuMapper092*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper092_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper092* p = (struct mmEmuMapper092*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    struct mmEmuAudioConfig* audio_config = &nes->config.audio;

    //DEBUGOUT( "A:%04X D:%02X\n", addr, data );

    data = addr & 0xFF;

    if (addr >= 0x9000)
    {
        if ((data & 0xF0) == 0xD0)
        {
            mmEmuMmu_SetPROM16KBank(mmu, 6, data & 0x0F);
        }
        else if ((data & 0xF0) == 0xE0)
        {
            mmEmuMmu_SetVROM8KBank(mmu, data & 0x0F);
        }
    }
    else
    {
        if ((data & 0xF0) == 0xB0)
        {
            mmEmuMmu_SetPROM16KBank(mmu, 6, data & 0x0F);
        }
        else if ((data & 0xF0) == 0x70)
        {
            mmEmuMmu_SetVROM8KBank(mmu, data & 0x0F);
        }
        else if ((data & 0xF0) == 0xC0)
        {
            static const mmInt_t tbl[] =
            {
                3, 4, 5, 6, 0, 1, 2, 7,
                9,10, 8,11,13,12,14,15,
            };

            // Should it be an OSD ...
            if (audio_config->bExtraSoundEnable)
            {
                // struct mmLogger* gLogger = mmLogger_Instance();
                //
                // mmLogger_LogI(gLogger, "%s %d CODE %02X", __FUNCTION__, __LINE__, data);

                mmEmuNes_EsfAllStop(nes);
                mmEmuNes_EsfPlay(nes, MM_EMU_ESF_MOEPRO_STRIKE + tbl[data & 0x0F]);
            }
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper092_Produce(void)
{
    struct mmEmuMapper092* p = (struct mmEmuMapper092*)mmMalloc(sizeof(struct mmEmuMapper092));
    mmEmuMapper092_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper092_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper092* p = (struct mmEmuMapper092*)(m);
    mmEmuMapper092_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER092 =
{
    &__static_mmEmuMapper092_Produce,
    &__static_mmEmuMapper092_Recycle,
};
