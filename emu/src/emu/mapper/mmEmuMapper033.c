/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper033.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper033_Init(struct mmEmuMapper033* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 7);

    p->patch = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    //
    p->super.Reset = &mmEmuMapper033_Reset;
    p->super.Write = &mmEmuMapper033_Write;
    //p->super.Read = &mmEmuMapper033_Read;
    //p->super.ReadLow = &mmEmuMapper033_ReadLow;
    //p->super.WriteLow = &mmEmuMapper033_WriteLow;
    //p->super.ExRead = &mmEmuMapper033_ExRead;
    //p->super.ExWrite = &mmEmuMapper033_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper033_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper033_ExCmdWrite;
    p->super.HSync = &mmEmuMapper033_HSync;
    //p->super.VSync = &mmEmuMapper033_VSync;
    //p->super.Clock = &mmEmuMapper033_Clock;
    //p->super.PPULatch = &mmEmuMapper033_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper033_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper033_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper033_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper033_IsStateSave;
    p->super.SaveState = &mmEmuMapper033_SaveState;
    p->super.LoadState = &mmEmuMapper033_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper033_Destroy(struct mmEmuMapper033* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 7);

    p->patch = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
}

MM_EXPORT_EMU void mmEmuMapper033_SetBank(struct mmEmuMapper033* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetVROM2KBank(mmu, 0, p->reg[0]);
    mmEmuMmu_SetVROM2KBank(mmu, 2, p->reg[1]);

    mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[2]);
    mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[3]);
    mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[4]);
    mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[5]);

    //mmEmuMmu_SetVROM2KBank(mmu, 0, p->reg[0]);
    //mmEmuMmu_SetVROM2KBank(mmu, 2, p->reg[1]);

    //if (p->reg[6] & 0x01)
    //{
    //  mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[2]);
    //  mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[3]);
    //  mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[4]);
    //  mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[5]);
    //}
    //else {
    //  SetVROM_2K_Bank(4, p->reg[0]);
    //  SetVROM_2K_Bank(6, p->reg[1]);
    //}
}

MM_EXPORT_EMU void mmEmuMapper033_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper033* p = (struct mmEmuMapper033*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    p->reg[0] = 0;
    p->reg[1] = 2;
    p->reg[2] = 4;
    p->reg[3] = 5;
    p->reg[4] = 6;
    p->reg[5] = 7;
    p->reg[6] = 1;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_8K_SIZE)
    {
        mmEmuMapper033_SetBank(p);
    }

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    // Check For Old #33 games.... (CRC code by NesToy)
    if (crc == 0x5e9bc161 ||        // Akira(J)
        crc == 0xecdbafa4 ||        // Bakushou!! Jinsei Gekijou(J)
        crc == 0x59cd0c31 ||        // Don Doko Don(J)
        crc == 0x837c1342 ||        // Golf Ko Open(J)
        crc == 0x42d893e4 ||        // Operation Wolf(J)
        crc == 0x1388aeb9 ||        // Operation Wolf(U)
        crc == 0x07ee6d8f ||        // Power Blazer(J)
        crc == 0x5193fb54 ||        // Takeshi no Sengoku Fuuunji(J)
        crc == 0xa71c3452)          // Insector X(J)
    {
        p->patch = 1;
    }

    mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_RENDER);

    if (crc == 0x202df297)          // Captain Saver(J) 
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
    if (crc == 0x63bb86b5)          // The Jetsons(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
}
MM_EXPORT_EMU void mmEmuMapper033_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper033* p = (struct mmEmuMapper033*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //  LOG( "Mapper033 addr=%04X data=%02X", addr&0xFFFF, data&0xFF );

    switch (addr)
    {
    case 0x8000:
        if (p->patch)
        {
            if (data & 0x40)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            mmEmuMmu_SetPROM8KBank(mmu, 4, data & 0x1F);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        }
        break;
    case 0x8001:
        if (p->patch)
        {
            mmEmuMmu_SetPROM8KBank(mmu, 5, data & 0x1F);
        }
        else
        {
            mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        }
        break;

    case 0x8002:
        p->reg[0] = data;
        mmEmuMapper033_SetBank(p);
        break;
    case 0x8003:
        p->reg[1] = data;
        mmEmuMapper033_SetBank(p);
        break;
    case 0xA000:
        p->reg[2] = data;
        mmEmuMapper033_SetBank(p);
        break;
    case 0xA001:
        p->reg[3] = data;
        mmEmuMapper033_SetBank(p);
        break;
    case 0xA002:
        p->reg[4] = data;
        mmEmuMapper033_SetBank(p);
        break;
    case 0xA003:
        p->reg[5] = data;
        mmEmuMapper033_SetBank(p);
        break;

#if 0
    case 0xC003:
    case 0xE003:
        p->reg[6] = data;
        mmEmuMapper033_SetBank(p);
        break;

    case 0xC000:
        p->irq_counter = data;
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xC001:
    case 0xC002:
    case 0xE001:
    case 0xE002:
        p->irq_enable = data;
        //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
#else
    case 0xC000:
        p->irq_latch = data;
        p->irq_counter = p->irq_latch;
        break;
    case 0xC001:
        p->irq_counter = p->irq_latch;
        break;
    case 0xC002:
        p->irq_enable = 1;
        break;
    case 0xC003:
        p->irq_enable = 0;
        break;

    case 0xE001:
    case 0xE002:
    case 0xE003:
        break;
#endif

    case 0xE000:
        if (data & 0x40)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper033_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper033* p = (struct mmEmuMapper033*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

#if 0
    //mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
    if (scanline >= 0 && scanline <= 239)
    {
        if (mm_emu_nes_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (p->irq_counter == 0xFF)
                {
                    p->irq_enable = 0;
                    p->irq_counter = 0;
                    //mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_TRIGGER);
                }
                else
                {
                    p->irq_counter++;
                }
            }
        }
    }
#else
    if (scanline >= 0 && scanline <= 239 && mmEmuPpu_IsDispON(&nes->ppu))
    {
        if (p->irq_enable)
        {
            if (++p->irq_counter == 0)
            {
                p->irq_enable = 0;
                p->irq_counter = 0;
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_TRIGGER);
            }
        }
    }
#endif
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper033_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper033_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper033* p = (struct mmEmuMapper033*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 7);
    buffer[7] = p->irq_enable;
    buffer[8] = p->irq_counter;
    buffer[9] = p->irq_latch;
}
MM_EXPORT_EMU void mmEmuMapper033_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper033* p = (struct mmEmuMapper033*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 7);
    p->irq_enable = buffer[7];
    p->irq_counter = buffer[8];
    p->irq_latch = buffer[9];
}

static struct mmEmuMapper* __static_mmEmuMapper033_Produce(void)
{
    struct mmEmuMapper033* p = (struct mmEmuMapper033*)mmMalloc(sizeof(struct mmEmuMapper033));
    mmEmuMapper033_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper033_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper033* p = (struct mmEmuMapper033*)(m);
    mmEmuMapper033_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER033 =
{
    &__static_mmEmuMapper033_Produce,
    &__static_mmEmuMapper033_Recycle,
};
