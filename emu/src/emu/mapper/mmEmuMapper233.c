/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper233.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper233_Init(struct mmEmuMapper233* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper233_Reset;
    p->super.Write = &mmEmuMapper233_Write;
    //p->super.Read = &mmEmuMapper233_Read;
    //p->super.ReadLow = &mmEmuMapper233_ReadLow;
    //p->super.WriteLow = &mmEmuMapper233_WriteLow;
    //p->super.ExRead = &mmEmuMapper233_ExRead;
    //p->super.ExWrite = &mmEmuMapper233_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper233_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper233_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper233_HSync;
    //p->super.VSync = &mmEmuMapper233_VSync;
    //p->super.Clock = &mmEmuMapper233_Clock;
    //p->super.PPULatch = &mmEmuMapper233_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper233_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper233_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper233_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper233_IsStateSave;
    //p->super.SaveState = &mmEmuMapper233_SaveState;
    //p->super.LoadState = &mmEmuMapper233_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper233_Destroy(struct mmEmuMapper233* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper233_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper233* p = (struct mmEmuMapper233*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
}
MM_EXPORT_EMU void mmEmuMapper233_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper233* p = (struct mmEmuMapper233*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (data & 0x20)
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, (data & 0x1F) * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, (data & 0x1F) * 2 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, (data & 0x1F) * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 7, (data & 0x1F) * 2 + 1);
    }
    else
    {
        mmByte_t bank = (data & 0x1E) >> 1;

        mmEmuMmu_SetPROM8KBank(mmu, 4, bank * 4 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, bank * 4 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, bank * 4 + 2);
        mmEmuMmu_SetPROM8KBank(mmu, 7, bank * 4 + 3);
    }

    if ((data & 0xC0) == 0x00)
    {
        mmEmuMmu_SetVRAMMirrorArray(mmu, 0, 0, 0, 1);
    }
    else if ((data & 0xC0) == 0x40)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
    else if ((data & 0xC0) == 0x80)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper233_Produce(void)
{
    struct mmEmuMapper233* p = (struct mmEmuMapper233*)mmMalloc(sizeof(struct mmEmuMapper233));
    mmEmuMapper233_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper233_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper233* p = (struct mmEmuMapper233*)(m);
    mmEmuMapper233_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER233 =
{
    &__static_mmEmuMapper233_Produce,
    &__static_mmEmuMapper233_Recycle,
};
