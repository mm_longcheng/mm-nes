/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper181.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper181_Init(struct mmEmuMapper181* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper181_Reset;
    //p->super.Write = &mmEmuMapper181_Write;
    //p->super.Read = &mmEmuMapper181_Read;
    //p->super.ReadLow = &mmEmuMapper181_ReadLow;
    p->super.WriteLow = &mmEmuMapper181_WriteLow;
    //p->super.ExRead = &mmEmuMapper181_ExRead;
    //p->super.ExWrite = &mmEmuMapper181_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper181_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper181_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper181_HSync;
    //p->super.VSync = &mmEmuMapper181_VSync;
    //p->super.Clock = &mmEmuMapper181_Clock;
    //p->super.PPULatch = &mmEmuMapper181_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper181_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper181_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper181_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper181_IsStateSave;
    //p->super.SaveState = &mmEmuMapper181_SaveState;
    //p->super.LoadState = &mmEmuMapper181_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper181_Destroy(struct mmEmuMapper181* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper181_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper181* p = (struct mmEmuMapper181*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper181_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper181* p = (struct mmEmuMapper181*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "$%04X:$%02X\n", addr, data );
    if (addr == 0x4120)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (data & 0x08) >> 3);
        mmEmuMmu_SetVROM8KBank(mmu, data & 0x07);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper181_Produce(void)
{
    struct mmEmuMapper181* p = (struct mmEmuMapper181*)mmMalloc(sizeof(struct mmEmuMapper181));
    mmEmuMapper181_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper181_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper181* p = (struct mmEmuMapper181*)(m);
    mmEmuMapper181_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER181 =
{
    &__static_mmEmuMapper181_Produce,
    &__static_mmEmuMapper181_Recycle,
};
