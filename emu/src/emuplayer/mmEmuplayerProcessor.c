/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuplayerProcessor.h"


void mmEmuplayerProcessor_Init(struct mmEmuplayerProcessor* p)
{
    mmEmuEmulatorProcessor_Init(&p->super);
    mmEmulatorAudio_Init(&p->audio);

    p->super.ThreadEnter = &mmEmuplayerProcessor_ThreadEnter;
    p->super.ThreadLeave = &mmEmuplayerProcessor_ThreadLeave;
    p->super.ProcessAudio = &mmEmuplayerProcessor_ProcessAudio;
    p->super.ProcessFrame = &mmEmuplayerProcessor_ProcessFrame;
}
void mmEmuplayerProcessor_Destroy(struct mmEmuplayerProcessor* p)
{
    mmEmuEmulatorProcessor_Destroy(&p->super);
    mmEmulatorAudio_Destroy(&p->audio);
}

void mmEmuplayerProcessor_Start(struct mmEmuplayerProcessor* p)
{

}
void mmEmuplayerProcessor_Interrupt(struct mmEmuplayerProcessor* p)
{

}
void mmEmuplayerProcessor_Shutdown(struct mmEmuplayerProcessor* p)
{

}
void mmEmuplayerProcessor_Join(struct mmEmuplayerProcessor* p)
{

}

void mmEmuplayerProcessor_ThreadEnter(struct mmEmuEmulatorProcessor* super)
{
    struct mmEmuplayerProcessor* p = (struct mmEmuplayerProcessor*)(super);

    mmEmulatorAudio_FinishLaunching(&p->audio);
    mmEmulatorAudio_Play(&p->audio);
}
void mmEmuplayerProcessor_ThreadLeave(struct mmEmuEmulatorProcessor* super)
{
    struct mmEmuplayerProcessor* p = (struct mmEmuplayerProcessor*)(super);

    mmEmulatorAudio_Stop(&p->audio);
    mmEmulatorAudio_BeforeTerminate(&p->audio);
}
int mmEmuplayerProcessor_ProcessAudio(struct mmEmuEmulatorProcessor* super, mmByte_t* buffer, size_t length)
{
    struct mmEmuplayerProcessor* p = (struct mmEmuplayerProcessor*)(super);

    return mmEmulatorAudio_ProcessBuffer(&p->audio, buffer, length);
}
int mmEmuplayerProcessor_ProcessFrame(struct mmEmuEmulatorProcessor* super, mmByte_t* buffer, size_t length)
{
    return 0;
}
