/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper112.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper112_Init(struct mmEmuMapper112* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);
    p->prg0 = 0;
    p->prg1 = 0;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    //
    p->super.Reset = &mmEmuMapper112_Reset;
    p->super.Write = &mmEmuMapper112_Write;
    //p->super.Read = &mmEmuMapper112_Read;
    //p->super.ReadLow = &mmEmuMapper112_ReadLow;
    //p->super.WriteLow = &mmEmuMapper112_WriteLow;
    //p->super.ExRead = &mmEmuMapper112_ExRead;
    //p->super.ExWrite = &mmEmuMapper112_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper112_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper112_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper112_HSync;
    //p->super.VSync = &mmEmuMapper112_VSync;
    //p->super.Clock = &mmEmuMapper112_Clock;
    //p->super.PPULatch = &mmEmuMapper112_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper112_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper112_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper112_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper112_IsStateSave;
    p->super.SaveState = &mmEmuMapper112_SaveState;
    p->super.LoadState = &mmEmuMapper112_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper112_Destroy(struct mmEmuMapper112* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);
    p->prg0 = 0;
    p->prg1 = 0;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;
}

MM_EXPORT_EMU void mmEmuMapper112_SetBankCPU(struct mmEmuMapper112* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper112_SetBankPPU(struct mmEmuMapper112* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[2] & 0x02)
    {
        mmEmuMmu_SetVROM8KBankArray(mmu, p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1, p->chr4, p->chr5, p->chr6, p->chr7);
    }
    else
    {
        mmEmuMmu_SetVROM8KBankArray(
            mmu,
            ((p->reg[3] << 6) & 0x100) + p->chr01,
            ((p->reg[3] << 6) & 0x100) + p->chr01 + 1,
            ((p->reg[3] << 5) & 0x100) + p->chr23,
            ((p->reg[3] << 5) & 0x100) + p->chr23 + 1,
            ((p->reg[3] << 4) & 0x100) + p->chr4,
            ((p->reg[3] << 3) & 0x100) + p->chr5,
            ((p->reg[3] << 2) & 0x100) + p->chr6,
            ((p->reg[3] << 1) & 0x100) + p->chr7);
    }
}

MM_EXPORT_EMU void mmEmuMapper112_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper112* p = (struct mmEmuMapper112*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 4);

    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper112_SetBankCPU(p);

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    mmEmuMapper112_SetBankPPU(p);
}
MM_EXPORT_EMU void mmEmuMapper112_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper112* p = (struct mmEmuMapper112*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper112_SetBankCPU(p);
        mmEmuMapper112_SetBankPPU(p);
        break;
    case 0xA000:
        p->reg[1] = data;
        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->prg0 = (data&(mmu->PROM_08K_SIZE - 1));
            mmEmuMapper112_SetBankCPU(p);
            break;
        case 0x01:
            p->prg1 = (data&(mmu->PROM_08K_SIZE - 1));
            mmEmuMapper112_SetBankCPU(p);
            break;
        case 0x02:
            p->chr01 = data & 0xFE;
            mmEmuMapper112_SetBankPPU(p);
            break;
        case 0x03:
            p->chr23 = data & 0xFE;
            mmEmuMapper112_SetBankPPU(p);
            break;
        case 0x04:
            p->chr4 = data;
            mmEmuMapper112_SetBankPPU(p);
            break;
        case 0x05:
            p->chr5 = data;
            mmEmuMapper112_SetBankPPU(p);
            break;
        case 0x06:
            p->chr6 = data;
            mmEmuMapper112_SetBankPPU(p);
            break;
        case 0x07:
            p->chr7 = data;
            mmEmuMapper112_SetBankPPU(p);
            break;
        default:
            break;
        }
        break;

    case 0xC000:
        p->reg[3] = data;
        mmEmuMapper112_SetBankPPU(p);

    case 0xE000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        mmEmuMapper112_SetBankPPU(p);
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper112_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper112_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper112* p = (struct mmEmuMapper112*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 4);
    buffer[4] = p->chr01;
    buffer[5] = p->chr23;
    buffer[6] = p->chr4;
    buffer[7] = p->chr5;
    buffer[8] = p->chr6;
    buffer[9] = p->chr7;
}
MM_EXPORT_EMU void mmEmuMapper112_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper112* p = (struct mmEmuMapper112*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 4);
    p->chr01 = buffer[4];
    p->chr23 = buffer[5];
    p->chr4 = buffer[6];
    p->chr5 = buffer[7];
    p->chr6 = buffer[8];
    p->chr7 = buffer[9];
}

static struct mmEmuMapper* __static_mmEmuMapper112_Produce(void)
{
    struct mmEmuMapper112* p = (struct mmEmuMapper112*)mmMalloc(sizeof(struct mmEmuMapper112));
    mmEmuMapper112_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper112_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper112* p = (struct mmEmuMapper112*)(m);
    mmEmuMapper112_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER112 =
{
    &__static_mmEmuMapper112_Produce,
    &__static_mmEmuMapper112_Recycle,
};
