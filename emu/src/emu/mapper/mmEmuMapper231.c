/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper231.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper231_Init(struct mmEmuMapper231* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper231_Reset;
    p->super.Write = &mmEmuMapper231_Write;
    //p->super.Read = &mmEmuMapper231_Read;
    //p->super.ReadLow = &mmEmuMapper231_ReadLow;
    //p->super.WriteLow = &mmEmuMapper231_WriteLow;
    //p->super.ExRead = &mmEmuMapper231_ExRead;
    //p->super.ExWrite = &mmEmuMapper231_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper231_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper231_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper231_HSync;
    //p->super.VSync = &mmEmuMapper231_VSync;
    //p->super.Clock = &mmEmuMapper231_Clock;
    //p->super.PPULatch = &mmEmuMapper231_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper231_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper231_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper231_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper231_IsStateSave;
    //p->super.SaveState = &mmEmuMapper231_SaveState;
    //p->super.LoadState = &mmEmuMapper231_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper231_Destroy(struct mmEmuMapper231* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper231_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper231* p = (struct mmEmuMapper231*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper231_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper231* p = (struct mmEmuMapper231*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr & 0x0020)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (mmByte_t)(addr >> 1));
    }
    else
    {
        mmByte_t bank = addr & 0x1E;
        mmEmuMmu_SetPROM8KBank(mmu, 4, bank * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 5, bank * 2 + 1);
        mmEmuMmu_SetPROM8KBank(mmu, 6, bank * 2 + 0);
        mmEmuMmu_SetPROM8KBank(mmu, 7, bank * 2 + 1);
    }

    if (addr & 0x0080)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper231_Produce(void)
{
    struct mmEmuMapper231* p = (struct mmEmuMapper231*)mmMalloc(sizeof(struct mmEmuMapper231));
    mmEmuMapper231_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper231_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper231* p = (struct mmEmuMapper231*)(m);
    mmEmuMapper231_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER231 =
{
    &__static_mmEmuMapper231_Produce,
    &__static_mmEmuMapper231_Recycle,
};
