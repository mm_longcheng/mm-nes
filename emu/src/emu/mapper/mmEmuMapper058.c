/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper058.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper058_Init(struct mmEmuMapper058* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper058_Reset;
    p->super.Write = &mmEmuMapper058_Write;
    //p->super.Read = &mmEmuMapper058_Read;
    //p->super.ReadLow = &mmEmuMapper058_ReadLow;
    //p->super.WriteLow = &mmEmuMapper058_WriteLow;
    //p->super.ExRead = &mmEmuMapper058_ExRead;
    //p->super.ExWrite = &mmEmuMapper058_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper058_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper058_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper058_HSync;
    //p->super.VSync = &mmEmuMapper058_VSync;
    //p->super.Clock = &mmEmuMapper058_Clock;
    //p->super.PPULatch = &mmEmuMapper058_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper058_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper058_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper058_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper058_IsStateSave;
    //p->super.SaveState = &mmEmuMapper058_SaveState;
    //p->super.LoadState = &mmEmuMapper058_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper058_Destroy(struct mmEmuMapper058* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper058_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper058* p = (struct mmEmuMapper058*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 0, 1);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper058_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper058* p = (struct mmEmuMapper058*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr & 0x40)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, addr & 0x07);
        mmEmuMmu_SetPROM16KBank(mmu, 6, addr & 0x07);
    }
    else
    {
        mmEmuMmu_SetPROM32KBank(mmu, (addr & 0x06) >> 1);
    }

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, (addr & 0x38) >> 3);
    }

    if (data & 0x02)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper058_Produce(void)
{
    struct mmEmuMapper058* p = (struct mmEmuMapper058*)mmMalloc(sizeof(struct mmEmuMapper058));
    mmEmuMapper058_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper058_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper058* p = (struct mmEmuMapper058*)(m);
    mmEmuMapper058_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER058 =
{
    &__static_mmEmuMapper058_Produce,
    &__static_mmEmuMapper058_Recycle,
};
