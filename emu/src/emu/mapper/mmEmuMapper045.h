/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper045_h__
#define __mmEmuMapper045_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper045  1000000-in-1                                              //
//----------------------------------------------------------------------//
struct mmEmuMapper045
{
    struct mmEmuMapper super;

    mmByte_t    reg[8];
    mmByte_t    patch;
    mmByte_t    prg0, prg1, prg2, prg3;
    mmByte_t    chr0, chr1, chr2, chr3, chr4, chr5, chr6, chr7;
    mmByte_t    p[4];
    mmInt_t     c[8];
    mmByte_t    irq_enable;
    mmByte_t    irq_counter;
    mmByte_t    irq_latch;
    mmByte_t    irq_latched;
    mmByte_t    irq_reset;
};

MM_EXPORT_EMU void mmEmuMapper045_Init(struct mmEmuMapper045* p);
MM_EXPORT_EMU void mmEmuMapper045_Destroy(struct mmEmuMapper045* p);

MM_EXPORT_EMU void mmEmuMapper045_SetBankCPU4(struct mmEmuMapper045* p, mmInt_t data);
MM_EXPORT_EMU void mmEmuMapper045_SetBankCPU5(struct mmEmuMapper045* p, mmInt_t data);
MM_EXPORT_EMU void mmEmuMapper045_SetBankCPU6(struct mmEmuMapper045* p, mmInt_t data);
MM_EXPORT_EMU void mmEmuMapper045_SetBankCPU7(struct mmEmuMapper045* p, mmInt_t data);
MM_EXPORT_EMU void mmEmuMapper045_SetBankPPU(struct mmEmuMapper045* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper045_Reset(struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper045_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper045_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);

MM_EXPORT_EMU void mmEmuMapper045_HSync(struct mmEmuMapper* super, mmInt_t scanline);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper045_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper045_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper045_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER045;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper045_h__
