/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper108.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper108_Init(struct mmEmuMapper108* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper108_Reset;
    p->super.Write = &mmEmuMapper108_Write;
    //p->super.Read = &mmEmuMapper108_Read;
    //p->super.ReadLow = &mmEmuMapper108_ReadLow;
    //p->super.WriteLow = &mmEmuMapper108_WriteLow;
    //p->super.ExRead = &mmEmuMapper108_ExRead;
    //p->super.ExWrite = &mmEmuMapper108_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper108_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper108_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper108_HSync;
    //p->super.VSync = &mmEmuMapper108_VSync;
    //p->super.Clock = &mmEmuMapper108_Clock;
    //p->super.PPULatch = &mmEmuMapper108_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper108_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper108_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper108_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper108_IsStateSave;
    //p->super.SaveState = &mmEmuMapper108_SaveState;
    //p->super.LoadState = &mmEmuMapper108_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper108_Destroy(struct mmEmuMapper108* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper108_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper108* p = (struct mmEmuMapper108*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0xC, 0xD, 0xE, 0xF);
    mmEmuMmu_SetPROM8KBank(mmu, 3, 0);
}
MM_EXPORT_EMU void mmEmuMapper108_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper108* p = (struct mmEmuMapper108*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM8KBank(mmu, 3, data);
}

static struct mmEmuMapper* __static_mmEmuMapper108_Produce(void)
{
    struct mmEmuMapper108* p = (struct mmEmuMapper108*)mmMalloc(sizeof(struct mmEmuMapper108));
    mmEmuMapper108_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper108_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper108* p = (struct mmEmuMapper108*)(m);
    mmEmuMapper108_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER108 =
{
    &__static_mmEmuMapper108_Produce,
    &__static_mmEmuMapper108_Recycle,
};
