/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper090.h"
#include "emu/mmEmuNes.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuMapper090_Init(struct mmEmuMapper090* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;

    mmMemset(p->prg_reg, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->nth_reg, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->ntl_reg, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->chh_reg, 0, sizeof(mmByte_t) * 8);
    mmMemset(p->chl_reg, 0, sizeof(mmByte_t) * 8);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_preset = 0;
    p->irq_offset = 0;

    p->prg_6000 = 0;
    p->prg_E000 = 0;
    p->prg_size = 0;
    p->chr_size = 0;
    p->mir_mode = 0;
    p->mir_type = 0;

    p->key_val = 0;
    p->mul_val1 = 0;
    p->mul_val2 = 0;
    p->sw_val = 0;

    //
    p->super.Reset = &mmEmuMapper090_Reset;
    p->super.Write = &mmEmuMapper090_Write;
    //p->super.Read = &mmEmuMapper090_Read;
    p->super.ReadLow = &mmEmuMapper090_ReadLow;
    p->super.WriteLow = &mmEmuMapper090_WriteLow;
    //p->super.ExRead = &mmEmuMapper090_ExRead;
    //p->super.ExWrite = &mmEmuMapper090_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper090_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper090_ExCmdWrite;
    p->super.HSync = &mmEmuMapper090_HSync;
    //p->super.VSync = &mmEmuMapper090_VSync;
    p->super.Clock = &mmEmuMapper090_Clock;
    //p->super.PPULatch = &mmEmuMapper090_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper090_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper090_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper090_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper090_IsStateSave;
    p->super.SaveState = &mmEmuMapper090_SaveState;
    p->super.LoadState = &mmEmuMapper090_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper090_Destroy(struct mmEmuMapper090* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;

    mmMemset(p->prg_reg, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->nth_reg, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->ntl_reg, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->chh_reg, 0, sizeof(mmByte_t) * 8);
    mmMemset(p->chl_reg, 0, sizeof(mmByte_t) * 8);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_preset = 0;
    p->irq_offset = 0;

    p->prg_6000 = 0;
    p->prg_E000 = 0;
    p->prg_size = 0;
    p->chr_size = 0;
    p->mir_mode = 0;
    p->mir_type = 0;

    p->key_val = 0;
    p->mul_val1 = 0;
    p->mul_val2 = 0;
    p->sw_val = 0;
}

MM_EXPORT_EMU void mmEmuMapper090_SetBankCPU(struct mmEmuMapper090* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->prg_size == 0)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 4, mmu->PROM_08K_SIZE - 3, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        if (p->prg_size == 1)
        {
            mmEmuMmu_SetPROM32KBankArray(mmu, p->prg_reg[1] * 2, p->prg_reg[1] * 2 + 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
        }
        else
        {
            if (p->prg_size == 2)
            {
                if (p->prg_E000)
                {
                    mmEmuMmu_SetPROM32KBankArray(mmu, p->prg_reg[0], p->prg_reg[1], p->prg_reg[2], p->prg_reg[3]);
                }
                else
                {
                    if (p->prg_6000)
                    {
                        mmEmuMmu_SetPROM8KBank(mmu, 3, p->prg_reg[3]);
                    }
                    mmEmuMmu_SetPROM32KBankArray(mmu, p->prg_reg[0], p->prg_reg[1], p->prg_reg[2], mmu->PROM_08K_SIZE - 1);
                }
            }
            else
            {
                mmEmuMmu_SetPROM32KBankArray(mmu, p->prg_reg[3], p->prg_reg[2], p->prg_reg[1], p->prg_reg[0]);
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper090_SetBankPPU(struct mmEmuMapper090* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t bank[8];

    mmInt_t i = 0;

    for (i = 0; i < 8; i++)
    {
        bank[i] = ((mmInt_t)p->chh_reg[i] << 8) | ((mmInt_t)p->chl_reg[i]);
    }

    if (p->chr_size == 0)
    {
        mmEmuMmu_SetVROM8KBank(mmu, bank[0]);
    }
    else
    {
        if (p->chr_size == 1)
        {
            mmEmuMmu_SetVROM4KBank(mmu, 0, bank[0]);
            mmEmuMmu_SetVROM4KBank(mmu, 4, bank[4]);
        }
        else
        {
            if (p->chr_size == 2)
            {
                mmEmuMmu_SetVROM2KBank(mmu, 0, bank[0]);
                mmEmuMmu_SetVROM2KBank(mmu, 2, bank[2]);
                mmEmuMmu_SetVROM2KBank(mmu, 4, bank[4]);
                mmEmuMmu_SetVROM2KBank(mmu, 6, bank[6]);
            }
            else
            {
                mmEmuMmu_SetVROM8KBankArray(mmu, bank[0], bank[1], bank[2], bank[3], bank[4], bank[5], bank[6], bank[7]);
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper090_SetBankVRAM(struct mmEmuMapper090* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t bank[4];

    mmInt_t i = 0;

    for (i = 0; i < 4; i++)
    {
        bank[i] = ((mmInt_t)p->nth_reg[i] << 8) | ((mmInt_t)p->ntl_reg[i]);
    }

    if (!p->patch && p->mir_mode)
    {
        for (i = 0; i < 4; i++)
        {
            if (!p->nth_reg[i] && (p->ntl_reg[i] == (mmByte_t)i))
            {
                p->mir_mode = 0;
            }
        }

        if (p->mir_mode)
        {
            mmEmuMmu_SetVROM1KBank(mmu, 8, bank[0]);
            mmEmuMmu_SetVROM1KBank(mmu, 9, bank[1]);
            mmEmuMmu_SetVROM1KBank(mmu, 10, bank[2]);
            mmEmuMmu_SetVROM1KBank(mmu, 11, bank[3]);
        }
    }
    else
    {
        if (p->mir_type == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else
        {
            if (p->mir_type == 1)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
            }
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper090_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmInt_t i = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 4, mmu->PROM_08K_SIZE - 3, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x2a268152)
    {
        p->patch = 1;
    }
    if (crc == 0x2224b882)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }

    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_preset = 0;
    p->irq_offset = 0;

    p->prg_6000 = 0;
    p->prg_E000 = 0;
    p->prg_size = 0;
    p->chr_size = 0;
    p->mir_mode = 0;
    p->mir_type = 0;

    p->key_val = 0;
    p->mul_val1 = p->mul_val2 = 0;

    for (i = 0; i < 4; i++)
    {
        p->prg_reg[i] = mmu->PROM_08K_SIZE - 4 + i;
        p->ntl_reg[i] = 0;
        p->nth_reg[i] = 0;
        p->chl_reg[i] = i;
        p->chh_reg[i] = 0;
        p->chl_reg[i + 4] = i + 4;
        p->chh_reg[i + 4] = 0;
    }

    if (p->sw_val)
    {
        p->sw_val = 0x00;
    }
    else
    {
        p->sw_val = 0xFF;
    }

    //mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
}
MM_EXPORT_EMU mmByte_t mmEmuMapper090_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)(super);

    //struct mmLogger* gLogger = mmLogger_Instance();
    //
    //mmLogger_LogD(gLogger, "%s %d RD:%04X", __FUNCTION__, __LINE__, addr);

    switch (addr)
    {
    case 0x5000:
        return p->sw_val ? 0x00 : 0xFF;
    case 0x5800:
        return (mmByte_t)(p->mul_val1 * p->mul_val2);
    case 0x5801:
        return (mmByte_t)((p->mul_val1 * p->mul_val2) >> 8);
    case 0x5803:
        return p->key_val;
    default:
        break;
    }

    if (addr >= 0x6000)
    {
        return mmEmuMapper_ReadLow(super, addr);
    }

    //return p->sw_val ? 0x00 : 0xFF;
    return (mmByte_t)(addr >> 8);
}
MM_EXPORT_EMU void mmEmuMapper090_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)(super);

    //struct mmLogger* gLogger = mmLogger_Instance();
    //
    //mmLogger_LogD(gLogger, "%s %d WR:%04X %02X", __FUNCTION__, __LINE__, addr, data);

    if (addr == 0x5800)
    {
        p->mul_val1 = data;
    }
    else
    {
        if (addr == 0x5801)
        {
            p->mul_val2 = data;
        }
        else
        {
            if (addr == 0x5803)
            {
                p->key_val = data;
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper090_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF007)
    {
    case 0x8000:
    case 0x8001:
    case 0x8002:
    case 0x8003:
        p->prg_reg[addr & 3] = data;
        mmEmuMapper090_SetBankCPU(p);
        break;

    case 0x9000:
    case 0x9001:
    case 0x9002:
    case 0x9003:
    case 0x9004:
    case 0x9005:
    case 0x9006:
    case 0x9007:
        p->chl_reg[addr & 7] = data;
        mmEmuMapper090_SetBankPPU(p);
        break;

    case 0xA000:
    case 0xA001:
    case 0xA002:
    case 0xA003:
    case 0xA004:
    case 0xA005:
    case 0xA006:
    case 0xA007:
        p->chh_reg[addr & 7] = data;
        mmEmuMapper090_SetBankPPU(p);
        break;

    case 0xB000:
    case 0xB001:
    case 0xB002:
    case 0xB003:
        p->ntl_reg[addr & 3] = data;
        mmEmuMapper090_SetBankVRAM(p);
        break;

    case 0xB004:
    case 0xB005:
    case 0xB006:
    case 0xB007:
        p->nth_reg[addr & 3] = data;
        mmEmuMapper090_SetBankVRAM(p);
        break;

    case 0xC002:
        p->irq_enable = 0;
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xC003:
        p->irq_enable = 0xFF;
        p->irq_preset = 0xFF;
        break;
    case 0xC004:
        break;
    case 0xC005:
        if (p->irq_offset & 0x80)
        {
            p->irq_latch = data ^ (p->irq_offset | 1);
        }
        else
        {
            p->irq_latch = data | (p->irq_offset & 0x27);
        }
        p->irq_preset = 0xFF;
        break;
    case 0xC006:
        if (p->patch)
        {
            p->irq_offset = data;
        }
        break;

    case 0xD000:
        p->prg_6000 = data & 0x80;
        p->prg_E000 = data & 0x04;
        p->prg_size = data & 0x03;
        p->chr_size = (data & 0x18) >> 3;
        p->mir_mode = data & 0x20;
        mmEmuMapper090_SetBankCPU(p);
        mmEmuMapper090_SetBankPPU(p);
        mmEmuMapper090_SetBankVRAM(p);
        break;

    case 0xD001:
        p->mir_type = data & 0x03;
        mmEmuMapper090_SetBankVRAM(p);
        break;

    case 0xD003:
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper090_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_preset)
            {
                p->irq_counter = p->irq_latch;
                p->irq_preset = 0;
            }
            if (p->irq_counter)
            {
                p->irq_counter--;
            }
            if (!p->irq_counter)
            {
                if (p->irq_enable)
                {
                    //p->irq_occur = 0xFF;
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper090_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    //struct mmEmuMapper090* p = (struct mmEmuMapper090*)(super);

    //struct mmEmuNes* nes = p->super.nes;

    //if(p->irq_occur)
    //{
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    //}
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper090_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper090_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)(super);

    mmMemcpy(&buffer[0], &p->prg_reg[0], sizeof(mmByte_t) * 4);
    mmMemcpy(&buffer[4], &p->chh_reg[0], sizeof(mmByte_t) * 8);
    mmMemcpy(&buffer[12], &p->chl_reg[0], sizeof(mmByte_t) * 8);
    mmMemcpy(&buffer[20], &p->nth_reg[0], sizeof(mmByte_t) * 4);
    mmMemcpy(&buffer[24], &p->ntl_reg[0], sizeof(mmByte_t) * 4);
    buffer[28] = p->irq_enable;
    buffer[29] = p->irq_counter;
    buffer[30] = p->irq_latch;
    buffer[31] = p->prg_6000;
    buffer[32] = p->prg_E000;
    buffer[33] = p->prg_size;
    buffer[34] = p->chr_size;
    buffer[35] = p->mir_mode;
    buffer[36] = p->mir_type;
    buffer[37] = p->mul_val1;
    buffer[38] = p->mul_val2;
    buffer[39] = p->key_val;
    buffer[40] = p->irq_occur;
    buffer[41] = p->irq_preset;
    buffer[42] = p->irq_offset;
}
MM_EXPORT_EMU void mmEmuMapper090_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)(super);

    mmMemcpy(&p->prg_reg[0], &buffer[0], sizeof(mmByte_t) * 4);
    mmMemcpy(&p->chh_reg[0], &buffer[4], sizeof(mmByte_t) * 8);
    mmMemcpy(&p->chl_reg[0], &buffer[12], sizeof(mmByte_t) * 8);
    mmMemcpy(&p->nth_reg[0], &buffer[20], sizeof(mmByte_t) * 4);
    mmMemcpy(&p->ntl_reg[0], &buffer[24], sizeof(mmByte_t) * 4);
    p->irq_enable = buffer[28];
    p->irq_counter = buffer[29];
    p->irq_latch = buffer[30];
    p->prg_6000 = buffer[31];
    p->prg_E000 = buffer[32];
    p->prg_size = buffer[33];
    p->chr_size = buffer[34];
    p->mir_mode = buffer[35];
    p->mir_type = buffer[36];
    p->mul_val1 = buffer[37];
    p->mul_val2 = buffer[38];
    p->key_val = buffer[39];
    p->irq_occur = buffer[40];
    p->irq_preset = buffer[41];
    p->irq_offset = buffer[42];
}

static struct mmEmuMapper* __static_mmEmuMapper090_Produce(void)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)mmMalloc(sizeof(struct mmEmuMapper090));
    mmEmuMapper090_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper090_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper090* p = (struct mmEmuMapper090*)(m);
    mmEmuMapper090_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER090 =
{
    &__static_mmEmuMapper090_Produce,
    &__static_mmEmuMapper090_Recycle,
};
