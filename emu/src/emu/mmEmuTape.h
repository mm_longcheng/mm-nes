/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuTape_h__
#define __mmEmuTape_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuNes;

struct mmEmuTape
{
    struct mmEmuNes* nes;

    mmBool_t    m_bTapePlay;
    mmBool_t    m_bTapeRec;
    FILE*       m_fpTape;
    double      m_TapeCycles;
    mmByte_t    m_TapeIn;
    mmByte_t    m_TapeOut;
};

MM_EXPORT_EMU void mmEmuTape_Init(struct mmEmuTape* p);
MM_EXPORT_EMU void mmEmuTape_Destroy(struct mmEmuTape* p);

MM_EXPORT_EMU void mmEmuTape_SetParent(struct mmEmuTape* p, struct mmEmuNes* parent);

static mmInline mmBool_t mmEmuTape_IsTapePlay(const struct mmEmuTape* p) { return p->m_bTapePlay; }
static mmInline mmBool_t mmEmuTape_IsTapeRec(const struct mmEmuTape* p) { return p->m_bTapeRec; }
MM_EXPORT_EMU int mmEmuTape_TapePlay(struct mmEmuTape* p, const char* fname);
MM_EXPORT_EMU int mmEmuTape_TapeRec(struct mmEmuTape* p, const char* fname);
MM_EXPORT_EMU void mmEmuTape_TapeStop(struct mmEmuTape* p);
MM_EXPORT_EMU void mmEmuTape_Tape(struct mmEmuTape* p, mmInt_t cycles);

#include "core/mmSuffix.h"

#endif//__mmEmuTape_h__
