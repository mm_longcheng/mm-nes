/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuState_h__
#define __mmEmuState_h__

#include "core/mmCore.h"
#include "core/mmAlloc.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( push, 1 )
#endif

struct MM_ALIGNED(1) mmEmuFileHdr
{
    //  0123456789AB
    mmByte_t    ID[12];             // "NES ST"
    mmWord_t    Reserved;
    mmWord_t    BlockVersion;
};

// 
struct MM_ALIGNED(1) mmEmuFileHdr2
{
    //  0123456789AB
    mmByte_t    ID[12];             // "NES ST"
    mmWord_t    Reserved;
    mmWord_t    BlockVersion;       // 0x0200 / 0x0210(v0.60 after)

    mmUInt32_t  Ext0;               // ROM:program  FDS:program ID
    mmWord_t    Ext1;               // ROM:none     FDS:developer ID
    mmWord_t    Ext2;               // ROM:none     FDS:the number of discs
    mmSInt32_t  MovieStep;          // The number of frames when adding a movie
    mmSInt32_t  MovieOffset;        // Other file offsets when recording (rework) a movie
};

struct MM_ALIGNED(1) mmEmuBlockHdr
{
    mmByte_t    ID[8];
    mmWord_t    Reserved;
    mmWord_t    BlockVersion;
    mmUInt32_t  BlockSize;
};

// CPU register
// Version 0x0110 
struct MM_ALIGNED(1) mmEmuCpuStatO
{
    mmWord_t    PC;
    mmByte_t    A;
    mmByte_t    X;
    mmByte_t    Y;
    mmByte_t    S;
    mmByte_t    P;
    mmByte_t    I;                  // Interrupt pending flag

    mmByte_t    FrameIRQ;
    mmByte_t    reserved[3];

    mmSInt32_t  mod_cycles;         // Prevent subtle discrepancies between clocks and movies

    // Version 0x0110
    mmUInt64_t  emul_cycles;
    mmUInt64_t  base_cycles;
};

// Version 0x0210
struct MM_ALIGNED(1) mmEmuCpuStat
{
    mmWord_t    PC;
    mmByte_t    A;
    mmByte_t    X;
    mmByte_t    Y;
    mmByte_t    S;
    mmByte_t    P;
    mmByte_t    I;                  // Interrupt pending flag

    mmByte_t    FrameIRQ;
    mmByte_t    FrameIRQ_occur;
    mmByte_t    FrameIRQ_count;
    mmByte_t    FrameIRQ_type;
    mmSInt32_t  FrameIRQ_cycles;

    mmSInt32_t  DMA_cycles;

    mmUInt64_t  emul_cycles;
    mmUInt64_t  base_cycles;
};

// PPU register
struct MM_ALIGNED(1) mmEmuPpuStat
{
    mmByte_t    reg0;
    mmByte_t    reg1;
    mmByte_t    reg2;
    mmByte_t    reg3;
    mmByte_t    reg7;
    mmByte_t    toggle56;

    mmWord_t    loopy_t;
    mmWord_t    loopy_v;
    mmWord_t    loopy_x;
};

// APU register(including expanding the sound)
struct MM_ALIGNED(1) mmEmuApuStatO
{
    mmByte_t    reg[0x0018];
    mmByte_t    ext[0x0100];
};

// Controller registration
struct MM_ALIGNED(1) mmEmuCtrReg
{
    mmUInt32_t  pad1bit;
    mmUInt32_t  pad2bit;
    mmUInt32_t  pad3bit;
    mmUInt32_t  pad4bit;
    mmByte_t    strobe;
};

//
// Register data
// ID "REG DATA"
// Version 0x0110 
struct MM_ALIGNED(1) mmEmuRegStatO
{
    union UniCpuRegO
    {
        mmByte_t    cpudata[32];
        struct mmEmuCpuStatO cpu;
    } cpureg;
    union UniPpuRegO
    {
        mmByte_t    ppudata[32];
        struct mmEmuPpuStat ppu;
    } ppureg;
    struct mmEmuApuStatO    apu;
};

// Version 0x0200 after
struct MM_ALIGNED(1) mmEmuRegStat
{
    union uni_cpureg
    {
        mmByte_t    cpudata[64];
        struct mmEmuCpuStat cpu;
    } cpureg;
    union uni_ppureg
    {
        mmByte_t    ppudata[32];
        struct mmEmuPpuStat ppu;
    } ppureg;
};


//
// Internal RAM data
// ID "RAM DATA"
struct MM_ALIGNED(1) mmEmuRamStat
{
    mmByte_t    RAM[2 * 1024];      // Internal NES RAM
    mmByte_t    BGPAL[16];          // BG Palette
    mmByte_t    SPPAL[16];          // SP Palette
    mmByte_t    SPRAM[256];         // Sprite RAM
};

//
// MMU data
// ID "MMU DATA"
struct MM_ALIGNED(1) mmEmuMmuStat
{
    mmByte_t    CPU_MEM_TYPE[8];
    mmWord_t    CPU_MEM_PAGE[8];
    mmByte_t    PPU_MEM_TYPE[12];
    mmWord_t    PPU_MEM_PAGE[12];
    mmByte_t    CRAM_USED[8];
};

//
// Mapper data
// ID "MMC DATA"
struct MM_ALIGNED(1) mmEmuMmcStat
{
    mmByte_t    mmcdata[256];
};

//
// Controller data
// ID "CTR DATA"
struct MM_ALIGNED(1) mmEmuCtrStat
{
    union UniCtrData
    {
        mmByte_t    ctrdata[32];
        struct mmEmuCtrReg  ctr;
    } ctrreg;
};

//
// SND data
// ID "SND DATA"
struct MM_ALIGNED(1) mmEmuSndStat
{
    mmByte_t    snddata[0x800];     // 2KB
};

//
// Disk image
// Version 0.24
// ID "DSIDE 0A","DSIDE 0B","DSIDE 1A","DSIDE 1B"
struct MM_ALIGNED(1) mmEmuDiskStat
{
    mmByte_t    DiskTouch[16];
};

// Version 0.30 after
// ID "DISKDATA"
struct MM_ALIGNED(1) mmEmuDiskData
{
    mmSInt32_t  DifferentSize;
};

// Use the following in your disk save image file
// Version 0.24 
struct MM_ALIGNED(1) mmEmuDiskImgFileHdr
{
    //  0123456789AB
    mmByte_t    ID[12];             // "NES DI"
    mmWord_t    BlockVersion;
    mmWord_t    DiskNumber;
};

struct MM_ALIGNED(1) mmEmuDiskImgHdr
{
    mmByte_t    ID[6];              // ID "SIDE0A","SIDE0B","SIDE1A","SIDE1B"
    mmByte_t    DiskTouch[16];
};

// NES version0.30 for later use
struct MM_ALIGNED(1) mmEmuDiskFileHdr
{
    //  0123456789AB
    mmByte_t    ID[12];             // "NES DI"
    mmWord_t    BlockVersion;       // 0x0200:0.30  0x0210:0.31
    mmWord_t    Reserved;
    mmUInt32_t  ProgID;             // Program ID
    mmWord_t    MakerID;            // Manufacturer ID
    mmWord_t    DiskNo;             // The number of disks
    mmUInt32_t  DifferentSize;      // The number of differences
};

// ID "EXCTRDAT"
struct MM_ALIGNED(1) mmEmu_ExCtrStat
{
    mmUInt32_t  data;
};

//
// Movie file
//
// NES version0.60 for later use
struct MM_ALIGNED(1) mmEmuMovieFileHdr
{
    mmByte_t    ID[12];             // "NES MV"
    mmWord_t    BlockVersion;       // Movie version 0x0300
    mmWord_t    RecordVersion;      // Record version
    mmUInt32_t  Control;            // Control byte
                                    // 76543210(Bit)
                                    // E---4321
                                    // |   |||+-- 1P data
                                    // |   ||+--- 2P data
                                    // |   |+---- 3P data
                                    // |   +----- 4P data
                                    // +--------- No additional writing
                                    // Other controls are the arrow keys 1P to 4P (any good)
                                    // When all are turned on, the next 4 bytes become control data
    mmUInt32_t  Ext0;               // ROM:program CRC  FDS:program ID
    mmWord_t    Ext1;               // ROM:none     FDS:Manufacturer ID
    mmWord_t    Ext2;               // ROM:none     FDS:The number of discs
    mmUInt32_t  RecordTimes;        // Recording frequency (number of reprograms)

    mmByte_t    RenderMethod;       // Rendering method
    mmByte_t    IRQtype;            // IRQ type
    mmByte_t    FrameIRQ;           // FrameIRQ is disabled
    mmByte_t    VideoMode;          // NTSC/PAL/DENDY

    mmByte_t    reserved2[8];       // Booking

    mmSInt32_t  StateStOffset;      // Movie start state offset
    mmSInt32_t  StateEdOffset;      // Movie end state offset
    mmSInt32_t  MovieOffset;        // Movie data offset
    mmSInt32_t  MovieStep;          // Movie steps(Frame Number)

    mmUInt32_t  CRC;                // CRC does not include these data
};

struct MM_ALIGNED(1) mmEmuMovieImgFileHdrX
{
    mmByte_t    ID[12];             // "NES MV"
    mmWord_t    BlockVersion;
    mmWord_t    reserved;
    mmSInt32_t  StateStOffset;      // Movie start state offset
    mmSInt32_t  StateEdOffset;      // Movie end state offset
    mmSInt32_t  MovieOffset;        // Movie data offset
    mmSInt32_t  MovieStep;          // Movie steps
};

// Famtasia Movie....
struct MM_ALIGNED(1) mmEmuFmvHdr
{
    mmByte_t    ID[4];              // "FMV^Z"
    mmByte_t    Control1;           // R  ??????? 0: record after reset? 1: Record from the middle
    mmByte_t    Control2;           // OT ??????? O: 1P information      T: 2P information
    mmUInt32_t  Unknown1;
    mmWord_t    RecordTimes;        // Recording frequency -1
    mmUInt32_t  Unknown2;
    mmByte_t    szEmulators[0x40];  // Record simulator
    mmByte_t    szTitle[0x40];      // Title
};

// Nesticle Movie....
struct MM_ALIGNED(1) mmEmuNmvHdr
{
    mmByte_t    ExRAM[0x2000];
    mmByte_t    S_RAM[0x0800];
    mmWord_t    PC;
    mmByte_t    A;
    mmByte_t    P;
    mmByte_t    X;
    mmByte_t    Y;
    mmByte_t    SP;
    mmByte_t    OAM[0x0100];
    mmByte_t    VRAM[0x4000];
    mmByte_t    Other[0xC9];
    mmUInt32_t  ScanlineCycles;
    mmUInt32_t  VblankScanlines;
    mmUInt32_t  FrameScanlines;
    mmUInt32_t  VirtualFPS;
};

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( pop )
#endif

// Macro
#define MM_EMU_SETBYTE(p,v)     { *p = (v); p++; }
#define MM_EMU_SETWORD(p,v)     { *(mmWord_t*)p = (v); p += sizeof(mmWord_t); }
#define MM_EMU_SETDWORD(p,v)    { *(mmUInt32_t*)p = (v); p += sizeof(mmUInt32_t); }
#define MM_EMU_SETINT(p,v)      { *(mmInt_t*)p = (v); p += sizeof(mmInt_t); }
#define MM_EMU_SETBLOCK(p,v,s)  { mmMemcpy( p, (v), s ); p += s; }

#define MM_EMU_GETBYTE(p,v)     { (v) = *p; p++; }
#define MM_EMU_GETWORD(p,v)     { (v) = *(mmWord_t*)p; p += sizeof(mmWord_t); }
#define MM_EMU_GETDWORD(p,v)    { (v) = *(mmUInt32_t*)p; p += sizeof(mmUInt32_t); }
#define MM_EMU_GETINT(p,v)      { (v) = *(mmInt_t*)p; p += sizeof(mmInt_t); }
#define MM_EMU_GETBLOCK(p,v,s)  { mmMemcpy( (v), p, s ); p += s; }

#include "core/mmSuffix.h"

#endif//__mmEmuState_h__
