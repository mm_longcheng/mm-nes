/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuRomNsfHeader_h__
#define __mmEmuRomNsfHeader_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmLogger;

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( push, 1 )
#endif

struct MM_ALIGNED(1) mmEmuRomNsfHeader
{
    mmByte_t ID[5];
    mmByte_t Version;
    mmByte_t TotalSong;
    mmByte_t StartSong;
    mmWord_t LoadAddress;
    mmWord_t InitAddress;
    mmWord_t PlayAddress;
    mmByte_t SongName[32];
    mmByte_t ArtistName[32];
    mmByte_t CopyrightName[32];
    mmWord_t SpeedNTSC;
    mmByte_t BankSwitch[8];
    mmWord_t SpeedPAL;
    mmByte_t NTSC_PALbits;
    mmByte_t ExtraChipSelect;
    // must be 0
    mmByte_t Expansion[4];
};

#if MM_COMPILER == MM_COMPILER_MSVC
#pragma pack( pop )
#endif

MM_EXPORT_EMU void mmEmuRomNsfHeader_Init(struct mmEmuRomNsfHeader* p);
MM_EXPORT_EMU void mmEmuRomNsfHeader_Destroy(struct mmEmuRomNsfHeader* p);

MM_EXPORT_EMU void mmEmuRomNsfHeader_LoggerInformation(const struct mmEmuRomNsfHeader* p, struct mmLogger* logger);

#include "core/mmSuffix.h"

#endif//__mmEmuRomNsfHeader_h__
