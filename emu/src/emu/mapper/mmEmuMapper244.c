/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper244.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper244_Init(struct mmEmuMapper244* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper244_Reset;
    p->super.Write = &mmEmuMapper244_Write;
    //p->super.Read = &mmEmuMapper244_Read;
    //p->super.ReadLow = &mmEmuMapper244_ReadLow;
    //p->super.WriteLow = &mmEmuMapper244_WriteLow;
    //p->super.ExRead = &mmEmuMapper244_ExRead;
    //p->super.ExWrite = &mmEmuMapper244_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper244_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper244_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper244_HSync;
    //p->super.VSync = &mmEmuMapper244_VSync;
    //p->super.Clock = &mmEmuMapper244_Clock;
    //p->super.PPULatch = &mmEmuMapper244_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper244_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper244_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper244_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper244_IsStateSave;
    //p->super.SaveState = &mmEmuMapper244_SaveState;
    //p->super.LoadState = &mmEmuMapper244_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper244_Destroy(struct mmEmuMapper244* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper244_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper244* p = (struct mmEmuMapper244*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper244_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper244* p = (struct mmEmuMapper244*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0x8065 && addr <= 0x80A4)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (addr - 0x8065) & 0x3);
    }

    if (addr >= 0x80A5 && addr <= 0x80E4)
    {
        mmEmuMmu_SetVROM8KBank(mmu, (addr - 0x80A5) & 0x7);
    }
}

static struct mmEmuMapper* __static_mmEmuMapper244_Produce(void)
{
    struct mmEmuMapper244* p = (struct mmEmuMapper244*)mmMalloc(sizeof(struct mmEmuMapper244));
    mmEmuMapper244_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper244_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper244* p = (struct mmEmuMapper244*)(m);
    mmEmuMapper244_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER244 =
{
    &__static_mmEmuMapper244_Produce,
    &__static_mmEmuMapper244_Recycle,
};
