/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper045.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper045_Init(struct mmEmuMapper045* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->patch = 0;

    p->prg0 = 0;
    p->prg1 = 0;
    p->prg2 = 0;
    p->prg3 = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    mmMemset(p->p, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->c, 0, sizeof(mmInt_t) * 9);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_latched = 0;
    p->irq_reset = 0;

    //
    p->super.Reset = &mmEmuMapper045_Reset;
    p->super.Write = &mmEmuMapper045_Write;
    //p->super.Read = &mmEmuMapper045_Read;
    //p->super.ReadLow = &mmEmuMapper045_ReadLow;
    p->super.WriteLow = &mmEmuMapper045_WriteLow;
    //p->super.ExRead = &mmEmuMapper045_ExRead;
    //p->super.ExWrite = &mmEmuMapper045_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper045_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper045_ExCmdWrite;
    p->super.HSync = &mmEmuMapper045_HSync;
    //p->super.VSync = &mmEmuMapper045_VSync;
    //p->super.Clock = &mmEmuMapper045_Clock;
    //p->super.PPULatch = &mmEmuMapper045_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper045_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper045_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper045_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper045_IsStateSave;
    p->super.SaveState = &mmEmuMapper045_SaveState;
    p->super.LoadState = &mmEmuMapper045_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper045_Destroy(struct mmEmuMapper045* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->patch = 0;

    p->prg0 = 0;
    p->prg1 = 0;
    p->prg2 = 0;
    p->prg3 = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    mmMemset(p->p, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->c, 0, sizeof(mmInt_t) * 9);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_latched = 0;
    p->irq_reset = 0;
}

MM_EXPORT_EMU void mmEmuMapper045_SetBankCPU4(struct mmEmuMapper045* p, mmInt_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    data &= (p->reg[3] & 0x3F) ^ 0xFF;
    data &= 0x3F;
    data |= p->reg[1];
    mmEmuMmu_SetPROM8KBank(mmu, 4, data);
    p->p[0] = data;
}
MM_EXPORT_EMU void mmEmuMapper045_SetBankCPU5(struct mmEmuMapper045* p, mmInt_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    data &= (p->reg[3] & 0x3F) ^ 0xFF;
    data &= 0x3F;
    data |= p->reg[1];
    mmEmuMmu_SetPROM8KBank(mmu, 5, data);
    p->p[1] = data;
}
MM_EXPORT_EMU void mmEmuMapper045_SetBankCPU6(struct mmEmuMapper045* p, mmInt_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    data &= (p->reg[3] & 0x3F) ^ 0xFF;
    data &= 0x3F;
    data |= p->reg[1];
    mmEmuMmu_SetPROM8KBank(mmu, 6, data);
    p->p[2] = data;
}
MM_EXPORT_EMU void mmEmuMapper045_SetBankCPU7(struct mmEmuMapper045* p, mmInt_t data)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    data &= (p->reg[3] & 0x3F) ^ 0xFF;
    data &= 0x3F;
    data |= p->reg[1];
    mmEmuMmu_SetPROM8KBank(mmu, 7, data);
    p->p[3] = data;
}
MM_EXPORT_EMU void mmEmuMapper045_SetBankPPU(struct mmEmuMapper045* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    static const mmByte_t table[16] =
    {
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x01,0x03,0x07,0x0F,0x1F,0x3F,0x7F,0xFF
    };

    mmInt_t i = 0;

    p->c[0] = p->chr0;
    p->c[1] = p->chr1;
    p->c[2] = p->chr2;
    p->c[3] = p->chr3;
    p->c[4] = p->chr4;
    p->c[5] = p->chr5;
    p->c[6] = p->chr6;
    p->c[7] = p->chr7;

    for (i = 0; i < 8; i++)
    {
        p->c[i] &= table[p->reg[2] & 0x0F];
        p->c[i] |= p->reg[0] & ((p->patch != 1) ? 0xFF : 0xC0);
        p->c[i] += (p->reg[2] & ((p->patch != 1) ? 0x10 : 0x30)) << 4;
    }

    if (p->reg[6] & 0x80)
    {
        mmEmuMmu_SetVROM8KBankArray(mmu, p->c[4], p->c[5], p->c[6], p->c[7], p->c[0], p->c[1], p->c[2], p->c[3]);
    }
    else
    {
        mmEmuMmu_SetVROM8KBankArray(mmu, p->c[0], p->c[1], p->c[2], p->c[3], p->c[4], p->c[5], p->c[6], p->c[7]);
    }
}

MM_EXPORT_EMU void mmEmuMapper045_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper045* p = (struct mmEmuMapper045*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;
    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    p->prg2 = mmu->PROM_08K_SIZE - 2;
    p->prg3 = mmu->PROM_08K_SIZE - 1;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x58bcacf6 ||        // Kunio 8-in-1 (Pirate Cart)
        crc == 0x9103cfd6 ||        // HIK 7-in-1 (Pirate Cart)
        crc == 0xc082e6d3)          // Super 8-in-1 (Pirate Cart)
    {
        p->patch = 1;
        p->prg2 = 62;
        p->prg3 = 63;
    }
    if (crc == 0xe0dd259d)          // Super 3-in-1 (Pirate Cart)
    {
        p->patch = 2;
    }
    mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, p->prg2, p->prg3);
    p->p[0] = p->prg0;
    p->p[1] = p->prg1;
    p->p[2] = p->prg2;
    p->p[3] = p->prg3;

    mmEmuMmu_SetVROM8KBank(mmu, 0);

    //  p->chr0 = p->c[0] = 0;
    //  p->chr1 = p->c[1] = 0
    //  p->chr2 = p->c[2] = 0;
    //  p->chr3 = p->c[3] = 0;
    //  p->chr4 = p->c[4] = 0;
    //  p->chr5 = p->c[5] = 0;
    //  p->chr6 = p->c[6] = 0;
    //  p->chr7 = p->c[7] = 0;

    p->chr0 = p->c[0] = 0;
    p->chr1 = p->c[1] = 1;
    p->chr2 = p->c[2] = 2;
    p->chr3 = p->c[3] = 3;
    p->chr4 = p->c[4] = 4;
    p->chr5 = p->c[5] = 5;
    p->chr6 = p->c[6] = 6;
    p->chr7 = p->c[7] = 7;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_latched = 0;
    p->irq_reset = 0;
}
MM_EXPORT_EMU void mmEmuMapper045_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper045* p = (struct mmEmuMapper045*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        if ((data & 0x40) != (p->reg[6] & 0x40))
        {
            mmByte_t swp;
            swp = p->prg0; p->prg0 = p->prg2; p->prg2 = swp;
            swp = p->p[0]; p->p[0] = p->p[2]; p->p[2] = swp;
            mmEmuMapper045_SetBankCPU4(p, p->p[0]);
            mmEmuMapper045_SetBankCPU5(p, p->p[1]);
        }
        if (mmu->VROM_1K_SIZE)
        {
            if ((data & 0x80) != (p->reg[6] & 0x80))
            {
                mmInt_t swp;
                swp = p->chr4; p->chr4 = p->chr0; p->chr0 = swp;
                swp = p->chr5; p->chr5 = p->chr1; p->chr1 = swp;
                swp = p->chr6; p->chr6 = p->chr2; p->chr2 = swp;
                swp = p->chr7; p->chr7 = p->chr3; p->chr3 = swp;
                swp = p->c[4]; p->c[4] = p->c[0]; p->c[0] = swp;
                swp = p->c[5]; p->c[5] = p->c[1]; p->c[1] = swp;
                swp = p->c[6]; p->c[6] = p->c[2]; p->c[2] = swp;
                swp = p->c[7]; p->c[7] = p->c[3]; p->c[3] = swp;
                mmEmuMmu_SetVROM8KBankArray(mmu, p->c[0], p->c[1], p->c[2], p->c[3], p->c[4], p->c[5], p->c[6], p->c[7]);
            }
        }
        p->reg[6] = data;
        break;
    case 0x8001:
        switch (p->reg[6] & 0x07)
        {
        case 0x00:
            p->chr0 = (data & 0xFE) + 0;
            p->chr1 = (data & 0xFE) + 1;
            mmEmuMapper045_SetBankPPU(p);
            break;
        case 0x01:
            p->chr2 = (data & 0xFE) + 0;
            p->chr3 = (data & 0xFE) + 1;
            mmEmuMapper045_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper045_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper045_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper045_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper045_SetBankPPU(p);
            break;
        case 0x06:
            if (p->reg[6] & 0x40)
            {
                p->prg2 = data & 0x3F;
                mmEmuMapper045_SetBankCPU6(p, data);
            }
            else
            {
                p->prg0 = data & 0x3F;
                mmEmuMapper045_SetBankCPU4(p, data);
            }
            break;
        case 0x07:
            p->prg1 = data & 0x3F;
            mmEmuMapper045_SetBankCPU5(p, data);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    case 0xC000:
        if (p->patch == 2)
        {
            if (data == 0x29 || data == 0x70)
            {
                data = 0x07;
            }
        }
        p->irq_latch = data;
        p->irq_latched = 1;
        if (p->irq_reset)
        {
            p->irq_counter = data;
            p->irq_latched = 0;
        }
        //p->irq_counter = data;
        break;
    case 0xC001:
        //p->irq_latch = data;
        p->irq_counter = p->irq_latch;
        break;
    case 0xE000:
        p->irq_enable = 0;
        p->irq_reset = 1;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->irq_enable = 1;
        if (p->irq_latched)
        {
            p->irq_counter = p->irq_latch;
        }
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU void mmEmuMapper045_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper045* p = (struct mmEmuMapper045*)(super);

    //  if( addr == 0x6000 )
    //  if( addr == 0x6000 && !(p->reg[3]&0x40) )
    if (!(p->reg[3] & 0x40))
    {
        p->reg[p->reg[5]] = data;
        p->reg[5] = (p->reg[5] + 1) & 0x03;

        mmEmuMapper045_SetBankCPU4(p, p->prg0);
        mmEmuMapper045_SetBankCPU5(p, p->prg1);
        mmEmuMapper045_SetBankCPU6(p, p->prg2);
        mmEmuMapper045_SetBankCPU7(p, p->prg3);
        mmEmuMapper045_SetBankPPU(p);
    }
}

MM_EXPORT_EMU void mmEmuMapper045_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper045* p = (struct mmEmuMapper045*)(super);

    struct mmEmuNes* nes = p->super.nes;

    p->irq_reset = 0;
    if ((scanline >= 0 && scanline <= 239) && mmEmuPpu_IsDispON(&nes->ppu))
    {
        if (p->irq_counter)
        {
            p->irq_counter--;
            if (p->irq_counter == 0)
            {
                if (p->irq_enable)
                {
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper045_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper045_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper045* p = (struct mmEmuMapper045*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    mmMemcpy(&buffer[8], &p->p[0], sizeof(mmByte_t) * 4);
    mmMemcpy(&buffer[64], &p->c[0], sizeof(mmInt_t) * 8);
    buffer[20] = p->prg0;
    buffer[21] = p->prg1;
    buffer[22] = p->prg2;
    buffer[23] = p->prg3;
    buffer[24] = p->chr0;
    buffer[25] = p->chr1;
    buffer[26] = p->chr2;
    buffer[27] = p->chr3;
    buffer[28] = p->chr4;
    buffer[29] = p->chr5;
    buffer[30] = p->chr6;
    buffer[31] = p->chr7;
    buffer[32] = p->irq_enable;
    buffer[33] = p->irq_counter;
    buffer[34] = p->irq_latch;
}
MM_EXPORT_EMU void mmEmuMapper045_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper045* p = (struct mmEmuMapper045*)(super);

    p->prg0 = buffer[20];
    p->prg1 = buffer[21];
    p->prg2 = buffer[22];
    p->prg3 = buffer[23];
    p->chr0 = buffer[24];
    p->chr1 = buffer[25];
    p->chr2 = buffer[26];
    p->chr3 = buffer[27];
    p->chr4 = buffer[28];
    p->chr5 = buffer[29];
    p->chr6 = buffer[30];
    p->chr7 = buffer[31];
    p->irq_enable = buffer[32];
    p->irq_counter = buffer[33];
    p->irq_latch = buffer[34];
}

static struct mmEmuMapper* __static_mmEmuMapper045_Produce(void)
{
    struct mmEmuMapper045* p = (struct mmEmuMapper045*)mmMalloc(sizeof(struct mmEmuMapper045));
    mmEmuMapper045_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper045_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper045* p = (struct mmEmuMapper045*)(m);
    mmEmuMapper045_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER045 =
{
    &__static_mmEmuMapper045_Produce,
    &__static_mmEmuMapper045_Recycle,
};
