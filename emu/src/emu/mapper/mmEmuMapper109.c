/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper109.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper109_Init(struct mmEmuMapper109* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;

    p->chrmode0 = 0;
    p->chrmode1 = 0;

    //
    p->super.Reset = &mmEmuMapper109_Reset;
    //p->super.Write = &mmEmuMapper109_Write;
    //p->super.Read = &mmEmuMapper109_Read;
    //p->super.ReadLow = &mmEmuMapper109_ReadLow;
    p->super.WriteLow = &mmEmuMapper109_WriteLow;
    //p->super.ExRead = &mmEmuMapper109_ExRead;
    //p->super.ExWrite = &mmEmuMapper109_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper109_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper109_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper109_HSync;
    //p->super.VSync = &mmEmuMapper109_VSync;
    //p->super.Clock = &mmEmuMapper109_Clock;
    //p->super.PPULatch = &mmEmuMapper109_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper109_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper109_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper109_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper109_IsStateSave;
    p->super.SaveState = &mmEmuMapper109_SaveState;
    p->super.LoadState = &mmEmuMapper109_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper109_Destroy(struct mmEmuMapper109* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;

    p->chrmode0 = 0;
    p->chrmode1 = 0;
}

MM_EXPORT_EMU void mmEmuMapper109_SetBankPPU(struct mmEmuMapper109* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->chr0);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->chr1 | ((p->chrmode1 << 3) & 0x8));
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->chr2 | ((p->chrmode1 << 2) & 0x8));
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->chr3 | ((p->chrmode1 << 1) & 0x8) | (p->chrmode0 * 0x10));
        mmEmuMmu_SetVROM1KBank(mmu, 4, mmu->VROM_1K_SIZE - 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, mmu->VROM_1K_SIZE - 3);
        mmEmuMmu_SetVROM1KBank(mmu, 6, mmu->VROM_1K_SIZE - 2);
        mmEmuMmu_SetVROM1KBank(mmu, 7, mmu->VROM_1K_SIZE - 1);
    }
}

MM_EXPORT_EMU void mmEmuMapper109_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper109* p = (struct mmEmuMapper109*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->reg = 0;
    mmEmuMmu_SetPROM32KBank(mmu, 0);

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;

    mmEmuMapper109_SetBankPPU(p);

    p->chrmode0 = 0;
    p->chrmode1 = 0;
}
MM_EXPORT_EMU void mmEmuMapper109_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper109* p = (struct mmEmuMapper109*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x4100:
        p->reg = data;
        break;
    case 0x4101:
        switch (p->reg)
        {
        case 0:
            p->chr0 = data;
            mmEmuMapper109_SetBankPPU(p);
            break;
        case 1:
            p->chr1 = data;
            mmEmuMapper109_SetBankPPU(p);
            break;
        case 2:
            p->chr2 = data;
            mmEmuMapper109_SetBankPPU(p);
            break;
        case 3:
            p->chr3 = data;
            mmEmuMapper109_SetBankPPU(p);
            break;
        case 4:
            p->chrmode0 = data & 0x01;
            mmEmuMapper109_SetBankPPU(p);
            break;
        case 5:
            mmEmuMmu_SetPROM32KBank(mmu, data & 0x07);
            break;
        case 6:
            p->chrmode1 = data & 0x07;
            mmEmuMapper109_SetBankPPU(p);
            break;
        case 7:
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }

}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper109_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper109_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper109* p = (struct mmEmuMapper109*)(super);

    buffer[0] = p->reg;
    buffer[1] = p->chr0;
    buffer[2] = p->chr1;
    buffer[3] = p->chr2;
    buffer[4] = p->chr3;
    buffer[5] = p->chrmode0;
    buffer[6] = p->chrmode1;
}
MM_EXPORT_EMU void mmEmuMapper109_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper109* p = (struct mmEmuMapper109*)(super);

    p->reg = buffer[0];
    p->chr0 = buffer[1];
    p->chr1 = buffer[2];
    p->chr2 = buffer[3];
    p->chr3 = buffer[4];
    p->chrmode0 = buffer[5];
    p->chrmode1 = buffer[6];
}

static struct mmEmuMapper* __static_mmEmuMapper109_Produce(void)
{
    struct mmEmuMapper109* p = (struct mmEmuMapper109*)mmMalloc(sizeof(struct mmEmuMapper109));
    mmEmuMapper109_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper109_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper109* p = (struct mmEmuMapper109*)(m);
    mmEmuMapper109_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER109 =
{
    &__static_mmEmuMapper109_Produce,
    &__static_mmEmuMapper109_Recycle,
};
