/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper.h"
#include "mmEmuNes.h"
#include "mmEmuMmu.h"
#include "mmEmuMapperRegister.h"

/*--------------[ DEFINE                ]-------------------------------*/
/*--------------[ EXTERNAL PROGRAM      ]-------------------------------*/
/*--------------[ EXTERNAL WORK         ]-------------------------------*/
/*--------------[ WORK                  ]-------------------------------*/
/*--------------[ PROTOTYPE             ]-------------------------------*/
/*--------------[ CONST                 ]-------------------------------*/
/*--------------[ PROGRAM               ]-------------------------------*/

MM_EXPORT_EMU void mmEmuMapper_Init(struct mmEmuMapper* p)
{
    p->nes = NULL;

    p->Reset = &mmEmuMapper_Reset;
    p->Write = &mmEmuMapper_Write;
    p->Read = &mmEmuMapper_Read;
    p->ReadLow = &mmEmuMapper_ReadLow;
    p->WriteLow = &mmEmuMapper_WriteLow;
    p->ExRead = &mmEmuMapper_ExRead;
    p->ExWrite = &mmEmuMapper_ExWrite;
    p->ExCmdRead = &mmEmuMapper_ExCmdRead;
    p->ExCmdWrite = &mmEmuMapper_ExCmdWrite;
    p->HSync = &mmEmuMapper_HSync;
    p->VSync = &mmEmuMapper_VSync;
    p->Clock = &mmEmuMapper_Clock;
    p->PPULatch = &mmEmuMapper_PPULatch;
    p->PPUChrLatch = &mmEmuMapper_PPUChrLatch;
    p->PPUExtLatchX = &mmEmuMapper_PPUExtLatchX;
    p->PPUExtLatch = &mmEmuMapper_PPUExtLatch;
    p->IsStateSave = &mmEmuMapper_IsStateSave;
    p->SaveState = &mmEmuMapper_SaveState;
    p->LoadState = &mmEmuMapper_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper_Destroy(struct mmEmuMapper* p)
{
    p->nes = NULL;

    p->Reset = &mmEmuMapper_Reset;
    p->Write = &mmEmuMapper_Write;
    p->Read = &mmEmuMapper_Read;
    p->ReadLow = &mmEmuMapper_ReadLow;
    p->WriteLow = &mmEmuMapper_WriteLow;
    p->ExRead = &mmEmuMapper_ExRead;
    p->ExWrite = &mmEmuMapper_ExWrite;
    p->ExCmdRead = &mmEmuMapper_ExCmdRead;
    p->ExCmdWrite = &mmEmuMapper_ExCmdWrite;
    p->HSync = &mmEmuMapper_HSync;
    p->VSync = &mmEmuMapper_VSync;
    p->Clock = &mmEmuMapper_Clock;
    p->PPULatch = &mmEmuMapper_PPULatch;
    p->PPUChrLatch = &mmEmuMapper_PPUChrLatch;
    p->PPUExtLatchX = &mmEmuMapper_PPUExtLatchX;
    p->PPUExtLatch = &mmEmuMapper_PPUExtLatch;
    p->IsStateSave = &mmEmuMapper_IsStateSave;
    p->SaveState = &mmEmuMapper_SaveState;
    p->LoadState = &mmEmuMapper_LoadState;
}

MM_EXPORT_EMU void mmEmuMapper_SetParent(struct mmEmuMapper* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

// For Mapper
// Reset
MM_EXPORT_EMU void mmEmuMapper_Reset(struct mmEmuMapper* p)
{

}

// $8000-$FFFF Memory write
MM_EXPORT_EMU void mmEmuMapper_Write(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data)
{

}

// $8000-$FFFF Memory read(Dummy)
MM_EXPORT_EMU void mmEmuMapper_Read(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data)
{

}

// $4100-$7FFF Lower Memory read/write
MM_EXPORT_EMU mmByte_t mmEmuMapper_ReadLow(struct mmEmuMapper* p, mmWord_t addr)
{
    // $6000-$7FFF WRAM
    if (addr >= 0x6000 && addr <= 0x7FFF)
    {
        return p->nes->mmu.CPU_MEM_BANK[addr >> 13][addr & 0x1FFF];
    }

    return (mmByte_t)(addr >> 8);
}
MM_EXPORT_EMU void mmEmuMapper_WriteLow(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data)
{
    // $6000-$7FFF WRAM
    if (addr >= 0x6000 && addr <= 0x7FFF)
    {
        p->nes->mmu.CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }
}

// $4018-$40FF Extention register read/write
MM_EXPORT_EMU mmByte_t mmEmuMapper_ExRead(struct mmEmuMapper* p, mmWord_t addr)
{
    return 0x00;
}
MM_EXPORT_EMU void mmEmuMapper_ExWrite(struct mmEmuMapper* p, mmWord_t addr, mmByte_t data)
{

}

// cmd mmEmuMapperExCmdRd_t
MM_EXPORT_EMU mmByte_t mmEmuMapper_ExCmdRead(struct mmEmuMapper* p, mmInt_t cmd)
{
    return 0x00;
}
MM_EXPORT_EMU void mmEmuMapper_ExCmdWrite(struct mmEmuMapper* p, mmInt_t cmd, mmByte_t data)
{

}

// H sync/V sync/Clock sync
MM_EXPORT_EMU void mmEmuMapper_HSync(struct mmEmuMapper* p, mmInt_t scanline)
{

}
MM_EXPORT_EMU void mmEmuMapper_VSync(struct mmEmuMapper* p)
{

}
MM_EXPORT_EMU void mmEmuMapper_Clock(struct mmEmuMapper* p, mmInt_t cycles)
{

}

// PPU address bus latch
MM_EXPORT_EMU void mmEmuMapper_PPULatch(struct mmEmuMapper* p, mmWord_t addr)
{

}

// PPU Character latch
MM_EXPORT_EMU void mmEmuMapper_PPUChrLatch(struct mmEmuMapper* p, mmWord_t addr)
{

}

// PPU Extension character/palette
MM_EXPORT_EMU void mmEmuMapper_PPUExtLatchX(struct mmEmuMapper* p, mmInt_t x)
{

}
MM_EXPORT_EMU void mmEmuMapper_PPUExtLatch(struct mmEmuMapper* p, mmWord_t addr, mmByte_t* chr_l, mmByte_t* chr_h, mmByte_t* attr)
{

}

// For State save
MM_EXPORT_EMU mmBool_t mmEmuMapper_IsStateSave(const struct mmEmuMapper* p)
{
    return MM_FALSE;
}
MM_EXPORT_EMU void mmEmuMapper_SaveState(const struct mmEmuMapper* p, mmByte_t* buffer)
{

}
MM_EXPORT_EMU void mmEmuMapper_LoadState(struct mmEmuMapper* p, const mmByte_t* buffer)
{

}

MM_EXPORT_EMU void mmEmuMapperFactory_Init(struct mmEmuMapperFactory* p)
{
    mmRbtreeU32Vpt_Init(&p->rbtree);
}
MM_EXPORT_EMU void mmEmuMapperFactory_Destroy(struct mmEmuMapperFactory* p)
{
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
}
MM_EXPORT_EMU void mmEmuMapperFactory_Register(struct mmEmuMapperFactory* p, mmUInt32_t id, const struct mmEmuMapperCreator* creator)
{
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)creator);
}
MM_EXPORT_EMU void mmEmuMapperFactory_Unregister(struct mmEmuMapperFactory* p, mmUInt32_t id)
{
    mmRbtreeU32Vpt_Rmv(&p->rbtree, id);
}
MM_EXPORT_EMU void mmEmuMapperFactory_Clear(struct mmEmuMapperFactory* p)
{
    mmRbtreeU32Vpt_Clear(&p->rbtree);
}

MM_EXPORT_EMU struct mmEmuMapper* mmEmuMapperFactory_Produce(struct mmEmuMapperFactory* p, mmUInt32_t id)
{
    struct mmEmuMapper* m = NULL;
    struct mmEmuMapperCreator* creator = NULL;
    creator = (struct mmEmuMapperCreator*)mmRbtreeU32Vpt_Get(&p->rbtree, id);
    if (NULL != creator)
    {
        m = (*(creator->Produce))();
    }
    else
    {
        m = mmEmuMapper_BaseProduce(id);
    }
    return m;
}
MM_EXPORT_EMU void mmEmuMapperFactory_Recycle(struct mmEmuMapperFactory* p, mmUInt32_t id, struct mmEmuMapper* m)
{
    if (NULL != m)
    {
        struct mmEmuMapperCreator* creator = NULL;
        creator = (struct mmEmuMapperCreator*)mmRbtreeU32Vpt_Get(&p->rbtree, id);
        if (NULL != creator)
        {
            (*(creator->Recycle))(m);
        }
        else
        {
            mmEmuMapper_BaseRecycle(id, m);
        }
    }
}


