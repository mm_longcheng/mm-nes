/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuScreen_h__
#define __mmEmuScreen_h__

#include "core/mmCore.h"

#include "mmEmuColor.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

// Render screen size
enum
{
    MM_EMU_SCREEN_RENDER_W = 256, // width 
    MM_EMU_SCREEN_RENDER_H = 240, // height
};

// Screen size
enum
{
    MM_EMU_SCREEN_W = MM_EMU_SCREEN_RENDER_W + 16, // width 
    MM_EMU_SCREEN_H = MM_EMU_SCREEN_RENDER_H,      // height
};

// Delta screen size
enum
{
    MM_EMU_SCREEN_DELTA_W = MM_EMU_SCREEN_RENDER_W,     // width 
    MM_EMU_SCREEN_DELTA_H = MM_EMU_SCREEN_RENDER_H + 6, // height
};

struct mmEmuScreen;

struct mmEmuScreenCallback
{
    void(*ScreenMessage)(struct mmEmuScreen* obj, const char* message);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_EMU void mmEmuScreenCallback_Init(struct mmEmuScreenCallback* p);
MM_EXPORT_EMU void mmEmuScreenCallback_Destroy(struct mmEmuScreenCallback* p);

struct mmEmuScreen
{
    struct mmEmuNes* nes;

    struct mmEmuScreenCallback callback;

    // we use malloc for less stack memory.
    mmByte_t*   m_lpFramebuffer;
    mmByte_t*   m_lpRender;
    mmByte_t*   m_lpRenderDelta;
    mmByte_t    m_LineColormode[MM_EMU_SCREEN_H];

    mmBool_t    m_bScanlineMode;                    // Scanline mode.
    mmInt_t     m_nScanlineColor;                   // Scanline color.

    // For Zapper
    mmBool_t    m_bZapper;                          // Zapper
    mmBool_t    m_bZapperDraw;                      // Zapper Sight drawing
    mmLong_t    m_ZapperPosX;                       // Zapper pos X
    mmLong_t    m_ZapperPosY;                       // Zapper pos Y

    mmBool_t    m_bDeltaUpdate;

    // palette.
    mmInt_t     m_nPaletteMode;
    mmBool_t    m_bMonoMode;
    mmBool_t    m_bPaletteUpdate;                   // Palette Update?

    mmUInt32_t  m_PaletteBuffer[64];                // Palette buffer.

    // R8G8B8
    // For 256 color mode.
    struct mmEmuRGBQuad m_cpPalette[8][64 * 2];     // Color
    struct mmEmuRGBQuad m_mpPalette[8][64 * 2];     // Monochrome

    // Palette converted to pixel format.
    mmUInt32_t  m_cnPalette[8][256];                // Color
    mmUInt32_t  m_csPalette[8][256];                // Color/Scanline
    mmUInt32_t  m_mnPalette[8][256];                // Monochrome
    mmUInt32_t  m_msPalette[8][256];                // Monochrome/Scanline

    // RGB 555 palette for 2xSaImmx.
    mmUInt32_t  m_cfPalette[8][256];                // Color
    mmUInt32_t  m_mfPalette[8][256];                // Monochrome
};

MM_EXPORT_EMU void mmEmuScreen_Init(struct mmEmuScreen* p);
MM_EXPORT_EMU void mmEmuScreen_Destroy(struct mmEmuScreen* p);

MM_EXPORT_EMU void mmEmuScreen_SetParent(struct mmEmuScreen* p, struct mmEmuNes* parent);
MM_EXPORT_EMU void mmEmuScreen_SetCallback(struct mmEmuScreen* p, struct mmEmuScreenCallback* callback);

MM_EXPORT_EMU void mmEmuScreen_ScreenClear(struct mmEmuScreen* p);

MM_EXPORT_EMU void mmEmuScreen_SetPaletteMode(struct mmEmuScreen* p, mmInt_t nMode, mmBool_t bMono);
MM_EXPORT_EMU void mmEmuScreen_GetPaletteData(const struct mmEmuScreen* p, struct mmEmuRGBQuad* rgb);
MM_EXPORT_EMU void mmEmuScreen_GetPaletteData2(const struct mmEmuScreen* p, struct mmEmuRGBQuad* rgb);
MM_EXPORT_EMU void mmEmuScreen_GetPaletteR8G8B8A8(const struct mmEmuScreen* p, mmUInt32_t* palette);

MM_EXPORT_EMU mmBool_t mmEmuScreen_CalcPaletteTable(struct mmEmuScreen* p);
MM_EXPORT_EMU void mmEmuScreen_SetPaletteTableByte(struct mmEmuScreen* p, const mmUInt32_t pal[64]);
MM_EXPORT_EMU void mmEmuScreen_SetPaletteTableRGBQuad(struct mmEmuScreen* p, struct mmEmuRGBQuad* rgb);
MM_EXPORT_EMU void mmEmuScreen_GetPaletteTable(const struct mmEmuScreen* p, struct mmEmuRGBQuad* rgb);
MM_EXPORT_EMU void mmEmuScreen_SetPaletteFile(struct mmEmuScreen* p, const char* fname);

// For Zapper
static mmInline void mmEmuScreen_SetZapperMode(struct mmEmuScreen* p, mmBool_t bZapper) { p->m_bZapper = bZapper; }
static mmInline void mmEmuScreen_SetZapperDrawMode(struct mmEmuScreen* p, mmBool_t bDraw) { p->m_bZapperDraw = bDraw; }

MM_EXPORT_EMU mmByte_t mmEmuScreen_GetZapperHit(const struct mmEmuScreen* p);
MM_EXPORT_EMU void mmEmuScreen_UpdateZapperPos(struct mmEmuScreen* p, mmLong_t* x, mmLong_t* y);
MM_EXPORT_EMU void mmEmuScreen_SetZapperPos(struct mmEmuScreen* p, mmLong_t x, mmLong_t y);

static mmInline mmByte_t* mmEmuScreen_GetFramebuffer(const struct mmEmuScreen* p) { return p->m_lpFramebuffer; }
static mmInline mmByte_t* mmEmuScreen_GetRenderScreen(const struct mmEmuScreen* p) { return p->m_lpRender; }
static mmInline mmByte_t* mmEmuScreen_GetLineColormode(struct mmEmuScreen* p) { return p->m_LineColormode; }

MM_EXPORT_EMU void mmEmuScreen_SetMessageString(struct mmEmuScreen* p, const char* message);

MM_EXPORT_EMU void mmEmuScreen_BitBlockTransfer(struct mmEmuScreen* p);

#include "core/mmSuffix.h"

#endif//__mmEmuScreen_h__
