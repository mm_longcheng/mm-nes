/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper116.h"
#include "emu/mmEmuNes.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuMapper116_Init(struct mmEmuMapper116* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;
    p->prg2 = 0;
    p->prg3 = 0;

    p->prg0L = 0;
    p->prg1L = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    p->ExPrgSwitch = 0;
    p->ExChrSwitch = 0;

    //
    p->super.Reset = &mmEmuMapper116_Reset;
    p->super.Write = &mmEmuMapper116_Write;
    //p->super.Read = &mmEmuMapper116_Read;
    //p->super.ReadLow = &mmEmuMapper116_ReadLow;
    p->super.WriteLow = &mmEmuMapper116_WriteLow;
    //p->super.ExRead = &mmEmuMapper116_ExRead;
    //p->super.ExWrite = &mmEmuMapper116_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper116_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper116_ExCmdWrite;
    p->super.HSync = &mmEmuMapper116_HSync;
    //p->super.VSync = &mmEmuMapper116_VSync;
    //p->super.Clock = &mmEmuMapper116_Clock;
    //p->super.PPULatch = &mmEmuMapper116_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper116_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper116_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper116_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper116_IsStateSave;
    p->super.SaveState = &mmEmuMapper116_SaveState;
    p->super.LoadState = &mmEmuMapper116_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper116_Destroy(struct mmEmuMapper116* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;
    p->prg2 = 0;
    p->prg3 = 0;

    p->prg0L = 0;
    p->prg1L = 0;

    p->chr0 = 0;
    p->chr1 = 0;
    p->chr2 = 0;
    p->chr3 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    p->ExPrgSwitch = 0;
    p->ExChrSwitch = 0;
}

MM_EXPORT_EMU void mmEmuMapper116_SetBankCPU(struct mmEmuMapper116* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, p->prg1, p->prg0, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper116_SetBankPPU(struct mmEmuMapper116* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->ExChrSwitch & 0x04)
        {
            static const mmInt_t chrbank = 256;
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                chrbank + p->chr4, chrbank + p->chr5,
                chrbank + p->chr6, chrbank + p->chr7,
                p->chr0, p->chr1,
                p->chr2, p->chr3);
        }
        else
        {
            static const mmInt_t chrbank = 0;
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                chrbank + p->chr4, chrbank + p->chr5,
                chrbank + p->chr6, chrbank + p->chr7,
                p->chr0, p->chr1,
                p->chr2, p->chr3);
        }

#if 0
        if (p->reg[0] & 0x80)
        {
            //mmEmuMmu_SetVROM8KBank( 
            //  mmu,
            //  chrbank + p->chr4, chrbank + p->chr5,
            //  chrbank + p->chr6, chrbank + p->chr7,
            //  chrbank + p->chr0, chrbank + p->chr1,
            //  chrbank + p->chr2, chrbank + p->chr3);

            mmEmuMmu_SetVROM8KBank(
                mmu,
                chrbank + p->chr4, chrbank + p->chr5,
                chrbank + p->chr6, chrbank + p->chr7,
                p->chr0, p->chr1,
                p->chr2, p->chr3);
        }
        else
        {
            mmEmuMmu_SetVROM8KBank(
                mmu,
                p->chr0, p->chr1,
                p->chr2, p->chr3,
                chrbank + p->chr4, chrbank + p->chr5,
                chrbank + p->chr6, chrbank + p->chr7);
        }
#endif
    }
}

MM_EXPORT_EMU void mmEmuMapper116_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper116* p = (struct mmEmuMapper116*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = p->prg0L = 0;
    p->prg1 = p->prg1L = 1;
    p->prg2 = mmu->PROM_08K_SIZE - 2;
    p->prg3 = mmu->PROM_08K_SIZE - 1;

    p->ExPrgSwitch = 0;
    p->ExChrSwitch = 0;

    mmEmuMapper116_SetBankCPU(p);

    if (mmu->VROM_1K_SIZE)
    {
        p->chr0 = 0;
        p->chr1 = 1;
        p->chr2 = 2;
        p->chr3 = 3;
        p->chr4 = 4;
        p->chr5 = 5;
        p->chr6 = 6;
        p->chr7 = 7;
        mmEmuMapper116_SetBankPPU(p);
    }
    else
    {
        p->chr0 = p->chr2 = p->chr4 = p->chr5 = p->chr6 = p->chr7 = 0;
        p->chr1 = p->chr3 = 1;
    }

    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0;

    //mmEmuNes_SetFrameIRQmode(nes, MM_FALSE);
}
MM_EXPORT_EMU void mmEmuMapper116_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper116* p = (struct mmEmuMapper116*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    //
    // struct mmLogger* gLogger = mmLogger_Instance();
    //
    // mmLogger_LogD(gLogger, "%s %d MPRWR A=%04X D=%02X L=%3d CYC=%" PRIu64 "", __FUNCTION__, __LINE__,
    //     addr & 0xFFFF,
    //     data & 0xFF,
    //     mmEmuNes_GetScanline(nes),
    //     mmEmuCpu_GetTotalCycles(&nes->cpu));

    if ((addr & 0x4100) == 0x4100)
    {
        p->ExChrSwitch = data;
        mmEmuMapper116_SetBankPPU(p);
    }
}
MM_EXPORT_EMU void mmEmuMapper116_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper116* p = (struct mmEmuMapper116*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // struct mmLogger* gLogger = mmLogger_Instance();
    //
    // mmLogger_LogD(gLogger, "%s %d MPRWR A=%04X D=%02X L=%3d CYC=%" PRIu64 "", __FUNCTION__, __LINE__,
    //     addr & 0xFFFF,
    //     data & 0xFF,
    //     mmEmuNes_GetScanline(nes),
    //     mmEmuCpu_GetTotalCycles(&nes->cpu));

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper116_SetBankCPU(p);
        mmEmuMapper116_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;
        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr0 = data & 0xFE;
            p->chr1 = p->chr0 + 1;
            mmEmuMapper116_SetBankPPU(p);
            break;
        case 0x01:
            p->chr2 = data & 0xFE;
            p->chr3 = p->chr2 + 1;
            mmEmuMapper116_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper116_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper116_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper116_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper116_SetBankPPU(p);
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper116_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper116_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        break;
    case 0xC000:
        p->reg[4] = data;
        p->irq_counter = data;
        //p->irq_enable = 0xFF;
        break;
    case 0xC001:
        p->reg[5] = data;
        p->irq_latch = data;
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_counter = p->irq_latch;
        p->irq_enable = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 0xFF;
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper116_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper116* p = (struct mmEmuMapper116*)(super);

    struct mmEmuNes* nes = p->super.nes;

    //struct mmLogger* gLogger = mmLogger_Instance();

    if ((scanline >= 0 && scanline <= 239))
    {
        if (p->irq_counter <= 0)
        {
            if (p->irq_enable)
            {
                //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);

#if 0
                mmLogger_LogD(gLogger, "%s %d IRQ L=%3d", __FUNCTION__, __LINE__, scanline);

                {
                    mmInt_t i = 0;

                    mmByte_t* lpScn = mmEmuPpu_GetScreenPtr(&nes->ppu);

                    lpScn = lpScn + (256 + 16)*scanline;

                    for (i = 0; i < 64; i++)
                    {
                        lpScn[i] = 22;
                    }
                }
#endif
                return;
            }
        }

        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            p->irq_counter--;
        }
    }

#if 0
    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!(p->irq_counter--))
                {
                    //p->irq_counter = p->irq_latch;
                    mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                }
            }
        }
    }
#endif
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper116_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper116_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper116* p = (struct mmEmuMapper116*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->prg2;
    buffer[11] = p->prg3;
    buffer[12] = p->chr0;
    buffer[13] = p->chr1;
    buffer[14] = p->chr2;
    buffer[15] = p->chr3;
    buffer[16] = p->chr4;
    buffer[17] = p->chr5;
    buffer[18] = p->chr6;
    buffer[19] = p->chr7;
    buffer[20] = p->irq_enable;
    buffer[21] = p->irq_counter;
    buffer[22] = p->irq_latch;
    buffer[23] = p->ExPrgSwitch;
    buffer[24] = p->prg0L;
    buffer[25] = p->prg1L;
    buffer[26] = p->ExChrSwitch;
}
MM_EXPORT_EMU void mmEmuMapper116_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper116* p = (struct mmEmuMapper116*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->prg2 = buffer[10];
    p->prg3 = buffer[11];
    p->chr0 = buffer[12];
    p->chr1 = buffer[13];
    p->chr2 = buffer[14];
    p->chr3 = buffer[15];
    p->chr4 = buffer[16];
    p->chr5 = buffer[17];
    p->chr6 = buffer[18];
    p->chr7 = buffer[19];
    p->irq_enable = buffer[20];
    p->irq_counter = buffer[21];
    p->irq_latch = buffer[22];
    p->ExPrgSwitch = buffer[23];
    p->prg0L = buffer[24];
    p->prg1L = buffer[25];
    p->ExChrSwitch = buffer[26];
}

static struct mmEmuMapper* __static_mmEmuMapper116_Produce(void)
{
    struct mmEmuMapper116* p = (struct mmEmuMapper116*)mmMalloc(sizeof(struct mmEmuMapper116));
    mmEmuMapper116_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper116_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper116* p = (struct mmEmuMapper116*)(m);
    mmEmuMapper116_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER116 =
{
    &__static_mmEmuMapper116_Produce,
    &__static_mmEmuMapper116_Recycle,
};
