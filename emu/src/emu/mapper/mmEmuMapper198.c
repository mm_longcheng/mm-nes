/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper198.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper198_Init(struct mmEmuMapper198* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    mmMemset(p->adr5000buf, 0, sizeof(mmByte_t) * 1024 * 4);

    //
    p->super.Reset = &mmEmuMapper198_Reset;
    p->super.Write = &mmEmuMapper198_Write;
    //p->super.Read = &mmEmuMapper198_Read;
    p->super.ReadLow = &mmEmuMapper198_ReadLow;
    p->super.WriteLow = &mmEmuMapper198_WriteLow;
    //p->super.ExRead = &mmEmuMapper198_ExRead;
    //p->super.ExWrite = &mmEmuMapper198_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper198_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper198_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper198_HSync;
    //p->super.VSync = &mmEmuMapper198_VSync;
    //p->super.Clock = &mmEmuMapper198_Clock;
    //p->super.PPULatch = &mmEmuMapper198_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper198_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper198_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper198_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper198_IsStateSave;
    p->super.SaveState = &mmEmuMapper198_SaveState;
    p->super.LoadState = &mmEmuMapper198_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper198_Destroy(struct mmEmuMapper198* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    mmMemset(p->adr5000buf, 0, sizeof(mmByte_t) * 1024 * 4);
}

MM_EXPORT_EMU void mmEmuMapper198_SetBankCPU(struct mmEmuMapper198* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, p->prg1, p->prg0, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper198_SetBankPPU(struct mmEmuMapper198* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr4, p->chr5, p->chr6, p->chr7,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1);
        }
        else
        {
            mmEmuMmu_SetVROM8KBankArray(
                mmu,
                p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1,
                p->chr4, p->chr5, p->chr6, p->chr7);
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper198_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper198* p = (struct mmEmuMapper198*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);

    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper198_SetBankCPU(p);

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    mmEmuMapper198_SetBankPPU(p);
}
MM_EXPORT_EMU void mmEmuMapper198_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper198* p = (struct mmEmuMapper198*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper198_SetBankCPU(p);
        mmEmuMapper198_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;

        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr01 = data & 0xFE;
            mmEmuMapper198_SetBankPPU(p);
            break;
        case 0x01:
            p->chr23 = data & 0xFE;
            mmEmuMapper198_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper198_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper198_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper198_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper198_SetBankPPU(p);
            break;
        case 0x06:
            if (data >= 0x50)
            {
                data &= 0x4F;
            }
            p->prg0 = data;
            mmEmuMapper198_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper198_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        break;
    case 0xC000:
        p->reg[4] = data;
        break;
    case 0xC001:
        p->reg[5] = data;
        break;
    case 0xE000:
        p->reg[6] = data;
        break;
    case 0xE001:
        p->reg[7] = data;
        break;
    default:
        break;
    }
}
MM_EXPORT_EMU mmByte_t mmEmuMapper198_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper198* p = (struct mmEmuMapper198*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr > 0x4018 && addr < 0x6000)
    {
        return mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF];
    }
    else
    {
        return p->adr5000buf[addr & 0xFFF];
    }
}
MM_EXPORT_EMU void mmEmuMapper198_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper198* p = (struct mmEmuMapper198*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr > 0x4018 && addr < 0x6000)
    {
        mmu->CPU_MEM_BANK[addr >> 13][addr & 0x1FFF] = data;
    }
    else
    {
        p->adr5000buf[addr & 0xFFF] = data;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper198_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper198_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper198* p = (struct mmEmuMapper198*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
}
MM_EXPORT_EMU void mmEmuMapper198_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper198* p = (struct mmEmuMapper198*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
}

static struct mmEmuMapper* __static_mmEmuMapper198_Produce(void)
{
    struct mmEmuMapper198* p = (struct mmEmuMapper198*)mmMalloc(sizeof(struct mmEmuMapper198));
    mmEmuMapper198_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper198_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper198* p = (struct mmEmuMapper198*)(m);
    mmEmuMapper198_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER198 =
{
    &__static_mmEmuMapper198_Produce,
    &__static_mmEmuMapper198_Recycle,
};
