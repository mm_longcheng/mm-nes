/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper182.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper182_Init(struct mmEmuMapper182* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;

    //
    p->super.Reset = &mmEmuMapper182_Reset;
    p->super.Write = &mmEmuMapper182_Write;
    //p->super.Read = &mmEmuMapper182_Read;
    //p->super.ReadLow = &mmEmuMapper182_ReadLow;
    //p->super.WriteLow = &mmEmuMapper182_WriteLow;
    //p->super.ExRead = &mmEmuMapper182_ExRead;
    //p->super.ExWrite = &mmEmuMapper182_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper182_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper182_ExCmdWrite;
    p->super.HSync = &mmEmuMapper182_HSync;
    //p->super.VSync = &mmEmuMapper182_VSync;
    //p->super.Clock = &mmEmuMapper182_Clock;
    //p->super.PPULatch = &mmEmuMapper182_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper182_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper182_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper182_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper182_IsStateSave;
    p->super.SaveState = &mmEmuMapper182_SaveState;
    p->super.LoadState = &mmEmuMapper182_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper182_Destroy(struct mmEmuMapper182* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
}

MM_EXPORT_EMU void mmEmuMapper182_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper182* p = (struct mmEmuMapper182*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    p->reg = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
}
MM_EXPORT_EMU void mmEmuMapper182_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper182* p = (struct mmEmuMapper182*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF003)
    {
    case 0x8001:
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    case 0xA000:
        p->reg = data & 0x07;
        break;
    case 0xC000:
        switch (p->reg)
        {
        case 0:
            mmEmuMmu_SetVROM1KBank(mmu, 0, (data & 0xFE) + 0);
            mmEmuMmu_SetVROM1KBank(mmu, 1, (data & 0xFE) + 1);
            break;
        case 1:
            mmEmuMmu_SetVROM1KBank(mmu, 5, data);
            break;
        case 2:
            mmEmuMmu_SetVROM1KBank(mmu, 2, (data & 0xFE) + 0);
            mmEmuMmu_SetVROM1KBank(mmu, 3, (data & 0xFE) + 1);
            break;
        case 3:
            mmEmuMmu_SetVROM1KBank(mmu, 7, data);
            break;
        case 4:
            mmEmuMmu_SetPROM8KBank(mmu, 4, data);
            break;
        case 5:
            mmEmuMmu_SetPROM8KBank(mmu, 5, data);
            break;
        case 6:
            mmEmuMmu_SetVROM1KBank(mmu, 4, data);
            break;
        case 7:
            mmEmuMmu_SetVROM1KBank(mmu, 6, data);
            break;
        default:
            break;
        }
        break;
    case 0xE003:
        p->irq_enable = data;
        p->irq_counter = data;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper182_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper182* p = (struct mmEmuMapper182*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable)
    {
        if ((scanline >= 0 && scanline <= 239) && mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (!(--p->irq_counter))
            {
                p->irq_enable = 0;
                p->irq_counter = 0;
                //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper182_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper182_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper182* p = (struct mmEmuMapper182*)(super);

    buffer[0] = p->reg;
    buffer[1] = p->irq_enable;
    buffer[2] = p->irq_counter;
}
MM_EXPORT_EMU void mmEmuMapper182_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper182* p = (struct mmEmuMapper182*)(super);

    p->reg = buffer[0];
    p->irq_enable = buffer[1];
    p->irq_counter = buffer[2];
}

static struct mmEmuMapper* __static_mmEmuMapper182_Produce(void)
{
    struct mmEmuMapper182* p = (struct mmEmuMapper182*)mmMalloc(sizeof(struct mmEmuMapper182));
    mmEmuMapper182_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper182_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper182* p = (struct mmEmuMapper182*)(m);
    mmEmuMapper182_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER182 =
{
    &__static_mmEmuMapper182_Produce,
    &__static_mmEmuMapper182_Recycle,
};
