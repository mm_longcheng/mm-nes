/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper089.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper089_Init(struct mmEmuMapper089* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper089_Reset;
    p->super.Write = &mmEmuMapper089_Write;
    //p->super.Read = &mmEmuMapper089_Read;
    //p->super.ReadLow = &mmEmuMapper089_ReadLow;
    //p->super.WriteLow = &mmEmuMapper089_WriteLow;
    //p->super.ExRead = &mmEmuMapper089_ExRead;
    //p->super.ExWrite = &mmEmuMapper089_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper089_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper089_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper089_HSync;
    //p->super.VSync = &mmEmuMapper089_VSync;
    //p->super.Clock = &mmEmuMapper089_Clock;
    //p->super.PPULatch = &mmEmuMapper089_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper089_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper089_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper089_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper089_IsStateSave;
    //p->super.SaveState = &mmEmuMapper089_SaveState;
    //p->super.LoadState = &mmEmuMapper089_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper089_Destroy(struct mmEmuMapper089* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper089_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper089* p = (struct mmEmuMapper089*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper089_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper089* p = (struct mmEmuMapper089*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xFF00) == 0xC000)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, (data & 0x70) >> 4);

        mmEmuMmu_SetVROM8KBank(mmu, ((data & 0x80) >> 4) | (data & 0x07));

        if (data & 0x08)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper089_Produce(void)
{
    struct mmEmuMapper089* p = (struct mmEmuMapper089*)mmMalloc(sizeof(struct mmEmuMapper089));
    mmEmuMapper089_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper089_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper089* p = (struct mmEmuMapper089*)(m);
    mmEmuMapper089_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER089 =
{
    &__static_mmEmuMapper089_Produce,
    &__static_mmEmuMapper089_Recycle,
};
