/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper020.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuExtSoundFile.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuMapper020_Init(struct mmEmuMapper020* p)
{
    mmEmuMapper_Init(&p->super);

    p->bDiskThrottle = MM_FALSE;
    p->DiskThrottleTime = 0;

    p->disk = NULL;
    p->disk_w = NULL;

    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_enable = 0;
    p->irq_repeat = 0;
    p->irq_occur = 0;
    p->irq_transfer = 0;

    p->disk_enable = 0xFF;
    p->sound_enable = 0xFF;
    p->RW_start = 0xFF;
    p->RW_mode = 0;
    p->disk_motor_mode = 0;
    p->disk_eject = 0xFF;
    p->drive_ready = 0;
    p->drive_reset = 0;

    p->block_point = 0;
    p->block_mode = 0;
    p->size_file_data = 0;
    p->file_amount = 0;
    p->point = 0;
    p->first_access = 0;

    p->disk_side = 0xFF;
    p->disk_mount_count = 119;

    p->irq_type = 0;

    p->sound_startup_flag = 0xFF;
    p->sound_startup_timer = -1;
    p->sound_seekend_timer = -1;

    //
    p->super.Reset = &mmEmuMapper020_Reset;
    p->super.Write = &mmEmuMapper020_Write;
    //p->super.Read = &mmEmuMapper020_Read;
    //p->super.ReadLow = &mmEmuMapper020_ReadLow;
    p->super.WriteLow = &mmEmuMapper020_WriteLow;
    p->super.ExRead = &mmEmuMapper020_ExRead;
    p->super.ExWrite = &mmEmuMapper020_ExWrite;
    p->super.ExCmdRead = &mmEmuMapper020_ExCmdRead;
    p->super.ExCmdWrite = &mmEmuMapper020_ExCmdWrite;
    p->super.HSync = &mmEmuMapper020_HSync;
    p->super.VSync = &mmEmuMapper020_VSync;
    p->super.Clock = &mmEmuMapper020_Clock;
    //p->super.PPULatch = &mmEmuMapper020_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper020_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper020_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper020_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper020_IsStateSave;
    p->super.SaveState = &mmEmuMapper020_SaveState;
    p->super.LoadState = &mmEmuMapper020_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper020_Destroy(struct mmEmuMapper020* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->bDiskThrottle = MM_FALSE;
    p->DiskThrottleTime = 0;

    p->disk = NULL;
    p->disk_w = NULL;

    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_enable = 0;
    p->irq_repeat = 0;
    p->irq_occur = 0;
    p->irq_transfer = 0;

    p->disk_enable = 0xFF;
    p->sound_enable = 0xFF;
    p->RW_start = 0xFF;
    p->RW_mode = 0;
    p->disk_motor_mode = 0;
    p->disk_eject = 0xFF;
    p->drive_ready = 0;
    p->drive_reset = 0;

    p->block_point = 0;
    p->block_mode = 0;
    p->size_file_data = 0;
    p->file_amount = 0;
    p->point = 0;
    p->first_access = 0;

    p->disk_side = 0xFF;
    p->disk_mount_count = 119;

    p->irq_type = 0;

    p->sound_startup_flag = 0xFF;
    p->sound_startup_timer = -1;
    p->sound_seekend_timer = -1;
}

MM_EXPORT_EMU void mmEmuMapper020_MechanicalSound(struct mmEmuMapper020* p, mmInt_t type)
{
    struct mmEmuNes* nes = p->super.nes;

    struct mmEmuAudioConfig* audio_config = &nes->config.audio;

    switch (type)
    {
    case MM_EMU_MAPPER020_MECHANICAL_SOUND_BOOT:
        // Head start point CAM sound.
        if (audio_config->bExtraSoundEnable)
        {
            mmEmuNes_EsfPlay(nes, MM_EMU_ESF_DISKSYSTEM_BOOT);
        }
        break;
    case MM_EMU_MAPPER020_MECHANICAL_SOUND_SEEKEND:
        if (audio_config->bExtraSoundEnable)
        {
            mmEmuNes_EsfPlay(nes, MM_EMU_ESF_DISKSYSTEM_SEEKEND);
        }
        // Reset or Seekend sound.
        break;
    case MM_EMU_MAPPER020_MECHANICAL_SOUND_MOTOR_ON:
        if (audio_config->bExtraSoundEnable)
        {
            mmEmuNes_EsfPlayLoop(nes, MM_EMU_ESF_DISKSYSTEM_MOTOR);
        }
        // Start Motor sound.(loop)
        break;
    case MM_EMU_MAPPER020_MECHANICAL_SOUND_MOTOR_OFF:
        mmEmuNes_EsfStop(nes, MM_EMU_ESF_DISKSYSTEM_MOTOR);
        // Stop Motor sound.(loop)
        break;
    case MM_EMU_MAPPER020_MECHANICAL_SOUND_ALLSTOP:
        // Stop sound.
        mmEmuNes_EsfAllStop(nes);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper020_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt32_t MakerID = 0;
    mmUInt32_t GameID = 0;

    p->irq_type = 0;

    p->irq_enable = p->irq_repeat = 0;
    p->irq_counter = p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_transfer = 0;

    p->disk_enable = 0xFF;
    p->sound_enable = 0xFF;

    p->block_point = 0;
    p->block_mode = 0;
    p->RW_start = 0xFF;
    p->size_file_data = 0;
    p->file_amount = 0;
    p->point = 0;
    p->first_access = 0;

    p->disk_eject = 0xFF;
    p->drive_ready = 0;

    p->disk_side = 0xFF;    // Eject
    p->disk = p->disk_w = NULL;

    // Mechanical sound
    p->sound_startup_flag = 0xFF;
    p->sound_startup_timer = -1;    // stop
    p->sound_seekend_timer = -1;    // stop
    mmEmuMapper020_MechanicalSound(p, MM_EMU_MAPPER020_MECHANICAL_SOUND_ALLSTOP);

    //mmMemset(mmu->DRAM, 0xFF, sizeof(mmu->DRAM));
    mmEmuMmu_SetPROMBank(mmu, 3, mmu->DRAM + 0x0000, MM_EMU_MMU_BANKTYPE_DRAM);
    mmEmuMmu_SetPROMBank(mmu, 4, mmu->DRAM + 0x2000, MM_EMU_MMU_BANKTYPE_DRAM);
    mmEmuMmu_SetPROMBank(mmu, 5, mmu->DRAM + 0x4000, MM_EMU_MMU_BANKTYPE_DRAM);
    mmEmuMmu_SetPROMBank(mmu, 6, mmu->DRAM + 0x6000, MM_EMU_MMU_BANKTYPE_DRAM);
    mmEmuMmu_SetPROMBank(mmu, 7, mmEmuRom_GetDISKBIOS(&nes->rom), MM_EMU_MMU_BANKTYPE_ROM);
    mmEmuMmu_SetCRAM8KBank(mmu, 0);

    // Default
    //mmEmuNes_SetIrqType(nes, MM_EMU_NES_IRQ_HSYNC);

    MakerID = mmEmuRom_GetMakerID(&nes->rom);
    GameID = mmEmuRom_GetGameID(&nes->rom);

    // Mario Brothers came back.
    if (MakerID == 0x01 && GameID == 0x4b4d4152)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }

    // Garforth.
    if (MakerID == 0xB6 && GameID == 0x47414C20)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
    // Fire bum.
    if (MakerID == 0xB6 && GameID == 0x46424D20)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
    // 3D Hot Rally.
    if (MakerID == 0x01 && GameID == 0x54445245)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
        p->irq_type = 1;    // Inchi
    }
    // Time twist.
    if (MakerID == 0x01 && GameID == 0x54540120)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
    // Bio Miracle I am UPA.
    if (MakerID == 0xA4 && GameID == 0x424F4B20)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_PRE_ALL_RENDER);
    }
    // Pat putt Golf.
    if (MakerID == 0x99 && GameID == 0x50504720)
    {
        p->irq_type = 99;
    }

    // Pacific.
    if (MakerID == 0x00 && GameID == 0x00000000)
    {
        mmMemset(mmu->RAM + 0x100, 0xFF, 0x100);
    }

    //mmLogger_LogD(gLogger, "%s %d MAKER ID=%02X", __FUNCTION__, __LINE__, MakerID);
    //mmLogger_LogD(gLogger, "%s %d GAME  ID=%08X", __FUNCTION__, __LINE__, GameID);

    mmEmuApu_SelectExSound(&nes->apu, 4);

    //mmEmuMapper020_ExCmdWrite(super, MM_EMU_MAPPER_EXCMDWR_DISKINSERT, 0);
    // Set Disk 0, Side A
    p->disk = mmEmuRom_GetPROM(&nes->rom) + 16 + 65500 * 0;
    p->disk_w = mmEmuRom_GetDISK(&nes->rom) + 16 + 65500 * 0;

    p->disk_side = 0;
    p->disk_eject = 0xFF;
    p->drive_ready = 0;
    p->disk_mount_count = 119;

    mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_THROTTLE_OFF);

    p->bDiskThrottle = MM_FALSE;
    p->DiskThrottleTime = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuMapper020_ExRead(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmByte_t data = addr >> 8;

    switch (addr)
    {
    case 0x4030:    // Disk I/O status
        data = 0x80;
        data |= (p->irq_occur && !p->irq_transfer) ? 0x01 : 0x00;
        data |= (p->irq_occur && p->irq_transfer) ? 0x02 : 0x00;
        p->irq_occur = 0;

        // Clear Timer IRQ
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        // Clear Disk IRQ
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER2);
        break;
    case 0x4031:    // Disk data read
        if (!p->RW_mode)
        {
            return 0xFF;
        }

        p->first_access = 0;

        if (p->disk)
        {
            switch (p->block_mode)
            {
            case MM_EMU_MAPPER020_BLOCK_VOLUME_LABEL:
                data = p->disk[p->block_point];
                if (p->block_point < MM_EMU_MAPPER020_SIZE_VOLUME_LABEL)
                {
                    p->block_point++;
                }
                else
                {
                    data = 0;
                }
                return  data;
            case MM_EMU_MAPPER020_BLOCK_FILE_AMOUNT:
                data = p->disk[p->block_point + p->point];
                if (p->block_point < MM_EMU_MAPPER020_SIZE_FILE_AMOUNT)
                {
                    p->block_point++;
                    p->file_amount = data;
                }
                else
                {
                    data = 0;
                }
                return  data;
            case MM_EMU_MAPPER020_BLOCK_FILE_HEADER:
                data = p->disk[p->block_point + p->point];
                if (p->block_point == 13)
                {
                    p->size_file_data = data;
                }
                else if (p->block_point == 14)
                {
                    p->size_file_data += ((mmInt_t)data << 8);
                }

                if (p->block_point < MM_EMU_MAPPER020_SIZE_FILE_HEADER)
                {
                    p->block_point++;
                }
                else
                {
                    data = 0;
                }
                return  data;
            case MM_EMU_MAPPER020_BLOCK_FILE_DATA:
                data = p->disk[p->block_point + p->point];
                if (p->block_point < p->size_file_data + 1)
                {
                    p->block_point++;
                }
                else
                {
                    data = 0;
                }
                return data;
            default:
                break;
            }
        }
        else
        {
            return 0xFF;
        }
        break;
    case 0x4032:    // Disk status
        data = 0x40;
        data |= p->disk_eject ? 0x01 : 0x00;
        data |= p->disk_eject ? 0x04 : 0x00;
        data |= (!p->disk_eject && p->disk_motor_mode && !p->drive_reset) ? 0x00 : 0x02;
        break;
    case 0x4033:    // External connector data/Battery sense
        data = 0x80;
        break;
    default:
        if (addr >= 0x4040)
        {
            data = mmEmuApu_ExRead(&nes->apu, addr);
        }
        break;
    }

    //if( addr >= 0x4030 && addr <= 0x4033 )
    //{
    //  struct mmLogger* gLogger = mmLogger_Instance();
    //  mmLogger_LogD(gLogger, "%s %d R %04X %02X", __FUNCTION__, __LINE__, addr, data);
    //}
    return data;
}
MM_EXPORT_EMU void mmEmuMapper020_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "W %04X %02X C:%d\n", addr, data, nes->cpu->GetTotalCycles() );
    switch (addr)
    {
    case 0x4020:    // IRQ latch low
        p->irq_latch = (p->irq_latch & 0xFF00) | data;
        break;
    case 0x4021:    // IRQ latch high
        p->irq_latch = (p->irq_latch & 0x00FF) | ((mmWord_t)data << 8);
        break;
    case 0x4022:    // IRQ control
        p->irq_repeat = data & 0x01;
        p->irq_enable = (data & 0x02) && (p->disk_enable);
        p->irq_occur = 0;
        if (p->irq_enable)
        {
            p->irq_counter = p->irq_latch;
        }
        else
        {
            // Clear Timer IRQ
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        }
        break;

    case 0x4023: // 2C33 control
        p->disk_enable = data & 0x01;
        if (!p->disk_enable)
        {
            p->irq_enable = 0;
            p->irq_occur = 0;
            // Clear Timer IRQ
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            // Clear Disk IRQ
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER2);
        }
        break;

    case 0x4024:    // Data write
        // Clear Disk IRQ
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER2);

        if (p->RW_mode)
        {
            break;
        }

        if (p->first_access)
        {
            p->first_access = 0;
            break;
        }

        if (p->disk)
        {
            switch (p->block_mode)
            {
            case MM_EMU_MAPPER020_BLOCK_VOLUME_LABEL:
                if (p->block_point < MM_EMU_MAPPER020_SIZE_VOLUME_LABEL - 1)
                {
                    p->disk[p->block_point] = data;
                    p->disk_w[p->block_point] = 0xFF;
                    p->block_point++;
                }
                break;
            case MM_EMU_MAPPER020_BLOCK_FILE_AMOUNT:
                if (p->block_point < MM_EMU_MAPPER020_SIZE_FILE_AMOUNT)
                {
                    p->disk[p->block_point + p->point] = data;
                    p->disk_w[p->block_point + p->point] = 0xFF;
                    p->block_point++;
                }
                break;
            case MM_EMU_MAPPER020_BLOCK_FILE_HEADER:
                if (p->block_point < MM_EMU_MAPPER020_SIZE_FILE_HEADER)
                {
                    p->disk[p->block_point + p->point] = data;
                    p->disk_w[p->block_point + p->point] = 0xFF;
                    if (p->block_point == 13)
                    {
                        p->size_file_data = data;
                    }
                    else if (p->block_point == 14)
                    {
                        p->size_file_data |= data << 8;
                    }
                    p->block_point++;
                }
                break;
            case MM_EMU_MAPPER020_BLOCK_FILE_DATA:
                if (p->block_point < p->size_file_data + 1)
                {
                    p->disk[p->block_point + p->point] = data;
                    p->disk_w[p->block_point + p->point] = 0xFF;
                    p->block_point++;
                }
                break;
            default:
                break;
            }
        }
        break;

    case 0x4025:    // Disk I/O control
        // Interrupt transfer
        p->irq_transfer = data & 0x80;
        if (!p->irq_transfer)
        {
            mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER2);
        }

        if (!p->RW_start && (data & 0x40))
        {
            p->block_point = 0;
            switch (p->block_mode)
            {
            case MM_EMU_MAPPER020_BLOCK_READY:
                p->block_mode = MM_EMU_MAPPER020_BLOCK_VOLUME_LABEL;
                p->point = 0;
                break;
            case MM_EMU_MAPPER020_BLOCK_VOLUME_LABEL:
                p->block_mode = MM_EMU_MAPPER020_BLOCK_FILE_AMOUNT;
                p->point += MM_EMU_MAPPER020_SIZE_VOLUME_LABEL;
                break;
            case MM_EMU_MAPPER020_BLOCK_FILE_AMOUNT:
                p->block_mode = MM_EMU_MAPPER020_BLOCK_FILE_HEADER;
                p->point += MM_EMU_MAPPER020_SIZE_FILE_AMOUNT;
                break;
            case MM_EMU_MAPPER020_BLOCK_FILE_HEADER:
                p->block_mode = MM_EMU_MAPPER020_BLOCK_FILE_DATA;
                p->point += MM_EMU_MAPPER020_SIZE_FILE_HEADER;
                break;
            case MM_EMU_MAPPER020_BLOCK_FILE_DATA:
                p->block_mode = MM_EMU_MAPPER020_BLOCK_FILE_HEADER;
                p->point += p->size_file_data + 1;
                break;
            default:
                break;
            }

            // In order to ignore the first first write.
            p->first_access = 0xFF;
        }

        // Start reading and writing.
        p->RW_start = data & 0x40;

        // Read / write mode.
        p->RW_mode = data & 0x04;

        // Reset read / write.
        if (data & 0x02)
        {
            p->point = 0;
            p->block_point = 0;
            p->block_mode = MM_EMU_MAPPER020_BLOCK_READY;
            p->RW_start = 0xFF;
            p->drive_reset = 0xFF;

            p->sound_startup_flag = 0;
            p->sound_startup_timer = -1;    // stop
        }
        else
        {
            p->drive_reset = 0;

            if (!p->sound_startup_flag)
            {
                mmEmuMapper020_MechanicalSound(p, MM_EMU_MAPPER020_MECHANICAL_SOUND_MOTOR_ON);
                p->sound_startup_flag = 0xFF;
                p->sound_startup_timer = 40;
                p->sound_seekend_timer = 60 * 7;
            }
        }

        // Disk motor control.
        p->disk_motor_mode = data & 0x01;
        if (!(data & 0x01))
        {
            if (p->sound_seekend_timer >= 0)
            {
                p->sound_seekend_timer = -1;
                mmEmuMapper020_MechanicalSound(p, MM_EMU_MAPPER020_MECHANICAL_SOUND_MOTOR_OFF);
            }
        }

        // Mirror
        if (data & 0x08)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;

    case 0x4026:    // External connector output/Battery sense
        break;
    default:
        if (addr >= 0x4040)
        {
            mmEmuApu_ExWrite(&nes->apu, addr, data);
        }
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper020_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmu->DRAM[addr - 0x6000] = data;
}

MM_EXPORT_EMU void mmEmuMapper020_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (addr < 0xE000)
    {
        mmu->DRAM[addr - 0x6000] = data;
    }
}

MM_EXPORT_EMU void mmEmuMapper020_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;

    // Timer IRQ
    if (p->irq_enable)
    {
        if (!p->irq_type)
        {
            p->irq_counter -= cycles;
        }
        if (p->irq_counter <= 0)
        {
            //p->irq_counter &= 0xFFFF;
            p->irq_counter += p->irq_latch;

            if (!p->irq_occur)
            {
                p->irq_occur = 0xFF;
                if (!p->irq_repeat)
                {
                    p->irq_enable = 0;
                }
                if (p->irq_type != 99)
                {
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
                else
                {
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_TRIGGER2);
                }
            }
        }
        if (p->irq_type)
        {
            p->irq_counter -= cycles;
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper020_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;

    // Disk IRQ
    if (p->irq_transfer)
    {
        mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER2);
    }
}
MM_EXPORT_EMU void mmEmuMapper020_VSync(struct mmEmuMapper* super)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->disk && p->disk_eject)
    {
        if (p->disk_mount_count > 120)
        {
            p->disk_eject = 0;
        }
        else
        {
            p->disk_mount_count++;
        }
    }

    //  if( disk && (disk_mount_count > 120) )
    if (p->disk)
    {
        if (p->sound_startup_timer > 0)
        {
            p->sound_startup_timer--;
        }
        else if (p->sound_startup_timer == 0)
        {
            p->sound_startup_timer--;
            mmEmuMapper020_MechanicalSound(p, MM_EMU_MAPPER020_MECHANICAL_SOUND_BOOT);
        }

        if (p->sound_seekend_timer > 0)
        {
            p->sound_seekend_timer--;
        }
        else if (p->sound_seekend_timer == 0)
        {
            p->sound_seekend_timer--;
            mmEmuMapper020_MechanicalSound(p, MM_EMU_MAPPER020_MECHANICAL_SOUND_MOTOR_OFF);
            mmEmuMapper020_MechanicalSound(p, MM_EMU_MAPPER020_MECHANICAL_SOUND_SEEKEND);
            p->sound_startup_flag = 0;
        }
    }

    if (p->irq_transfer || (p->disk && p->disk_mount_count < 120))
    {
        if (p->DiskThrottleTime > 2)
        {
            p->bDiskThrottle = MM_TRUE;
        }
        else
        {
            p->bDiskThrottle = MM_FALSE;
            p->DiskThrottleTime++;
        }
    }
    else
    {
        p->DiskThrottleTime = 0;
        p->bDiskThrottle = MM_FALSE;
    }
    if (!p->bDiskThrottle)
    {
        mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_THROTTLE_OFF);
    }
    else
    {
        mmEmuNes_Command(nes, MM_EMU_NES_NESCMD_DISK_THROTTLE_ON);
    }
}

MM_EXPORT_EMU mmByte_t mmEmuMapper020_ExCmdRead(struct mmEmuMapper* super, mmInt_t cmd)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    mmByte_t data = 0x00;

    if (cmd == MM_EMU_MAPPER_EXCMDRD_DISKACCESS)
    {
        if (p->irq_transfer)
        {
            return 0xFF;
        }
    }

    return data;
}
MM_EXPORT_EMU void mmEmuMapper020_ExCmdWrite(struct mmEmuMapper* super, mmInt_t cmd, mmByte_t data)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;

    switch (cmd)
    {
    case MM_EMU_MAPPER_EXCMDWR_NONE:
        break;
    case MM_EMU_MAPPER_EXCMDWR_DISKINSERT:
        p->disk = mmEmuRom_GetPROM(&nes->rom) + 16 + 65500 * data;
        p->disk_w = mmEmuRom_GetDISK(&nes->rom) + 16 + 65500 * data;
        p->disk_side = data;
        p->disk_eject = 0xFF;
        p->drive_ready = 0;
        p->disk_mount_count = 0;
        break;
    case MM_EMU_MAPPER_EXCMDWR_DISKEJECT:
        p->disk = NULL; // Tentatively
        p->disk_w = NULL;
        p->disk_side = 0xFF;
        p->disk_eject = 0xFF;
        p->drive_ready = 0;
        p->disk_mount_count = 0;
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper020_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper020_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_repeat;
    buffer[2] = p->irq_occur;
    buffer[3] = p->irq_transfer;

    *(mmInt_t*)&buffer[4] = p->irq_counter;
    *(mmInt_t*)&buffer[8] = p->irq_latch;

    buffer[12] = p->disk_enable;
    buffer[13] = p->sound_enable;
    buffer[14] = p->RW_start;
    buffer[15] = p->RW_mode;
    buffer[16] = p->disk_motor_mode;
    buffer[17] = p->disk_eject;
    buffer[18] = p->drive_ready;
    buffer[19] = p->drive_reset;

    *(mmInt_t*)&buffer[20] = p->block_point;
    *(mmInt_t*)&buffer[24] = p->block_mode;
    *(mmInt_t*)&buffer[28] = p->size_file_data;
    *(mmInt_t*)&buffer[32] = p->file_amount;
    *(mmInt_t*)&buffer[36] = p->point;

    buffer[40] = p->first_access;
    buffer[41] = p->disk_side;
    buffer[42] = p->disk_mount_count;

    buffer[44] = p->sound_startup_flag;
    *(mmInt_t*)&buffer[48] = p->sound_startup_timer;
    *(mmInt_t*)&buffer[52] = p->sound_seekend_timer;
}
MM_EXPORT_EMU void mmEmuMapper020_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->irq_enable = buffer[0];
    p->irq_repeat = buffer[1];
    p->irq_occur = buffer[2];
    p->irq_transfer = buffer[3];

    p->irq_counter = *(mmInt_t*)&buffer[4];
    p->irq_latch = *(mmInt_t*)&buffer[8];

    p->disk_enable = buffer[12];
    p->sound_enable = buffer[13];
    p->RW_start = buffer[14];
    p->RW_mode = buffer[15];
    p->disk_motor_mode = buffer[16];
    p->disk_eject = buffer[17];
    p->drive_ready = buffer[18];
    p->drive_reset = buffer[19];

    p->block_point = *(mmInt_t*)&buffer[20];
    p->block_mode = *(mmInt_t*)&buffer[24];
    p->size_file_data = *(mmInt_t*)&buffer[28];
    p->file_amount = *(mmInt_t*)&buffer[32];
    p->point = *(mmInt_t*)&buffer[36];

    p->first_access = buffer[40];
    p->disk_side = buffer[41];
    p->disk_mount_count = buffer[42];

    p->sound_startup_flag = buffer[44];
    p->sound_startup_timer = *(mmInt_t*)&buffer[48];
    p->sound_seekend_timer = *(mmInt_t*)&buffer[52];

    if (p->disk_side != 0xFF)
    {
        p->disk = mmEmuRom_GetPROM(&nes->rom) + sizeof(struct mmEmuRomNesHeader) + 65500 * p->disk_side;
        p->disk_w = mmEmuRom_GetDISK(&nes->rom) + sizeof(struct mmEmuRomNesHeader) + 65500 * p->disk_side;
    }
    else
    {
        p->disk = NULL;
        p->disk_w = NULL;
    }

    // DiskBios Setup (because it is overwritten in the state).
    mmEmuMmu_SetPROMBank(mmu, 7, mmEmuRom_GetDISKBIOS(&nes->rom), MM_EMU_MMU_BANKTYPE_ROM);
}

static struct mmEmuMapper* __static_mmEmuMapper020_Produce(void)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)mmMalloc(sizeof(struct mmEmuMapper020));
    mmEmuMapper020_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper020_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper020* p = (struct mmEmuMapper020*)(m);
    mmEmuMapper020_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER020 =
{
    &__static_mmEmuMapper020_Produce,
    &__static_mmEmuMapper020_Recycle,
};
