/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper234.h"
#include "emu/mmEmuNes.h"


MM_EXPORT_EMU void mmEmuMapper234_Init(struct mmEmuMapper234* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 0;

    //
    p->super.Reset = &mmEmuMapper234_Reset;
    p->super.Write = &mmEmuMapper234_Write;
    p->super.Read = &mmEmuMapper234_Read;
    //p->super.ReadLow = &mmEmuMapper234_ReadLow;
    //p->super.WriteLow = &mmEmuMapper234_WriteLow;
    //p->super.ExRead = &mmEmuMapper234_ExRead;
    //p->super.ExWrite = &mmEmuMapper234_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper234_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper234_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper234_HSync;
    //p->super.VSync = &mmEmuMapper234_VSync;
    //p->super.Clock = &mmEmuMapper234_Clock;
    //p->super.PPULatch = &mmEmuMapper234_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper234_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper234_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper234_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper234_IsStateSave;
    p->super.SaveState = &mmEmuMapper234_SaveState;
    p->super.LoadState = &mmEmuMapper234_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper234_Destroy(struct mmEmuMapper234* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg[0] = 0;
    p->reg[1] = 0;
}
//----------------------------------------------------------------------//
MM_EXPORT_EMU void mmEmuMapper234_SetBank(struct mmEmuMapper234* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x80)
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
    }
    else
    {
        mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
    }
    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM32KBank(mmu, (p->reg[0] & 0x0E) | (p->reg[1] & 0x01));
        mmEmuMmu_SetVROM8KBank(mmu, ((p->reg[0] & 0x0E) << 2) | ((p->reg[1] >> 4) & 0x07));
    }
    else
    {
        mmEmuMmu_SetPROM32KBank(mmu, p->reg[0] & 0x0F);
        mmEmuMmu_SetVROM8KBank(mmu, ((p->reg[0] & 0x0F) << 2) | ((p->reg[1] >> 4) & 0x03));
    }
}

MM_EXPORT_EMU void mmEmuMapper234_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper234* p = (struct mmEmuMapper234*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);

    p->reg[0] = 0;
    p->reg[1] = 0;
}
MM_EXPORT_EMU void mmEmuMapper234_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper234* p = (struct mmEmuMapper234*)(super);

    // struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    if (addr >= 0xFF80 && addr <= 0xFF9F)
    {
        if (!p->reg[0])
        {
            p->reg[0] = data;
            mmEmuMapper234_SetBank(p);
        }
    }

    if (addr >= 0xFFE8 && addr <= 0xFFF7)
    {
        p->reg[1] = data;
        mmEmuMapper234_SetBank(p);
    }
}
MM_EXPORT_EMU void mmEmuMapper234_Read(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper234* p = (struct mmEmuMapper234*)(super);

    if (addr >= 0xFF80 && addr <= 0xFF9F)
    {
        if (!p->reg[0])
        {
            p->reg[0] = data;
            mmEmuMapper234_SetBank(p);
        }
    }

    if (addr >= 0xFFE8 && addr <= 0xFFF7)
    {
        p->reg[1] = data;
        mmEmuMapper234_SetBank(p);
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper234_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper234_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper234* p = (struct mmEmuMapper234*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
}
MM_EXPORT_EMU void mmEmuMapper234_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper234* p = (struct mmEmuMapper234*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
}

static struct mmEmuMapper* __static_mmEmuMapper234_Produce(void)
{
    struct mmEmuMapper234* p = (struct mmEmuMapper234*)mmMalloc(sizeof(struct mmEmuMapper234));
    mmEmuMapper234_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper234_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper234* p = (struct mmEmuMapper234*)(m);
    mmEmuMapper234_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER234 =
{
    &__static_mmEmuMapper234_Produce,
    &__static_mmEmuMapper234_Recycle,
};
