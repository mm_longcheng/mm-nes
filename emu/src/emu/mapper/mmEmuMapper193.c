/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper193.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper193_Init(struct mmEmuMapper193* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper193_Reset;
    //p->super.Write = &mmEmuMapper193_Write;
    //p->super.Read = &mmEmuMapper193_Read;
    //p->super.ReadLow = &mmEmuMapper193_ReadLow;
    p->super.WriteLow = &mmEmuMapper193_WriteLow;
    //p->super.ExRead = &mmEmuMapper193_ExRead;
    //p->super.ExWrite = &mmEmuMapper193_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper193_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper193_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper193_HSync;
    //p->super.VSync = &mmEmuMapper193_VSync;
    //p->super.Clock = &mmEmuMapper193_Clock;
    //p->super.PPULatch = &mmEmuMapper193_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper193_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper193_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper193_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper193_IsStateSave;
    //p->super.SaveState = &mmEmuMapper193_SaveState;
    //p->super.LoadState = &mmEmuMapper193_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper193_Destroy(struct mmEmuMapper193* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper193_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper193* p = (struct mmEmuMapper193*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, mmu->PROM_32K_SIZE - 1);
    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper193_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper193* p = (struct mmEmuMapper193*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x6000:
        mmEmuMmu_SetVROM2KBank(mmu, 0, ((data >> 1) & 0x7e) + 0);
        mmEmuMmu_SetVROM2KBank(mmu, 2, ((data >> 1) & 0x7e) + 1);
        break;
    case 0x6001:
        mmEmuMmu_SetVROM2KBank(mmu, 4, data >> 1);
        break;
    case 0x6002:
        mmEmuMmu_SetVROM2KBank(mmu, 6, data >> 1);
        break;
    case 0x6003:
        mmEmuMmu_SetPROM32KBank(mmu, data);
        break;
    default:
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper193_Produce(void)
{
    struct mmEmuMapper193* p = (struct mmEmuMapper193*)mmMalloc(sizeof(struct mmEmuMapper193));
    mmEmuMapper193_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper193_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper193* p = (struct mmEmuMapper193*)(m);
    mmEmuMapper193_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER193 =
{
    &__static_mmEmuMapper193_Produce,
    &__static_mmEmuMapper193_Recycle,
};
