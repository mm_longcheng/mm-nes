/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuStateFile_h__
#define __mmEmuStateFile_h__

#include "core/mmCore.h"

#include "mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuNes;

// 0:ERROR 1:CRC OK -1:CRC ERR
MM_EXPORT_EMU mmInt_t mmEmuStateFile_IsStateFile(const struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU mmUInt32_t mmEmuStateFile_LoadState(struct mmEmuNes* p, const char* fname);
MM_EXPORT_EMU mmUInt32_t mmEmuStateFile_SaveState(struct mmEmuNes* p, const char* fname);
//
MM_EXPORT_EMU int mmEmuStateFile_ReadState(struct mmEmuNes* p, FILE* fp);
MM_EXPORT_EMU int mmEmuStateFile_WriteState(struct mmEmuNes* p, FILE* fp);

#include "core/mmSuffix.h"

#endif//__mmEmuStateFile_h__
