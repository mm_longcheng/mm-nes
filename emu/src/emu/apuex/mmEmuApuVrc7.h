/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApuVrc7_h__
#define __mmEmuApuVrc7_h__

#include "core/mmCore.h"

#include "emu/mmEmuApuInterface.h"
#include "emu/mmEmu2413.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuApuVrc7
{
    struct mmEmuApuInterface super;

    struct mmEmuOpll VRC7_OPLL;

    mmByte_t    address;
};

MM_EXPORT_EMU void mmEmuApuVrc7_Init(struct mmEmuApuVrc7* p);
MM_EXPORT_EMU void mmEmuApuVrc7_Destroy(struct mmEmuApuVrc7* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuApuVrc7_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuVrc7_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuVrc7_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmInt_t mmEmuApuVrc7_Process(struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuVrc7_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel);

#include "core/mmSuffix.h"

#endif//__mmEmuApuVrc7_h__
