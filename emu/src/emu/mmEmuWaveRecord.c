/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuWaveRecord.h"

#include "core/mmAlloc.h"

MM_EXPORT_EMU void mmEmuWaveRecord_Init(struct mmEmuWaveRecord* p)
{
    mmMemset(&p->wavefile, 0, sizeof(struct mmEmuWaveFile));
    p->fp = NULL;

    mmMemcpy(p->wavefile.tagRIFF, "RIFF", sizeof(p->wavefile.tagRIFF));
    mmMemcpy(p->wavefile.tagTYPE, "WAVEfmt ", sizeof(p->wavefile.tagTYPE));
    mmMemcpy(p->wavefile.tagDATA, "data", sizeof(p->wavefile.tagDATA));

    p->wavefile.dwChunkOffset = 0x0010;
    p->wavefile.wCodingType = 0x0001;
}
MM_EXPORT_EMU void mmEmuWaveRecord_Destroy(struct mmEmuWaveRecord* p)
{
    if (p->fp)
    {
        mmEmuWaveRecord_Stop(p);
    }

    mmMemset(&p->wavefile, 0, sizeof(struct mmEmuWaveFile));
    p->fp = NULL;
}

MM_EXPORT_EMU void mmEmuWaveRecord_Start(struct mmEmuWaveRecord* p, const char* szFile, mmInt_t nSample, mmInt_t nBits, mmBool_t bStereo)
{
    if (p->fp)
    {
        mmEmuWaveRecord_Stop(p);
    }

    if ((p->fp = fopen(szFile, "wb")))
    {
        p->wavefile.wChannel = bStereo ? 2 : 1;
        p->wavefile.dwSample = nSample;
        p->wavefile.wBits = nBits;
        p->wavefile.wBytesPerSample = (mmWord_t)(nBits >> 3) * p->wavefile.wChannel;
        p->wavefile.dwBytesPerSec = nSample * p->wavefile.wBytesPerSample;
    }
}
MM_EXPORT_EMU void mmEmuWaveRecord_Stop(struct mmEmuWaveRecord* p)
{
    if (p->fp)
    {
        mmUInt32_t size = (mmUInt32_t)ftell(p->fp);
        p->wavefile.dwFileSize = (mmUInt32_t)size - 8;
        p->wavefile.dwDataSize = (mmUInt32_t)size - sizeof(struct mmEmuWaveFile);
        fseek(p->fp, 0, SEEK_SET);
        fwrite(&p->wavefile, sizeof(struct mmEmuWaveFile), 1, p->fp);
        fclose(p->fp);
        p->fp = NULL;
    }
}

MM_EXPORT_EMU mmBool_t mmEmuWaveRecord_IsWaveRecord(const struct mmEmuWaveRecord* p)
{
    return p->fp ? MM_TRUE : MM_FALSE;
}

MM_EXPORT_EMU void mmEmuWaveRecord_Output(struct mmEmuWaveRecord* p, void* lpBuf, mmUInt32_t dwSize)
{
    if (p->fp)
    {
        fwrite(lpBuf, dwSize, 1, p->fp);
    }
}

