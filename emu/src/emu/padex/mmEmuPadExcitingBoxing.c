/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuPadExcitingBoxing.h"
#include "emu/mmEmuNes.h"
#include "emu/mmEmuController.h"

MM_EXPORT_EMU void mmEmuPadExcitingBoxing_Init(struct mmEmuPadExcitingBoxing* p)
{
    mmEmuExpad_Init(&p->super);

    p->outbits = 0;
    p->padbits = 0;

    //
    p->super.Reset = &mmEmuPadExcitingBoxing_Reset;
    //p->super.Strobe = &mm_emu_pad_excitingboxing_Strobe;
    //p->super.Read4016 = &mm_emu_pad_excitingboxing_Read4016;
    p->super.Read4017 = &mmEmuPadExcitingBoxing_Read4017;
    p->super.Write4016 = &mmEmuPadExcitingBoxing_Write4016;
    //p->super.Write4017 = &mm_emu_pad_excitingboxing_Write4017;
    p->super.Sync = &mmEmuPadExcitingBoxing_Sync;
    p->super.SetSyncData = &mmEmuPadExcitingBoxing_SetSyncData;
    p->super.GetSyncData = &mmEmuPadExcitingBoxing_GetSyncData;
}
MM_EXPORT_EMU void mmEmuPadExcitingBoxing_Destroy(struct mmEmuPadExcitingBoxing* p)
{
    mmEmuExpad_Destroy(&p->super);

    p->outbits = 0;
    p->padbits = 0;
}

MM_EXPORT_EMU void mmEmuPadExcitingBoxing_Reset(struct mmEmuExpad* super)
{
    struct mmEmuPadExcitingBoxing* p = (struct mmEmuPadExcitingBoxing*)(super);

    p->outbits = 0;
    p->padbits = 0;
}

MM_EXPORT_EMU mmByte_t mmEmuPadExcitingBoxing_Read4017(struct mmEmuExpad* super)
{
    struct mmEmuPadExcitingBoxing* p = (struct mmEmuPadExcitingBoxing*)(super);

    return p->outbits;
}
MM_EXPORT_EMU void mmEmuPadExcitingBoxing_Write4016(struct mmEmuExpad* super, mmByte_t data)
{
    struct mmEmuPadExcitingBoxing* p = (struct mmEmuPadExcitingBoxing*)(super);

    p->outbits = 0;
    if (data & 0x02)
    {
        p->outbits = (~p->padbits << 1) & 0x1E;
    }
    else
    {
        p->outbits = (~p->padbits >> 3) & 0x1E;
    }
}

MM_EXPORT_EMU void mmEmuPadExcitingBoxing_Sync(struct mmEmuExpad* super)
{
    struct mmEmuPadExcitingBoxing* p = (struct mmEmuPadExcitingBoxing*)(super);

    struct mmEmuController* controller = &p->super.nes->controller;

    p->padbits = 0;

    // Straight
    if (mmEmuController_ExButtonCheck(controller, 2, 0)) p->padbits |= 1 << 3;
    // Right Jabb
    if (mmEmuController_ExButtonCheck(controller, 2, 1)) p->padbits |= 1 << 2;
    // Body
    if (mmEmuController_ExButtonCheck(controller, 2, 2)) p->padbits |= 1 << 1;
    // Left Jabb
    if (mmEmuController_ExButtonCheck(controller, 2, 3)) p->padbits |= 1 << 0;
    // Right hook
    if (mmEmuController_ExButtonCheck(controller, 2, 4)) p->padbits |= 1 << 7;
    // Left hook
    if (mmEmuController_ExButtonCheck(controller, 2, 5)) p->padbits |= 1 << 4;
    // Right move
    if (mmEmuController_ExButtonCheck(controller, 2, 6)) p->padbits |= 1 << 5;
    // Left move
    if (mmEmuController_ExButtonCheck(controller, 2, 7)) p->padbits |= 1 << 6;
}
MM_EXPORT_EMU void mmEmuPadExcitingBoxing_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data)
{
    struct mmEmuPadExcitingBoxing* p = (struct mmEmuPadExcitingBoxing*)(super);

    p->padbits = (mmByte_t)data;
}
MM_EXPORT_EMU mmLong_t mmEmuPadExcitingBoxing_GetSyncData(const struct mmEmuExpad* super, mmInt_t type)
{
    struct mmEmuPadExcitingBoxing* p = (struct mmEmuPadExcitingBoxing*)(super);

    return (mmLong_t)p->padbits;
}

static struct mmEmuExpad* __static_mmEmuPadExcitingBoxing_Produce(void)
{
    struct mmEmuPadExcitingBoxing* p = (struct mmEmuPadExcitingBoxing*)mmMalloc(sizeof(struct mmEmuPadExcitingBoxing));
    mmEmuPadExcitingBoxing_Init(p);
    return (struct mmEmuExpad*)p;
}
static void __static_mmEmuPadExcitingBoxing_Recycle(struct mmEmuExpad* m)
{
    struct mmEmuPadExcitingBoxing* p = (struct mmEmuPadExcitingBoxing*)(m);
    mmEmuPadExcitingBoxing_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_EXCITINGBOXING =
{
    &__static_mmEmuPadExcitingBoxing_Produce,
    &__static_mmEmuPadExcitingBoxing_Recycle,
};
