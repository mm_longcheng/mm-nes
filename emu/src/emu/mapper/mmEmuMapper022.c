/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper022.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper022_Init(struct mmEmuMapper022* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper022_Reset;
    p->super.Write = &mmEmuMapper022_Write;
    //p->super.Read = &mmEmuMapper022_Read;
    //p->super.ReadLow = &mmEmuMapper022_ReadLow;
    //p->super.WriteLow = &mmEmuMapper022_WriteLow;
    //p->super.ExRead = &mmEmuMapper022_ExRead;
    //p->super.ExWrite = &mmEmuMapper022_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper022_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper022_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper022_HSync;
    //p->super.VSync = &mmEmuMapper022_VSync;
    //p->super.Clock = &mmEmuMapper022_Clock;
    //p->super.PPULatch = &mmEmuMapper022_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper022_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper022_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper022_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper022_IsStateSave;
    //p->super.SaveState = &mmEmuMapper022_SaveState;
    //p->super.LoadState = &mmEmuMapper022_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper022_Destroy(struct mmEmuMapper022* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper022_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper022* p = (struct mmEmuMapper022*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
MM_EXPORT_EMU void mmEmuMapper022_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper022* p = (struct mmEmuMapper022*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr)
    {
    case 0x8000:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;

    case 0x9000:
        data &= 0x03;
        if (data == 0)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 1)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 2)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        break;

    case 0xA000:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;

    case 0xB000:
        mmEmuMmu_SetVROM1KBank(mmu, 0, data >> 1);
        break;

    case 0xB001:
        mmEmuMmu_SetVROM1KBank(mmu, 1, data >> 1);
        break;

    case 0xC000:
        mmEmuMmu_SetVROM1KBank(mmu, 2, data >> 1);
        break;

    case 0xC001:
        mmEmuMmu_SetVROM1KBank(mmu, 3, data >> 1);
        break;

    case 0xD000:
        mmEmuMmu_SetVROM1KBank(mmu, 4, data >> 1);
        break;

    case 0xD001:
        mmEmuMmu_SetVROM1KBank(mmu, 5, data >> 1);
        break;

    case 0xE000:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data >> 1);
        break;

    case 0xE001:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data >> 1);
        break;
    default:
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper022_Produce(void)
{
    struct mmEmuMapper022* p = (struct mmEmuMapper022*)mmMalloc(sizeof(struct mmEmuMapper022));
    mmEmuMapper022_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper022_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper022* p = (struct mmEmuMapper022*)(m);
    mmEmuMapper022_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER022 =
{
    &__static_mmEmuMapper022_Produce,
    &__static_mmEmuMapper022_Recycle,
};
