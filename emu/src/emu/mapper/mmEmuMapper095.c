/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper095.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper095_Init(struct mmEmuMapper095* p)
{
    mmEmuMapper_Init(&p->super);

    p->reg = 0x00;
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;

    //
    p->super.Reset = &mmEmuMapper095_Reset;
    p->super.Write = &mmEmuMapper095_Write;
    //p->super.Read = &mmEmuMapper095_Read;
    //p->super.ReadLow = &mmEmuMapper095_ReadLow;
    //p->super.WriteLow = &mmEmuMapper095_WriteLow;
    //p->super.ExRead = &mmEmuMapper095_ExRead;
    //p->super.ExWrite = &mmEmuMapper095_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper095_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper095_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper095_HSync;
    //p->super.VSync = &mmEmuMapper095_VSync;
    //p->super.Clock = &mmEmuMapper095_Clock;
    //p->super.PPULatch = &mmEmuMapper095_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper095_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper095_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper095_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper095_IsStateSave;
    p->super.SaveState = &mmEmuMapper095_SaveState;
    p->super.LoadState = &mmEmuMapper095_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper095_Destroy(struct mmEmuMapper095* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->reg = 0x00;
    p->prg0 = 0;
    p->prg1 = 1;

    p->chr01 = 0;
    p->chr23 = 0;
    p->chr4 = 0;
    p->chr5 = 0;
    p->chr6 = 0;
    p->chr7 = 0;
}

MM_EXPORT_EMU void mmEmuMapper095_SetBankCPU(struct mmEmuMapper095* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, p->prg1, p->prg0, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper095_SetBankPPU(struct mmEmuMapper095* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg & 0x80)
        {
            mmEmuMmu_SetVROM8KBankArray(mmu, p->chr4, p->chr5, p->chr6, p->chr7, p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1);
        }
        else
        {
            mmEmuMmu_SetVROM8KBankArray(mmu, p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1, p->chr4, p->chr5, p->chr6, p->chr7);
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper095_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper095* p = (struct mmEmuMapper095*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->reg = 0x00;
    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper095_SetBankCPU(p);

    if (mmu->VROM_1K_SIZE)
    {
        p->chr01 = 0;
        p->chr23 = 2;
        p->chr4 = 4;
        p->chr5 = 5;
        p->chr6 = 6;
        p->chr7 = 7;
    }
    else
    {
        p->chr01 = p->chr23 = p->chr4 = p->chr5 = p->chr6 = p->chr7 = 0;
    }

    mmEmuMapper095_SetBankPPU(p);

    mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
}
MM_EXPORT_EMU void mmEmuMapper095_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper095* p = (struct mmEmuMapper095*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg = data;
        mmEmuMapper095_SetBankCPU(p);
        mmEmuMapper095_SetBankPPU(p);
        break;
    case 0x8001:
        if (p->reg <= 0x05)
        {
            if (data & 0x20)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
            }
            data &= 0x1F;
        }

        switch (p->reg & 0x07)
        {
        case 0x00:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr01 = data & 0xFE;
                mmEmuMapper095_SetBankPPU(p);
            }
            break;
        case 0x01:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr23 = data & 0xFE;
                mmEmuMapper095_SetBankPPU(p);
            }
            break;
        case 0x02:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr4 = data;
                mmEmuMapper095_SetBankPPU(p);
            }
            break;
        case 0x03:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr5 = data;
                mmEmuMapper095_SetBankPPU(p);
            }
            break;
        case 0x04:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr6 = data;
                mmEmuMapper095_SetBankPPU(p);
            }
            break;
        case 0x05:
            if (mmu->VROM_1K_SIZE)
            {
                p->chr7 = data;
                mmEmuMapper095_SetBankPPU(p);
            }
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper095_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper095_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper095_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper095_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper095* p = (struct mmEmuMapper095*)(super);

    buffer[0] = p->reg;
    buffer[1] = p->prg0;
    buffer[2] = p->prg1;
    buffer[3] = p->chr01;
    buffer[4] = p->chr23;
    buffer[5] = p->chr4;
    buffer[6] = p->chr5;
    buffer[7] = p->chr6;
    buffer[8] = p->chr7;
}
MM_EXPORT_EMU void mmEmuMapper095_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper095* p = (struct mmEmuMapper095*)(super);

    p->reg = buffer[0];
    p->prg0 = buffer[1];
    p->prg1 = buffer[2];
    p->chr01 = buffer[3];
    p->chr23 = buffer[4];
    p->chr4 = buffer[5];
    p->chr5 = buffer[6];
    p->chr6 = buffer[7];
    p->chr7 = buffer[8];
}

static struct mmEmuMapper* __static_mmEmuMapper095_Produce(void)
{
    struct mmEmuMapper095* p = (struct mmEmuMapper095*)mmMalloc(sizeof(struct mmEmuMapper095));
    mmEmuMapper095_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper095_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper095* p = (struct mmEmuMapper095*)(m);
    mmEmuMapper095_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER095 =
{
    &__static_mmEmuMapper095_Produce,
    &__static_mmEmuMapper095_Recycle,
};
