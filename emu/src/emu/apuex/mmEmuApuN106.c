/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApuN106.h"

#include "emu/mmEmuState.h"

#include "core/mmString.h"

#define CHANNEL_VOL_SHIFT   6

MM_EXPORT_EMU void mmEmuApuN106_Init(struct mmEmuApuN106* p)
{
    mmEmuApuInterface_Init(&p->super);

    mmMemset(p->op, 0, sizeof(struct mmEmuApuN106Channel) * 8);

    p->cpu_clock = 0;
    p->cycle_rate = 0;

    p->addrinc = 0;
    p->address = 0;
    p->channel_use = 0;

    mmMemset(p->tone, 0, sizeof(mmByte_t) * 0x100);

    // Provisional setting.
    p->cpu_clock = MM_EMU_APU_CLOCK;
    p->cycle_rate = (mmUInt32_t)(p->cpu_clock*12.0f * (1 << 20) / (45.0f * 22050.0f));

    //
    p->super.Reset = &mmEmuApuN106_Reset;
    p->super.Setup = &mmEmuApuN106_Setup;
    p->super.Process = &mmEmuApuN106_Process;
    p->super.Write = &mmEmuApuN106_Write;
    p->super.Read = &mmEmuApuN106_Read;
    //p->super.SyncWrite = &mm_emu_apu_n106_SyncWrite;
    //p->super.SyncRead = &mm_emu_apu_n106_SyncRead;
    //p->super.Sync = &mm_emu_apu_n106_Sync;
    p->super.GetFreq = &mmEmuApuN106_GetFreq;
    p->super.GetStateSize = &mmEmuApuN106_GetStateSize;
    p->super.SaveState = &mmEmuApuN106_SaveState;
    p->super.LoadState = &mmEmuApuN106_LoadState;
}
MM_EXPORT_EMU void mmEmuApuN106_Destroy(struct mmEmuApuN106* p)
{
    mmEmuApuInterface_Destroy(&p->super);

    mmMemset(p->op, 0, sizeof(struct mmEmuApuN106Channel) * 8);

    p->cpu_clock = 0;
    p->cycle_rate = 0;

    p->addrinc = 0;
    p->address = 0;
    p->channel_use = 0;

    mmMemset(p->tone, 0, sizeof(mmByte_t) * 0x100);
}

MM_EXPORT_EMU mmInt_t mmEmuApuN106_ChannelRender(struct mmEmuApuN106* p, struct mmEmuApuN106Channel* ch)
{
    mmUInt32_t phasespd = p->channel_use << 20;

    ch->phaseacc -= p->cycle_rate;
    if (ch->phaseacc >= 0)
    {
        if (ch->volupdate)
        {
            ch->output = ((mmInt_t)p->tone[((ch->phase >> 18) + ch->toneadr) & 0xFF] * ch->vol) << CHANNEL_VOL_SHIFT;
            ch->volupdate = 0;
        }
        return ch->output;
    }

    while (ch->phaseacc < 0)
    {
        ch->phaseacc += phasespd;
        ch->phase += ch->freq;
    }
    while (ch->tonelen && (ch->phase >= ch->tonelen))
    {
        ch->phase -= ch->tonelen;
    }

    ch->output = ((mmInt_t)p->tone[((ch->phase >> 18) + ch->toneadr) & 0xFF] * ch->vol) << CHANNEL_VOL_SHIFT;

    return ch->output;
}

MM_EXPORT_EMU void mmEmuApuN106_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    mmInt_t i = 0;

    mmMemset(p->op, 0, sizeof(struct mmEmuApuN106Channel) * 8);
    for (i = 0; i < 8; i++)
    {
        p->op[i].tonelen = 0x10 << 18;
    }

    p->address = 0;
    p->addrinc = 1;
    p->channel_use = 8;

    (*(p->super.Setup))(&p->super, fClock, nRate);

    // I do not initialize TONE ...
}
MM_EXPORT_EMU void mmEmuApuN106_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    p->cpu_clock = fClock;
    p->cycle_rate = (mmUInt32_t)(p->cpu_clock * 12.0f * (1 << 20) / (45.0f * nRate));
}
MM_EXPORT_EMU void mmEmuApuN106_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    if (addr == 0x4800)
    {
        // tone[p->address*2+0] = (mmInt_t)(data&0x0F);
        // tone[p->address*2+1] = (mmInt_t)(data  >>4);
        p->tone[p->address * 2 + 0] = data & 0x0F;
        p->tone[p->address * 2 + 1] = data >> 4;

        if (p->address >= 0x40)
        {
            mmInt_t no = (p->address - 0x40) >> 3;
            mmUInt32_t  tonelen;
            struct mmEmuApuN106Channel* ch = &(p->op[no]);

            switch (p->address & 7)
            {
            case 0x00:
                ch->freq = (ch->freq&~0x000000FF) | (mmUInt32_t)data;
                break;
            case 0x02:
                ch->freq = (ch->freq&~0x0000FF00) | ((mmUInt32_t)data << 8);
                break;
            case 0x04:
                ch->freq = (ch->freq&~0x00030000) | (((mmUInt32_t)data & 0x03) << 16);
                tonelen = (0x20 - (data & 0x1c)) << 18;
                ch->databuf = (data & 0x1c) >> 2;
                if (ch->tonelen != tonelen)
                {
                    ch->tonelen = tonelen;
                    ch->phase = 0;
                }
                break;
            case 0x06:
                ch->toneadr = data;
                break;
            case 0x07:
                ch->vol = data & 0x0f;
                ch->volupdate = 0xFF;
                if (no == 7)
                {
                    p->channel_use = ((data >> 4) & 0x07) + 1;
                }
                break;
            default:
                break;
            }
        }

        if (p->addrinc)
        {
            p->address = (p->address + 1) & 0x7f;
        }
    }
    else if (addr == 0xF800)
    {
        p->address = data & 0x7F;
        p->addrinc = data & 0x80;
    }
}
MM_EXPORT_EMU mmByte_t mmEmuApuN106_Read(struct mmEmuApuInterface* super, mmWord_t addr)
{
    struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    // $4800 dummy read!!
    if (addr == 0x0000)
    {
        if (p->addrinc)
        {
            p->address = (p->address + 1) & 0x7F;
        }
    }

    return (mmByte_t)(addr >> 8);
}
MM_EXPORT_EMU mmInt_t mmEmuApuN106_Process(struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    if (channel >= (8 - p->channel_use) && channel < 8)
    {
        return mmEmuApuN106_ChannelRender(p, &(p->op[channel]));
    }

    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuN106_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    if (channel < 8)
    {
        mmInt_t temp = 0;

        channel &= 7;
        if (channel < (8 - p->channel_use))
        {
            return 0;
        }

        struct mmEmuApuN106Channel* ch = &(p->op[channel & 0x07]);
        if (!ch->freq || !ch->vol)
        {
            return 0;
        }
        temp = p->channel_use * (8 - ch->databuf) * 4 * 45;
        if (!temp)
        {
            return 0;
        }
        return (mmInt_t)(256.0 * (double)p->cpu_clock * 12.0 * ch->freq / ((double)0x40000 * temp));
    }

    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuN106_GetStateSize(const struct mmEmuApuInterface* super)
{
    // struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    return
        sizeof(mmByte_t) * 3 +
        sizeof(struct mmEmuApuN106Channel) * 8 +
        sizeof(mmByte_t) * 0x100;
}
MM_EXPORT_EMU void mmEmuApuN106_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    MM_EMU_SETBYTE(buffer, p->addrinc);
    MM_EMU_SETBYTE(buffer, p->address);
    MM_EMU_SETBYTE(buffer, p->channel_use);

    MM_EMU_SETBLOCK(buffer, p->op, sizeof(struct mmEmuApuN106Channel) * 8);
    MM_EMU_SETBLOCK(buffer, p->tone, sizeof(mmByte_t) * 0x100);
}
MM_EXPORT_EMU void mmEmuApuN106_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuN106* p = (struct mmEmuApuN106*)(super);

    MM_EMU_GETBYTE(buffer, p->addrinc);
    MM_EMU_GETBYTE(buffer, p->address);
    MM_EMU_GETBYTE(buffer, p->channel_use);

    MM_EMU_GETBLOCK(buffer, p->op, sizeof(struct mmEmuApuN106Channel) * 8);
    MM_EMU_GETBLOCK(buffer, p->tone, sizeof(mmByte_t) * 0x100);
}
