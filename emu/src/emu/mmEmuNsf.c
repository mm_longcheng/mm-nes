/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuNsf.h"
#include "mmEmuNes.h"

MM_EXPORT_EMU void mmEmuNsf_Init(struct mmEmuNsf* p)
{
    p->nes = NULL;

    p->m_bNsfPlaying = MM_FALSE;
    p->m_bNsfInit = MM_FALSE;
    p->m_nNsfSongNo = 0;
    p->m_nNsfSongMode = 0;
}
MM_EXPORT_EMU void mmEmuNsf_Destroy(struct mmEmuNsf* p)
{
    p->nes = NULL;

    p->m_bNsfPlaying = MM_FALSE;
    p->m_bNsfInit = MM_FALSE;
    p->m_nNsfSongNo = 0;
    p->m_nNsfSongMode = 0;
}

MM_EXPORT_EMU void mmEmuNsf_SetParent(struct mmEmuNsf* p, struct mmEmuNes* parent)
{
    p->nes = parent;
}

MM_EXPORT_EMU void mmEmuNsf_EmulateNSF(struct mmEmuNsf* p)
{
    struct mmEmuNes* nes = p->nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    struct mmEmuVideoFormatConfig* nescfg = &nes->config.nescfg;

    mmInt_t i = 0;

    struct mmEmuX6502State  reg;

    mmEmuPpu_Reset(&nes->ppu);
    (*(nes->mapper->VSync))(nes->mapper);

    // DEBUGOUT( "Frame\n" );

    if (p->m_bNsfPlaying)
    {
        if (p->m_bNsfInit)
        {
            mmMemset(mmu->RAM, 0, sizeof(mmByte_t) * 8 * 1024);
            if (!(mmEmuRom_GetNsfHeader(&nes->rom)->ExtraChipSelect & 0x04))
            {
                mmMemset(mmu->WRAM, 0, 0x2000);
            }

            mmEmuApu_Reset(&nes->apu);
            mmEmuApu_Write(&nes->apu, 0x4015, 0x0F);
            mmEmuApu_Write(&nes->apu, 0x4017, 0xC0);
            mmEmuApu_ExWrite(&nes->apu, 0x4080, 0x80);// FDS Volume 0
            mmEmuApu_ExWrite(&nes->apu, 0x408A, 0xE8);// FDS Envelope Speed

            mmEmuCpu_GetContext(&nes->cpu, &reg);
            reg.PC = 0x4710;    // Init Address
            reg.A = (mmByte_t)p->m_nNsfSongNo;
            reg.X = (mmByte_t)p->m_nNsfSongMode;
            reg.Y = 0;
            reg.S = 0xFF;
            reg.P = MM_EMU_CPU_Z_FLAG | MM_EMU_CPU_R_FLAG | MM_EMU_CPU_I_FLAG;
            mmEmuCpu_SetContext(&nes->cpu, &reg);

            // Dare to combine safety measures (1 second worth).
            for (i = 0; i < nescfg->TotalScanlines * 60; i++)
            {
                mmEmuNes_EmulationCPU(nes, nescfg->ScanlineCycles);
                mmEmuCpu_GetContext(&nes->cpu, &reg);

                // After confirming that you entered an infinite loop, exit.
                if (reg.PC == 0x4700)
                {
                    break;
                }
            }

            p->m_bNsfInit = MM_FALSE;
        }

        mmEmuCpu_GetContext(&nes->cpu, &reg);
        // If you are in an infinite loop, reset it.
        if (reg.PC == 0x4700)
        {
            reg.PC = 0x4720;    // Play Address
            reg.A = 0;
            reg.S = 0xFF;
            mmEmuCpu_SetContext(&nes->cpu, &reg);
        }

        for (i = 0; i < nescfg->TotalScanlines; i++)
        {
            mmEmuNes_EmulationCPU(nes, nescfg->ScanlineCycles);
        }
    }
    else
    {
        mmEmuCpu_GetContext(&nes->cpu, &reg);
        reg.PC = 0x4700;    // infinite loop.
        reg.S = 0xFF;
        mmEmuCpu_SetContext(&nes->cpu, &reg);

        mmEmuNes_EmulationCPU(nes, nescfg->ScanlineCycles * nescfg->TotalScanlines);
    }
}
MM_EXPORT_EMU void mmEmuNsf_SetNsfPlay(struct mmEmuNsf* p, mmInt_t songno, mmInt_t songmode)
{
    p->m_bNsfPlaying = MM_TRUE;
    p->m_bNsfInit = MM_TRUE;
    p->m_nNsfSongNo = songno;
    p->m_nNsfSongMode = songmode;
}
MM_EXPORT_EMU void mmEmuNsf_SetNsfStop(struct mmEmuNsf* p)
{
    struct mmEmuNes* nes = p->nes;

    p->m_bNsfPlaying = MM_FALSE;
    mmEmuApu_Reset(&nes->apu);
}

