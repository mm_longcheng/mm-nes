/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper062.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper062_Init(struct mmEmuMapper062* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper062_Reset;
    p->super.Write = &mmEmuMapper062_Write;
    //p->super.Read = &mmEmuMapper062_Read;
    //p->super.ReadLow = &mmEmuMapper062_ReadLow;
    //p->super.WriteLow = &mmEmuMapper062_WriteLow;
    //p->super.ExRead = &mmEmuMapper062_ExRead;
    //p->super.ExWrite = &mmEmuMapper062_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper062_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper062_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper062_HSync;
    //p->super.VSync = &mmEmuMapper062_VSync;
    //p->super.Clock = &mmEmuMapper062_Clock;
    //p->super.PPULatch = &mmEmuMapper062_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper062_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper062_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper062_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper062_IsStateSave;
    //p->super.SaveState = &mmEmuMapper062_SaveState;
    //p->super.LoadState = &mmEmuMapper062_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper062_Destroy(struct mmEmuMapper062* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper062_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper062* p = (struct mmEmuMapper062*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, 0);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper062_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper062* p = (struct mmEmuMapper062*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xFF00)
    {
    case 0x8100:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        mmEmuMmu_SetPROM8KBank(mmu, 5, data + 1);
        break;
    case 0x8500:
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        break;
    case 0x8700:
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        break;
    default:
        mmEmuMmu_SetVROM1KBank(mmu, 0, data + 0);
        mmEmuMmu_SetVROM1KBank(mmu, 1, data + 1);
        mmEmuMmu_SetVROM1KBank(mmu, 2, data + 2);
        mmEmuMmu_SetVROM1KBank(mmu, 3, data + 3);
        mmEmuMmu_SetVROM1KBank(mmu, 4, data + 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, data + 5);
        mmEmuMmu_SetVROM1KBank(mmu, 6, data + 6);
        mmEmuMmu_SetVROM1KBank(mmu, 7, data + 7);
        break;
    }
}

static struct mmEmuMapper* __static_mmEmuMapper062_Produce(void)
{
    struct mmEmuMapper062* p = (struct mmEmuMapper062*)mmMalloc(sizeof(struct mmEmuMapper062));
    mmEmuMapper062_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper062_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper062* p = (struct mmEmuMapper062*)(m);
    mmEmuMapper062_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER062 =
{
    &__static_mmEmuMapper062_Produce,
    &__static_mmEmuMapper062_Recycle,
};
