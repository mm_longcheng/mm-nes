/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper068.h"
#include "emu/mmEmuNes.h"

#include "core/mmLogger.h"

MM_EXPORT_EMU void mmEmuMapper068_Init(struct mmEmuMapper068* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);
    p->coin = 0;

    //
    p->super.Reset = &mmEmuMapper068_Reset;
    p->super.Write = &mmEmuMapper068_Write;
    //p->super.Read = &mmEmuMapper068_Read;
    //p->super.ReadLow = &mmEmuMapper068_ReadLow;
    //p->super.WriteLow = &mmEmuMapper068_WriteLow;
#ifdef MM_EMU_MAPPER068_DEBUG
    p->super.ExRead = &mmEmuMapper068_ExRead;
    p->super.ExWrite = &mmEmuMapper068_ExWrite;
#else
    //p->super.ExRead = &mmEmuMapper068_ExRead;
    //p->super.ExWrite = &mmEmuMapper068_ExWrite;
#endif//MM_EMU_MAPPER068_DEBUG
    //p->super.ExCmdRead = &mmEmuMapper068_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper068_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper068_HSync;
    //p->super.VSync = &mmEmuMapper068_VSync;
    //p->super.Clock = &mmEmuMapper068_Clock;
    //p->super.PPULatch = &mmEmuMapper068_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper068_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper068_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper068_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper068_IsStateSave;
    p->super.SaveState = &mmEmuMapper068_SaveState;
    p->super.LoadState = &mmEmuMapper068_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper068_Destroy(struct mmEmuMapper068* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 4);
    p->coin = 0;
}

MM_EXPORT_EMU void mmEmuMapper068_SetBank(struct mmEmuMapper068* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0])
    {
        switch (p->reg[1])
        {
        case 0:
            mmEmuMmu_SetVROM1KBank(mmu, 8, (mmInt_t)p->reg[2] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 9, (mmInt_t)p->reg[3] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 10, (mmInt_t)p->reg[2] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 11, (mmInt_t)p->reg[3] + 0x80);
            break;
        case 1:
            mmEmuMmu_SetVROM1KBank(mmu, 8, (mmInt_t)p->reg[2] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 9, (mmInt_t)p->reg[2] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 10, (mmInt_t)p->reg[3] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 11, (mmInt_t)p->reg[3] + 0x80);
            break;
        case 2:
            mmEmuMmu_SetVROM1KBank(mmu, 8, (mmInt_t)p->reg[2] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 9, (mmInt_t)p->reg[2] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 10, (mmInt_t)p->reg[2] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 11, (mmInt_t)p->reg[2] + 0x80);
            break;
        case 3:
            mmEmuMmu_SetVROM1KBank(mmu, 8, (mmInt_t)p->reg[3] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 9, (mmInt_t)p->reg[3] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 10, (mmInt_t)p->reg[3] + 0x80);
            mmEmuMmu_SetVROM1KBank(mmu, 11, (mmInt_t)p->reg[3] + 0x80);
            break;
        default:
            break;
        }
    }
    else
    {
        switch (p->reg[1])
        {
        case 0:
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            break;
        case 1:
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            break;
        case 2:
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
            break;
        case 3:
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
            break;
        default:
            break;
        }
    }
}

MM_EXPORT_EMU void mmEmuMapper068_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper068* p = (struct mmEmuMapper068*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->reg[0] = p->reg[1] = p->reg[2] = p->reg[3] = 0;
    p->coin = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
}
#ifdef MM_EMU_MAPPER068_DEBUG
MM_EXPORT_EMU BYTE mmEmuMapper068_ExRead(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper068* p = (struct mmEmuMapper068*)(super);

    if (addr == 0x4020)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmLogger_LogD(gLogger, "%s %d RD $4020:%02X", __FUNCTION__, __LINE__, p->coin);
        return p->coin;
    }

    return addr >> 8;
}
MM_EXPORT_EMU void mmEmuMapper068_ExWrite(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper068* p = (struct mmEmuMapper068*)(super);

    if (addr == 0x4020)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmLogger_LogD(gLogger, "%s %d WR $4020:%02X", __FUNCTION__, __LINE__, data);
        p->coin = data;
    }
}
#endif//MM_EMU_MAPPER068_DEBUG
MM_EXPORT_EMU void mmEmuMapper068_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper068* p = (struct mmEmuMapper068*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF000)
    {
    case 0x8000:
        mmEmuMmu_SetVROM2KBank(mmu, 0, data);
        break;
    case 0x9000:
        mmEmuMmu_SetVROM2KBank(mmu, 2, data);
        break;
    case 0xA000:
        mmEmuMmu_SetVROM2KBank(mmu, 4, data);
        break;
    case 0xB000:
        mmEmuMmu_SetVROM2KBank(mmu, 6, data);
        break;

    case 0xC000:
        p->reg[2] = data;
        mmEmuMapper068_SetBank(p);
        break;
    case 0xD000:
        p->reg[3] = data;
        mmEmuMapper068_SetBank(p);
        break;
    case 0xE000:
        p->reg[0] = (data & 0x10) >> 4;
        p->reg[1] = data & 0x03;
        mmEmuMapper068_SetBank(p);
        break;

    case 0xF000:
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        break;
    default:
        break;
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper068_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper068_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper068* p = (struct mmEmuMapper068*)(super);

    buffer[0] = p->reg[0];
    buffer[1] = p->reg[1];
    buffer[2] = p->reg[2];
    buffer[3] = p->reg[3];
}
MM_EXPORT_EMU void mmEmuMapper068_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper068* p = (struct mmEmuMapper068*)(super);

    p->reg[0] = buffer[0];
    p->reg[1] = buffer[1];
    p->reg[2] = buffer[2];
    p->reg[3] = buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper068_Produce(void)
{
    struct mmEmuMapper068* p = (struct mmEmuMapper068*)mmMalloc(sizeof(struct mmEmuMapper068));
    mmEmuMapper068_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper068_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper068* p = (struct mmEmuMapper068*)(m);
    mmEmuMapper068_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER068 =
{
    &__static_mmEmuMapper068_Produce,
    &__static_mmEmuMapper068_Recycle,
};
