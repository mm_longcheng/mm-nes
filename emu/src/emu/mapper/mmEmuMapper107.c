/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper107.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper107_Init(struct mmEmuMapper107* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper107_Reset;
    p->super.Write = &mmEmuMapper107_Write;
    //p->super.Read = &mmEmuMapper107_Read;
    //p->super.ReadLow = &mmEmuMapper107_ReadLow;
    //p->super.WriteLow = &mmEmuMapper107_WriteLow;
    //p->super.ExRead = &mmEmuMapper107_ExRead;
    //p->super.ExWrite = &mmEmuMapper107_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper107_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper107_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper107_HSync;
    //p->super.VSync = &mmEmuMapper107_VSync;
    //p->super.Clock = &mmEmuMapper107_Clock;
    //p->super.PPULatch = &mmEmuMapper107_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper107_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper107_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper107_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper107_IsStateSave;
    //p->super.SaveState = &mmEmuMapper107_SaveState;
    //p->super.LoadState = &mmEmuMapper107_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper107_Destroy(struct mmEmuMapper107* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper107_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper107* p = (struct mmEmuMapper107*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper107_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper107* p = (struct mmEmuMapper107*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBank(mmu, (data >> 1) & 0x03);
    mmEmuMmu_SetVROM8KBank(mmu, data & 0x07);
}

static struct mmEmuMapper* __static_mmEmuMapper107_Produce(void)
{
    struct mmEmuMapper107* p = (struct mmEmuMapper107*)mmMalloc(sizeof(struct mmEmuMapper107));
    mmEmuMapper107_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper107_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper107* p = (struct mmEmuMapper107*)(m);
    mmEmuMapper107_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER107 =
{
    &__static_mmEmuMapper107_Produce,
    &__static_mmEmuMapper107_Recycle,
};
