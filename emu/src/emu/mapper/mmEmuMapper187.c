/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper187.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper187_Init(struct mmEmuMapper187* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->prg, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->chr, 0, sizeof(mmInt_t) * 8);
    mmMemset(p->bank, 0, sizeof(mmByte_t) * 8);

    p->ext_mode = 0;
    p->chr_mode = 0;
    p->ext_enable = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->last_write = 0;

    //
    p->super.Reset = &mmEmuMapper187_Reset;
    p->super.Write = &mmEmuMapper187_Write;
    //p->super.Read = &mmEmuMapper187_Read;
    p->super.ReadLow = &mmEmuMapper187_ReadLow;
    p->super.WriteLow = &mmEmuMapper187_WriteLow;
    //p->super.ExRead = &mmEmuMapper187_ExRead;
    //p->super.ExWrite = &mmEmuMapper187_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper187_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper187_ExCmdWrite;
    p->super.HSync = &mmEmuMapper187_HSync;
    //p->super.VSync = &mmEmuMapper187_VSync;
    p->super.Clock = &mmEmuMapper187_Clock;
    //p->super.PPULatch = &mmEmuMapper187_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper187_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper187_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper187_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper187_IsStateSave;
    p->super.SaveState = &mmEmuMapper187_SaveState;
    p->super.LoadState = &mmEmuMapper187_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper187_Destroy(struct mmEmuMapper187* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->prg, 0, sizeof(mmByte_t) * 4);
    mmMemset(p->chr, 0, sizeof(mmInt_t) * 8);
    mmMemset(p->bank, 0, sizeof(mmByte_t) * 8);

    p->ext_mode = 0;
    p->chr_mode = 0;
    p->ext_enable = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->last_write = 0;
}

MM_EXPORT_EMU void mmEmuMapper187_SetBankCPU(struct mmEmuMapper187* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, p->prg[0], p->prg[1], p->prg[2], p->prg[3]);
}
MM_EXPORT_EMU void mmEmuMapper187_SetBankPPU(struct mmEmuMapper187* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetVROM8KBankArray(
        mmu,
        p->chr[0], p->chr[1], p->chr[2], p->chr[3],
        p->chr[4], p->chr[5], p->chr[6], p->chr[7]);
}

MM_EXPORT_EMU void mmEmuMapper187_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmMemset(p->chr, 0x00, sizeof(mmInt_t) * 8);
    mmMemset(p->bank, 0x00, sizeof(mmByte_t) * 8);

    p->prg[0] = mmu->PROM_08K_SIZE - 4;
    p->prg[1] = mmu->PROM_08K_SIZE - 3;
    p->prg[2] = mmu->PROM_08K_SIZE - 2;
    p->prg[3] = mmu->PROM_08K_SIZE - 1;
    mmEmuMapper187_SetBankCPU(p);

    p->ext_mode = 0;
    p->chr_mode = 0;
    p->ext_enable = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;

    p->last_write = 0;

    mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
}
MM_EXPORT_EMU mmByte_t mmEmuMapper187_ReadLow(struct mmEmuMapper* super, mmWord_t addr)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)(super);

    switch (p->last_write & 0x03)
    {
    case 0:
        return 0x83;
    case 1:
        return 0x83;
    case 2:
        return 0x42;
    case 3:
        return 0x00;
    default:
        break;
    }
    return 0;
}
MM_EXPORT_EMU void mmEmuMapper187_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->last_write = data;
    if (addr == 0x5000)
    {
        p->ext_mode = data;
        if (data & 0x80)
        {
            if (data & 0x20)
            {
                p->prg[0] = ((data & 0x1E) << 1) + 0;
                p->prg[1] = ((data & 0x1E) << 1) + 1;
                p->prg[2] = ((data & 0x1E) << 1) + 2;
                p->prg[3] = ((data & 0x1E) << 1) + 3;
            }
            else
            {
                p->prg[2] = ((data & 0x1F) << 1) + 0;
                p->prg[3] = ((data & 0x1F) << 1) + 1;
            }
        }
        else
        {
            p->prg[0] = p->bank[6];
            p->prg[1] = p->bank[7];
            p->prg[2] = mmu->PROM_08K_SIZE - 2;
            p->prg[3] = mmu->PROM_08K_SIZE - 1;
        }
        mmEmuMapper187_SetBankCPU(p);
    }
}
MM_EXPORT_EMU void mmEmuMapper187_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    p->last_write = data;
    switch (addr)
    {
    case 0x8003:
        p->ext_enable = 0xFF;

        //if ((data & 0x80) != (p->chr_mode & 0x80))
        //{
        //  mmInt_t i = 0;
        //  for (i = 0; i < 4; i++) 
        //  {
        //      mmInt_t temp = p->chr[i];
        //      p->chr[i] = p->chr[i + 4];
        //      p->chr[i + 4] = temp;
        //  }
        //  mmEmuMapper187_SetBankPPU(p);
        //}

        p->chr_mode = data;
        if ((data & 0xF0) == 0)
        {
            p->prg[2] = mmu->PROM_08K_SIZE - 2;
            mmEmuMapper187_SetBankCPU(p);
        }
        break;

    case 0x8000:
        p->ext_enable = 0;

        //if ((data & 0x80) != (p->chr_mode & 0x80))
        //{
        //  mmInt_t i = 0;
        //  for (i = 0; i < 4; i++) 
        //  {
        //      mmInt_t temp = p->chr[i];
        //      p->chr[i] = p->chr[i + 4];
        //      p->chr[i + 4] = temp;
        //  }
        //  mmEmuMapper187_SetBankPPU(p);
        //}

        p->chr_mode = data;
        break;

    case 0x8001:
        if (!p->ext_enable)
        {
            switch (p->chr_mode & 7)
            {
            case 0:
                data &= 0xFE;
                p->chr[4] = (mmInt_t)data + 0x100;
                p->chr[5] = (mmInt_t)data + 0x100 + 1;
                //p->chr[0+((p->chr_mode&0x80)?4:0)] = data;
                //p->chr[1+((p->chr_mode&0x80)?4:0)] = data+1;
                mmEmuMapper187_SetBankPPU(p);
                break;
            case 1:
                data &= 0xFE;
                p->chr[6] = (mmInt_t)data + 0x100;
                p->chr[7] = (mmInt_t)data + 0x100 + 1;
                //p->chr[2 + ((p->chr_mode & 0x80) ? 4 : 0)] = data;
                //p->chr[3 + ((p->chr_mode & 0x80) ? 4 : 0)] = data + 1;
                mmEmuMapper187_SetBankPPU(p);
                break;
            case 2:
                p->chr[0] = data;
                //p->chr[0+((p->chr_mode&0x80)?0:4)] = data;
                mmEmuMapper187_SetBankPPU(p);
                break;
            case 3:
                p->chr[1] = data;
                //p->chr[1+((p->chr_mode&0x80)?0:4)] = data;
                mmEmuMapper187_SetBankPPU(p);
                break;
            case 4:
                p->chr[2] = data;
                //p->chr[2+((p->chr_mode&0x80)?0:4)] = data;
                mmEmuMapper187_SetBankPPU(p);
                break;
            case 5:
                p->chr[3] = data;
                //p->chr[3+((p->chr_mode&0x80)?0:4)] = data;
                mmEmuMapper187_SetBankPPU(p);
                break;
            case 6:
                if ((p->ext_mode & 0xA0) != 0xA0)
                {
                    p->prg[0] = data;
                    mmEmuMapper187_SetBankCPU(p);
                }
                break;
            case 7:
                if ((p->ext_mode & 0xA0) != 0xA0)
                {
                    p->prg[1] = data;
                    mmEmuMapper187_SetBankCPU(p);
                }
                break;
            default:
                break;
            }
        }
        else
        {
            switch (p->chr_mode)
            {
            case 0x2A:
                p->prg[1] = 0x0F;
                break;
            case 0x28:
                p->prg[2] = 0x17;
                break;
            case 0x26:
                break;
            default:
                break;
            }
            mmEmuMapper187_SetBankCPU(p);
        }
        p->bank[p->chr_mode & 7] = data;
        break;

    case 0xA000:
        if (data & 0x01)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        break;
    case 0xA001:
        break;

    case 0xC000:
        p->irq_counter = data;
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xC001:
        p->irq_latch = data;
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE000:
    case 0xE002:
        p->irq_enable = 0;
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
    case 0xE003:
        p->irq_enable = 1;
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper187_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    //struct mmEmuMapper187* p = (struct mmEmuMapper187*)(super);

    //struct mmEmuNes* nes = p->super.nes;

    //if (p->irq_occur)
    //{
    //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
    //}
}
MM_EXPORT_EMU void mmEmuMapper187_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable)
            {
                if (!p->irq_counter)
                {
                    p->irq_counter--;
                    p->irq_enable = 0;
                    p->irq_occur = 0xFF;
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
                else
                {
                    p->irq_counter--;
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper187_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper187_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)(super);

    mmMemcpy(&buffer[0], &p->prg[0], sizeof(mmByte_t) * 4);
    mmMemcpy(&buffer[4], &p->bank[0], sizeof(mmByte_t) * 8);
    mmMemcpy(&buffer[12], &p->chr[0], sizeof(mmInt_t) * 8);
    buffer[44] = p->ext_mode;
    buffer[45] = p->chr_mode;
    buffer[46] = p->ext_enable;
    buffer[47] = p->irq_enable;
    buffer[48] = p->irq_counter;
    buffer[49] = p->irq_latch;
    buffer[50] = p->irq_occur;
    buffer[51] = p->last_write;
}
MM_EXPORT_EMU void mmEmuMapper187_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)(super);

    mmMemcpy(&p->prg[0], &buffer[0], sizeof(mmByte_t) * 4);
    mmMemcpy(&p->bank[0], &buffer[4], sizeof(mmByte_t) * 8);
    mmMemcpy(&p->chr[0], &buffer[12], sizeof(mmInt_t) * 8);
    p->ext_mode = buffer[44];
    p->chr_mode = buffer[45];
    p->ext_enable = buffer[46];
    p->irq_enable = buffer[47];
    p->irq_counter = buffer[48];
    p->irq_latch = buffer[49];
    p->irq_occur = buffer[50];
    p->last_write = buffer[51];
}

static struct mmEmuMapper* __static_mmEmuMapper187_Produce(void)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)mmMalloc(sizeof(struct mmEmuMapper187));
    mmEmuMapper187_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper187_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper187* p = (struct mmEmuMapper187*)(m);
    mmEmuMapper187_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER187 =
{
    &__static_mmEmuMapper187_Produce,
    &__static_mmEmuMapper187_Recycle,
};
