/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuApuFme7.h"

#include "emu/mmEmuState.h"

#include "core/mmString.h"

#define CHANNEL_VOL_SHIFT 8

// Envelope tables
MM_EXPORT_EMU const mmByte_t MM_EMU_APU_FME7_ENVELOPE_PULSE0[] =
{
    0x1F, 0x1E, 0x1D, 0x1C, 0x1B, 0x1A, 0x19, 0x18,
    0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10,
    0x0F, 0x0E, 0x0D, 0x0C, 0x0B, 0x0A, 0x09, 0x08,
    0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00,
};
MM_EXPORT_EMU const mmByte_t MM_EMU_APU_FME7_ENVELOPE_PULSE1[] =
{
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
    0x00,
};
MM_EXPORT_EMU const mmByte_t MM_EMU_APU_FME7_ENVELOPE_PULSE2[] =
{
    0x1F, 0x1E, 0x1D, 0x1C, 0x1B, 0x1A, 0x19, 0x18,
    0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10,
    0x0F, 0x0E, 0x0D, 0x0C, 0x0B, 0x0A, 0x09, 0x08,
    0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x1F,
};
MM_EXPORT_EMU const mmByte_t MM_EMU_APU_FME7_ENVELOPE_PULSE3[] =
{
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
    0x1F,
};
MM_EXPORT_EMU const mmSChar_t MM_EMU_APU_FME7_ENVSTEP_PULSE[] =
{
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 0,
};

MM_EXPORT_EMU const mmByte_t MM_EMU_APU_FME7_ENVELOPE_SAWTOOTH0[] =
{
    0x1F, 0x1E, 0x1D, 0x1C, 0x1B, 0x1A, 0x19, 0x18,
    0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10,
    0x0F, 0x0E, 0x0D, 0x0C, 0x0B, 0x0A, 0x09, 0x08,
    0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00,
};
MM_EXPORT_EMU const mmByte_t MM_EMU_APU_FME7_ENVELOPE_SAWTOOTH1[] =
{
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
};
MM_EXPORT_EMU const mmSChar_t MM_EMU_APU_FME7_ENVSTEP_SAWTOOTH[] =
{
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, -15,
};

MM_EXPORT_EMU const mmByte_t MM_EMU_APU_FME7_ENVELOPE_TRIANGLE0[] =
{
    0x1F, 0x1E, 0x1D, 0x1C, 0x1B, 0x1A, 0x19, 0x18,
    0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10,
    0x0F, 0x0E, 0x0D, 0x0C, 0x0B, 0x0A, 0x09, 0x08,
    0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00,
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
};
MM_EXPORT_EMU const mmByte_t MM_EMU_APU_FME7_ENVELOPE_TRIANGLE1[] =
{
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
    0x1F, 0x1E, 0x1D, 0x1C, 0x1B, 0x1A, 0x19, 0x18,
    0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10,
    0x0F, 0x0E, 0x0D, 0x0C, 0x0B, 0x0A, 0x09, 0x08,
    0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00,
};
MM_EXPORT_EMU const mmSChar_t MM_EMU_APU_FME7_ENVSTEP_TRIANGLE[] =
{
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, -31,
};

MM_EXPORT_EMU const mmByte_t* MM_EMU_APU_FME7_ENVELOPE_TABLE[16] =
{
    MM_EMU_APU_FME7_ENVELOPE_PULSE0,    MM_EMU_APU_FME7_ENVELOPE_PULSE0, MM_EMU_APU_FME7_ENVELOPE_PULSE0,    MM_EMU_APU_FME7_ENVELOPE_PULSE0,
    MM_EMU_APU_FME7_ENVELOPE_PULSE1,    MM_EMU_APU_FME7_ENVELOPE_PULSE1, MM_EMU_APU_FME7_ENVELOPE_PULSE1,    MM_EMU_APU_FME7_ENVELOPE_PULSE1,
    MM_EMU_APU_FME7_ENVELOPE_SAWTOOTH0, MM_EMU_APU_FME7_ENVELOPE_PULSE0, MM_EMU_APU_FME7_ENVELOPE_TRIANGLE0, MM_EMU_APU_FME7_ENVELOPE_PULSE2,
    MM_EMU_APU_FME7_ENVELOPE_SAWTOOTH1, MM_EMU_APU_FME7_ENVELOPE_PULSE3, MM_EMU_APU_FME7_ENVELOPE_TRIANGLE1, MM_EMU_APU_FME7_ENVELOPE_PULSE1,
};
MM_EXPORT_EMU const mmSChar_t*  MM_EMU_APU_FME7_ENVSTEP_TABLE[16] =
{
    MM_EMU_APU_FME7_ENVSTEP_PULSE,    MM_EMU_APU_FME7_ENVSTEP_PULSE, MM_EMU_APU_FME7_ENVSTEP_PULSE,    MM_EMU_APU_FME7_ENVSTEP_PULSE,
    MM_EMU_APU_FME7_ENVSTEP_PULSE,    MM_EMU_APU_FME7_ENVSTEP_PULSE, MM_EMU_APU_FME7_ENVSTEP_PULSE,    MM_EMU_APU_FME7_ENVSTEP_PULSE,
    MM_EMU_APU_FME7_ENVSTEP_SAWTOOTH, MM_EMU_APU_FME7_ENVSTEP_PULSE, MM_EMU_APU_FME7_ENVSTEP_TRIANGLE, MM_EMU_APU_FME7_ENVSTEP_PULSE,
    MM_EMU_APU_FME7_ENVSTEP_SAWTOOTH, MM_EMU_APU_FME7_ENVSTEP_PULSE, MM_EMU_APU_FME7_ENVSTEP_TRIANGLE, MM_EMU_APU_FME7_ENVSTEP_PULSE,
};

MM_EXPORT_EMU void mmEmuApuFme7_Init(struct mmEmuApuFme7* p)
{
    mmEmuApuInterface_Init(&p->super);

    mmMemset(&p->envelope, 0, sizeof(struct mmEmuApuFme7Envelope));
    mmMemset(&p->noise, 0, sizeof(struct mmEmuApuFme7Noise));
    mmMemset(&p->op, 0, sizeof(struct mmEmuApuFme7Channel) * 3);

    p->address = 0;

    mmMemset(&p->vol_table, 0, sizeof(mmInt_t) * 0x20);

    p->cycle_rate = 0;

    p->cpu_clock = 0;

    //
    p->super.Reset = &mmEmuApuFme7_Reset;
    p->super.Setup = &mmEmuApuFme7_Setup;
    p->super.Process = &mmEmuApuFme7_Process;
    p->super.Write = &mmEmuApuFme7_Write;
    //p->super.Read = &mm_emu_apu_fme7_Read;
    //p->super.SyncWrite = &mm_emu_apu_fme7_SyncWrite;
    //p->super.SyncRead = &mm_emu_apu_fme7_SyncRead;
    //p->super.Sync = &mm_emu_apu_fme7_Sync;
    p->super.GetFreq = &mmEmuApuFme7_GetFreq;
    p->super.GetStateSize = &mmEmuApuFme7_GetStateSize;
    p->super.SaveState = &mmEmuApuFme7_SaveState;
    p->super.LoadState = &mmEmuApuFme7_LoadState;

    // Provisional setting.
    (*(p->super.Reset))(&p->super, MM_EMU_APU_CLOCK, 22050);
}
MM_EXPORT_EMU void mmEmuApuFme7_Destroy(struct mmEmuApuFme7* p)
{
    mmEmuApuInterface_Destroy(&p->super);

    mmMemset(&p->envelope, 0, sizeof(struct mmEmuApuFme7Envelope));
    mmMemset(&p->noise, 0, sizeof(struct mmEmuApuFme7Noise));
    mmMemset(&p->op, 0, sizeof(struct mmEmuApuFme7Channel) * 3);

    p->address = 0;

    mmMemset(&p->vol_table, 0, sizeof(mmInt_t) * 0x20);

    p->cycle_rate = 0;

    p->cpu_clock = 0;
}

MM_EXPORT_EMU void mmEmuApuFme7_EnvelopeRender(struct mmEmuApuFme7* p)
{
    if (!p->envelope.freq)
    {
        return;
    }
    p->envelope.phaseacc -= p->cycle_rate;
    if (p->envelope.phaseacc >= 0)
    {
        return;
    }
    while (p->envelope.phaseacc < 0)
    {
        p->envelope.phaseacc += p->envelope.freq;
        p->envelope.envadr += p->envelope.envstep[p->envelope.envadr];
    }
    p->envelope.volume = p->envelope.envtbl[p->envelope.envadr];
}
MM_EXPORT_EMU void mmEmuApuFme7_NoiseRender(struct mmEmuApuFme7* p)
{
    if (!p->noise.freq)
    {
        return;
    }
    p->noise.phaseacc -= p->cycle_rate;
    if (p->noise.phaseacc >= 0)
    {
        return;
    }
    while (p->noise.phaseacc < 0)
    {
        p->noise.phaseacc += p->noise.freq;
        if ((p->noise.noiserange + 1) & 0x02)
        {
            p->noise.noiseout = ~p->noise.noiseout;
        }
        if (p->noise.noiserange & 0x01)
        {
            p->noise.noiserange ^= 0x28000;
        }
        p->noise.noiserange >>= 1;
    }
}

MM_EXPORT_EMU mmInt_t mmEmuApuFme7_ChannelRender(struct mmEmuApuFme7* p, struct mmEmuApuFme7Channel* ch)
{
    mmInt_t output, volume;

    if (ch->enable)
    {
        return 0;
    }
    if (!ch->freq)
    {
        return 0;
    }

    ch->phaseacc -= p->cycle_rate;
    while (ch->phaseacc < 0)
    {
        ch->phaseacc += ch->freq;
        ch->adder++;
    }

    output = volume = 0;
    volume = ch->env_on ? p->vol_table[p->envelope.volume] : p->vol_table[ch->volume + 1];

    if (ch->adder & 0x01)
    {
        output += volume;
    }
    else
    {
        output -= volume;
    }
    if (!ch->noise_on)
    {
        if (p->noise.noiseout)
        {
            output += volume;
        }
        else
        {
            output -= volume;
        }
    }

    ch->output_vol = output;

    return ch->output_vol;
}

MM_EXPORT_EMU void mmEmuApuFme7_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuFme7* p = (struct mmEmuApuFme7*)(super);

    mmInt_t i;

    double out = 0;

    mmMemset(&p->envelope, 0, sizeof(struct mmEmuApuFme7Envelope));
    mmMemset(&p->noise, 0, sizeof(struct mmEmuApuFme7Noise));
    mmMemset(&p->op, 0, sizeof(struct mmEmuApuFme7Channel) * 3);

    p->envelope.envtbl = MM_EMU_APU_FME7_ENVELOPE_TABLE[0];
    p->envelope.envstep = MM_EMU_APU_FME7_ENVSTEP_TABLE[0];

    p->noise.noiserange = 1;
    p->noise.noiseout = 0xFF;

    p->address = 0;

    // Volume to voltage
    out = 0x1FFF;
    for (i = 31; i > 1; i--)
    {
        p->vol_table[i] = (mmInt_t)(out + 0.5);
        out /= 1.188502227; /* = 10 ^ (1.5/20) = 1.5dB */
    }
    p->vol_table[1] = 0;
    p->vol_table[0] = 0;

    (*(p->super.Setup))(&p->super, fClock, nRate);
}
MM_EXPORT_EMU void mmEmuApuFme7_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate)
{
    struct mmEmuApuFme7* p = (struct mmEmuApuFme7*)(super);

    p->cpu_clock = fClock;
    p->cycle_rate = (mmInt_t)((fClock / 16.0f)*(1 << 16) / nRate);
}
MM_EXPORT_EMU void mmEmuApuFme7_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuApuFme7* p = (struct mmEmuApuFme7*)(super);

    if (addr == 0xC000)
    {
        p->address = data;
    }
    else if (addr == 0xE000)
    {
        mmByte_t chaddr = p->address;
        switch (chaddr)
        {
        case 0x00: case 0x01:
        case 0x02: case 0x03:
        case 0x04: case 0x05:
        {
            struct mmEmuApuFme7Channel* ch = &(p->op[chaddr >> 1]);
            ch->reg[chaddr & 0x01] = data;
            ch->freq = MM_EMU_INT2FIX(((mmInt_t)(ch->reg[1] & 0x0F) << 8) + ch->reg[0] + 1);
        }
        break;
        case 0x06:
            p->noise.freq = MM_EMU_INT2FIX((mmInt_t)(data & 0x1F) + 1);
            break;
        case 0x07:
        {
            mmInt_t i = 0;
            for (i = 0; i < 3; i++)
            {
                p->op[i].enable = data & (1 << i);
                p->op[i].noise_on = data & (8 << i);
            }
        }
        break;
        case 0x08:
        case 0x09:
        case 0x0A:
        {
            struct mmEmuApuFme7Channel* ch = &(p->op[chaddr & 3]);
            ch->reg[2] = data;
            ch->env_on = data & 0x10;
            ch->volume = (data & 0x0F) * 2;
        }
        break;
        case 0x0B:
        case 0x0C:
            p->envelope.reg[chaddr - 0x0B] = data;
            p->envelope.freq = MM_EMU_INT2FIX(((mmInt_t)(p->envelope.reg[1] & 0x0F) << 8) + p->envelope.reg[0] + 1);
            break;
        case 0x0D:
            p->envelope.envtbl = MM_EMU_APU_FME7_ENVELOPE_TABLE[data & 0x0F];
            p->envelope.envstep = MM_EMU_APU_FME7_ENVSTEP_TABLE[data & 0x0F];
            p->envelope.envadr = 0;
            break;
        default:
            break;
        }
    }
}
MM_EXPORT_EMU mmInt_t mmEmuApuFme7_Process(struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuFme7* p = (struct mmEmuApuFme7*)(super);

    if (channel < 3)
    {
        return mmEmuApuFme7_ChannelRender(p, &(p->op[channel]));
    }
    else if (channel == 3)
    {
        // Be sure to call ch 0 - 2 after calling ch 3 once.
        mmEmuApuFme7_EnvelopeRender(p);
        mmEmuApuFme7_NoiseRender(p);
    }
    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuFme7_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel)
{
    struct mmEmuApuFme7* p = (struct mmEmuApuFme7*)(super);

    if (channel < 3)
    {
        struct mmEmuApuFme7Channel* ch = &p->op[channel];

        if (ch->enable || !ch->freq)
        {
            return 0;
        }
        if (ch->env_on)
        {
            if (!p->envelope.volume)
            {
                return 0;
            }
        }
        else
        {
            if (!ch->volume)
            {
                return 0;
            }
        }

        return (mmInt_t)(256.0f * p->cpu_clock / ((double)MM_EMU_FIX2INT(ch->freq) * 16.0f));
    }

    return 0;
}

MM_EXPORT_EMU mmInt_t mmEmuApuFme7_GetStateSize(const struct mmEmuApuInterface* super)
{
    // struct mmEmuApuFme7* p = (struct mmEmuApuFme7*)(super);

    return
        sizeof(mmByte_t) +
        sizeof(struct mmEmuApuFme7Envelope) +
        sizeof(struct mmEmuApuFme7Noise) +
        sizeof(struct mmEmuApuFme7Channel) * 3;
}
MM_EXPORT_EMU void mmEmuApuFme7_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuFme7* p = (struct mmEmuApuFme7*)(super);

    MM_EMU_SETBYTE(buffer, p->address);

    MM_EMU_SETBLOCK(buffer, &p->envelope, sizeof(struct mmEmuApuFme7Envelope));
    MM_EMU_SETBLOCK(buffer, &p->noise, sizeof(struct mmEmuApuFme7Noise));
    MM_EMU_SETBLOCK(buffer, p->op, sizeof(struct mmEmuApuFme7Channel) * 3);
}
MM_EXPORT_EMU void mmEmuApuFme7_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer)
{
    struct mmEmuApuFme7* p = (struct mmEmuApuFme7*)(super);

    MM_EMU_GETBYTE(buffer, p->address);

    MM_EMU_GETBLOCK(buffer, &p->envelope, sizeof(struct mmEmuApuFme7Envelope));
    MM_EMU_GETBLOCK(buffer, &p->noise, sizeof(struct mmEmuApuFme7Noise));
    MM_EMU_GETBLOCK(buffer, p->op, sizeof(struct mmEmuApuFme7Channel) * 3);
}

