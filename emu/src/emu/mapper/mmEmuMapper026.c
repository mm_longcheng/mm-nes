/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper026.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper026_Init(struct mmEmuMapper026* p)
{
    mmEmuMapper_Init(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    //
    p->super.Reset = &mmEmuMapper026_Reset;
    p->super.Write = &mmEmuMapper026_Write;
    //p->super.Read = &mmEmuMapper026_Read;
    //p->super.ReadLow = &mmEmuMapper026_ReadLow;
    //p->super.WriteLow = &mmEmuMapper026_WriteLow;
    //p->super.ExRead = &mmEmuMapper026_ExRead;
    //p->super.ExWrite = &mmEmuMapper026_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper026_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper026_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper026_HSync;
    //p->super.VSync = &mmEmuMapper026_VSync;
    p->super.Clock = &mmEmuMapper026_Clock;
    //p->super.PPULatch = &mmEmuMapper026_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper026_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper026_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper026_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper026_IsStateSave;
    p->super.SaveState = &mmEmuMapper026_SaveState;
    p->super.LoadState = &mmEmuMapper026_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper026_Destroy(struct mmEmuMapper026* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;
}

MM_EXPORT_EMU void mmEmuMapper026_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper026* p = (struct mmEmuMapper026*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);

    if (mmu->VROM_1K_SIZE)
    {
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0x30e64d03)      // Esper Dream 2 - Aratanaru Tatakai(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_ALL_RENDER);
    }
    if (crc == 0x836cc1ab)      // Mouryou Senki Madara(J)
    {
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
    }

    mmEmuApu_SelectExSound(&nes->apu, 1);
}
MM_EXPORT_EMU void mmEmuMapper026_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper026* p = (struct mmEmuMapper026*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    switch (addr & 0xF003)
    {
    case 0x8000:
        mmEmuMmu_SetPROM16KBank(mmu, 4, data);
        break;

    case 0x9000: case 0x9001: case 0x9002: case 0x9003:
    case 0xA000: case 0xA001: case 0xA002: case 0xA003:
    case 0xB000: case 0xB001: case 0xB002:
        addr = (addr & 0xfffc) | ((addr & 1) << 1) | ((addr & 2) >> 1);
        mmEmuApu_ExWrite(&nes->apu, addr, data);
        break;

    case 0xB003:
        data = data & 0x7F;
        if (data == 0x08 || data == 0x2C)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4H);
        }
        else if (data == 0x20)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else if (data == 0x24)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
        else if (data == 0x28)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_MIRROR4L);
        }
        break;

    case 0xC000:
        mmEmuMmu_SetPROM8KBank(mmu, 6, data);
        break;

    case 0xD000:
        mmEmuMmu_SetVROM1KBank(mmu, 0, data);
        break;

    case 0xD001:
        mmEmuMmu_SetVROM1KBank(mmu, 2, data);
        break;

    case 0xD002:
        mmEmuMmu_SetVROM1KBank(mmu, 1, data);
        break;

    case 0xD003:
        mmEmuMmu_SetVROM1KBank(mmu, 3, data);
        break;

    case 0xE000:
        mmEmuMmu_SetVROM1KBank(mmu, 4, data);
        break;

    case 0xE001:
        mmEmuMmu_SetVROM1KBank(mmu, 6, data);
        break;

    case 0xE002:
        mmEmuMmu_SetVROM1KBank(mmu, 5, data);
        break;

    case 0xE003:
        mmEmuMmu_SetVROM1KBank(mmu, 7, data);
        break;

    case 0xF000:
        p->irq_latch = data;
        break;
    case 0xF001:
        p->irq_enable = (p->irq_enable & 0x01) * 3;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xF002:
        p->irq_enable = data & 0x03;
        if (p->irq_enable & 0x02)
        {
            p->irq_counter = p->irq_latch;
            p->irq_clock = 0;
        }
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper026_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper026* p = (struct mmEmuMapper026*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable & 0x02)
    {
        if ((p->irq_clock += cycles) >= 0x72)
        {
            p->irq_clock -= 0x72;
            if (p->irq_counter >= 0xFF)
            {
                p->irq_counter = p->irq_latch;

                //mm_emu_cpu_IRQ_NotPending(&nes->cpu);
                //mm_emu_cpu_IRQ(&nes->cpu);

                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
            else
            {
                p->irq_counter++;
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper026_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper026_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper026* p = (struct mmEmuMapper026*)(super);

    buffer[0] = p->irq_enable;
    buffer[1] = p->irq_counter;
    buffer[2] = p->irq_latch;
    *(mmInt_t*)&buffer[3] = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper026_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper026* p = (struct mmEmuMapper026*)(super);

    p->irq_enable = buffer[0];
    p->irq_counter = buffer[1];
    p->irq_latch = buffer[2];
    p->irq_clock = *(mmInt_t*)&buffer[3];
}

static struct mmEmuMapper* __static_mmEmuMapper026_Produce(void)
{
    struct mmEmuMapper026* p = (struct mmEmuMapper026*)mmMalloc(sizeof(struct mmEmuMapper026));
    mmEmuMapper026_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper026_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper026* p = (struct mmEmuMapper026*)(m);
    mmEmuMapper026_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER026 =
{
    &__static_mmEmuMapper026_Produce,
    &__static_mmEmuMapper026_Recycle,
};
