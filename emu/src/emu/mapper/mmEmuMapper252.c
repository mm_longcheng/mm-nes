/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper252.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper252_Init(struct mmEmuMapper252* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_clock = 0;

    //
    p->super.Reset = &mmEmuMapper252_Reset;
    p->super.Write = &mmEmuMapper252_Write;
    //p->super.Read = &mmEmuMapper252_Read;
    //p->super.ReadLow = &mmEmuMapper252_ReadLow;
    //p->super.WriteLow = &mmEmuMapper252_WriteLow;
    //p->super.ExRead = &mmEmuMapper252_ExRead;
    //p->super.ExWrite = &mmEmuMapper252_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper252_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper252_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper252_HSync;
    //p->super.VSync = &mmEmuMapper252_VSync;
    p->super.Clock = &mmEmuMapper252_Clock;
    //p->super.PPULatch = &mmEmuMapper252_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper252_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper252_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper252_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper252_IsStateSave;
    p->super.SaveState = &mmEmuMapper252_SaveState;
    p->super.LoadState = &mmEmuMapper252_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper252_Destroy(struct mmEmuMapper252* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 9);
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_occur = 0;
    p->irq_clock = 0;
}

MM_EXPORT_EMU void mmEmuMapper252_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper252* p = (struct mmEmuMapper252*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmInt_t i = 0;

    for (i = 0; i < 8; i++)
    {
        p->reg[i] = i;
    }
    p->reg[8] = 0;

    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_clock = 0;
    p->irq_occur = 0;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    mmEmuMmu_SetVROM8KBank(mmu, 0);

    mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_POST_RENDER);
}
MM_EXPORT_EMU void mmEmuMapper252_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper252* p = (struct mmEmuMapper252*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if ((addr & 0xF000) == 0x8000)
    {
        mmEmuMmu_SetPROM8KBank(mmu, 4, data);
        return;
    }
    if ((addr & 0xF000) == 0xA000)
    {
        mmEmuMmu_SetPROM8KBank(mmu, 5, data);
        return;
    }
    switch (addr & 0xF00C)
    {
    case 0xB000:
        p->reg[0] = (p->reg[0] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;
    case 0xB004:
        p->reg[0] = (p->reg[0] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 0, p->reg[0]);
        break;
    case 0xB008:
        p->reg[1] = (p->reg[1] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;
    case 0xB00C:
        p->reg[1] = (p->reg[1] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 1, p->reg[1]);
        break;

    case 0xC000:
        p->reg[2] = (p->reg[2] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;
    case 0xC004:
        p->reg[2] = (p->reg[2] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 2, p->reg[2]);
        break;
    case 0xC008:
        p->reg[3] = (p->reg[3] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;
    case 0xC00C:
        p->reg[3] = (p->reg[3] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 3, p->reg[3]);
        break;

    case 0xD000:
        p->reg[4] = (p->reg[4] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;
    case 0xD004:
        p->reg[4] = (p->reg[4] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 4, p->reg[4]);
        break;
    case 0xD008:
        p->reg[5] = (p->reg[5] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;
    case 0xD00C:
        p->reg[5] = (p->reg[5] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 5, p->reg[5]);
        break;

    case 0xE000:
        p->reg[6] = (p->reg[6] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;
    case 0xE004:
        p->reg[6] = (p->reg[6] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 6, p->reg[6]);
        break;
    case 0xE008:
        p->reg[7] = (p->reg[7] & 0xF0) | (data & 0x0F);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;
    case 0xE00C:
        p->reg[7] = (p->reg[7] & 0x0F) | ((data & 0x0F) << 4);
        mmEmuMmu_SetVROM1KBank(mmu, 7, p->reg[7]);
        break;

    case 0xF000:
        p->irq_latch = (p->irq_latch & 0xF0) | (data & 0x0F);
        p->irq_occur = 0;
        break;
    case 0xF004:
        p->irq_latch = (p->irq_latch & 0x0F) | ((data & 0x0F) << 4);
        p->irq_occur = 0;
        break;

    case 0xF008:
        p->irq_enable = data & 0x03;
        if (p->irq_enable & 0x02)
        {
            p->irq_counter = p->irq_latch;
            p->irq_clock = 0;
        }
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;

    case 0xF00C:
        p->irq_enable = (p->irq_enable & 0x01) * 3;
        p->irq_occur = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper252_Clock(struct mmEmuMapper* super, mmInt_t cycles)
{
    struct mmEmuMapper252* p = (struct mmEmuMapper252*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if (p->irq_enable & 0x02)
    {
        if ((p->irq_clock += cycles) >= 0x72)
        {
            p->irq_clock -= 0x72;
            if (p->irq_counter == 0xFF)
            {
                p->irq_occur = 0xFF;
                p->irq_counter = p->irq_latch;
                p->irq_enable = (p->irq_enable & 0x01) * 3;

                mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
            }
            else
            {
                p->irq_counter++;
            }
        }
        //if (p->irq_occur)
        //{
        //  mm_emu_cpu_IRQ_NotPending(&nes->cpu);
        //}
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper252_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper252_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper252* p = (struct mmEmuMapper252*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 9);
    buffer[9] = p->irq_enable;
    buffer[10] = p->irq_counter;
    buffer[11] = p->irq_latch;
    *(mmInt_t*)&buffer[12] = p->irq_clock;
}
MM_EXPORT_EMU void mmEmuMapper252_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper252* p = (struct mmEmuMapper252*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 9);
    p->irq_enable = buffer[9];
    p->irq_counter = buffer[10];
    p->irq_latch = buffer[11];
    p->irq_clock = *(mmInt_t*)&buffer[12];
}

static struct mmEmuMapper* __static_mmEmuMapper252_Produce(void)
{
    struct mmEmuMapper252* p = (struct mmEmuMapper252*)mmMalloc(sizeof(struct mmEmuMapper252));
    mmEmuMapper252_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper252_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper252* p = (struct mmEmuMapper252*)(m);
    mmEmuMapper252_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER252 =
{
    &__static_mmEmuMapper252_Produce,
    &__static_mmEmuMapper252_Recycle,
};
