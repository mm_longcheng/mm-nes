/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper008.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper008_Init(struct mmEmuMapper008* p)
{
    mmEmuMapper_Init(&p->super);

    //
    p->super.Reset = &mmEmuMapper008_Reset;
    p->super.Write = &mmEmuMapper008_Write;
    //p->super.Read = &mmEmuMapper008_Read;
    //p->super.ReadLow = &mmEmuMapper008_ReadLow;
    //p->super.WriteLow = &mmEmuMapper008_WriteLow;
    //p->super.ExRead = &mmEmuMapper008_ExRead;
    //p->super.ExWrite = &mmEmuMapper008_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper008_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper008_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper008_HSync;
    //p->super.VSync = &mmEmuMapper008_VSync;
    //p->super.Clock = &mmEmuMapper008_Clock;
    //p->super.PPULatch = &mmEmuMapper008_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper008_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper008_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper008_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper008_IsStateSave;
    //p->super.SaveState = &mmEmuMapper008_SaveState;
    //p->super.LoadState = &mmEmuMapper008_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper008_Destroy(struct mmEmuMapper008* p)
{
    mmEmuMapper_Destroy(&p->super);
}

MM_EXPORT_EMU void mmEmuMapper008_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper008* p = (struct mmEmuMapper008*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM32KBankArray(mmu, 0, 1, 2, 3);
    mmEmuMmu_SetVROM8KBank(mmu, 0);
}
MM_EXPORT_EMU void mmEmuMapper008_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper008* p = (struct mmEmuMapper008*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmEmuMmu_SetPROM16KBank(mmu, 4, (data & 0xF8) >> 3);
    mmEmuMmu_SetVROM8KBank(mmu, data & 0x07);
}

static struct mmEmuMapper* __static_mmEmuMapper008_Produce(void)
{
    struct mmEmuMapper008* p = (struct mmEmuMapper008*)mmMalloc(sizeof(struct mmEmuMapper008));
    mmEmuMapper008_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper008_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper008* p = (struct mmEmuMapper008*)(m);
    mmEmuMapper008_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER008 =
{
    &__static_mmEmuMapper008_Produce,
    &__static_mmEmuMapper008_Recycle,
};
