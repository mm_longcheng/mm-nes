/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper060.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper060_Init(struct mmEmuMapper060* p)
{
    mmEmuMapper_Init(&p->super);

    p->patch = 0;
    p->game_sel = 0;

    //
    p->super.Reset = &mmEmuMapper060_Reset;
    p->super.Write = &mmEmuMapper060_Write;
    //p->super.Read = &mmEmuMapper060_Read;
    //p->super.ReadLow = &mmEmuMapper060_ReadLow;
    //p->super.WriteLow = &mmEmuMapper060_WriteLow;
    //p->super.ExRead = &mmEmuMapper060_ExRead;
    //p->super.ExWrite = &mmEmuMapper060_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper060_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper060_ExCmdWrite;
    //p->super.HSync = &mmEmuMapper060_HSync;
    //p->super.VSync = &mmEmuMapper060_VSync;
    //p->super.Clock = &mmEmuMapper060_Clock;
    //p->super.PPULatch = &mmEmuMapper060_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper060_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper060_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper060_PPUExtLatch;
    //p->super.IsStateSave = &mmEmuMapper060_IsStateSave;
    //p->super.SaveState = &mmEmuMapper060_SaveState;
    //p->super.LoadState = &mmEmuMapper060_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper060_Destroy(struct mmEmuMapper060* p)
{
    mmEmuMapper_Destroy(&p->super);

    p->patch = 0;
    p->game_sel = 0;
}

MM_EXPORT_EMU void mmEmuMapper060_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper060* p = (struct mmEmuMapper060*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    p->patch = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    if (crc == 0xf9c484a0)          // Reset Based 4-in-1(Unl)
    {
        mmEmuMmu_SetPROM16KBank(mmu, 4, p->game_sel);
        mmEmuMmu_SetPROM16KBank(mmu, 6, p->game_sel);
        mmEmuMmu_SetVROM8KBank(mmu, p->game_sel);
        p->game_sel++;
        p->game_sel &= 3;
    }
    else
    {
        p->patch = 1;
        mmEmuMmu_SetPROM32KBank(mmu, 0);
        mmEmuMmu_SetVROM8KBank(mmu, 0);
    }
}
MM_EXPORT_EMU void mmEmuMapper060_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper060* p = (struct mmEmuMapper060*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->patch)
    {
        if (addr & 0x80)
        {
            mmEmuMmu_SetPROM16KBank(mmu, 4, (addr & 0x70) >> 4);
            mmEmuMmu_SetPROM16KBank(mmu, 6, (addr & 0x70) >> 4);
        }
        else
        {
            mmEmuMmu_SetPROM32KBank(mmu, (addr & 0x70) >> 5);
        }

        mmEmuMmu_SetVROM8KBank(mmu, addr & 0x07);

        if (data & 0x08)
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
        }
        else
        {
            mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
        }
    }
}

static struct mmEmuMapper* __static_mmEmuMapper060_Produce(void)
{
    struct mmEmuMapper060* p = (struct mmEmuMapper060*)mmMalloc(sizeof(struct mmEmuMapper060));
    mmEmuMapper060_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper060_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper060* p = (struct mmEmuMapper060*)(m);
    mmEmuMapper060_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER060 =
{
    &__static_mmEmuMapper060_Produce,
    &__static_mmEmuMapper060_Recycle,
};
