/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuPadGyromite_h__
#define __mmEmuPadGyromite_h__


#include "core/mmCore.h"

#include "emu/mmEmuExpad.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Gyromite                                                             //
//----------------------------------------------------------------------//
struct mmEmuPadGyromite
{
    struct mmEmuExpad super;

    // TV Light signals
    mmWord_t    code[8];

    mmWord_t    bits;
    mmByte_t    arms;
    mmByte_t    spot;
};

MM_EXPORT_EMU void mmEmuPadGyromite_Init(struct mmEmuPadGyromite* p);
MM_EXPORT_EMU void mmEmuPadGyromite_Destroy(struct mmEmuPadGyromite* p);

MM_EXPORT_EMU mmInt_t mmEmuPadGyromite_CheckSignal(struct mmEmuPadGyromite* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuPadGyromite_Reset(struct mmEmuExpad* super);
MM_EXPORT_EMU void mmEmuPadGyromite_Strobe(struct mmEmuExpad* super);

MM_EXPORT_EMU void mmEmuPadGyromite_Sync(struct mmEmuExpad* super);
MM_EXPORT_EMU void mmEmuPadGyromite_SetSyncData(struct mmEmuExpad* super, mmInt_t type, mmLong_t data);
MM_EXPORT_EMU mmLong_t mmEmuPadGyromite_GetSyncData(const struct mmEmuExpad* super, mmInt_t type);

MM_EXPORT_EMU extern const struct mmEmuExpadCreator MM_EMU_PAD_CREATOR_GYROMITE;

#include "core/mmSuffix.h"

#endif//__mmEmuPadGyromite_h__
