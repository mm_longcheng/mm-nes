/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuApuN106_h__
#define __mmEmuApuN106_h__

#include "core/mmCore.h"

#include "emu/mmEmuApuInterface.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

struct mmEmuApuN106Channel
{
    mmInt_t     phaseacc;

    mmUInt32_t  freq;
    mmUInt32_t  phase;
    mmUInt32_t  tonelen;

    mmInt_t     output;

    mmByte_t    toneadr;
    mmByte_t    volupdate;

    mmByte_t    vol;
    mmByte_t    databuf;
};

struct mmEmuApuN106
{
    struct mmEmuApuInterface super;

    struct mmEmuApuN106Channel  op[8];

    double      cpu_clock;
    mmUInt32_t  cycle_rate;

    mmByte_t    addrinc;
    mmByte_t    address;
    mmByte_t    channel_use;

    mmByte_t    tone[0x100];
};

MM_EXPORT_EMU void mmEmuApuN106_Init(struct mmEmuApuN106* p);
MM_EXPORT_EMU void mmEmuApuN106_Destroy(struct mmEmuApuN106* p);

MM_EXPORT_EMU mmInt_t mmEmuApuN106_ChannelRender(struct mmEmuApuN106* p, struct mmEmuApuN106Channel* ch);

// virtual function for super.
MM_EXPORT_EMU void mmEmuApuN106_Reset(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuN106_Setup(struct mmEmuApuInterface* super, double fClock, mmInt_t nRate);
MM_EXPORT_EMU void mmEmuApuN106_Write(struct mmEmuApuInterface* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU mmByte_t mmEmuApuN106_Read(struct mmEmuApuInterface* super, mmWord_t addr);
MM_EXPORT_EMU mmInt_t mmEmuApuN106_Process(struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuN106_GetFreq(const struct mmEmuApuInterface* super, mmInt_t channel);

MM_EXPORT_EMU mmInt_t mmEmuApuN106_GetStateSize(const struct mmEmuApuInterface* super);
MM_EXPORT_EMU void mmEmuApuN106_SaveState(struct mmEmuApuInterface* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuApuN106_LoadState(struct mmEmuApuInterface* super, mmByte_t* buffer);

#include "core/mmSuffix.h"

#endif//__mmEmuApuN106_h__
