/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEmuMapper189_h__
#define __mmEmuMapper189_h__

#include "core/mmCore.h"

#include "emu/mmEmuMapper.h"

#include "emu/mmEmuExport.h"

#include "core/mmPrefix.h"

//----------------------------------------------------------------------//
// Mapper189  Street Fighter 2/Yoko version                             //
//            Street Fighter IV (GOUDER)                                //
//----------------------------------------------------------------------//
struct mmEmuMapper189
{
    struct mmEmuMapper super;

    mmByte_t    patch;

    mmByte_t    reg[2];
    mmByte_t    chr01, chr23, chr4, chr5, chr6, chr7;

    mmByte_t    irq_enable;
    mmByte_t    irq_counter;
    mmByte_t    irq_latch;

    // SF4
    mmByte_t    protect_dat[4];
    mmByte_t    lwd;
};

MM_EXPORT_EMU void mmEmuMapper189_Init(struct mmEmuMapper189* p);
MM_EXPORT_EMU void mmEmuMapper189_Destroy(struct mmEmuMapper189* p);

MM_EXPORT_EMU void mmEmuMapper189_SetBankCPU(struct mmEmuMapper189* p);
MM_EXPORT_EMU void mmEmuMapper189_SetBankPPU(struct mmEmuMapper189* p);

// virtual function for super.
MM_EXPORT_EMU void mmEmuMapper189_Reset(struct mmEmuMapper* super);

MM_EXPORT_EMU void mmEmuMapper189_WriteLow(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper189_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data);
MM_EXPORT_EMU void mmEmuMapper189_HSync(struct mmEmuMapper* super, mmInt_t scanline);

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper189_IsStateSave(const struct mmEmuMapper* super);
MM_EXPORT_EMU void mmEmuMapper189_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer);
MM_EXPORT_EMU void mmEmuMapper189_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer);

MM_EXPORT_EMU extern const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER189;

#include "core/mmSuffix.h"

#endif//__mmEmuMapper189_h__
