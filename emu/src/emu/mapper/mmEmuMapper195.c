/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEmuMapper195.h"
#include "emu/mmEmuNes.h"

MM_EXPORT_EMU void mmEmuMapper195_Init(struct mmEmuMapper195* p)
{
    mmEmuMapper_Init(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    p->we_sram = 0;

    p->irq_type = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    p->patch = 0;

    //
    p->super.Reset = &mmEmuMapper195_Reset;
    p->super.Write = &mmEmuMapper195_Write;
    //p->super.Read = &mmEmuMapper195_Read;
    //p->super.ReadLow = &mmEmuMapper195_ReadLow;
    //p->super.WriteLow = &mmEmuMapper195_WriteLow;
    //p->super.ExRead = &mmEmuMapper195_ExRead;
    //p->super.ExWrite = &mmEmuMapper195_ExWrite;
    //p->super.ExCmdRead = &mmEmuMapper195_ExCmdRead;
    //p->super.ExCmdWrite = &mmEmuMapper195_ExCmdWrite;
    p->super.HSync = &mmEmuMapper195_HSync;
    //p->super.VSync = &mmEmuMapper195_VSync;
    //p->super.Clock = &mmEmuMapper195_Clock;
    //p->super.PPULatch = &mmEmuMapper195_PPULatch;
    //p->super.PPUChrLatch = &mmEmuMapper195_PPUChrLatch;
    //p->super.PPUExtLatchX = &mmEmuMapper195_PPUExtLatchX;
    //p->super.PPUExtLatch = &mmEmuMapper195_PPUExtLatch;
    p->super.IsStateSave = &mmEmuMapper195_IsStateSave;
    p->super.SaveState = &mmEmuMapper195_SaveState;
    p->super.LoadState = &mmEmuMapper195_LoadState;
}
MM_EXPORT_EMU void mmEmuMapper195_Destroy(struct mmEmuMapper195* p)
{
    mmEmuMapper_Destroy(&p->super);

    mmMemset(p->reg, 0, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 0;

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;

    p->we_sram = 0;

    p->irq_type = 0;
    p->irq_enable = 0;
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    p->patch = 0;
}

MM_EXPORT_EMU void mmEmuMapper195_SetBankCPU(struct mmEmuMapper195* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (p->reg[0] & 0x40)
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, mmu->PROM_08K_SIZE - 2, p->prg1, p->prg0, mmu->PROM_08K_SIZE - 1);
    }
    else
    {
        mmEmuMmu_SetPROM32KBankArray(mmu, p->prg0, p->prg1, mmu->PROM_08K_SIZE - 2, mmu->PROM_08K_SIZE - 1);
    }
}
MM_EXPORT_EMU void mmEmuMapper195_SetBankPPU(struct mmEmuMapper195* p)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    if (mmu->VROM_1K_SIZE)
    {
        if (p->reg[0] & 0x80)
        {
            //mmEmuMmu_SetVROM8KBank(mmu, p->chr4, p->chr5, p->chr6, p->chr7,   p->chr01, p->chr01 + 1, p->chr23, p->chr23 + 1);
            mmEmuMapper195_SetBankPPUSUB(p, 4, p->chr01 + 0);
            mmEmuMapper195_SetBankPPUSUB(p, 5, p->chr01 + 1);
            mmEmuMapper195_SetBankPPUSUB(p, 6, p->chr23 + 0);
            mmEmuMapper195_SetBankPPUSUB(p, 7, p->chr23 + 1);
            mmEmuMapper195_SetBankPPUSUB(p, 0, p->chr4);
            mmEmuMapper195_SetBankPPUSUB(p, 1, p->chr5);
            mmEmuMapper195_SetBankPPUSUB(p, 2, p->chr6);
            mmEmuMapper195_SetBankPPUSUB(p, 3, p->chr7);
        }
        else
        {
            //mmEmuMmu_SetVROM8KBank(mmu, p->chr01, p->chr01+1, p->chr23, p->chr23+1, p->chr4, p->chr5, p->chr6, p->chr7);
            mmEmuMapper195_SetBankPPUSUB(p, 0, p->chr01 + 0);
            mmEmuMapper195_SetBankPPUSUB(p, 1, p->chr01 + 1);
            mmEmuMapper195_SetBankPPUSUB(p, 2, p->chr23 + 0);
            mmEmuMapper195_SetBankPPUSUB(p, 3, p->chr23 + 1);
            mmEmuMapper195_SetBankPPUSUB(p, 4, p->chr4);
            mmEmuMapper195_SetBankPPUSUB(p, 5, p->chr5);
            mmEmuMapper195_SetBankPPUSUB(p, 6, p->chr6);
            mmEmuMapper195_SetBankPPUSUB(p, 7, p->chr7);
        }
    }
    else
    {
        if (p->reg[0] & 0x80)
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 4, (p->chr01 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 5, (p->chr01 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 6, (p->chr23 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 7, (p->chr23 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 0, p->chr4 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 1, p->chr5 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 2, p->chr6 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 3, p->chr7 & 0x07);
        }
        else
        {
            mmEmuMmu_SetCRAM1KBank(mmu, 0, (p->chr01 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 1, (p->chr01 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 2, (p->chr23 + 0) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 3, (p->chr23 + 1) & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 4, p->chr4 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 5, p->chr5 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 6, p->chr6 & 0x07);
            mmEmuMmu_SetCRAM1KBank(mmu, 7, p->chr7 & 0x07);
        }
    }
}
MM_EXPORT_EMU void mmEmuMapper195_SetBankPPUSUB(struct mmEmuMapper195* p, int bank, int page)
{
    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    // [0x00, 0x03]
    if (!p->patch && (0x00 <= page && page <= 0x03))
    {
        mmEmuMmu_SetCRAM1KBank(mmu, bank, page & 7);
    }
    else if (p->patch == 1 && page >= 128)
    {
        mmEmuMmu_SetCRAM1KBank(mmu, bank, page & 7);
    }
    else
    {
        mmEmuMmu_SetVROM1KBank(mmu, bank, page);
    }
}

MM_EXPORT_EMU void mmEmuMapper195_Reset(struct mmEmuMapper* super)
{
    struct mmEmuMapper195* p = (struct mmEmuMapper195*)(super);

    struct mmEmuNes* nes = p->super.nes;
    // struct mmEmuMmu* mmu = &nes->mmu;

    mmUInt32_t crc = 0;

    mmMemset(p->reg, 0x00, sizeof(mmByte_t) * 8);
    p->prg0 = 0;
    p->prg1 = 1;
    mmEmuMapper195_SetBankCPU(p);

    p->chr01 = 0;
    p->chr23 = 2;
    p->chr4 = 4;
    p->chr5 = 5;
    p->chr6 = 6;
    p->chr7 = 7;
    mmEmuMapper195_SetBankPPU(p);

    p->we_sram = 0; // Disable
    p->irq_enable = 0;  // Disable
    p->irq_counter = 0;
    p->irq_latch = 0;
    p->irq_request = 0;

    crc = mmEmuRom_GetPROMCRC(&nes->rom);

    p->patch = 0;
    if (crc == 0x37ae04a8)
    {
        p->patch = 1;
        mmEmuNes_SetRenderMethod(nes, MM_EMU_NES_TILE_RENDER);
    }
}
MM_EXPORT_EMU void mmEmuMapper195_Write(struct mmEmuMapper* super, mmWord_t addr, mmByte_t data)
{
    struct mmEmuMapper195* p = (struct mmEmuMapper195*)(super);

    struct mmEmuNes* nes = p->super.nes;
    struct mmEmuMmu* mmu = &nes->mmu;

    //DEBUGOUT( "MPRWR A=%04X D=%02X L=%3d CYC=%d\n", addr&0xFFFF, data&0xFF, nes->GetScanline(), nes->cpu->GetTotalCycles() );

    switch (addr & 0xE001)
    {
    case 0x8000:
        p->reg[0] = data;
        mmEmuMapper195_SetBankCPU(p);
        mmEmuMapper195_SetBankPPU(p);
        break;
    case 0x8001:
        p->reg[1] = data;

        switch (p->reg[0] & 0x07)
        {
        case 0x00:
            p->chr01 = data & 0xFE;
            mmEmuMapper195_SetBankPPU(p);
            break;
        case 0x01:
            p->chr23 = data & 0xFE;
            mmEmuMapper195_SetBankPPU(p);
            break;
        case 0x02:
            p->chr4 = data;
            mmEmuMapper195_SetBankPPU(p);
            break;
        case 0x03:
            p->chr5 = data;
            mmEmuMapper195_SetBankPPU(p);
            break;
        case 0x04:
            p->chr6 = data;
            mmEmuMapper195_SetBankPPU(p);
            break;
        case 0x05:
            p->chr7 = data;
            mmEmuMapper195_SetBankPPU(p);
            break;
        case 0x06:
            p->prg0 = data;
            mmEmuMapper195_SetBankCPU(p);
            break;
        case 0x07:
            p->prg1 = data;
            mmEmuMapper195_SetBankCPU(p);
            break;
        default:
            break;
        }
        break;
    case 0xA000:
        p->reg[2] = data;
        if (!mmEmuRom_Is4SCREEN(&nes->rom))
        {
            if (data & 0x01)
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_HMIRROR);
            }
            else
            {
                mmEmuMmu_SetVRAMMirror(mmu, MM_EMU_MMU_VRAM_VMIRROR);
            }
        }
        break;
    case 0xA001:
        p->reg[3] = data;
        break;
    case 0xC000:
        p->reg[4] = data;
        p->irq_counter = data;
        p->irq_request = 0;
        break;
    case 0xC001:
        p->reg[5] = data;
        p->irq_latch = data;
        p->irq_request = 0;
        break;
    case 0xE000:
        p->reg[6] = data;
        p->irq_enable = 0;
        p->irq_request = 0;
        mmEmuCpu_ClrIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
        break;
    case 0xE001:
        p->reg[7] = data;
        p->irq_enable = 1;
        p->irq_request = 0;
        break;
    default:
        break;
    }
}

MM_EXPORT_EMU void mmEmuMapper195_HSync(struct mmEmuMapper* super, mmInt_t scanline)
{
    struct mmEmuMapper195* p = (struct mmEmuMapper195*)(super);

    struct mmEmuNes* nes = p->super.nes;

    if ((scanline >= 0 && scanline <= 239))
    {
        if (mmEmuPpu_IsDispON(&nes->ppu))
        {
            if (p->irq_enable && !p->irq_request)
            {
                if (scanline == 0)
                {
                    if (p->irq_counter)
                    {
                        p->irq_counter--;
                    }
                }
                if (!(p->irq_counter--))
                {
                    p->irq_request = 0xFF;
                    p->irq_counter = p->irq_latch;
                    mmEmuCpu_SetIRQ(&nes->cpu, MM_EMU_CPU_IRQ_MAPPER);
                }
            }
        }
    }
}

// For state save
MM_EXPORT_EMU mmBool_t mmEmuMapper195_IsStateSave(const struct mmEmuMapper* super)
{
    return MM_TRUE;
}
MM_EXPORT_EMU void mmEmuMapper195_SaveState(const struct mmEmuMapper* super, mmByte_t* buffer)
{
    struct mmEmuMapper195* p = (struct mmEmuMapper195*)(super);

    mmMemcpy(&buffer[0], &p->reg[0], sizeof(mmByte_t) * 8);
    buffer[8] = p->prg0;
    buffer[9] = p->prg1;
    buffer[10] = p->chr01;
    buffer[11] = p->chr23;
    buffer[12] = p->chr4;
    buffer[13] = p->chr5;
    buffer[14] = p->chr6;
    buffer[15] = p->chr7;
    buffer[16] = p->irq_enable;
    buffer[17] = p->irq_counter;
    buffer[18] = p->irq_latch;
    buffer[19] = p->irq_request;
}
MM_EXPORT_EMU void mmEmuMapper195_LoadState(struct mmEmuMapper* super, const mmByte_t* buffer)
{
    struct mmEmuMapper195* p = (struct mmEmuMapper195*)(super);

    mmMemcpy(&p->reg[0], &buffer[0], sizeof(mmByte_t) * 8);
    p->prg0 = buffer[8];
    p->prg1 = buffer[9];
    p->chr01 = buffer[10];
    p->chr23 = buffer[11];
    p->chr4 = buffer[12];
    p->chr5 = buffer[13];
    p->chr6 = buffer[14];
    p->chr7 = buffer[15];
    p->irq_enable = buffer[16];
    p->irq_counter = buffer[17];
    p->irq_latch = buffer[18];
    p->irq_request = buffer[19];
}

static struct mmEmuMapper* __static_mmEmuMapper195_Produce(void)
{
    struct mmEmuMapper195* p = (struct mmEmuMapper195*)mmMalloc(sizeof(struct mmEmuMapper195));
    mmEmuMapper195_Init(p);
    return (struct mmEmuMapper*)p;
}
static void __static_mmEmuMapper195_Recycle(struct mmEmuMapper* m)
{
    struct mmEmuMapper195* p = (struct mmEmuMapper195*)(m);
    mmEmuMapper195_Destroy(p);
    mmFree(p);
}

MM_EXPORT_EMU const struct mmEmuMapperCreator MM_EMU_MAPPER_CREATOR_MAPPER195 =
{
    &__static_mmEmuMapper195_Produce,
    &__static_mmEmuMapper195_Recycle,
};
